package event

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 上线每分钟事件
type OneMinutePassed struct {
}

func (OneMinutePassed) EventID() uint16 {
	return OneMinutePassedID
}

func (e OneMinutePassed) Event() {
}

// 上线5分钟事件
type FiveMinutePassed struct {
}

func (FiveMinutePassed) EventID() uint16 {
	return FiveMinutePassedID
}

func (e FiveMinutePassed) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 每日刷新事件
type OnDayRefresh struct {
}

func (OnDayRefresh) EventID() uint16 {
	return OnDayRefreshID
}

func (e OnDayRefresh) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 每日零点事件
type OnDayZeroClock struct {
}

func (OnDayZeroClock) EventID() uint16 {
	return OnDayZeroClockID
}

func (e OnDayZeroClock) Event() {
}
