package event

const (
	OneMinutePassedID  uint16 = iota + 1 // 上线每分钟事件
	FiveMinutePassedID                   // 上线每分钟事件
	OnDayRefreshID                       // 每日刷新
	OnDayZeroClockID                     // 每日零点
)
