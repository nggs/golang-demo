//go:build ignore
// +build ignore

package cache

import (
	"testing"

	"github.com/asynkron/protoactor-go/actor"

	"model"
)

func TestZone_Role(t *testing.T) {
	cache := New()

	roleID1 := model.AccountIDServerIDToRoleID(1, 1)
	roleID2 := model.AccountIDServerIDToRoleID(1, 2)
	roleID3 := model.AccountIDServerIDToRoleID(1, 3)

	cache.SetRole(roleID1, nil, false)
	cache.SetRole(model.AccountIDServerIDToRoleID(1, 2), nil, false)

	{
		_, _, _, ok := cache.GetAccountOnlineRole(1)
		if ok {
			t.Error("GetAccountOnlineRole error")
			return
		}
	}

	cache.SetRole(roleID1, &actor.PID{Address: "localhost", Id: "1"}, true)

	{
		serverID, roleID, rolePID, ok := cache.GetAccountOnlineRole(1)
		if !ok {
			t.Error("GetAccountOnlineRole error")
			return
		}
		if roleID != model.AccountIDServerIDToRoleID(1, 1) {
			t.Errorf("GetAccountOnlineRole error")
			return
		}

		t.Logf("online Role: server id=%d, Role id=%d, Role pid=%v", serverID, roleID, rolePID)
	}

	cache.SetRole(roleID2, &actor.PID{Address: "localhost", Id: "2"}, true)

	{
		serverID, roleID, rolePID, ok := cache.GetAccountOnlineRole(1)
		if !ok {
			t.Error("GetAccountOnlineRole error")
			return
		}

		if roleID != model.AccountIDServerIDToRoleID(1, 2) {
			t.Errorf("SetAccountOnlineRole error")
			return
		}

		t.Logf("online Role: server id=%d, Role id=%d, Role pid=%v", serverID, roleID, rolePID)
	}

	cache.SetRole(roleID3, &actor.PID{Address: "localhost", Id: "3"}, true)

	{
		serverID, roleID, rolePID, ok := cache.GetAccountOnlineRole(1)
		if !ok {
			t.Error("GetAccountOnlineRole error")
			return
		}

		if roleID != model.AccountIDServerIDToRoleID(1, 3) {
			t.Errorf("SetAccountOnlineRole error")
			return
		}

		t.Logf("online Role: server id=%d, Role id=%d, Role pid=%v", serverID, roleID, rolePID)
	}
}
