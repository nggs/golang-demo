//go:build ignore
// +build ignore

package cache

import (
	"fmt"
	"time"

	"github.com/asynkron/protoactor-go/actor"

	"model"
)

type Role struct {
	*model.RoleMinimal
	lastTouchTime int64
	pid           *actor.PID
	os            string
	channel       string
}

func NewRole(id int64, pid *actor.PID) *Role {
	p := &Role{
		RoleMinimal:   model.Get_RoleMinimal(),
		lastTouchTime: time.Now().Unix(),
		pid:           pid.Clone(),
	}
	p.ID = id
	p.AccountID, p.ServerID = model.RoleIDToAccountIDServerID(p.ID)
	return p
}

func (p Role) Clone() *Role {
	n := &Role{}
	n.RoleMinimal = p.RoleMinimal.Clone()
	if p.pid != nil {
		n.pid = p.pid.Clone()
	}
	return n
}

func (p Role) PID() *actor.PID {
	return p.pid.Clone()
}

func (p *Role) SetPID(pid *actor.PID) {
	p.pid = pid.Clone()
}

func (p *Role) Touch() {
	p.lastTouchTime = time.Now().Unix()
}

func (p Role) LastTouchTime() time.Time {
	return time.Unix(p.lastTouchTime, 0)
}

func (p Role) OS() string {
	return p.os
}

func (p Role) Channel() string {
	return p.channel
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type RoleContainer map[int64]*Role

func ToRoleContainer(m map[int64]*Role) *RoleContainer {
	if m == nil {
		return nil
	}
	return (*RoleContainer)(&m)
}

func NewRoleContainer() (m *RoleContainer) {
	m = &RoleContainer{}
	return
}

func (m *RoleContainer) Get(key int64) (value *Role, ok bool) {
	value, ok = (*m)[key]
	if ok {
		value.Touch()
	}
	return
}

func (m *RoleContainer) Set(key int64, value *Role) {
	(*m)[key] = value
}

//func (m *RoleContainer) Add(key int64) (value *Role) {
//	value = &Role{}
//	value.Touch()
//	(*m)[key] = value
//	return
//}

func (m *RoleContainer) Remove(key int64) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *RoleContainer) RemoveOne(fn func(key int64, value *Role) (removed bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *RoleContainer) RemoveSome(fn func(key int64, value *Role) (removed bool)) {
	left := map[int64]*Role{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *RoleContainer) Each(f func(key int64, value *Role) (continued bool)) {
	for key, value := range *m {
		value.Touch()
		if !f(key, value) {
			break
		}
	}
}

func (m RoleContainer) Size() int {
	return len(m)
}

func (m RoleContainer) Clone() (n *RoleContainer) {
	if m.Size() == 0 {
		return nil
	}
	n = ToRoleContainer(make(map[int64]*Role, m.Size()))
	for k, v := range m {
		if v != nil {
			(*n)[k] = v.Clone()
		} else {
			(*n)[k] = nil
		}
	}
	return n
}

func (m *RoleContainer) Clear() {
	*m = *NewRoleContainer()
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (m *Zone) RoleContainer() *RoleContainer {
	if m.roles == nil {
		m.roles = *NewRoleContainer()
	}
	return (*RoleContainer)(&m.roles)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (m Zone) GetRole(roleID int64) (rolePID *actor.PID, isOnline bool, ok bool) {
	var p *Role
	p, ok = m.RoleContainer().Get(roleID)
	if !ok {
		return
	}

	rolePID = p.pid

	if accountOnlineRoleID, ok := m.accountOnlineRoleIDs[p.AccountID]; ok && accountOnlineRoleID == p.ID {
		isOnline = true
	}

	p.Touch()

	return
}

func (m *Zone) SetRole(roleID int64, rolePID *actor.PID, isOnline bool) {
	roleContainer := m.RoleContainer()
	p, ok := roleContainer.Get(roleID)
	if !ok {
		p = NewRole(roleID, rolePID)
		roleContainer.Set(roleID, p)
	} else {
		p.pid = rolePID.Clone()
		p.Touch()
	}
	if isOnline {
		m.accountOnlineRoleIDs[p.AccountID] = roleID
	} else {
		if accountOnlineRoleID, ok := m.accountOnlineRoleIDs[p.AccountID]; ok && accountOnlineRoleID == roleID {
			delete(m.accountOnlineRoleIDs, roleID)
		}
	}
	if roleIDs, ok := m.accountRoleIDs[p.AccountID]; ok {
		if _, ok := roleIDs[roleID]; !ok {
			roleIDs[roleID] = struct{}{}
		}
	} else {
		m.accountRoleIDs[p.AccountID] = map[int64]struct{}{
			roleID: {},
		}
	}
	return
}

func (m *Zone) RemoveRole(roleID int64) {
	roleContainer := m.RoleContainer()
	p, ok := roleContainer.Get(roleID)
	if !ok {
		return
	}

	roleContainer.Remove(roleID)

	if accountOnlineRoleID, ok := m.accountOnlineRoleIDs[p.AccountID]; ok && accountOnlineRoleID == roleID {
		delete(m.accountOnlineRoleIDs, p.AccountID)
	}

	if roleIDs, ok := m.accountRoleIDs[p.AccountID]; ok {
		delete(roleIDs, roleID)
	}

	return
}

func (m *Zone) GetAccountOnlineRole(accountID int64) (serverID int32, roleID int64, rolePID *actor.PID, ok bool) {
	roleID, ok = m.accountOnlineRoleIDs[accountID]
	if !ok {
		return
	}

	var p *Role
	p, ok = m.RoleContainer().Get(roleID)
	if !ok || p == nil {
		return
	}

	serverID = p.ServerID
	rolePID = p.pid.Clone()

	p.Touch()

	return
}

func (m *Zone) AddLoginExtraInfo(roleID int64, os string, channel string) (err error) {
	p, ok := m.RoleContainer().Get(roleID)
	if !ok || p == nil {
		err = fmt.Errorf("role not exist")
		return
	}
	p.os = os
	p.channel = channel
	return
}

func (m *Zone) SetRoleOffline(roleID int64) bool {
	accountID, _ := model.RoleIDToAccountIDServerID(roleID)
	if accountOnlineRoleID, ok := m.accountOnlineRoleIDs[accountID]; ok && accountOnlineRoleID == roleID {
		delete(m.accountOnlineRoleIDs, accountID)
		return true
	}
	return false
}

func (m *Zone) EachAccountRole(accountID int64, fn func(role *Role) (continued bool)) {
	roleIDs, ok := m.accountRoleIDs[accountID]
	if !ok {
		return
	}

	roleContainer := m.RoleContainer()

	for roleID, _ := range roleIDs {
		p, ok := roleContainer.Get(roleID)
		if !ok {
			continue
		}
		if !fn(p) {
			break
		}
	}
}

func (m *Zone) AccountOnlineRoleIDsContainer() *model.Int64ToInt64Map {
	if m.accountOnlineRoleIDs == nil {
		m.accountOnlineRoleIDs = *model.NewInt64ToInt64Map()
	}
	return (*model.Int64ToInt64Map)(&m.accountOnlineRoleIDs)
}
