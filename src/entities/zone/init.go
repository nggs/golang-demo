package zone

import (
	"server/src/entities/zone/logic/common"
	"server/src/entities/zone/logic/role"
)

func Init() {
	common.Init()
	role.Init()
}
