package zone

import (
	"fmt"
	"rpc"
	"runtime/debug"
	"server/src/entities/zone/logic"
	"sync"
	"time"

	"github.com/asynkron/protoactor-go/actor"
	"github.com/globalsign/mgo/bson"

	nactor "nggs/actor"
	nlog "nggs/log"
	nrpc "nggs/rpc"

	"cluster"
	"model"
	"server/src/entities/zone/cache"
	"server/src/entities/zone/event"
	"server/src/entities/zone/export"
)

////////////////////////////////////////////////////////////////////////////////
type Entity struct {
	*nactor.Actor

	data *model.Zone // 需要保存到数据库的数据

	cache *cache.Zone

	dbc *model.SimpleClient

	globalCfg cluster.ZonesGlobalConfig
	cfg       cluster.ZonesConfig

	fiveMinuteCounter int // 5分钟计数器
}

func New(logger nlog.ILogger, data *model.Zone, dbc *model.SimpleClient, globalCfg cluster.ZonesGlobalConfig, cfg cluster.ZonesConfig,
	startedWg *sync.WaitGroup, stoppedWg *sync.WaitGroup) export.IZone {
	e := &Entity{
		data:      data,
		cache:     cache.New(),
		dbc:       dbc,
		globalCfg: globalCfg,
		cfg:       cfg,
	}

	e.Actor = nactor.New(
		nactor.WithLogger(logger),
		nactor.WithStartedWaitGroup(startedWg),
		nactor.WithStoppedWaitGroup(stoppedWg),
		nactor.WithOnStarted(e.onStarted),
		nactor.WithOnStopping(e.onStopping),
		nactor.WithOnStopped(e.onStopped),
		nactor.WithOnReceiveMessage(e.onReceiveMessage),
		nactor.WithOnActorTerminate(e.onActorTerminated),
		nactor.WithLogics(logic.GenerateLogicMap(e)),
		nactor.WithPulse(60*time.Second, e.onPulse),
		nactor.WithRPC(rpc.Protocol, func(sender *actor.PID, iRequestMessage nrpc.IMessage, ctx actor.Context) (args []interface{}, ok bool, err error) {
			if e.data == nil {
				e.Error("before dispatch rpc, data is nil")
				return
			}
			if e.cache == nil {
				e.Error("before dispatch rpc, cache is nil")
				return
			}
			ok = true
			return
		}, nil),
	)

	return e
}

func (Entity) IZone() {}

func (a *Entity) onStarted(ctx actor.Context) {
	a.SetSign(fmt.Sprintf("zone-%d", a.data.ID))

	//// 向world注册
	//cluster.SendMsgToWorld(&rpc.C2S_RegisterZone{
	//	ID:       a.data.ID,
	//	OpenTime: a.data.OpenTime,
	//	Name:     a.data.Name,
	//	PID:      a.PID(),
	//})
}

func (a *Entity) onStopping(ctx actor.Context) {
	// 保存数据
	a.SaveData(true)
}

func (a *Entity) onStopped(ctx actor.Context) {
	//// 向world反注册
	//cluster.SendMsgToWorld(&rpc.C2S_UnRegisterZone{
	//	ID: a.data.ID,
	//})

	//if a.data != nil {
	//	// 回收对象
	//	model.Put_Zone(a.data)
	//	a.data = nil
	//}
}

func (a *Entity) onReceiveMessage(ctx actor.Context) {
	defer func() {
		if r := recover(); r != nil {
			a.Error("%v\n%s", r, debug.Stack())
			panic(r)
		}
	}()

	sender := ctx.Sender()
	switch msg := ctx.Message().(type) {
	default:
		a.Error("recv unsupported msg [%#v] from [%v]", msg, sender)
	}
}

func (a *Entity) onActorTerminated(who *actor.PID, ctx actor.Context) {
	a.Debug("[%+v] terminated", who)
}

func (a *Entity) onPulse(ctx actor.Context) {
	// 定时保存，现阶段每分钟保存一次
	a.SaveData(false)

	// 触发1分钟事件
	a.PostEvent(&event.OneMinutePassed{})

	a.fiveMinuteCounter += 1
	if a.fiveMinuteCounter >= 5 {
		a.fiveMinuteCounter = 0
		// 触发5分钟事件
		a.PostEvent(&event.FiveMinutePassed{})
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (a Entity) DBC() *model.SimpleClient {
	return a.dbc
}

func (a Entity) Data() *model.Zone {
	return a.data
}

func (a *Entity) SaveData(force bool) {
	if a.data == nil || a.cache == nil {
		return
	}

	if !force {
		if !a.cache.Dirty {
			return
		}
	}

	dbSession := a.dbc.GetSession()
	defer a.dbc.PutSession(dbSession)
	_, err := a.dbc.UpsertZone(dbSession, bson.M{"_id": a.data.ID}, a.data)
	if err != nil {
		a.Error("save data fail, %s", err)
	} else {
		a.Debug("save data success")
	}

	a.cache.Dirty = false
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (a *Entity) Cache() *cache.Zone {
	return a.cache
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (a Entity) GlobalConfig() cluster.ZonesGlobalConfig {
	return a.globalCfg
}

func (a Entity) Config() cluster.ZonesConfig {
	return a.cfg
}
