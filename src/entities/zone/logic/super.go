package logic

import (
	"fmt"

	"github.com/asynkron/protoactor-go/actor"

	"server/src/entities/zone/export"
)

type Super struct {
	export.IZone

	ICommon export.ICommon
	IRole   export.IRole
}

func NewSuper(iZone export.IZone) Super {
	return Super{
		IZone: iZone,
	}
}

func (Super) ILogic() {

}

func (l *Super) Init() error {
	l.ICommon = l.GetLogic(Common).(export.ICommon)
	if l.ICommon == nil {
		return fmt.Errorf("get Common logic fail")
	}
	l.IRole = l.GetLogic(Role).(export.IRole)
	if l.IRole == nil {
		return fmt.Errorf("get Role logic fail")
	}
	return nil
}

func (Super) Run() {

}

func (Super) Finish() {

}

func (Super) OnActorTerminated(who *actor.PID, ctx actor.Context) {

}

func (Super) OnPulse(ctx actor.Context) {

}
