package logic

import (
	"log"

	nexport "nggs/export"

	"server/src/entities/zone/export"
)

const (
	Example nexport.LogicID = iota
	Common
	Role
)

type Factory struct {
	ID  nexport.LogicID
	New func(zone export.IZone) nexport.ILogic
}

var gFactoryMap = map[nexport.LogicID]*Factory{}

func GetFactory(id nexport.LogicID) (*Factory, bool) {
	if factory, ok := gFactoryMap[id]; ok {
		return factory, true
	}
	return nil, false
}

func RegisterFactory(id nexport.LogicID, New func(export.IZone) nexport.ILogic) {
	if _, ok := GetFactory(id); ok {
		log.Panicf("logic factory already exist, id=[%d]", id)
	}

	factory := &Factory{
		ID:  id,
		New: New,
	}

	gFactoryMap[factory.ID] = factory
}

func GenerateLogicMap(zone export.IZone) map[nexport.LogicID]nexport.ILogic {
	logicMap := map[nexport.LogicID]nexport.ILogic{}
	for id, factory := range gFactoryMap {
		logicMap[id] = factory.New(zone)
	}
	return logicMap
}
