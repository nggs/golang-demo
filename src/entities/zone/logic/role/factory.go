package role

import (
	nexport "nggs/export"

	"server/src/entities/zone/export"
	"server/src/entities/zone/logic"
)

const (
	ID = logic.Role
)

func NewLogic(iZone export.IZone) nexport.ILogic {
	l := &Logic{
		Super: logic.NewSuper(iZone),
	}
	return l
}

func Init() {
	logic.RegisterFactory(ID, NewLogic)
}
