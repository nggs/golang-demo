package role

import (
	"github.com/asynkron/protoactor-go/actor"

	nrpc "nggs/rpc"

	"rpc"
)

func (l *Logic) regAllRPCHandler() (err error) {
	err = l.RegisterRPCHandler(rpc.C2S_SpawnRole_MessageID(), l.handleSpawnRole)
	if err != nil {
		return
	}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) handleSpawnRole(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_SpawnRole)
	send := iSend.(*rpc.S2C_SpawnRole)
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
		l.Info("handleSpawnRole, recv=%s, send=%s", recv.String(), send.String())
	}()

	if recv.AccountID <= 0 || recv.ServerID <= 0 {
		l.Error("handleSpawnRole recv.AccountID <= 0 || recv.ServerID <= 0")
		send.EC = rpc.EC_Fail
		return
	}

	//sessionPID := recv.SessionPID.ToReal()
	//if nactor.IsPIDPtrEmpty(sessionPID) {
	//	l.Error("handleSpawnRole fail, recv.SessionPID is empty")
	//	send.EC = rpc.EC_Fail
	//	return
	//}

	if recv.Account == nil {
		l.Error("handleSpawnRole fail, recv.Account is nil")
		send.EC = rpc.EC_Fail
		return
	}

	recv.Zone = l.Data().ToInRole(nil)

	rolePID, err := rpc.ForwardSpawnRoleToGame(recv)
	if err != nil {
		l.Error("handleSpawnRole rpc.ForwardSpawnRoleToGame fail, %s", err)
		send.EC = rpc.EC_Fail
		return
	}

	send.RolePID = rpc.FromRealPID(rolePID)
}
