package common

import (
	"server/src/entities/zone/logic"

	nexport "nggs/export"
)

type Logic struct {
	logic.Super
}

func (Logic) ID() nexport.LogicID {
	return ID
}

func (Logic) ICommon() {

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) Init() (err error) {
	err = l.Super.Init()
	if err != nil {
		return
	}
	err = l.regAllRPCHandler()
	if err != nil {
		return
	}
	return nil
}
