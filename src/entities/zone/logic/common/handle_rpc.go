package common

import (
	"server/src/entities/zone/event"

	"github.com/asynkron/protoactor-go/actor"

	nrpc "nggs/rpc"

	"rpc"
)

func (l *Logic) regAllRPCHandler() (err error) {
	err = l.RegisterRPCHandler(rpc.C2S_GetZoneData_MessageID(), l.handleGetZoneData)
	if err != nil {
		return
	}
	err = l.RegisterRPCHandler(rpc.C2S_GetZoneMinimal_MessageID(), l.handleGetZoneMinimal)
	if err != nil {
		return
	}
	err = l.RegisterRPCHandler(rpc.C2S_DayRefresh_MessageID(), l.handleDayRefresh)
	if err != nil {
		return
	}
	err = l.RegisterRPCHandler(rpc.C2S_DayZeroClock_MessageID(), l.handleDayZeroClock)
	if err != nil {
		return
	}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) handleGetZoneData(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	//recv := iRecv.(*rpc.C2S_GetZoneData)
	send := iSend.(*rpc.S2C_GetZoneData)
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
	}()

	zoneData := l.Data()
	if zoneData == nil {
		l.Error("handleGetZoneData zoneData is nil")
		send.EC = rpc.EC_Fail
		return
	}

	send.Data = zoneData.Clone()
}

func (l *Logic) handleGetZoneMinimal(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	//recv := iRecv.(*rpc.C2S_GetZoneMinimal)
	send := rpc.Get_S2C_GetZoneMinimal()
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
	}()

	zoneData := l.Data()
	if zoneData == nil {
		l.Error("handleGetZoneMinimal zoneData is nil")
		send.EC = rpc.EC_Fail
		return
	}

	send.Minimal = zoneData.ToMinimal(send.Minimal)
}

func (l *Logic) handleDayRefresh(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	l.PostEvent(&event.OnDayRefresh{})
}

func (l *Logic) handleDayZeroClock(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	l.PostEvent(&event.OnDayZeroClock{})
}
