package account

import (
	"fmt"
	"runtime/debug"
	"sync"
	"time"

	nrpc "nggs/rpc"

	nlog "nggs/log"

	"github.com/asynkron/protoactor-go/actor"
	"github.com/globalsign/mgo/bson"

	nactor "nggs/actor"

	"cluster"
	"model"
	"rpc"
	"server/src/entities/account/cache"
	"server/src/entities/account/export"
	"server/src/entities/account/logic"
)

////////////////////////////////////////////////////////////////////////////////
type Entity struct {
	*nactor.Actor

	data *model.Account // 需要保存到数据库的数据

	cache *cache.Account

	dbc *model.SimpleClient

	globalCfg cluster.AccountsGlobalConfig
	cfg       cluster.AccountsConfig
}

func New(logger nlog.ILogger, data *model.Account, dbc *model.SimpleClient, globalCfg cluster.AccountsGlobalConfig, cfg cluster.AccountsConfig,
	startedWg *sync.WaitGroup, stoppedWg *sync.WaitGroup) export.IAccount {
	e := &Entity{
		data:      data,
		cache:     cache.New(),
		dbc:       dbc,
		globalCfg: globalCfg,
		cfg:       cfg,
	}

	e.Actor = nactor.New(
		nactor.WithLogger(logger),
		nactor.WithStartedWaitGroup(startedWg),
		nactor.WithStoppedWaitGroup(stoppedWg),
		nactor.WithOnStarted(e.onStarted),
		nactor.WithOnStopping(e.onStopping),
		nactor.WithOnStopped(e.onStopped),
		nactor.WithOnReceiveMessage(e.onReceiveMessage),
		nactor.WithOnActorTerminate(e.onActorTerminated),
		nactor.WithLogics(logic.GenerateLogicMap(e)),
		nactor.WithPulse(60*time.Second, e.onPulse),
		nactor.WithRPC(rpc.Protocol, func(sender *actor.PID, iRequestMessage nrpc.IMessage, ctx actor.Context) (args []interface{}, ok bool, err error) {
			if e.data == nil {
				e.Error("before dispatch rpc, data is nil")
				return
			}
			if e.cache == nil {
				e.Error("before dispatch rpc, cache is nil")
				return
			}
			ok = true
			return
		}, nil),
	)

	return e
}

func (Entity) IAccount() {}

func (e *Entity) onStarted(ctx actor.Context) {
	e.SetSign(fmt.Sprintf("account-%d", e.data.ID))
}

func (e *Entity) onPulse(ctx actor.Context) {
	// 每次心跳都检查是否脏数据，如果是脏数据则保存
	e.SaveData(false)

	if e.cache.RoleContainer().Size() == 0 && e.globalCfg.IdleToStopSec > 0 {
		now := time.Now()
		if now.Sub(e.cache.LastDataSaveTime) > e.globalCfg.IdleToStopSec {
			// 空闲超时，下线
			e.Info("idle too long time, now=%v, last data save time=%v", now, e.cache.LastDataSaveTime)
			_ = e.Stop()
			return
		}
	}
}

func (e *Entity) onStopping(ctx actor.Context) {
	// 保存数据
	e.SaveData(true)
}

func (e *Entity) onStopped(ctx actor.Context) {
	//if e.data != nil {
	//	// 回收对象
	//	//model.Put_Account(e.data)
	//	e.data = nil
	//}
}

func (e *Entity) onReceiveMessage(ctx actor.Context) {
	defer func() {
		if r := recover(); r != nil {
			e.Error("%v\n%s", r, debug.Stack())
			panic(r)
		}
	}()

	sender := ctx.Sender()
	switch msg := ctx.Message().(type) {
	case string:
		switch msg {
		case "id":
			if sender != nil {
				if e.data != nil {
					ctx.Respond(e.data.ID)
				} else {
					ctx.Respond(0)
				}
			}

		case "stop":
			_ = e.Stop()
			if sender != nil {
				ctx.Respond("stop")
			}

		default:
			e.Error("unknown command %s", msg)
		}

	default:
		e.Error("recv unsupported msg [%#v] from [%v]", msg, sender)
	}
}

func (e *Entity) onActorTerminated(who *actor.PID, ctx actor.Context) {
	e.Debug("[%+v] terminated", who)

	var appID int32
	var gameID int32
	var roleID int64
	if _, err := fmt.Sscanf(who.Id, "app-%d/game-%d/role-%d", &appID, &gameID, &roleID); err == nil {
		e.cache.RemoveRole(roleID)
		e.Info("remove role from cache, roleID=%d", roleID)
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (e Entity) DBC() *model.SimpleClient {
	return e.dbc
}

func (e Entity) Data() *model.Account {
	return e.data
}

func (e *Entity) SaveData(force bool) {
	if e.data == nil || e.cache == nil {
		return
	}

	if !force {
		if !e.cache.Dirty {
			return
		}
	}

	dbSession := e.dbc.GetSession()
	defer e.dbc.PutSession(dbSession)

	_, err := e.dbc.UpsertAccount(dbSession, bson.M{"_id": e.data.ID}, e.data)
	if err != nil {
		e.Error("save data fail, %s", err)
	} else {
		e.Debug("save data success")
	}

	e.cache.Dirty = false
	e.cache.LastDataSaveTime = time.Now()
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (e *Entity) Cache() *cache.Account {
	return e.cache
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (e Entity) GlobalConfig() cluster.AccountsGlobalConfig {
	return e.globalCfg
}

func (e Entity) Config() cluster.AccountsConfig {
	return e.cfg
}
