package cache

import (
	"encoding/json"
	"time"

	"github.com/asynkron/protoactor-go/actor"

	"model"
)

type Role struct {
	*model.RoleMinimal
	lastTouchTime int64
	pid           *actor.PID
}

func NewRole(id int64, pid *actor.PID) *Role {
	p := &Role{
		RoleMinimal:   model.Get_RoleMinimal(),
		lastTouchTime: time.Now().Unix(),
		pid:           pid.Clone(),
	}
	p.ID = id
	p.AccountID, p.ServerID = model.RoleIDToAccountIDServerID(p.ID)
	return p
}

func (p Role) Clone() *Role {
	n := &Role{}
	n.RoleMinimal = p.RoleMinimal.Clone()
	n.lastTouchTime = p.lastTouchTime
	if p.pid != nil {
		n.pid = p.pid.Clone()
	}
	return n
}

func (p Role) PID() *actor.PID {
	return p.pid.Clone()
}

func (p *Role) SetPID(pid *actor.PID) {
	p.pid = pid.Clone()
}

func (p *Role) Touch() {
	p.lastTouchTime = time.Now().Unix()
}

func (p Role) LastTouchTime() time.Time {
	return time.Unix(p.lastTouchTime, 0)
}

func (p Role) String() string {
	ba, _ := json.Marshal(p)
	return string(ba)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type RoleContainer map[int64]*Role

func ToRoleContainer(m map[int64]*Role) *RoleContainer {
	if m == nil {
		return nil
	}
	return (*RoleContainer)(&m)
}

func NewRoleContainer() (m *RoleContainer) {
	m = &RoleContainer{}
	return
}

func (m *RoleContainer) Get(key int64) (value *Role, ok bool) {
	if m == nil {
		return
	}
	value, ok = (*m)[key]
	if ok {
		value.Touch()
	}
	return
}

func (m *RoleContainer) Set(key int64, value *Role) {
	if m == nil {
		return
	}
	(*m)[key] = value
}

func (m *RoleContainer) Add(key int64) (value *Role) {
	if m == nil {
		return
	}
	value = &Role{}
	value.Touch()
	(*m)[key] = value
	return
}

func (m *RoleContainer) Remove(key int64) (removed bool) {
	if m == nil {
		return
	}
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *RoleContainer) RemoveOne(fn func(key int64, value *Role) (continued bool)) {
	if m == nil {
		return
	}
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *RoleContainer) RemoveSome(fn func(key int64, value *Role) (continued bool)) {
	left := map[int64]*Role{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *RoleContainer) Each(f func(key int64, value *Role) (continued bool)) {
	for key, value := range *m {
		value.Touch()
		if !f(key, value) {
			break
		}
	}
}

func (m RoleContainer) Size() int {
	return len(m)
}

func (m RoleContainer) Clone() (n *RoleContainer) {
	if m.Size() == 0 {
		return nil
	}
	n = ToRoleContainer(make(map[int64]*Role, m.Size()))
	for k, v := range m {
		if v != nil {
			(*n)[k] = v.Clone()
		} else {
			(*n)[k] = nil
		}
	}
	return n
}

func (m *RoleContainer) Clear() {
	*m = *NewRoleContainer()
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (m Account) GetRole(roleID int64) (rolePID *actor.PID, isOnline bool, ok bool) {
	if roleID <= 0 {
		return
	}

	var p *Role
	p, ok = m.RoleContainer().Get(roleID)
	if !ok {
		return
	}

	rolePID = p.pid

	if m.onlineRoleID > 0 && roleID == m.onlineRoleID {
		isOnline = true
	}

	p.Touch()

	return
}

func (m *Account) SetRole(roleID int64, rolePID *actor.PID, isOnline bool) {
	if roleID <= 0 {
		return
	}

	roleContainer := m.RoleContainer()
	p, ok := roleContainer.Get(roleID)
	if !ok {
		p = NewRole(roleID, rolePID)
		roleContainer.Set(roleID, p)
	} else {
		p.pid = rolePID.Clone()
		p.Touch()
	}

	if isOnline {
		m.onlineRoleID = roleID
	} else {
		if m.onlineRoleID > 0 && roleID == m.onlineRoleID {
			m.onlineRoleID = 0
		}
	}
}

func (m *Account) RemoveRole(roleID int64) {
	roleContainer := m.RoleContainer()
	_, ok := roleContainer.Get(roleID)
	if !ok {
		return
	}

	roleContainer.Remove(roleID)

	if m.onlineRoleID > 0 && roleID == m.onlineRoleID {
		m.onlineRoleID = 0
	}
}

func (m *Account) GetOnlineRole() (onlineRole *Role, ok bool) {
	if m.onlineRoleID <= 0 {
		return
	}

	onlineRole, ok = m.RoleContainer().Get(m.onlineRoleID)
	if !ok || onlineRole == nil {
		return
	}

	onlineRole.Touch()

	return
}

func (m *Account) SetRoleOffline(roleID int64) bool {
	if m.onlineRoleID > 0 && roleID == m.onlineRoleID {
		m.onlineRoleID = 0
		return true
	}
	return false
}

func (m *Account) SetRoleOnline(roleID int64) bool {
	if roleID >= 0 && roleID != m.onlineRoleID {
		m.onlineRoleID = roleID
		return true
	}
	return false
}
