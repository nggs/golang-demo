package cache

import (
	"testing"

	"github.com/asynkron/protoactor-go/actor"

	"model"
)

func Test_Role(t *testing.T) {
	cache := New()

	roleID1 := model.AccountIDServerIDToRoleID(1, 1)
	roleID2 := model.AccountIDServerIDToRoleID(1, 2)
	roleID3 := model.AccountIDServerIDToRoleID(1, 3)

	cache.SetRole(roleID1, nil, false)
	cache.SetRole(roleID2, nil, false)

	{
		_, ok := cache.GetOnlineRole()
		if ok {
			t.Error("GetOnlineRole error")
			return
		}
	}

	cache.SetRole(roleID1, &actor.PID{Address: "localhost", Id: "1"}, true)

	{
		onlineRole, ok := cache.GetOnlineRole()
		if !ok {
			t.Error("GetOnlineRole error")
			return
		}
		if onlineRole.ID != model.AccountIDServerIDToRoleID(1, 1) {
			t.Errorf("GetOnlineRole error")
			return
		}

		t.Logf("online Role: server id=%d, Role id=%d, Role pid=%v", onlineRole.ServerID, onlineRole.ServerID, onlineRole.PID())
	}

	cache.SetRole(roleID2, &actor.PID{Address: "localhost", Id: "2"}, true)

	{
		onlineRole, ok := cache.GetOnlineRole()
		if !ok {
			t.Error("GetOnlineRole error")
			return
		}

		if onlineRole.ID != model.AccountIDServerIDToRoleID(1, 2) {
			t.Errorf("SetOnlineRole error")
			return
		}

		t.Logf("online Role: server id=%d, Role id=%d, Role pid=%v", onlineRole.ServerID, onlineRole.ID, onlineRole.PID())
	}

	cache.SetRole(roleID3, &actor.PID{Address: "localhost", Id: "3"}, true)

	{
		onlineRole, ok := cache.GetOnlineRole()
		if !ok {
			t.Error("GetOnlineRole error")
			return
		}

		if onlineRole.ID != model.AccountIDServerIDToRoleID(1, 3) {
			t.Errorf("SetOnlineRole error")
			return
		}

		t.Logf("online Role: server id=%d, Role id=%d, Role pid=%v", onlineRole.ServerID, onlineRole.ID, onlineRole.PID())
	}
}
