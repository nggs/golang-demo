package cache

import "time"

// 账号缓存数据
type Account struct {
	Dirty            bool
	LastDataSaveTime time.Time

	roles        map[int64]*Role // roleID -> 被激活的角色id，不一定在线
	onlineRoleID int64           // 在线角色id，一个号只能有一个角色在线
}

func New() *Account {
	c := &Account{
		LastDataSaveTime: time.Now(),
		roles:            map[int64]*Role{},
	}
	return c
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (m *Account) RoleContainer() *RoleContainer {
	if m.roles == nil {
		m.roles = *NewRoleContainer()
	}
	return (*RoleContainer)(&m.roles)
}
