package auth

import (
	"time"

	nexport "nggs/export"

	"github.com/dgrijalva/jwt-go"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ErrInvalidTokenString struct {
}

func (e ErrInvalidTokenString) Error() string {
	return "invalid token string"
}

type ErrTokenExpired struct {
}

func (e ErrTokenExpired) Error() string {
	return "token expired"
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type Claims struct {
	jwt.StandardClaims
	GatewayID nexport.ServiceID
	AccountID int64
	ServerID  int32
	IsRelogin bool
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func GenerateToken(tokenKey string, tokenExpireSec int64, gatewayID nexport.ServiceID, accountID int64, serverID int32, isRelogin bool) (string, error) {
	//return util.GenRandomString(16), nil

	claims := &Claims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Unix() + tokenExpireSec,
		},
		GatewayID: gatewayID,
		AccountID: accountID,
		ServerID:  serverID,
		IsRelogin: isRelogin,
	}

	t := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := t.SignedString([]byte(tokenKey))
	return ss, err
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func ParseToken(tokenKey string, tokenString string) (claims *Claims, err error) {
	token, err := jwt.ParseWithClaims(tokenString, &Claims{},
		func(token *jwt.Token) (interface{}, error) { return []byte(tokenKey), nil })
	if err != nil {
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				// That's not even a token
				return nil, &ErrInvalidTokenString{}
			} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
				// Token is either expired or not active yet
				return nil, &ErrTokenExpired{}
			} else {
				// Couldn't handle this token
				return nil, &ErrInvalidTokenString{}
			}
		} else {
			// Couldn't handle this token
			return nil, &ErrInvalidTokenString{}
		}
	}
	if !token.Valid {
		return nil, &ErrInvalidTokenString{}
	}

	return token.Claims.(*Claims), nil
}
