package auth

import (
	"testing"
)

const (
	testTokenKey       = "AzeUntSFl7a85Jyr"
	testTokenExpireSec = 5
)

func TestTokenGenerateAndParse(t *testing.T) {
	tokenString, err := GenerateToken(testTokenKey, testTokenExpireSec, 1, 1, 1, true)
	t.Logf(tokenString)

	c, err := ParseToken(testTokenKey, tokenString)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("%#v\n", c)
}
