package common

import (
	"model"
	"time"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	nrpc "nggs/rpc"

	"cluster"
	"rpc"

	"server/src/entities/account/auth"
	loginMsg "server/src/services/login/http_msg"
)

func (l *Logic) regAllRPCHandler() (err error) {
	err = l.RegisterRPCHandler(rpc.C2S_AccountLogin_MessageID(), l.handleAccountLogin)
	if err != nil {
		return
	}
	err = l.RegisterRPCHandler(rpc.C2S_AcceptConnection_MessageID(), l.handleAcceptConnection)
	if err != nil {
		return
	}
	err = l.RegisterRPCHandler(rpc.C2S_RoleLogout_MessageID(), l.handleRoleLogout)
	if err != nil {
		return
	}
	err = l.RegisterRPCHandler(rpc.C2S_GetRolePID_MessageID(), l.handleGetRolePID)
	if err != nil {
		return
	}
	err = l.RegisterRPCHandler(rpc.C2S_GetSpawnedRolePID_MessageID(), l.handleGetSpawnedRolePID)
	if err != nil {
		return
	}
	err = l.RegisterRPCHandler(rpc.C2S_UpdateAccountLoginData_MessageID(), l.handleUpdateAccountLoginData)
	if err != nil {
		return
	}
	err = l.RegisterRPCHandler(rpc.C2S_UpdateAccountBan_MessageID(), l.handleUpdateAccountBan)
	if err != nil {
		return
	}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) handleAccountLogin(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_AccountLogin)
	send := iSend.(*rpc.S2C_AccountLogin)
	send.ID = recv.ID
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
		l.Info("handleAccountLogin, recv=%s, send=%s", recv.String(), send.String())
	}()

	data := l.Data()

	now := time.Now()

	globalConfig := l.GlobalConfig()

	if globalConfig.LoginMinIntervalSec > 0 {
		lastLoginTime := time.Unix(data.LastLoginTime, 0)
		// 检查帐号登录间隔，避免重复加载
		if now.After(lastLoginTime) && now.Sub(lastLoginTime) < globalConfig.LoginMinIntervalSec {
			l.Error("handleAccountLogin account login too often, ip=%s", recv.IP)
			send.EC = rpc.EC(loginMsg.EC_AccountLoginTooOften)
			return
		}
	}

	if recv.ServerID > 0 {
		data.LastLoginServerID = recv.ServerID
	}
	var serverID = data.LastLoginServerID

	data.LastLoginTime = now.Unix()
	data.LastLoginOS = recv.OS
	data.LastLoginChannel = recv.Channel
	data.LastLoginClientVersion = recv.ClientVersion
	data.LastLoginIP = recv.IP

	// 账号登录进行保存，游服角色actor启动时可以获取数据库中最新数据
	// 待优化：正常应该游服通过rpc请求账号服务获取角色账号数据
	l.SaveData(true)

	// 选择一个gateway实例
	gatewayInfo, err := cluster.RoundRobinPickGateway()
	if err != nil {
		l.Error("handleAccountLogin pick gateway fail, ip=%s, %s", recv.IP, err)
		send.EC = rpc.EC(loginMsg.EC_Fail)
		return
	}

	// 生成新token
	send.Token, err = auth.GenerateToken(globalConfig.TokenKey, globalConfig.TokenExpireSec, gatewayInfo.Config.GetID(),
		data.ID, serverID, recv.IsRelogin)
	if err != nil {
		l.Error("handleAccountLogin generate token fail, ip=%s, %s", recv.IP)
		send.EC = rpc.EC(loginMsg.EC_Fail)
		return
	}

	send.GatewayAddr = gatewayInfo.Config.(*cluster.GatewayConfig).Server.WanAddr
}

func (l *Logic) handleAcceptConnection(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_AcceptConnection)
	send := iSend.(*rpc.S2C_AcceptConnection)
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
		l.Info("handleAcceptConnection, recv=%s, send=%s", recv.String(), send.String())
	}()

	sessionPID := recv.SessionPID.ToReal()
	if nactor.IsPIDPtrEmpty(sessionPID) {
		l.Error("handleAcceptConnection fail, recv.SessionPID is empty")
		send.EC = rpc.EC_Fail
		return
	}

	cache := l.Cache()

	roleID := model.AccountIDServerIDToRoleID(recv.AccountID, recv.ServerID)

	rolePID, isOnline, ok := cache.GetRole(roleID)
	if ok {
		send.RoleID = roleID
		send.RolePID = rpc.FromRealPID(rolePID)

		if !isOnline {
			onlineRole, ok := cache.GetOnlineRole()
			if ok {
				// 发送重登录消息给当前账号在线的不同服角色，触发下线操作
				if err := rpc.NotifyRoleRelogin(onlineRole.PID(), nil, 0); err != nil {
					l.Error("handleAcceptConnection rpc.NotifyRoleRelogin fail, online role=%s, %s", onlineRole.String(), err)
					send.EC = rpc.EC_Fail
					return
				}
			}
		}

		// 设置账号下的该角色为上线状态
		cache.SetRoleOnline(roleID)

		// 发送重登录消息给角色，更新session
		if err := rpc.NotifyRoleRelogin(rolePID, recv.SessionPID.ToReal(), recv.SessionID); err != nil {
			l.Error("handleAcceptConnection rpc.NotifyRoleRelogin fail, %s", err)
			send.EC = rpc.EC_Fail
			return
		}

		return
	}

	data := l.Data()

	var err error
	rolePID, err = rpc.SpawnRole(roleID, recv.AccountID, recv.ServerID, sessionPID, recv.SessionID, data.ToInRole(nil))
	if err != nil {
		l.Error("handleAcceptConnection rpc.SpawnRole fail, %s", err)
		send.EC = rpc.EC_Fail
		return
	}

	onlineRole, ok := cache.GetOnlineRole()
	if ok {
		// 发送重登录消息给当前账号在线的不同服角色，触发下线操作
		if err := rpc.NotifyRoleRelogin(onlineRole.PID(), nil, 0); err != nil {
			l.Error("handleAcceptConnection rpc.NotifyRoleRelogin fail, account id=%d, server id=%d, online role=%s, %s",
				recv.AccountID, recv.ServerID, onlineRole.String(), err)
			send.EC = rpc.EC_Fail
			return
		}
	}

	cache.SetRole(roleID, rolePID, true)

	ctx.Watch(rolePID)

	send.RoleID = roleID
	send.RolePID = rpc.FromRealPID(rolePID)
}

func (l *Logic) handleRoleLogout(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_RoleLogout)

	if recv.RoleID <= 0 {
		l.Error("handleRoleLogout fail, recv.RoleID <= 0")
		return
	}

	data := l.Data()

	accountID, _ := model.RoleIDToAccountIDServerID(recv.RoleID)
	if accountID != data.ID {
		l.Error("handleRoleLogout fail, account id not match, data.ID[%d] != [%d]", data.ID, accountID)
		return
	}

	if !l.Cache().SetRoleOffline(recv.RoleID) {
		l.Error("handleRoleLogout fail, role not online in cache, role id=%d", recv.RoleID)
		return
	}

	l.Debug("handleRoleLogout success, role id=%d", recv.RoleID)
}

func (l *Logic) handleGetRolePID(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_GetRolePID)
	send := iSend.(*rpc.S2C_GetRolePID)
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
	}()

	if recv.RoleID <= 0 {
		l.Error("handleGetRolePID fail, recv.RoleID <= 0")
		send.EC = rpc.EC_Fail
		return
	}

	accountID, serverID := model.RoleIDToAccountIDServerID(recv.RoleID)

	data := l.Data()

	if accountID != data.ID {
		l.Error("handleGetRolePID fail, account id not match, data.ID[%d] != [%d]", data.ID, accountID)
		send.EC = rpc.EC_Fail
		return
	}

	send.RoleID = recv.RoleID

	cache := l.Cache()

	rolePID, _, ok := cache.GetRole(recv.RoleID)
	if ok {
		l.Debug("handleGetRolePID success, role pid=%s", rolePID.String())
		send.RolePID = rpc.FromRealPID(rolePID)
		return
	}

	var err error
	rolePID, err = rpc.SpawnRole(recv.RoleID, accountID, serverID, nil, 0, data.ToInRole(nil))
	if err != nil {
		l.Error("handleAcceptConnection rpc.SpawnRole fail, %s", err)
		send.EC = rpc.EC_Fail
		return
	}

	cache.SetRole(recv.RoleID, rolePID, false)

	ctx.Watch(rolePID)

	send.RolePID = rpc.FromRealPID(rolePID)
}

func (l *Logic) handleGetSpawnedRolePID(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_GetSpawnedRolePID)
	send := iSend.(*rpc.S2C_GetSpawnedRolePID)
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
	}()

	if recv.RoleID <= 0 {
		l.Error("handleGetSpawnedRolePID fail, recv.RoleID <= 0")
		send.EC = rpc.EC_Fail
		return
	}

	accountID, _ := model.RoleIDToAccountIDServerID(recv.RoleID)

	data := l.Data()

	if accountID != data.ID {
		l.Error("handleGetSpawnedRolePID fail, account id not match, data.ID[%d] != [%d]", data.ID, accountID)
		send.EC = rpc.EC_Fail
		return
	}

	send.RoleID = recv.RoleID

	cache := l.Cache()

	rolePID, _, ok := cache.GetRole(recv.RoleID)
	if ok {
		send.RolePID = rpc.FromRealPID(rolePID)
	}

	l.Debug("handleGetSpawnedRolePID success, role pid=%s", rolePID.String())
}

func (l *Logic) handleUpdateAccountLoginData(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	//recv := iRecv.(*rpc.C2S_UpdateAccountLoginData)
	//
	//accountData := l.Data()
	//if accountData == nil {
	//	l.Error("handleUpdateAccountLoginData account data is nil")
	//	return
	//}
	//
	//nowUnix := time.Now().Unix()
	//
	//_, serverID := model.RoleIDToAccountIDServerID(recv.RoleID)
	//
	//// 更新账号表中的最后登录服务器id
	//accountData.LastLoginServerID = serverID
	//
	//// 角色首次登陆的服务器ID
	//if accountData.FirstLoginServerID <= 0 {
	//	accountData.FirstLoginServerID = serverID
	//}
	//
	//// 账号首次付费时间
	//if accountData.FirstPayTime <= 0 {
	//	accountData.FirstPayTime = recv.RechargeTime
	//}
	//
	//// 更新账号表中的最后登陆服务器列表
	//lastLoginServerListContainer := accountData.LastLoginServerListContainer()
	//loginInfo := model.Get_AccountRoleLoginInfo()
	//loginInfo.Level = recv.Level
	//loginInfo.Avatar = recv.AvatarID
	//loginInfo.LoginTime = recv.LastLoginTime
	//lastLoginServerListContainer.Set(serverID, loginInfo)
	//lastLoginServerListContainer.RemoveSome(func(serverID int32, info *model.AccountRoleLoginInfo) (continued bool) {
	//	return info.LoginTime <= nowUnix-30*86400
	//})
	//
	//l.Info("handleUpdateAccountLoginData success, recv=%s, %s", recv.String(), accountData.String())
}

func (l *Logic) handleUpdateAccountBan(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_UpdateAccountBan)
	send := iSend.(*rpc.S2C_UpdateAccountBan)
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
	}()

	accountData := l.Data()
	if accountData == nil {
		l.Error("handleUpdateAccountBan account data is nil")
		send.EC = rpc.EC_Fail
		return
	}

	if accountData.Ban == recv.Ban {
		l.Error("handleUpdateAccountBan no change, ban=%d, %s", recv.Ban)
		send.EC = rpc.EC_DataNoChange
		return
	}

	accountData.Ban = recv.Ban

	l.SaveData(true)

	l.Info("handleUpdateAccountBan success, ban=%d", recv.Ban)
}
