package common

import (
	nexport "nggs/export"

	"server/src/entities/account/export"
	"server/src/entities/account/logic"
)

const (
	ID = logic.Common
)

func NewLogic(i export.IAccount) nexport.ILogic {
	l := &Logic{
		Super: logic.NewSuper(i),
	}
	return l
}

func Init() {
	logic.RegisterFactory(ID, NewLogic)
}
