package logic

import (
	"log"

	nexport "nggs/export"

	"server/src/entities/account/export"
)

const (
	Example nexport.LogicID = iota
	Common
)

type Factory struct {
	ID  nexport.LogicID
	New func(i export.IAccount) nexport.ILogic
}

var gFactoryMap = map[nexport.LogicID]*Factory{}

func GetFactory(id nexport.LogicID) (*Factory, bool) {
	if factory, ok := gFactoryMap[id]; ok {
		return factory, true
	}
	return nil, false
}

func RegisterFactory(id nexport.LogicID, New func(i export.IAccount) nexport.ILogic) {
	if _, ok := GetFactory(id); ok {
		log.Panicf("logic factory already exist, id=[%d]", id)
	}

	factory := &Factory{
		ID:  id,
		New: New,
	}

	gFactoryMap[factory.ID] = factory
}

func GenerateLogicMap(i export.IAccount) map[nexport.LogicID]nexport.ILogic {
	logicMap := map[nexport.LogicID]nexport.ILogic{}
	for id, factory := range gFactoryMap {
		logicMap[id] = factory.New(i)
	}
	return logicMap
}
