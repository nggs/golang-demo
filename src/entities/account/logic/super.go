package logic

import (
	"fmt"

	"github.com/asynkron/protoactor-go/actor"

	"server/src/entities/account/export"
)

type Super struct {
	export.IAccount

	ICommon export.ICommon
}

func NewSuper(i export.IAccount) Super {
	return Super{
		IAccount: i,
	}
}

func (Super) ILogic() {

}

func (l *Super) Init() error {
	l.ICommon = l.GetLogic(Common).(export.ICommon)
	if l.ICommon == nil {
		return fmt.Errorf("get Common logic fail")
	}
	return nil
}

func (Super) Run() {

}

func (Super) Finish() {

}

func (Super) OnActorTerminated(who *actor.PID, ctx actor.Context) {

}

func (Super) OnPulse(ctx actor.Context) {

}
