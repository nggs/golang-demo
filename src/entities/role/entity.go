package role

import (
	"errors"
	"fmt"
	"runtime/debug"
	"sync"
	"time"

	"github.com/globalsign/mgo"

	nlog "nggs/log"

	"github.com/asynkron/protoactor-go/actor"
	"github.com/globalsign/mgo/bson"

	nactor "nggs/actor"
	npb "nggs/network/protocol/protobuf/v3"
	nrpc "nggs/rpc"

	"cluster"
	"model"
	gameMsg "msg"
	"rpc"

	"server/src/entities/role/cache"
	roleEvent "server/src/entities/role/event"
	"server/src/entities/role/export"
	"server/src/entities/role/logic"
)

////////////////////////////////////////////////////////////////////////////////
type Entity struct {
	*nactor.Actor

	data    *model.Role // 需要保存到数据库的数据
	account *model.AccountInRole
	zone    *model.ZoneInRole
	cache   *cache.Role

	dbc *model.SimpleClient

	globalCfg cluster.GameGlobalConfig
	cfg       cluster.GameConfig

	sessionPID *actor.PID
	sessionID  uint64
	//disconnectTime time.Time

}

func New(logger nlog.ILogger, sessionPID *actor.PID, sessionID uint64,
	dbc *model.SimpleClient, globalCfg cluster.GameGlobalConfig, cfg cluster.GameConfig,
	account *model.AccountInRole, zone *model.ZoneInRole,
	roleID int64, accountID int64, serverID int32,
	startedWg *sync.WaitGroup, stoppedWg *sync.WaitGroup) export.IRole {
	e := &Entity{
		cache:      cache.New(),
		dbc:        dbc,
		globalCfg:  globalCfg,
		cfg:        cfg,
		sessionPID: sessionPID,
		sessionID:  sessionID,
		account:    account,
		zone:       zone,
	}

	e.cache.RoleID = roleID
	e.cache.AccountID = accountID
	e.cache.ServerID = serverID

	e.Actor = nactor.New(
		nactor.WithLogger(logger),
		nactor.WithStartedWaitGroup(startedWg),
		nactor.WithStoppedWaitGroup(stoppedWg),
		nactor.WithOnStarted(e.onStarted),
		nactor.WithOnStopping(e.onStopping),
		nactor.WithOnStopped(e.onStopped),
		nactor.WithOnReceiveMessage(e.onReceiveMessage),
		nactor.WithOnActorTerminate(e.onActorTerminated),
		nactor.WithLogics(logic.GenerateLogicMap(e)),
		nactor.WithPulse(60*time.Second, e.onPulse),
		nactor.WithMessage(gameMsg.Protocol, func(iProtocol npb.IProtocol, iRecv npb.IMessage, ctx actor.Context) (iSend npb.IMessage, args []interface{}, ok bool, err error) {
			if e.data == nil {
				e.Error("before dispatch message, data is nil")
				return
			}
			if e.cache == nil {
				e.Error("before dispatch message, cache is nil")
				return
			}

			responseMessageID := iRecv.ResponseMessageID()
			if responseMessageID > 0 {
				iSend, err = iProtocol.Produce(responseMessageID)
				if err != nil {
					e.Error("before dispatch message, produce response message fail, id=%d", responseMessageID)
					return
				}
				iSend.SetMessageSeq(iRecv.Head().MessageSeq)
			}

			e.Debug("recv %s, seq=%d", iRecv.String(), iRecv.Head().MessageSeq)

			ok = true

			return
		}, nil),
		nactor.WithRPC(rpc.Protocol, func(sender *actor.PID, iRequestMessage nrpc.IMessage, ctx actor.Context) (args []interface{}, ok bool, err error) {
			if e.data == nil {
				e.Error("before dispatch rpc, data is nil")
				return
			}
			if e.cache == nil {
				e.Error("before dispatch rpc, cache is nil")
				return
			}
			ok = true
			return
		}, nil),
	)

	return e
}

func (Entity) IRole() {}

func (e *Entity) onStarted(ctx actor.Context) {
	// 设置日志标识
	e.SetSign(fmt.Sprintf("role-%d", e.cache.RoleID))
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	dbSession := e.dbc.GetSession()
	defer e.dbc.PutSession(dbSession)
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 加载角色数据
	roleData, err := e.dbc.FindOneRole(dbSession, bson.M{"AccountID": e.cache.AccountID, "ServerID": e.cache.ServerID})
	if err != nil {
		if !errors.Is(err, mgo.ErrNotFound) {
			e.Error("onStarted FindOneRole fail, accountID=[%d], serverID=[%d], %s", e.cache.AccountID, e.cache.ServerID, err)
			_ = e.Stop()
			return
		}
		// 未找到角色数据，则创角
		roleData, err = e.dbc.CreateRole(e.cache.AccountID, e.cache.ServerID)
		if err != nil {
			e.Error("onStarted create role fail, accountID=[%d], serverID=[%d], %s", e.cache.AccountID, e.cache.ServerID, err)
			_ = e.Stop()
			return
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		e.Debug("onStarted create %s success", roleData.String())
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 设置角色数据
		e.data = roleData
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 触发创角事件
		_ = e.DispatchEvent(&roleEvent.OnCreate{}, ctx)
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 触发加载数据后事件
		_ = e.DispatchEvent(&roleEvent.AfterLoadData{}, ctx)
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 角色数据入库
		e.SaveData(true)
	} else {
		// 设置角色数据
		e.data = roleData
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		e.Info("handleOnStarted load role data success")
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 触发加载数据后事件
		_ = e.DispatchEvent(&roleEvent.AfterLoadData{}, ctx)
	}
}

func (e *Entity) onPulse(ctx actor.Context) {
	// 每次心跳都检查是否脏数据，如果是脏数据则保存
	e.SaveData(false)

	if !e.IsOnline() && e.globalCfg.IdleToStopSec > 0 {
		now := time.Now()
		if now.Sub(e.cache.LastDataSaveTime) > e.globalCfg.IdleToStopSec {
			e.Info("idle too long time, now=%v, last data save time=%v", now, e.cache.LastDataSaveTime)
			_ = e.Stop()
			return
		}
	}
}

func (e *Entity) onStopping(ctx actor.Context) {
	// 保存数据
	e.SaveData(true)

	if e.IsOnline() {
		// 告知gateway上的session断开连接
		nactor.RootContext().Send(e.sessionPID, rpc.Get_C2S_Disconnect())
	}
}

func (e *Entity) onStopped(ctx actor.Context) {
	//if e.data != nil {
	//	// 回收对象
	//	//model.Put_Role(e.data)
	//	e.data = nil
	//}
}

func (e *Entity) onReceiveMessage(ctx actor.Context) {
	defer func() {
		if r := recover(); r != nil {
			e.Error("%v\n%s", r, debug.Stack())
			panic(r)
		}
	}()

	sender := ctx.Sender()
	switch msg := ctx.Message().(type) {
	case *rpc.C2S_Disconnect:
		if e.data == nil || e.cache == nil {
			e.Error("recv *rpc.C2S_Disconnect, but data or cache is nil")
			return
		}

		e.Info("session disconnect, pid=%v", e.sessionPID)

		//if !e.IsOnline() {
		//	return
		//}

		// 更新最后下线时刻
		e.data.LastLogoutTime = time.Now().Unix()
		e.cache.Dirty = true

		// 更新session信息
		e.UpdateSession(nil, 0)

		_ = e.DispatchEvent(&roleEvent.OnDisconnect{}, ctx)

		// 通知account，角色下线
		if err := rpc.NotifyAccountRoleLogout(e.cache.AccountID, e.data.ID); err != nil {
			e.Error("notify account logout fail, %s", err)
		} else {
			e.Info("notify account logout success")
		}

	case string:
		switch msg {
		case "id":
			if sender != nil {
				if e.data != nil {
					ctx.Respond(e.data.ID)
				} else {
					ctx.Respond(0)
				}
			}

		case "stop":
			_ = e.Stop()
			if sender != nil {
				ctx.Respond("stop")
			}

		default:
			e.Error("unknown command %s", msg)
		}

	default:
		e.Error("recv unsupported msg [%#v] from [%v]", msg, sender)
	}
}

func (e *Entity) onActorTerminated(who *actor.PID, ctx actor.Context) {
	e.Debug("[%+v] terminated", who)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (e Entity) DBC() *model.SimpleClient {
	return e.dbc
}

func (e Entity) Data() *model.Role {
	return e.data
}

func (e *Entity) SaveData(force bool) {
	if e.data == nil || e.cache == nil {
		return
	}

	if !force {
		if !e.cache.Dirty {
			return
		}
	}

	dbSession := e.dbc.GetSession()
	defer e.dbc.PutSession(dbSession)

	_, err := e.dbc.UpsertRole(dbSession, bson.M{"_id": e.data.ID}, e.data)
	if err != nil {
		e.Error("save data fail, %s", err)
	} else {
		e.Debug("save data success")
	}

	e.cache.Dirty = false
	e.cache.LastDataSaveTime = time.Now()
}

func (e *Entity) AccountData() *model.AccountInRole {
	return e.account
}

func (e *Entity) ZoneData() *model.ZoneInRole {
	return e.zone
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (e *Entity) Cache() *cache.Role {
	return e.cache
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (e Entity) GlobalConfig() cluster.GameGlobalConfig {
	return e.globalCfg
}

func (e Entity) Config() cluster.GameConfig {
	return e.cfg
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (e *Entity) SessionPID() *actor.PID {
	return e.sessionPID.Clone()
}

func (e *Entity) UpdateSession(sessionPID *actor.PID, sessionID uint64) {
	e.sessionPID = sessionPID
	e.sessionID = sessionID
}

func (e *Entity) IsOnline() bool {
	return !nactor.IsPIDPtrEmpty(e.sessionPID)
}

func (e *Entity) SendMessage(iMsg npb.IMessage) (err error) {
	if !e.IsOnline() {
		err = fmt.Errorf("session pid is empty")
		e.Error("send message fail, %s", err)
		return
	}

	nactor.RootContext().Send(e.sessionPID, iMsg)

	e.Debug("send %s success, seq=%d, size=%d", iMsg.String(), iMsg.Head().MessageSeq, iMsg.Size())

	return
}
