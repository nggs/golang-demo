package cache

import "time"

// 角色缓存数据
type Role struct {
	Dirty            bool
	LastDataSaveTime time.Time

	RoleID    int64
	AccountID int64
	ServerID  int32
}

func New() *Role {
	c := &Role{
		LastDataSaveTime: time.Now(),
	}
	return c
}
