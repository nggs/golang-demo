package logic

import (
	"fmt"

	"github.com/asynkron/protoactor-go/actor"

	"server/src/entities/role/export"
)

type Super struct {
	export.IRole

	ICommon export.ICommon
	IBattle export.IBattle
}

func NewSuper(i export.IRole) Super {
	return Super{
		IRole: i,
	}
}

func (Super) ILogic() {

}

func (l *Super) Init() error {
	l.ICommon = l.GetLogic(Common).(export.ICommon)
	if l.ICommon == nil {
		return fmt.Errorf("get Common logic fail")
	}
	l.IBattle = l.GetLogic(Battle).(export.IBattle)
	if l.IBattle == nil {
		return fmt.Errorf("get Battle logic fail")
	}
	return nil
}

func (Super) Run() {

}

func (Super) Finish() {

}

func (Super) OnActorTerminated(who *actor.PID, ctx actor.Context) {

}

func (Super) OnPulse(ctx actor.Context) {

}
