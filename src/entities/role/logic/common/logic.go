package common

import (
	nexport "nggs/export"

	"server/src/entities/role/logic"
)

type Logic struct {
	logic.Super
}

func (Logic) ID() nexport.LogicID {
	return ID
}

func (Logic) ICommon() {

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) Init() (err error) {
	err = l.Super.Init()
	if err != nil {
		return
	}
	err = l.regAllRPCHandler()
	if err != nil {
		return
	}
	err = l.regAllMsgHandler()
	if err != nil {
		return
	}
	err = l.regAllEventHandler()
	if err != nil {
		return
	}
	return nil
}
