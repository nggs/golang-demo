package common

import (
	"time"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	npb "nggs/network/protocol/protobuf/v3"
	nutil "nggs/util"

	gameMsg "msg"
	roleEvent "server/src/entities/role/event"
)

func (l *Logic) regAllMsgHandler() (err error) {
	err = l.RegisterMessageHandler(gameMsg.C2S_Login_MessageID(), l.handleLogin)
	if err != nil {
		return
	}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) handleLogin(iRecv npb.IMessage, iSend npb.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*gameMsg.C2S_Login_EX)
	send := iSend.(*gameMsg.S2C_Login_EX)
	defer func() {
		if err := l.SendMessage(send); err == nil {
			if send.EC == gameMsg.EC_OK {
				// 触发下发角色数据后事件
				l.PostEvent(&roleEvent.AfterSendRole{})
			} else {
				l.NewTimer(5*time.Second, 0, func(id nactor.TimerID, tag nactor.TimerID) {
					l.Error("login fail, role stop")
					_ = l.Stop()
				})
			}
		}
	}()
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	cache := l.Cache()
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	accountData := l.AccountData()
	if accountData == nil {
		l.Error("handleLogin account data is nil, accountID=[%d]", cache.AccountID)
		send.EC = gameMsg.EC_Fail
		return
	}
	if accountData.Ban != 0 {
		l.Info("handleLogin account has be ban, accountID=[%d]", cache.AccountID)
		send.EC = gameMsg.EC_AccountBan
		return
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	roleData := l.Data()
	if roleData == nil {
		l.Error("handleLogin role data is nil, accountID=[%d], serverID=[%d]", cache.AccountID, cache.ServerID)
		send.EC = gameMsg.EC_Fail
		_ = l.Stop()
		return
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	oldLoginTime := roleData.LastLoginTime
	isNewRole := oldLoginTime == 0
	if isNewRole {
		//新角色默认非重连
		recv.IsRelogin = false
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	nowUnix := time.Now().Unix()
	// 正常登陆修改登录时间
	if !recv.IsRelogin {
		// 更新角色表中的最后登录时刻
		roleData.LastLoginTime = nowUnix
		cache.Dirty = true
		// 通知login更新账号登陆信息
		//_ = rpc.UpdateAccountLoginData(roleData.ID, roleData.LastLoginTime, roleData.Level, roleData.AvatarID, accountData.FirstPayTime)
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 当天首次登陆
	isTodayFirstLogin := false
	if !recv.IsRelogin && !nutil.IsSameDayUnixTimeStamp(oldLoginTime, nowUnix) {
		isTodayFirstLogin = true
	}
	// 触发登录事件
	_ = l.DispatchEvent(&roleEvent.OnLogin{
		Role:              send.Role,
		IsRelogin:         recv.IsRelogin,
		IsTodayFirstLogin: isTodayFirstLogin,
	}, ctx)
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 转换要下发的角色数据
	send.Role = roleData.ToMsg(send.Role)
}
