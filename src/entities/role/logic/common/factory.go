package common

import (
	nexport "nggs/export"

	"server/src/entities/role/export"
	"server/src/entities/role/logic"
)

const (
	ID = logic.Common
)

func NewLogic(i export.IRole) nexport.ILogic {
	l := &Logic{
		Super: logic.NewSuper(i),
	}
	return l
}

func Init() {
	logic.RegisterFactory(ID, NewLogic)
}
