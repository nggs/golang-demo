package common

import (
	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	nrpc "nggs/rpc"

	gameMsg "msg"
	"rpc"

	roleEvent "server/src/entities/role/event"
)

func (l *Logic) regAllRPCHandler() (err error) {
	err = l.RegisterRPCHandler(rpc.C2S_Relogin_MessageID(), l.handleRelogin)
	if err != nil {
		return
	}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) handleRelogin(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_Relogin)
	send := iSend.(*rpc.S2C_Relogin)
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
	}()

	newSessionPID := recv.SessionPID.ToReal()

	if !l.IsOnline() {
		// 重登
		l.Info("handleRelogin current session is empty")
	} else {
		// 顶号

		// 通知旧session发生顶号，必须断开
		//sessionPID := l.SessionPID()
		nactor.RootContext().Send(l.SessionPID(), gameMsg.Get_S2C_ReloginNeedOffline())
		//ctx.Unwatch(sessionPID)
		//l.Info("handleRelogin unwatch session pid %v", sessionPID)

		if nactor.IsPIDPtrEmpty(newSessionPID) {
			// 没有新连接，则触发断线事件
			l.PostEvent(&roleEvent.OnDisconnect{})
		}
	}

	// 更新session
	l.UpdateSession(newSessionPID, recv.SessionID)

	l.Info("handleRelogin new session is %s", newSessionPID)

	//if !nactor.IsPIDPtrEmpty(newSessionPID) {
	//	ctx.Watch(recv.SessionPID)
	//	l.Info("handleRelogin watch session pid %v", recv.SessionPID)
	//}
}
