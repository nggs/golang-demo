package role

import (
	"server/src/entities/role/logic/battle"
	"server/src/entities/role/logic/common"
)

func Init() {
	common.Init()
	battle.Init()
}
