package event

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//支付成功
type OnPaymentSuccess struct {
	Amount                            int32  //本次支付多少钱
	CurrentRechargeTotal              int64  //当前已累计充值了多少钱
	ActivityTID                       int32  //activity表ID
	ChargeID                          int32  //本次支付项ID
	LastRechargeTime                  int64  //最近一次充值成功时间
	IsFromAdmin                       bool   //是否后台模拟充值
	CustomizeGiftPackExpiredAt        int64  // 限时礼包过期时间
	CustomizeGiftPackPurchaseLevelTID int32  // 限时礼包购买力ID
	ActivityTimeID                    int32  // activityTime表ID
	OrderNO                           string // 订单id
}

func (OnPaymentSuccess) EventID() uint16 {
	return OnPaymentSuccessID
}

func (e OnPaymentSuccess) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//领取首充奖励
type OnGetFirstChargeReward struct {
	DayNum   int32 //第几天
	RewardID int32 //掉落ID
}

func (OnGetFirstChargeReward) EventID() uint16 {
	return OnGetFirstChargeRewardID
}

func (e OnGetFirstChargeReward) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//领取首次累充奖励
type OnGetFirstCumulativeChargeReward struct {
	DayNum   int32 //第几天
	RewardID int32 //掉落ID
}

func (OnGetFirstCumulativeChargeReward) EventID() uint16 {
	return OnGetFirstCumulativeChargeRewardID
}

func (e OnGetFirstCumulativeChargeReward) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//vip福利限购(元宝兑换礼包)
type OnGoldExchangeGiftPack struct {
	ActivityTID int32
}

func (OnGoldExchangeGiftPack) EventID() uint16 {
	return OnGoldExchangeGiftPackID
}

func (e OnGoldExchangeGiftPack) Event() {
}
