package event

// 世界boss挑战
type OnWorldBossBattle struct {
	RankID int32
}

func (OnWorldBossBattle) EventID() uint16 {
	return OnWorldBossBattleID
}

func (e OnWorldBossBattle) Event() {
}
