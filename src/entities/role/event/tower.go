package event

import (
	"sd"
)

// 通关试练塔
type OnPassTrialTower struct {
	SD sd.TrialTower
}

func (OnPassTrialTower) EventID() uint16 {
	return OnPassTrialTowerID
}

func (e OnPassTrialTower) Event() {
}

//挑战试练塔关卡
type OnChallengeTrialTower struct {
	SD sd.TrialTower
}

func (OnChallengeTrialTower) EventID() uint16 {
	return OnChallengeTrialTowerID
}

func (e OnChallengeTrialTower) Event() {

}
