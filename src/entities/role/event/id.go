package event

const (
	OnStartedID uint16 = iota + 1
	OnLoginID
	OnCreateID
	OnActorTerminatedID
	AfterLoadDataID
	BeforeSaveDataID
	AfterSendRoleID
	OnGainCurrencyID
	OnLostCurrencyID
	OnGainItemID
	OnLostItemID
	OnGainEquipID
	OnLostEquipID
	OnUpdateEquipID
	OnGainHeroID
	OnLostHeroID
	OnHeroLevelUpID
	OnHeroAdvanceID //在演武台进阶英雄
	OnEquipStrengthenID
	OnHeroUpdateEquipID
	OnCompleteStageID
	OnChallengeStageBossID             //挑战关卡BOSS
	OnGetIdleRewardID                  //领取挂机收益
	OnSendFriendPointID                //赠送好友点
	OnFastIdleID                       //快速挂机
	OnTavernCallHeroID                 //酒馆召唤英雄
	OnChallengeTrialTowerID            //挑战试练塔
	OnJoinArenaID                      //参加竞技场
	OnAcceptRewardTaskID               //接受个人或团队悬赏任务
	OnWinDaoistMagicID                 //奇门遁甲通关
	OnBuyProductAtShopID               //商店购买商品
	OnWinNormalArenaID                 //取得普通竞技场胜利
	OnRoleLevelUpID                    //角色升级
	OnPassTrialTowerID                 //通关试练塔
	OnArenaPointsChangeID              //竞技场积分变更事件
	OnHeroMasterYardOpenSlotID         //御将台开启槽位
	OnJoinGuildID                      //加入公会
	OnDecomposeHeroID                  //分解普通武将
	OnJoinGuildBossID                  //参加公会BOSS
	OnHuntGuildBossID                  // 打过一场公会BOSS战
	OnDailyQuestActiveID               //日常任务活跃点数
	OnWinDaoistMagicStageID            //击败奇门遁甲敌人
	OnGetFirstBountyTaskID             //首次领取悬赏奖励
	AfterGetZoneDataID                 // 加载zone数据之后
	OneMinutePassedID                  // 上线每分钟事件
	OneHourPassedID                    // 上线每小时事件
	OnDisconnectID                     // 断线事件
	OnHeroDebrisMergeID                // 武将碎片合成武将
	OnJoinDaoistMagicID                // 参与奇门遁甲战斗
	OnJoinHeroRescueID                 //参与英雄救美
	OnJoinDungeonsID                   //参与材料副本
	OnDayRefreshID                     // 每日刷新
	OnDayZeroClockID                   // 每日零点
	OnRefreshDaoistMagicID             //手动重置奇门遁甲
	OnVipLevelUpID                     //vip升级
	OnMonthCardBuyID                   //购买月卡/终身卡事件
	OnPaymentSuccessID                 //人民币支付成功
	OnRoleRenameSuccessID              //角色取名成功
	OnActivateArtifactID               //激活神器
	OnAddFriendID                      //成功添加好友
	OnGetFirstChargeRewardID           //领取首充奖励(1-3天)
	OnGetFirstCumulativeChargeRewardID //领取首次累充奖励(1-3天)
	OnGetInvestmentRewardID            //领取[投资|高级投资]计划奖励
	OnGetLuckyBoxRewardID              //幸运宝箱抽取到保底奖励
	OnGetVipRewardID                   //领取VIP贵族奖励
	OnEquipAdvanceID                   //装备升阶
	OnMonthFundID                      //购买月基金
	OnGainWarcraftID                   //获得兵书
	OnLostWarcraftID                   //失去兵书
	OnUpdateWarcraftID                 //更新兵书
	OnHeroUpdateWarcraftID             //英雄更新兵书
	OnExitGuildID                      //离开公会
	OnTreasureHuntID                   //参与寻宝
	OnGoldExchangeGiftPackID           //vip福利限购(元宝兑换礼包)
	OnExclusiveUnlockID                //专属解锁
	OnExclusiveLevelUpID               //专属升级
	OnSeniorHeroMasterLevelUpID        //高级御将台升级
	OnSubscribeID                      //根据类型id订阅微信推送
	OnSubscribeByTypeID                //根据类型订阅微信推送
	OnSendChatID                       //发送聊天事件
	OnEquipDismissID                   //装备分解
	OnWorldBossBattleID                //世界boss挑战成功
	OnHeroChallengeSingleBattleID      //过关斩将-单人挑战
	OnHeroChallengeTeamBattleID        //过关斩将-团队挑战
	OnWinSeniorArenaID                 //高阶竞技场挑战成功
	OnWarcraftSearchID                 //兵书搜索
	OnFrontChallengeStageID            //挑战关卡前
	OnWarcraftStrengthenID             //兵书强化
	OnJoinHeroChallengeID              //参与过关斩将
)

var noNeedMarkDirtyIDs = map[uint16]struct{}{
	OneMinutePassedID: {},
	OneHourPassedID:   {},
}

func NeedMarkDirty(id uint16) bool {
	if _, ok := noNeedMarkDirtyIDs[id]; ok {
		return false
	}
	return true
}
