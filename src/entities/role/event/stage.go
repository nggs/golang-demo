//+build ignore

package event

import (
	"sd"

	"model"
)

// 通关
type OnCompleteStage struct {
	StageSD            sd.Stage
	PassTime           int64
	LeftHeroInBattle   *model.HeroInBattleMap
	RightHeroInBattle  *model.HeroInBattleMap
	LeftBattleEndInfo  *model.BattleEndInfoMap
	RightBattleEndInfo *model.BattleEndInfoMap
}

func (OnCompleteStage) EventID() uint16 {
	return OnCompleteStageID
}

func (e OnCompleteStage) Event() {
}

//挑战关卡boss
type OnChallengeStageBoss struct {
	StageSD sd.Stage
	Passed  bool
}

func (OnChallengeStageBoss) EventID() uint16 {
	return OnChallengeStageBossID
}

func (e OnChallengeStageBoss) Event() {

}

//挑战关卡前
type OnFrontChallengeStage struct {
	StageSD sd.Stage
}

func (OnFrontChallengeStage) EventID() uint16 {
	return OnFrontChallengeStageID
}

func (e OnFrontChallengeStage) Event() {

}

//////////////// 领取挂机收益 /////////////////////////

type OnGetIdleReward struct {
	IdleSeconds int64
	AddGoldNum  int32
}

func (OnGetIdleReward) EventID() uint16 {
	return OnGetIdleRewardID
}

func (e OnGetIdleReward) Event() {

}

////////////// 快速挂机 //////////////////////////////

type OnFastIdle struct {
	IdleSeconds int64
	AddGoldNum  int32
}

func (OnFastIdle) EventID() uint16 {
	return OnFastIdleID
}

func (e OnFastIdle) Event() {

}
