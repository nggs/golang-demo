//+build ignore

package event

import (
	"sd"

	"model"
)

// 获得兵书
type OnGainWarcraft struct {
	SD              sd.Warcraft
	Num             int
	Notify          bool
	NewWarcrafts    []*model.Warcraft
	UpdateWarcrafts []*model.Warcraft
	From            int
}

func (OnGainWarcraft) EventID() uint16 {
	return OnGainWarcraftID
}

func (e OnGainWarcraft) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 失去兵书
type OnLostWarcraft struct {
	SD              sd.Warcraft
	Num             int
	Notify          bool
	LostWarcrafts   []*model.Warcraft
	UpdateWarcrafts []*model.Warcraft
	From            int
}

func (OnLostWarcraft) EventID() uint16 {
	return OnLostWarcraftID
}

func (e OnLostWarcraft) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 更新兵书
type OnUpdateWarcraft struct {
	SD sd.Warcraft
	//Num          int
	Notify         bool
	UpdateWarcraft *model.Warcraft
}

func (OnUpdateWarcraft) EventID() uint16 {
	return OnUpdateWarcraftID
}

func (e OnUpdateWarcraft) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 兵书搜索
type OnWarcraftSearch struct {
	Times int32
}

func (OnWarcraftSearch) EventID() uint16 {
	return OnWarcraftSearchID
}

func (e OnWarcraftSearch) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 兵书强化
type OnWarcraftStrengthen struct {
	Hero       *model.Hero
	HeroSD     sd.HeroBasic
	Pos        int
	Warcraft   *model.Warcraft
	WarcraftSD sd.Warcraft
	OldLevel   int
}

func (OnWarcraftStrengthen) EventID() uint16 {
	return OnWarcraftStrengthenID
}

func (e OnWarcraftStrengthen) Event() {
}
