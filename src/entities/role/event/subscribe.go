//+build ignore

package event

import (
	"sd"
)

// 根据类型id订阅微信推送
type OnSubscribe struct {
	SubscribeSD           sd.Subscribe
	Result                sd.E_WeChatSubscribeResult
	AccountFirstSubscribe bool // 是否是账号下第一个订阅的角色
	RoleFirstSubscribe    bool // 是否角色首次订阅
}

func (OnSubscribe) EventID() uint16 {
	return OnSubscribeID
}

func (e OnSubscribe) Event() {
}

// 根据类型订阅微信推送
type OnSubscribeByType struct {
	Type                     sd.E_WeChatSubscribeFunction
	Result                   sd.E_WeChatSubscribeResult
	AccountFirstSubscribeSDs map[int]sd.Subscribe // 账号下第一个订阅的静态表数据
	RoleFirstSubscribeSDs    map[int]sd.Subscribe // 角色首次订阅的静态表数据
}

func (OnSubscribeByType) EventID() uint16 {
	return OnSubscribeByTypeID
}

func (e OnSubscribeByType) Event() {
}
