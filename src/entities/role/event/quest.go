package event

import "sd"

//获取日常任务活跃点数
type OnDailyQuestActive struct {
	ActiveNum int
}

func (OnDailyQuestActive) EventID() uint16 {
	return OnDailyQuestActiveID
}

func (e OnDailyQuestActive) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//首次领取悬赏奖励
type OnGetFirstBountyTask struct {
	InitTaskId int
}

func (OnGetFirstBountyTask) EventID() uint16 {
	return OnGetFirstBountyTaskID
}

func (e OnGetFirstBountyTask) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//接受个人或团队悬赏任务
type OnAcceptRewardTask struct {
	//悬赏类型
	Type       sd.E_NoticeBoardType
	FriendHero map[int64]string
	ExpireTime int64
}

func (OnAcceptRewardTask) EventID() uint16 {
	return OnAcceptRewardTaskID
}

func (e OnAcceptRewardTask) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 参与英雄救美
type OnJoinHeroRescue struct {
	RescueNum int
}

func (OnJoinHeroRescue) EventID() uint16 {
	return OnJoinHeroRescueID
}

func (e OnJoinHeroRescue) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//参与资源副本
type OnJoinDungeons struct {
	ID       int32 //资源副本ID
	IsSweeps bool  //是否为扫荡模式
}

func (OnJoinDungeons) EventID() uint16 {
	return OnJoinDungeonsID
}

func (e OnJoinDungeons) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//激活神器
type OnActivateArtifact struct {
	ID int32 //神器ID
}

func (OnActivateArtifact) EventID() uint16 {
	return OnActivateArtifactID
}

func (e OnActivateArtifact) Event() {
}
