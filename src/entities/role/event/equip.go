//+build ignore

package event

import (
	"sd"

	"model"
)

// 获得装备
type OnGainEquip struct {
	SD           sd.Equipment
	Num          int
	Notify       bool
	NewEquips    []*model.Equip
	UpdateEquips []*model.Equip
	From         int
}

func (OnGainEquip) EventID() uint16 {
	return OnGainEquipID
}

func (e OnGainEquip) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 失去装备
type OnLostEquip struct {
	SD           sd.Equipment
	Num          int
	Notify       bool
	LostEquips   []*model.Equip
	UpdateEquips []*model.Equip
	From         int
}

func (OnLostEquip) EventID() uint16 {
	return OnLostEquipID
}

func (e OnLostEquip) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 更新装备
type OnUpdateEquip struct {
	SD sd.Equipment
	//Num          int
	Notify      bool
	UpdateEquip *model.Equip
}

func (OnUpdateEquip) EventID() uint16 {
	return OnUpdateEquipID
}

func (e OnUpdateEquip) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 装备强化
type OnEquipStrengthen struct {
	Hero     *model.Hero
	HeroSD   sd.HeroBasic
	Pos      sd.E_EquipmentPos
	Equip    *model.Equip
	EquipSD  sd.Equipment
	OldLevel int
}

func (OnEquipStrengthen) EventID() uint16 {
	return OnEquipStrengthenID
}

func (e OnEquipStrengthen) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 装备升阶
type OnEquipAdvance struct {
	OldEquipSD sd.Equipment
	EquipSD    sd.Equipment
}

func (OnEquipAdvance) EventID() uint16 {
	return OnEquipAdvanceID
}

func (e OnEquipAdvance) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 装备分解
type OnEquipDismiss struct {
	DismissNum int
}

func (OnEquipDismiss) EventID() uint16 {
	return OnEquipDismissID
}

func (e OnEquipDismiss) Event() {
}
