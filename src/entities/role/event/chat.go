package event

// 发送聊天事件
type OnSendChat struct {
	ChannelID string
}

func (OnSendChat) EventID() uint16 {
	return OnSendChatID
}

func (e OnSendChat) Event() {
}
