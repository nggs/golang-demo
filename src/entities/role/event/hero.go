//+build ignore

package event

import (
	"sd"

	"model"
)

// 获得英雄
type OnGainHero struct {
	SD        sd.HeroBasic
	Notify    bool
	NewHeroes []*model.Hero
	Channel   sd.E_HeroGetChannel //获取渠道
}

func (OnGainHero) EventID() uint16 {
	return OnGainHeroID
}

func (e OnGainHero) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 失去英雄
type OnLostHero struct {
	SD         sd.HeroBasic
	Notify     bool
	LostHeroes []*model.Hero
}

func (OnLostHero) EventID() uint16 {
	return OnLostHeroID
}

func (e OnLostHero) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 英雄升级
type OnHeroLevelUp struct {
	Hero     *model.Hero
	OldLevel int
}

func (OnHeroLevelUp) EventID() uint16 {
	return OnHeroLevelUpID
}

func (e OnHeroLevelUp) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 英雄升阶
type OnHeroAdvance struct {
	Hero      *model.Hero
	HeroSD    sd.HeroBasic
	OldHeroSD sd.HeroBasic
}

func (OnHeroAdvance) EventID() uint16 {
	return OnHeroAdvanceID
}

func (e OnHeroAdvance) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 英雄更新装备
type OnHeroUpdateEquip struct {
	Hero            *model.Hero
	HeroSD          sd.HeroBasic
	InstallEquips   map[sd.E_EquipmentPos]*model.Equip
	UninstallEquips map[sd.E_EquipmentPos]*model.Equip
}

func (OnHeroUpdateEquip) EventID() uint16 {
	return OnHeroUpdateEquipID
}

func (e OnHeroUpdateEquip) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 英雄更新兵书
type OnHeroUpdateWarcraft struct {
	Hero               *model.Hero
	HeroSD             sd.HeroBasic
	InstallWarcrafts   map[int]*model.Warcraft
	UninstallWarcrafts map[int]*model.Warcraft
}

func (OnHeroUpdateWarcraft) EventID() uint16 {
	return OnHeroUpdateWarcraftID
}

func (e OnHeroUpdateWarcraft) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 酒馆召唤英雄

type OnTavernCallHero struct {
	TavernSD sd.Tavern
	HeroTIDs []int32
	Times    int
}

func (OnTavernCallHero) EventID() uint16 {
	return OnTavernCallHeroID
}

func (e OnTavernCallHero) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 御将台开启槽位

type OnHeroMasterYardOpenSlot struct {
	CurrentSlots int32
}

func (OnHeroMasterYardOpenSlot) EventID() uint16 {
	return OnHeroMasterYardOpenSlotID
}

func (e OnHeroMasterYardOpenSlot) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 分解英雄
type OnDecomposeHero struct {
	LostNum int
}

func (OnDecomposeHero) EventID() uint16 {
	return OnDecomposeHeroID
}

func (e OnDecomposeHero) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 武将碎片合成武将
type OnHeroDebrisMerge struct {
}

func (OnHeroDebrisMerge) EventID() uint16 {
	return OnHeroDebrisMergeID
}

func (e OnHeroDebrisMerge) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 参与寻宝
type OnTreasureHunt struct {
	TavernSD sd.Tavern
	Times    int
}

func (OnTreasureHunt) EventID() uint16 {
	return OnTreasureHuntID
}

func (e OnTreasureHunt) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 解锁专属
type OnExclusiveUnlock struct {
	HeroSD sd.HeroBasic
}

func (OnExclusiveUnlock) EventID() uint16 {
	return OnExclusiveUnlockID
}

func (e OnExclusiveUnlock) Event() {
}

// 专属升级
type OnExclusiveLevelUp struct {
	HeroSD sd.HeroBasic
	Level  int
}

func (OnExclusiveLevelUp) EventID() uint16 {
	return OnExclusiveLevelUpID
}

func (e OnExclusiveLevelUp) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 高级御将台升级
type OnSeniorHeroMasterLevelUp struct {
	OldLevel int32
	NewLevel int32
}

func (OnSeniorHeroMasterLevelUp) EventID() uint16 {
	return OnSeniorHeroMasterLevelUpID
}

func (e OnSeniorHeroMasterLevelUp) Event() {
}
