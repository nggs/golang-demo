//+build ignore

package event

import (
	"sd"

	"model"
)

// 获得物品
type OnGainItem struct {
	SD     sd.Item
	Num    int
	Item   *model.Item
	Notify bool
}

func (OnGainItem) EventID() uint16 {
	return OnGainItemID
}

func (e OnGainItem) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 失去物品
type OnLostItem struct {
	SD     sd.Item
	Num    int
	Item   *model.Item
	Notify bool
}

func (OnLostItem) EventID() uint16 {
	return OnLostItemID
}

func (e OnLostItem) Event() {
}
