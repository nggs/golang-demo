package event

//竞技场积分变更
type OnArenaPointsChange struct {
	// 最新积分
	Point int32
	// 变更积分
	ChangePoint int32
}

func (OnArenaPointsChange) EventID() uint16 {
	return OnArenaPointsChangeID
}

func (e OnArenaPointsChange) Event() {
}

//参加竞技场
type OnJoinArena struct {
	OffenceFormation map[int32]string //战斗阵容数据
	ArenaType        int
}

func (OnJoinArena) EventID() uint16 {
	return OnJoinArenaID
}

func (e OnJoinArena) Event() {
}

//取得普通竞技场胜利
type OnWinNormalArena struct {
}

func (OnWinNormalArena) EventID() uint16 {
	return OnWinNormalArenaID
}

func (e OnWinNormalArena) Event() {
}

//取得高阶竞技场竞技场胜利
type OnWinSeniorArena struct {
	RankID int32
}

func (OnWinSeniorArena) EventID() uint16 {
	return OnWinSeniorArenaID
}

func (e OnWinSeniorArena) Event() {
}
