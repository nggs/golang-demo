package event

//领取幸运宝箱奖励
type OnGetLuckyBoxReward struct {
	RewardGroupID int32 //奖励掉落组ID
	ActivityID    int32 //活动表主键ID
}

func (OnGetLuckyBoxReward) EventID() uint16 {
	return OnGetLuckyBoxRewardID
}

func (e OnGetLuckyBoxReward) Event() {
}

// 购买月基金事件
type OnMonthFund struct {
	ChargeConfigID int32
}

func (OnMonthFund) EventID() uint16 {
	return OnMonthFundID
}

func (e OnMonthFund) Event() {
}
