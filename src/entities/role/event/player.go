package event

//角色取名成功事件
type OnRoleRenameSuccess struct {
	// 角色取的新昵称
	NewName string
}

func (OnRoleRenameSuccess) EventID() uint16 {
	return OnRoleRenameSuccessID
}

func (e OnRoleRenameSuccess) Event() {
}
