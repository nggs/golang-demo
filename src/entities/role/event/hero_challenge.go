//+build ignore

package event

import "sd"

// 参与过关斩将
type OnJoinHeroChallenge struct {
}

func (OnJoinHeroChallenge) EventID() uint16 {
	return OnJoinHeroChallengeID
}

func (e OnJoinHeroChallenge) Event() {
}

// 过关斩将-单人挑战
type OnHeroChallengeSingleBattle struct {
	HeroChallengeSD sd.HeroChallenge
}

func (OnHeroChallengeSingleBattle) EventID() uint16 {
	return OnHeroChallengeSingleBattleID
}

func (e OnHeroChallengeSingleBattle) Event() {
}

// 过关斩将-团队挑战
type OnHeroChallengeTeamBattle struct {
	HeroChallengeSD sd.HeroChallenge
}

func (OnHeroChallengeTeamBattle) EventID() uint16 {
	return OnHeroChallengeTeamBattleID
}

func (e OnHeroChallengeTeamBattle) Event() {
}
