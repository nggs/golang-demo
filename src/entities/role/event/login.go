package event

import (
	Msg "msg"
)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 登录成功
type OnLogin struct {
	Role              *Msg.Role
	IsRelogin         bool
	IsTodayFirstLogin bool
}

func (OnLogin) EventID() uint16 {
	return OnLoginID
}

func (e OnLogin) Event() {
}
