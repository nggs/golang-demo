package event

// 加载zone数据之后
type AfterGetZoneData struct {
}

func (AfterGetZoneData) EventID() uint16 {
	return AfterGetZoneDataID
}

func (e AfterGetZoneData) Event() {
}
