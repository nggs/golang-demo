package event

import "sd"

//商店购买(普通商店、公会商店、迷宫商店、遣散商店、高阶竞技场商店)
type OnBuyProductAtShop struct {
	ShopType sd.E_ShopType
}

func (OnBuyProductAtShop) EventID() uint16 {
	return OnBuyProductAtShopID
}

func (e OnBuyProductAtShop) Event() {
}
