//+build ignore

package event

import "model"

// 获得货币
type OnGainCurrency struct {
	TID      int
	Num      int
	Currency *model.Currency
	Notify   bool
	From     int
}

func (OnGainCurrency) EventID() uint16 {
	return OnGainCurrencyID
}

func (e OnGainCurrency) Event() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 失去货币
type OnLostCurrency struct {
	TID      int
	Num      int
	Currency *model.Currency
	Notify   bool
	From     int
}

func (OnLostCurrency) EventID() uint16 {
	return OnLostCurrencyID
}

func (e OnLostCurrency) Event() {
}
