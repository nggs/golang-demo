package entities

import (
	"server/src/entities/account"
	"server/src/entities/role"
	"server/src/entities/zone"
)

func Init() {
	account.Init()
	zone.Init()
	role.Init()
}
