package sd

import (
	"testing"
)

func Test(t *testing.T) {
	if !LoadAll(TestJsonDir) {
		t.Error("加载静态数据失败")
		return
	}
	if !AfterLoadAll(TestJsonDir) {
		t.Error("加载静态表后处理失败")
		return
	}
}
