// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 专属等级表
type ExclusiveLevel struct {
	id           int           // 自增ID
	name         string        // 专属名字
	exclusiveId  int           // 专属装备id（关联exclusive表）
	level        int           // 专属等级
	quality      E_Quality     // 专属装备品质
	dataFrom     E_ItemBigType // 材料来自表（当前等级升下一等级消耗的材料）
	itemId       int           // 升级所需材料id
	itemNum      int           // 升级所需材料数量
	hpUp         float64       // 生命提升百分比
	atkUp        float64       // 攻击提升百分比
	defUp        float64       // 防御提升百分比
	crit         float64       // 暴击
	hit          int           // 命中
	miss         int           // 闪避
	speed        int           // 急速
	physicReduce float64       // 物理减伤率
	magicReduce  float64       // 策略减伤率
	lifeLeech    int           // 吸血等级
	skillId      int           // 技能id

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	upLevelCost        *Cost
	totalRewardByLevel *Reward
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewExclusiveLevel() *ExclusiveLevel {
	sd := &ExclusiveLevel{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 自增ID
func (sd ExclusiveLevel) ID() int {
	return sd.id
}

// 专属名字
func (sd ExclusiveLevel) Name() string {
	return sd.name
}

// 专属装备id（关联exclusive表）
func (sd ExclusiveLevel) ExclusiveID() int {
	return sd.exclusiveId
}

// 专属等级
func (sd ExclusiveLevel) Level() int {
	return sd.level
}

// 专属装备品质
func (sd ExclusiveLevel) Quality() E_Quality {
	return sd.quality
}

// 材料来自表（当前等级升下一等级消耗的材料）
func (sd ExclusiveLevel) DataFrom() E_ItemBigType {
	return sd.dataFrom
}

// 升级所需材料id
func (sd ExclusiveLevel) ItemID() int {
	return sd.itemId
}

// 升级所需材料数量
func (sd ExclusiveLevel) ItemNum() int {
	return sd.itemNum
}

// 生命提升百分比
func (sd ExclusiveLevel) HpUp() float64 {
	return sd.hpUp
}

// 攻击提升百分比
func (sd ExclusiveLevel) AtkUp() float64 {
	return sd.atkUp
}

// 防御提升百分比
func (sd ExclusiveLevel) DefUp() float64 {
	return sd.defUp
}

// 暴击
func (sd ExclusiveLevel) Crit() float64 {
	return sd.crit
}

// 命中
func (sd ExclusiveLevel) Hit() int {
	return sd.hit
}

// 闪避
func (sd ExclusiveLevel) Miss() int {
	return sd.miss
}

// 急速
func (sd ExclusiveLevel) Speed() int {
	return sd.speed
}

// 物理减伤率
func (sd ExclusiveLevel) PhysicReduce() float64 {
	return sd.physicReduce
}

// 策略减伤率
func (sd ExclusiveLevel) MagicReduce() float64 {
	return sd.magicReduce
}

// 吸血等级
func (sd ExclusiveLevel) LifeLeech() int {
	return sd.lifeLeech
}

// 技能id
func (sd ExclusiveLevel) SkillID() int {
	return sd.skillId
}

func (sd ExclusiveLevel) Clone() *ExclusiveLevel {
	n := NewExclusiveLevel()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 专属等级表全局属性
type ExclusiveLevelGlobal struct {
	levelLimitWei  int // 魏国专属等级上限
	levelLimitShu  int // 蜀国专属等级上限
	levelLimitWu   int // 吴国专属等级上限
	levelLimitQun  int // 群国专属等级上限
	levelLimitXian int // 仙专属等级上限
	levelLimitMo   int // 魔专属等级上限
	levelLimitSSR  int // SSR武将专属等级上限
	levelLimitSSSR int // SSSR武将专属等级上限

}

// 魏国专属等级上限
func (g ExclusiveLevelGlobal) LevelLimitWei() int {
	return g.levelLimitWei
}

// 蜀国专属等级上限
func (g ExclusiveLevelGlobal) LevelLimitShu() int {
	return g.levelLimitShu
}

// 吴国专属等级上限
func (g ExclusiveLevelGlobal) LevelLimitWu() int {
	return g.levelLimitWu
}

// 群国专属等级上限
func (g ExclusiveLevelGlobal) LevelLimitQun() int {
	return g.levelLimitQun
}

// 仙专属等级上限
func (g ExclusiveLevelGlobal) LevelLimitXian() int {
	return g.levelLimitXian
}

// 魔专属等级上限
func (g ExclusiveLevelGlobal) LevelLimitMo() int {
	return g.levelLimitMo
}

// SSR武将专属等级上限
func (g ExclusiveLevelGlobal) LevelLimitSSR() int {
	return g.levelLimitSSR
}

// SSSR武将专属等级上限
func (g ExclusiveLevelGlobal) LevelLimitSSSR() int {
	return g.levelLimitSSSR
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type ExclusiveLevelManager struct {
	Datas  []*ExclusiveLevel
	Global ExclusiveLevelGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*ExclusiveLevel // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byExclusiveID map[int][]*ExclusiveLevel // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newExclusiveLevelManager() *ExclusiveLevelManager {
	mgr := &ExclusiveLevelManager{
		Datas:         []*ExclusiveLevel{},
		byID:          map[int]*ExclusiveLevel{},
		byExclusiveID: map[int][]*ExclusiveLevel{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *ExclusiveLevelManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byExclusiveID[d.exclusiveId] = append(mgr.byExclusiveID[d.exclusiveId], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func ExclusiveLevelSize() int {
	return exclusiveLevelMgr.size
}

func (mgr ExclusiveLevelManager) check(path string, row int, sd *ExclusiveLevel) error {
	if _, ok := exclusiveLevelMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *ExclusiveLevelManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	totalReward := Get_Reward()
	exclusiveID := 0
	for _, d := range mgr.Datas {
		if d.id != d.exclusiveId+d.level {
			log.Printf("专属等级表ID[%d]不等于专属ID[%d]和等级[%d]的和\n", d.id, d.exclusiveId, d.level)
		}
		exclusiveSD, ok := GetExclusiveByID(d.exclusiveId)
		if !ok {
			success = false
			log.Printf("专属等级表ID[%d]找不到专属ID[%d]配置\n", d.id, d.exclusiveId)
		}
		groupSD, ok := GetHeroGroupByID(exclusiveSD.heroGroupId)
		if !ok {
			success = false
			log.Printf("专属表ID[%d]找不到组[%d]配置\n", d.exclusiveId, exclusiveSD.heroGroupId)
		}

		levelLimit := 0
		switch groupSD.rarity {
		case E_HeroRarity_RaritySSR:
			levelLimit = GetExclusiveLevelGlobal().LevelLimitSSR()
		case E_HeroRarity_RaritySSSR:
			levelLimit = GetExclusiveLevelGlobal().LevelLimitSSSR()
		default:
			success = false
			log.Printf("专属等级表表头稀有度[%d]专属等级上限[%d]找不到配置\n", groupSD.rarity, levelLimit)
		}

		switch d.dataFrom {
		case E_ItemBigType_Currency:
			if d.level != levelLimit {
				if _, ok := GetCurrencyByID(d.itemId); !ok {
					success = false
					log.Printf("专属等级表升级[%d]消耗的货币[%d]未找到\n", d.id, d.itemId)
				}
				if d.itemNum <= 0 {
					success = false
					log.Printf("专属等级表升级[%d]单抽消耗的货币[%d]数量[%d]不对\n", d.id, d.itemId, d.itemNum)
				}
			}
		case E_ItemBigType_Item:
			if d.level != levelLimit {
				if _, ok := GetItemByID(d.itemId); !ok {
					success = false
					log.Printf("专属等级表升级[%d]消耗的物品[%d]未找到\n", d.id, d.itemId)
				}
				if d.itemNum <= 0 {
					success = false
					log.Printf("专属等级表升级[%d]单抽消耗的物品[%d]数量[%d]不对\n", d.id, d.itemId, d.itemNum)
				}
			}
		default:
			success = false
			log.Printf("专属等级表升级[%d]对应的消耗类型[%v]错误\n", d.id, d.dataFrom)
		}

		d.upLevelCost = AppendOneCost(d.upLevelCost, d.dataFrom, d.itemId, d.itemNum)

		if exclusiveID != d.exclusiveId {
			exclusiveID = d.exclusiveId
			totalReward = Get_Reward()
		}
		d.totalRewardByLevel = totalReward.Clone()
		totalReward.AppendOne(d.dataFrom, d.itemId, d.itemNum)
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *ExclusiveLevelManager) each(f func(sd *ExclusiveLevel) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *ExclusiveLevelManager) findIf(f func(sd *ExclusiveLevel) (find bool)) *ExclusiveLevel {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachExclusiveLevel(f func(sd ExclusiveLevel) (continued bool)) {
	for _, sd := range exclusiveLevelMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindExclusiveLevelIf(f func(sd ExclusiveLevel) bool) (ExclusiveLevel, bool) {
	for _, sd := range exclusiveLevelMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilExclusiveLevel, false
}

func GetExclusiveLevelByID(id int) (ExclusiveLevel, bool) {
	temp, ok := exclusiveLevelMgr.byID[id]
	if !ok {
		return nilExclusiveLevel, false
	}
	return *temp, true
}

func GetExclusiveLevelByExclusiveID(exclusiveId int) (vs []ExclusiveLevel, ok bool) {
	var ds []*ExclusiveLevel
	ds, ok = exclusiveLevelMgr.byExclusiveID[exclusiveId]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachExclusiveLevelByExclusiveID(exclusiveId int, fn func(d ExclusiveLevel) (continued bool)) bool {
	ds, ok := exclusiveLevelMgr.byExclusiveID[exclusiveId]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetExclusiveLevelGlobal() ExclusiveLevelGlobal {
	return exclusiveLevelMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func GetExclusiveLevelByExclusiveIDAndLevel(exclusiveId int, level int) (ExclusiveLevel, bool) {
	id := exclusiveId + level
	temp, ok := exclusiveLevelMgr.byID[id]
	if !ok {
		return nilExclusiveLevel, false
	}
	return *temp, true
}

func (sd ExclusiveLevel) UpLevelCost() *Cost {
	return sd.upLevelCost.Clone()
}

func (sd ExclusiveLevel) TotalRewardByLevel() *Reward {
	return sd.totalRewardByLevel.Clone()
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
