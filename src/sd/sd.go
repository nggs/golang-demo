package sd

import (
	jsoniter "github.com/json-iterator/go"
	"github.com/json-iterator/go/extra"
)

const TestJsonDir = "../../data/json"

var json = jsoniter.ConfigCompatibleWithStandardLibrary

func init() {
	extra.SupportPrivateFields()
}
