// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 活动表
type Activity struct {
	id         int    // ID
	activityId int    // 活动ID 与活动时间表ID一致
	targetId   int    // 同个时间段的同个活动中的排序
	desc       string // 活动描述
	condition  string // 活动参数表， activityTime表中，activityType=1，填写quest表的任务id
	value1     int    // 参数1
	value2     int    // 参数2
	value3     int    // 参数3
	value4     int    // 参数4
	value5     int    // 参数5
	vip        int    // 可以购买的VIP等级 99=激活月卡才能购买
	reward     int    // 奖励
	times      int    // 领取次数
	charge     bool   // 是否关联充值表

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewActivity() *Activity {
	sd := &Activity{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// ID
func (sd Activity) ID() int {
	return sd.id
}

// 活动ID 与活动时间表ID一致
func (sd Activity) ActivityID() int {
	return sd.activityId
}

// 同个时间段的同个活动中的排序
func (sd Activity) TargetID() int {
	return sd.targetId
}

// 活动描述
func (sd Activity) Desc() string {
	return sd.desc
}

// 活动参数表， activityTime表中，activityType=1，填写quest表的任务id
func (sd Activity) Condition() string {
	return sd.condition
}

// 参数1
func (sd Activity) Value1() int {
	return sd.value1
}

// 参数2
func (sd Activity) Value2() int {
	return sd.value2
}

// 参数3
func (sd Activity) Value3() int {
	return sd.value3
}

// 参数4
func (sd Activity) Value4() int {
	return sd.value4
}

// 参数5
func (sd Activity) Value5() int {
	return sd.value5
}

// 可以购买的VIP等级 99=激活月卡才能购买
func (sd Activity) Vip() int {
	return sd.vip
}

// 奖励
func (sd Activity) Reward() int {
	return sd.reward
}

// 领取次数
func (sd Activity) Times() int {
	return sd.times
}

// 是否关联充值表
func (sd Activity) Charge() bool {
	return sd.charge
}

func (sd Activity) Clone() *Activity {
	n := NewActivity()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 活动表全局属性
type ActivityGlobal struct {
	openServeGiftLastPage       int // 开服限时礼包活动最后一页的背景图位置
	openServeGiftLastPageReward int // 开服限时礼包活动最后一页奖励预览的奖励组
	openServeGiftLastPageJump   int // 开服限时礼包活动最后一页点击前往跳转到活动ID，关联此表中activityId
	heroAdventAgainShow         int // 当前挑战关卡达到指定关卡时再次弹出貂蝉降临活动，关联关卡表
	heroLevelUpGiftPack1Max     int // 全天最多触发几个武将升级限时礼包1
	heroLevelUpGiftPack2Max     int // 全天最多触发几个武将升级限时礼包2
	heroAdvanceGiftPackMax      int // 全天最多触发几个武将进阶限时礼包
	boughtMonthCard             int // 激活月卡才能购买

}

// 开服限时礼包活动最后一页的背景图位置
func (g ActivityGlobal) OpenServeGiftLastPage() int {
	return g.openServeGiftLastPage
}

// 开服限时礼包活动最后一页奖励预览的奖励组
func (g ActivityGlobal) OpenServeGiftLastPageReward() int {
	return g.openServeGiftLastPageReward
}

// 开服限时礼包活动最后一页点击前往跳转到活动ID，关联此表中activityId
func (g ActivityGlobal) OpenServeGiftLastPageJump() int {
	return g.openServeGiftLastPageJump
}

// 当前挑战关卡达到指定关卡时再次弹出貂蝉降临活动，关联关卡表
func (g ActivityGlobal) HeroAdventAgainShow() int {
	return g.heroAdventAgainShow
}

// 全天最多触发几个武将升级限时礼包1
func (g ActivityGlobal) HeroLevelUpGiftPack1Max() int {
	return g.heroLevelUpGiftPack1Max
}

// 全天最多触发几个武将升级限时礼包2
func (g ActivityGlobal) HeroLevelUpGiftPack2Max() int {
	return g.heroLevelUpGiftPack2Max
}

// 全天最多触发几个武将进阶限时礼包
func (g ActivityGlobal) HeroAdvanceGiftPackMax() int {
	return g.heroAdvanceGiftPackMax
}

// 激活月卡才能购买
func (g ActivityGlobal) BoughtMonthCard() int {
	return g.boughtMonthCard
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type ActivityManager struct {
	Datas  []*Activity
	Global ActivityGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*Activity // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byActivityID map[int][]*Activity // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	QuestIDToActivityTID map[string]int32
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newActivityManager() *ActivityManager {
	mgr := &ActivityManager{
		Datas:        []*Activity{},
		byID:         map[int]*Activity{},
		byActivityID: map[int][]*Activity{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>
	mgr.QuestIDToActivityTID = map[string]int32{}
	//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *ActivityManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byActivityID[d.activityId] = append(mgr.byActivityID[d.activityId], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>
	//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func ActivitySize() int {
	return activityMgr.size
}

func (mgr ActivityManager) check(path string, row int, sd *Activity) error {
	if _, ok := activityMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *ActivityManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		activityTimeSD, ok := GetActivityTimeByID(d.activityId)
		if !ok {
			success = false
			log.Printf("活动表[%d]的活动[%d]不存在", d.id, d.activityId)
		}
		switch activityTimeSD.activityType {
		case E_ActivityType_LimitLogin:
			if _, err := strconv.Atoi(d.condition); err != nil {
				success = false
				log.Printf("活动表[%d]的条件[%s]不是数字", d.id, d.condition)
			}
		}

		actTimeSD, ok := GetActivityTimeByID(d.ActivityID())
		if ok && actTimeSD.ActivityType() == E_ActivityType_QuestActivity {
			mgr.QuestIDToActivityTID[d.Condition()] = int32(d.ID())
		}
		if d.reward > 0 {
			if _, ok := GetLootByGroup(d.reward); !ok {
				success = false
				log.Printf("活动表[%d]奖励[%d]不存在", d.id, d.reward)
			}
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *ActivityManager) each(f func(sd *Activity) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *ActivityManager) findIf(f func(sd *Activity) (find bool)) *Activity {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachActivity(f func(sd Activity) (continued bool)) {
	for _, sd := range activityMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindActivityIf(f func(sd Activity) bool) (Activity, bool) {
	for _, sd := range activityMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilActivity, false
}

func GetActivityByID(id int) (Activity, bool) {
	temp, ok := activityMgr.byID[id]
	if !ok {
		return nilActivity, false
	}
	return *temp, true
}

func GetActivityByActivityID(activityId int) (vs []Activity, ok bool) {
	var ds []*Activity
	ds, ok = activityMgr.byActivityID[activityId]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachActivityByActivityID(activityId int, fn func(d Activity) (continued bool)) bool {
	ds, ok := activityMgr.byActivityID[activityId]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetActivityGlobal() ActivityGlobal {
	return activityMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func GetActivityTIDByQuestID(questID string) int {
	if v, ok := activityMgr.QuestIDToActivityTID[questID]; ok {
		return int(v)
	}
	return 0
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
