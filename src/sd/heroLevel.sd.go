// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 英雄升级表
type HeroLevel struct {
	level           int           // 等级 当前等级升级下一级需要的材料
	dataFrom1       E_ItemBigType // 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
	itemId1         int           // 物品ID，关联物品表
	itemNum1        int           // 物品数量
	dataFrom2       E_ItemBigType // 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
	itemId2         int           // 物品ID，关联物品表
	itemNum2        int           // 物品数量
	dataFrom3       E_ItemBigType // 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
	itemId3         int           // 物品ID，关联物品表
	itemNum3        int           // 物品数量
	hp              float64       // 生命
	atk             float64       // 攻击
	def             float64       // 防御
	heroMasterLevel int           // 水晶祭司等级限制

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	cost      *Cost
	totalCost *Cost
	totalHp   float64
	totalAtk  float64
	totalDef  float64
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewHeroLevel() *HeroLevel {
	sd := &HeroLevel{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 等级 当前等级升级下一级需要的材料
func (sd HeroLevel) Level() int {
	return sd.level
}

// 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
func (sd HeroLevel) DataFrom1() E_ItemBigType {
	return sd.dataFrom1
}

// 物品ID，关联物品表
func (sd HeroLevel) ItemID1() int {
	return sd.itemId1
}

// 物品数量
func (sd HeroLevel) ItemNum1() int {
	return sd.itemNum1
}

// 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
func (sd HeroLevel) DataFrom2() E_ItemBigType {
	return sd.dataFrom2
}

// 物品ID，关联物品表
func (sd HeroLevel) ItemID2() int {
	return sd.itemId2
}

// 物品数量
func (sd HeroLevel) ItemNum2() int {
	return sd.itemNum2
}

// 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
func (sd HeroLevel) DataFrom3() E_ItemBigType {
	return sd.dataFrom3
}

// 物品ID，关联物品表
func (sd HeroLevel) ItemID3() int {
	return sd.itemId3
}

// 物品数量
func (sd HeroLevel) ItemNum3() int {
	return sd.itemNum3
}

// 生命
func (sd HeroLevel) Hp() float64 {
	return sd.hp
}

// 攻击
func (sd HeroLevel) Atk() float64 {
	return sd.atk
}

// 防御
func (sd HeroLevel) Def() float64 {
	return sd.def
}

// 水晶祭司等级限制
func (sd HeroLevel) HeroMasterLevel() int {
	return sd.heroMasterLevel
}

func (sd HeroLevel) Clone() *HeroLevel {
	n := NewHeroLevel()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 英雄升级表全局属性
type HeroLevelGlobal struct {
	batchLevel            []int // 若条件满足可以直升的等级数
	heroShareDefaultLimit int   // 御将台初始等级上限
	heroShareLimitUp      int   // 每拥有一个白金武将增加多少等级上限
	heroShareLimitUp2     int   // 每拥有一个白金+5武将增加多少等级上限

}

// 若条件满足可以直升的等级数
func (g HeroLevelGlobal) BatchLevel() []int {
	return g.batchLevel
}

// 御将台初始等级上限
func (g HeroLevelGlobal) HeroShareDefaultLimit() int {
	return g.heroShareDefaultLimit
}

// 每拥有一个白金武将增加多少等级上限
func (g HeroLevelGlobal) HeroShareLimitUp() int {
	return g.heroShareLimitUp
}

// 每拥有一个白金+5武将增加多少等级上限
func (g HeroLevelGlobal) HeroShareLimitUp2() int {
	return g.heroShareLimitUp2
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type HeroLevelManager struct {
	Datas  []*HeroLevel
	Global HeroLevelGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byLevel map[int]*HeroLevel // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	maxLevel int
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newHeroLevelManager() *HeroLevelManager {
	mgr := &HeroLevelManager{
		Datas:   []*HeroLevel{},
		byLevel: map[int]*HeroLevel{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *HeroLevelManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>
		d.cost = Get_Cost()
		if d.dataFrom1 > E_ItemBigType_Null && d.itemId1 > 0 {
			d.cost.AppendOne(d.dataFrom1, d.itemId1, d.itemNum1)
		}
		if d.dataFrom2 > E_ItemBigType_Null && d.itemId2 > 0 {
			d.cost.AppendOne(d.dataFrom2, d.itemId2, d.itemNum2)
		}
		if d.dataFrom3 > E_ItemBigType_Null && d.itemId3 > 0 {
			d.cost.AppendOne(d.dataFrom3, d.itemId3, d.itemNum3)
		}
		if mgr.maxLevel < d.level {
			mgr.maxLevel = d.level
		}
		//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byLevel[d.level] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func HeroLevelSize() int {
	return heroLevelMgr.size
}

func (mgr HeroLevelManager) check(path string, row int, sd *HeroLevel) error {
	if _, ok := heroLevelMgr.byLevel[sd.level]; ok {
		return fmt.Errorf("[%s]第[%d]行的level[%v]重复", path, row, sd.level)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *HeroLevelManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	var totalCost = Get_Cost()
	var totalHp float64
	var totalAtk float64
	var totalDef float64
	for _, d := range mgr.Datas {
		if d.cost == nil {
			success = false
			log.Printf("英雄升级表[%d]级没有消耗\n", d.level)
			continue
		}
		d.totalCost = totalCost.Clone()
		totalCost = totalCost.Merge(d.cost)
		for i, t := range d.cost.Things {
			switch t.Type {
			case E_ItemBigType_Currency:
				if _, ok := GetCurrencyByID(int(t.TID)); !ok {
					success = false
					log.Printf("英雄升级表[%d]级消耗%d的货币类型[%d]错误\n", d.level, i+1, t.TID)
				}
				if t.Num <= 0 {
					success = false
					log.Printf("英雄升级表[%d]级消耗%d的货币数量[%d]错误\n", d.level, i+1, t.Num)
				}
			case E_ItemBigType_Item:
				if _, ok := GetItemByID(int(t.TID)); !ok {
					success = false
					log.Printf("英雄升级表[%d]级消耗%d的道具类型[%d]错误\n", d.level, i+1, t.TID)
				}
				if t.Num <= 0 {
					success = false
					log.Printf("英雄升级表[%d]级消耗%d的道具数量[%d]错误\n", d.level, i+1, t.Num)
				}
			case E_ItemBigType_Hero:
				if _, ok := GetHeroBasicByID(int(t.TID)); !ok {
					success = false
					log.Printf("英雄升级表[%d]级消耗%d的英雄类型[%d]错误\n", d.level, i+1, t.TID)
				}
				if t.Num <= 0 {
					success = false
					log.Printf("英雄升级表[%d]级消耗%d的英雄数量[%d]错误\n", d.level, i+1, t.Num)
				}
			case E_ItemBigType_Equip:
				if _, ok := GetEquipmentByID(int(t.TID)); !ok {
					success = false
					log.Printf("英雄升级表[%d]级消耗%d的装备类型[%d]错误\n", d.level, i+1, t.TID)
				}
				if t.Num <= 0 {
					success = false
					log.Printf("英雄升级表[%d]级消耗%d的装备数量[%d]错误\n", d.level, i+1, t.Num)
				}
			default:
				success = false
				log.Printf("英雄升级表[%d]级消耗%d的大类型[%d]错误\n", d.level, i+1, t.Type)
			}
		}

		d.totalHp = totalHp
		totalHp += d.hp

		d.totalAtk = totalAtk
		totalAtk += d.atk

		d.totalDef = totalDef
		totalDef += d.def
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *HeroLevelManager) each(f func(sd *HeroLevel) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *HeroLevelManager) findIf(f func(sd *HeroLevel) (find bool)) *HeroLevel {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachHeroLevel(f func(sd HeroLevel) (continued bool)) {
	for _, sd := range heroLevelMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindHeroLevelIf(f func(sd HeroLevel) bool) (HeroLevel, bool) {
	for _, sd := range heroLevelMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilHeroLevel, false
}

func GetHeroLevelByLevel(level int) (HeroLevel, bool) {
	temp, ok := heroLevelMgr.byLevel[level]
	if !ok {
		return nilHeroLevel, false
	}
	return *temp, true
}

func GetHeroLevelGlobal() HeroLevelGlobal {
	return heroLevelMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd HeroLevel) Cost() *Cost {
	return sd.cost.Clone()
}

func (sd HeroLevel) TotalCost() *Cost {
	return sd.totalCost.Clone()
}

func (sd HeroLevel) TotalHp() float64 {
	return sd.totalHp
}

func (sd HeroLevel) TotalAtk() float64 {
	return sd.totalAtk
}

func (sd HeroLevel) TotalDef() float64 {
	return sd.totalDef
}

func GetHeroMaxLevel() int {
	return heroLevelMgr.maxLevel
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
