package sd

type CostThing struct {
	Type E_ItemBigType
	TID  int
	Num  int
}

func (m CostThing) IsEmpty() bool {
	return m.Type == 0 || m.TID == 0 || m.Num == 0
}

func (m CostThing) ToRewardThing(thing *RewardThing) *RewardThing {
	if thing == nil {
		thing = Get_RewardThing()
	}
	thing.Type = m.Type
	thing.TID = m.TID
	thing.Num = m.Num
	return thing
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type Cost struct {
	Things []*CostThing
}

func (m Cost) Each(f func(thing *CostThing) (continued bool)) {
	for _, thing := range m.Things {
		if !f(thing) {
			break
		}
	}
}

func (m Cost) IsEmpty() bool {
	if len(m.Things) == 0 {
		return true
	}
	return false
}

func (m Cost) ToReward(reward *Reward) *Reward {
	if reward == nil {
		reward = Get_Reward()
	}
	if len(m.Things) == 0 {
		return reward
	}
	reward.Things = make([]*RewardThing, len(m.Things))
	for i, thing := range m.Things {
		reward.Things[i] = thing.ToRewardThing(reward.Things[i])
	}
	return reward
}

func (m *Cost) getByTID(tid int) *CostThing {
	for _, thing := range m.Things {
		if thing.TID == tid {
			return thing
		}
	}
	return nil
}

func (m *Cost) AppendOne(tYpE E_ItemBigType, tid int, num int) *Cost {
	if tYpE <= 0 || tid <= 0 || num <= 0 {
		return m
	}

	thing := m.getByTID(tid)
	if thing != nil {
		thing.Num += num
		return m
	}

	thing = Get_CostThing()
	thing.Type = tYpE
	thing.TID = tid
	thing.Num = num
	m.Things = append(m.Things, thing)
	return m
}

func (m *Cost) AppendOneThing(thing *CostThing) *Cost {
	if thing == nil {
		return m
	}

	t := m.getByTID(thing.TID)
	if t != nil {
		t.Num += thing.Num
		return m
	}

	m.Things = append(m.Things, thing.Clone())

	return m
}

func (m *Cost) Merge(other *Cost) *Cost {
	var thing *CostThing
	for _, t := range other.Things {
		thing = m.getByTID(t.TID)
		if thing != nil {
			thing.Num += t.Num
		} else {
			m.Things = append(m.Things, t.Clone())
		}
	}
	return m
}

func (m *Cost) Multiple(mul int) *Cost {
	for _, thing := range m.Things {
		thing.Num = thing.Num * mul
	}
	return m
}

func AppendOneCost(cost *Cost, tYpE E_ItemBigType, tid int, num int) *Cost {
	if cost == nil {
		cost = Get_Cost()
	}
	return cost.AppendOne(tYpE, tid, num)
}
