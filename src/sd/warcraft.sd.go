// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 兵书表
type Warcraft struct {
	id            int       // 兵书ID
	name          string    // 兵书名字
	icon          string    // 兵书图标
	desc          string    // 兵书描述
	heroRequired  []int     // 适用的英雄组id（为0则适用全部英雄）
	quality       E_Quality // 装备的品质，关联品质表 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+1
	hp            int       // 生命
	atk           int       // 攻击
	def           int       // 防御
	crit          float64   // 暴击
	hit           int       // 命中
	miss          int       // 闪避
	speed         int       // 急速
	recover       int       // 每秒恢复
	physicReduce  float64   // 物理减伤率
	magicReduce   float64   // 魔法减伤率
	lifeLeech     int       // 吸血等级
	critDamage    float64   // 爆伤加成
	hpUp          int       // 生命增长值
	atkUp         int       // 攻击增长值
	defUp         int       // 防御增长值
	hitUp         float64   // 命中增长值
	missUp        float64   // 闪避增长值
	recoverUp     float64   // 每秒回血增长值
	hpLevel       int       // 随兵书等级成长的生命值
	atkLevel      int       // 随兵书等级成长的攻击力
	defLevel      int       // 随兵书等级成长的防御力
	skill         int       // 兵书附带的技能id，为空则没有技能
	power         int       // 兵书提供的战力。计算兵书战力时需要额外加上该值
	portraitsShow int       // 是否显示在图鉴
	acquire       int       // 获取渠道 关联acquire表
	useJump       int       // 在背包中使用，点击后跳转到指定页面

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewWarcraft() *Warcraft {
	sd := &Warcraft{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 兵书ID
func (sd Warcraft) ID() int {
	return sd.id
}

// 兵书名字
func (sd Warcraft) Name() string {
	return sd.name
}

// 兵书图标
func (sd Warcraft) Icon() string {
	return sd.icon
}

// 兵书描述
func (sd Warcraft) Desc() string {
	return sd.desc
}

// 适用的英雄组id（为0则适用全部英雄）
func (sd Warcraft) HeroRequired() []int {
	return sd.heroRequired
}

// 装备的品质，关联品质表 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+1
func (sd Warcraft) Quality() E_Quality {
	return sd.quality
}

// 生命
func (sd Warcraft) Hp() int {
	return sd.hp
}

// 攻击
func (sd Warcraft) Atk() int {
	return sd.atk
}

// 防御
func (sd Warcraft) Def() int {
	return sd.def
}

// 暴击
func (sd Warcraft) Crit() float64 {
	return sd.crit
}

// 命中
func (sd Warcraft) Hit() int {
	return sd.hit
}

// 闪避
func (sd Warcraft) Miss() int {
	return sd.miss
}

// 急速
func (sd Warcraft) Speed() int {
	return sd.speed
}

// 每秒恢复
func (sd Warcraft) Recover() int {
	return sd.recover
}

// 物理减伤率
func (sd Warcraft) PhysicReduce() float64 {
	return sd.physicReduce
}

// 魔法减伤率
func (sd Warcraft) MagicReduce() float64 {
	return sd.magicReduce
}

// 吸血等级
func (sd Warcraft) LifeLeech() int {
	return sd.lifeLeech
}

// 爆伤加成
func (sd Warcraft) CritDamage() float64 {
	return sd.critDamage
}

// 生命增长值
func (sd Warcraft) HpUp() int {
	return sd.hpUp
}

// 攻击增长值
func (sd Warcraft) AtkUp() int {
	return sd.atkUp
}

// 防御增长值
func (sd Warcraft) DefUp() int {
	return sd.defUp
}

// 命中增长值
func (sd Warcraft) HitUp() float64 {
	return sd.hitUp
}

// 闪避增长值
func (sd Warcraft) MissUp() float64 {
	return sd.missUp
}

// 每秒回血增长值
func (sd Warcraft) RecoverUp() float64 {
	return sd.recoverUp
}

// 随兵书等级成长的生命值
func (sd Warcraft) HpLevel() int {
	return sd.hpLevel
}

// 随兵书等级成长的攻击力
func (sd Warcraft) AtkLevel() int {
	return sd.atkLevel
}

// 随兵书等级成长的防御力
func (sd Warcraft) DefLevel() int {
	return sd.defLevel
}

// 兵书附带的技能id，为空则没有技能
func (sd Warcraft) Skill() int {
	return sd.skill
}

// 兵书提供的战力。计算兵书战力时需要额外加上该值
func (sd Warcraft) Power() int {
	return sd.power
}

// 是否显示在图鉴
func (sd Warcraft) PortraitsShow() int {
	return sd.portraitsShow
}

// 获取渠道 关联acquire表
func (sd Warcraft) Acquire() int {
	return sd.acquire
}

// 在背包中使用，点击后跳转到指定页面
func (sd Warcraft) UseJump() int {
	return sd.useJump
}

func (sd Warcraft) Clone() *Warcraft {
	n := NewWarcraft()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 兵书表全局属性
type WarcraftGlobal struct {
	growth int // 几级开始计算成长属性

}

// 几级开始计算成长属性
func (g WarcraftGlobal) Growth() int {
	return g.growth
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type WarcraftManager struct {
	Datas  []*Warcraft
	Global WarcraftGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*Warcraft // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byQuality map[E_Quality][]*Warcraft // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newWarcraftManager() *WarcraftManager {
	mgr := &WarcraftManager{
		Datas:     []*Warcraft{},
		byID:      map[int]*Warcraft{},
		byQuality: map[E_Quality][]*Warcraft{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *WarcraftManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byQuality[d.quality] = append(mgr.byQuality[d.quality], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func WarcraftSize() int {
	return warcraftMgr.size
}

func (mgr WarcraftManager) check(path string, row int, sd *Warcraft) error {
	if _, ok := warcraftMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *WarcraftManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if _, ok := GetQualityByID(d.quality); !ok {
			success = false
			log.Printf("兵书表兵书[%d]的品质[%d]找不到对应配置\n", d.id, d.quality)
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *WarcraftManager) each(f func(sd *Warcraft) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *WarcraftManager) findIf(f func(sd *Warcraft) (find bool)) *Warcraft {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachWarcraft(f func(sd Warcraft) (continued bool)) {
	for _, sd := range warcraftMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindWarcraftIf(f func(sd Warcraft) bool) (Warcraft, bool) {
	for _, sd := range warcraftMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilWarcraft, false
}

func GetWarcraftByID(id int) (Warcraft, bool) {
	temp, ok := warcraftMgr.byID[id]
	if !ok {
		return nilWarcraft, false
	}
	return *temp, true
}

func GetWarcraftByQuality(quality E_Quality) (vs []Warcraft, ok bool) {
	var ds []*Warcraft
	ds, ok = warcraftMgr.byQuality[quality]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachWarcraftByQuality(quality E_Quality, fn func(d Warcraft) (continued bool)) bool {
	ds, ok := warcraftMgr.byQuality[quality]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetWarcraftGlobal() WarcraftGlobal {
	return warcraftMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd Warcraft) BattleAttribute(ba *BattleAttribute, level int, warcraftLevel int) *BattleAttribute {
	if ba == nil {
		ba = GetBattleAttribute()
	}
	if level < GetWarcraftGlobal().Growth() {
		level = GetWarcraftGlobal().Growth()
	}
	up := level - GetWarcraftGlobal().Growth()

	warcraftRate := int(warcraftLevel/GetWarcraftLevelGlobal().LockLevel()) * GetWarcraftLevelGlobal().LockPlus()
	baseHp := (sd.hp+sd.hpLevel*warcraftLevel)*(100+warcraftRate)/100 + sd.hpUp*up
	baseAtk := (sd.atk+sd.atkLevel*warcraftLevel)*(100+warcraftRate)/100 + sd.atkUp*up
	baseDef := (sd.def+sd.defLevel*warcraftLevel)*(100+warcraftRate)/100 + sd.defUp*up
	ba.add(E_BattleAttribute_Hp, BattleAttributeValue(baseHp))
	ba.add(E_BattleAttribute_Atk, BattleAttributeValue(baseAtk))
	ba.add(E_BattleAttribute_Def, BattleAttributeValue(baseDef))

	ba.add(E_BattleAttribute_Crit, BattleAttributeValue(sd.crit))
	ba.add(E_BattleAttribute_Hit, BattleAttributeValue(float64(sd.hit)+sd.hitUp*float64(up)))
	ba.add(E_BattleAttribute_Miss, BattleAttributeValue(float64(sd.miss)+sd.missUp*float64(up)))
	ba.add(E_BattleAttribute_Speed, BattleAttributeValue(sd.speed))
	ba.add(E_BattleAttribute_Recover, BattleAttributeValue(float64(sd.recover)+sd.recoverUp*float64(up)))
	ba.add(E_BattleAttribute_PhysicReduce, BattleAttributeValue(sd.physicReduce))
	ba.add(E_BattleAttribute_MagicReduce, BattleAttributeValue(sd.magicReduce))
	ba.add(E_BattleAttribute_LifeLeech, BattleAttributeValue(sd.lifeLeech))
	ba.add(E_BattleAttribute_CritDamage, BattleAttributeValue(sd.critDamage))
	return ba
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
