// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 次数购买表
type MoneyBuy struct {
	id      int            // ID
	buyFunc E_FunctionType // 购买功能
	times   int            // 第几次购买 若购买次数1次后，没有后续次数，表示后续购买价格和最后一次一样
	item    E_CurrencyType // 货币ID，关联货币表
	num     int            // 消耗货币数量

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewMoneyBuy() *MoneyBuy {
	sd := &MoneyBuy{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// ID
func (sd MoneyBuy) ID() int {
	return sd.id
}

// 购买功能
func (sd MoneyBuy) BuyFunc() E_FunctionType {
	return sd.buyFunc
}

// 第几次购买 若购买次数1次后，没有后续次数，表示后续购买价格和最后一次一样
func (sd MoneyBuy) Times() int {
	return sd.times
}

// 货币ID，关联货币表
func (sd MoneyBuy) Item() E_CurrencyType {
	return sd.item
}

// 消耗货币数量
func (sd MoneyBuy) Num() int {
	return sd.num
}

func (sd MoneyBuy) Clone() *MoneyBuy {
	n := NewMoneyBuy()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 次数购买表全局属性
type MoneyBuyGlobal struct {
	heroBagBeginNum        int // 初始武将背包格数
	heroBagBuyNum          int // 武将背包每次购买格数
	heroMasterYardBeginNum int // 御将台（共鸣水晶）初始槽位
	heroMasterYardBuyNum   int // 御将台（共鸣水晶）购买增加槽位数量

}

// 初始武将背包格数
func (g MoneyBuyGlobal) HeroBagBeginNum() int {
	return g.heroBagBeginNum
}

// 武将背包每次购买格数
func (g MoneyBuyGlobal) HeroBagBuyNum() int {
	return g.heroBagBuyNum
}

// 御将台（共鸣水晶）初始槽位
func (g MoneyBuyGlobal) HeroMasterYardBeginNum() int {
	return g.heroMasterYardBeginNum
}

// 御将台（共鸣水晶）购买增加槽位数量
func (g MoneyBuyGlobal) HeroMasterYardBuyNum() int {
	return g.heroMasterYardBuyNum
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type MoneyBuyManager struct {
	Datas  []*MoneyBuy
	Global MoneyBuyGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*MoneyBuy // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byBuyFunc map[E_FunctionType][]*MoneyBuy // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	maxBuyTimeSDs            map[E_FunctionType]*MoneyBuy
	quickBuyItem             map[int]map[int]*MoneyBuy
	quickByItemMaxBuyTimeSDs map[int]*MoneyBuy
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newMoneyBuyManager() *MoneyBuyManager {
	mgr := &MoneyBuyManager{
		Datas:     []*MoneyBuy{},
		byID:      map[int]*MoneyBuy{},
		byBuyFunc: map[E_FunctionType][]*MoneyBuy{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>
	mgr.maxBuyTimeSDs = map[E_FunctionType]*MoneyBuy{}
	mgr.quickBuyItem = map[int]map[int]*MoneyBuy{}
	mgr.quickByItemMaxBuyTimeSDs = map[int]*MoneyBuy{}
	//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *MoneyBuyManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byBuyFunc[d.buyFunc] = append(mgr.byBuyFunc[d.buyFunc], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func MoneyBuySize() int {
	return moneyBuyMgr.size
}

func (mgr MoneyBuyManager) check(path string, row int, sd *MoneyBuy) error {
	if _, ok := moneyBuyMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *MoneyBuyManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	Each_E_FunctionType(func(functionType E_FunctionType) bool {
		ds, ok := mgr.byBuyFunc[functionType]
		if !ok {
			return true
		}
		for i, d := range ds {
			// 判断购买次数是否连续
			if d.times != i+1 {
				success = false
				log.Printf("次数购买表记录[%d]购买次数不连续[%d]!=[%d]\n", d.id, d.times, i+1)
			}
			// 判断扣除货币数量
			if d.item != E_CurrencyType_Null {
				if d.num < 0 {
					success = false
					log.Printf("次数购买表记录[%d]消耗货币[%d]数量错误[%d]\n", d.id, d.item, d.num)
				}
			}
			// 记录下最大购买次数
			if maxSD, ok := mgr.maxBuyTimeSDs[functionType]; !ok {
				mgr.maxBuyTimeSDs[functionType] = d
			} else {
				if maxSD.times < d.times {
					mgr.maxBuyTimeSDs[functionType] = d
				}
			}

			//if d.dataFrom > 0 && d.num <= 0 {
			//	success = false
			//	log.Printf("次数购买表记录[%d]快捷购买的道具[%d]的数量不对", d.id, d.itemId)
			//}
			//switch d.dataFrom {
			//case E_ItemBigType_Currency:
			//	if _, ok := GetCurrencyByID(d.itemId); !ok {
			//		success = false
			//		log.Printf("次数购买表记录[%d]快捷购买的货币[%d]找不到数据", d.id, d.itemId)
			//	}
			//case E_ItemBigType_Item:
			//	if _, ok := GetItemByID(d.itemId); !ok {
			//		success = false
			//		log.Printf("次数购买表记录[%d]快捷购买的物品[%d]找不到数据", d.id, d.itemId)
			//	}
			//	if is, ok := mgr.quickBuyItem[d.itemId]; ok {
			//		is[d.times] = d
			//	} else {
			//		mgr.quickBuyItem[d.itemId] = map[int]*MoneyBuy{
			//			d.times: d,
			//		}
			//	}
			//	if i, ok := mgr.quickByItemMaxBuyTimeSDs[d.itemId]; ok {
			//		if i.times < d.times {
			//			mgr.quickByItemMaxBuyTimeSDs[d.itemId] = i
			//		}
			//	} else {
			//		mgr.quickByItemMaxBuyTimeSDs[d.itemId] = i
			//	}
			//case E_ItemBigType_Hero:
			//	if _, ok := GetHeroBasicByID(d.itemId); !ok {
			//		success = false
			//		log.Printf("次数购买表记录[%d]快捷购买的英雄[%d]找不到数据", d.id, d.itemId)
			//	}
			//case E_ItemBigType_Equip:
			//	if _, ok := GetEquipmentByID(d.itemId); !ok {
			//		success = false
			//		log.Printf("次数购买表记录[%d]快捷购买的装备[%d]找不到数据", d.id, d.itemId)
			//	}
			//}
		}
		return true
	})
	if _, ok := mgr.maxBuyTimeSDs[E_FunctionType_HeroBag]; !ok {
		success = false
		log.Printf("次数购买表没有配置英雄背包购买次数\n")
	}
	if _, ok := mgr.maxBuyTimeSDs[E_FunctionType_HeroShare]; !ok {
		success = false
		log.Printf("次数购买表没有配置御将台格子购买次数\n")
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *MoneyBuyManager) each(f func(sd *MoneyBuy) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *MoneyBuyManager) findIf(f func(sd *MoneyBuy) (find bool)) *MoneyBuy {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachMoneyBuy(f func(sd MoneyBuy) (continued bool)) {
	for _, sd := range moneyBuyMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindMoneyBuyIf(f func(sd MoneyBuy) bool) (MoneyBuy, bool) {
	for _, sd := range moneyBuyMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilMoneyBuy, false
}

func GetMoneyBuyByID(id int) (MoneyBuy, bool) {
	temp, ok := moneyBuyMgr.byID[id]
	if !ok {
		return nilMoneyBuy, false
	}
	return *temp, true
}

func GetMoneyBuyByBuyFunc(buyFunc E_FunctionType) (vs []MoneyBuy, ok bool) {
	var ds []*MoneyBuy
	ds, ok = moneyBuyMgr.byBuyFunc[buyFunc]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachMoneyBuyByBuyFunc(buyFunc E_FunctionType, fn func(d MoneyBuy) (continued bool)) bool {
	ds, ok := moneyBuyMgr.byBuyFunc[buyFunc]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetMoneyBuyGlobal() MoneyBuyGlobal {
	return moneyBuyMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func GetMoneyBuyMaxTimeSD(t E_FunctionType) (MoneyBuy, bool) {
	if d, ok := moneyBuyMgr.maxBuyTimeSDs[t]; ok {
		return *d, true
	}
	return nilMoneyBuy, false
}

func GetHeroBagMaxBuyTimeSD() MoneyBuy {
	return *(moneyBuyMgr.maxBuyTimeSDs[E_FunctionType_HeroBag])
}

func GetHeroMasterGridMaxBuyTimeSD() MoneyBuy {
	return *(moneyBuyMgr.maxBuyTimeSDs[E_FunctionType_HeroShare])
}

func GetHeroMasterGridGoldMaxBuyTimeSD() MoneyBuy {
	return *(moneyBuyMgr.maxBuyTimeSDs[E_FunctionType_HeroSharePlaceGoLd])
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
