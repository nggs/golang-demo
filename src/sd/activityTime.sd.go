// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 活动时间表
type ActivityTime struct {
	id                    int                   // 任务ID
	container             E_ActContainerType    // 活动容器类型 1福利2活动3节日活动4限时活动5月卡基金0表示该活动不在活动容器中，是单独的一个入口（例如貂蝉降临）
	activityType          E_ActivityType        // 活动类型
	timeType              E_TimeType            // 时间类型 1创角多少天2开服多少天3定时多少天4合服多少天5每周第几天
	startTime             int                   // 活动开始时间 1创角第几天 2开服第几天 3日期如20161121表示21号0点开始 4合服第几天
	lastTime              int                   // 活动结束时间 1创角第几天 2开服第几天 3日期如20161122表示22号0点之前结束 4合服第几天
	openLock              int                   // 开服大于等于该值后解锁该活动
	activityName          string                // 底部功能栏中活动名称
	activityIcon          string                // 底部功能栏中活动ICON仅对容器中的活动生效
	banner                string                // 活动banner
	desc                  string                // 活动描述
	reset                 E_ActivityRefreshType // 刷新类型，每日0点刷新 1活动期间不刷新 2每日刷新，每日0点刷新 3每周刷新，每周一0点刷新 4每月刷新，每月1日0点刷新 5奖励全部被领取后重置
	redPoint              bool                  // 当日角色没有进入此活动时，是否显示小红点
	chargeHide            bool                  // 若后台配置屏蔽充值，则屏蔽此活动
	order                 int                   // 底部活动图标的排序次序
	actUI                 E_ActUIType           // 活动UI样式 仅在活动容器中活动需要填写此字段
	failStatementActivity string                // 失败界面的活动banner

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	beginTime time.Time
	endTime   time.Time
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewActivityTime() *ActivityTime {
	sd := &ActivityTime{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 任务ID
func (sd ActivityTime) ID() int {
	return sd.id
}

// 活动容器类型 1福利2活动3节日活动4限时活动5月卡基金0表示该活动不在活动容器中，是单独的一个入口（例如貂蝉降临）
func (sd ActivityTime) Container() E_ActContainerType {
	return sd.container
}

// 活动类型
func (sd ActivityTime) ActivityType() E_ActivityType {
	return sd.activityType
}

// 时间类型 1创角多少天2开服多少天3定时多少天4合服多少天5每周第几天
func (sd ActivityTime) TimeType() E_TimeType {
	return sd.timeType
}

// 活动开始时间 1创角第几天 2开服第几天 3日期如20161121表示21号0点开始 4合服第几天
func (sd ActivityTime) StartTime() int {
	return sd.startTime
}

// 活动结束时间 1创角第几天 2开服第几天 3日期如20161122表示22号0点之前结束 4合服第几天
func (sd ActivityTime) LastTime() int {
	return sd.lastTime
}

// 开服大于等于该值后解锁该活动
func (sd ActivityTime) OpenLock() int {
	return sd.openLock
}

// 底部功能栏中活动名称
func (sd ActivityTime) ActivityName() string {
	return sd.activityName
}

// 底部功能栏中活动ICON仅对容器中的活动生效
func (sd ActivityTime) ActivityIcon() string {
	return sd.activityIcon
}

// 活动banner
func (sd ActivityTime) Banner() string {
	return sd.banner
}

// 活动描述
func (sd ActivityTime) Desc() string {
	return sd.desc
}

// 刷新类型，每日0点刷新 1活动期间不刷新 2每日刷新，每日0点刷新 3每周刷新，每周一0点刷新 4每月刷新，每月1日0点刷新 5奖励全部被领取后重置
func (sd ActivityTime) Reset() E_ActivityRefreshType {
	return sd.reset
}

// 当日角色没有进入此活动时，是否显示小红点
func (sd ActivityTime) RedPoint() bool {
	return sd.redPoint
}

// 若后台配置屏蔽充值，则屏蔽此活动
func (sd ActivityTime) ChargeHide() bool {
	return sd.chargeHide
}

// 底部活动图标的排序次序
func (sd ActivityTime) Order() int {
	return sd.order
}

// 活动UI样式 仅在活动容器中活动需要填写此字段
func (sd ActivityTime) ActUI() E_ActUIType {
	return sd.actUI
}

// 失败界面的活动banner
func (sd ActivityTime) FailStatementActivity() string {
	return sd.failStatementActivity
}

func (sd ActivityTime) Clone() *ActivityTime {
	n := NewActivityTime()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 活动时间表全局属性
type ActivityTimeGlobal struct {
	firstChargeStage             int // 首充浮动窗口出现关卡，关联stage表
	questActivityResetTimesLimit int // 重置型任务活动的重置次数限制

}

// 首充浮动窗口出现关卡，关联stage表
func (g ActivityTimeGlobal) FirstChargeStage() int {
	return g.firstChargeStage
}

// 重置型任务活动的重置次数限制
func (g ActivityTimeGlobal) QuestActivityResetTimesLimit() int {
	return g.questActivityResetTimesLimit
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type ActivityTimeManager struct {
	Datas  []*ActivityTime
	Global ActivityTimeGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*ActivityTime // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byActivityType map[E_ActivityType][]*ActivityTime // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	rankActivitySD *ActivityTime
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newActivityTimeManager() *ActivityTimeManager {
	mgr := &ActivityTimeManager{
		Datas:          []*ActivityTime{},
		byID:           map[int]*ActivityTime{},
		byActivityType: map[E_ActivityType][]*ActivityTime{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *ActivityTimeManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byActivityType[d.activityType] = append(mgr.byActivityType[d.activityType], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func ActivityTimeSize() int {
	return activityTimeMgr.size
}

func (mgr ActivityTimeManager) check(path string, row int, sd *ActivityTime) error {
	if _, ok := activityTimeMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *ActivityTimeManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		switch d.activityType {
		case E_ActivityType_OpenRank:
			if mgr.rankActivitySD != nil {
				success = false
				log.Printf("活动时间表配置了多个开服排行榜")
			}
			mgr.rankActivitySD = d
		}

		if d.timeType == E_TimeType_Date {
			beginTime, err := nutil.ParseTimeStringInLocationByFormat("20060102", strconv.Itoa(d.startTime))
			if err != nil {
				success = false
				log.Printf("活动时间表活动[%d]开始时间[%d]配置有误", d.id, d.startTime)
				continue
			}
			endTime, err := nutil.ParseTimeStringInLocationByFormat("20060102", strconv.Itoa(d.lastTime))
			if err != nil {
				success = false
				log.Printf("活动时间表活动[%d]结束时间[%d]配置有误", d.id, d.lastTime)
				continue
			}
			if endTime.Before(beginTime) {
				success = false
				log.Printf("活动时间表活动[%d]结束时间[%d]在开始时间[%d]之前配置有误", d.id, d.lastTime, d.startTime)
			}
			d.beginTime = beginTime
			d.endTime = endTime
		}
	}

	//ds := mgr.byActivityType[E_ActivityType_Exchange]
	//for _, d := range ds {
	//	if d.timeType != E_TimeType_Date {
	//		continue
	//	}
	//	for _, od := range ds {
	//		if od.id == d.id {
	//			continue
	//		}
	//		if od.timeType != E_TimeType_Date {
	//			continue
	//		}
	//		if (d.beginTime.After(od.beginTime) && d.beginTime.Before(od.endTime)) ||
	//			(d.endTime.After(od.beginTime) && d.endTime.Before(od.endTime)) {
	//			success = false
	//			log.Printf("活动时间表限时兑换活动[%d]和[%d]时间有重合", d.id, od.id)
	//		}
	//	}
	//}

	//ds := mgr.byActivityType[E_ActivityType_LimitLogin]
	//for _, d := range ds {
	//	for _, od := range ds {
	//		if od.id == d.id {
	//			continue
	//		}
	//		if (d.beginTime.After(od.beginTime) && d.beginTime.Before(od.endTime)) ||
	//			(d.endTime.After(od.beginTime) && d.endTime.Before(od.endTime)) {
	//			success = false
	//			log.Printf("活动时间表限时登录活动[%d]和[%d]时间有重合", d.id, od.id)
	//		}
	//	}
	//}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *ActivityTimeManager) each(f func(sd *ActivityTime) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *ActivityTimeManager) findIf(f func(sd *ActivityTime) (find bool)) *ActivityTime {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachActivityTime(f func(sd ActivityTime) (continued bool)) {
	for _, sd := range activityTimeMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindActivityTimeIf(f func(sd ActivityTime) bool) (ActivityTime, bool) {
	for _, sd := range activityTimeMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilActivityTime, false
}

func GetActivityTimeByID(id int) (ActivityTime, bool) {
	temp, ok := activityTimeMgr.byID[id]
	if !ok {
		return nilActivityTime, false
	}
	return *temp, true
}

func GetActivityTimeByActivityType(activityType E_ActivityType) (vs []ActivityTime, ok bool) {
	var ds []*ActivityTime
	ds, ok = activityTimeMgr.byActivityType[activityType]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachActivityTimeByActivityType(activityType E_ActivityType, fn func(d ActivityTime) (continued bool)) bool {
	ds, ok := activityTimeMgr.byActivityType[activityType]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetActivityTimeGlobal() ActivityTimeGlobal {
	return activityTimeMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd ActivityTime) BeginTime() time.Time {
	return sd.beginTime
}

func (sd *ActivityTime) SetBeginTime(t time.Time) {
	sd.beginTime = t
}

func (sd ActivityTime) EndTime() time.Time {
	return sd.endTime
}

func (sd *ActivityTime) SetEndTime(t time.Time) {
	sd.endTime = t
}

func GetRankActivitySD() (ActivityTime, bool) {
	if activityTimeMgr.rankActivitySD == nil {
		return nilActivityTime, false
	}
	return *activityTimeMgr.rankActivitySD, true
}

//func GetActiveLimitTimeExchangeSD(t time.Time) (ActivityTime, bool) {
//	for _, d := range activityTimeMgr.byActivityType[E_ActivityType_Exchange] {
//		if t.After(d.beginTime) && t.Before(d.endTime) {
//			return *d, true
//		}
//	}
//	return nilActivityTime, false
//}

//func GetActiveLimitTimeLoginSD(t time.Time) (ActivityTime, bool) {
//	for _, d := range activityTimeMgr.byActivityType[E_ActivityType_LimitLogin] {
//		if t.After(d.beginTime) && t.Before(d.endTime) {
//			return *d, true
//		}
//	}
//	return nilActivityTime, false
//}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
