// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 关卡表
type Stage struct {
	id                 int     // 主键ID
	chapterId          int     // 归属章节 关联stage_chapte表
	order              int     // 关卡次序
	tYpE               int     // 0=普通关卡 1=BOSS关卡
	name               string  // 关卡名称
	sceneId            int     // 战斗1，关联scene表
	sceneId2           int     // 战斗2，关联scene表
	sceneId3           int     // 战斗3，关联scene表
	keepTime           int     // 完成时间限制/秒
	silverCoin         int     // 关卡通关奖励银币
	heroExp            int     // 关卡通关奖励武将经验
	roleExp            int     // 关卡通关奖励角色经验
	reward             int     // 关卡通关奖励物品（除前面的3个的其他物品），关联reward
	idleSilverCoin     int     // 挂机银币收益 必须填大于0的数字
	idleHeroExp        int     // 挂机英雄经验收益 必须填大于0的数字
	idlePlayExp        int     // 挂机角色经验收益 必须填大于0的数字
	idleLimitPassPill  float64 // 挂机武将突破丹（英雄粉尘）收益 必须填大于0的数字
	idleHeroMasterFlag float64 // 挂机御将旗（生命精华）收益
	idleRandReward     int     // 挂机随机收益，关联reward
	specialReward      int     // 累积特殊奖励，对应reward
	specialRewardTime  int     // 累积特殊奖励获取需要经历的时间，单位为秒
	limitGiftId        int     // 通关此关卡后是否触发限时礼包活动 填写活动ID关联activity
	funcNotice         int     // 功能开启预告 关联funcTrailNotice中ID

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	shopID    int           // 商店id，对应shop.xlsx分页shop的id
	chapterSD *StageChapter // 对应的章节配置
	fixReward *Reward
	sceneSDs  map[int]*Scene
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewStage() *Stage {
	sd := &Stage{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>
	sd.sceneSDs = map[int]*Scene{}
	//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 主键ID
func (sd Stage) ID() int {
	return sd.id
}

// 归属章节 关联stage_chapte表
func (sd Stage) ChapterID() int {
	return sd.chapterId
}

// 关卡次序
func (sd Stage) Order() int {
	return sd.order
}

// 0=普通关卡 1=BOSS关卡
func (sd Stage) Type() int {
	return sd.tYpE
}

// 关卡名称
func (sd Stage) Name() string {
	return sd.name
}

// 战斗1，关联scene表
func (sd Stage) SceneID() int {
	return sd.sceneId
}

// 战斗2，关联scene表
func (sd Stage) SceneID2() int {
	return sd.sceneId2
}

// 战斗3，关联scene表
func (sd Stage) SceneID3() int {
	return sd.sceneId3
}

// 完成时间限制/秒
func (sd Stage) KeepTime() int {
	return sd.keepTime
}

// 关卡通关奖励银币
func (sd Stage) SilverCoin() int {
	return sd.silverCoin
}

// 关卡通关奖励武将经验
func (sd Stage) HeroExp() int {
	return sd.heroExp
}

// 关卡通关奖励角色经验
func (sd Stage) RoleExp() int {
	return sd.roleExp
}

// 关卡通关奖励物品（除前面的3个的其他物品），关联reward
func (sd Stage) Reward() int {
	return sd.reward
}

// 挂机银币收益 必须填大于0的数字
func (sd Stage) IdleSilverCoin() int {
	return sd.idleSilverCoin
}

// 挂机英雄经验收益 必须填大于0的数字
func (sd Stage) IdleHeroExp() int {
	return sd.idleHeroExp
}

// 挂机角色经验收益 必须填大于0的数字
func (sd Stage) IdlePlayExp() int {
	return sd.idlePlayExp
}

// 挂机武将突破丹（英雄粉尘）收益 必须填大于0的数字
func (sd Stage) IdleLimitPassPill() float64 {
	return sd.idleLimitPassPill
}

// 挂机御将旗（生命精华）收益
func (sd Stage) IdleHeroMasterFlag() float64 {
	return sd.idleHeroMasterFlag
}

// 挂机随机收益，关联reward
func (sd Stage) IdleRandReward() int {
	return sd.idleRandReward
}

// 累积特殊奖励，对应reward
func (sd Stage) SpecialReward() int {
	return sd.specialReward
}

// 累积特殊奖励获取需要经历的时间，单位为秒
func (sd Stage) SpecialRewardTime() int {
	return sd.specialRewardTime
}

// 通关此关卡后是否触发限时礼包活动 填写活动ID关联activity
func (sd Stage) LimitGiftID() int {
	return sd.limitGiftId
}

// 功能开启预告 关联funcTrailNotice中ID
func (sd Stage) FuncNotice() int {
	return sd.funcNotice
}

func (sd Stage) Clone() *Stage {
	n := NewStage()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 关卡表全局属性
type StageGlobal struct {
	idleFixReward      int     // 固定奖励发放时间间隔/秒
	idleRandomReward   int     // 随机奖励发放时间间隔/秒
	idleFastTime       int     // 快速挂机的指定时间
	idleMaxTime        int     // 最长挂机收益时间
	specialRewardValue float64 // 累积特殊奖励在通关时获取需要乘以的参数
	moonlightBoxTime   int     // 月光宝盒X小时的挂机收益/秒

}

// 固定奖励发放时间间隔/秒
func (g StageGlobal) IdleFixReward() int {
	return g.idleFixReward
}

// 随机奖励发放时间间隔/秒
func (g StageGlobal) IdleRandomReward() int {
	return g.idleRandomReward
}

// 快速挂机的指定时间
func (g StageGlobal) IdleFastTime() int {
	return g.idleFastTime
}

// 最长挂机收益时间
func (g StageGlobal) IdleMaxTime() int {
	return g.idleMaxTime
}

// 累积特殊奖励在通关时获取需要乘以的参数
func (g StageGlobal) SpecialRewardValue() float64 {
	return g.specialRewardValue
}

// 月光宝盒X小时的挂机收益/秒
func (g StageGlobal) MoonlightBoxTime() int {
	return g.moonlightBoxTime
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type StageManager struct {
	Datas  []*Stage
	Global StageGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID    map[int]*Stage // UniqueIndex
	byOrder map[int]*Stage // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byChapterID map[int][]*Stage // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	first     *Stage
	lastOrder int // 最后一关序列
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newStageManager() *StageManager {
	mgr := &StageManager{
		Datas: []*Stage{},
		byID:  map[int]*Stage{}, byOrder: map[int]*Stage{},
		byChapterID: map[int][]*Stage{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *StageManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>
		if mgr.lastOrder < d.order {
			mgr.lastOrder = d.order
		}
		if d.silverCoin > 0 {
			d.fixReward = AppendOneReward(d.fixReward, E_ItemBigType_Currency, int(E_CurrencyType_SilverCoin), d.silverCoin)
		}
		if d.heroExp > 0 {
			d.fixReward = AppendOneReward(d.fixReward, E_ItemBigType_Currency, int(E_CurrencyType_HeroExp), d.heroExp)
		}
		if d.roleExp > 0 {
			d.fixReward = AppendOneReward(d.fixReward, E_ItemBigType_Currency, int(E_CurrencyType_RoleExp), d.roleExp)
		}
		//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d
		mgr.byOrder[d.order] = d

		mgr.byChapterID[d.chapterId] = append(mgr.byChapterID[d.chapterId], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func StageSize() int {
	return stageMgr.size
}

func (mgr StageManager) check(path string, row int, sd *Stage) error {
	if _, ok := stageMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}
	if _, ok := stageMgr.byOrder[sd.order]; ok {
		return fmt.Errorf("[%s]第[%d]行的order[%v]重复", path, row, sd.order)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *StageManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	heroMasterFuncOpenSD, ok := GetFuncOpenByID(E_FunctionType_HeroShare)
	if !ok {
		success = false
		log.Printf("找不到御将台功能开启配置")
	}
	for i, d := range mgr.Datas {
		if d.order == 1 {
			mgr.first = d
		}
		if d.chapterSD, ok = stageChapterMgr.byID[d.chapterId]; !ok || d.chapterSD == nil {
			success = false
			log.Printf("关卡表关卡[%d]找不到对应的章节，章节id=[%d]\n", d.id, d.chapterId)
		}
		if _, ok := GetStageByOrder(d.order + 1); !ok {
			if i != mgr.size-1 {
				success = false
				log.Printf("关卡表关卡[%d]找不到下一个关卡数据，章节id=[%d]\n", d.id, d.chapterId)
			}
		}

		// 检查挂机固定收益相关字段
		if d.idleSilverCoin <= 0 {
			success = false
			log.Printf("关卡表关卡[%d]的挂机银币收益错误[%d]", d.id, d.idleSilverCoin)
		}
		if d.idleHeroExp <= 0 {
			success = false
			log.Printf("关卡表关卡[%d]的挂机英雄经验收益错误[%d]", d.id, d.idleHeroExp)
		}
		if d.idleLimitPassPill <= 0.0 {
			success = false
			log.Printf("关卡表关卡[%d]的挂机武将突破丹收益错误[%f]", d.id, d.idleLimitPassPill)
		}
		if d.idleHeroMasterFlag < 0.0 {
			success = false
			log.Printf("关卡表关卡[%d]的挂机御将旗收益错误[%f]", d.id, d.idleHeroMasterFlag)
		} else if d.idleHeroMasterFlag == 0 {
			if d.id > heroMasterFuncOpenSD.stageId {
				success = false
				log.Printf("关卡表关卡[%d]的挂机御将旗收益错误[%f]", d.id, d.idleHeroMasterFlag)
			}
		}

		if _, ok := GetLootByGroup(d.idleRandReward); !ok {
			success = false
			log.Printf("关卡表关卡[%d]找不到挂机随机奖励配置[%d]\n", d.id, d.idleRandReward)
		}

		if d.limitGiftId > 0 {
			if _, ok := GetActivityByID(d.limitGiftId); !ok {
				success = false
				log.Printf("关卡表关卡[%d]找不到限时礼包配置[%d]\n", d.id, d.limitGiftId)
			}
		}

		d.shopID = shopMgr.firstShopID
		EachShop(func(shopSD Shop) (continued bool) {
			if d.id >= shopSD.stageId {
				d.shopID = shopSD.stageId
			}
			return true
		})

		if d.specialReward > 0 {
			if _, ok := GetLootByGroup(d.specialReward); !ok {
				success = false
				log.Printf("关卡表关卡[%d]找不到通关特殊奖励配置[%d]\n", d.id, d.specialReward)
			}

			if d.specialRewardTime <= 0 {
				success = false
				log.Printf("关卡表关卡[%d]累积特殊奖励获取需要经历的时间[%d]有误\n", d.id, d.specialRewardTime)
			}
		}

		if d.sceneSDs == nil {
			d.sceneSDs = map[int]*Scene{}
		}

		//if d.sceneId > 0 {
		sceneSD, ok := GetSceneByID(d.sceneId)
		if !ok {
			success = false
			log.Printf("关卡表关卡[%d]的战斗场景1[%d]不存在\n", d.id, d.sceneId)
		}
		d.sceneSDs[1] = sceneSD.Clone()
		//}
		if d.sceneId2 > 0 {
			sceneSD, ok := GetSceneByID(d.sceneId2)
			if !ok {
				success = false
				log.Printf("关卡表关卡[%d]的战斗场景2[%d]不存在\n", d.id, d.sceneId2)
			}
			d.sceneSDs[2] = sceneSD.Clone()
		}
		if d.sceneId3 > 0 {
			sceneSD, ok := GetSceneByID(d.sceneId3)
			if !ok {
				success = false
				log.Printf("关卡表关卡[%d]的战斗场景3[%d]不存在\n", d.id, d.sceneId3)
			}
			d.sceneSDs[3] = sceneSD.Clone()
		}
	}
	if mgr.first == nil {
		success = false
		log.Printf("关卡表未找到第一关\n")
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *StageManager) each(f func(sd *Stage) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *StageManager) findIf(f func(sd *Stage) (find bool)) *Stage {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachStage(f func(sd Stage) (continued bool)) {
	for _, sd := range stageMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindStageIf(f func(sd Stage) bool) (Stage, bool) {
	for _, sd := range stageMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilStage, false
}

func GetStageByID(id int) (Stage, bool) {
	temp, ok := stageMgr.byID[id]
	if !ok {
		return nilStage, false
	}
	return *temp, true
}

func GetStageByOrder(order int) (Stage, bool) {
	temp, ok := stageMgr.byOrder[order]
	if !ok {
		return nilStage, false
	}
	return *temp, true
}

func GetStageByChapterID(chapterId int) (vs []Stage, ok bool) {
	var ds []*Stage
	ds, ok = stageMgr.byChapterID[chapterId]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachStageByChapterID(chapterId int, fn func(d Stage) (continued bool)) bool {
	ds, ok := stageMgr.byChapterID[chapterId]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetStageGlobal() StageGlobal {
	return stageMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd Stage) ShopID() int {
	return sd.shopID
}

func (sd Stage) ChapterSD() StageChapter {
	return *sd.chapterSD
}

func (sd Stage) FixReward() *Reward {
	if sd.fixReward != nil {
		return sd.fixReward.Clone()
	}
	return nil
}

func (sd Stage) EachBattle(fn func(num int, sceneSD Scene) (continued bool)) {
	for i, sceneSD := range sd.sceneSDs {
		if !fn(i, *(sceneSD.Clone())) {
			break
		}
	}
}

func StageLastOrder() int {
	return stageMgr.lastOrder
}

func GetFirstStage() Stage {
	return *stageMgr.first
}

func IsFunctionOpen(id E_FunctionType, curStageID int32) (opened bool, err error) {
	funcOpenSD, ok := GetFuncOpenByID(id)
	if !ok {
		err = fmt.Errorf("get func open static data fail, id=[%d]", id)
		return
	}
	if int(curStageID) > funcOpenSD.StageID() {
		opened = true
		return
	}
	return
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
