// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 商店组表
type ShopThing struct {
	id       int            // ID
	itemName string         // 物品名称
	groupId  int            // 商品组ID
	num      int            // 数量
	dataFrom E_ItemBigType  // 掉落取哪张表 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
	itemId   int            // 物品ID
	currency E_CurrencyType // 消耗货币类型，关联货币表
	costNum  int            // 消耗货币数量 此价格为折扣后价格
	discount float64        // 折扣 1没有折扣
	mustDrop bool           // 是否必掉
	weight   int            // 掉落的权重

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	cost   *Cost
	reward *Reward
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewShopThing() *ShopThing {
	sd := &ShopThing{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// ID
func (sd ShopThing) ID() int {
	return sd.id
}

// 物品名称
func (sd ShopThing) ItemName() string {
	return sd.itemName
}

// 商品组ID
func (sd ShopThing) GroupID() int {
	return sd.groupId
}

// 数量
func (sd ShopThing) Num() int {
	return sd.num
}

// 掉落取哪张表 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
func (sd ShopThing) DataFrom() E_ItemBigType {
	return sd.dataFrom
}

// 物品ID
func (sd ShopThing) ItemID() int {
	return sd.itemId
}

// 消耗货币类型，关联货币表
func (sd ShopThing) Currency() E_CurrencyType {
	return sd.currency
}

// 消耗货币数量 此价格为折扣后价格
func (sd ShopThing) CostNum() int {
	return sd.costNum
}

// 折扣 1没有折扣
func (sd ShopThing) Discount() float64 {
	return sd.discount
}

// 是否必掉
func (sd ShopThing) MustDrop() bool {
	return sd.mustDrop
}

// 掉落的权重
func (sd ShopThing) Weight() int {
	return sd.weight
}

func (sd ShopThing) Clone() *ShopThing {
	n := NewShopThing()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 商店组表全局属性
type ShopThingGlobal struct {
	shopType                     []string // 商店类型
	limitTimes                   int      // 全部商店物品的限购次数
	commonShopRefresh            int      // 普通商店刷新时间，关联limit
	guildShopRefresh             int      // 公会商店刷新时间，关联limit
	dismissShopRefresh           int      // 遣散商店刷新时间，关联limit
	daoistMagicShopRefresh       int      // 迷宫商店刷新时间，关联limit
	arenaShopRefresh             int      // 竞技场商店刷新时间，关联limit
	legendArenaShopRefresh       int      // 高阶竞技场商店刷新时间，关联limit
	warcraftPointShopRefresh     int      // 兵书积分商城刷新时间，关联limit
	battlePointsShopRefresh      int      // 战功商店刷新时间，关联limit
	heroChallengeShopRefresh     int      // 过关斩将商店刷新时间，关联limit
	commonShopRefreshCost        int      // 普通商店手动刷新消耗元宝数量
	guildShopRefreshCost         int      // 公会商店手动刷新消耗元宝数量
	dismissShopRefreshCost       int      // 遣散商店手动刷新消耗元宝数量
	daoistMagicShopRefreshCost   int      // 迷宫商店手动刷新消耗元宝数量
	legendArenaShopRefreshCost   int      // 高阶竞技场商店手动刷新消耗元宝数量
	aenaShopRefreshCost          int      // 竞技场商店手动刷新消耗元宝数量
	warcraftPointShopRefreshCost int      // 兵书积分商城刷新消耗元宝数量
	battlePointsShopRefreshCost  int      // 战功商店刷新消耗元宝数量
	heroChallengeShopRefreshCost int      // 过关斩将商店刷新消耗元宝数量

}

// 商店类型
func (g ShopThingGlobal) ShopType() []string {
	return g.shopType
}

// 全部商店物品的限购次数
func (g ShopThingGlobal) LimitTimes() int {
	return g.limitTimes
}

// 普通商店刷新时间，关联limit
func (g ShopThingGlobal) CommonShopRefresh() int {
	return g.commonShopRefresh
}

// 公会商店刷新时间，关联limit
func (g ShopThingGlobal) GuildShopRefresh() int {
	return g.guildShopRefresh
}

// 遣散商店刷新时间，关联limit
func (g ShopThingGlobal) DismissShopRefresh() int {
	return g.dismissShopRefresh
}

// 迷宫商店刷新时间，关联limit
func (g ShopThingGlobal) DaoistMagicShopRefresh() int {
	return g.daoistMagicShopRefresh
}

// 竞技场商店刷新时间，关联limit
func (g ShopThingGlobal) ArenaShopRefresh() int {
	return g.arenaShopRefresh
}

// 高阶竞技场商店刷新时间，关联limit
func (g ShopThingGlobal) LegendArenaShopRefresh() int {
	return g.legendArenaShopRefresh
}

// 兵书积分商城刷新时间，关联limit
func (g ShopThingGlobal) WarcraftPointShopRefresh() int {
	return g.warcraftPointShopRefresh
}

// 战功商店刷新时间，关联limit
func (g ShopThingGlobal) BattlePointsShopRefresh() int {
	return g.battlePointsShopRefresh
}

// 过关斩将商店刷新时间，关联limit
func (g ShopThingGlobal) HeroChallengeShopRefresh() int {
	return g.heroChallengeShopRefresh
}

// 普通商店手动刷新消耗元宝数量
func (g ShopThingGlobal) CommonShopRefreshCost() int {
	return g.commonShopRefreshCost
}

// 公会商店手动刷新消耗元宝数量
func (g ShopThingGlobal) GuildShopRefreshCost() int {
	return g.guildShopRefreshCost
}

// 遣散商店手动刷新消耗元宝数量
func (g ShopThingGlobal) DismissShopRefreshCost() int {
	return g.dismissShopRefreshCost
}

// 迷宫商店手动刷新消耗元宝数量
func (g ShopThingGlobal) DaoistMagicShopRefreshCost() int {
	return g.daoistMagicShopRefreshCost
}

// 高阶竞技场商店手动刷新消耗元宝数量
func (g ShopThingGlobal) LegendArenaShopRefreshCost() int {
	return g.legendArenaShopRefreshCost
}

// 竞技场商店手动刷新消耗元宝数量
func (g ShopThingGlobal) AenaShopRefreshCost() int {
	return g.aenaShopRefreshCost
}

// 兵书积分商城刷新消耗元宝数量
func (g ShopThingGlobal) WarcraftPointShopRefreshCost() int {
	return g.warcraftPointShopRefreshCost
}

// 战功商店刷新消耗元宝数量
func (g ShopThingGlobal) BattlePointsShopRefreshCost() int {
	return g.battlePointsShopRefreshCost
}

// 过关斩将商店刷新消耗元宝数量
func (g ShopThingGlobal) HeroChallengeShopRefreshCost() int {
	return g.heroChallengeShopRefreshCost
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type ShopThingManager struct {
	Datas  []*ShopThing
	Global ShopThingGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*ShopThing // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byGroupID map[int][]*ShopThing // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	dices    map[int]*ShopThingDice
	contexts map[E_ShopType]*ShopContext
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newShopThingManager() *ShopThingManager {
	mgr := &ShopThingManager{
		Datas:     []*ShopThing{},
		byID:      map[int]*ShopThing{},
		byGroupID: map[int][]*ShopThing{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>
	mgr.dices = map[int]*ShopThingDice{}
	mgr.contexts = map[E_ShopType]*ShopContext{}
	//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *ShopThingManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>
		d.cost = AppendOneCost(d.cost, E_ItemBigType_Currency, int(d.currency), d.costNum)
		d.reward = AppendOneReward(d.reward, d.dataFrom, d.itemId, d.num)
		//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byGroupID[d.groupId] = append(mgr.byGroupID[d.groupId], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>
	for groupID, group := range mgr.byGroupID {
		mgr.dices[groupID] = NewShopThingDice(groupID, group)
	}
	//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func ShopThingSize() int {
	return shopThingMgr.size
}

func (mgr ShopThingManager) check(path string, row int, sd *ShopThing) error {
	if _, ok := shopThingMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *ShopThingManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if d.num <= 0 {
			success = false
			log.Printf("商品表商品[%d]的数量[%d]错误\n", d.id, d.num)
		}
		if !d.mustDrop && d.weight <= 0 {
			success = false
			log.Printf("商品表商品[%d]的权重[%d]错误\n", d.id, d.weight)
		}
		switch E_ItemBigType(d.dataFrom) {
		case E_ItemBigType_Currency:
			if _, ok := GetCurrencyByID(d.itemId); !ok {
				success = false
				log.Printf("商品表商品[%d]的货币[%d]不存在\n", d.id, d.itemId)
			}
		case E_ItemBigType_Item:
			if _, ok := GetItemByID(d.itemId); !ok {
				success = false
				log.Printf("商品表商品[%d]的物品[%d]不存在\n", d.id, d.itemId)
			}
		case E_ItemBigType_Hero:
			if _, ok := GetHeroBasicByID(d.itemId); !ok {
				success = false
				log.Printf("商品表商品[%d]的英雄[%d]不存在\n", d.id, d.itemId)
			}
		case E_ItemBigType_Equip:
			if _, ok := GetEquipmentByID(d.itemId); !ok {
				success = false
				log.Printf("商品表商品[%d]的装备[%d]不存在\n", d.id, d.itemId)
			}
		case E_ItemBigType_Warcraft:
			if _, ok := GetWarcraftByID(d.itemId); !ok {
				success = false
				log.Printf("商品表商品[%d]的兵书[%d]不存在\n", d.id, d.itemId)
			}
		default:
			success = false
			log.Printf("商品表商品[%d]的道具大类[%d]错误\n", d.id, d.dataFrom)
		}
		if !Check_E_CurrencyType(d.currency) {
			success = false
			log.Printf("商品表商品[%d]的消耗货币类型[%d]错误\n", d.id, d.currency)
		}
		if d.costNum <= 0 {
			success = false
			log.Printf("商品表商品[%d]的消耗货币数量[%d]错误\n", d.id, d.costNum)
		}
	}

	if mgr.Global.commonShopRefreshCost <= 0 {
		success = false
		log.Printf("商品表普通商店刷新消耗货币数量错误")
	}
	if _, ok := GetLimitByID(mgr.Global.commonShopRefresh); !ok {
		success = false
		log.Printf("商品表普通商店刷新限制数据未找到")
	}
	openFunctionSD, ok := GetFuncOpenByID(E_FunctionType_CommonShop)
	if !ok {
		success = false
		log.Printf("未找到普通商店的功能开启配置")
	} else {
		if _, ok := GetStageByID(openFunctionSD.stageId); !ok {
			success = false
			log.Printf("未找到普通商店功能开启关卡[%d]的配置", openFunctionSD.stageId)
		}
	}
	mgr.contexts[E_ShopType_CommonShop] = &ShopContext{
		Type:               E_ShopType_CommonShop,
		FunctionType:       E_FunctionType_CommonShop,
		OpenStageID:        openFunctionSD.stageId,
		RefreshLimitID:     mgr.Global.commonShopRefresh,
		RefreshGoldCoinNum: mgr.Global.commonShopRefreshCost,
	}

	if mgr.Global.guildShopRefreshCost <= 0 {
		success = false
		log.Printf("商品表公会商店刷新消耗货币数量错误")
	}
	if _, ok := GetLimitByID(mgr.Global.guildShopRefresh); !ok {
		success = false
		log.Printf("商品表公会商店刷新限制数据未找到")
	}
	openFunctionSD, ok = GetFuncOpenByID(E_FunctionType_GuildShop)
	if !ok {
		success = false
		log.Printf("未找到公会商店的功能开启配置")
	} else {
		if _, ok := GetStageByID(openFunctionSD.stageId); !ok {
			success = false
			log.Printf("未找到公会商店功能开启关卡[%d]的配置", openFunctionSD.stageId)
		}
	}
	mgr.contexts[E_ShopType_GuildShop] = &ShopContext{
		Type:               E_ShopType_GuildShop,
		FunctionType:       E_FunctionType_GuildShop,
		OpenStageID:        openFunctionSD.stageId,
		RefreshLimitID:     mgr.Global.guildShopRefresh,
		RefreshGoldCoinNum: mgr.Global.guildShopRefreshCost,
	}

	if mgr.Global.dismissShopRefreshCost <= 0 {
		success = false
		log.Printf("商品表遣散商店刷新消耗货币数量错误")
	}
	if _, ok := GetLimitByID(mgr.Global.dismissShopRefresh); !ok {
		success = false
		log.Printf("商品表遣散商店刷新限制数据未找到")
	}
	openFunctionSD, ok = GetFuncOpenByID(E_FunctionType_DismissShop)
	if !ok {
		success = false
		log.Printf("未找到遣散商店的功能开启配置")
	} else {
		if _, ok := GetStageByID(openFunctionSD.stageId); !ok {
			success = false
			log.Printf("未找到遣散商店功能开启关卡[%d]的配置", openFunctionSD.stageId)
		}
	}
	mgr.contexts[E_ShopType_DismissShop] = &ShopContext{
		Type:               E_ShopType_DismissShop,
		FunctionType:       E_FunctionType_DismissShop,
		OpenStageID:        openFunctionSD.stageId,
		RefreshLimitID:     mgr.Global.dismissShopRefresh,
		RefreshGoldCoinNum: mgr.Global.dismissShopRefreshCost,
	}

	if mgr.Global.daoistMagicShopRefreshCost <= 0 {
		success = false
		log.Printf("商品表迷宫商店刷新消耗货币数量错误")
	}
	if _, ok := GetLimitByID(mgr.Global.daoistMagicShopRefresh); !ok {
		success = false
		log.Printf("商品表迷宫商店刷新限制数据未找到")
	}
	openFunctionSD, ok = GetFuncOpenByID(E_FunctionType_DaoistMagic)
	if !ok {
		success = false
		log.Printf("未找到迷宫商店的功能开启配置")
	} else {
		if _, ok := GetStageByID(openFunctionSD.stageId); !ok {
			success = false
			log.Printf("未找到迷宫商店功能开启关卡[%d]的配置", openFunctionSD.stageId)
		}
	}
	mgr.contexts[E_ShopType_DaoistMagicShop] = &ShopContext{
		Type:               E_ShopType_DaoistMagicShop,
		FunctionType:       E_FunctionType_DaoistMagic,
		OpenStageID:        openFunctionSD.stageId,
		RefreshLimitID:     mgr.Global.daoistMagicShopRefresh,
		RefreshGoldCoinNum: mgr.Global.daoistMagicShopRefreshCost,
	}

	if mgr.Global.legendArenaShopRefreshCost <= 0 {
		success = false
		log.Printf("商品表高阶竞技场商店刷新消耗货币数量错误")
	}
	if _, ok := GetLimitByID(mgr.Global.legendArenaShopRefresh); !ok {
		success = false
		log.Printf("商品表高阶竞技场商店刷新限制数据未找到")
	}
	openFunctionSD, ok = GetFuncOpenByID(E_FunctionType_LegendArenaShop)
	if !ok {
		success = false
		log.Printf("未找到高阶竞技场商店的功能开启配置")
	} else {
		if _, ok := GetStageByID(openFunctionSD.stageId); !ok {
			success = false
			log.Printf("未找到高阶竞技场商店功能开启关卡[%d]的配置", openFunctionSD.stageId)
		}
	}
	mgr.contexts[E_ShopType_LegendArenaShop] = &ShopContext{
		Type:               E_ShopType_LegendArenaShop,
		FunctionType:       E_FunctionType_LegendArenaShop,
		OpenStageID:        openFunctionSD.stageId,
		RefreshLimitID:     mgr.Global.legendArenaShopRefresh,
		RefreshGoldCoinNum: mgr.Global.legendArenaShopRefreshCost,
	}

	if mgr.Global.aenaShopRefreshCost <= 0 {
		success = false
		log.Printf("商品表竞技场商店刷新消耗货币数量错误")
	}
	if _, ok := GetLimitByID(mgr.Global.arenaShopRefresh); !ok {
		success = false
		log.Printf("商品表竞技场商店刷新限制数据未找到")
	}
	openFunctionSD, ok = GetFuncOpenByID(E_FunctionType_ArenaShop)
	if !ok {
		success = false
		log.Printf("未找到竞技场商店的功能开启配置")
	} else {
		if _, ok := GetStageByID(openFunctionSD.stageId); !ok {
			success = false
			log.Printf("未找到竞技场商店功能开启关卡[%d]的配置", openFunctionSD.stageId)
		}
	}
	mgr.contexts[E_ShopType_ArenaShop] = &ShopContext{
		Type:               E_ShopType_ArenaShop,
		FunctionType:       E_FunctionType_ArenaShop,
		OpenStageID:        openFunctionSD.stageId,
		RefreshLimitID:     mgr.Global.arenaShopRefresh,
		RefreshGoldCoinNum: mgr.Global.aenaShopRefreshCost,
	}

	if mgr.Global.warcraftPointShopRefreshCost <= 0 {
		success = false
		log.Printf("商品表兵书积分商店刷新消耗货币数量错误")
	}
	if _, ok := GetLimitByID(mgr.Global.warcraftPointShopRefresh); !ok {
		success = false
		log.Printf("商品表兵书积分商店刷新限制数据未找到")
	}
	openFunctionSD, ok = GetFuncOpenByID(E_FunctionType_WarcraftPointShop)
	if !ok {
		success = false
		log.Printf("未找到兵书积分商店的功能开启配置")
	} else {
		if _, ok := GetStageByID(openFunctionSD.stageId); !ok {
			success = false
			log.Printf("未找到兵书积分商店功能开启关卡[%d]的配置", openFunctionSD.stageId)
		}
	}
	mgr.contexts[E_ShopType_WarcraftPointShop] = &ShopContext{
		Type:               E_ShopType_WarcraftPointShop,
		FunctionType:       E_FunctionType_WarcraftPointShop,
		OpenStageID:        openFunctionSD.stageId,
		RefreshLimitID:     mgr.Global.warcraftPointShopRefresh,
		RefreshGoldCoinNum: mgr.Global.warcraftPointShopRefreshCost,
	}

	if mgr.Global.battlePointsShopRefreshCost <= 0 {
		success = false
		log.Printf("商品表战功商店刷新消耗货币数量错误")
	}
	if _, ok := GetLimitByID(mgr.Global.battlePointsShopRefresh); !ok {
		success = false
		log.Printf("商品表战功商店刷新限制数据未找到")
	}
	openFunctionSD, ok = GetFuncOpenByID(E_FunctionType_BattlePointsShop)
	if !ok {
		success = false
		log.Printf("未找到战功商店的功能开启配置")
	} else {
		if _, ok := GetStageByID(openFunctionSD.stageId); !ok {
			success = false
			log.Printf("未找到战功商店功能开启关卡[%d]的配置", openFunctionSD.stageId)
		}
	}
	mgr.contexts[E_ShopType_BattlePointsShop] = &ShopContext{
		Type:               E_ShopType_BattlePointsShop,
		FunctionType:       E_FunctionType_BattlePointsShop,
		OpenStageID:        openFunctionSD.stageId,
		RefreshLimitID:     mgr.Global.battlePointsShopRefresh,
		RefreshGoldCoinNum: mgr.Global.battlePointsShopRefreshCost,
	}

	if mgr.Global.heroChallengeShopRefreshCost <= 0 {
		success = false
		log.Printf("商品表过关斩将商店刷新消耗货币数量错误")
	}
	if _, ok := GetLimitByID(mgr.Global.heroChallengeShopRefresh); !ok {
		success = false
		log.Printf("商品表过关斩将商店刷新限制数据未找到")
	}
	openFunctionSD, ok = GetFuncOpenByID(E_FunctionType_HeroChallenge)
	if !ok {
		success = false
		log.Printf("未找到过关斩将商店的功能开启配置")
	} else {
		if _, ok := GetStageByID(openFunctionSD.stageId); !ok {
			success = false
			log.Printf("未找到过关斩将商店功能开启关卡[%d]的配置", openFunctionSD.stageId)
		}
	}
	mgr.contexts[E_ShopType_HeroChallengeShop] = &ShopContext{
		Type:               E_ShopType_HeroChallengeShop,
		FunctionType:       E_FunctionType_HeroChallenge,
		OpenStageID:        openFunctionSD.stageId,
		RefreshLimitID:     mgr.Global.heroChallengeShopRefresh,
		RefreshGoldCoinNum: mgr.Global.heroChallengeShopRefreshCost,
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *ShopThingManager) each(f func(sd *ShopThing) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *ShopThingManager) findIf(f func(sd *ShopThing) (find bool)) *ShopThing {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachShopThing(f func(sd ShopThing) (continued bool)) {
	for _, sd := range shopThingMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindShopThingIf(f func(sd ShopThing) bool) (ShopThing, bool) {
	for _, sd := range shopThingMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilShopThing, false
}

func GetShopThingByID(id int) (ShopThing, bool) {
	temp, ok := shopThingMgr.byID[id]
	if !ok {
		return nilShopThing, false
	}
	return *temp, true
}

func GetShopThingByGroupID(groupId int) (vs []ShopThing, ok bool) {
	var ds []*ShopThing
	ds, ok = shopThingMgr.byGroupID[groupId]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachShopThingByGroupID(groupId int, fn func(d ShopThing) (continued bool)) bool {
	ds, ok := shopThingMgr.byGroupID[groupId]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetShopThingGlobal() ShopThingGlobal {
	return shopThingMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd ShopThing) Cost() *Cost {
	return sd.cost.Clone()
}

func (sd ShopThing) Reward() *Reward {
	return sd.reward.Clone()
}

func GetShopContext(shopType E_ShopType) (context ShopContext, ok bool) {
	var c *ShopContext
	c, ok = shopThingMgr.contexts[shopType]
	if ok {
		return *c, true
	}
	return ShopContext{}, false
}

func EachShopContext(f func(context ShopContext) (continued bool)) {
	for _, context := range shopThingMgr.contexts {
		if !f(*context) {
			break
		}
	}
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
