package sd

import (
	"log"

	nrandom "nggs/random"
)

type EquipFactionCandidate struct {
	Type   E_Faction
	Weight int
}

func NewEquipFactionCandidate(t E_Faction, weight int) *EquipFactionCandidate {
	return &EquipFactionCandidate{
		Type:   t,
		Weight: weight,
	}
}

type EquipFactionDice struct {
	nrandom.IDiscreteDistributionDice
}

func NewEquipFactionDice(candidates []*EquipFactionCandidate) *EquipFactionDice {
	if len(candidates) == 0 {
		log.Panicf("装备种族加成候选项为空")
	}
	dice := &EquipFactionDice{
		IDiscreteDistributionDice: nrandom.NewDiscreteDistributionDice(),
	}
	for _, candidate := range candidates {
		dice.Append(nrandom.NewDiscreteDistributionCandidate(candidate, func(context interface{}) (int, interface{}) {
			return context.(*EquipFactionCandidate).Weight, nil
		}))
	}
	dice.Build()
	return dice
}

func RollEquipFaction() E_Faction {
	ctx, err := equipmentMgr.factionDice.Roll()
	if err != nil {
		log.Panic(err)
	}
	return ctx.(*EquipFactionCandidate).Type
}
