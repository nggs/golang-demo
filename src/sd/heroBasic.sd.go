// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 英雄基础表
type HeroBasic struct {
	id               int       // 英雄ID
	group            int       // 英雄组ID，同一个英雄不同品质为一个组
	name             string    // 英雄名称，用于策划查表时识别英雄
	quality          E_Quality // 品质，关联到品质表 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+1
	hp               int       // 生命
	atk              int       // 攻击
	def              int       // 防御
	crit             float64   // 暴击率
	hit              int       // 命中
	miss             int       // 闪避
	speed            int       // 急速
	recover          int       // 每秒恢复
	physicReduce     float64   // 物理减伤率
	magicReduce      float64   // 魔法减伤率
	lifeLeech        int       // 吸血等级
	hurtEnergy       float64   // 受伤回能
	critDamage       float64   // 暴击伤害加成
	addRecovery      float64   // 技能加成生命恢复量
	buffTimeReduce   float64   // 增益buff的持续时间减少百分比
	debuffTimeReduce float64   // 减益buff的持续时间减少百分比
	allEnergyReduce  float64   // 所有回能增加百分比
	rAnGe            int       // 攻击距离
	attackCooldown   float64   // 攻击间隔（秒）
	enlarge          float64   // 模型缩放
	upgradeTo        int       // 下一级的进化英雄ID
	remark           string    // 备注（用于奖励表编辑，程序无用）

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	maxQuality      E_Quality    // 最高品质
	groupMaxLevel   int          // 英雄组最高等级
	qualityMaxLevel int          // 当前品质最大等级
	faction         E_Faction    // 种族
	heroType        E_HeroType   // 英雄职业类型
	hpRatio         float64      // 生命系数
	atkRatio        float64      // 攻击系数
	defRatio        float64      // 防御系数
	point           int          // 积分
	rarity          E_HeroRarity // 英雄稀有度
	exclusiveId     int          // 专属id
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewHeroBasic() *HeroBasic {
	sd := &HeroBasic{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 英雄ID
func (sd HeroBasic) ID() int {
	return sd.id
}

// 英雄组ID，同一个英雄不同品质为一个组
func (sd HeroBasic) Group() int {
	return sd.group
}

// 英雄名称，用于策划查表时识别英雄
func (sd HeroBasic) Name() string {
	return sd.name
}

// 品质，关联到品质表 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+1
func (sd HeroBasic) Quality() E_Quality {
	return sd.quality
}

// 生命
func (sd HeroBasic) Hp() int {
	return sd.hp
}

// 攻击
func (sd HeroBasic) Atk() int {
	return sd.atk
}

// 防御
func (sd HeroBasic) Def() int {
	return sd.def
}

// 暴击率
func (sd HeroBasic) Crit() float64 {
	return sd.crit
}

// 命中
func (sd HeroBasic) Hit() int {
	return sd.hit
}

// 闪避
func (sd HeroBasic) Miss() int {
	return sd.miss
}

// 急速
func (sd HeroBasic) Speed() int {
	return sd.speed
}

// 每秒恢复
func (sd HeroBasic) Recover() int {
	return sd.recover
}

// 物理减伤率
func (sd HeroBasic) PhysicReduce() float64 {
	return sd.physicReduce
}

// 魔法减伤率
func (sd HeroBasic) MagicReduce() float64 {
	return sd.magicReduce
}

// 吸血等级
func (sd HeroBasic) LifeLeech() int {
	return sd.lifeLeech
}

// 受伤回能
func (sd HeroBasic) HurtEnergy() float64 {
	return sd.hurtEnergy
}

// 暴击伤害加成
func (sd HeroBasic) CritDamage() float64 {
	return sd.critDamage
}

// 技能加成生命恢复量
func (sd HeroBasic) AddRecovery() float64 {
	return sd.addRecovery
}

// 增益buff的持续时间减少百分比
func (sd HeroBasic) BuffTimeReduce() float64 {
	return sd.buffTimeReduce
}

// 减益buff的持续时间减少百分比
func (sd HeroBasic) DebuffTimeReduce() float64 {
	return sd.debuffTimeReduce
}

// 所有回能增加百分比
func (sd HeroBasic) AllEnergyReduce() float64 {
	return sd.allEnergyReduce
}

// 攻击距离
func (sd HeroBasic) Range() int {
	return sd.rAnGe
}

// 攻击间隔（秒）
func (sd HeroBasic) AttackCooldown() float64 {
	return sd.attackCooldown
}

// 模型缩放
func (sd HeroBasic) Enlarge() float64 {
	return sd.enlarge
}

// 下一级的进化英雄ID
func (sd HeroBasic) UpgradeTo() int {
	return sd.upgradeTo
}

// 备注（用于奖励表编辑，程序无用）
func (sd HeroBasic) Remark() string {
	return sd.remark
}

func (sd HeroBasic) Clone() *HeroBasic {
	n := NewHeroBasic()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 英雄基础表全局属性
type HeroBasicGlobal struct {
	energyRecoverSecond    int     // 英雄每秒回复能量值
	energyRecoverAttack    int     // 英雄攻击回复能量值
	energyRecoverKillEnemy int     // 击杀敌军回复能量值
	energyRecoverHit       int     // 被攻击恢复能量值基数
	racialRestraint        float64 // 种族克制
	initEnergy             int     // 初始能量

}

// 英雄每秒回复能量值
func (g HeroBasicGlobal) EnergyRecoverSecond() int {
	return g.energyRecoverSecond
}

// 英雄攻击回复能量值
func (g HeroBasicGlobal) EnergyRecoverAttack() int {
	return g.energyRecoverAttack
}

// 击杀敌军回复能量值
func (g HeroBasicGlobal) EnergyRecoverKillEnemy() int {
	return g.energyRecoverKillEnemy
}

// 被攻击恢复能量值基数
func (g HeroBasicGlobal) EnergyRecoverHit() int {
	return g.energyRecoverHit
}

// 种族克制
func (g HeroBasicGlobal) RacialRestraint() float64 {
	return g.racialRestraint
}

// 初始能量
func (g HeroBasicGlobal) InitEnergy() int {
	return g.initEnergy
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type HeroBasicManager struct {
	Datas  []*HeroBasic
	Global HeroBasicGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*HeroBasic // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byGroup   map[int][]*HeroBasic       // NormalIndex
	byQuality map[E_Quality][]*HeroBasic // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newHeroBasicManager() *HeroBasicManager {
	mgr := &HeroBasicManager{
		Datas:   []*HeroBasic{},
		byID:    map[int]*HeroBasic{},
		byGroup: map[int][]*HeroBasic{}, byQuality: map[E_Quality][]*HeroBasic{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *HeroBasicManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byGroup[d.group] = append(mgr.byGroup[d.group], d)
		mgr.byQuality[d.quality] = append(mgr.byQuality[d.quality], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func HeroBasicSize() int {
	return heroBasicMgr.size
}

func (mgr HeroBasicManager) check(path string, row int, sd *HeroBasic) error {
	if _, ok := heroBasicMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *HeroBasicManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		groupSD, ok := GetHeroGroupByID(d.group)
		if !ok {
			success = false
			log.Printf("英雄表英雄[%d]找不到对应组[%d]配置\n", d.id, d.group)
		}
		qualitySD, ok := GetQualityByID(groupSD.maxQuality)
		if !ok {
			success = false
			log.Printf("英雄表英雄[%d]的最高品质[%d]找不到对应配置\n", d.id, groupSD.maxQuality)
		}
		d.maxQuality = groupSD.maxQuality
		d.groupMaxLevel = qualitySD.heroMaxLevel

		qualitySD, ok = GetQualityByID(d.quality)
		if !ok {
			success = false
			log.Printf("英雄表英雄[%d]的当前品质[%d]找不到对应配置\n", d.id, d.quality)
		}
		d.qualityMaxLevel = qualitySD.heroMaxLevel

		if d.upgradeTo > 0 {
			nextQualityHeroSD, ok := GetHeroBasicByID(d.upgradeTo)
			if !ok {
				success = false
				log.Printf("英雄表英雄[%d]的下一个品质英雄[%d]找不到对应配置\n", d.id, d.upgradeTo)
			} else {
				if nextQualityHeroSD.group != d.group {
					success = false
					log.Printf("英雄表英雄[%d]的下一个品质英雄[%d]的组不一样\n", d.id, d.upgradeTo)
				}
				if nextQualityHeroSD.quality <= d.quality {
					success = false
					log.Printf("英雄表英雄[%d]的下一个品质英雄[%d]的品质小于或等于当前\n", d.id, d.upgradeTo)
				}
			}
		}
		if groupSD, ok := GetHeroGroupByID(d.group); !ok {
			success = false
			log.Printf("英雄表英雄[%d]找不到组[%d]配置\n", d.id, d.group)
		} else {
			d.faction = groupSD.faction
			d.heroType = groupSD.heroType
			d.hpRatio = groupSD.hpRatio
			d.atkRatio = groupSD.atkRatio
			d.defRatio = groupSD.defRatio
			d.rarity = groupSD.rarity
		}

		// 专属表有配置则赋值
		if exclusiveSD, ok := GetExclusiveByHeroGroupID(d.group); ok {
			d.exclusiveId = exclusiveSD.id
		}

		switch d.quality {
		case E_Quality_Blue:
			d.point = ladderMgr.Global.bluePoint
		case E_Quality_BluePlus:
			d.point = ladderMgr.Global.bluePlusPoint
		case E_Quality_Purple:
			d.point = ladderMgr.Global.purplePoint
		case E_Quality_PurplePlus:
			d.point = ladderMgr.Global.purplePlusPoint
		case E_Quality_Orange:
			d.point = ladderMgr.Global.orangePoint
		case E_Quality_OrangePlus:
			d.point = ladderMgr.Global.orangePlusPoint
		case E_Quality_Red:
			d.point = ladderMgr.Global.redPoint
		case E_Quality_RedPlus:
			d.point = ladderMgr.Global.redPlusPoint
		case E_Quality_Platinum:
			d.point = ladderMgr.Global.platinumPoint
		case E_Quality_PlatinumOne:
			d.point = ladderMgr.Global.platinumOnePoint
		case E_Quality_PlatinumTwo:
			d.point = ladderMgr.Global.platinumTwoPoint
		case E_Quality_PlatinumThree:
			d.point = ladderMgr.Global.platinumThreePoint
		case E_Quality_PlatinumFour:
			d.point = ladderMgr.Global.platinumFourPoint
		case E_Quality_PlatinumFive:
			d.point = ladderMgr.Global.platinumFivePoint
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *HeroBasicManager) each(f func(sd *HeroBasic) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *HeroBasicManager) findIf(f func(sd *HeroBasic) (find bool)) *HeroBasic {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachHeroBasic(f func(sd HeroBasic) (continued bool)) {
	for _, sd := range heroBasicMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindHeroBasicIf(f func(sd HeroBasic) bool) (HeroBasic, bool) {
	for _, sd := range heroBasicMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilHeroBasic, false
}

func GetHeroBasicByID(id int) (HeroBasic, bool) {
	temp, ok := heroBasicMgr.byID[id]
	if !ok {
		return nilHeroBasic, false
	}
	return *temp, true
}

func GetHeroBasicByGroup(group int) (vs []HeroBasic, ok bool) {
	var ds []*HeroBasic
	ds, ok = heroBasicMgr.byGroup[group]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachHeroBasicByGroup(group int, fn func(d HeroBasic) (continued bool)) bool {
	ds, ok := heroBasicMgr.byGroup[group]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetHeroBasicByQuality(quality E_Quality) (vs []HeroBasic, ok bool) {
	var ds []*HeroBasic
	ds, ok = heroBasicMgr.byQuality[quality]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachHeroBasicByQuality(quality E_Quality, fn func(d HeroBasic) (continued bool)) bool {
	ds, ok := heroBasicMgr.byQuality[quality]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetHeroBasicGlobal() HeroBasicGlobal {
	return heroBasicMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd HeroBasic) MaxQuality() E_Quality {
	return sd.maxQuality
}

func (sd HeroBasic) GroupMaxLevel() int {
	return sd.groupMaxLevel
}

func (sd HeroBasic) QualityMaxLevel() int {
	return sd.qualityMaxLevel
}

func (sd HeroBasic) Faction() E_Faction {
	return sd.faction
}

func (sd HeroBasic) HeroType() E_HeroType {
	return sd.heroType
}

func (sd HeroBasic) HpRatio() float64 {
	return sd.hpRatio
}

func (sd HeroBasic) AtkRatio() float64 {
	return sd.atkRatio
}

func (sd HeroBasic) DefRatio() float64 {
	return sd.defRatio
}

func (sd HeroBasic) BattleAttribute(ba *BattleAttribute) *BattleAttribute {
	if ba == nil {
		ba = GetBattleAttribute()
	}
	ba.add(E_BattleAttribute_Hp, BattleAttributeValue(sd.hp))
	ba.add(E_BattleAttribute_Atk, BattleAttributeValue(sd.atk))
	ba.add(E_BattleAttribute_Def, BattleAttributeValue(sd.def))
	ba.add(E_BattleAttribute_Crit, BattleAttributeValue(sd.crit))
	ba.add(E_BattleAttribute_Hit, BattleAttributeValue(sd.hit))
	ba.add(E_BattleAttribute_Miss, BattleAttributeValue(sd.miss))
	ba.add(E_BattleAttribute_Speed, BattleAttributeValue(sd.speed))
	ba.add(E_BattleAttribute_Recover, BattleAttributeValue(sd.recover))
	ba.add(E_BattleAttribute_PhysicReduce, BattleAttributeValue(sd.physicReduce))
	ba.add(E_BattleAttribute_MagicReduce, BattleAttributeValue(sd.magicReduce))
	ba.add(E_BattleAttribute_LifeLeech, BattleAttributeValue(sd.lifeLeech))
	ba.add(E_BattleAttribute_HurtEnergy, BattleAttributeValue(sd.hurtEnergy))
	ba.add(E_BattleAttribute_CritDamage, BattleAttributeValue(sd.critDamage))
	return ba
}

func (sd HeroBasic) Point() int {
	return sd.point
}

func (sd HeroBasic) Rarity() E_HeroRarity {
	return sd.rarity
}

func (sd HeroBasic) ExclusiveID() int {
	return sd.exclusiveId
}

func (sd HeroBasic) IsXianGouLiang() bool {
	if sd.group == 11900 {
		return true
	}
	return false
}

func (sd HeroBasic) IsMoGouLiang() bool {
	if sd.group == 13900 {
		return true
	}
	return false
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
