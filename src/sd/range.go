package sd

type Range struct {
	Min int
	Max int
}

func NewRange() *Range {
	return &Range{}
}

func (r Range) Clone() *Range {
	return &Range{
		Min: r.Min,
		Max: r.Max,
	}
}

type RangeF struct {
	Min float64
	Max float64
}

func NewRangeF() *RangeF {
	return &RangeF{}
}

func (r RangeF) Clone() *RangeF {
	return &RangeF{
		Min: r.Min,
		Max: r.Max,
	}
}
