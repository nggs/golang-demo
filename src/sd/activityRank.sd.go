// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 排行活动
type ActivityRank struct {
	id       int                // 自增ID
	rankType E_ActivityRankType // 排行榜活动类型
	theme    string             // 主题名
	desc     string             // 主题描述
	day      int                // 开服的第几天开放
	rankMin  int                // 最小名次
	rankMax  int                // 最大名次
	value    int                // 对应主题所需值
	reward   int                // 奖励
	needText string             // 需求类型文本

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewActivityRank() *ActivityRank {
	sd := &ActivityRank{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 自增ID
func (sd ActivityRank) ID() int {
	return sd.id
}

// 排行榜活动类型
func (sd ActivityRank) RankType() E_ActivityRankType {
	return sd.rankType
}

// 主题名
func (sd ActivityRank) Theme() string {
	return sd.theme
}

// 主题描述
func (sd ActivityRank) Desc() string {
	return sd.desc
}

// 开服的第几天开放
func (sd ActivityRank) Day() int {
	return sd.day
}

// 最小名次
func (sd ActivityRank) RankMin() int {
	return sd.rankMin
}

// 最大名次
func (sd ActivityRank) RankMax() int {
	return sd.rankMax
}

// 对应主题所需值
func (sd ActivityRank) Value() int {
	return sd.value
}

// 奖励
func (sd ActivityRank) Reward() int {
	return sd.reward
}

// 需求类型文本
func (sd ActivityRank) NeedText() string {
	return sd.needText
}

func (sd ActivityRank) Clone() *ActivityRank {
	n := NewActivityRank()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type ActivityRankManager struct {
	Datas []*ActivityRank

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*ActivityRank // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byRankType map[E_ActivityRankType][]*ActivityRank // NormalIndex
	byDay      map[int][]*ActivityRank                // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	topicsByDay  map[int]*ActivityRankTopic
	topicsByType map[E_ActivityRankType]*ActivityRankTopic
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newActivityRankManager() *ActivityRankManager {
	mgr := &ActivityRankManager{
		Datas:      []*ActivityRank{},
		byID:       map[int]*ActivityRank{},
		byRankType: map[E_ActivityRankType][]*ActivityRank{}, byDay: map[int][]*ActivityRank{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>
	mgr.topicsByDay = map[int]*ActivityRankTopic{}
	mgr.topicsByType = map[E_ActivityRankType]*ActivityRankTopic{}
	//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *ActivityRankManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byRankType[d.rankType] = append(mgr.byRankType[d.rankType], d)
		mgr.byDay[d.day] = append(mgr.byDay[d.day], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func ActivityRankSize() int {
	return activityRankMgr.size
}

func (mgr ActivityRankManager) check(path string, row int, sd *ActivityRank) error {
	if _, ok := activityRankMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *ActivityRankManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	Each_E_ActivityRankType(func(tYpE E_ActivityRankType) (continued bool) {
		ds, ok := activityRankMgr.byRankType[tYpE]
		if !ok {
			return true
		}

		topic := &ActivityRankTopic{
			Type:     tYpE,
			MinValue: math.MaxInt32,
			MailType: GetActivityRankMailType(tYpE),
		}

		var lastRankMax int

		for _, d := range ds {
			if d.day <= 0 {
				success = false
				log.Printf("排行榜活动[%d]的开服天数[%d]有误", d.id, d.day)
			}
			if topic.Day == 0 {
				topic.Day = d.day
			} else if topic.Day != d.day {
				success = false
				log.Printf("排行榜活动[%d]的第几天开放[%d]和同组其他数据不一样", d.id, d.day)
			}

			if d.rankMin > d.rankMax {
				success = false
				log.Printf("排行榜活动[%d]的排名区间[%d,%d]有误", d.id, d.rankMin, d.rankMax)
			}
			if d.rankMax < lastRankMax {
				success = false
				log.Printf("排行榜活动[%d]的排名区间[%d,%d]和上一条有重合", d.id, d.rankMin, d.rankMax)
			} else {
				lastRankMax = d.rankMax
			}

			if d.value <= 0 {
				success = false
				log.Printf("排行榜活动[%d]的所需值[%d]有误", d.id, d.value)
			}
			if topic.MinValue > int64(d.value) {
				topic.MinValue = int64(d.value)
			}

			if _, ok := GetLootByGroup(d.reward); !ok {
				success = false
				log.Printf("排行榜活动[%d]的奖励[%d]不存在", d.id, d.reward)
			}

			switch d.rankType {
			case E_ActivityRankType_Stage:
				if _, ok := GetStageByID(d.value); !ok {
					success = false
					log.Printf("排行榜活动[%d]的关卡[%d]不存在", d.id, d.value)
				}

			case E_ActivityRankType_TrialTower:
				if _, ok := GetTrialTowerByTypeAndLevel(E_TrialTowerType_TrialTowerNormal, d.value); !ok {
					success = false
					log.Printf("排行榜活动[%d]的试炼塔[%d]不存在", d.id, d.value)
				}
			}
		}

		mgr.topicsByDay[topic.Day] = topic
		mgr.topicsByType[topic.Type] = topic
		return true
	})
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *ActivityRankManager) each(f func(sd *ActivityRank) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *ActivityRankManager) findIf(f func(sd *ActivityRank) (find bool)) *ActivityRank {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachActivityRank(f func(sd ActivityRank) (continued bool)) {
	for _, sd := range activityRankMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindActivityRankIf(f func(sd ActivityRank) bool) (ActivityRank, bool) {
	for _, sd := range activityRankMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilActivityRank, false
}

func GetActivityRankByID(id int) (ActivityRank, bool) {
	temp, ok := activityRankMgr.byID[id]
	if !ok {
		return nilActivityRank, false
	}
	return *temp, true
}

func GetActivityRankByRankType(rankType E_ActivityRankType) (vs []ActivityRank, ok bool) {
	var ds []*ActivityRank
	ds, ok = activityRankMgr.byRankType[rankType]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachActivityRankByRankType(rankType E_ActivityRankType, fn func(d ActivityRank) (continued bool)) bool {
	ds, ok := activityRankMgr.byRankType[rankType]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetActivityRankByDay(day int) (vs []ActivityRank, ok bool) {
	var ds []*ActivityRank
	ds, ok = activityRankMgr.byDay[day]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachActivityRankByDay(day int, fn func(d ActivityRank) (continued bool)) bool {
	ds, ok := activityRankMgr.byDay[day]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func GetRankActivityMinTopPos() int32 {
	return 10
}

func GetRankActivityMaxPos() int32 {
	return 50
}

func GetRankActivityReward(day int, rank int) (reward *Reward, ok bool) {
	EachActivityRankByDay(day, func(d ActivityRank) (continued bool) {
		if rank >= d.rankMin && rank <= d.rankMax {
			reward, ok = GenerateReward(d.reward)
			return false
		}
		return true
	})
	return
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
