// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 场景表
type Scene struct {
	id              int    // 主键
	bg              string // 关联资源
	tYpE            int    // 副本类型1=关卡2=竞技场
	position1       int    // 野怪或者BOSSid，关联野怪表
	position2       int    // 野怪或者BOSSid，关联野怪表
	position3       int    // 野怪或者BOSSid，关联野怪表
	position4       int    // 野怪或者BOSSid，关联野怪
	position5       int    // 野怪或者BOSSid，关联野怪表
	boss            int    // boss位置，1到5，0代表没有BOSS
	monster_quality []int  // 怪物1~5底下的光环颜色显示，数组显示，顺序1~5，例如[2,2,3,5,3]则代表1号位是绿色，2号位是绿色，3号位是蓝色，4号位是紫色，5号位是蓝色（颜色代码读quality）
	minPower        int    // 全部上阵英雄最低战力限制
	beginDialogId   int    // 开始对话，关联对话表
	endDialogId     int    // 结束对话，关联对话表

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewScene() *Scene {
	sd := &Scene{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 主键
func (sd Scene) ID() int {
	return sd.id
}

// 关联资源
func (sd Scene) Bg() string {
	return sd.bg
}

// 副本类型1=关卡2=竞技场
func (sd Scene) Type() int {
	return sd.tYpE
}

// 野怪或者BOSSid，关联野怪表
func (sd Scene) Position1() int {
	return sd.position1
}

// 野怪或者BOSSid，关联野怪表
func (sd Scene) Position2() int {
	return sd.position2
}

// 野怪或者BOSSid，关联野怪表
func (sd Scene) Position3() int {
	return sd.position3
}

// 野怪或者BOSSid，关联野怪
func (sd Scene) Position4() int {
	return sd.position4
}

// 野怪或者BOSSid，关联野怪表
func (sd Scene) Position5() int {
	return sd.position5
}

// boss位置，1到5，0代表没有BOSS
func (sd Scene) Boss() int {
	return sd.boss
}

// 怪物1~5底下的光环颜色显示，数组显示，顺序1~5，例如[2,2,3,5,3]则代表1号位是绿色，2号位是绿色，3号位是蓝色，4号位是紫色，5号位是蓝色（颜色代码读quality）
func (sd Scene) MonsterQuality() []int {
	return sd.monster_quality
}

// 全部上阵英雄最低战力限制
func (sd Scene) MinPower() int {
	return sd.minPower
}

// 开始对话，关联对话表
func (sd Scene) BeginDialogID() int {
	return sd.beginDialogId
}

// 结束对话，关联对话表
func (sd Scene) EndDialogID() int {
	return sd.endDialogId
}

func (sd Scene) Clone() *Scene {
	n := NewScene()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type SceneManager struct {
	Datas []*Scene

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*Scene // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newSceneManager() *SceneManager {
	mgr := &SceneManager{
		Datas: []*Scene{},
		byID:  map[int]*Scene{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *SceneManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func SceneSize() int {
	return sceneMgr.size
}

func (mgr SceneManager) check(path string, row int, sd *Scene) error {
	if _, ok := sceneMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *SceneManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *SceneManager) each(f func(sd *Scene) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *SceneManager) findIf(f func(sd *Scene) (find bool)) *Scene {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachScene(f func(sd Scene) (continued bool)) {
	for _, sd := range sceneMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindSceneIf(f func(sd Scene) bool) (Scene, bool) {
	for _, sd := range sceneMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilScene, false
}

func GetSceneByID(id int) (Scene, bool) {
	temp, ok := sceneMgr.byID[id]
	if !ok {
		return nilScene, false
	}
	return *temp, true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
