// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 装备升阶表
type EquipmentAdvance struct {
	id        int // ID
	advanceId int // 升阶后装备id
	costId    int // 升阶需要材料id（关联item表）
	costNum   int // 升阶需要材料数量

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewEquipmentAdvance() *EquipmentAdvance {
	sd := &EquipmentAdvance{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// ID
func (sd EquipmentAdvance) ID() int {
	return sd.id
}

// 升阶后装备id
func (sd EquipmentAdvance) AdvanceID() int {
	return sd.advanceId
}

// 升阶需要材料id（关联item表）
func (sd EquipmentAdvance) CostID() int {
	return sd.costId
}

// 升阶需要材料数量
func (sd EquipmentAdvance) CostNum() int {
	return sd.costNum
}

func (sd EquipmentAdvance) Clone() *EquipmentAdvance {
	n := NewEquipmentAdvance()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type EquipmentAdvanceManager struct {
	Datas []*EquipmentAdvance

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID        map[int]*EquipmentAdvance // UniqueIndex
	byAdvanceID map[int]*EquipmentAdvance // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newEquipmentAdvanceManager() *EquipmentAdvanceManager {
	mgr := &EquipmentAdvanceManager{
		Datas: []*EquipmentAdvance{},
		byID:  map[int]*EquipmentAdvance{}, byAdvanceID: map[int]*EquipmentAdvance{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *EquipmentAdvanceManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d
		mgr.byAdvanceID[d.advanceId] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func EquipmentAdvanceSize() int {
	return equipmentAdvanceMgr.size
}

func (mgr EquipmentAdvanceManager) check(path string, row int, sd *EquipmentAdvance) error {
	if _, ok := equipmentAdvanceMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}
	if _, ok := equipmentAdvanceMgr.byAdvanceID[sd.advanceId]; ok {
		return fmt.Errorf("[%s]第[%d]行的advanceId[%v]重复", path, row, sd.advanceId)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *EquipmentAdvanceManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if _, ok := GetEquipmentByID(d.id); !ok {
			success = false
			log.Printf("装备升阶表装备[%d]不存在\n", d.id)
		}
		if _, ok := GetEquipmentByID(d.advanceId); !ok {
			success = false
			log.Printf("装备升阶表装备[%d]不存在\n", d.advanceId)
		}
		if _, ok := GetItemByID(d.costId); !ok {
			success = false
			log.Printf("装备升阶表道具[%d]不存在\n", d.costId)
		}
		if d.costNum <= 0 {
			success = false
			log.Printf("装备升阶表道具数量[%d]配置错误\n", d.costNum)
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *EquipmentAdvanceManager) each(f func(sd *EquipmentAdvance) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *EquipmentAdvanceManager) findIf(f func(sd *EquipmentAdvance) (find bool)) *EquipmentAdvance {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachEquipmentAdvance(f func(sd EquipmentAdvance) (continued bool)) {
	for _, sd := range equipmentAdvanceMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindEquipmentAdvanceIf(f func(sd EquipmentAdvance) bool) (EquipmentAdvance, bool) {
	for _, sd := range equipmentAdvanceMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilEquipmentAdvance, false
}

func GetEquipmentAdvanceByID(id int) (EquipmentAdvance, bool) {
	temp, ok := equipmentAdvanceMgr.byID[id]
	if !ok {
		return nilEquipmentAdvance, false
	}
	return *temp, true
}

func GetEquipmentAdvanceByAdvanceID(advanceId int) (EquipmentAdvance, bool) {
	temp, ok := equipmentAdvanceMgr.byAdvanceID[advanceId]
	if !ok {
		return nilEquipmentAdvance, false
	}
	return *temp, true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
