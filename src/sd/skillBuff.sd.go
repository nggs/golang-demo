// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 技能Buff表
type SkillBuff struct {
	id       int   // id
	tYpE     int   // 类型
	rate     int   // 命中几率万分比
	skillMod []int // buff技能系数
	turn     int   // 持续回合（0=不持续）
	side     int   // 目标群体（0=我方，1=敌方，2=自己，3=攻击目标，4=攻击方）
	rAnGe    int   // 目标范围（0=无差别，1=前排，2=后排）
	num      int   // 目标数量（0=所有，数字=个数）
	choose   int   // 目标选择（1=按照位置从低到高，2随机，3按照生命值从低到高，4按照生命值比例从低到高）
	job      int   // 目标职业（0=无要求，数字=职业要求1战士2法师3牧师4刺客5游侠）
	race     int   // 阵营
	max      int   // 每场战斗命中上限（0=不限
	add      int   // 是否可叠加（0=不可叠加，1=可叠加，2=覆盖）
	nextId   int   // 目标传递（将目标传递给下一个BUFF并生效）
	idName   int   // 别名

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewSkillBuff() *SkillBuff {
	sd := &SkillBuff{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// id
func (sd SkillBuff) ID() int {
	return sd.id
}

// 类型
func (sd SkillBuff) Type() int {
	return sd.tYpE
}

// 命中几率万分比
func (sd SkillBuff) Rate() int {
	return sd.rate
}

// buff技能系数
func (sd SkillBuff) SkillMod() []int {
	return sd.skillMod
}

// 持续回合（0=不持续）
func (sd SkillBuff) Turn() int {
	return sd.turn
}

// 目标群体（0=我方，1=敌方，2=自己，3=攻击目标，4=攻击方）
func (sd SkillBuff) Side() int {
	return sd.side
}

// 目标范围（0=无差别，1=前排，2=后排）
func (sd SkillBuff) Range() int {
	return sd.rAnGe
}

// 目标数量（0=所有，数字=个数）
func (sd SkillBuff) Num() int {
	return sd.num
}

// 目标选择（1=按照位置从低到高，2随机，3按照生命值从低到高，4按照生命值比例从低到高）
func (sd SkillBuff) Choose() int {
	return sd.choose
}

// 目标职业（0=无要求，数字=职业要求1战士2法师3牧师4刺客5游侠）
func (sd SkillBuff) Job() int {
	return sd.job
}

// 阵营
func (sd SkillBuff) Race() int {
	return sd.race
}

// 每场战斗命中上限（0=不限
func (sd SkillBuff) Max() int {
	return sd.max
}

// 是否可叠加（0=不可叠加，1=可叠加，2=覆盖）
func (sd SkillBuff) Add() int {
	return sd.add
}

// 目标传递（将目标传递给下一个BUFF并生效）
func (sd SkillBuff) NextID() int {
	return sd.nextId
}

// 别名
func (sd SkillBuff) IdName() int {
	return sd.idName
}

func (sd SkillBuff) Clone() *SkillBuff {
	n := NewSkillBuff()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type SkillBuffManager struct {
	Datas []*SkillBuff

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*SkillBuff // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newSkillBuffManager() *SkillBuffManager {
	mgr := &SkillBuffManager{
		Datas: []*SkillBuff{},
		byID:  map[int]*SkillBuff{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *SkillBuffManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func SkillBuffSize() int {
	return skillBuffMgr.size
}

func (mgr SkillBuffManager) check(path string, row int, sd *SkillBuff) error {
	if _, ok := skillBuffMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *SkillBuffManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *SkillBuffManager) each(f func(sd *SkillBuff) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *SkillBuffManager) findIf(f func(sd *SkillBuff) (find bool)) *SkillBuff {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachSkillBuff(f func(sd SkillBuff) (continued bool)) {
	for _, sd := range skillBuffMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindSkillBuffIf(f func(sd SkillBuff) bool) (SkillBuff, bool) {
	for _, sd := range skillBuffMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilSkillBuff, false
}

func GetSkillBuffByID(id int) (SkillBuff, bool) {
	temp, ok := skillBuffMgr.byID[id]
	if !ok {
		return nilSkillBuff, false
	}
	return *temp, true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
