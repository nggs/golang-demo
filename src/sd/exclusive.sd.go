// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 专属表
type Exclusive struct {
	id          int    // 专属ID
	name        string // 专属名字
	icon        string // 专属图标
	desc        string // 专属描述
	heroGroupId int    // 专属武将组id
	skillGroup  int    // 专属技能组id
	isOpen      bool   // 开放专属

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewExclusive() *Exclusive {
	sd := &Exclusive{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 专属ID
func (sd Exclusive) ID() int {
	return sd.id
}

// 专属名字
func (sd Exclusive) Name() string {
	return sd.name
}

// 专属图标
func (sd Exclusive) Icon() string {
	return sd.icon
}

// 专属描述
func (sd Exclusive) Desc() string {
	return sd.desc
}

// 专属武将组id
func (sd Exclusive) HeroGroupID() int {
	return sd.heroGroupId
}

// 专属技能组id
func (sd Exclusive) SkillGroup() int {
	return sd.skillGroup
}

// 开放专属
func (sd Exclusive) IsOpen() bool {
	return sd.isOpen
}

func (sd Exclusive) Clone() *Exclusive {
	n := NewExclusive()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 专属表全局属性
type ExclusiveGlobal struct {
	exclusiveSkillLevel1 int       // 1级专属技能所需专属等级
	exclusiveSkillLevel2 int       // 2级专属技能所需专属等级
	exclusiveSkillLevel3 int       // 3级专属技能所需专属等级
	exclusiveSkillLevel4 int       // 4级专属技能所需专属等级
	exclusiveNeedQuality E_Quality // 解锁专属所需武将品质
	exclusiveNeedNum     int       // 开启专属所需材料数量
	exclusiveNeedItem    int       // 开启专属所需材料id(item表)

}

// 1级专属技能所需专属等级
func (g ExclusiveGlobal) ExclusiveSkillLevel1() int {
	return g.exclusiveSkillLevel1
}

// 2级专属技能所需专属等级
func (g ExclusiveGlobal) ExclusiveSkillLevel2() int {
	return g.exclusiveSkillLevel2
}

// 3级专属技能所需专属等级
func (g ExclusiveGlobal) ExclusiveSkillLevel3() int {
	return g.exclusiveSkillLevel3
}

// 4级专属技能所需专属等级
func (g ExclusiveGlobal) ExclusiveSkillLevel4() int {
	return g.exclusiveSkillLevel4
}

// 解锁专属所需武将品质
func (g ExclusiveGlobal) ExclusiveNeedQuality() E_Quality {
	return g.exclusiveNeedQuality
}

// 开启专属所需材料数量
func (g ExclusiveGlobal) ExclusiveNeedNum() int {
	return g.exclusiveNeedNum
}

// 开启专属所需材料id(item表)
func (g ExclusiveGlobal) ExclusiveNeedItem() int {
	return g.exclusiveNeedItem
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type ExclusiveManager struct {
	Datas  []*Exclusive
	Global ExclusiveGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID          map[int]*Exclusive // UniqueIndex
	byHeroGroupID map[int]*Exclusive // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newExclusiveManager() *ExclusiveManager {
	mgr := &ExclusiveManager{
		Datas: []*Exclusive{},
		byID:  map[int]*Exclusive{}, byHeroGroupID: map[int]*Exclusive{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *ExclusiveManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d
		mgr.byHeroGroupID[d.heroGroupId] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func ExclusiveSize() int {
	return exclusiveMgr.size
}

func (mgr ExclusiveManager) check(path string, row int, sd *Exclusive) error {
	if _, ok := exclusiveMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}
	if _, ok := exclusiveMgr.byHeroGroupID[sd.heroGroupId]; ok {
		return fmt.Errorf("[%s]第[%d]行的heroGroupId[%v]重复", path, row, sd.heroGroupId)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *ExclusiveManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if _, ok := GetHeroGroupByID(d.heroGroupId); !ok {
			success = false
			log.Printf("专属表ID[%d]找不到英雄组ID[%d]配置\n", d.id, d.heroGroupId)
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *ExclusiveManager) each(f func(sd *Exclusive) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *ExclusiveManager) findIf(f func(sd *Exclusive) (find bool)) *Exclusive {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachExclusive(f func(sd Exclusive) (continued bool)) {
	for _, sd := range exclusiveMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindExclusiveIf(f func(sd Exclusive) bool) (Exclusive, bool) {
	for _, sd := range exclusiveMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilExclusive, false
}

func GetExclusiveByID(id int) (Exclusive, bool) {
	temp, ok := exclusiveMgr.byID[id]
	if !ok {
		return nilExclusive, false
	}
	return *temp, true
}

func GetExclusiveByHeroGroupID(heroGroupId int) (Exclusive, bool) {
	temp, ok := exclusiveMgr.byHeroGroupID[heroGroupId]
	if !ok {
		return nilExclusive, false
	}
	return *temp, true
}

func GetExclusiveGlobal() ExclusiveGlobal {
	return exclusiveMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
