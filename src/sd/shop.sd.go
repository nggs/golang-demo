// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 商店表
type Shop struct {
	stageId           int   // 关卡ID，严格按照顺序由小到大填写
	commonShop        []int // 普通商店的商品组，格式[120,121,122,123,124]
	guildShop         []int // 公会商店的商品组，格式[120,121,122,123,124]
	dismissShop       []int // 遣散商店的商品组，格式[120,121,122,123,124]
	daoistMagicShop   []int // 迷宫商店的商品组，格式[120,121,122,123,124]
	legendArenaShop   []int // 高阶竞技场商店的商品组，格式[120,121,122,123,124]
	daoistMagicTrader []int // 迷宫商人
	arenaShop         []int // 竞技场商店的商品组，格式[120,121,122,123,124]
	contributionShop  []int // 战功商店
	warcraftPointShop []int // 兵书积分商城
	heroChallengeShop []int // 过关斩将商城

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewShop() *Shop {
	sd := &Shop{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 关卡ID，严格按照顺序由小到大填写
func (sd Shop) StageID() int {
	return sd.stageId
}

// 普通商店的商品组，格式[120,121,122,123,124]
func (sd Shop) CommonShop() []int {
	return sd.commonShop
}

// 公会商店的商品组，格式[120,121,122,123,124]
func (sd Shop) GuildShop() []int {
	return sd.guildShop
}

// 遣散商店的商品组，格式[120,121,122,123,124]
func (sd Shop) DismissShop() []int {
	return sd.dismissShop
}

// 迷宫商店的商品组，格式[120,121,122,123,124]
func (sd Shop) DaoistMagicShop() []int {
	return sd.daoistMagicShop
}

// 高阶竞技场商店的商品组，格式[120,121,122,123,124]
func (sd Shop) LegendArenaShop() []int {
	return sd.legendArenaShop
}

// 迷宫商人
func (sd Shop) DaoistMagicTrader() []int {
	return sd.daoistMagicTrader
}

// 竞技场商店的商品组，格式[120,121,122,123,124]
func (sd Shop) ArenaShop() []int {
	return sd.arenaShop
}

// 战功商店
func (sd Shop) ContributionShop() []int {
	return sd.contributionShop
}

// 兵书积分商城
func (sd Shop) WarcraftPointShop() []int {
	return sd.warcraftPointShop
}

// 过关斩将商城
func (sd Shop) HeroChallengeShop() []int {
	return sd.heroChallengeShop
}

func (sd Shop) Clone() *Shop {
	n := NewShop()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type ShopManager struct {
	Datas []*Shop

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byStageID map[int]*Shop // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	firstShopID int // 第一个商店id
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newShopManager() *ShopManager {
	mgr := &ShopManager{
		Datas:     []*Shop{},
		byStageID: map[int]*Shop{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *ShopManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byStageID[d.stageId] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func ShopSize() int {
	return shopMgr.size
}

func (mgr ShopManager) check(path string, row int, sd *Shop) error {
	if _, ok := shopMgr.byStageID[sd.stageId]; ok {
		return fmt.Errorf("[%s]第[%d]行的stageId[%v]重复", path, row, sd.stageId)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *ShopManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if mgr.firstShopID == 0 {
			mgr.firstShopID = d.stageId
		}
		if _, ok := GetStageByID(d.stageId); !ok {
			success = false
			log.Printf("商店表[%d]对应的关卡不存在\n", d.stageId)
		}
	}
	if mgr.firstShopID == 0 {
		success = false
		log.Printf("商店表没有第一个商店的数据\n")
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *ShopManager) each(f func(sd *Shop) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *ShopManager) findIf(f func(sd *Shop) (find bool)) *Shop {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachShop(f func(sd Shop) (continued bool)) {
	for _, sd := range shopMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindShopIf(f func(sd Shop) bool) (Shop, bool) {
	for _, sd := range shopMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilShop, false
}

func GetShopByStageID(stageId int) (Shop, bool) {
	temp, ok := shopMgr.byStageID[stageId]
	if !ok {
		return nilShop, false
	}
	return *temp, true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
//func ReverseEachShop(f func(sd Shop) (continued bool)) {
//	for i := len(shopMgr.Datas) - 1; i >= 0; i-- {
//		if !f(*shopMgr.Datas[i]) {
//			break
//		}
//	}
//}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
