package sd

import (
	"fmt"
	"sync"

	"msg"
)

func New_RewardThing() *RewardThing {
	m := &RewardThing{}
	return m
}

func (m RewardThing) JsonString() string {
	bs, _ := json.Marshal(m)
	return fmt.Sprintf("{\"RewardThing\":%s}", string(bs))
}

func (m *RewardThing) ResetEx() {
	m.Type = 0
	m.TID = 0
	m.Num = 0
	m.Faction = 0
	m.Level = 0
}

func (m RewardThing) Clone() *RewardThing {
	n, ok := g_RewardThing_Pool.Get().(*RewardThing)
	if !ok || n == nil {
		n = &RewardThing{}
	}
	n.Type = m.Type
	n.TID = m.TID
	n.Num = m.Num
	n.Faction = m.Faction
	n.Level = m.Level
	return n
}

func (m RewardThing) ToMsg(n *msg.RewardThing) *msg.RewardThing {
	if n == nil {
		n = msg.Get_RewardThing()
	}
	n.Type = int32(m.Type)
	n.TID = int32(m.TID)
	n.Num = int32(m.Num)
	n.Faction = int32(m.Faction)
	n.Level = int32(m.Level)
	return n
}

var g_RewardThing_Pool = sync.Pool{}

func Get_RewardThing() *RewardThing {
	m, ok := g_RewardThing_Pool.Get().(*RewardThing)
	if !ok {
		m = New_RewardThing()
	} else {
		if m == nil {
			m = New_RewardThing()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_RewardThing(i interface{}) {
	if m, ok := i.(*RewardThing); ok && m != nil {
		g_RewardThing_Pool.Put(i)
	}
}

func New_Reward() *Reward {
	m := &Reward{}
	return m
}

func (m Reward) String() string {
	bs, _ := json.Marshal(m)
	return fmt.Sprintf("{\"Reward\":%s}", string(bs))
}

func (m *Reward) ResetEx() {

	for _, i := range m.Things {
		Put_Reward(i)
	}

	//m.Things = []*Reward{}
	m.Things = nil

}

func (m Reward) Clone() *Reward {
	n, ok := g_Reward_Pool.Get().(*Reward)
	if !ok || n == nil {
		n = &Reward{}
	}

	if len(m.Things) > 0 {
		n.Things = make([]*RewardThing, len(m.Things))
		for i, e := range m.Things {

			if e != nil {
				n.Things[i] = e.Clone()
			}

		}
	} else {
		//n.Things = []*RewardThing{}
		n.Things = nil
	}

	return n
}

func (m Reward) ToMsg(n *msg.Reward) *msg.Reward {
	if n == nil {
		n = msg.Get_Reward()
	}

	if len(m.Things) > 0 {
		n.Things = make([]*msg.RewardThing, len(m.Things))
		for i, e := range m.Things {
			if e != nil {
				n.Things[i] = e.ToMsg(n.Things[i])
			} else {
				n.Things[i] = msg.Get_RewardThing()
			}
		}
	} else {
		//n.Things = []*RewardThing{}
		n.Things = nil
	}

	return n
}

var g_Reward_Pool = sync.Pool{}

func Get_Reward() *Reward {
	m, ok := g_Reward_Pool.Get().(*Reward)
	if !ok {
		m = New_Reward()
	} else {
		if m == nil {
			m = New_Reward()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_Reward(i interface{}) {
	if m, ok := i.(*Reward); ok && m != nil {
		g_Reward_Pool.Put(i)
	}
}
