// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 被动技能表
type SkillPassive struct {
	id              int   // 技能id
	condition       []int // 条件触发（对应skillPassiveCondition表中id，条件触发1，条件触发2）
	hpRate          int   // 生命万分比
	atkRate         int   // 攻击万分比
	defRate         int   // 护甲万分比
	speedRate       int   // 速度万分比
	hitRate         int   // 精准（万分比）
	blockRate       int   // 格挡（万分比）
	critRate        int   // 暴击（万分比）
	critDamageRate  int   // 暴击伤害增加（万分比）
	skillDamageRate int   // 技能伤害增加（万分比）
	deDamageRAte    int   // 伤害减免（万分比）
	antiDefend      int   // 破甲
	antiControl     int   // 免控
	holyAtk         int   // 神圣伤害
	job1            int   // 对战士伤害增加（万分比）
	job2            int   // 对法师伤害增加（万分比）
	job3            int   // 对牧师伤害增加（万分比）
	job4            int   // 对刺客伤害增加（万分比）
	job5            int   // 对游侠伤害增加（万分比）
	speed           int   // 增加速度点数

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewSkillPassive() *SkillPassive {
	sd := &SkillPassive{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 技能id
func (sd SkillPassive) ID() int {
	return sd.id
}

// 条件触发（对应skillPassiveCondition表中id，条件触发1，条件触发2）
func (sd SkillPassive) Condition() []int {
	return sd.condition
}

// 生命万分比
func (sd SkillPassive) HpRate() int {
	return sd.hpRate
}

// 攻击万分比
func (sd SkillPassive) AtkRate() int {
	return sd.atkRate
}

// 护甲万分比
func (sd SkillPassive) DefRate() int {
	return sd.defRate
}

// 速度万分比
func (sd SkillPassive) SpeedRate() int {
	return sd.speedRate
}

// 精准（万分比）
func (sd SkillPassive) HitRate() int {
	return sd.hitRate
}

// 格挡（万分比）
func (sd SkillPassive) BlockRate() int {
	return sd.blockRate
}

// 暴击（万分比）
func (sd SkillPassive) CritRate() int {
	return sd.critRate
}

// 暴击伤害增加（万分比）
func (sd SkillPassive) CritDamageRate() int {
	return sd.critDamageRate
}

// 技能伤害增加（万分比）
func (sd SkillPassive) SkillDamageRate() int {
	return sd.skillDamageRate
}

// 伤害减免（万分比）
func (sd SkillPassive) DeDamageRAte() int {
	return sd.deDamageRAte
}

// 破甲
func (sd SkillPassive) AntiDefend() int {
	return sd.antiDefend
}

// 免控
func (sd SkillPassive) AntiControl() int {
	return sd.antiControl
}

// 神圣伤害
func (sd SkillPassive) HolyAtk() int {
	return sd.holyAtk
}

// 对战士伤害增加（万分比）
func (sd SkillPassive) Job1() int {
	return sd.job1
}

// 对法师伤害增加（万分比）
func (sd SkillPassive) Job2() int {
	return sd.job2
}

// 对牧师伤害增加（万分比）
func (sd SkillPassive) Job3() int {
	return sd.job3
}

// 对刺客伤害增加（万分比）
func (sd SkillPassive) Job4() int {
	return sd.job4
}

// 对游侠伤害增加（万分比）
func (sd SkillPassive) Job5() int {
	return sd.job5
}

// 增加速度点数
func (sd SkillPassive) Speed() int {
	return sd.speed
}

func (sd SkillPassive) Clone() *SkillPassive {
	n := NewSkillPassive()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type SkillPassiveManager struct {
	Datas []*SkillPassive

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*SkillPassive // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newSkillPassiveManager() *SkillPassiveManager {
	mgr := &SkillPassiveManager{
		Datas: []*SkillPassive{},
		byID:  map[int]*SkillPassive{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *SkillPassiveManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func SkillPassiveSize() int {
	return skillPassiveMgr.size
}

func (mgr SkillPassiveManager) check(path string, row int, sd *SkillPassive) error {
	if _, ok := skillPassiveMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *SkillPassiveManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *SkillPassiveManager) each(f func(sd *SkillPassive) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *SkillPassiveManager) findIf(f func(sd *SkillPassive) (find bool)) *SkillPassive {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachSkillPassive(f func(sd SkillPassive) (continued bool)) {
	for _, sd := range skillPassiveMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindSkillPassiveIf(f func(sd SkillPassive) bool) (SkillPassive, bool) {
	for _, sd := range skillPassiveMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilSkillPassive, false
}

func GetSkillPassiveByID(id int) (SkillPassive, bool) {
	temp, ok := skillPassiveMgr.byID[id]
	if !ok {
		return nilSkillPassive, false
	}
	return *temp, true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
