// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 品质表
type Quality struct {
	id           E_Quality // 品质ID
	heroMaxLevel int       // 该品质英雄可升级到最高等级
	name         string    // 品质名称 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+
	itemFrame    string    // 道具边框
	heroIcon     string    // 英雄品质图标
	heroFrame    string    // 英雄边框
	battleFrame  string    // 战斗边框
	flash        string    // 图标的光圈环绕光效
	warcraft     int       // 开启的兵书槽位数

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewQuality() *Quality {
	sd := &Quality{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 品质ID
func (sd Quality) ID() E_Quality {
	return sd.id
}

// 该品质英雄可升级到最高等级
func (sd Quality) HeroMaxLevel() int {
	return sd.heroMaxLevel
}

// 品质名称 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+
func (sd Quality) Name() string {
	return sd.name
}

// 道具边框
func (sd Quality) ItemFrame() string {
	return sd.itemFrame
}

// 英雄品质图标
func (sd Quality) HeroIcon() string {
	return sd.heroIcon
}

// 英雄边框
func (sd Quality) HeroFrame() string {
	return sd.heroFrame
}

// 战斗边框
func (sd Quality) BattleFrame() string {
	return sd.battleFrame
}

// 图标的光圈环绕光效
func (sd Quality) Flash() string {
	return sd.flash
}

// 开启的兵书槽位数
func (sd Quality) Warcraft() int {
	return sd.warcraft
}

func (sd Quality) Clone() *Quality {
	n := NewQuality()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 品质表全局属性
type QualityGlobal struct {
	equipmentMaxQuality E_Quality // 装备的最高品质
	heroMaxQuality      E_Quality // 武将最高品质

}

// 装备的最高品质
func (g QualityGlobal) EquipmentMaxQuality() E_Quality {
	return g.equipmentMaxQuality
}

// 武将最高品质
func (g QualityGlobal) HeroMaxQuality() E_Quality {
	return g.heroMaxQuality
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type QualityManager struct {
	Datas  []*Quality
	Global QualityGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[E_Quality]*Quality // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	openMinWarcraftQuality E_Quality // 开启的兵书最小品质
	openMaxWarcraft        int       // 开启的兵书最大槽位数
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newQualityManager() *QualityManager {
	mgr := &QualityManager{
		Datas: []*Quality{},
		byID:  map[E_Quality]*Quality{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *QualityManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func QualitySize() int {
	return qualityMgr.size
}

func (mgr QualityManager) check(path string, row int, sd *Quality) error {
	if _, ok := qualityMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *QualityManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if d.heroMaxLevel > GetHeroMaxLevel() {
			success = false
			log.Printf("品质表品质[%d]的英雄最大等级[%d]超过[%d]\n", d.id, d.heroMaxLevel, GetHeroMaxLevel())
		}
		if d.warcraft > 0 && int(mgr.openMinWarcraftQuality) == 0 {
			mgr.openMinWarcraftQuality = d.ID()
		}
		if mgr.openMaxWarcraft < d.warcraft {
			mgr.openMaxWarcraft = d.warcraft
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *QualityManager) each(f func(sd *Quality) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *QualityManager) findIf(f func(sd *Quality) (find bool)) *Quality {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachQuality(f func(sd Quality) (continued bool)) {
	for _, sd := range qualityMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindQualityIf(f func(sd Quality) bool) (Quality, bool) {
	for _, sd := range qualityMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilQuality, false
}

func GetQualityByID(id E_Quality) (Quality, bool) {
	temp, ok := qualityMgr.byID[id]
	if !ok {
		return nilQuality, false
	}
	return *temp, true
}

func GetQualityGlobal() QualityGlobal {
	return qualityMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
// 开启的兵书最小品质
func OpenMinWarcraftQuality() E_Quality {
	return qualityMgr.openMinWarcraftQuality
}

// 开启的兵书最大槽位数
func OpenMaxWarcraftNum() int {
	return qualityMgr.openMaxWarcraft
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
