package sd

import (
	nrandom "nggs/random"
)

//func GetEquipUIDFromMsgHeroUpdateEquip(m *msg.HeroUpdateEquipRequest, pos E_EquipmentPos) (uid string) {
//	if m == nil {
//		return
//	}
//	if !Check_E_EquipmentPos(pos) {
//		return
//	}
//	if int(pos) > len(m.EquipUIDs) {
//		return
//	}
//	return m.EquipUIDs[pos-1]
//}

type HeroGroupDice struct {
	super nrandom.IDiscreteDistributionDice
}

func NewHeroGroupDice(heroGroups []*HeroGroup) *HeroGroupDice {
	dice := &HeroGroupDice{}
	dice.super = nrandom.NewDiscreteDistributionDice()
	for _, v := range heroGroups {
		dice.super.Append(nrandom.NewDiscreteDistributionCandidate(v, func(context interface{}) (int, interface{}) {
			return context.(*HeroGroup).daoistMagicAssistant, nil
		}))
	}
	dice.super.Build()
	return dice
}

func NewAdvancedHeroGroupDice(heroGroups []*HeroGroup) *HeroGroupDice {
	dice := &HeroGroupDice{}
	dice.super = nrandom.NewDiscreteDistributionDice()
	for _, v := range heroGroups {
		if v.daoistMagicAdvancedAssistant == 0 {
			continue
		}
		dice.super.Append(nrandom.NewDiscreteDistributionCandidate(v, func(context interface{}) (int, interface{}) {
			return context.(*HeroGroup).daoistMagicAdvancedAssistant, nil
		}))
	}
	dice.super.Build()
	return dice
}

func HeroMasterNum() int {
	return 5
}
