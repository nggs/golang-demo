// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 被动技能条件表
type SkillPassiveCondition struct {
	id   int   // id
	tYpE int   // 类型（对应skillPassiveConditionType表中id）
	mod  []int // 条件系数（对应skillPassiveConditionType表中每个条件使用的参数x0,x1,...）
	max  int   // 每场战斗命中上限（0=不限
	buff []int // 触发buff（对应skillBuff表中id）

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewSkillPassiveCondition() *SkillPassiveCondition {
	sd := &SkillPassiveCondition{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// id
func (sd SkillPassiveCondition) ID() int {
	return sd.id
}

// 类型（对应skillPassiveConditionType表中id）
func (sd SkillPassiveCondition) Type() int {
	return sd.tYpE
}

// 条件系数（对应skillPassiveConditionType表中每个条件使用的参数x0,x1,...）
func (sd SkillPassiveCondition) Mod() []int {
	return sd.mod
}

// 每场战斗命中上限（0=不限
func (sd SkillPassiveCondition) Max() int {
	return sd.max
}

// 触发buff（对应skillBuff表中id）
func (sd SkillPassiveCondition) Buff() []int {
	return sd.buff
}

func (sd SkillPassiveCondition) Clone() *SkillPassiveCondition {
	n := NewSkillPassiveCondition()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type SkillPassiveConditionManager struct {
	Datas []*SkillPassiveCondition

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*SkillPassiveCondition // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newSkillPassiveConditionManager() *SkillPassiveConditionManager {
	mgr := &SkillPassiveConditionManager{
		Datas: []*SkillPassiveCondition{},
		byID:  map[int]*SkillPassiveCondition{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *SkillPassiveConditionManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func SkillPassiveConditionSize() int {
	return skillPassiveConditionMgr.size
}

func (mgr SkillPassiveConditionManager) check(path string, row int, sd *SkillPassiveCondition) error {
	if _, ok := skillPassiveConditionMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *SkillPassiveConditionManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *SkillPassiveConditionManager) each(f func(sd *SkillPassiveCondition) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *SkillPassiveConditionManager) findIf(f func(sd *SkillPassiveCondition) (find bool)) *SkillPassiveCondition {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachSkillPassiveCondition(f func(sd SkillPassiveCondition) (continued bool)) {
	for _, sd := range skillPassiveConditionMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindSkillPassiveConditionIf(f func(sd SkillPassiveCondition) bool) (SkillPassiveCondition, bool) {
	for _, sd := range skillPassiveConditionMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilSkillPassiveCondition, false
}

func GetSkillPassiveConditionByID(id int) (SkillPassiveCondition, bool) {
	temp, ok := skillPassiveConditionMgr.byID[id]
	if !ok {
		return nilSkillPassiveCondition, false
	}
	return *temp, true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
