package sd

var nilActivityRankTopic = ActivityRankTopic{}

type ActivityRankTopic struct {
	Day      int
	Type     E_ActivityRankType
	MinValue int64
	MailType E_MailType
}

func GetActivityRankTopicByOpenDay(zoneOpenDay int) (ActivityRankTopic, bool) {
	topic, ok := activityRankMgr.topicsByDay[zoneOpenDay]
	if !ok {
		return nilActivityRankTopic, false
	}
	return *topic, true
}

func GetActivityRankTopicByType(tYpE E_ActivityRankType) (ActivityRankTopic, bool) {
	topic, ok := activityRankMgr.topicsByType[tYpE]
	if !ok {
		return nilActivityRankTopic, false
	}
	return *topic, true
}

func GetActivityRankMailType(tYpE E_ActivityRankType) E_MailType {
	switch tYpE {
	case E_ActivityRankType_EmployHero:
		return E_MailType_EmployHeroRank
	case E_ActivityRankType_LuckBox:
		return E_MailType_LuckBoxRank
	case E_ActivityRankType_Stage:
		return E_MailType_StageRank
	case E_ActivityRankType_HeroLevelUp:
		return E_MailType_HeroLevelUpRank
	case E_ActivityRankType_TrialTower:
		return E_MailType_TrialTowerRank
	case E_ActivityRankType_HeroEvolution:
		return E_MailType_HeroEvolutionRank
	case E_ActivityRankType_EquipStrengthen:
		return E_MailType_EquipStrengthenRank
	case E_ActivityRankType_Hunt:
		return E_MailType_HuntRank
	case E_ActivityRankType_Power:
		return E_MailType_PowerRank
	case E_ActivityRankType_CostGoldCoin:
		return E_MailType_CostGoldCoinRank
	}
	return E_MailType_Null
}

func EachActivityRankTopic(fn func(day int, d ActivityRankTopic) (continued bool)) {
	for day, topic := range activityRankMgr.topicsByDay {
		if !fn(day, *topic) {
			break
		}
	}
}
