package sd

import (
	"fmt"
	"log"

	nrandom "nggs/random"

	"msg"
)

type RewardThing struct {
	Type    E_ItemBigType
	TID     int
	Num     int
	Faction E_Faction
	Level   int
}

func (m RewardThing) IsEmpty() bool {
	return m.Type == 0 || m.TID == 0 || m.Num == 0
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type Reward struct {
	Things []*RewardThing
}

func (m Reward) Each(f func(thing *RewardThing) (continued bool)) {
	for _, thing := range m.Things {
		if !f(thing) {
			break
		}
	}
}

func (m Reward) IsEmpty() bool {
	if len(m.Things) == 0 {
		return true
	}
	return false
}

func (m *Reward) get(tYpE E_ItemBigType, tid int, faction E_Faction, level int) *RewardThing {
	for _, thing := range m.Things {
		if thing.Type != tYpE {
			continue
		}
		switch thing.Type {
		case E_ItemBigType_Equip:
			if faction > 0 {
				if level > 0 {
					if thing.TID == tid && thing.Faction == faction && thing.Level == level {
						return thing
					}
				} else {
					if thing.TID == tid && thing.Faction == faction {
						return thing
					}
				}
			} else {
				if level > 0 {
					if thing.TID == tid && thing.Level == level {
						return thing
					}
				} else {
					if thing.TID == tid {
						return thing
					}
				}
			}
		case E_ItemBigType_Hero:
			if level > 0 {
				if thing.TID == tid && thing.Level == level {
					return thing
				}
			} else {
				if thing.TID == tid {
					return thing
				}
			}
		default:
			if thing.TID == tid {
				return thing
			}
		}
	}
	return nil
}

func (m *Reward) MustAppendOne(tYpE E_ItemBigType, tid int, num int, faction E_Faction, level int) *Reward {
	if !Check_E_ItemBigType(tYpE) || tid <= 0 || num <= 0 || (!Check_E_Faction(faction) && faction != E_Faction_Null) || level < 0 {
		log.Panicf("invalid reward thing, tYpE=%v, tid=%d, num=%d, faction=%d, level=%d", tYpE, tid, num, faction, level)
	}

	thing := m.get(tYpE, tid, faction, level)
	if thing != nil {
		thing.Num += num
		return m
	}

	thing = Get_RewardThing()
	thing.Type = tYpE
	thing.TID = tid
	thing.Num = num
	thing.Faction = faction
	switch thing.Type {
	case E_ItemBigType_Equip:
		if level == 0 {
			thing.Level = 1
		}
	default:
		thing.Level = level
	}
	m.Things = append(m.Things, thing)
	return m
}

func (m *Reward) AppendOneWithFactionAndLevel(tYpE E_ItemBigType, tid int, num int, faction E_Faction, level int) *Reward {
	if !Check_E_ItemBigType(tYpE) || tid <= 0 || num <= 0 || (!Check_E_Faction(faction) && faction != E_Faction_Null) || level < 0 {
		return m
	}

	thing := m.get(tYpE, tid, faction, level)
	if thing != nil {
		thing.Num += num
		return m
	}

	thing = Get_RewardThing()
	thing.Type = tYpE
	thing.TID = tid
	thing.Num = num
	thing.Faction = faction
	switch thing.Type {
	case E_ItemBigType_Equip:
		if level == 0 {
			thing.Level = 1
		}
	default:
		thing.Level = level
	}
	m.Things = append(m.Things, thing)
	return m
}

func (m *Reward) AppendOneWithNotMerge(tYpE E_ItemBigType, tid int, num int, faction E_Faction, level int) *Reward {
	if !Check_E_ItemBigType(tYpE) || tid <= 0 || num <= 0 || (!Check_E_Faction(faction) && faction != E_Faction_Null) || level < 0 {
		return m
	}

	thing := Get_RewardThing()
	thing.Type = tYpE
	thing.TID = tid
	thing.Num = num
	thing.Faction = faction
	switch thing.Type {
	case E_ItemBigType_Equip:
		if level == 0 {
			thing.Level = 1
		}
	default:
		thing.Level = level
	}
	m.Things = append(m.Things, thing)
	return m
}

func (m *Reward) AppendOne(tYpE E_ItemBigType, tid int, num int) *Reward {
	return m.AppendOneWithFactionAndLevel(tYpE, tid, num, E_Faction_Null, 0)
}

func (m *Reward) AppendOneStaticData(d Loot) *Reward {
	return m.AppendOneWithFactionAndLevel(d.DataFrom(), d.ItemID(), d.Num(), d.Faction(), d.Level())
}

func (m *Reward) AppendOneThing(thing *RewardThing) *Reward {
	if thing == nil {
		return m
	}

	if thing.Num <= 0 {
		return m
	}

	t := m.get(thing.Type, thing.TID, thing.Faction, thing.Level)
	if t != nil {
		t.Num += thing.Num
		return m
	}

	m.Things = append(m.Things, thing.Clone())

	return m
}

func (m *Reward) Merge(other *Reward) *Reward {
	var thing *RewardThing
	for _, t := range other.Things {
		if t.Num <= 0 {
			continue
		}
		thing = m.get(t.Type, t.TID, t.Faction, t.Level)
		if thing != nil {
			thing.Num += t.Num
		} else {
			m.Things = append(m.Things, t.Clone())
		}
	}
	return m
}

func (m Reward) Check() (err error) {
	for _, thing := range m.Things {
		switch thing.Type {
		case E_ItemBigType_Currency:
			// 货币
			_, ok := GetCurrencyByID(thing.TID)
			if !ok {
				return fmt.Errorf("currency[%d] static data not exist", thing.TID)
			}
		case E_ItemBigType_Item:
			// 物品
			_, ok := GetItemByID(thing.TID)
			if !ok {
				return fmt.Errorf("item[%d] static data not exist", thing.TID)
			}
		case E_ItemBigType_Hero:
			// 英雄
			_, ok := GetHeroBasicByID(thing.TID)
			if !ok {
				return fmt.Errorf("hero[%d] static data not exist", thing.TID)
			}

		case E_ItemBigType_Equip:
			// 装备
			_, ok := GetEquipmentByID(thing.TID)
			if !ok {
				return fmt.Errorf("equip[%d] static data not exist", thing.TID)
			}
			if thing.Faction != E_Faction_Null {
				if !Check_E_Faction(thing.Faction) {
					return fmt.Errorf("equip[%d] invalid faction[%v]", thing.TID, thing.Faction)
				}
			}
		}
		if thing.Num <= 0 {
			return fmt.Errorf("reward thing num <= 0")
		}
	}
	return
}

func (m *Reward) Multiple(multiple float64) *Reward {
	if multiple <= 0 {
		return m
	}
	for _, t := range m.Things {
		t.Num = int(float64(t.Num) * multiple)
	}
	return m
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type IntToSimpleRewardMap map[int]*Reward

func NewIntToSimpleRewardMap() (m *IntToSimpleRewardMap) {
	m = &IntToSimpleRewardMap{}
	return
}

func ToIntToSimpleRewardMap(m map[int]*Reward) *IntToSimpleRewardMap {
	return (*IntToSimpleRewardMap)(&m)
}

func (m *IntToSimpleRewardMap) Assign(n map[int]*Reward) *IntToSimpleRewardMap {
	*m = make(map[int]*Reward, len(n))
	if n == nil || len(n) == 0 {
		return m
	}
	for key, value := range n {
		(*m)[key] = value.Clone()
	}
	return m
}

func (m IntToSimpleRewardMap) ToMsg(n map[int32]*msg.Reward) map[int32]*msg.Reward {
	if len(m) == 0 {
		n = map[int32]*msg.Reward{}
	} else {
		n = make(map[int32]*msg.Reward, len(m))
	}
	for key, value := range m {
		n[int32(key)] = value.ToMsg(nil)
	}
	return n
}

func (m *IntToSimpleRewardMap) Get(key int) (value *Reward, ok bool) {
	value, ok = (*m)[key]
	return
}

func (m *IntToSimpleRewardMap) Set(key int, value *Reward) {
	(*m)[key] = value
}

func (m *IntToSimpleRewardMap) Add(key int) (value *Reward) {
	value = Get_Reward()
	(*m)[key] = value
	return
}

func (m *IntToSimpleRewardMap) Remove(key int) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *IntToSimpleRewardMap) RemoveOne(fn func(key int, value *Reward) (continued bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *IntToSimpleRewardMap) RemoveSome(fn func(key int, value *Reward) (continued bool)) {
	left := map[int]*Reward{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *IntToSimpleRewardMap) Each(f func(key int, value *Reward) (continued bool)) {
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m IntToSimpleRewardMap) Size() int {
	return len(m)
}

func (m *IntToSimpleRewardMap) Clear() {
	*m = *NewIntToSimpleRewardMap()
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (sd Loot) ToMsg() (m *msg.RewardThing) {
	m = msg.Get_RewardThing()
	m.Type = int32(sd.dataFrom)
	m.TID = int32(sd.itemId)
	m.Num = int32(sd.num)
	m.Faction = int32(sd.faction)
	m.Level = int32(sd.level)
	return m
}

func AppendOneReward(reward *Reward, tYpE E_ItemBigType, tid int, num int) *Reward {
	if reward == nil {
		reward = Get_Reward()
	}
	return reward.AppendOne(tYpE, tid, num)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type RewardDice struct {
	super nrandom.IDiscreteDistributionDice

	id int
	//rewards            []*Reward
	minTimes          int // 最小随机次数
	maxTimes          int // 最大随机次数
	mustDropRewards   []*Loot
	randomDropRewards []*Loot
}

func NewRewardDice(id int, rewards []*Loot) *RewardDice {
	if id == 0 {
		log.Panicf("奖励组id为0")
	}
	if len(rewards) == 0 {
		log.Panicf("奖励组[%d]奖励项为空", id)
	}
	firstReward := rewards[0]
	if firstReward.minTimes < 0 {
		log.Panicf("奖励组[%d]最少掉落次数[%d]错误", id, firstReward.minTimes)
	}
	if firstReward.maxTimes < 0 {
		log.Panicf("奖励组[%d]最多掉落次数[%d]错误", id, firstReward.maxTimes)
	}
	dice := &RewardDice{
		id: id,
		//rewards:   rewards,
		minTimes: firstReward.minTimes,
		maxTimes: firstReward.maxTimes,
	}
	dice.super = nrandom.NewDiscreteDistributionDice()
	for _, reward := range rewards {
		if reward.mustDrop {
			dice.mustDropRewards = append(dice.mustDropRewards, reward)
		} else {
			if reward.weight <= 0 {
				continue
			}
			dice.randomDropRewards = append(dice.randomDropRewards, reward)
			dice.super.Append(nrandom.NewDiscreteDistributionCandidate(reward, func(context interface{}) (int, interface{}) {
				d := context.(*Loot)
				return d.weight, d.id
			}))
		}
	}
	dice.super.Build()
	return dice
}

func GenerateReward(groupID int) (simpleReward *Reward, ok bool) {
	dice, ok := lootMgr.dices[groupID]
	if !ok {
		return nil, false
	}
	simpleReward = Get_Reward()
	for _, thing := range dice.mustDropRewards {
		simpleReward.AppendOneThing(thing.simple)
	}
	if len(dice.randomDropRewards) > 0 {
		times := nrandom.Int(dice.minTimes, dice.maxTimes)
		for i := 0; i < times; i++ {
			ctx, err := dice.super.Roll()
			if err != nil {
				log.Panic(err)
			}
			loot, ok := ctx.(*Loot)
			if !ok || loot == nil {
				log.Panicf("奖励组[%d]随机奖励出错", groupID)
			}
			simpleReward.AppendOneThing(loot.simple)
		}
	}
	return
}
