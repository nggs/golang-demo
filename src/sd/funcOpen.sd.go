// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 功能开启表
type FuncOpen struct {
	id      E_FunctionType // 功能id
	name    string         // 功能名
	desc    string         // 功能描述
	stageId int            // 开启关卡（填-1表示只与vip等级挂钩）

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewFuncOpen() *FuncOpen {
	sd := &FuncOpen{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 功能id
func (sd FuncOpen) ID() E_FunctionType {
	return sd.id
}

// 功能名
func (sd FuncOpen) Name() string {
	return sd.name
}

// 功能描述
func (sd FuncOpen) Desc() string {
	return sd.desc
}

// 开启关卡（填-1表示只与vip等级挂钩）
func (sd FuncOpen) StageID() int {
	return sd.stageId
}

func (sd FuncOpen) Clone() *FuncOpen {
	n := NewFuncOpen()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 功能开启表全局属性
type FuncOpenGlobal struct {
	battleSkipStage int // 通关该关卡后出现战斗跳过按钮

}

// 通关该关卡后出现战斗跳过按钮
func (g FuncOpenGlobal) BattleSkipStage() int {
	return g.battleSkipStage
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type FuncOpenManager struct {
	Datas  []*FuncOpen
	Global FuncOpenGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[E_FunctionType]*FuncOpen // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newFuncOpenManager() *FuncOpenManager {
	mgr := &FuncOpenManager{
		Datas: []*FuncOpen{},
		byID:  map[E_FunctionType]*FuncOpen{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *FuncOpenManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func FuncOpenSize() int {
	return funcOpenMgr.size
}

func (mgr FuncOpenManager) check(path string, row int, sd *FuncOpen) error {
	if _, ok := funcOpenMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *FuncOpenManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if _, ok := GetStageByID(d.stageId); d.stageId > 0 && !ok {
			success = false
			log.Printf("功能表功能[%d]的解锁关卡[%d]找不到对应数据\n", d.id, d.stageId)
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *FuncOpenManager) each(f func(sd *FuncOpen) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *FuncOpenManager) findIf(f func(sd *FuncOpen) (find bool)) *FuncOpen {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachFuncOpen(f func(sd FuncOpen) (continued bool)) {
	for _, sd := range funcOpenMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindFuncOpenIf(f func(sd FuncOpen) bool) (FuncOpen, bool) {
	for _, sd := range funcOpenMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilFuncOpen, false
}

func GetFuncOpenByID(id E_FunctionType) (FuncOpen, bool) {
	temp, ok := funcOpenMgr.byID[id]
	if !ok {
		return nilFuncOpen, false
	}
	return *temp, true
}

func GetFuncOpenGlobal() FuncOpenGlobal {
	return funcOpenMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
