//+build ignore

package sd

import (
	"testing"
)

func TestBattleAttribute(t *testing.T) {
	ba := GetBattleAttribute()
	defer PutBattleAttribute(ba)
	ba.MustAdd(101, 100)
	t.Logf("%+v\n", ba)
}
