// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 排行榜表
type Ladder struct {
	id         int          // id
	ladderType E_LadderType // 排行榜类型，关联枚举表排行榜类型
	value      int          // 任务目标值 类型是积分榜，此值是积分 类型是关卡榜，此值是关卡ID 类型是试炼塔榜，此值是试炼塔ID
	desc       string       // 任务描述

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewLadder() *Ladder {
	sd := &Ladder{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// id
func (sd Ladder) ID() int {
	return sd.id
}

// 排行榜类型，关联枚举表排行榜类型
func (sd Ladder) LadderType() E_LadderType {
	return sd.ladderType
}

// 任务目标值 类型是积分榜，此值是积分 类型是关卡榜，此值是关卡ID 类型是试炼塔榜，此值是试炼塔ID
func (sd Ladder) Value() int {
	return sd.value
}

// 任务描述
func (sd Ladder) Desc() string {
	return sd.desc
}

func (sd Ladder) Clone() *Ladder {
	n := NewLadder()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 排行榜表全局属性
type LadderGlobal struct {
	ladderReward           int            // 当出现首位达成任务进度的角色后，本服所有角色会获得奖励。关联奖励表
	ladderRoleNum          int            // 排行榜上榜角色数量
	ladderRefreshTime      int            // 排行榜数据更新时间/秒
	ladderTaskPlayNum      int            // 前多少名达成的任务进度的角色显示在日志中
	ladderWei              E_Faction      // 魏国积分榜对应的种族
	ladderShu              E_Faction      // 蜀国积分榜对应的种族
	ladderWu               E_Faction      // 吴国积分榜对应的种族
	ladderQun              E_Faction      // 群雄积分榜对应的种族
	ladderStage            E_FunctionType // 关卡进度榜对应的功能ID
	ladderTrialTower       E_FunctionType // 试炼塔进度榜对应的功能ID
	ladderWeiBg            string         // 魏国积分榜选择图
	ladderShuBg            string         // 蜀国积分榜选择图
	ladderWuBg             string         // 吴国积分榜选择图
	ladderQunBg            string         // 群雄积分榜选择图
	ladderStageBg          string         // 关卡进度榜对选择图
	ladderTrialTowerBg     string         // 试炼塔进度榜选择图
	bluePoint              int            // 蓝色英雄积分
	bluePlusPoint          int            // 蓝色+英雄积分
	purplePoint            int            // 紫色英雄积分
	purplePlusPoint        int            // 紫色+英雄积分
	orangePoint            int            // 橙色英雄积分
	orangePlusPoint        int            // 橙色+英雄积分
	redPoint               int            // 红色英雄积分
	redPlusPoint           int            // 红色+英雄积分
	platinumPoint          int            // 白金英雄积分
	platinumOnePoint       int            // 白金+1英雄积分
	platinumTwoPoint       int            // 白金+2英雄积分
	platinumThreePoint     int            // 白金+3英雄积分
	platinumFourPoint      int            // 白金+4英雄积分
	platinumFivePoint      int            // 白金+5英雄积分
	ladderWeiLowest        int            // 魏国积分榜最低上榜积分
	ladderShuLowest        int            // 蜀国积分榜最低上榜积分
	ladderWuLowest         int            // 吴国积分榜最低上榜积分
	ladderQunLowest        int            // 群雄积分榜最低上榜积分
	ladderStageLowest      int            // 关卡进度榜最低通关关卡ID，关联stage表
	ladderTrialTowerLowest int            // 试炼塔进度榜最低通关试炼塔ID，关联trialTower表
	ladderStageOpen        int            // 关卡进度榜开启通关关卡ID，关联stage表

}

// 当出现首位达成任务进度的角色后，本服所有角色会获得奖励。关联奖励表
func (g LadderGlobal) LadderReward() int {
	return g.ladderReward
}

// 排行榜上榜角色数量
func (g LadderGlobal) LadderRoleNum() int {
	return g.ladderRoleNum
}

// 排行榜数据更新时间/秒
func (g LadderGlobal) LadderRefreshTime() int {
	return g.ladderRefreshTime
}

// 前多少名达成的任务进度的角色显示在日志中
func (g LadderGlobal) LadderTaskPlayNum() int {
	return g.ladderTaskPlayNum
}

// 魏国积分榜对应的种族
func (g LadderGlobal) LadderWei() E_Faction {
	return g.ladderWei
}

// 蜀国积分榜对应的种族
func (g LadderGlobal) LadderShu() E_Faction {
	return g.ladderShu
}

// 吴国积分榜对应的种族
func (g LadderGlobal) LadderWu() E_Faction {
	return g.ladderWu
}

// 群雄积分榜对应的种族
func (g LadderGlobal) LadderQun() E_Faction {
	return g.ladderQun
}

// 关卡进度榜对应的功能ID
func (g LadderGlobal) LadderStage() E_FunctionType {
	return g.ladderStage
}

// 试炼塔进度榜对应的功能ID
func (g LadderGlobal) LadderTrialTower() E_FunctionType {
	return g.ladderTrialTower
}

// 魏国积分榜选择图
func (g LadderGlobal) LadderWeiBg() string {
	return g.ladderWeiBg
}

// 蜀国积分榜选择图
func (g LadderGlobal) LadderShuBg() string {
	return g.ladderShuBg
}

// 吴国积分榜选择图
func (g LadderGlobal) LadderWuBg() string {
	return g.ladderWuBg
}

// 群雄积分榜选择图
func (g LadderGlobal) LadderQunBg() string {
	return g.ladderQunBg
}

// 关卡进度榜对选择图
func (g LadderGlobal) LadderStageBg() string {
	return g.ladderStageBg
}

// 试炼塔进度榜选择图
func (g LadderGlobal) LadderTrialTowerBg() string {
	return g.ladderTrialTowerBg
}

// 蓝色英雄积分
func (g LadderGlobal) BluePoint() int {
	return g.bluePoint
}

// 蓝色+英雄积分
func (g LadderGlobal) BluePlusPoint() int {
	return g.bluePlusPoint
}

// 紫色英雄积分
func (g LadderGlobal) PurplePoint() int {
	return g.purplePoint
}

// 紫色+英雄积分
func (g LadderGlobal) PurplePlusPoint() int {
	return g.purplePlusPoint
}

// 橙色英雄积分
func (g LadderGlobal) OrangePoint() int {
	return g.orangePoint
}

// 橙色+英雄积分
func (g LadderGlobal) OrangePlusPoint() int {
	return g.orangePlusPoint
}

// 红色英雄积分
func (g LadderGlobal) RedPoint() int {
	return g.redPoint
}

// 红色+英雄积分
func (g LadderGlobal) RedPlusPoint() int {
	return g.redPlusPoint
}

// 白金英雄积分
func (g LadderGlobal) PlatinumPoint() int {
	return g.platinumPoint
}

// 白金+1英雄积分
func (g LadderGlobal) PlatinumOnePoint() int {
	return g.platinumOnePoint
}

// 白金+2英雄积分
func (g LadderGlobal) PlatinumTwoPoint() int {
	return g.platinumTwoPoint
}

// 白金+3英雄积分
func (g LadderGlobal) PlatinumThreePoint() int {
	return g.platinumThreePoint
}

// 白金+4英雄积分
func (g LadderGlobal) PlatinumFourPoint() int {
	return g.platinumFourPoint
}

// 白金+5英雄积分
func (g LadderGlobal) PlatinumFivePoint() int {
	return g.platinumFivePoint
}

// 魏国积分榜最低上榜积分
func (g LadderGlobal) LadderWeiLowest() int {
	return g.ladderWeiLowest
}

// 蜀国积分榜最低上榜积分
func (g LadderGlobal) LadderShuLowest() int {
	return g.ladderShuLowest
}

// 吴国积分榜最低上榜积分
func (g LadderGlobal) LadderWuLowest() int {
	return g.ladderWuLowest
}

// 群雄积分榜最低上榜积分
func (g LadderGlobal) LadderQunLowest() int {
	return g.ladderQunLowest
}

// 关卡进度榜最低通关关卡ID，关联stage表
func (g LadderGlobal) LadderStageLowest() int {
	return g.ladderStageLowest
}

// 试炼塔进度榜最低通关试炼塔ID，关联trialTower表
func (g LadderGlobal) LadderTrialTowerLowest() int {
	return g.ladderTrialTowerLowest
}

// 关卡进度榜开启通关关卡ID，关联stage表
func (g LadderGlobal) LadderStageOpen() int {
	return g.ladderStageOpen
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type LadderManager struct {
	Datas  []*Ladder
	Global LadderGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*Ladder // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byLadderType map[E_LadderType][]*Ladder // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	pointByQuality map[E_Quality]int
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newLadderManager() *LadderManager {
	mgr := &LadderManager{
		Datas:        []*Ladder{},
		byID:         map[int]*Ladder{},
		byLadderType: map[E_LadderType][]*Ladder{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>
	mgr.pointByQuality = map[E_Quality]int{}
	//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *LadderManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byLadderType[d.ladderType] = append(mgr.byLadderType[d.ladderType], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>
	mgr.pointByQuality[E_Quality_Blue] = mgr.Global.bluePoint
	mgr.pointByQuality[E_Quality_BluePlus] = mgr.Global.bluePlusPoint
	mgr.pointByQuality[E_Quality_Purple] = mgr.Global.purplePoint
	mgr.pointByQuality[E_Quality_PurplePlus] = mgr.Global.purplePlusPoint
	mgr.pointByQuality[E_Quality_Orange] = mgr.Global.orangePoint
	mgr.pointByQuality[E_Quality_OrangePlus] = mgr.Global.orangePlusPoint
	mgr.pointByQuality[E_Quality_Red] = mgr.Global.redPoint
	mgr.pointByQuality[E_Quality_RedPlus] = mgr.Global.redPlusPoint
	mgr.pointByQuality[E_Quality_Platinum] = mgr.Global.platinumPoint
	mgr.pointByQuality[E_Quality_PlatinumOne] = mgr.Global.platinumOnePoint
	mgr.pointByQuality[E_Quality_PlatinumTwo] = mgr.Global.platinumTwoPoint
	mgr.pointByQuality[E_Quality_PlatinumThree] = mgr.Global.platinumThreePoint
	mgr.pointByQuality[E_Quality_PlatinumFour] = mgr.Global.platinumFourPoint
	mgr.pointByQuality[E_Quality_PlatinumFive] = mgr.Global.platinumFivePoint
	//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func LadderSize() int {
	return ladderMgr.size
}

func (mgr LadderManager) check(path string, row int, sd *Ladder) error {
	if _, ok := ladderMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *LadderManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		switch d.ladderType {
		case E_LadderType_LadderWei:
			if d.value <= 0 {
				success = false
				log.Printf("排行榜表魏国积分榜[%d]的积分[%d]有误", d.id, d.value)
			}

		case E_LadderType_LadderShu:
			if d.value <= 0 {
				success = false
				log.Printf("排行榜表蜀国积分榜[%d]的积分[%d]有误", d.id, d.value)
			}

		case E_LadderType_LadderWu:
			if d.value <= 0 {
				success = false
				log.Printf("排行榜表吴国积分榜[%d]的积分[%d]有误", d.id, d.value)
			}

		case E_LadderType_LadderQun:
			if d.value <= 0 {
				success = false
				log.Printf("排行榜表群雄积分榜[%d]的积分[%d]有误", d.id, d.value)
			}

		case E_LadderType_LadderStage:
			if _, ok := GetStageByID(d.value); !ok {
				success = false
				log.Printf("排行榜表关卡进度榜[%d]的关卡[%d有]误", d.id, d.value)
			}

		case E_LadderType_LadderTrialTower:
			if _, ok := GetTrialTowerByID(d.value); !ok {
				success = false
				log.Printf("排行榜表试炼塔进度榜[%d]的目标值[%d]有误", d.id, d.value)
			}

		default:
			success = false
			log.Printf("排行榜表[%d]的排行榜类型[%d]有误", d.id, d.ladderType)
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *LadderManager) each(f func(sd *Ladder) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *LadderManager) findIf(f func(sd *Ladder) (find bool)) *Ladder {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachLadder(f func(sd Ladder) (continued bool)) {
	for _, sd := range ladderMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindLadderIf(f func(sd Ladder) bool) (Ladder, bool) {
	for _, sd := range ladderMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilLadder, false
}

func GetLadderByID(id int) (Ladder, bool) {
	temp, ok := ladderMgr.byID[id]
	if !ok {
		return nilLadder, false
	}
	return *temp, true
}

func GetLadderByLadderType(ladderType E_LadderType) (vs []Ladder, ok bool) {
	var ds []*Ladder
	ds, ok = ladderMgr.byLadderType[ladderType]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachLadderByLadderType(ladderType E_LadderType, fn func(d Ladder) (continued bool)) bool {
	ds, ok := ladderMgr.byLadderType[ladderType]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetLadderGlobal() LadderGlobal {
	return ladderMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func GetLadderByTypeAndValue(tYpE E_LadderType, value int) (Ladder, bool) {
	ds, ok := ladderMgr.byLadderType[tYpE]
	if !ok {
		return nilLadder, false
	}
	for _, d := range ds {
		if d.value == value {
			return *d, true
		}
	}
	return nilLadder, false
}

func GetLadderFaction(tYpE E_LadderType) E_Faction {
	switch tYpE {
	case E_LadderType_LadderWei:
		return E_Faction_Wei
	case E_LadderType_LadderShu:
		return E_Faction_Shu
	case E_LadderType_LadderWu:
		return E_Faction_Wu
	case E_LadderType_LadderQun:
		return E_Faction_Qun
	}
	return E_Faction_Null
}

func GetLadderLowestHeroPoint(tYpE E_LadderType) int {
	switch tYpE {
	case E_LadderType_LadderWei:
		return ladderMgr.Global.ladderWeiLowest
	case E_LadderType_LadderShu:
		return ladderMgr.Global.ladderShuLowest
	case E_LadderType_LadderWu:
		return ladderMgr.Global.ladderWuLowest
	case E_LadderType_LadderQun:
		return ladderMgr.Global.ladderQunLowest
	}
	return 0
}

func GetFactionLadder(tYpE E_Faction) E_LadderType {
	switch tYpE {
	case E_Faction_Wei:
		return E_LadderType_LadderWei
	case E_Faction_Shu:
		return E_LadderType_LadderShu
	case E_Faction_Wu:
		return E_LadderType_LadderWu
	case E_Faction_Qun:
		return E_LadderType_LadderQun
	}
	return E_LadderType_Null
}

func GetHeroPointByQuality(quality E_Quality) int {
	return ladderMgr.pointByQuality[quality]
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
