package sd

import (
	"fmt"
	"sync"

	"msg"
)

func New_CostThing() *CostThing {
	m := &CostThing{}
	return m
}

func (m CostThing) JsonString() string {
	bs, _ := json.Marshal(m)
	return fmt.Sprintf("{\"CostThing\":%s}", string(bs))
}

func (m *CostThing) ResetEx() {
	m.Type = 0
	m.TID = 0
	m.Num = 0
}

func (m CostThing) Clone() *CostThing {
	n, ok := g_CostThing_Pool.Get().(*CostThing)
	if !ok || n == nil {
		n = &CostThing{}
	}
	n.Type = m.Type
	n.TID = m.TID
	n.Num = m.Num
	return n
}

func Clone_CostThing_Slice(dst []*CostThing, src []*CostThing) []*CostThing {
	for _, i := range dst {
		Put_CostThing(i)
	}
	if len(src) > 0 {
		dst = make([]*CostThing, len(src))
		for i, e := range src {
			if e != nil {
				dst[i] = e.Clone()
			}
		}
	} else {
		//dst = []*CostThing{}
		dst = nil
	}
	return dst
}

func (m CostThing) ToMsg(n *msg.CostThing) *msg.CostThing {
	if n == nil {
		n = msg.Get_CostThing()
	}
	n.Type = int32(m.Type)
	n.TID = int32(m.TID)
	n.Num = int32(m.Num)
	return n
}

var g_CostThing_Pool = sync.Pool{}

func Get_CostThing() *CostThing {
	m, ok := g_CostThing_Pool.Get().(*CostThing)
	if !ok {
		m = New_CostThing()
	} else {
		if m == nil {
			m = New_CostThing()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_CostThing(i interface{}) {
	if m, ok := i.(*CostThing); ok && m != nil {
		g_CostThing_Pool.Put(i)
	}
}

func New_Cost() *Cost {
	m := &Cost{}
	return m
}

func (m Cost) JsonString() string {
	bs, _ := json.Marshal(m)
	return fmt.Sprintf("{\"Cost\":%s}", string(bs))
}

func (m *Cost) ResetEx() {

	for _, i := range m.Things {
		Put_Cost(i)
	}

	//m.Things = []*Cost{}
	m.Things = nil

}

func (m Cost) Clone() *Cost {
	n, ok := g_Cost_Pool.Get().(*Cost)
	if !ok || n == nil {
		n = &Cost{}
	}

	if len(m.Things) > 0 {
		n.Things = make([]*CostThing, len(m.Things))
		for i, e := range m.Things {

			if e != nil {
				n.Things[i] = e.Clone()
			}

		}
	} else {
		//n.Things = []*CostThing{}
		n.Things = nil
	}

	return n
}

func Clone_Cost_Slice(dst []*Cost, src []*Cost) []*Cost {
	for _, i := range dst {
		Put_Cost(i)
	}
	if len(src) > 0 {
		dst = make([]*Cost, len(src))
		for i, e := range src {
			if e != nil {
				dst[i] = e.Clone()
			}
		}
	} else {
		//dst = []*Cost{}
		dst = nil
	}
	return dst
}

func (m Cost) ToMsg(n *msg.Cost) *msg.Cost {
	if n == nil {
		n = msg.Get_Cost()
	}

	if len(m.Things) > 0 {
		n.Things = make([]*msg.CostThing, len(m.Things))
		for i, e := range m.Things {
			if e != nil {
				n.Things[i] = e.ToMsg(n.Things[i])
			} else {
				n.Things[i] = msg.Get_CostThing()
			}
		}
	} else {
		//n.Things = []*CostThing{}
		n.Things = nil
	}

	return n
}

var g_Cost_Pool = sync.Pool{}

func Get_Cost() *Cost {
	m, ok := g_Cost_Pool.Get().(*Cost)
	if !ok {
		m = New_Cost()
	} else {
		if m == nil {
			m = New_Cost()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_Cost(i interface{}) {
	if m, ok := i.(*Cost); ok && m != nil {
		g_Cost_Pool.Put(i)
	}
}
