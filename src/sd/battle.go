package sd

import (
	"fmt"
	"log"
	"math"
	"sync"

	"msg"
)

func IsRatioBattleAttribute(tYpE E_BattleAttribute) bool {
	switch tYpE {
	case E_BattleAttribute_Crit,
		E_BattleAttribute_PhysicReduce,
		E_BattleAttribute_MagicReduce,
		E_BattleAttribute_HurtEnergy,
		E_BattleAttribute_CritDamage,
		E_BattleAttribute_AddRecovery:
		return true
	}
	return false
}

type BattleAttributeValue float64

type BattleAttribute map[E_BattleAttribute]BattleAttributeValue

func ToBattleAttribute(m map[E_BattleAttribute]BattleAttributeValue) *BattleAttribute {
	return (*BattleAttribute)(&m)
}

func NewBattleAttribute() *BattleAttribute {
	return &BattleAttribute{}
}

var gBattleAttributePool sync.Pool

func GetBattleAttribute() *BattleAttribute {
	m, ok := gBattleAttributePool.Get().(*BattleAttribute)
	if !ok {
		m = NewBattleAttribute()
	} else {
		if m == nil {
			m = NewBattleAttribute()
		} else {
			m.Reset()
		}
	}
	return m
}

func PutBattleAttribute(i interface{}) {
	if m, ok := i.(*BattleAttribute); ok && m != nil {
		gBattleAttributePool.Put(i)
	}
}

func (d BattleAttribute) get(tYpE E_BattleAttribute) BattleAttributeValue {
	v, ok := d[tYpE]
	if !ok {
		return 0
	}
	return v
}

func (d BattleAttribute) MustGet(tYpE E_BattleAttribute) BattleAttributeValue {
	if !Check_E_BattleAttribute(tYpE) {
		log.Panicf("invalid E_BattleAttribute: %d", tYpE)
	}
	return d.get(tYpE)
}

func (d *BattleAttribute) set(tYpE E_BattleAttribute, value BattleAttributeValue) {
	if value < 0 {
		value = 0
	}
	(*d)[tYpE] = value
}

func (d *BattleAttribute) MustSet(tYpE E_BattleAttribute, value BattleAttributeValue) {
	if !Check_E_BattleAttribute(tYpE) {
		log.Panicf("invalid E_BattleAttribute: %d", tYpE)
	}
	d.set(tYpE, value)
}

func (d *BattleAttribute) add(tYpE E_BattleAttribute, value BattleAttributeValue) {
	v, ok := (*d)[tYpE]
	if ok {
		v += value
	} else {
		v = value
	}
	if v < 0 {
		v = 0
	}
	(*d)[tYpE] = v
}

func (d *BattleAttribute) MustAdd(tYpE E_BattleAttribute, value BattleAttributeValue) {
	if !Check_E_BattleAttribute(tYpE) {
		log.Panicf("invalid E_BattleAttribute: %d", tYpE)
	}
	d.add(tYpE, value)
}

func (d *BattleAttribute) Reset() {
	for t := range *d {
		delete(*d, t)
	}
}

func (d *BattleAttribute) Multiple(multiple float64) {
	for i, v := range *d {
		fv := float64(v) * multiple
		if fv < 0 {
			fv = 0
		}
		(*d)[i] = BattleAttributeValue(fv)
	}
}

func (d *BattleAttribute) Merge(other BattleAttribute) {
	for i, v := range *d {
		v += other[i]
		if v < 0 {
			v = 0
		}
		(*d)[i] = v
	}
}

func (d BattleAttribute) Size() int {
	return len(d)
}

// 战斗属性换算出来的战斗力
func (d BattleAttribute) Power() int {
	var f64Power float64
	EachPowerRatio(func(tYpE E_BattleAttribute, ratio float64) (continued bool) {
		v, ok := d[tYpE]
		if !ok {
			return true
		}
		f64Power += float64(v) * ratio
		return true
	})
	// 向上取整
	return int(math.Ceil(f64Power))
}

func (d BattleAttribute) ToMsg(m *msg.BattleAttribute) *msg.BattleAttribute {
	if m == nil {
		m = msg.Get_BattleAttribute()
	}

	if len(d) > 0 {
		m.Values = map[int32]int64{}
		for t, v := range d {
			if t > 100 {
				continue
			}
			if !IsRatioBattleAttribute(t) {
				m.Values[int32(t)] = int64(v)
			} else {
				m.Values[int32(t)] = int64(v * 1000)
			}
		}
	} else {
		m.Values = nil
	}

	return m
}

func (d *BattleAttribute) FromMsg(m *msg.BattleAttribute) *BattleAttribute {
	d.Reset()

	for t, v := range m.Values {
		if !IsRatioBattleAttribute(E_BattleAttribute(t)) {
			(*d)[E_BattleAttribute(t)] = BattleAttributeValue(v)
		} else {
			(*d)[E_BattleAttribute(t)] = BattleAttributeValue(v) / 1000.0
		}
	}

	return d
}

func (d BattleAttribute) Equal(other BattleAttribute, float float64) (err error) {
	//if len(d) != len(other) {
	//	return false
	//}

	if float > 0 {
		for k, v := range other {
			dv, ok := d[k]
			if !ok {
				//err = fmt.Errorf("%v not found", k)
				//return
				continue
			}
			if float > 0 {
				min := math.Floor(float64(dv * BattleAttributeValue(1.0-float)))
				max := math.Ceil(float64(dv * BattleAttributeValue(1.0+float)))
				if float64(v) < min || float64(v) > max {
					err = fmt.Errorf("%v not match, svr=%v, cli=%v", k, dv, v)
					return
				}
			}
		}
	} else {
		for k, v := range other {
			dv, ok := d[k]
			if !ok {
				//err = fmt.Errorf("%v not found", k)
				//return
				continue
			}
			if dv != v {
				err = fmt.Errorf("%v not match, svr=%v, cli=%v", k, dv, v)
				return
			}
		}
	}

	return
}

func (d BattleAttribute) JsonString() string {
	ba, _ := json.Marshal(d)
	return string(ba)
}

func (d *BattleAttribute) Ceil() *BattleAttribute {
	for t, v := range *d {
		if !IsRatioBattleAttribute(t) {
			(*d)[t] = BattleAttributeValue(math.Ceil(float64(v)))
		} else {
			(*d)[t] = v
		}
	}
	return d
}
