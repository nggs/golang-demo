// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 技能Buff类型表
type SkillBuffType struct {
	id        int    // buff类型
	text      string // 文本
	text2     string // 文本2（BUFF帮助提示）
	text3     string // 文本3(BUFF配置)
	isControl int    // 是否控制
	stage     []int  // 结算阶段（0=无，1=回合开始，2=攻击结算后，3=复活阶段
	icon      string // 图标
	tYpE      int    // buff类型（0增益，1减益，2不显示）

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewSkillBuffType() *SkillBuffType {
	sd := &SkillBuffType{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// buff类型
func (sd SkillBuffType) ID() int {
	return sd.id
}

// 文本
func (sd SkillBuffType) Text() string {
	return sd.text
}

// 文本2（BUFF帮助提示）
func (sd SkillBuffType) Text2() string {
	return sd.text2
}

// 文本3(BUFF配置)
func (sd SkillBuffType) Text3() string {
	return sd.text3
}

// 是否控制
func (sd SkillBuffType) IsControl() int {
	return sd.isControl
}

// 结算阶段（0=无，1=回合开始，2=攻击结算后，3=复活阶段
func (sd SkillBuffType) Stage() []int {
	return sd.stage
}

// 图标
func (sd SkillBuffType) Icon() string {
	return sd.icon
}

// buff类型（0增益，1减益，2不显示）
func (sd SkillBuffType) Type() int {
	return sd.tYpE
}

func (sd SkillBuffType) Clone() *SkillBuffType {
	n := NewSkillBuffType()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type SkillBuffTypeManager struct {
	Datas []*SkillBuffType

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*SkillBuffType // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newSkillBuffTypeManager() *SkillBuffTypeManager {
	mgr := &SkillBuffTypeManager{
		Datas: []*SkillBuffType{},
		byID:  map[int]*SkillBuffType{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *SkillBuffTypeManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func SkillBuffTypeSize() int {
	return skillBuffTypeMgr.size
}

func (mgr SkillBuffTypeManager) check(path string, row int, sd *SkillBuffType) error {
	if _, ok := skillBuffTypeMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *SkillBuffTypeManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *SkillBuffTypeManager) each(f func(sd *SkillBuffType) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *SkillBuffTypeManager) findIf(f func(sd *SkillBuffType) (find bool)) *SkillBuffType {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachSkillBuffType(f func(sd SkillBuffType) (continued bool)) {
	for _, sd := range skillBuffTypeMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindSkillBuffTypeIf(f func(sd SkillBuffType) bool) (SkillBuffType, bool) {
	for _, sd := range skillBuffTypeMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilSkillBuffType, false
}

func GetSkillBuffTypeByID(id int) (SkillBuffType, bool) {
	temp, ok := skillBuffTypeMgr.byID[id]
	if !ok {
		return nilSkillBuffType, false
	}
	return *temp, true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
