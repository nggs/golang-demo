// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 道具表
type Item struct {
	id         int        // 物品ID
	name       string     // 物品名称
	desc       string     // 物品说明
	tYpE       E_ItemType // 物品类型，1=招募令
	typeDesc   string     // 物品类型描述
	icon       string     // 物品显示图标
	effect     string     // 物品特效（填写特效文件名）
	quality    E_Quality  // 物品品质，关联品质表
	bagType    E_BAG_TYPE // 背包存储类型 1=道具4=武将碎片
	usable     bool       // 是否可在背包中使用0=否1=是
	reward     int        // 道具使用后获取掉落，对应reward组id
	useJump    int        // 在背包中使用，点击后跳转到指定页面
	redPoint   bool       // 背包是否红点提示 0=否1=是
	smallIcon  string     // 物品显示小图标
	acquire    int        // 获取渠道 关联acquire表 为0表示没有渠道
	itemRecord int        // 是否需要记录获取/消耗
	data       []int      // 未定义参数，如碎片，此处填收集数量。

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewItem() *Item {
	sd := &Item{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 物品ID
func (sd Item) ID() int {
	return sd.id
}

// 物品名称
func (sd Item) Name() string {
	return sd.name
}

// 物品说明
func (sd Item) Desc() string {
	return sd.desc
}

// 物品类型，1=招募令
func (sd Item) Type() E_ItemType {
	return sd.tYpE
}

// 物品类型描述
func (sd Item) TypeDesc() string {
	return sd.typeDesc
}

// 物品显示图标
func (sd Item) Icon() string {
	return sd.icon
}

// 物品特效（填写特效文件名）
func (sd Item) Effect() string {
	return sd.effect
}

// 物品品质，关联品质表
func (sd Item) Quality() E_Quality {
	return sd.quality
}

// 背包存储类型 1=道具4=武将碎片
func (sd Item) BagType() E_BAG_TYPE {
	return sd.bagType
}

// 是否可在背包中使用0=否1=是
func (sd Item) Usable() bool {
	return sd.usable
}

// 道具使用后获取掉落，对应reward组id
func (sd Item) Reward() int {
	return sd.reward
}

// 在背包中使用，点击后跳转到指定页面
func (sd Item) UseJump() int {
	return sd.useJump
}

// 背包是否红点提示 0=否1=是
func (sd Item) RedPoint() bool {
	return sd.redPoint
}

// 物品显示小图标
func (sd Item) SmallIcon() string {
	return sd.smallIcon
}

// 获取渠道 关联acquire表 为0表示没有渠道
func (sd Item) Acquire() int {
	return sd.acquire
}

// 是否需要记录获取/消耗
func (sd Item) ItemRecord() int {
	return sd.itemRecord
}

// 未定义参数，如碎片，此处填收集数量。
func (sd Item) Data() []int {
	return sd.data
}

func (sd Item) Clone() *Item {
	n := NewItem()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 道具表全局属性
type ItemGlobal struct {
	acceleratorSilverCoin    []int // 急行军令（银币）ID 段
	acceleratorHeroExp       []int // 急行军令（武将经验）ID 段
	acceleratorLimitPassPill []int // 急行军令（武将突破丹）ID 段
	acceleratorAll           []int // 急行军令（三者都掉）ID 段
	limitPassPill            int   // 武将突破丹物品ID
	arenaKey                 int   // 普通竞技场门票物品ID
	revivePill               int   // 艾雅之泪物品ID
	rescueTimes              int   // 救援令
	equipmentDungeons        int   // 装备副本挑战令
	heroDungeons             int   // 武将副本挑战令
	silverCoinDungeons       int   // 银币副本挑战令
	heroExpDungeons          int   // 武将经验副本挑战令
	limitPassPillDungeons    int   // 突破丹副本挑战令
	legendArenaKey           int   // 高阶竞技场战书
	heroBackWei              int   // 魏国武将回退返还的武将卡id
	heroBackShu              int   // 蜀国武将回退返还的武将卡id
	heroBackWu               int   // 吴国武将回退返还的武将卡id
	heroBackQun              int   // 群国武将回退返还的武将卡id
	heroBackXian             int   // 仙族武将回退返还的武将卡id
	heroBackMo               int   // 魔族武将回退返还的武将卡id

}

// 急行军令（银币）ID 段
func (g ItemGlobal) AcceleratorSilverCoin() []int {
	return g.acceleratorSilverCoin
}

// 急行军令（武将经验）ID 段
func (g ItemGlobal) AcceleratorHeroExp() []int {
	return g.acceleratorHeroExp
}

// 急行军令（武将突破丹）ID 段
func (g ItemGlobal) AcceleratorLimitPassPill() []int {
	return g.acceleratorLimitPassPill
}

// 急行军令（三者都掉）ID 段
func (g ItemGlobal) AcceleratorAll() []int {
	return g.acceleratorAll
}

// 武将突破丹物品ID
func (g ItemGlobal) LimitPassPill() int {
	return g.limitPassPill
}

// 普通竞技场门票物品ID
func (g ItemGlobal) ArenaKey() int {
	return g.arenaKey
}

// 艾雅之泪物品ID
func (g ItemGlobal) RevivePill() int {
	return g.revivePill
}

// 救援令
func (g ItemGlobal) RescueTimes() int {
	return g.rescueTimes
}

// 装备副本挑战令
func (g ItemGlobal) EquipmentDungeons() int {
	return g.equipmentDungeons
}

// 武将副本挑战令
func (g ItemGlobal) HeroDungeons() int {
	return g.heroDungeons
}

// 银币副本挑战令
func (g ItemGlobal) SilverCoinDungeons() int {
	return g.silverCoinDungeons
}

// 武将经验副本挑战令
func (g ItemGlobal) HeroExpDungeons() int {
	return g.heroExpDungeons
}

// 突破丹副本挑战令
func (g ItemGlobal) LimitPassPillDungeons() int {
	return g.limitPassPillDungeons
}

// 高阶竞技场战书
func (g ItemGlobal) LegendArenaKey() int {
	return g.legendArenaKey
}

// 魏国武将回退返还的武将卡id
func (g ItemGlobal) HeroBackWei() int {
	return g.heroBackWei
}

// 蜀国武将回退返还的武将卡id
func (g ItemGlobal) HeroBackShu() int {
	return g.heroBackShu
}

// 吴国武将回退返还的武将卡id
func (g ItemGlobal) HeroBackWu() int {
	return g.heroBackWu
}

// 群国武将回退返还的武将卡id
func (g ItemGlobal) HeroBackQun() int {
	return g.heroBackQun
}

// 仙族武将回退返还的武将卡id
func (g ItemGlobal) HeroBackXian() int {
	return g.heroBackXian
}

// 魔族武将回退返还的武将卡id
func (g ItemGlobal) HeroBackMo() int {
	return g.heroBackMo
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type ItemManager struct {
	Datas  []*Item
	Global ItemGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*Item // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byType    map[E_ItemType][]*Item // NormalIndex
	byQuality map[E_Quality][]*Item  // NormalIndex
	byBagType map[E_BAG_TYPE][]*Item // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	iron10TID    int
	iron100TID   int
	iron1000TID  int
	iron10000TID int
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newItemManager() *ItemManager {
	mgr := &ItemManager{
		Datas:  []*Item{},
		byID:   map[int]*Item{},
		byType: map[E_ItemType][]*Item{}, byQuality: map[E_Quality][]*Item{}, byBagType: map[E_BAG_TYPE][]*Item{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *ItemManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byType[d.tYpE] = append(mgr.byType[d.tYpE], d)
		mgr.byQuality[d.quality] = append(mgr.byQuality[d.quality], d)
		mgr.byBagType[d.bagType] = append(mgr.byBagType[d.bagType], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func ItemSize() int {
	return itemMgr.size
}

func (mgr ItemManager) check(path string, row int, sd *Item) error {
	if _, ok := itemMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>
	switch sd.tYpE {
	case E_ItemType_Iron:
		// 铸铁
		if sd.data[0] <= 0 {
			return fmt.Errorf("[%s]铸铁[%d]的经验值[%d]错误", path, sd.id, sd.data[0])
		}
	case E_ItemType_Accelerator:
		// 急行军令
		if !Check_E_AcceleratorType_I(sd.data[0]) {
			return fmt.Errorf("[%s]急行军令[%d]的类型[%d]错误", path, sd.id, sd.data[0])
		}
		if sd.data[1] <= 0 {
			return fmt.Errorf("[%s]急行军令[%d]的时长[%d]错误", path, sd.id, sd.data[0])
		}
	case E_ItemType_HeroDebris:
		// 武将碎片
		if sd.data[0] <= 0 {
			return fmt.Errorf("[%s]武将碎片[%d]的使用数量[%d]错误", path, sd.id, sd.data[0])
		}
	}
	//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *ItemManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if _, ok := GetQualityByID(d.quality); !ok {
			success = false
			log.Printf("物品表物品[%d]的品质[%d]找不到对应配置\n", d.id, d.quality)
		}

		switch d.tYpE {
		case E_ItemType_Accelerator:
			// 急行军令
			if !Check_E_AcceleratorType_I(d.data[0]) {
				success = false
				log.Printf("物品表急行军令[%d]的类型[%d]配置有误\n", d.id, d.data[0])
			}
			if d.data[1] < 0 {
				success = false
				log.Printf("物品表急行军令[%d]的时长[%d]配置有误\n", d.id, d.data[1])
			}
		case E_ItemType_GiftPack:
			// 礼包
			// 检查礼包对应的掉落组是否存在
			if _, ok := GetLootByGroup(d.reward); !ok {
				success = false
				log.Printf("物品表礼包[%d]的奖励组数据[%d]不存在\n", d.id, d.reward)
			}
		case E_ItemType_Iron:
			// 铸铁
			// 检查铸铁替代经验值是否正确
			if d.data[0] <= 0 {
				success = false
				log.Printf("物品表铸铁[%d]的经验字段[%d]数字错误\n", d.id, d.data[0])
			}
			switch d.data[0] {
			case 10:
				mgr.iron10TID = d.id
			case 100:
				mgr.iron100TID = d.id
			case 1000:
				mgr.iron1000TID = d.id
			case 10000:
				mgr.iron10000TID = d.id
			}
		}
	}
	if mgr.iron10TID == 0 {
		success = false
		log.Printf("物品表未找到经验值10的铸铁\n")
	}
	if mgr.iron100TID == 0 {
		success = false
		log.Printf("物品表未找到经验值100的铸铁\n")
	}
	if mgr.iron1000TID == 0 {
		success = false
		log.Printf("物品表未找到经验值1000的铸铁\n")
	}
	if mgr.iron10000TID == 0 {
		success = false
		log.Printf("物品表未找到经验值10000的铸铁\n")
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *ItemManager) each(f func(sd *Item) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *ItemManager) findIf(f func(sd *Item) (find bool)) *Item {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachItem(f func(sd Item) (continued bool)) {
	for _, sd := range itemMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindItemIf(f func(sd Item) bool) (Item, bool) {
	for _, sd := range itemMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilItem, false
}

func GetItemByID(id int) (Item, bool) {
	temp, ok := itemMgr.byID[id]
	if !ok {
		return nilItem, false
	}
	return *temp, true
}

func GetItemByType(tYpE E_ItemType) (vs []Item, ok bool) {
	var ds []*Item
	ds, ok = itemMgr.byType[tYpE]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachItemByType(tYpE E_ItemType, fn func(d Item) (continued bool)) bool {
	ds, ok := itemMgr.byType[tYpE]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetItemByQuality(quality E_Quality) (vs []Item, ok bool) {
	var ds []*Item
	ds, ok = itemMgr.byQuality[quality]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachItemByQuality(quality E_Quality, fn func(d Item) (continued bool)) bool {
	ds, ok := itemMgr.byQuality[quality]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetItemByBagType(bagType E_BAG_TYPE) (vs []Item, ok bool) {
	var ds []*Item
	ds, ok = itemMgr.byBagType[bagType]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachItemByBagType(bagType E_BAG_TYPE, fn func(d Item) (continued bool)) bool {
	ds, ok := itemMgr.byBagType[bagType]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetItemGlobal() ItemGlobal {
	return itemMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func EquipStrengthenExpToIronNum(exp int) (irons map[int]int) {
	if exp <= 0 {
		return
	}
	irons = map[int]int{}
	if exp/10000 > 0 {
		irons[itemMgr.iron10000TID] += exp / 10000
		exp = exp % 10000
	}
	if exp/1000 > 0 {
		irons[itemMgr.iron1000TID] += exp / 1000
		exp = exp % 1000
	}
	if exp/100 > 0 {
		irons[itemMgr.iron100TID] += exp / 100
		exp = exp % 100
	}
	if exp/10 > 0 {
		irons[itemMgr.iron10TID] += exp / 10
		exp = exp % 10
	}
	return
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
