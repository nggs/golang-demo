//+build ignore

package sd

import (
	"bytes"
	"encoding/csv"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
	"testing"

	"gitee.com/nggs/util"
)

func testReward(t *testing.T) {
	if !LoadAll(TestJsonDir) {
		t.Error("加载静态数据失败")
		return
	}
	if !AfterLoadAll(TestJsonDir) {
		t.Error("加载静态表后处理失败")
		return
	}

	groupID := 11002
	num := 10000
	dice, ok := rewardMgr.dices[groupID]
	if !ok {
		return
	}

	totalRewardHits := map[string]int{}

	for i := 0; i < num; i++ {
		rewardHits := map[string]int{}
		for j := 0; j < 100; j++ {
			for _, reward := range dice.mustDropRewards {
				rewardHits[reward.itemName] += 1
			}
			if len(dice.randomDropRewards) > 0 {
				times := util.RandomInt(dice.minTimes, dice.maxTimes)
				for i := 0; i < times; i++ {
					reward, ok := dice.super.Roll().(*Reward)
					if !ok || reward == nil {
						t.Errorf("奖励组[%d]随机奖励出错", groupID)
						return
					}
					rewardHits[reward.itemName] += 1
					totalRewardHits[reward.itemName] += 1
				}
			}
		}
		var purpleNum int
		for heroName, hitHum := range rewardHits {
			if !strings.Contains(heroName, "紫色") {
				continue
			}
			purpleNum += hitHum
		}
		log.Printf("第%d轮获得%d个紫色", i+1, purpleNum)
	}

	buffer := new(bytes.Buffer)
	writer := csv.NewWriter(buffer)
	err := writer.Write([]string{"英雄", "次数"})
	if err != nil {
		t.Errorf("写入表头失败：%s", err)
		return
	}

	for heroName, hitNum := range totalRewardHits {
		err = writer.Write([]string{
			heroName,
			strconv.Itoa(hitNum),
		})
		if err != nil {
			t.Errorf("写入数据失败[%s]=[%d]：%s", heroName, hitNum, err)
			continue
		}
	}

	writer.Flush()

	err = ioutil.WriteFile("reward_test.csv", bytes.Join([][]byte{[]byte("\xEF\xBB\xBF"), buffer.Bytes()}, nil), 0766)
}
