// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 全局表
type Global struct {

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewGlobal() *Global {
	sd := &Global{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

func (sd Global) Clone() *Global {
	n := NewGlobal()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 全局表全局属性
type GlobalGlobal struct {
	endPageAutoClose             int       // 结算界面几秒后自动关闭
	openReward                   int       // 游戏开场赠送英雄（掉落组）
	powerHp                      float64   // 生命对应的战斗力系数
	powerAtk                     float64   // 攻击对应的战斗力系数
	powerDef                     float64   // 防御对应的战斗力系数
	powerCrit                    float64   // 暴击对应的战斗力系数
	powerHit                     float64   // 命中对应的战斗力系数
	powerMiss                    float64   // 闪避对应的战斗力系数
	powerSpeed                   float64   // 急速对应的战斗力系数
	powerRecover                 float64   // 每秒恢复对应的战斗力系数
	powerPhysicReduce            float64   // 物理减伤率对应的战斗力系数
	powerMagicReduce             float64   // 魔法减伤率对应的战斗力系数
	powerLifeLeech               float64   // 吸血等级对应的战斗力系数
	powerHurtEnergy              float64   // 受伤回能对应的战斗力系数
	powerCritDamage              float64   // 暴击伤害加成对应的战斗力系数
	battleLimitTime              int       // 战斗限制时间/秒
	insteadFaction               E_Faction // 什么阵营可以代替其他阵营
	factionRestraint             int       // 阵营伤害加成，千分数
	aidHeroNum                   int       // 外援英雄数量
	aidHeroUseTime               int       // 角色的外援英雄最多只能被其他角色借用几次
	friendNumMax                 int       // 最多可以添加多少名好友
	friendRequestNumMax          int       // 好友申请记录最多保存多少条
	companionPointsItem          int       // 好友点物品ID，关联货币经验表currency
	companionPointsEachNum       int       // 每次赠送的好友数量
	companionPointsHandselNumMax int       // 每天至多可赠送多少个好友的好友点
	companionPointsReceiveNumMax int       // 每天至多领取多少个好友赠送的好友点
	blacklistNumMax              int       // 黑名单数量上限
	openTime                     int       // 功能在当日的几点开启
	initialAvatar                int       // 初始角色头像，关联英雄组表ID
	avatarFrame                  string    // 初始角色头像框，填写文件位置
	initialName                  string    // 初始角色名
	characterNum                 int       // 个性签名，字符数量上限
	roleNameMaxLength            int       // 角色名字最大字数
	roleNameRename               int       // 角色改名消耗的元宝数量
	mailTime                     int       // 邮件最长存放时间/秒
	chatLimit                    int       // 本地最多保存的最近聊天消息条数
	chatTimeout                  int       // 本地最长保存的最近聊天消息天数
	chatCoolDown                 int       // 两次发送消息之间的间隔/秒
	myChatLimit                  int       // 服务端最多保存的最近私聊条数
	myChatTimeout                int       // 服务端最长保存的最近私聊天数
	chatWordLimit                int       // 单条消息最长字符数
	recommendFriendsLevel        int       // 推荐的好友与角色等级差上限
	recommendFriendsLimits       int       // 每次推荐的好友数量上限
	recommendFriendsTime         int       // 离线天数小于这个值的角色才可以被推荐
	guideQuestAutoClose          int       // 有未完成的引导任务时指指示持续时间
	blueHeroDebrisFirstReward    int       // 随机蓝色武将碎片第一次合成的掉落
	blueHeroDebrisItemID         int       // 随机蓝色武将碎片物品id
	miniProgramReward            int       // 我的小程序进入随机奖励
	chargeHideOpenStage          int       // 正在挑战的关卡到指定关卡后，开启充值功能，关联stage
	sleepEnemyWeaken             float64   // 角色在卡关后每日削弱多少攻防
	sleepEnemyWeakenMax          float64   // 角色在卡关后削弱攻防上限
	heroBackCost                 int       // 武将回退消耗元宝
	chargeJumpActivity1          int       // 充值跳转活动，在创角14天内跳转到指定活动1
	chargeJumpActivity2          int       // 充值跳转活动，在创角14天后跳转到指定活动2
	chargeJumpCreateDay          int       // 充值跳转活动，创角几天后跳转指定活动2
	newRoleLoginReward           int       // 新角色首次登录游戏手动领取的奖励
	guideFingerBegin             int       // 手指点击3下挑战按键的开始关卡
	guideFingerEnd               int       // 手指点击3下挑战按键的结束关卡

}

// 结算界面几秒后自动关闭
func (g GlobalGlobal) EndPageAutoClose() int {
	return g.endPageAutoClose
}

// 游戏开场赠送英雄（掉落组）
func (g GlobalGlobal) OpenReward() int {
	return g.openReward
}

// 生命对应的战斗力系数
func (g GlobalGlobal) PowerHp() float64 {
	return g.powerHp
}

// 攻击对应的战斗力系数
func (g GlobalGlobal) PowerAtk() float64 {
	return g.powerAtk
}

// 防御对应的战斗力系数
func (g GlobalGlobal) PowerDef() float64 {
	return g.powerDef
}

// 暴击对应的战斗力系数
func (g GlobalGlobal) PowerCrit() float64 {
	return g.powerCrit
}

// 命中对应的战斗力系数
func (g GlobalGlobal) PowerHit() float64 {
	return g.powerHit
}

// 闪避对应的战斗力系数
func (g GlobalGlobal) PowerMiss() float64 {
	return g.powerMiss
}

// 急速对应的战斗力系数
func (g GlobalGlobal) PowerSpeed() float64 {
	return g.powerSpeed
}

// 每秒恢复对应的战斗力系数
func (g GlobalGlobal) PowerRecover() float64 {
	return g.powerRecover
}

// 物理减伤率对应的战斗力系数
func (g GlobalGlobal) PowerPhysicReduce() float64 {
	return g.powerPhysicReduce
}

// 魔法减伤率对应的战斗力系数
func (g GlobalGlobal) PowerMagicReduce() float64 {
	return g.powerMagicReduce
}

// 吸血等级对应的战斗力系数
func (g GlobalGlobal) PowerLifeLeech() float64 {
	return g.powerLifeLeech
}

// 受伤回能对应的战斗力系数
func (g GlobalGlobal) PowerHurtEnergy() float64 {
	return g.powerHurtEnergy
}

// 暴击伤害加成对应的战斗力系数
func (g GlobalGlobal) PowerCritDamage() float64 {
	return g.powerCritDamage
}

// 战斗限制时间/秒
func (g GlobalGlobal) BattleLimitTime() int {
	return g.battleLimitTime
}

// 什么阵营可以代替其他阵营
func (g GlobalGlobal) InsteadFaction() E_Faction {
	return g.insteadFaction
}

// 阵营伤害加成，千分数
func (g GlobalGlobal) FactionRestraint() int {
	return g.factionRestraint
}

// 外援英雄数量
func (g GlobalGlobal) AidHeroNum() int {
	return g.aidHeroNum
}

// 角色的外援英雄最多只能被其他角色借用几次
func (g GlobalGlobal) AidHeroUseTime() int {
	return g.aidHeroUseTime
}

// 最多可以添加多少名好友
func (g GlobalGlobal) FriendNumMax() int {
	return g.friendNumMax
}

// 好友申请记录最多保存多少条
func (g GlobalGlobal) FriendRequestNumMax() int {
	return g.friendRequestNumMax
}

// 好友点物品ID，关联货币经验表currency
func (g GlobalGlobal) CompanionPointsItem() int {
	return g.companionPointsItem
}

// 每次赠送的好友数量
func (g GlobalGlobal) CompanionPointsEachNum() int {
	return g.companionPointsEachNum
}

// 每天至多可赠送多少个好友的好友点
func (g GlobalGlobal) CompanionPointsHandselNumMax() int {
	return g.companionPointsHandselNumMax
}

// 每天至多领取多少个好友赠送的好友点
func (g GlobalGlobal) CompanionPointsReceiveNumMax() int {
	return g.companionPointsReceiveNumMax
}

// 黑名单数量上限
func (g GlobalGlobal) BlacklistNumMax() int {
	return g.blacklistNumMax
}

// 功能在当日的几点开启
func (g GlobalGlobal) OpenTime() int {
	return g.openTime
}

// 初始角色头像，关联英雄组表ID
func (g GlobalGlobal) InitialAvatar() int {
	return g.initialAvatar
}

// 初始角色头像框，填写文件位置
func (g GlobalGlobal) AvatarFrame() string {
	return g.avatarFrame
}

// 初始角色名
func (g GlobalGlobal) InitialName() string {
	return g.initialName
}

// 个性签名，字符数量上限
func (g GlobalGlobal) CharacterNum() int {
	return g.characterNum
}

// 角色名字最大字数
func (g GlobalGlobal) RoleNameMaxLength() int {
	return g.roleNameMaxLength
}

// 角色改名消耗的元宝数量
func (g GlobalGlobal) RoleNameRename() int {
	return g.roleNameRename
}

// 邮件最长存放时间/秒
func (g GlobalGlobal) MailTime() int {
	return g.mailTime
}

// 本地最多保存的最近聊天消息条数
func (g GlobalGlobal) ChatLimit() int {
	return g.chatLimit
}

// 本地最长保存的最近聊天消息天数
func (g GlobalGlobal) ChatTimeout() int {
	return g.chatTimeout
}

// 两次发送消息之间的间隔/秒
func (g GlobalGlobal) ChatCoolDown() int {
	return g.chatCoolDown
}

// 服务端最多保存的最近私聊条数
func (g GlobalGlobal) MyChatLimit() int {
	return g.myChatLimit
}

// 服务端最长保存的最近私聊天数
func (g GlobalGlobal) MyChatTimeout() int {
	return g.myChatTimeout
}

// 单条消息最长字符数
func (g GlobalGlobal) ChatWordLimit() int {
	return g.chatWordLimit
}

// 推荐的好友与角色等级差上限
func (g GlobalGlobal) RecommendFriendsLevel() int {
	return g.recommendFriendsLevel
}

// 每次推荐的好友数量上限
func (g GlobalGlobal) RecommendFriendsLimits() int {
	return g.recommendFriendsLimits
}

// 离线天数小于这个值的角色才可以被推荐
func (g GlobalGlobal) RecommendFriendsTime() int {
	return g.recommendFriendsTime
}

// 有未完成的引导任务时指指示持续时间
func (g GlobalGlobal) GuideQuestAutoClose() int {
	return g.guideQuestAutoClose
}

// 随机蓝色武将碎片第一次合成的掉落
func (g GlobalGlobal) BlueHeroDebrisFirstReward() int {
	return g.blueHeroDebrisFirstReward
}

// 随机蓝色武将碎片物品id
func (g GlobalGlobal) BlueHeroDebrisItemID() int {
	return g.blueHeroDebrisItemID
}

// 我的小程序进入随机奖励
func (g GlobalGlobal) MiniProgramReward() int {
	return g.miniProgramReward
}

// 正在挑战的关卡到指定关卡后，开启充值功能，关联stage
func (g GlobalGlobal) ChargeHideOpenStage() int {
	return g.chargeHideOpenStage
}

// 角色在卡关后每日削弱多少攻防
func (g GlobalGlobal) SleepEnemyWeaken() float64 {
	return g.sleepEnemyWeaken
}

// 角色在卡关后削弱攻防上限
func (g GlobalGlobal) SleepEnemyWeakenMax() float64 {
	return g.sleepEnemyWeakenMax
}

// 武将回退消耗元宝
func (g GlobalGlobal) HeroBackCost() int {
	return g.heroBackCost
}

// 充值跳转活动，在创角14天内跳转到指定活动1
func (g GlobalGlobal) ChargeJumpActivity1() int {
	return g.chargeJumpActivity1
}

// 充值跳转活动，在创角14天后跳转到指定活动2
func (g GlobalGlobal) ChargeJumpActivity2() int {
	return g.chargeJumpActivity2
}

// 充值跳转活动，创角几天后跳转指定活动2
func (g GlobalGlobal) ChargeJumpCreateDay() int {
	return g.chargeJumpCreateDay
}

// 新角色首次登录游戏手动领取的奖励
func (g GlobalGlobal) NewRoleLoginReward() int {
	return g.newRoleLoginReward
}

// 手指点击3下挑战按键的开始关卡
func (g GlobalGlobal) GuideFingerBegin() int {
	return g.guideFingerBegin
}

// 手指点击3下挑战按键的结束关卡
func (g GlobalGlobal) GuideFingerEnd() int {
	return g.guideFingerEnd
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type GlobalManager struct {
	Datas  []*Global
	Global GlobalGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	powerRatios map[E_BattleAttribute]float64
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newGlobalManager() *GlobalManager {
	mgr := &GlobalManager{
		Datas: []*Global{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>
	mgr.powerRatios = map[E_BattleAttribute]float64{}
	//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *GlobalManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func GlobalSize() int {
	return globalMgr.size
}

func (mgr GlobalManager) check(path string, row int, sd *Global) error {

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *GlobalManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	reward, ok := GenerateReward(mgr.Global.openReward)
	if !ok {
		success = false
		log.Printf("全局表初始奖励[%d]生成失败\n", mgr.Global.openReward)
		return
	}
	reward.Each(func(thing *RewardThing) (continued bool) {
		if thing.TID <= 0 {
			return true
		}
		if thing.Num <= 0 {
			success = false
			log.Printf("全局表初始奖励[%d]奖励项[%d]的数量有误\n", mgr.Global.openReward, thing.TID)
			return true
		}
		switch thing.Type {
		case E_ItemBigType_Currency:
			if _, ok := GetCurrencyByID(thing.TID); !ok {
				success = false
				log.Printf("全局表初始奖励[%d]奖励货币[%d]的静态表数据不存在\n", mgr.Global.openReward, thing.TID)
			}
		case E_ItemBigType_Item:
			if _, ok := GetItemByID(thing.TID); !ok {
				success = false
				log.Printf("全局表初始奖励[%d]奖励物品[%d]的静态表数据不存在\n", mgr.Global.openReward, thing.TID)
			}
		case E_ItemBigType_Hero:
			if _, ok := GetHeroBasicByID(thing.TID); !ok {
				success = false
				log.Printf("全局表初始奖励[%d]奖励英雄[%d]的静态表数据不存在\n", mgr.Global.openReward, thing.TID)
			}
		case E_ItemBigType_Equip:
			if _, ok := GetEquipmentByID(thing.TID); !ok {
				success = false
				log.Printf("全局表初始奖励[%d]奖励装备[%d]的静态表数据不存在\n", mgr.Global.openReward, thing.TID)
			}
		}
		return true
	})

	mgr.powerRatios[E_BattleAttribute_Hp] = mgr.Global.powerHp
	mgr.powerRatios[E_BattleAttribute_Atk] = mgr.Global.powerAtk
	mgr.powerRatios[E_BattleAttribute_Def] = mgr.Global.powerDef
	mgr.powerRatios[E_BattleAttribute_Crit] = mgr.Global.powerCrit
	mgr.powerRatios[E_BattleAttribute_Hit] = mgr.Global.powerHit
	mgr.powerRatios[E_BattleAttribute_Miss] = mgr.Global.powerMiss
	mgr.powerRatios[E_BattleAttribute_Speed] = mgr.Global.powerSpeed
	mgr.powerRatios[E_BattleAttribute_Recover] = mgr.Global.powerRecover
	mgr.powerRatios[E_BattleAttribute_PhysicReduce] = mgr.Global.powerPhysicReduce
	mgr.powerRatios[E_BattleAttribute_MagicReduce] = mgr.Global.powerMagicReduce
	mgr.powerRatios[E_BattleAttribute_LifeLeech] = mgr.Global.powerLifeLeech
	mgr.powerRatios[E_BattleAttribute_HurtEnergy] = mgr.Global.powerHurtEnergy
	mgr.powerRatios[E_BattleAttribute_CritDamage] = mgr.Global.powerCritDamage

	if _, ok := GetLootByGroup(mgr.Global.miniProgramReward); !ok {
		success = false
		log.Printf("全局表未找到微信我的小程序奖励，奖励组id=%d", mgr.Global.miniProgramReward)
	}

	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *GlobalManager) each(f func(sd *Global) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *GlobalManager) findIf(f func(sd *Global) (find bool)) *Global {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachGlobal(f func(sd Global) (continued bool)) {
	for _, sd := range globalMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindGlobalIf(f func(sd Global) bool) (Global, bool) {
	for _, sd := range globalMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilGlobal, false
}

func GetGlobalGlobal() GlobalGlobal {
	return globalMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func GetPowerRatio(tYpE E_BattleAttribute) (ratio float64, ok bool) {
	ratio, ok = globalMgr.powerRatios[tYpE]
	return
}

func EachPowerRatio(fn func(tYpE E_BattleAttribute, ratio float64) (continued bool)) {
	if fn == nil {
		return
	}
	for tYpE, ratio := range globalMgr.powerRatios {
		if !fn(tYpE, ratio) {
			break
		}
	}
}

func GetTodayRefreshTime() time.Time {
	//获取每天重置时刻，例：235959（六位数，时、分、秒），23点59分59秒发放
	hour := globalMgr.Global.openTime / 10000
	min := (globalMgr.Global.openTime % 10000) / 100
	sec := globalMgr.Global.openTime % 100
	now := time.Now()
	return time.Date(now.Year(), now.Month(), now.Day(), hour, min, sec, 0, now.Location())
}

func GetTodayZeroClock() time.Time {
	//获取每天零点时刻
	now := time.Now()
	return time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
}

func GetNextDayRefreshTime() time.Time {
	//获取下一次重置时刻
	now := time.Now()
	todayRefreshTime := GetTodayRefreshTime()
	if now.After(todayRefreshTime) {
		return todayRefreshTime.AddDate(0, 0, 1)
	}
	return todayRefreshTime
}

func GetNextZeroClock() time.Time {
	//获取下一个零点时刻
	now := time.Now()
	todayZeroClock := GetTodayZeroClock()
	if now.After(todayZeroClock) {
		return todayZeroClock.AddDate(0, 0, 1)
	}
	return todayZeroClock
}

func GenWeChatMiniAppReward() (*Reward, bool) {
	return GenerateReward(globalMgr.Global.miniProgramReward)
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
