// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 掉落表
type Loot struct {
	id       int           // 掉落主键ID
	group    int           // 掉落组ID
	num      int           // 掉落的数量
	dataFrom E_ItemBigType // 掉落取哪张表 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
	itemId   int           // 掉落的物品ID
	mustDrop bool          // 是否必掉 0=随机 1=必掉
	minTimes int           // 掉落的最少次数
	maxTimes int           // 掉落的最多次数
	weight   int           // 掉落的权重
	flash    bool          // 图标的光圈环绕光效 1紫色、2橙色、3红色、4白金 0没有光效
	faction  E_Faction     // 生成指定阵营的装备 0按照原规则随机生成阵营 1魏2蜀3吴4群5仙6魔7无阵营 此字段仅当dataFrom=4装备表时有效
	level    int           // 指定英雄等级

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	simple *RewardThing
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewLoot() *Loot {
	sd := &Loot{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 掉落主键ID
func (sd Loot) ID() int {
	return sd.id
}

// 掉落组ID
func (sd Loot) Group() int {
	return sd.group
}

// 掉落的数量
func (sd Loot) Num() int {
	return sd.num
}

// 掉落取哪张表 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
func (sd Loot) DataFrom() E_ItemBigType {
	return sd.dataFrom
}

// 掉落的物品ID
func (sd Loot) ItemID() int {
	return sd.itemId
}

// 是否必掉 0=随机 1=必掉
func (sd Loot) MustDrop() bool {
	return sd.mustDrop
}

// 掉落的最少次数
func (sd Loot) MinTimes() int {
	return sd.minTimes
}

// 掉落的最多次数
func (sd Loot) MaxTimes() int {
	return sd.maxTimes
}

// 掉落的权重
func (sd Loot) Weight() int {
	return sd.weight
}

// 图标的光圈环绕光效 1紫色、2橙色、3红色、4白金 0没有光效
func (sd Loot) Flash() bool {
	return sd.flash
}

// 生成指定阵营的装备 0按照原规则随机生成阵营 1魏2蜀3吴4群5仙6魔7无阵营 此字段仅当dataFrom=4装备表时有效
func (sd Loot) Faction() E_Faction {
	return sd.faction
}

// 指定英雄等级
func (sd Loot) Level() int {
	return sd.level
}

func (sd Loot) Clone() *Loot {
	n := NewLoot()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type LootManager struct {
	Datas []*Loot

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*Loot // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byGroup map[int][]*Loot // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	dices map[int]*RewardDice
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newLootManager() *LootManager {
	mgr := &LootManager{
		Datas:   []*Loot{},
		byID:    map[int]*Loot{},
		byGroup: map[int][]*Loot{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>
	mgr.dices = map[int]*RewardDice{}
	//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *LootManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>
		switch d.dataFrom {
		case E_ItemBigType_Hero:
			if d.level == 0 {
				d.level = 1
			}
			//case E_ItemBigType_Equip:
			//	if d.faction == E_Faction_Null {
			//		d.faction = E_Faction_Nothing
			//	}
		}
		d.simple = Get_RewardThing()
		d.simple.Type = d.dataFrom
		d.simple.TID = d.itemId
		d.simple.Num = d.num
		d.simple.Faction = d.faction
		d.simple.Level = d.level
		//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byGroup[d.group] = append(mgr.byGroup[d.group], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>
	for groupID, group := range mgr.byGroup {
		mgr.dices[groupID] = NewRewardDice(groupID, group)
	}
	//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func LootSize() int {
	return lootMgr.size
}

func (mgr LootManager) check(path string, row int, sd *Loot) error {
	if _, ok := lootMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *LootManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for groupID, ds := range mgr.byGroup {
		var candidateNum int
		for _, d := range ds {
			if d.num <= 0 {
				success = false
				log.Printf("奖励表奖励[%d]的数量[%d]错误\n", d.id, d.num)
			}
			if d.minTimes > d.maxTimes {
				success = false
				log.Printf("奖励表奖励[%d]的最小次数[%d]大于最大次数[%d]\n", d.id, d.minTimes, d.maxTimes)
			}
			if d.mustDrop {
				candidateNum += 1
			} else {
				if d.weight > 0 {
					candidateNum += 1
				} /*else {
					success = false
					log.Printf("奖励表奖励[%d]的权重[%d]错误\n", d.id, d.weight)
				}*/
			}
			if d.faction > 0 {
				if !Check_E_Faction(d.faction) {
					success = false
					log.Printf("奖励表奖励[%d]的种族[%d]错误\n", d.id, d.faction)
				}
			}
			switch d.dataFrom {
			case E_ItemBigType_Currency:
				if _, ok := GetCurrencyByID(d.itemId); !ok {
					success = false
					log.Printf("奖励表奖励[%d]的货币[%d]不存在\n", d.id, d.itemId)
				}
			case E_ItemBigType_Item:
				if _, ok := GetItemByID(d.itemId); !ok {
					success = false
					log.Printf("奖励表奖励[%d]的物品[%d]不存在\n", d.id, d.itemId)
				}
			case E_ItemBigType_Hero:
				hero, ok := GetHeroBasicByID(d.itemId)
				if !ok {
					success = false
					log.Printf("奖励表奖励[%d]的英雄[%d]不存在\n", d.id, d.itemId)
				}
				qualitySD, ok := GetQualityByID(hero.quality)
				if !ok {
					success = false
					log.Printf("奖励表奖励[%d]的英雄[%d]对应的品质[%d]不存在\n", d.id, d.itemId, hero.quality)
				}
				if d.level > qualitySD.heroMaxLevel {
					success = false
					log.Printf("奖励表奖励[%d]的英雄[%d]等级[%d]超过品质上限[%d]\n", d.id, d.itemId, d.level, qualitySD.heroMaxLevel)
				}
			case E_ItemBigType_Equip:
				equipSD, ok := GetEquipmentByID(d.itemId)
				if !ok {
					success = false
					log.Printf("奖励表奖励[%d]的装备[%d]不存在\n", d.id, d.itemId)
				}
				qualitySD, ok := GetEquipmentLevelUpByQuality(equipSD.quality)
				if !ok {
					success = false
					log.Printf("奖励表奖励[%d]的装备[%d]的品质[%d]不存在\n", d.id, d.itemId, equipSD.quality)
				}
				for _, qsd := range qualitySD {
					if d.level > qsd.levelMax {
						success = false
						log.Printf("奖励表奖励[%d]的装备[%d]的等级[%d]大于最高等级[%d]\n", d.id, d.itemId, d.level, qsd.levelMax)
					}
					break
				}
			case E_ItemBigType_Warcraft:
				if _, ok := GetWarcraftByID(d.itemId); !ok {
					success = false
					log.Printf("奖励表奖励[%d]的兵书[%d]不存在\n", d.id, d.itemId)
				}
			default:
				success = false
				log.Printf("奖励表奖励[%d]的道具大类[%d]错误\n", d.id, d.dataFrom)
			}
		}
		if candidateNum <= 0 {
			success = false
			log.Printf("奖励表奖励组[%d]候选项是0，会随机出空奖励", groupID)
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *LootManager) each(f func(sd *Loot) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *LootManager) findIf(f func(sd *Loot) (find bool)) *Loot {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachLoot(f func(sd Loot) (continued bool)) {
	for _, sd := range lootMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindLootIf(f func(sd Loot) bool) (Loot, bool) {
	for _, sd := range lootMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilLoot, false
}

func GetLootByID(id int) (Loot, bool) {
	temp, ok := lootMgr.byID[id]
	if !ok {
		return nilLoot, false
	}
	return *temp, true
}

func GetLootByGroup(group int) (vs []Loot, ok bool) {
	var ds []*Loot
	ds, ok = lootMgr.byGroup[group]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachLootByGroup(group int, fn func(d Loot) (continued bool)) bool {
	ds, ok := lootMgr.byGroup[group]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd Loot) Simple() *RewardThing {
	return sd.simple.Clone()
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
