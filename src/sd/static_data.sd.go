// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！
package sd

import (
	"log"
	"path/filepath"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var (
	activityMgr                  = newActivityManager()
	activityRankMgr              = newActivityRankManager()
	activityTimeMgr              = newActivityTimeManager()
	currencyMgr                  = newCurrencyManager()
	equipmentMgr                 = newEquipmentManager()
	equipmentAdvanceMgr          = newEquipmentAdvanceManager()
	equipmentLevelUpMgr          = newEquipmentLevelUpManager()
	exclusiveMgr                 = newExclusiveManager()
	exclusiveLevelMgr            = newExclusiveLevelManager()
	filterWordMgr                = newFilterWordManager()
	funcOpenMgr                  = newFuncOpenManager()
	globalMgr                    = newGlobalManager()
	heroAdvanceMgr               = newHeroAdvanceManager()
	heroBasicMgr                 = newHeroBasicManager()
	heroGroupMgr                 = newHeroGroupManager()
	heroLevelMgr                 = newHeroLevelManager()
	itemMgr                      = newItemManager()
	ladderMgr                    = newLadderManager()
	limitMgr                     = newLimitManager()
	lootMgr                      = newLootManager()
	moneyBuyMgr                  = newMoneyBuyManager()
	monsterMgr                   = newMonsterManager()
	qualityMgr                   = newQualityManager()
	sceneMgr                     = newSceneManager()
	shopMgr                      = newShopManager()
	shopThingMgr                 = newShopThingManager()
	skillActiveMgr               = newSkillActiveManager()
	skillBuffMgr                 = newSkillBuffManager()
	skillBuffTypeMgr             = newSkillBuffTypeManager()
	skillPassiveMgr              = newSkillPassiveManager()
	skillPassiveConditionMgr     = newSkillPassiveConditionManager()
	skillPassiveConditionTypeMgr = newSkillPassiveConditionTypeManager()
	stageMgr                     = newStageManager()
	stageChapterMgr              = newStageChapterManager()
	trialTowerMgr                = newTrialTowerManager()
	trialTowerTypeMgr            = newTrialTowerTypeManager()
	vipMgr                       = newVipManager()
	warcraftMgr                  = newWarcraftManager()
	warcraftLevelMgr             = newWarcraftLevelManager()
	warcraftSearchMgr            = newWarcraftSearchManager()
)

var (
	nilActivity                  Activity
	nilActivityRank              ActivityRank
	nilActivityTime              ActivityTime
	nilCurrency                  Currency
	nilEquipment                 Equipment
	nilEquipmentAdvance          EquipmentAdvance
	nilEquipmentLevelUp          EquipmentLevelUp
	nilExclusive                 Exclusive
	nilExclusiveLevel            ExclusiveLevel
	nilFilterWord                FilterWord
	nilFuncOpen                  FuncOpen
	nilGlobal                    Global
	nilHeroAdvance               HeroAdvance
	nilHeroBasic                 HeroBasic
	nilHeroGroup                 HeroGroup
	nilHeroLevel                 HeroLevel
	nilItem                      Item
	nilLadder                    Ladder
	nilLimit                     Limit
	nilLoot                      Loot
	nilMoneyBuy                  MoneyBuy
	nilMonster                   Monster
	nilQuality                   Quality
	nilScene                     Scene
	nilShop                      Shop
	nilShopThing                 ShopThing
	nilSkillActive               SkillActive
	nilSkillBuff                 SkillBuff
	nilSkillBuffType             SkillBuffType
	nilSkillPassive              SkillPassive
	nilSkillPassiveCondition     SkillPassiveCondition
	nilSkillPassiveConditionType SkillPassiveConditionType
	nilStage                     Stage
	nilStageChapter              StageChapter
	nilTrialTower                TrialTower
	nilTrialTowerType            TrialTowerType
	nilVip                       Vip
	nilWarcraft                  Warcraft
	nilWarcraftLevel             WarcraftLevel
	nilWarcraftSearch            WarcraftSearch
)

func LoadAll(jsonDir string) (success bool) {
	absJsonDir, err := filepath.Abs(jsonDir)
	if err != nil {
		log.Println(err)
		return false
	}

	success = true

	success = activityMgr.load(filepath.Join(absJsonDir, "activity.json")) && success
	success = activityRankMgr.load(filepath.Join(absJsonDir, "activityRank.json")) && success
	success = activityTimeMgr.load(filepath.Join(absJsonDir, "activityTime.json")) && success
	success = currencyMgr.load(filepath.Join(absJsonDir, "currency.json")) && success
	success = equipmentMgr.load(filepath.Join(absJsonDir, "equipment.json")) && success
	success = equipmentAdvanceMgr.load(filepath.Join(absJsonDir, "equipmentAdvance.json")) && success
	success = equipmentLevelUpMgr.load(filepath.Join(absJsonDir, "equipmentLevelUp.json")) && success
	success = exclusiveMgr.load(filepath.Join(absJsonDir, "exclusive.json")) && success
	success = exclusiveLevelMgr.load(filepath.Join(absJsonDir, "exclusiveLevel.json")) && success
	success = filterWordMgr.load(filepath.Join(absJsonDir, "filterWord.json")) && success
	success = funcOpenMgr.load(filepath.Join(absJsonDir, "funcOpen.json")) && success
	success = globalMgr.load(filepath.Join(absJsonDir, "global.json")) && success
	success = heroAdvanceMgr.load(filepath.Join(absJsonDir, "heroAdvance.json")) && success
	success = heroBasicMgr.load(filepath.Join(absJsonDir, "heroBasic.json")) && success
	success = heroGroupMgr.load(filepath.Join(absJsonDir, "heroGroup.json")) && success
	success = heroLevelMgr.load(filepath.Join(absJsonDir, "heroLevel.json")) && success
	success = itemMgr.load(filepath.Join(absJsonDir, "item.json")) && success
	success = ladderMgr.load(filepath.Join(absJsonDir, "ladder.json")) && success
	success = limitMgr.load(filepath.Join(absJsonDir, "limit.json")) && success
	success = lootMgr.load(filepath.Join(absJsonDir, "loot.json")) && success
	success = moneyBuyMgr.load(filepath.Join(absJsonDir, "moneyBuy.json")) && success
	success = monsterMgr.load(filepath.Join(absJsonDir, "monster.json")) && success
	success = qualityMgr.load(filepath.Join(absJsonDir, "quality.json")) && success
	success = sceneMgr.load(filepath.Join(absJsonDir, "scene.json")) && success
	success = shopMgr.load(filepath.Join(absJsonDir, "shop.json")) && success
	success = shopThingMgr.load(filepath.Join(absJsonDir, "shopThing.json")) && success
	success = skillActiveMgr.load(filepath.Join(absJsonDir, "skillActive.json")) && success
	success = skillBuffMgr.load(filepath.Join(absJsonDir, "skillBuff.json")) && success
	success = skillBuffTypeMgr.load(filepath.Join(absJsonDir, "skillBuffType.json")) && success
	success = skillPassiveMgr.load(filepath.Join(absJsonDir, "skillPassive.json")) && success
	success = skillPassiveConditionMgr.load(filepath.Join(absJsonDir, "skillPassiveCondition.json")) && success
	success = skillPassiveConditionTypeMgr.load(filepath.Join(absJsonDir, "skillPassiveConditionType.json")) && success
	success = stageMgr.load(filepath.Join(absJsonDir, "stage.json")) && success
	success = stageChapterMgr.load(filepath.Join(absJsonDir, "stageChapter.json")) && success
	success = trialTowerMgr.load(filepath.Join(absJsonDir, "trialTower.json")) && success
	success = trialTowerTypeMgr.load(filepath.Join(absJsonDir, "trialTowerType.json")) && success
	success = vipMgr.load(filepath.Join(absJsonDir, "vip.json")) && success
	success = warcraftMgr.load(filepath.Join(absJsonDir, "warcraft.json")) && success
	success = warcraftLevelMgr.load(filepath.Join(absJsonDir, "warcraftLevel.json")) && success
	success = warcraftSearchMgr.load(filepath.Join(absJsonDir, "warcraftSearch.json")) && success

	return
}

func AfterLoadAll(jsonDir string) (success bool) {
	absJsonDir, err := filepath.Abs(jsonDir)
	if err != nil {
		log.Println(err)
		return false
	}

	success = true

	success = activityMgr.afterLoadAll(filepath.Join(absJsonDir, "activity.json")) && success
	success = activityRankMgr.afterLoadAll(filepath.Join(absJsonDir, "activityRank.json")) && success
	success = activityTimeMgr.afterLoadAll(filepath.Join(absJsonDir, "activityTime.json")) && success
	success = currencyMgr.afterLoadAll(filepath.Join(absJsonDir, "currency.json")) && success
	success = equipmentMgr.afterLoadAll(filepath.Join(absJsonDir, "equipment.json")) && success
	success = equipmentAdvanceMgr.afterLoadAll(filepath.Join(absJsonDir, "equipmentAdvance.json")) && success
	success = equipmentLevelUpMgr.afterLoadAll(filepath.Join(absJsonDir, "equipmentLevelUp.json")) && success
	success = exclusiveMgr.afterLoadAll(filepath.Join(absJsonDir, "exclusive.json")) && success
	success = exclusiveLevelMgr.afterLoadAll(filepath.Join(absJsonDir, "exclusiveLevel.json")) && success
	success = filterWordMgr.afterLoadAll(filepath.Join(absJsonDir, "filterWord.json")) && success
	success = funcOpenMgr.afterLoadAll(filepath.Join(absJsonDir, "funcOpen.json")) && success
	success = globalMgr.afterLoadAll(filepath.Join(absJsonDir, "global.json")) && success
	success = heroAdvanceMgr.afterLoadAll(filepath.Join(absJsonDir, "heroAdvance.json")) && success
	success = heroBasicMgr.afterLoadAll(filepath.Join(absJsonDir, "heroBasic.json")) && success
	success = heroGroupMgr.afterLoadAll(filepath.Join(absJsonDir, "heroGroup.json")) && success
	success = heroLevelMgr.afterLoadAll(filepath.Join(absJsonDir, "heroLevel.json")) && success
	success = itemMgr.afterLoadAll(filepath.Join(absJsonDir, "item.json")) && success
	success = ladderMgr.afterLoadAll(filepath.Join(absJsonDir, "ladder.json")) && success
	success = limitMgr.afterLoadAll(filepath.Join(absJsonDir, "limit.json")) && success
	success = lootMgr.afterLoadAll(filepath.Join(absJsonDir, "loot.json")) && success
	success = moneyBuyMgr.afterLoadAll(filepath.Join(absJsonDir, "moneyBuy.json")) && success
	success = monsterMgr.afterLoadAll(filepath.Join(absJsonDir, "monster.json")) && success
	success = qualityMgr.afterLoadAll(filepath.Join(absJsonDir, "quality.json")) && success
	success = sceneMgr.afterLoadAll(filepath.Join(absJsonDir, "scene.json")) && success
	success = shopMgr.afterLoadAll(filepath.Join(absJsonDir, "shop.json")) && success
	success = shopThingMgr.afterLoadAll(filepath.Join(absJsonDir, "shopThing.json")) && success
	success = skillActiveMgr.afterLoadAll(filepath.Join(absJsonDir, "skillActive.json")) && success
	success = skillBuffMgr.afterLoadAll(filepath.Join(absJsonDir, "skillBuff.json")) && success
	success = skillBuffTypeMgr.afterLoadAll(filepath.Join(absJsonDir, "skillBuffType.json")) && success
	success = skillPassiveMgr.afterLoadAll(filepath.Join(absJsonDir, "skillPassive.json")) && success
	success = skillPassiveConditionMgr.afterLoadAll(filepath.Join(absJsonDir, "skillPassiveCondition.json")) && success
	success = skillPassiveConditionTypeMgr.afterLoadAll(filepath.Join(absJsonDir, "skillPassiveConditionType.json")) && success
	success = stageMgr.afterLoadAll(filepath.Join(absJsonDir, "stage.json")) && success
	success = stageChapterMgr.afterLoadAll(filepath.Join(absJsonDir, "stageChapter.json")) && success
	success = trialTowerMgr.afterLoadAll(filepath.Join(absJsonDir, "trialTower.json")) && success
	success = trialTowerTypeMgr.afterLoadAll(filepath.Join(absJsonDir, "trialTowerType.json")) && success
	success = vipMgr.afterLoadAll(filepath.Join(absJsonDir, "vip.json")) && success
	success = warcraftMgr.afterLoadAll(filepath.Join(absJsonDir, "warcraft.json")) && success
	success = warcraftLevelMgr.afterLoadAll(filepath.Join(absJsonDir, "warcraftLevel.json")) && success
	success = warcraftSearchMgr.afterLoadAll(filepath.Join(absJsonDir, "warcraftSearch.json")) && success

	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
