package sd

import (
	"fmt"
	"log"

	nrandom "nggs/random"

	"msg"
)

func (sd ShopThing) ToRewardThingMsg() (m *msg.RewardThing) {
	m = msg.Get_RewardThing()
	m.Type = int32(sd.dataFrom)
	m.TID = int32(sd.itemId)
	m.Num = int32(sd.num)
	return m
}

func (sd ShopThing) ToRewardMsg() (m *msg.Reward) {
	m = msg.Get_Reward()
	m.Things = append(m.Things, sd.ToRewardThingMsg())
	return m
}

type ShopThingDice struct {
	super nrandom.IDiscreteDistributionDice

	id           int
	randomThings []*ShopThing
}

func NewShopThingDice(id int, shopThings []*ShopThing) *ShopThingDice {
	if id == 0 {
		log.Panicf("商店组id为0")
	}
	if len(shopThings) == 0 {
		log.Panicf("商店组[%d]物品项为空", id)
	}
	dice := &ShopThingDice{
		id: id,
	}
	dice.super = nrandom.NewDiscreteDistributionDice()
	for _, thing := range shopThings {
		dice.randomThings = append(dice.randomThings, thing)
		dice.super.Append(nrandom.NewDiscreteDistributionCandidate(thing, func(context interface{}) (int, interface{}) {
			return context.(*ShopThing).weight, nil
		}))
	}
	dice.super.Build()
	return dice
}

func GenerateOneShopThing(groupID int) (shopThing ShopThing, ok bool) {
	dice, ok := shopThingMgr.dices[groupID]
	if !ok {
		return nilShopThing, false
	}
	ctx, err := dice.super.Roll()
	if err != nil {
		log.Panic(err)
	}
	shopThing = *(ctx.(*ShopThing))
	return
}

func GenerateShopThings(shopID int, tYpE E_ShopType) (shopThings []ShopThing, ok bool) {
	shop, ok := GetShopByStageID(shopID)
	if !ok {
		return
	}
	switch tYpE {
	case E_ShopType_CommonShop:
		for _, groupID := range shop.commonShop {
			shopThing, ok := GenerateOneShopThing(groupID)
			if ok {
				shopThings = append(shopThings, shopThing)
			}
		}
	case E_ShopType_GuildShop:
		for _, groupID := range shop.guildShop {
			shopThing, ok := GenerateOneShopThing(groupID)
			if ok {
				shopThings = append(shopThings, shopThing)
			}
		}
	case E_ShopType_DismissShop:
		for _, groupID := range shop.dismissShop {
			shopThing, ok := GenerateOneShopThing(groupID)
			if ok {
				shopThings = append(shopThings, shopThing)
			}
		}
	case E_ShopType_DaoistMagicShop:
		for _, groupID := range shop.daoistMagicShop {
			shopThing, ok := GenerateOneShopThing(groupID)
			if ok {
				shopThings = append(shopThings, shopThing)
			}
		}
	case E_ShopType_LegendArenaShop:
		for _, groupID := range shop.legendArenaShop {
			shopThing, ok := GenerateOneShopThing(groupID)
			if ok {
				shopThings = append(shopThings, shopThing)
			}
		}
	case E_ShopType_DaoistMagicTrader:
		for _, groupID := range shop.daoistMagicTrader {
			shopThing, ok := GenerateOneShopThing(groupID)
			if ok {
				shopThings = append(shopThings, shopThing)
			}
		}
	case E_ShopType_ArenaShop:
		for _, groupID := range shop.arenaShop {
			shopThing, ok := GenerateOneShopThing(groupID)
			if ok {
				shopThings = append(shopThings, shopThing)
			}
		}
	case E_ShopType_BattlePointsShop:
		for _, groupID := range shop.contributionShop {
			shopThing, ok := GenerateOneShopThing(groupID)
			if ok {
				shopThings = append(shopThings, shopThing)
			}
		}
	case E_ShopType_WarcraftPointShop:
		for _, groupID := range shop.warcraftPointShop {
			shopThing, ok := GenerateOneShopThing(groupID)
			if ok {
				shopThings = append(shopThings, shopThing)
			}
		}
	case E_ShopType_HeroChallengeShop:
		for _, groupID := range shop.heroChallengeShop {
			shopThing, ok := GenerateOneShopThing(groupID)
			if ok {
				shopThings = append(shopThings, shopThing)
			}
		}
	default:
		return
	}
	return
}

type ShopContext struct {
	Type               E_ShopType
	FunctionType       E_FunctionType
	OpenStageID        int
	RefreshLimitID     int
	RefreshGoldCoinNum int
}

func (c ShopContext) String() string {
	ba, _ := json.Marshal(c)
	return fmt.Sprintf("ShopContext:%s", ba)
}
