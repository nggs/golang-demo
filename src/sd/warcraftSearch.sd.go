// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 兵书搜索表
type WarcraftSearch struct {
	id                 int           // 兵书搜索奖池id
	name               string        // 卡池名称
	dataFrom           E_ItemBigType // 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
	item               int           // 抽卡物品ID
	oneNum             int           // 单抽物品数量
	tenNum             int           // 十抽物品数量 0=无法十连抽
	insteadFrom        E_ItemBigType // 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
	oneInstead         int           // 代替单抽物品ID 0=无代替物
	tenInstead         int           // 代替十抽物品ID 0=无代替物
	reward             int           // 奖池reward
	noneOrange10       int           // 10抽无橙卡池
	noneOrange20       int           // 20抽无橙卡池
	noneOrange30       int           // 30抽无橙卡池
	qualityProbability []string      // 各品质概率 仅前端显示
	guaranteeNum       int           // 保底所需抽卡次数
	guarantee          int           // 保底奖池reward组id
	upItem             []int         // 本期up的兵书组id
	upProbability      []string      // up兵书概率 仅前段显示

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	oneCost        *Cost
	tenCost        *Cost
	oneInsteadCost *Cost
	tenInsteadCost *Cost
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewWarcraftSearch() *WarcraftSearch {
	sd := &WarcraftSearch{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 兵书搜索奖池id
func (sd WarcraftSearch) ID() int {
	return sd.id
}

// 卡池名称
func (sd WarcraftSearch) Name() string {
	return sd.name
}

// 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
func (sd WarcraftSearch) DataFrom() E_ItemBigType {
	return sd.dataFrom
}

// 抽卡物品ID
func (sd WarcraftSearch) Item() int {
	return sd.item
}

// 单抽物品数量
func (sd WarcraftSearch) OneNum() int {
	return sd.oneNum
}

// 十抽物品数量 0=无法十连抽
func (sd WarcraftSearch) TenNum() int {
	return sd.tenNum
}

// 1、货币表currency 2、道具表item   3、英雄表heroBasic   4、装备表equipment
func (sd WarcraftSearch) InsteadFrom() E_ItemBigType {
	return sd.insteadFrom
}

// 代替单抽物品ID 0=无代替物
func (sd WarcraftSearch) OneInstead() int {
	return sd.oneInstead
}

// 代替十抽物品ID 0=无代替物
func (sd WarcraftSearch) TenInstead() int {
	return sd.tenInstead
}

// 奖池reward
func (sd WarcraftSearch) Reward() int {
	return sd.reward
}

// 10抽无橙卡池
func (sd WarcraftSearch) NoneOrange10() int {
	return sd.noneOrange10
}

// 20抽无橙卡池
func (sd WarcraftSearch) NoneOrange20() int {
	return sd.noneOrange20
}

// 30抽无橙卡池
func (sd WarcraftSearch) NoneOrange30() int {
	return sd.noneOrange30
}

// 各品质概率 仅前端显示
func (sd WarcraftSearch) QualityProbability() []string {
	return sd.qualityProbability
}

// 保底所需抽卡次数
func (sd WarcraftSearch) GuaranteeNum() int {
	return sd.guaranteeNum
}

// 保底奖池reward组id
func (sd WarcraftSearch) Guarantee() int {
	return sd.guarantee
}

// 本期up的兵书组id
func (sd WarcraftSearch) UpItem() []int {
	return sd.upItem
}

// up兵书概率 仅前段显示
func (sd WarcraftSearch) UpProbability() []string {
	return sd.upProbability
}

func (sd WarcraftSearch) Clone() *WarcraftSearch {
	n := NewWarcraftSearch()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 兵书搜索表全局属性
type WarcraftSearchGlobal struct {
	warcraftPoint int // 每次抽取获得的兵书积分

}

// 每次抽取获得的兵书积分
func (g WarcraftSearchGlobal) WarcraftPoint() int {
	return g.warcraftPoint
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type WarcraftSearchManager struct {
	Datas  []*WarcraftSearch
	Global WarcraftSearchGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*WarcraftSearch // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newWarcraftSearchManager() *WarcraftSearchManager {
	mgr := &WarcraftSearchManager{
		Datas: []*WarcraftSearch{},
		byID:  map[int]*WarcraftSearch{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *WarcraftSearchManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func WarcraftSearchSize() int {
	return warcraftSearchMgr.size
}

func (mgr WarcraftSearchManager) check(path string, row int, sd *WarcraftSearch) error {
	if _, ok := warcraftSearchMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *WarcraftSearchManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if _, ok := GetLootByGroup(d.reward); !ok {
			success = false
			log.Printf("兵书搜索表兑换方式[%d]对应的奖励[%d]未找到\n", d.id, d.reward)
		}
		if d.noneOrange10 > 0 {
			if _, ok := GetLootByGroup(d.noneOrange10); !ok {
				success = false
				log.Printf("兵书搜索表兑换方式[%d]10抽无紫池对应的奖励[%d]未找到\n", d.id, d.noneOrange10)
			}
		}
		if d.noneOrange20 > 0 {
			if _, ok := GetLootByGroup(d.noneOrange20); !ok {
				success = false
				log.Printf("兵书搜索表兑换方式[%d]20抽无紫池对应的奖励[%d]未找到\n", d.id, d.noneOrange20)
			}
		}
		if d.noneOrange30 > 0 {
			if _, ok := GetLootByGroup(d.noneOrange30); !ok {
				success = false
				log.Printf("兵书搜索表兑换方式[%d]30抽无紫卡对应的奖励[%d]未找到\n", d.id, d.noneOrange30)
			}
		}

		switch d.dataFrom {
		case E_ItemBigType_Currency:
			if _, ok := GetCurrencyByID(d.item); !ok {
				success = false
				log.Printf("兵书搜索表兑换方式[%d]消耗的货币[%d]未找到\n", d.id, d.item)
			}
			if d.oneNum <= 0 {
				success = false
				log.Printf("兵书搜索兑换方式[%d]单抽消耗的货币[%d]数量[%d]不对\n", d.id, d.item, d.oneNum)
			}
		case E_ItemBigType_Item:
			if _, ok := GetItemByID(d.item); !ok {
				success = false
				log.Printf("兵书搜索兑换方式[%d]消耗的物品[%d]未找到\n", d.id, d.item)
			}
			if d.oneNum <= 0 {
				success = false
				log.Printf("兵书搜索兑换方式[%d]单抽消耗的物品[%d]数量[%d]不对\n", d.id, d.item, d.oneNum)
			}
		default:
			success = false
			log.Printf("兵书搜索兑换方式[%d]对应的消耗类型[%v]错误\n", d.id, d.dataFrom)
		}

		d.oneCost = AppendOneCost(d.oneCost, d.dataFrom, d.item, d.oneNum)
		if d.tenNum > 0 {
			d.tenCost = AppendOneCost(d.tenCost, d.dataFrom, d.item, d.tenNum)
		}

		switch d.insteadFrom {
		case E_ItemBigType_Currency:
			if d.oneInstead > 0 {
				if _, ok := GetCurrencyByID(d.oneInstead); !ok {
					success = false
					log.Printf("兵书搜索兑换方式[%d]单抽替代货币[%d]未找到\n", d.id, d.oneInstead)
				}
				d.oneInsteadCost = AppendOneCost(d.oneInsteadCost, d.insteadFrom, d.oneInstead, 1)
			}
			if d.tenInstead > 0 {
				if _, ok := GetCurrencyByID(d.tenInstead); !ok {
					success = false
					log.Printf("兵书搜索兑换方式[%d]十连抽替代货币[%d]未找到\n", d.id, d.tenInstead)
				}
				d.tenInsteadCost = AppendOneCost(d.tenInsteadCost, d.insteadFrom, d.tenInstead, 1)
			}
		case E_ItemBigType_Item:
			if d.oneInstead > 0 {
				if _, ok := GetItemByID(d.oneInstead); !ok {
					success = false
					log.Printf("兵书搜索兑换方式[%d]单抽替代物品[%d]未找到\n", d.id, d.oneInstead)
				}
				d.oneInsteadCost = AppendOneCost(d.oneInsteadCost, d.insteadFrom, d.oneInstead, 1)
			}
			if d.tenInstead > 0 {
				if _, ok := GetItemByID(d.tenInstead); !ok {
					success = false
					log.Printf("兵书搜索兑换方式[%d]十连抽替代物品[%d]未找到\n", d.id, d.tenInstead)
				}
				d.tenInsteadCost = AppendOneCost(d.tenInsteadCost, d.insteadFrom, d.tenInstead, 1)
			}
		default:
			success = false
			log.Printf("兵书搜索兑换方式[%d]消耗替代物类型[%v]错误\n", d.id, d.dataFrom)
		}

		if _, ok := GetLootByGroup(d.guarantee); !ok {
			success = false
			log.Printf("兵书搜索兑换方式[%d]保底奖池[%d]未找到\n", d.id, d.guarantee)
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *WarcraftSearchManager) each(f func(sd *WarcraftSearch) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *WarcraftSearchManager) findIf(f func(sd *WarcraftSearch) (find bool)) *WarcraftSearch {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachWarcraftSearch(f func(sd WarcraftSearch) (continued bool)) {
	for _, sd := range warcraftSearchMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindWarcraftSearchIf(f func(sd WarcraftSearch) bool) (WarcraftSearch, bool) {
	for _, sd := range warcraftSearchMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilWarcraftSearch, false
}

func GetWarcraftSearchByID(id int) (WarcraftSearch, bool) {
	temp, ok := warcraftSearchMgr.byID[id]
	if !ok {
		return nilWarcraftSearch, false
	}
	return *temp, true
}

func GetWarcraftSearchGlobal() WarcraftSearchGlobal {
	return warcraftSearchMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd WarcraftSearch) OneCost() *Cost {
	if sd.oneCost == nil {
		return nil
	}
	return sd.oneCost.Clone()
}

func (sd WarcraftSearch) TenCost() *Cost {
	if sd.tenCost == nil {
		return nil
	}
	return sd.tenCost.Clone()
}

func (sd WarcraftSearch) OneInsteadCost() *Cost {
	if sd.oneInsteadCost == nil {
		return nil
	}
	return sd.oneInsteadCost.Clone()
}

func (sd WarcraftSearch) TenInsteadCost() *Cost {
	if sd.tenInsteadCost == nil {
		return nil
	}
	return sd.tenInsteadCost.Clone()
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
