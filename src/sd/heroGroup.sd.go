// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 英雄组表
type HeroGroup struct {
	id                           int            // 英雄组ID
	heroName                     string         // 英雄名称
	nickName                     string         // 英雄绰号
	maxQualityHero               int            // 最高品质的英雄ID
	maxQuality                   E_Quality      // 英雄可到的最高品质，关联品质表 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+1
	icon                         string         // 英雄图标
	halfBody                     string         // 英雄半身像
	model                        string         // 英雄模型
	act                          string         // 英雄动作
	faction                      E_Faction      // 种族1=魏国 魏2=蜀国 蜀3=吴国 吴4=群雄 群5=仙 仙6=魔 魔
	heroType                     E_HeroType     // 职业1=力量型2=智慧型3=敏捷型
	skillGroup1                  int            // 技能组，关联到技能表
	skillGroup2                  int            // 技能组，关联到技能表
	skillGroup3                  int            // 技能组，关联到技能表
	skillGroup4                  int            // 技能组，关联到技能表 不填表示没有技能
	heroLogic                    string         // 英雄行为树
	desc                         string         // 英雄描述
	heroStory                    string         // 英雄故事
	reward                       int            // 图鉴奖励，关联reward表组ID
	hpRatio                      float64        // 生命系数
	atkRatio                     float64        // 攻击系数
	defRatio                     float64        // 防御系数
	attackConfig                 int            // 普攻对应的技能文件名(去掉skill)
	daoistMagicAssistant         int            // 迷宫马车援军随机权重
	daoistMagicAdvancedAssistant int            // 迷宫天牢援军随机权重
	portraitsShow                int            // 是否会显示在图鉴里，1是会，0是不会
	acquire                      int            // 获取渠道 关联acquire表
	rarity                       E_HeroRarity   // 武将稀有度
	e_HeroClass                  E_HeroClass    // 武将定位
	mainLocationDesc             E_LocationType // 主要职责描述
	secondLocationDesc           E_LocationType // 次要职责描述
	y                            int            // 血条高度偏移

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewHeroGroup() *HeroGroup {
	sd := &HeroGroup{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 英雄组ID
func (sd HeroGroup) ID() int {
	return sd.id
}

// 英雄名称
func (sd HeroGroup) HeroName() string {
	return sd.heroName
}

// 英雄绰号
func (sd HeroGroup) NickName() string {
	return sd.nickName
}

// 最高品质的英雄ID
func (sd HeroGroup) MaxQualityHero() int {
	return sd.maxQualityHero
}

// 英雄可到的最高品质，关联品质表 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+1
func (sd HeroGroup) MaxQuality() E_Quality {
	return sd.maxQuality
}

// 英雄图标
func (sd HeroGroup) Icon() string {
	return sd.icon
}

// 英雄半身像
func (sd HeroGroup) HalfBody() string {
	return sd.halfBody
}

// 英雄模型
func (sd HeroGroup) Model() string {
	return sd.model
}

// 英雄动作
func (sd HeroGroup) Act() string {
	return sd.act
}

// 种族1=魏国 魏2=蜀国 蜀3=吴国 吴4=群雄 群5=仙 仙6=魔 魔
func (sd HeroGroup) Faction() E_Faction {
	return sd.faction
}

// 职业1=力量型2=智慧型3=敏捷型
func (sd HeroGroup) HeroType() E_HeroType {
	return sd.heroType
}

// 技能组，关联到技能表
func (sd HeroGroup) SkillGroup1() int {
	return sd.skillGroup1
}

// 技能组，关联到技能表
func (sd HeroGroup) SkillGroup2() int {
	return sd.skillGroup2
}

// 技能组，关联到技能表
func (sd HeroGroup) SkillGroup3() int {
	return sd.skillGroup3
}

// 技能组，关联到技能表 不填表示没有技能
func (sd HeroGroup) SkillGroup4() int {
	return sd.skillGroup4
}

// 英雄行为树
func (sd HeroGroup) HeroLogic() string {
	return sd.heroLogic
}

// 英雄描述
func (sd HeroGroup) Desc() string {
	return sd.desc
}

// 英雄故事
func (sd HeroGroup) HeroStory() string {
	return sd.heroStory
}

// 图鉴奖励，关联reward表组ID
func (sd HeroGroup) Reward() int {
	return sd.reward
}

// 生命系数
func (sd HeroGroup) HpRatio() float64 {
	return sd.hpRatio
}

// 攻击系数
func (sd HeroGroup) AtkRatio() float64 {
	return sd.atkRatio
}

// 防御系数
func (sd HeroGroup) DefRatio() float64 {
	return sd.defRatio
}

// 普攻对应的技能文件名(去掉skill)
func (sd HeroGroup) AttackConfig() int {
	return sd.attackConfig
}

// 迷宫马车援军随机权重
func (sd HeroGroup) DaoistMagicAssistant() int {
	return sd.daoistMagicAssistant
}

// 迷宫天牢援军随机权重
func (sd HeroGroup) DaoistMagicAdvancedAssistant() int {
	return sd.daoistMagicAdvancedAssistant
}

// 是否会显示在图鉴里，1是会，0是不会
func (sd HeroGroup) PortraitsShow() int {
	return sd.portraitsShow
}

// 获取渠道 关联acquire表
func (sd HeroGroup) Acquire() int {
	return sd.acquire
}

// 武将稀有度
func (sd HeroGroup) Rarity() E_HeroRarity {
	return sd.rarity
}

// 武将定位
func (sd HeroGroup) EHeroClass() E_HeroClass {
	return sd.e_HeroClass
}

// 主要职责描述
func (sd HeroGroup) MainLocationDesc() E_LocationType {
	return sd.mainLocationDesc
}

// 次要职责描述
func (sd HeroGroup) SecondLocationDesc() E_LocationType {
	return sd.secondLocationDesc
}

// 血条高度偏移
func (sd HeroGroup) Y() int {
	return sd.y
}

func (sd HeroGroup) Clone() *HeroGroup {
	n := NewHeroGroup()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 英雄组表全局属性
type HeroGroupGlobal struct {
	heroResetCost               int          // 英雄重置消耗元宝数量
	heroPlaceCoolTime           int          // 共鸣槽位重新放置英雄冷却时间/秒
	heroPlaceCoolTimeCancel     int          // 立即刷新共鸣槽位冷却时间消耗元宝数量
	heroMasterYardBeginLevel    int          // ≤240级在英雄界面升级,＞240后在共鸣水晶中升级
	heroDismissRarity           E_HeroRarity // 此稀有度及以下的武将才能被遣散
	heroDismissQuality          E_Quality    // 可以自动被遣散的武将品质
	heroDismissReward           int          // 自动遣散绿色武将获得的物品，关联reward表
	greenHeroDismissReward      int          // 绿色遣散获得的物品，关联reward表
	blueHeroDismissReward       int          // 蓝色遣散获得的物品，关联reward表
	bluePlusHeroDismissReward   int          // 蓝色+遣散获得的物品，关联reward表
	purpleHeroDismissReward     int          // 紫色遣散获得的物品，关联reward表
	purplePlusHeroDismissReward int          // 紫色+遣散获得的物品，关联reward表
	orangeHeroDismissReward     int          // 橙色遣散获得的物品，关联reward表
	orangePlusHeroDismissReward int          // 橙色+遣散获得的物品，关联reward表
	forbidHeroGroup             []int        // 禁止上阵的武将组id

}

// 英雄重置消耗元宝数量
func (g HeroGroupGlobal) HeroResetCost() int {
	return g.heroResetCost
}

// 共鸣槽位重新放置英雄冷却时间/秒
func (g HeroGroupGlobal) HeroPlaceCoolTime() int {
	return g.heroPlaceCoolTime
}

// 立即刷新共鸣槽位冷却时间消耗元宝数量
func (g HeroGroupGlobal) HeroPlaceCoolTimeCancel() int {
	return g.heroPlaceCoolTimeCancel
}

// ≤240级在英雄界面升级,＞240后在共鸣水晶中升级
func (g HeroGroupGlobal) HeroMasterYardBeginLevel() int {
	return g.heroMasterYardBeginLevel
}

// 此稀有度及以下的武将才能被遣散
func (g HeroGroupGlobal) HeroDismissRarity() E_HeroRarity {
	return g.heroDismissRarity
}

// 可以自动被遣散的武将品质
func (g HeroGroupGlobal) HeroDismissQuality() E_Quality {
	return g.heroDismissQuality
}

// 自动遣散绿色武将获得的物品，关联reward表
func (g HeroGroupGlobal) HeroDismissReward() int {
	return g.heroDismissReward
}

// 绿色遣散获得的物品，关联reward表
func (g HeroGroupGlobal) GreenHeroDismissReward() int {
	return g.greenHeroDismissReward
}

// 蓝色遣散获得的物品，关联reward表
func (g HeroGroupGlobal) BlueHeroDismissReward() int {
	return g.blueHeroDismissReward
}

// 蓝色+遣散获得的物品，关联reward表
func (g HeroGroupGlobal) BluePlusHeroDismissReward() int {
	return g.bluePlusHeroDismissReward
}

// 紫色遣散获得的物品，关联reward表
func (g HeroGroupGlobal) PurpleHeroDismissReward() int {
	return g.purpleHeroDismissReward
}

// 紫色+遣散获得的物品，关联reward表
func (g HeroGroupGlobal) PurplePlusHeroDismissReward() int {
	return g.purplePlusHeroDismissReward
}

// 橙色遣散获得的物品，关联reward表
func (g HeroGroupGlobal) OrangeHeroDismissReward() int {
	return g.orangeHeroDismissReward
}

// 橙色+遣散获得的物品，关联reward表
func (g HeroGroupGlobal) OrangePlusHeroDismissReward() int {
	return g.orangePlusHeroDismissReward
}

// 禁止上阵的武将组id
func (g HeroGroupGlobal) ForbidHeroGroup() []int {
	return g.forbidHeroGroup
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type HeroGroupManager struct {
	Datas  []*HeroGroup
	Global HeroGroupGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*HeroGroup // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byMaxQuality map[E_Quality][]*HeroGroup // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	resetCost *Cost
	//dismissReward *Reward
	dice         *HeroGroupDice
	advancedDice *HeroGroupDice
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newHeroGroupManager() *HeroGroupManager {
	mgr := &HeroGroupManager{
		Datas:        []*HeroGroup{},
		byID:         map[int]*HeroGroup{},
		byMaxQuality: map[E_Quality][]*HeroGroup{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>
	mgr.dice = &HeroGroupDice{}
	mgr.advancedDice = &HeroGroupDice{}
	//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *HeroGroupManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byMaxQuality[d.maxQuality] = append(mgr.byMaxQuality[d.maxQuality], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>
	if mgr.Global.heroResetCost <= 0 {
		success = false
		log.Printf("英雄组表没有配置重置扣除的元宝\n")
	} else {
		// 重置英雄需要扣除元宝
		mgr.resetCost = Get_Cost()
		mgr.resetCost.AppendOne(E_ItemBigType_Currency, int(E_CurrencyType_GoldCoin), mgr.Global.heroResetCost)
	}
	if mgr.Global.heroPlaceCoolTime <= 0 {
		success = false
		log.Printf("英雄组表共鸣槽位重新放置英雄冷却时间错误[%d]\n", mgr.Global.heroPlaceCoolTime)
	}
	if mgr.Global.heroPlaceCoolTimeCancel <= 0 {
		success = false
		log.Printf("英雄组表立即刷新共鸣槽位冷却时间消耗元宝数量错误[%d]\n", mgr.Global.heroPlaceCoolTimeCancel)
	}

	//把英雄组按权重分布好
	var hg []*HeroGroup
	for _, v := range mgr.byID {
		hg = append(hg, v)
	}
	mgr.dice = NewHeroGroupDice(hg)
	mgr.advancedDice = NewAdvancedHeroGroupDice(hg)
	//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func HeroGroupSize() int {
	return heroGroupMgr.size
}

func (mgr HeroGroupManager) check(path string, row int, sd *HeroGroup) error {
	if _, ok := heroGroupMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *HeroGroupManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		heroes, ok := GetHeroBasicByGroup(d.id)
		if !ok {
			success = false
			log.Printf("英雄组表组[%d]没有对应英雄\n", d.id)
			continue
		}
		var maxQuality E_Quality
		var maxQualityHeroTID int
		for _, hero := range heroes {
			if hero.quality > maxQuality {
				maxQuality = hero.quality
				maxQualityHeroTID = hero.id
			}
		}
		if maxQuality != d.maxQuality {
			success = false
			log.Printf("英雄组表组[%d]的最高品质[%v]与英雄表的配置[%v]不符\n", d.id, d.maxQuality, maxQuality)
		}
		if maxQualityHeroTID != d.maxQualityHero {
			success = false
			log.Printf("英雄组表组[%d]的最高品质英雄[%d]与英雄表的配置[%d]不符\n", d.id, d.maxQualityHero, maxQualityHeroTID)
		}

		//mgr.dismissReward, ok = GenerateReward(mgr.Global.heroDismissReward)
		//if !ok {
		//	success = false
		//	log.Printf("英雄组表组的遣散奖励[%d]有误\n", mgr.Global.heroDismissReward)
		//}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *HeroGroupManager) each(f func(sd *HeroGroup) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *HeroGroupManager) findIf(f func(sd *HeroGroup) (find bool)) *HeroGroup {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachHeroGroup(f func(sd HeroGroup) (continued bool)) {
	for _, sd := range heroGroupMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindHeroGroupIf(f func(sd HeroGroup) bool) (HeroGroup, bool) {
	for _, sd := range heroGroupMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilHeroGroup, false
}

func GetHeroGroupByID(id int) (HeroGroup, bool) {
	temp, ok := heroGroupMgr.byID[id]
	if !ok {
		return nilHeroGroup, false
	}
	return *temp, true
}

func GetHeroGroupByMaxQuality(maxQuality E_Quality) (vs []HeroGroup, ok bool) {
	var ds []*HeroGroup
	ds, ok = heroGroupMgr.byMaxQuality[maxQuality]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachHeroGroupByMaxQuality(maxQuality E_Quality, fn func(d HeroGroup) (continued bool)) bool {
	ds, ok := heroGroupMgr.byMaxQuality[maxQuality]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetHeroGroupGlobal() HeroGroupGlobal {
	return heroGroupMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func GetHeroResetCost() *Cost {
	return heroGroupMgr.resetCost.Clone()
}

//func GetHeroDismissReward() *Reward {
//	return heroGroupMgr.dismissReward.Clone()
//}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
