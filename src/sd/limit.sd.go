// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 次数限制表
type Limit struct {
	id                int                // #唯一自增id
	refreshType       E_LimitRefreshType // 刷新类型：1.永远不刷新，用完拉倒；2.每隔一段时间刷新；3.每日固定时间点刷新；4.每周固定时间点刷新；
	refreshTime       int                // 刷新时间：刷新类型=1.无效；刷新类型=2.单位秒，刷新需要的间隔秒数，例：86400；刷新类型=3.每天某个时刻更新，例：235959（六位数，时、分、秒），表示每条23点59分59秒刷新；刷新类型=4.每周某个时刻更新，例：5235959（7位数，week、时、分、秒），表示每周五23点59分59秒刷新；
	desc              string             // 备注
	initCountVip      []int              // 初始次数VIP的切片
	refreshedCountVip []int              // 重置后次数VIP的切片

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewLimit() *Limit {
	sd := &Limit{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// #唯一自增id
func (sd Limit) ID() int {
	return sd.id
}

// 刷新类型：1.永远不刷新，用完拉倒；2.每隔一段时间刷新；3.每日固定时间点刷新；4.每周固定时间点刷新；
func (sd Limit) RefreshType() E_LimitRefreshType {
	return sd.refreshType
}

// 刷新时间：刷新类型=1.无效；刷新类型=2.单位秒，刷新需要的间隔秒数，例：86400；刷新类型=3.每天某个时刻更新，例：235959（六位数，时、分、秒），表示每条23点59分59秒刷新；刷新类型=4.每周某个时刻更新，例：5235959（7位数，week、时、分、秒），表示每周五23点59分59秒刷新；
func (sd Limit) RefreshTime() int {
	return sd.refreshTime
}

// 备注
func (sd Limit) Desc() string {
	return sd.desc
}

// 初始次数VIP的切片
func (sd Limit) InitCountVip() []int {
	return sd.initCountVip
}

// 重置后次数VIP的切片
func (sd Limit) RefreshedCountVip() []int {
	return sd.refreshedCountVip
}

func (sd Limit) Clone() *Limit {
	n := NewLimit()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 次数限制表全局属性
type LimitGlobal struct {
	fastIdle                     int // 快速挂机功能ID
	singleBountyRefresh          int // 单人悬赏任务日刷新限制id
	teamBountyRefresh            int // 团队悬赏任务日刷新限制id
	friendHeartDayRefresh        int // 好友赠送和领取好友点每日刷新时间限制id
	dailyQuestRefresh            int // 日常任务刷新功能ID
	weeklyQuestRefresh           int // 周常任务刷新功能ID
	trialTowerWeiTimesRefresh    int // 魏国试炼塔每日挑战次数刷新
	trialTowerShuTimesRefresh    int // 蜀国试炼塔每日挑战次数刷新
	trialTowerWuTimesRefresh     int // 吴国试炼塔每日挑战次数刷新
	trialTowerQunTimesRefresh    int // 群雄试炼塔每日挑战次数刷新
	guildBoss1Refresh            int // 公会常驻boss刷新
	guildBoss2Leave              int // 公会限时boss离开
	daoistMagicRefresh           int // 迷宫功能ID
	daoistMagicFreeRefresh       int // 迷宫免费次数刷新ID
	daoistMagicPayRefresh        int // 迷宫付费次数刷新ID
	daoistMagicAutoRefresh       int // 奇门遁甲扫荡记录刷新ID
	arenaDailyFree               int // 竞技场免费次数刷新ID
	onlineRewardRefresh          int // 每日奖励和当日在线时长重置时间id
	rescueTimes                  int // 英雄救美购买次数
	equipmentDungeons            int // 装备副本购买次数
	heroDungeons                 int // 武将副本购买次数
	silverCoinDungeons           int // 银币副本购买次数
	heroExpDungeons              int // 武将经验副本购买次数
	limitPassPillDungeons        int // 突破丹副本购买次数
	equipmentDungeonsBase        int // 装备副本基础次数
	heroDungeonsBase             int // 武将副本基础次数
	silverCoinDungeonsBase       int // 银币副本基础次数
	heroExpDungeonsBase          int // 武将经验副本基础次数
	limitPassPillDungeonsBase    int // 突破丹副本基础次数
	quickBuyItem                 int // 快捷购买每日刷新
	monthCardRefresh             int // 月卡领取刷新
	luckyBoxCharge               int // 幸运宝箱（充值）免费次数刷新
	luckyBoxGold                 int // 幸运宝箱（元宝）免费次数刷新
	legendArenaDailyFree         int // 高阶竞技场免费次数刷新ID
	worldBossRefresh             int // 世界BOSS挑战次数刷新
	treasureDailyRewardRefresh   int // 每日免费寻宝令
	shareReward                  int // 好友邀请分享奖励次数限制
	shareTimesRewardRefresh      int // 分享次数奖励刷新ID
	newTavernRefresh             int // 无双招募免费次数刷新
	shareTimeLineRefresh         int // 分享朋友圈奖励刷新
	giftPackDailyTriggersRefresh int // 成长限时礼包每日刷新
	moonlightBoxRefresh          int // 月光宝盒领取次数刷新

}

// 快速挂机功能ID
func (g LimitGlobal) FastIdle() int {
	return g.fastIdle
}

// 单人悬赏任务日刷新限制id
func (g LimitGlobal) SingleBountyRefresh() int {
	return g.singleBountyRefresh
}

// 团队悬赏任务日刷新限制id
func (g LimitGlobal) TeamBountyRefresh() int {
	return g.teamBountyRefresh
}

// 好友赠送和领取好友点每日刷新时间限制id
func (g LimitGlobal) FriendHeartDayRefresh() int {
	return g.friendHeartDayRefresh
}

// 日常任务刷新功能ID
func (g LimitGlobal) DailyQuestRefresh() int {
	return g.dailyQuestRefresh
}

// 周常任务刷新功能ID
func (g LimitGlobal) WeeklyQuestRefresh() int {
	return g.weeklyQuestRefresh
}

// 魏国试炼塔每日挑战次数刷新
func (g LimitGlobal) TrialTowerWeiTimesRefresh() int {
	return g.trialTowerWeiTimesRefresh
}

// 蜀国试炼塔每日挑战次数刷新
func (g LimitGlobal) TrialTowerShuTimesRefresh() int {
	return g.trialTowerShuTimesRefresh
}

// 吴国试炼塔每日挑战次数刷新
func (g LimitGlobal) TrialTowerWuTimesRefresh() int {
	return g.trialTowerWuTimesRefresh
}

// 群雄试炼塔每日挑战次数刷新
func (g LimitGlobal) TrialTowerQunTimesRefresh() int {
	return g.trialTowerQunTimesRefresh
}

// 公会常驻boss刷新
func (g LimitGlobal) GuildBoss1Refresh() int {
	return g.guildBoss1Refresh
}

// 公会限时boss离开
func (g LimitGlobal) GuildBoss2Leave() int {
	return g.guildBoss2Leave
}

// 迷宫功能ID
func (g LimitGlobal) DaoistMagicRefresh() int {
	return g.daoistMagicRefresh
}

// 迷宫免费次数刷新ID
func (g LimitGlobal) DaoistMagicFreeRefresh() int {
	return g.daoistMagicFreeRefresh
}

// 迷宫付费次数刷新ID
func (g LimitGlobal) DaoistMagicPayRefresh() int {
	return g.daoistMagicPayRefresh
}

// 奇门遁甲扫荡记录刷新ID
func (g LimitGlobal) DaoistMagicAutoRefresh() int {
	return g.daoistMagicAutoRefresh
}

// 竞技场免费次数刷新ID
func (g LimitGlobal) ArenaDailyFree() int {
	return g.arenaDailyFree
}

// 每日奖励和当日在线时长重置时间id
func (g LimitGlobal) OnlineRewardRefresh() int {
	return g.onlineRewardRefresh
}

// 英雄救美购买次数
func (g LimitGlobal) RescueTimes() int {
	return g.rescueTimes
}

// 装备副本购买次数
func (g LimitGlobal) EquipmentDungeons() int {
	return g.equipmentDungeons
}

// 武将副本购买次数
func (g LimitGlobal) HeroDungeons() int {
	return g.heroDungeons
}

// 银币副本购买次数
func (g LimitGlobal) SilverCoinDungeons() int {
	return g.silverCoinDungeons
}

// 武将经验副本购买次数
func (g LimitGlobal) HeroExpDungeons() int {
	return g.heroExpDungeons
}

// 突破丹副本购买次数
func (g LimitGlobal) LimitPassPillDungeons() int {
	return g.limitPassPillDungeons
}

// 装备副本基础次数
func (g LimitGlobal) EquipmentDungeonsBase() int {
	return g.equipmentDungeonsBase
}

// 武将副本基础次数
func (g LimitGlobal) HeroDungeonsBase() int {
	return g.heroDungeonsBase
}

// 银币副本基础次数
func (g LimitGlobal) SilverCoinDungeonsBase() int {
	return g.silverCoinDungeonsBase
}

// 武将经验副本基础次数
func (g LimitGlobal) HeroExpDungeonsBase() int {
	return g.heroExpDungeonsBase
}

// 突破丹副本基础次数
func (g LimitGlobal) LimitPassPillDungeonsBase() int {
	return g.limitPassPillDungeonsBase
}

// 快捷购买每日刷新
func (g LimitGlobal) QuickBuyItem() int {
	return g.quickBuyItem
}

// 月卡领取刷新
func (g LimitGlobal) MonthCardRefresh() int {
	return g.monthCardRefresh
}

// 幸运宝箱（充值）免费次数刷新
func (g LimitGlobal) LuckyBoxCharge() int {
	return g.luckyBoxCharge
}

// 幸运宝箱（元宝）免费次数刷新
func (g LimitGlobal) LuckyBoxGold() int {
	return g.luckyBoxGold
}

// 高阶竞技场免费次数刷新ID
func (g LimitGlobal) LegendArenaDailyFree() int {
	return g.legendArenaDailyFree
}

// 世界BOSS挑战次数刷新
func (g LimitGlobal) WorldBossRefresh() int {
	return g.worldBossRefresh
}

// 每日免费寻宝令
func (g LimitGlobal) TreasureDailyRewardRefresh() int {
	return g.treasureDailyRewardRefresh
}

// 好友邀请分享奖励次数限制
func (g LimitGlobal) ShareReward() int {
	return g.shareReward
}

// 分享次数奖励刷新ID
func (g LimitGlobal) ShareTimesRewardRefresh() int {
	return g.shareTimesRewardRefresh
}

// 无双招募免费次数刷新
func (g LimitGlobal) NewTavernRefresh() int {
	return g.newTavernRefresh
}

// 分享朋友圈奖励刷新
func (g LimitGlobal) ShareTimeLineRefresh() int {
	return g.shareTimeLineRefresh
}

// 成长限时礼包每日刷新
func (g LimitGlobal) GiftPackDailyTriggersRefresh() int {
	return g.giftPackDailyTriggersRefresh
}

// 月光宝盒领取次数刷新
func (g LimitGlobal) MoonlightBoxRefresh() int {
	return g.moonlightBoxRefresh
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type LimitManager struct {
	Datas  []*Limit
	Global LimitGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*Limit // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newLimitManager() *LimitManager {
	mgr := &LimitManager{
		Datas: []*Limit{},
		byID:  map[int]*Limit{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *LimitManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func LimitSize() int {
	return limitMgr.size
}

func (mgr LimitManager) check(path string, row int, sd *Limit) error {
	if _, ok := limitMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *LimitManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *LimitManager) each(f func(sd *Limit) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *LimitManager) findIf(f func(sd *Limit) (find bool)) *Limit {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachLimit(f func(sd Limit) (continued bool)) {
	for _, sd := range limitMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindLimitIf(f func(sd Limit) bool) (Limit, bool) {
	for _, sd := range limitMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilLimit, false
}

func GetLimitByID(id int) (Limit, bool) {
	temp, ok := limitMgr.byID[id]
	if !ok {
		return nilLimit, false
	}
	return *temp, true
}

func GetLimitGlobal() LimitGlobal {
	return limitMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd Limit) RefreshTimeOffset() int64 {
	switch sd.refreshType {
	case E_LimitRefreshType_Day:
		// 每天某个时刻更新，例：235959（六位数，时、分、秒），23点59分59秒刷新
		hour := sd.refreshTime / 10000
		min := (sd.refreshTime % 10000) / 100
		sec := sd.refreshTime % 100
		return int64(hour*3600 + min*60 + sec)
	case E_LimitRefreshType_Week:
		// 每周某个时刻更新，例：5235959（7位数，week、时、分、秒），每周五23点59分59秒刷新
		weekday := sd.refreshTime / 1000000
		hour := (sd.refreshTime % 1000000) / 10000
		min := (sd.refreshTime % 10000) / 100
		sec := sd.refreshTime % 100
		return int64((weekday-1)*86400 + hour*3600 + min*60 + sec)
	}
	return math.MaxInt32
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
