// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 关卡章节表
type StageChapter struct {
	id                 int     // 主键ID
	chapterNum         int     // 章节数
	name               string  // 章节名称
	bg                 string  // 章节背景图
	idleBg1            string  // 挂机背景图
	idleBg2            string  // 挂机背景图
	idleBg3            string  // 挂机背景图
	ourBeginEnergy     float64 // 我方上阵英雄初始能量 此章节的关卡战斗开始时就赋予一定的能量
	monsterBeginEnergy float64 // 野怪初始能量 此章节的关卡战斗开始时就赋予一定的能量
	idleRewardShow     int     // 展示挂机掉落奖励,仅用于客户端显示 关联reward

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	firstStage *Stage // 本章第一个关卡
	lastStage  *Stage // 本章节最后一关关卡
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewStageChapter() *StageChapter {
	sd := &StageChapter{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 主键ID
func (sd StageChapter) ID() int {
	return sd.id
}

// 章节数
func (sd StageChapter) ChapterNum() int {
	return sd.chapterNum
}

// 章节名称
func (sd StageChapter) Name() string {
	return sd.name
}

// 章节背景图
func (sd StageChapter) Bg() string {
	return sd.bg
}

// 挂机背景图
func (sd StageChapter) IdleBg1() string {
	return sd.idleBg1
}

// 挂机背景图
func (sd StageChapter) IdleBg2() string {
	return sd.idleBg2
}

// 挂机背景图
func (sd StageChapter) IdleBg3() string {
	return sd.idleBg3
}

// 我方上阵英雄初始能量 此章节的关卡战斗开始时就赋予一定的能量
func (sd StageChapter) OurBeginEnergy() float64 {
	return sd.ourBeginEnergy
}

// 野怪初始能量 此章节的关卡战斗开始时就赋予一定的能量
func (sd StageChapter) MonsterBeginEnergy() float64 {
	return sd.monsterBeginEnergy
}

// 展示挂机掉落奖励,仅用于客户端显示 关联reward
func (sd StageChapter) IdleRewardShow() int {
	return sd.idleRewardShow
}

func (sd StageChapter) Clone() *StageChapter {
	n := NewStageChapter()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 关卡章节表全局属性
type StageChapterGlobal struct {
	showNotChallengeNum int // 显示到当前所在章节之后的多少个章节信息

}

// 显示到当前所在章节之后的多少个章节信息
func (g StageChapterGlobal) ShowNotChallengeNum() int {
	return g.showNotChallengeNum
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type StageChapterManager struct {
	Datas  []*StageChapter
	Global StageChapterGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*StageChapter // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	first *StageChapter
	last  *StageChapter
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newStageChapterManager() *StageChapterManager {
	mgr := &StageChapterManager{
		Datas: []*StageChapter{},
		byID:  map[int]*StageChapter{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *StageChapterManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func StageChapterSize() int {
	return stageChapterMgr.size
}

func (mgr StageChapterManager) check(path string, row int, sd *StageChapter) error {
	if _, ok := stageChapterMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *StageChapterManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	if len(mgr.Datas) == 0 {
		success = false
		log.Printf("关卡章节表没有数据")
	} else {
		mgr.first = mgr.Datas[0]
		mgr.last = mgr.Datas[len(mgr.Datas)-1]
	}
	for _, d := range mgr.Datas {
		stages, ok := stageMgr.byChapterID[d.id]
		if !ok && len(stages) == 0 {
			success = false
			log.Printf("章节[%d]未找到关卡配置", d.id)
		} else {
			d.firstStage = stages[0]
			d.lastStage = stages[len(stages)-1]
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *StageChapterManager) each(f func(sd *StageChapter) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *StageChapterManager) findIf(f func(sd *StageChapter) (find bool)) *StageChapter {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachStageChapter(f func(sd StageChapter) (continued bool)) {
	for _, sd := range stageChapterMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindStageChapterIf(f func(sd StageChapter) bool) (StageChapter, bool) {
	for _, sd := range stageChapterMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilStageChapter, false
}

func GetStageChapterByID(id int) (StageChapter, bool) {
	temp, ok := stageChapterMgr.byID[id]
	if !ok {
		return nilStageChapter, false
	}
	return *temp, true
}

func GetStageChapterGlobal() StageChapterGlobal {
	return stageChapterMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd StageChapter) FirstStage() Stage {
	return *sd.firstStage
}

func (sd StageChapter) LastStage() Stage {
	return *sd.lastStage
}

func GetFirstStageChapter() StageChapter {
	return *stageChapterMgr.first
}

func GetLastStageChapter() StageChapter {
	return *stageChapterMgr.last
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
