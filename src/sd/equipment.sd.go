// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 装备表
type Equipment struct {
	id            int             // 装备ID
	name          string          // 装备名字
	icon          string          // 装备图标
	desc          string          // 装备描述
	equipHeroType E_HeroType      // 装备职业1=智力2=力量3=敏捷
	tYpE          E_EquipmentType // 装备类型 1=军师扇2=布甲3=重武器4=板甲5=轻武器6=皮甲
	pos           E_EquipmentPos  // 装备的部位 1=武器2=帽子3=衣服4=鞋子
	quality       E_Quality       // 装备的品质，关联品质表 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+1
	hp            int             // 生命
	atk           int             // 攻击
	def           int             // 防御
	crit          float64         // 暴击
	hit           int             // 命中
	miss          int             // 闪避
	speed         int             // 急速
	recover       int             // 每秒恢复
	physicReduce  float64         // 物理减伤率
	magicReduce   float64         // 魔法减伤率
	lifeLeech     int             // 吸血等级
	acquire       int             // 获取渠道 关联acquire表
	useJump       int             // 在背包中使用，点击后跳转到指定页面
	basePrice     int             // 装备分解 基础银币数

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	maxStrengthenLevel int // 最大强化等级
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewEquipment() *Equipment {
	sd := &Equipment{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 装备ID
func (sd Equipment) ID() int {
	return sd.id
}

// 装备名字
func (sd Equipment) Name() string {
	return sd.name
}

// 装备图标
func (sd Equipment) Icon() string {
	return sd.icon
}

// 装备描述
func (sd Equipment) Desc() string {
	return sd.desc
}

// 装备职业1=智力2=力量3=敏捷
func (sd Equipment) EquipHeroType() E_HeroType {
	return sd.equipHeroType
}

// 装备类型 1=军师扇2=布甲3=重武器4=板甲5=轻武器6=皮甲
func (sd Equipment) Type() E_EquipmentType {
	return sd.tYpE
}

// 装备的部位 1=武器2=帽子3=衣服4=鞋子
func (sd Equipment) Pos() E_EquipmentPos {
	return sd.pos
}

// 装备的品质，关联品质表 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+1
func (sd Equipment) Quality() E_Quality {
	return sd.quality
}

// 生命
func (sd Equipment) Hp() int {
	return sd.hp
}

// 攻击
func (sd Equipment) Atk() int {
	return sd.atk
}

// 防御
func (sd Equipment) Def() int {
	return sd.def
}

// 暴击
func (sd Equipment) Crit() float64 {
	return sd.crit
}

// 命中
func (sd Equipment) Hit() int {
	return sd.hit
}

// 闪避
func (sd Equipment) Miss() int {
	return sd.miss
}

// 急速
func (sd Equipment) Speed() int {
	return sd.speed
}

// 每秒恢复
func (sd Equipment) Recover() int {
	return sd.recover
}

// 物理减伤率
func (sd Equipment) PhysicReduce() float64 {
	return sd.physicReduce
}

// 魔法减伤率
func (sd Equipment) MagicReduce() float64 {
	return sd.magicReduce
}

// 吸血等级
func (sd Equipment) LifeLeech() int {
	return sd.lifeLeech
}

// 获取渠道 关联acquire表
func (sd Equipment) Acquire() int {
	return sd.acquire
}

// 在背包中使用，点击后跳转到指定页面
func (sd Equipment) UseJump() int {
	return sd.useJump
}

// 装备分解 基础银币数
func (sd Equipment) BasePrice() int {
	return sd.basePrice
}

func (sd Equipment) Clone() *Equipment {
	n := NewEquipment()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 装备表全局属性
type EquipmentGlobal struct {
	equipmentFactionAdd     float64   // 装备种族加成比
	equipmentFactionChance  []int     // 依次是魏、蜀、吴、群、仙、魔、无的装备出现种族加成的概率
	equipBreakSilver        float64   // 分解银币系数
	equipBreakIron          float64   // 分解铁矿石系数
	equipBreakOnekeyQuality E_Quality // 一键选择装备品质初始品质

}

// 装备种族加成比
func (g EquipmentGlobal) EquipmentFactionAdd() float64 {
	return g.equipmentFactionAdd
}

// 依次是魏、蜀、吴、群、仙、魔、无的装备出现种族加成的概率
func (g EquipmentGlobal) EquipmentFactionChance() []int {
	return g.equipmentFactionChance
}

// 分解银币系数
func (g EquipmentGlobal) EquipBreakSilver() float64 {
	return g.equipBreakSilver
}

// 分解铁矿石系数
func (g EquipmentGlobal) EquipBreakIron() float64 {
	return g.equipBreakIron
}

// 一键选择装备品质初始品质
func (g EquipmentGlobal) EquipBreakOnekeyQuality() E_Quality {
	return g.equipBreakOnekeyQuality
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type EquipmentManager struct {
	Datas  []*Equipment
	Global EquipmentGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*Equipment // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byEquipHeroType map[E_HeroType][]*Equipment // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	factionDice *EquipFactionDice
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newEquipmentManager() *EquipmentManager {
	mgr := &EquipmentManager{
		Datas:           []*Equipment{},
		byID:            map[int]*Equipment{},
		byEquipHeroType: map[E_HeroType][]*Equipment{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *EquipmentManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byEquipHeroType[d.equipHeroType] = append(mgr.byEquipHeroType[d.equipHeroType], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func EquipmentSize() int {
	return equipmentMgr.size
}

func (mgr EquipmentManager) check(path string, row int, sd *Equipment) error {
	if _, ok := equipmentMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *EquipmentManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if _, ok := GetQualityByID(d.quality); !ok {
			success = false
			log.Printf("装备表装备[%d]的品质[%d]找不到对应配置\n", d.id, d.quality)
		}
		switch d.equipHeroType {
		case E_HeroType_Intelligence:
			switch d.pos {
			case E_EquipmentPos_Weapon:
				if d.tYpE != E_EquipmentType_Staff {
					success = false
					log.Printf("装备表智力系武器[%d]的类型[%v]不是法杖\n", d.id, d.tYpE)
				}
			case E_EquipmentPos_Helmet:
				if d.tYpE != E_EquipmentType_ClothArmor {
					success = false
					log.Printf("装备表智力系头盔[%d]的类型[%v]不是布甲\n", d.id, d.tYpE)
				}
			case E_EquipmentPos_Armor:
				if d.tYpE != E_EquipmentType_ClothArmor {
					success = false
					log.Printf("装备表智力系铠甲[%d]的类型[%v]不是布甲\n", d.id, d.tYpE)
				}
			case E_EquipmentPos_Boot:
				if d.tYpE != E_EquipmentType_ClothArmor {
					success = false
					log.Printf("装备表智力系靴子[%d]的类型[%v]不是布甲\n", d.id, d.tYpE)
				}
			}
		case E_HeroType_Strength:
			switch d.pos {
			case E_EquipmentPos_Weapon:
				if d.tYpE != E_EquipmentType_HeavyWeapon {
					success = false
					log.Printf("装备表力量系武器[%d]的类型[%v]不是重武器\n", d.id, d.tYpE)
				}
			case E_EquipmentPos_Helmet:
				if d.tYpE != E_EquipmentType_PlateArmor {
					success = false
					log.Printf("装备表力量系头盔[%d]的类型[%v]不是布甲\n", d.id, d.tYpE)
				}
			case E_EquipmentPos_Armor:
				if d.tYpE != E_EquipmentType_PlateArmor {
					success = false
					log.Printf("装备表力量系铠甲[%d]的类型[%v]不是布甲\n", d.id, d.tYpE)
				}
			case E_EquipmentPos_Boot:
				if d.tYpE != E_EquipmentType_PlateArmor {
					success = false
					log.Printf("装备表力量系靴子[%d]的类型[%v]不是布甲\n", d.id, d.tYpE)
				}
			}
		case E_HeroType_Agile:
			switch d.pos {
			case E_EquipmentPos_Weapon:
				if d.tYpE != E_EquipmentType_LightWeapons {
					success = false
					log.Printf("装备表敏捷系武器[%d]的类型[%v]不是轻武器\n", d.id, d.tYpE)
				}
			case E_EquipmentPos_Helmet:
				if d.tYpE != E_EquipmentType_LeatherArmor {
					success = false
					log.Printf("装备表敏捷系头盔[%d]的类型[%v]不是轻甲\n", d.id, d.tYpE)
				}
			case E_EquipmentPos_Armor:
				if d.tYpE != E_EquipmentType_LeatherArmor {
					success = false
					log.Printf("装备表敏捷系铠甲[%d]的类型[%v]不是轻甲\n", d.id, d.tYpE)
				}
			case E_EquipmentPos_Boot:
				if d.tYpE != E_EquipmentType_LeatherArmor {
					success = false
					log.Printf("装备表敏捷系靴子[%d]的类型[%v]不是轻甲\n", d.id, d.tYpE)
				}
			}
		}
		equipLevelUpSDs, ok := GetEquipmentLevelUpByQuality(d.quality)
		if !ok {
			success = false
			log.Printf("装备表装备[%d]的品质[%d]找不到对应的强化配置\n", d.id, d.quality)
		}
		for _, equipLevelUpSD := range equipLevelUpSDs {
			if d.maxStrengthenLevel < equipLevelUpSD.level {
				d.maxStrengthenLevel = equipLevelUpSD.level
			}
		}
	}

	if len(equipmentMgr.Global.equipmentFactionChance) != E_Faction_Size() {
		success = false
		log.Printf("装备表种族概率权重数量有误\n")
	}
	var factionCandidates []*EquipFactionCandidate
	Each_E_Faction(func(faction E_Faction) bool {
		factionCandidates = append(factionCandidates, NewEquipFactionCandidate(faction, equipmentMgr.Global.equipmentFactionChance[int(faction)-1]))
		return true
	})
	mgr.factionDice = NewEquipFactionDice(factionCandidates)
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *EquipmentManager) each(f func(sd *Equipment) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *EquipmentManager) findIf(f func(sd *Equipment) (find bool)) *Equipment {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachEquipment(f func(sd Equipment) (continued bool)) {
	for _, sd := range equipmentMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindEquipmentIf(f func(sd Equipment) bool) (Equipment, bool) {
	for _, sd := range equipmentMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilEquipment, false
}

func GetEquipmentByID(id int) (Equipment, bool) {
	temp, ok := equipmentMgr.byID[id]
	if !ok {
		return nilEquipment, false
	}
	return *temp, true
}

func GetEquipmentByEquipHeroType(equipHeroType E_HeroType) (vs []Equipment, ok bool) {
	var ds []*Equipment
	ds, ok = equipmentMgr.byEquipHeroType[equipHeroType]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachEquipmentByEquipHeroType(equipHeroType E_HeroType, fn func(d Equipment) (continued bool)) bool {
	ds, ok := equipmentMgr.byEquipHeroType[equipHeroType]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetEquipmentGlobal() EquipmentGlobal {
	return equipmentMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd Equipment) MaxStrengthenLevel() int {
	return sd.maxStrengthenLevel
}

func (sd Equipment) BattleAttribute(ba *BattleAttribute) *BattleAttribute {
	if ba == nil {
		ba = GetBattleAttribute()
	}
	ba.add(E_BattleAttribute_Hp, BattleAttributeValue(sd.hp))
	ba.add(E_BattleAttribute_Atk, BattleAttributeValue(sd.atk))
	ba.add(E_BattleAttribute_Def, BattleAttributeValue(sd.def))
	ba.add(E_BattleAttribute_Crit, BattleAttributeValue(sd.crit))
	ba.add(E_BattleAttribute_Hit, BattleAttributeValue(sd.hit))
	ba.add(E_BattleAttribute_Miss, BattleAttributeValue(sd.miss))
	ba.add(E_BattleAttribute_Speed, BattleAttributeValue(sd.speed))
	ba.add(E_BattleAttribute_Recover, BattleAttributeValue(sd.recover))
	ba.add(E_BattleAttribute_PhysicReduce, BattleAttributeValue(sd.physicReduce))
	ba.add(E_BattleAttribute_MagicReduce, BattleAttributeValue(sd.magicReduce))
	ba.add(E_BattleAttribute_LifeLeech, BattleAttributeValue(sd.lifeLeech))
	//ba.add(E_BattleAttribute_HurtEnergy, BattleAttributeValue(sd.hurtEnergy))
	//ba.add(E_BattleAttribute_CritDamage, BattleAttributeValue(sd.critDamage))
	return ba
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
