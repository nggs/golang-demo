// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 英雄进阶表
type HeroAdvance struct {
	id             int           // id
	quality        E_Quality     // 品质，关联品质表 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+1
	faction        []E_Faction   // 种族组，格式:魏国;蜀国;吴国;群雄
	tYpE           E_HeroAdvance // 升阶方式 1无法升阶 2使用相同组的英雄 3使用同种族英雄 4使用不同种族英雄
	needQuality    E_Quality     // 需要的英雄品质 空缺表示无法升阶
	num            int           // 材料数量
	type1          E_HeroAdvance // 升阶方式 1无法升阶 2使用相同组的英雄 3使用同种族英雄 4使用不同种族英雄
	needQuality1   E_Quality     // 需要的英雄品质 空缺表示无法升阶
	num1           int           // 材料数量
	desc           string        // 描述
	heroBackPurple int           // 回退返还同组武将数量
	heroBackBlue   int           // 回退返还蓝色武将卡数量

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewHeroAdvance() *HeroAdvance {
	sd := &HeroAdvance{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// id
func (sd HeroAdvance) ID() int {
	return sd.id
}

// 品质，关联品质表 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+1
func (sd HeroAdvance) Quality() E_Quality {
	return sd.quality
}

// 种族组，格式:魏国;蜀国;吴国;群雄
func (sd HeroAdvance) Faction() []E_Faction {
	return sd.faction
}

// 升阶方式 1无法升阶 2使用相同组的英雄 3使用同种族英雄 4使用不同种族英雄
func (sd HeroAdvance) Type() E_HeroAdvance {
	return sd.tYpE
}

// 需要的英雄品质 空缺表示无法升阶
func (sd HeroAdvance) NeedQuality() E_Quality {
	return sd.needQuality
}

// 材料数量
func (sd HeroAdvance) Num() int {
	return sd.num
}

// 升阶方式 1无法升阶 2使用相同组的英雄 3使用同种族英雄 4使用不同种族英雄
func (sd HeroAdvance) Type1() E_HeroAdvance {
	return sd.type1
}

// 需要的英雄品质 空缺表示无法升阶
func (sd HeroAdvance) NeedQuality1() E_Quality {
	return sd.needQuality1
}

// 材料数量
func (sd HeroAdvance) Num1() int {
	return sd.num1
}

// 描述
func (sd HeroAdvance) Desc() string {
	return sd.desc
}

// 回退返还同组武将数量
func (sd HeroAdvance) HeroBackPurple() int {
	return sd.heroBackPurple
}

// 回退返还蓝色武将卡数量
func (sd HeroAdvance) HeroBackBlue() int {
	return sd.heroBackBlue
}

func (sd HeroAdvance) Clone() *HeroAdvance {
	n := NewHeroAdvance()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type HeroAdvanceManager struct {
	Datas []*HeroAdvance

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	byFactionAndQuality map[E_Faction]map[E_Quality]*HeroAdvance
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newHeroAdvanceManager() *HeroAdvanceManager {
	mgr := &HeroAdvanceManager{
		Datas: []*HeroAdvance{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>
	mgr.byFactionAndQuality = map[E_Faction]map[E_Quality]*HeroAdvance{}
	//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *HeroAdvanceManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func HeroAdvanceSize() int {
	return heroAdvanceMgr.size
}

func (mgr HeroAdvanceManager) check(path string, row int, sd *HeroAdvance) error {

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *HeroAdvanceManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if len(d.faction) == 0 {
			success = false
			log.Printf("英雄升级表数据[%d]的没有配种族\n", d.id)
		} else {
			for _, f := range d.faction {
				if !Check_E_Faction(f) {
					success = false
					log.Printf("英雄升级表数据[%d]的种族[%d]有误\n", d.id, f)
				} else {
					byQuality, ok := mgr.byFactionAndQuality[f]
					if !ok {
						byQuality = map[E_Quality]*HeroAdvance{
							d.quality: d,
						}
						mgr.byFactionAndQuality[f] = byQuality
					} else {
						byQuality[d.quality] = d
					}
				}
			}
		}
		if d.tYpE != E_HeroAdvance_Cannot {
			if !Check_E_Quality(d.quality) {
				success = false
				log.Printf("英雄升级表数据[%d]的品质[%d]有误\n", d.id, d.quality)
			}
			if d.num <= 0 {
				success = false
				log.Printf("英雄升级表数据[%d]的数量[%d]有误\n", d.id, d.num)
			}
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *HeroAdvanceManager) each(f func(sd *HeroAdvance) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *HeroAdvanceManager) findIf(f func(sd *HeroAdvance) (find bool)) *HeroAdvance {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachHeroAdvance(f func(sd HeroAdvance) (continued bool)) {
	for _, sd := range heroAdvanceMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindHeroAdvanceIf(f func(sd HeroAdvance) bool) (HeroAdvance, bool) {
	for _, sd := range heroAdvanceMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilHeroAdvance, false
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func GetHeroAdvanceByFactionAndQuality(faction E_Faction, quality E_Quality) (HeroAdvance, bool) {
	byQuality, ok := heroAdvanceMgr.byFactionAndQuality[faction]
	if !ok {
		return nilHeroAdvance, false
	}
	d, ok := byQuality[quality]
	if !ok {
		return nilHeroAdvance, false
	}
	return *d, true
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
