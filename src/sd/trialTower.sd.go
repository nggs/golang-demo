// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 试炼塔表
type TrialTower struct {
	id       int              // id
	tYpE     E_TrialTowerType // 1=试炼塔2=魏国试炼塔3=蜀国试炼塔4=吴国试炼塔5=群雄试炼塔，关联trialTowerType表
	layerNum int              // 层数
	scene    int              // 敌人阵容，关联场景表
	reward   int              // 通关奖励，关联奖励表
	boss     bool             // 是否boss关

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	maxLayerNum int // 最大试练塔层数
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewTrialTower() *TrialTower {
	sd := &TrialTower{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// id
func (sd TrialTower) ID() int {
	return sd.id
}

// 1=试炼塔2=魏国试炼塔3=蜀国试炼塔4=吴国试炼塔5=群雄试炼塔，关联trialTowerType表
func (sd TrialTower) Type() E_TrialTowerType {
	return sd.tYpE
}

// 层数
func (sd TrialTower) LayerNum() int {
	return sd.layerNum
}

// 敌人阵容，关联场景表
func (sd TrialTower) Scene() int {
	return sd.scene
}

// 通关奖励，关联奖励表
func (sd TrialTower) Reward() int {
	return sd.reward
}

// 是否boss关
func (sd TrialTower) Boss() bool {
	return sd.boss
}

func (sd TrialTower) Clone() *TrialTower {
	n := NewTrialTower()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 试炼塔表全局属性
type TrialTowerGlobal struct {
	layerHasChallenge       int // 最多显示多少个已经挑战成功的层数
	layerNotChallenge       int // 最多显示多少个未挑战成功的层数
	factionTowerDayTimesMax int // 种族塔每天最多挑战多少次

}

// 最多显示多少个已经挑战成功的层数
func (g TrialTowerGlobal) LayerHasChallenge() int {
	return g.layerHasChallenge
}

// 最多显示多少个未挑战成功的层数
func (g TrialTowerGlobal) LayerNotChallenge() int {
	return g.layerNotChallenge
}

// 种族塔每天最多挑战多少次
func (g TrialTowerGlobal) FactionTowerDayTimesMax() int {
	return g.factionTowerDayTimesMax
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type TrialTowerManager struct {
	Datas  []*TrialTower
	Global TrialTowerGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*TrialTower // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byType map[E_TrialTowerType][]*TrialTower // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newTrialTowerManager() *TrialTowerManager {
	mgr := &TrialTowerManager{
		Datas:  []*TrialTower{},
		byID:   map[int]*TrialTower{},
		byType: map[E_TrialTowerType][]*TrialTower{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *TrialTowerManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byType[d.tYpE] = append(mgr.byType[d.tYpE], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func TrialTowerSize() int {
	return trialTowerMgr.size
}

func (mgr TrialTowerManager) check(path string, row int, sd *TrialTower) error {
	if _, ok := trialTowerMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *TrialTowerManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if _, ok := GetTrialTowerByID(d.id); !ok {
			success = false
			log.Printf("试练塔ID=[%d]找不到对应配置\n", d.id)
		}
		if !Check_E_TrialTowerType(d.tYpE) {
			success = false
			log.Printf("试练塔ID=[%d]的类型[%d]配置有误\n", d.id, d.tYpE)
		}
		towerSDs, ok := GetTrialTowerByType(d.tYpE)
		if !ok {
			success = false
			log.Printf("试练塔ID=[%d]的类型[%d]找不到对应的配置\n", d.id, d.tYpE)
		}
		for _, towerSD := range towerSDs {
			if d.maxLayerNum < towerSD.layerNum {
				d.maxLayerNum = towerSD.layerNum
			}
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *TrialTowerManager) each(f func(sd *TrialTower) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *TrialTowerManager) findIf(f func(sd *TrialTower) (find bool)) *TrialTower {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachTrialTower(f func(sd TrialTower) (continued bool)) {
	for _, sd := range trialTowerMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindTrialTowerIf(f func(sd TrialTower) bool) (TrialTower, bool) {
	for _, sd := range trialTowerMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilTrialTower, false
}

func GetTrialTowerByID(id int) (TrialTower, bool) {
	temp, ok := trialTowerMgr.byID[id]
	if !ok {
		return nilTrialTower, false
	}
	return *temp, true
}

func GetTrialTowerByType(tYpE E_TrialTowerType) (vs []TrialTower, ok bool) {
	var ds []*TrialTower
	ds, ok = trialTowerMgr.byType[tYpE]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachTrialTowerByType(tYpE E_TrialTowerType, fn func(d TrialTower) (continued bool)) bool {
	ds, ok := trialTowerMgr.byType[tYpE]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetTrialTowerGlobal() TrialTowerGlobal {
	return trialTowerMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd TrialTower) MaxLayerNum() int {
	return sd.maxLayerNum
}

func GetTrialTowerByTypeAndLevel(tYpE E_TrialTowerType, level int) (TrialTower, bool) {
	ds, ok := trialTowerMgr.byType[tYpE]
	if !ok {
		return nilTrialTower, false
	}
	for _, d := range ds {
		if d.layerNum == level {
			return *d, true
		}
	}
	return nilTrialTower, false
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
