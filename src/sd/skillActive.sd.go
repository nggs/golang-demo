// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 主动技能表
type SkillActive struct {
	id         int    // 技能id
	skillMod   int    // 技能系数（万分比）
	tYpE       int    // 伤害类型
	value      []int  // 伤害类型参数
	rAnGe      int    // 目标范围（0=无差别，1=前排，2=后排）
	num        int    // 目标数量
	choose     int    // 目标选择（1=按照位置从低到高，2随机，3按照生命值从低到高，4按照生命值比例从低到高）
	buff       []int  // 触发buff
	sound      string // 受击音效
	moveTo     int    // 角色移动（0=不移动，1=移动到目标前，2移动到地图中间），3移动至前/后排
	offsetXY   []int  // 偏移
	boneRotate string // 骨骼旋转
	ani        string // 攻击特效
	moveType   int    // 攻击特效类型（0=不移动，1=飞行，2=激光，3=从目标头上落下，4=屏幕中间播放）
	missile    int    // 导弹编号
	rshadow    int    // 残影
	shake      int    // 震屏
	hitAni     string // 受击特效

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewSkillActive() *SkillActive {
	sd := &SkillActive{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// 技能id
func (sd SkillActive) ID() int {
	return sd.id
}

// 技能系数（万分比）
func (sd SkillActive) SkillMod() int {
	return sd.skillMod
}

// 伤害类型
func (sd SkillActive) Type() int {
	return sd.tYpE
}

// 伤害类型参数
func (sd SkillActive) Value() []int {
	return sd.value
}

// 目标范围（0=无差别，1=前排，2=后排）
func (sd SkillActive) Range() int {
	return sd.rAnGe
}

// 目标数量
func (sd SkillActive) Num() int {
	return sd.num
}

// 目标选择（1=按照位置从低到高，2随机，3按照生命值从低到高，4按照生命值比例从低到高）
func (sd SkillActive) Choose() int {
	return sd.choose
}

// 触发buff
func (sd SkillActive) Buff() []int {
	return sd.buff
}

// 受击音效
func (sd SkillActive) Sound() string {
	return sd.sound
}

// 角色移动（0=不移动，1=移动到目标前，2移动到地图中间），3移动至前/后排
func (sd SkillActive) MoveTo() int {
	return sd.moveTo
}

// 偏移
func (sd SkillActive) OffsetXY() []int {
	return sd.offsetXY
}

// 骨骼旋转
func (sd SkillActive) BoneRotate() string {
	return sd.boneRotate
}

// 攻击特效
func (sd SkillActive) Ani() string {
	return sd.ani
}

// 攻击特效类型（0=不移动，1=飞行，2=激光，3=从目标头上落下，4=屏幕中间播放）
func (sd SkillActive) MoveType() int {
	return sd.moveType
}

// 导弹编号
func (sd SkillActive) Missile() int {
	return sd.missile
}

// 残影
func (sd SkillActive) Rshadow() int {
	return sd.rshadow
}

// 震屏
func (sd SkillActive) Shake() int {
	return sd.shake
}

// 受击特效
func (sd SkillActive) HitAni() string {
	return sd.hitAni
}

func (sd SkillActive) Clone() *SkillActive {
	n := NewSkillActive()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type SkillActiveManager struct {
	Datas []*SkillActive

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*SkillActive // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newSkillActiveManager() *SkillActiveManager {
	mgr := &SkillActiveManager{
		Datas: []*SkillActive{},
		byID:  map[int]*SkillActive{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *SkillActiveManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func SkillActiveSize() int {
	return skillActiveMgr.size
}

func (mgr SkillActiveManager) check(path string, row int, sd *SkillActive) error {
	if _, ok := skillActiveMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *SkillActiveManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *SkillActiveManager) each(f func(sd *SkillActive) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *SkillActiveManager) findIf(f func(sd *SkillActive) (find bool)) *SkillActive {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachSkillActive(f func(sd SkillActive) (continued bool)) {
	for _, sd := range skillActiveMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindSkillActiveIf(f func(sd SkillActive) bool) (SkillActive, bool) {
	for _, sd := range skillActiveMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilSkillActive, false
}

func GetSkillActiveByID(id int) (SkillActive, bool) {
	temp, ok := skillActiveMgr.byID[id]
	if !ok {
		return nilSkillActive, false
	}
	return *temp, true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
