// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 试炼塔类型表
type TrialTowerType struct {
	id       E_TrialTowerType // id
	name     string           // 试炼塔名称
	openTime []E_Week         // 开启时间，每周周几。[1,3,7]表示每周一三七开启
	faction  E_Faction        // 指定出阵的阵营
	bg       string           // 背景图

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewTrialTowerType() *TrialTowerType {
	sd := &TrialTowerType{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// id
func (sd TrialTowerType) ID() E_TrialTowerType {
	return sd.id
}

// 试炼塔名称
func (sd TrialTowerType) Name() string {
	return sd.name
}

// 开启时间，每周周几。[1,3,7]表示每周一三七开启
func (sd TrialTowerType) OpenTime() []E_Week {
	return sd.openTime
}

// 指定出阵的阵营
func (sd TrialTowerType) Faction() E_Faction {
	return sd.faction
}

// 背景图
func (sd TrialTowerType) Bg() string {
	return sd.bg
}

func (sd TrialTowerType) Clone() *TrialTowerType {
	n := NewTrialTowerType()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type TrialTowerTypeManager struct {
	Datas []*TrialTowerType

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID      map[E_TrialTowerType]*TrialTowerType // UniqueIndex
	byFaction map[E_Faction]*TrialTowerType        // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newTrialTowerTypeManager() *TrialTowerTypeManager {
	mgr := &TrialTowerTypeManager{
		Datas: []*TrialTowerType{},
		byID:  map[E_TrialTowerType]*TrialTowerType{}, byFaction: map[E_Faction]*TrialTowerType{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *TrialTowerTypeManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d
		mgr.byFaction[d.faction] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func TrialTowerTypeSize() int {
	return trialTowerTypeMgr.size
}

func (mgr TrialTowerTypeManager) check(path string, row int, sd *TrialTowerType) error {
	if _, ok := trialTowerTypeMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}
	if _, ok := trialTowerTypeMgr.byFaction[sd.faction]; ok {
		return fmt.Errorf("[%s]第[%d]行的faction[%v]重复", path, row, sd.faction)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *TrialTowerTypeManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *TrialTowerTypeManager) each(f func(sd *TrialTowerType) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *TrialTowerTypeManager) findIf(f func(sd *TrialTowerType) (find bool)) *TrialTowerType {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachTrialTowerType(f func(sd TrialTowerType) (continued bool)) {
	for _, sd := range trialTowerTypeMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindTrialTowerTypeIf(f func(sd TrialTowerType) bool) (TrialTowerType, bool) {
	for _, sd := range trialTowerTypeMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilTrialTowerType, false
}

func GetTrialTowerTypeByID(id E_TrialTowerType) (TrialTowerType, bool) {
	temp, ok := trialTowerTypeMgr.byID[id]
	if !ok {
		return nilTrialTowerType, false
	}
	return *temp, true
}

func GetTrialTowerTypeByFaction(faction E_Faction) (TrialTowerType, bool) {
	temp, ok := trialTowerTypeMgr.byFaction[faction]
	if !ok {
		return nilTrialTowerType, false
	}
	return *temp, true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
