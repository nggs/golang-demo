// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！
package sd

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ItemBigType] begin

// 道具大类型
type E_ItemBigType int

const (
	E_ItemBigType_Null E_ItemBigType = 0

	// 货币
	E_ItemBigType_Currency E_ItemBigType = 1
	// 道具
	E_ItemBigType_Item E_ItemBigType = 2
	// 武将
	E_ItemBigType_Hero E_ItemBigType = 3
	// 装备
	E_ItemBigType_Equip E_ItemBigType = 4
	// 神器
	E_ItemBigType_Artifact E_ItemBigType = 5
	// 兵书
	E_ItemBigType_Warcraft E_ItemBigType = 6
)

var E_ItemBigType_name = map[int]string{
	1: "E_ItemBigType_Currency",
	2: "E_ItemBigType_Item",
	3: "E_ItemBigType_Hero",
	4: "E_ItemBigType_Equip",
	5: "E_ItemBigType_Artifact",
	6: "E_ItemBigType_Warcraft",
}

var E_ItemBigType_value = map[string]int{
	"E_ItemBigType_Currency": 1,
	"E_ItemBigType_Item":     2,
	"E_ItemBigType_Hero":     3,
	"E_ItemBigType_Equip":    4,
	"E_ItemBigType_Artifact": 5,
	"E_ItemBigType_Warcraft": 6,
}

var E_ItemBigType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
}

func (x E_ItemBigType) String() string {
	if name, ok := E_ItemBigType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ItemBigType_Size() int {
	return len(E_ItemBigType_Slice)
}

func Check_E_ItemBigType_I(value int) bool {
	if _, ok := E_ItemBigType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ItemBigType(value E_ItemBigType) bool {
	return Check_E_ItemBigType_I(int(value))
}

func Each_E_ItemBigType(f func(E_ItemBigType) (continued bool)) {
	for _, value := range E_ItemBigType_Slice {
		if !f(E_ItemBigType(value)) {
			break
		}
	}
}

func Each_E_ItemBigType_I(f func(int) (continued bool)) {
	for _, value := range E_ItemBigType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ItemBigType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ItemType] begin

// 道具类型
type E_ItemType int

const (
	E_ItemType_Null E_ItemType = 0

	// 招募令
	E_ItemType_HeroDrawTickets E_ItemType = 1
	// 急行军令
	E_ItemType_Accelerator E_ItemType = 2
	// 铸铁
	E_ItemType_Iron E_ItemType = 3
	// 消耗品
	E_ItemType_Consumables E_ItemType = 4
	// 门票钥匙
	E_ItemType_Key E_ItemType = 5
	// 礼包
	E_ItemType_GiftPack E_ItemType = 6
	// 武将碎片
	E_ItemType_HeroDebris E_ItemType = 7
	// 活动/节日道具
	E_ItemType_EventItems E_ItemType = 8
	// 强化石
	E_ItemType_Enhancement E_ItemType = 9
	// 多选一礼包
	E_ItemType_MultChoiceGiftPack E_ItemType = 10
	// 紫色武将阵营选择令
	E_ItemType_FactionHeroDrawTickets E_ItemType = 11
	// 竹简
	E_ItemType_BambooSlip E_ItemType = 12
)

var E_ItemType_name = map[int]string{
	1:  "E_ItemType_HeroDrawTickets",
	2:  "E_ItemType_Accelerator",
	3:  "E_ItemType_Iron",
	4:  "E_ItemType_Consumables",
	5:  "E_ItemType_Key",
	6:  "E_ItemType_GiftPack",
	7:  "E_ItemType_HeroDebris",
	8:  "E_ItemType_EventItems",
	9:  "E_ItemType_Enhancement",
	10: "E_ItemType_MultChoiceGiftPack",
	11: "E_ItemType_FactionHeroDrawTickets",
	12: "E_ItemType_BambooSlip",
}

var E_ItemType_value = map[string]int{
	"E_ItemType_HeroDrawTickets":        1,
	"E_ItemType_Accelerator":            2,
	"E_ItemType_Iron":                   3,
	"E_ItemType_Consumables":            4,
	"E_ItemType_Key":                    5,
	"E_ItemType_GiftPack":               6,
	"E_ItemType_HeroDebris":             7,
	"E_ItemType_EventItems":             8,
	"E_ItemType_Enhancement":            9,
	"E_ItemType_MultChoiceGiftPack":     10,
	"E_ItemType_FactionHeroDrawTickets": 11,
	"E_ItemType_BambooSlip":             12,
}

var E_ItemType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
}

func (x E_ItemType) String() string {
	if name, ok := E_ItemType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ItemType_Size() int {
	return len(E_ItemType_Slice)
}

func Check_E_ItemType_I(value int) bool {
	if _, ok := E_ItemType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ItemType(value E_ItemType) bool {
	return Check_E_ItemType_I(int(value))
}

func Each_E_ItemType(f func(E_ItemType) (continued bool)) {
	for _, value := range E_ItemType_Slice {
		if !f(E_ItemType(value)) {
			break
		}
	}
}

func Each_E_ItemType_I(f func(int) (continued bool)) {
	for _, value := range E_ItemType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ItemType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_CurrencyType] begin

// 货币类型
type E_CurrencyType int

const (
	E_CurrencyType_Null E_CurrencyType = 0

	// 元宝
	E_CurrencyType_GoldCoin E_CurrencyType = 1
	// 银币
	E_CurrencyType_SilverCoin E_CurrencyType = 2
	// 御将旗
	E_CurrencyType_HeroMasterFlag E_CurrencyType = 3
	// 公会币
	E_CurrencyType_GuildPoints E_CurrencyType = 4
	// 将魂
	E_CurrencyType_HeroSoul E_CurrencyType = 5
	// 奇门令
	E_CurrencyType_DaoistMagicToken E_CurrencyType = 6
	// 公会活跃值
	E_CurrencyType_GuildActivityPoints E_CurrencyType = 7
	// 日常活动活跃值
	E_CurrencyType_DailyActivityPoints E_CurrencyType = 8
	// 好友点
	E_CurrencyType_CompanionPoints E_CurrencyType = 9
	// 美酒
	E_CurrencyType_GoodWine E_CurrencyType = 10
	// 高阶竞技币
	E_CurrencyType_Prestige E_CurrencyType = 11
	// 周常任务活跃值
	E_CurrencyType_WeeklyActivityPoints E_CurrencyType = 12
	// 救援次数
	E_CurrencyType_RescueTimes E_CurrencyType = 13
	// 竞技币
	E_CurrencyType_ArenaCoin E_CurrencyType = 14
	// 兵书积分
	E_CurrencyType_WarcraftPoint E_CurrencyType = 15
	// 战功
	E_CurrencyType_BattlePoint E_CurrencyType = 16
	// 通关令
	E_CurrencyType_HeroChallengeToken E_CurrencyType = 17
	// 兑换物1
	E_CurrencyType_ExchangeItem1 E_CurrencyType = 18
	// 兑换物2
	E_CurrencyType_ExchangeItem2 E_CurrencyType = 19
	// 兑换物3
	E_CurrencyType_ExchangeItem3 E_CurrencyType = 20
	// 兑换物4
	E_CurrencyType_ExchangeItem4 E_CurrencyType = 21
	// 兑换物5
	E_CurrencyType_ExchangeItem5 E_CurrencyType = 22
	// 兑换物6
	E_CurrencyType_ExchangeItem6 E_CurrencyType = 23
	// 兑换物7
	E_CurrencyType_ExchangeItem7 E_CurrencyType = 24
	// 兑换物8
	E_CurrencyType_ExchangeItem8 E_CurrencyType = 25
	// 战旗
	E_CurrencyType_BattleFlag E_CurrencyType = 26
	// 战鼓
	E_CurrencyType_WarDrum E_CurrencyType = 27
	// 号角
	E_CurrencyType_Horn E_CurrencyType = 28
	// 沙盘
	E_CurrencyType_SandTable E_CurrencyType = 29
	// 攻城车
	E_CurrencyType_AttackCar E_CurrencyType = 30
	// 运粮车
	E_CurrencyType_FoodCar E_CurrencyType = 31
	// 贵族经验
	E_CurrencyType_VipExp E_CurrencyType = 51
	// 角色经验
	E_CurrencyType_RoleExp E_CurrencyType = 52
	// 武将经验
	E_CurrencyType_HeroExp E_CurrencyType = 53
	// 公会经验
	E_CurrencyType_GuildExp E_CurrencyType = 54
	// 封赏令
	E_CurrencyType_AssaultPoints E_CurrencyType = 55
	// 装备经验
	E_CurrencyType_EquipmentExp E_CurrencyType = 56
	// 竞技场积分
	E_CurrencyType_ArenaPoints E_CurrencyType = 57
	// 阵营积分
	E_CurrencyType_FactionPoints E_CurrencyType = 58
	// 高阶竞技场积分
	E_CurrencyType_LegendArenaPoints E_CurrencyType = 59
	// 活动道具101
	E_CurrencyType_Currency101 E_CurrencyType = 101
	// 活动道具102
	E_CurrencyType_Currency102 E_CurrencyType = 102
	// 活动道具103
	E_CurrencyType_Currency103 E_CurrencyType = 103
	// 活动道具104
	E_CurrencyType_Currency104 E_CurrencyType = 104
	// 活动道具105
	E_CurrencyType_Currency105 E_CurrencyType = 105
	// 活动道具106
	E_CurrencyType_Currency106 E_CurrencyType = 106
	// 活动道具107
	E_CurrencyType_Currency107 E_CurrencyType = 107
	// 活动道具108
	E_CurrencyType_Currency108 E_CurrencyType = 108
	// 活动道具109
	E_CurrencyType_Currency109 E_CurrencyType = 109
	// 活动道具110
	E_CurrencyType_Currency110 E_CurrencyType = 110
	// 活动道具111
	E_CurrencyType_Currency111 E_CurrencyType = 111
	// 活动道具112
	E_CurrencyType_Currency112 E_CurrencyType = 112
	// 活动道具113
	E_CurrencyType_Currency113 E_CurrencyType = 113
	// 活动道具114
	E_CurrencyType_Currency114 E_CurrencyType = 114
	// 活动道具115
	E_CurrencyType_Currency115 E_CurrencyType = 115
	// 活动道具116
	E_CurrencyType_Currency116 E_CurrencyType = 116
	// 活动道具117
	E_CurrencyType_Currency117 E_CurrencyType = 117
	// 活动道具118
	E_CurrencyType_Currency118 E_CurrencyType = 118
	// 活动道具119
	E_CurrencyType_Currency119 E_CurrencyType = 119
	// 活动道具120
	E_CurrencyType_Currency120 E_CurrencyType = 120
	// 活动道具121
	E_CurrencyType_Currency121 E_CurrencyType = 121
	// 活动道具122
	E_CurrencyType_Currency122 E_CurrencyType = 122
	// 活动道具123
	E_CurrencyType_Currency123 E_CurrencyType = 123
	// 活动道具124
	E_CurrencyType_Currency124 E_CurrencyType = 124
	// 活动道具125
	E_CurrencyType_Currency125 E_CurrencyType = 125
	// 活动道具126
	E_CurrencyType_Currency126 E_CurrencyType = 126
	// 活动道具127
	E_CurrencyType_Currency127 E_CurrencyType = 127
	// 活动道具128
	E_CurrencyType_Currency128 E_CurrencyType = 128
	// 活动道具129
	E_CurrencyType_Currency129 E_CurrencyType = 129
	// 活动道具130
	E_CurrencyType_Currency130 E_CurrencyType = 130
	// 活动道具131
	E_CurrencyType_Currency131 E_CurrencyType = 131
	// 活动道具132
	E_CurrencyType_Currency132 E_CurrencyType = 132
	// 活动道具133
	E_CurrencyType_Currency133 E_CurrencyType = 133
	// 活动道具134
	E_CurrencyType_Currency134 E_CurrencyType = 134
	// 活动道具135
	E_CurrencyType_Currency135 E_CurrencyType = 135
	// 活动道具136
	E_CurrencyType_Currency136 E_CurrencyType = 136
	// 活动道具137
	E_CurrencyType_Currency137 E_CurrencyType = 137
	// 活动道具138
	E_CurrencyType_Currency138 E_CurrencyType = 138
	// 活动道具139
	E_CurrencyType_Currency139 E_CurrencyType = 139
	// 活动道具140
	E_CurrencyType_Currency140 E_CurrencyType = 140
	// 活动道具141
	E_CurrencyType_Currency141 E_CurrencyType = 141
	// 活动道具142
	E_CurrencyType_Currency142 E_CurrencyType = 142
	// 活动道具143
	E_CurrencyType_Currency143 E_CurrencyType = 143
	// 活动道具144
	E_CurrencyType_Currency144 E_CurrencyType = 144
	// 活动道具145
	E_CurrencyType_Currency145 E_CurrencyType = 145
	// 活动道具146
	E_CurrencyType_Currency146 E_CurrencyType = 146
	// 活动道具147
	E_CurrencyType_Currency147 E_CurrencyType = 147
	// 活动道具148
	E_CurrencyType_Currency148 E_CurrencyType = 148
	// 活动道具149
	E_CurrencyType_Currency149 E_CurrencyType = 149
	// 活动道具150
	E_CurrencyType_Currency150 E_CurrencyType = 150
	// 活动道具151
	E_CurrencyType_Currency151 E_CurrencyType = 151
	// 活动道具152
	E_CurrencyType_Currency152 E_CurrencyType = 152
	// 活动道具153
	E_CurrencyType_Currency153 E_CurrencyType = 153
	// 活动道具154
	E_CurrencyType_Currency154 E_CurrencyType = 154
	// 活动道具155
	E_CurrencyType_Currency155 E_CurrencyType = 155
	// 活动道具156
	E_CurrencyType_Currency156 E_CurrencyType = 156
	// 活动道具157
	E_CurrencyType_Currency157 E_CurrencyType = 157
	// 活动道具158
	E_CurrencyType_Currency158 E_CurrencyType = 158
	// 活动道具159
	E_CurrencyType_Currency159 E_CurrencyType = 159
	// 活动道具160
	E_CurrencyType_Currency160 E_CurrencyType = 160
	// 活动道具161
	E_CurrencyType_Currency161 E_CurrencyType = 161
	// 活动道具162
	E_CurrencyType_Currency162 E_CurrencyType = 162
	// 活动道具163
	E_CurrencyType_Currency163 E_CurrencyType = 163
	// 活动道具164
	E_CurrencyType_Currency164 E_CurrencyType = 164
	// 活动道具165
	E_CurrencyType_Currency165 E_CurrencyType = 165
	// 活动道具166
	E_CurrencyType_Currency166 E_CurrencyType = 166
	// 活动道具167
	E_CurrencyType_Currency167 E_CurrencyType = 167
	// 活动道具168
	E_CurrencyType_Currency168 E_CurrencyType = 168
	// 活动道具169
	E_CurrencyType_Currency169 E_CurrencyType = 169
	// 活动道具170
	E_CurrencyType_Currency170 E_CurrencyType = 170
)

var E_CurrencyType_name = map[int]string{
	1:   "E_CurrencyType_GoldCoin",
	2:   "E_CurrencyType_SilverCoin",
	3:   "E_CurrencyType_HeroMasterFlag",
	4:   "E_CurrencyType_GuildPoints",
	5:   "E_CurrencyType_HeroSoul",
	6:   "E_CurrencyType_DaoistMagicToken",
	7:   "E_CurrencyType_GuildActivityPoints",
	8:   "E_CurrencyType_DailyActivityPoints",
	9:   "E_CurrencyType_CompanionPoints",
	10:  "E_CurrencyType_GoodWine",
	11:  "E_CurrencyType_Prestige",
	12:  "E_CurrencyType_WeeklyActivityPoints",
	13:  "E_CurrencyType_RescueTimes",
	14:  "E_CurrencyType_ArenaCoin",
	15:  "E_CurrencyType_WarcraftPoint",
	16:  "E_CurrencyType_BattlePoint",
	17:  "E_CurrencyType_HeroChallengeToken",
	18:  "E_CurrencyType_ExchangeItem1",
	19:  "E_CurrencyType_ExchangeItem2",
	20:  "E_CurrencyType_ExchangeItem3",
	21:  "E_CurrencyType_ExchangeItem4",
	22:  "E_CurrencyType_ExchangeItem5",
	23:  "E_CurrencyType_ExchangeItem6",
	24:  "E_CurrencyType_ExchangeItem7",
	25:  "E_CurrencyType_ExchangeItem8",
	26:  "E_CurrencyType_BattleFlag",
	27:  "E_CurrencyType_WarDrum",
	28:  "E_CurrencyType_Horn",
	29:  "E_CurrencyType_SandTable",
	30:  "E_CurrencyType_AttackCar",
	31:  "E_CurrencyType_FoodCar",
	51:  "E_CurrencyType_VipExp",
	52:  "E_CurrencyType_RoleExp",
	53:  "E_CurrencyType_HeroExp",
	54:  "E_CurrencyType_GuildExp",
	55:  "E_CurrencyType_AssaultPoints",
	56:  "E_CurrencyType_EquipmentExp",
	57:  "E_CurrencyType_ArenaPoints",
	58:  "E_CurrencyType_FactionPoints",
	59:  "E_CurrencyType_LegendArenaPoints",
	101: "E_CurrencyType_Currency101",
	102: "E_CurrencyType_Currency102",
	103: "E_CurrencyType_Currency103",
	104: "E_CurrencyType_Currency104",
	105: "E_CurrencyType_Currency105",
	106: "E_CurrencyType_Currency106",
	107: "E_CurrencyType_Currency107",
	108: "E_CurrencyType_Currency108",
	109: "E_CurrencyType_Currency109",
	110: "E_CurrencyType_Currency110",
	111: "E_CurrencyType_Currency111",
	112: "E_CurrencyType_Currency112",
	113: "E_CurrencyType_Currency113",
	114: "E_CurrencyType_Currency114",
	115: "E_CurrencyType_Currency115",
	116: "E_CurrencyType_Currency116",
	117: "E_CurrencyType_Currency117",
	118: "E_CurrencyType_Currency118",
	119: "E_CurrencyType_Currency119",
	120: "E_CurrencyType_Currency120",
	121: "E_CurrencyType_Currency121",
	122: "E_CurrencyType_Currency122",
	123: "E_CurrencyType_Currency123",
	124: "E_CurrencyType_Currency124",
	125: "E_CurrencyType_Currency125",
	126: "E_CurrencyType_Currency126",
	127: "E_CurrencyType_Currency127",
	128: "E_CurrencyType_Currency128",
	129: "E_CurrencyType_Currency129",
	130: "E_CurrencyType_Currency130",
	131: "E_CurrencyType_Currency131",
	132: "E_CurrencyType_Currency132",
	133: "E_CurrencyType_Currency133",
	134: "E_CurrencyType_Currency134",
	135: "E_CurrencyType_Currency135",
	136: "E_CurrencyType_Currency136",
	137: "E_CurrencyType_Currency137",
	138: "E_CurrencyType_Currency138",
	139: "E_CurrencyType_Currency139",
	140: "E_CurrencyType_Currency140",
	141: "E_CurrencyType_Currency141",
	142: "E_CurrencyType_Currency142",
	143: "E_CurrencyType_Currency143",
	144: "E_CurrencyType_Currency144",
	145: "E_CurrencyType_Currency145",
	146: "E_CurrencyType_Currency146",
	147: "E_CurrencyType_Currency147",
	148: "E_CurrencyType_Currency148",
	149: "E_CurrencyType_Currency149",
	150: "E_CurrencyType_Currency150",
	151: "E_CurrencyType_Currency151",
	152: "E_CurrencyType_Currency152",
	153: "E_CurrencyType_Currency153",
	154: "E_CurrencyType_Currency154",
	155: "E_CurrencyType_Currency155",
	156: "E_CurrencyType_Currency156",
	157: "E_CurrencyType_Currency157",
	158: "E_CurrencyType_Currency158",
	159: "E_CurrencyType_Currency159",
	160: "E_CurrencyType_Currency160",
	161: "E_CurrencyType_Currency161",
	162: "E_CurrencyType_Currency162",
	163: "E_CurrencyType_Currency163",
	164: "E_CurrencyType_Currency164",
	165: "E_CurrencyType_Currency165",
	166: "E_CurrencyType_Currency166",
	167: "E_CurrencyType_Currency167",
	168: "E_CurrencyType_Currency168",
	169: "E_CurrencyType_Currency169",
	170: "E_CurrencyType_Currency170",
}

var E_CurrencyType_value = map[string]int{
	"E_CurrencyType_GoldCoin":             1,
	"E_CurrencyType_SilverCoin":           2,
	"E_CurrencyType_HeroMasterFlag":       3,
	"E_CurrencyType_GuildPoints":          4,
	"E_CurrencyType_HeroSoul":             5,
	"E_CurrencyType_DaoistMagicToken":     6,
	"E_CurrencyType_GuildActivityPoints":  7,
	"E_CurrencyType_DailyActivityPoints":  8,
	"E_CurrencyType_CompanionPoints":      9,
	"E_CurrencyType_GoodWine":             10,
	"E_CurrencyType_Prestige":             11,
	"E_CurrencyType_WeeklyActivityPoints": 12,
	"E_CurrencyType_RescueTimes":          13,
	"E_CurrencyType_ArenaCoin":            14,
	"E_CurrencyType_WarcraftPoint":        15,
	"E_CurrencyType_BattlePoint":          16,
	"E_CurrencyType_HeroChallengeToken":   17,
	"E_CurrencyType_ExchangeItem1":        18,
	"E_CurrencyType_ExchangeItem2":        19,
	"E_CurrencyType_ExchangeItem3":        20,
	"E_CurrencyType_ExchangeItem4":        21,
	"E_CurrencyType_ExchangeItem5":        22,
	"E_CurrencyType_ExchangeItem6":        23,
	"E_CurrencyType_ExchangeItem7":        24,
	"E_CurrencyType_ExchangeItem8":        25,
	"E_CurrencyType_BattleFlag":           26,
	"E_CurrencyType_WarDrum":              27,
	"E_CurrencyType_Horn":                 28,
	"E_CurrencyType_SandTable":            29,
	"E_CurrencyType_AttackCar":            30,
	"E_CurrencyType_FoodCar":              31,
	"E_CurrencyType_VipExp":               51,
	"E_CurrencyType_RoleExp":              52,
	"E_CurrencyType_HeroExp":              53,
	"E_CurrencyType_GuildExp":             54,
	"E_CurrencyType_AssaultPoints":        55,
	"E_CurrencyType_EquipmentExp":         56,
	"E_CurrencyType_ArenaPoints":          57,
	"E_CurrencyType_FactionPoints":        58,
	"E_CurrencyType_LegendArenaPoints":    59,
	"E_CurrencyType_Currency101":          101,
	"E_CurrencyType_Currency102":          102,
	"E_CurrencyType_Currency103":          103,
	"E_CurrencyType_Currency104":          104,
	"E_CurrencyType_Currency105":          105,
	"E_CurrencyType_Currency106":          106,
	"E_CurrencyType_Currency107":          107,
	"E_CurrencyType_Currency108":          108,
	"E_CurrencyType_Currency109":          109,
	"E_CurrencyType_Currency110":          110,
	"E_CurrencyType_Currency111":          111,
	"E_CurrencyType_Currency112":          112,
	"E_CurrencyType_Currency113":          113,
	"E_CurrencyType_Currency114":          114,
	"E_CurrencyType_Currency115":          115,
	"E_CurrencyType_Currency116":          116,
	"E_CurrencyType_Currency117":          117,
	"E_CurrencyType_Currency118":          118,
	"E_CurrencyType_Currency119":          119,
	"E_CurrencyType_Currency120":          120,
	"E_CurrencyType_Currency121":          121,
	"E_CurrencyType_Currency122":          122,
	"E_CurrencyType_Currency123":          123,
	"E_CurrencyType_Currency124":          124,
	"E_CurrencyType_Currency125":          125,
	"E_CurrencyType_Currency126":          126,
	"E_CurrencyType_Currency127":          127,
	"E_CurrencyType_Currency128":          128,
	"E_CurrencyType_Currency129":          129,
	"E_CurrencyType_Currency130":          130,
	"E_CurrencyType_Currency131":          131,
	"E_CurrencyType_Currency132":          132,
	"E_CurrencyType_Currency133":          133,
	"E_CurrencyType_Currency134":          134,
	"E_CurrencyType_Currency135":          135,
	"E_CurrencyType_Currency136":          136,
	"E_CurrencyType_Currency137":          137,
	"E_CurrencyType_Currency138":          138,
	"E_CurrencyType_Currency139":          139,
	"E_CurrencyType_Currency140":          140,
	"E_CurrencyType_Currency141":          141,
	"E_CurrencyType_Currency142":          142,
	"E_CurrencyType_Currency143":          143,
	"E_CurrencyType_Currency144":          144,
	"E_CurrencyType_Currency145":          145,
	"E_CurrencyType_Currency146":          146,
	"E_CurrencyType_Currency147":          147,
	"E_CurrencyType_Currency148":          148,
	"E_CurrencyType_Currency149":          149,
	"E_CurrencyType_Currency150":          150,
	"E_CurrencyType_Currency151":          151,
	"E_CurrencyType_Currency152":          152,
	"E_CurrencyType_Currency153":          153,
	"E_CurrencyType_Currency154":          154,
	"E_CurrencyType_Currency155":          155,
	"E_CurrencyType_Currency156":          156,
	"E_CurrencyType_Currency157":          157,
	"E_CurrencyType_Currency158":          158,
	"E_CurrencyType_Currency159":          159,
	"E_CurrencyType_Currency160":          160,
	"E_CurrencyType_Currency161":          161,
	"E_CurrencyType_Currency162":          162,
	"E_CurrencyType_Currency163":          163,
	"E_CurrencyType_Currency164":          164,
	"E_CurrencyType_Currency165":          165,
	"E_CurrencyType_Currency166":          166,
	"E_CurrencyType_Currency167":          167,
	"E_CurrencyType_Currency168":          168,
	"E_CurrencyType_Currency169":          169,
	"E_CurrencyType_Currency170":          170,
}

var E_CurrencyType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	14,
	15,
	16,
	17,
	18,
	19,
	20,
	21,
	22,
	23,
	24,
	25,
	26,
	27,
	28,
	29,
	30,
	31,
	51,
	52,
	53,
	54,
	55,
	56,
	57,
	58,
	59,
	101,
	102,
	103,
	104,
	105,
	106,
	107,
	108,
	109,
	110,
	111,
	112,
	113,
	114,
	115,
	116,
	117,
	118,
	119,
	120,
	121,
	122,
	123,
	124,
	125,
	126,
	127,
	128,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
	150,
	151,
	152,
	153,
	154,
	155,
	156,
	157,
	158,
	159,
	160,
	161,
	162,
	163,
	164,
	165,
	166,
	167,
	168,
	169,
	170,
}

func (x E_CurrencyType) String() string {
	if name, ok := E_CurrencyType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_CurrencyType_Size() int {
	return len(E_CurrencyType_Slice)
}

func Check_E_CurrencyType_I(value int) bool {
	if _, ok := E_CurrencyType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_CurrencyType(value E_CurrencyType) bool {
	return Check_E_CurrencyType_I(int(value))
}

func Each_E_CurrencyType(f func(E_CurrencyType) (continued bool)) {
	for _, value := range E_CurrencyType_Slice {
		if !f(E_CurrencyType(value)) {
			break
		}
	}
}

func Each_E_CurrencyType_I(f func(int) (continued bool)) {
	for _, value := range E_CurrencyType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_CurrencyType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_AcceleratorType] begin

// 急行军令掉落类型
type E_AcceleratorType int

const (
	E_AcceleratorType_Null E_AcceleratorType = 0

	// 急行军令掉落银币
	E_AcceleratorType_SilverCoin E_AcceleratorType = 1
	// 急行军令掉落武将经验
	E_AcceleratorType_HeroExp E_AcceleratorType = 2
	// 急行军令掉落武将突破丹
	E_AcceleratorType_LimitPassPill E_AcceleratorType = 3
	// 急行军令三者都掉
	E_AcceleratorType_Total E_AcceleratorType = 4
	// 急行军令掉落银币没有VIP加成
	E_AcceleratorType_SilverCoinNoVIP E_AcceleratorType = 5
	// 急行军令掉落武将经验没有VIP加成
	E_AcceleratorType_HeroExpNoVIP E_AcceleratorType = 6
	// 急行军令掉落武将突破丹没有VIP加成
	E_AcceleratorType_LimitPassPillNoVIP E_AcceleratorType = 7
	// 急行军令三者都掉没有VIP加成
	E_AcceleratorType_TotalNoVIP E_AcceleratorType = 8
)

var E_AcceleratorType_name = map[int]string{
	1: "E_AcceleratorType_SilverCoin",
	2: "E_AcceleratorType_HeroExp",
	3: "E_AcceleratorType_LimitPassPill",
	4: "E_AcceleratorType_Total",
	5: "E_AcceleratorType_SilverCoinNoVIP",
	6: "E_AcceleratorType_HeroExpNoVIP",
	7: "E_AcceleratorType_LimitPassPillNoVIP",
	8: "E_AcceleratorType_TotalNoVIP",
}

var E_AcceleratorType_value = map[string]int{
	"E_AcceleratorType_SilverCoin":         1,
	"E_AcceleratorType_HeroExp":            2,
	"E_AcceleratorType_LimitPassPill":      3,
	"E_AcceleratorType_Total":              4,
	"E_AcceleratorType_SilverCoinNoVIP":    5,
	"E_AcceleratorType_HeroExpNoVIP":       6,
	"E_AcceleratorType_LimitPassPillNoVIP": 7,
	"E_AcceleratorType_TotalNoVIP":         8,
}

var E_AcceleratorType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
}

func (x E_AcceleratorType) String() string {
	if name, ok := E_AcceleratorType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_AcceleratorType_Size() int {
	return len(E_AcceleratorType_Slice)
}

func Check_E_AcceleratorType_I(value int) bool {
	if _, ok := E_AcceleratorType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_AcceleratorType(value E_AcceleratorType) bool {
	return Check_E_AcceleratorType_I(int(value))
}

func Each_E_AcceleratorType(f func(E_AcceleratorType) (continued bool)) {
	for _, value := range E_AcceleratorType_Slice {
		if !f(E_AcceleratorType(value)) {
			break
		}
	}
}

func Each_E_AcceleratorType_I(f func(int) (continued bool)) {
	for _, value := range E_AcceleratorType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_AcceleratorType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_Quality] begin

// 品质类型
type E_Quality int

const (
	E_Quality_Null E_Quality = 0

	// 灰色
	E_Quality_Gray E_Quality = 1
	// 绿色
	E_Quality_Green E_Quality = 2
	// 蓝色
	E_Quality_Blue E_Quality = 3
	// 蓝色+
	E_Quality_BluePlus E_Quality = 4
	// 紫色
	E_Quality_Purple E_Quality = 5
	// 紫色+
	E_Quality_PurplePlus E_Quality = 6
	// 橙色
	E_Quality_Orange E_Quality = 7
	// 橙色+
	E_Quality_OrangePlus E_Quality = 8
	// 红色
	E_Quality_Red E_Quality = 9
	// 红色+
	E_Quality_RedPlus E_Quality = 10
	// 白金
	E_Quality_Platinum E_Quality = 11
	// 白金+1
	E_Quality_PlatinumOne E_Quality = 12
	// 白金+2
	E_Quality_PlatinumTwo E_Quality = 13
	// 白金+3
	E_Quality_PlatinumThree E_Quality = 14
	// 白金+4
	E_Quality_PlatinumFour E_Quality = 15
	// 白金+5
	E_Quality_PlatinumFive E_Quality = 16
)

var E_Quality_name = map[int]string{
	1:  "E_Quality_Gray",
	2:  "E_Quality_Green",
	3:  "E_Quality_Blue",
	4:  "E_Quality_BluePlus",
	5:  "E_Quality_Purple",
	6:  "E_Quality_PurplePlus",
	7:  "E_Quality_Orange",
	8:  "E_Quality_OrangePlus",
	9:  "E_Quality_Red",
	10: "E_Quality_RedPlus",
	11: "E_Quality_Platinum",
	12: "E_Quality_PlatinumOne",
	13: "E_Quality_PlatinumTwo",
	14: "E_Quality_PlatinumThree",
	15: "E_Quality_PlatinumFour",
	16: "E_Quality_PlatinumFive",
}

var E_Quality_value = map[string]int{
	"E_Quality_Gray":          1,
	"E_Quality_Green":         2,
	"E_Quality_Blue":          3,
	"E_Quality_BluePlus":      4,
	"E_Quality_Purple":        5,
	"E_Quality_PurplePlus":    6,
	"E_Quality_Orange":        7,
	"E_Quality_OrangePlus":    8,
	"E_Quality_Red":           9,
	"E_Quality_RedPlus":       10,
	"E_Quality_Platinum":      11,
	"E_Quality_PlatinumOne":   12,
	"E_Quality_PlatinumTwo":   13,
	"E_Quality_PlatinumThree": 14,
	"E_Quality_PlatinumFour":  15,
	"E_Quality_PlatinumFive":  16,
}

var E_Quality_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	14,
	15,
	16,
}

func (x E_Quality) String() string {
	if name, ok := E_Quality_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_Quality_Size() int {
	return len(E_Quality_Slice)
}

func Check_E_Quality_I(value int) bool {
	if _, ok := E_Quality_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_Quality(value E_Quality) bool {
	return Check_E_Quality_I(int(value))
}

func Each_E_Quality(f func(E_Quality) (continued bool)) {
	for _, value := range E_Quality_Slice {
		if !f(E_Quality(value)) {
			break
		}
	}
}

func Each_E_Quality_I(f func(int) (continued bool)) {
	for _, value := range E_Quality_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_Quality] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_Faction] begin

// 种族类型
type E_Faction int

const (
	E_Faction_Null E_Faction = 0

	// 魏国
	E_Faction_Wei E_Faction = 1
	// 蜀国
	E_Faction_Shu E_Faction = 2
	// 吴国
	E_Faction_Wu E_Faction = 3
	// 群雄
	E_Faction_Qun E_Faction = 4
	// 仙
	E_Faction_Xian E_Faction = 5
	// 魔
	E_Faction_Mo E_Faction = 6
	// 无
	E_Faction_Nothing E_Faction = 7
)

var E_Faction_name = map[int]string{
	1: "E_Faction_Wei",
	2: "E_Faction_Shu",
	3: "E_Faction_Wu",
	4: "E_Faction_Qun",
	5: "E_Faction_Xian",
	6: "E_Faction_Mo",
	7: "E_Faction_Nothing",
}

var E_Faction_value = map[string]int{
	"E_Faction_Wei":     1,
	"E_Faction_Shu":     2,
	"E_Faction_Wu":      3,
	"E_Faction_Qun":     4,
	"E_Faction_Xian":    5,
	"E_Faction_Mo":      6,
	"E_Faction_Nothing": 7,
}

var E_Faction_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
}

func (x E_Faction) String() string {
	if name, ok := E_Faction_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_Faction_Size() int {
	return len(E_Faction_Slice)
}

func Check_E_Faction_I(value int) bool {
	if _, ok := E_Faction_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_Faction(value E_Faction) bool {
	return Check_E_Faction_I(int(value))
}

func Each_E_Faction(f func(E_Faction) (continued bool)) {
	for _, value := range E_Faction_Slice {
		if !f(E_Faction(value)) {
			break
		}
	}
}

func Each_E_Faction_I(f func(int) (continued bool)) {
	for _, value := range E_Faction_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_Faction] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_HeroType] begin

// 职业类型
type E_HeroType int

const (
	E_HeroType_Null E_HeroType = 0

	// 力量型
	E_HeroType_Strength E_HeroType = 1
	// 智力型
	E_HeroType_Intelligence E_HeroType = 2
	// 敏捷型
	E_HeroType_Agile E_HeroType = 3
)

var E_HeroType_name = map[int]string{
	1: "E_HeroType_Strength",
	2: "E_HeroType_Intelligence",
	3: "E_HeroType_Agile",
}

var E_HeroType_value = map[string]int{
	"E_HeroType_Strength":     1,
	"E_HeroType_Intelligence": 2,
	"E_HeroType_Agile":        3,
}

var E_HeroType_Slice = []int{
	1,
	2,
	3,
}

func (x E_HeroType) String() string {
	if name, ok := E_HeroType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_HeroType_Size() int {
	return len(E_HeroType_Slice)
}

func Check_E_HeroType_I(value int) bool {
	if _, ok := E_HeroType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_HeroType(value E_HeroType) bool {
	return Check_E_HeroType_I(int(value))
}

func Each_E_HeroType(f func(E_HeroType) (continued bool)) {
	for _, value := range E_HeroType_Slice {
		if !f(E_HeroType(value)) {
			break
		}
	}
}

func Each_E_HeroType_I(f func(int) (continued bool)) {
	for _, value := range E_HeroType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_HeroType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_LocationType] begin

// 职责定位
type E_LocationType int

const (
	E_LocationType_Null E_LocationType = 0

	// 爆发输出
	E_LocationType_Outburst E_LocationType = 1
	// 持续输出
	E_LocationType_Continue E_LocationType = 2
	// 刺客
	E_LocationType_Assassin E_LocationType = 3
	// 减益
	E_LocationType_Debuffs E_LocationType = 4
	// 控制
	E_LocationType_Control E_LocationType = 5
	// 群体输出
	E_LocationType_GroupAtk E_LocationType = 6
	// 坦克
	E_LocationType_Tank E_LocationType = 7
	// 增益
	E_LocationType_Addbuffs E_LocationType = 8
	// 治疗
	E_LocationType_Cure E_LocationType = 9
)

var E_LocationType_name = map[int]string{
	1: "E_LocationType_Outburst",
	2: "E_LocationType_Continue",
	3: "E_LocationType_Assassin",
	4: "E_LocationType_Debuffs",
	5: "E_LocationType_Control",
	6: "E_LocationType_GroupAtk",
	7: "E_LocationType_Tank",
	8: "E_LocationType_Addbuffs",
	9: "E_LocationType_Cure",
}

var E_LocationType_value = map[string]int{
	"E_LocationType_Outburst": 1,
	"E_LocationType_Continue": 2,
	"E_LocationType_Assassin": 3,
	"E_LocationType_Debuffs":  4,
	"E_LocationType_Control":  5,
	"E_LocationType_GroupAtk": 6,
	"E_LocationType_Tank":     7,
	"E_LocationType_Addbuffs": 8,
	"E_LocationType_Cure":     9,
}

var E_LocationType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
}

func (x E_LocationType) String() string {
	if name, ok := E_LocationType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_LocationType_Size() int {
	return len(E_LocationType_Slice)
}

func Check_E_LocationType_I(value int) bool {
	if _, ok := E_LocationType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_LocationType(value E_LocationType) bool {
	return Check_E_LocationType_I(int(value))
}

func Each_E_LocationType(f func(E_LocationType) (continued bool)) {
	for _, value := range E_LocationType_Slice {
		if !f(E_LocationType(value)) {
			break
		}
	}
}

func Each_E_LocationType_I(f func(int) (continued bool)) {
	for _, value := range E_LocationType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_LocationType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_HeroClass] begin

// 武将主定位
type E_HeroClass int

const (
	E_HeroClass_Null E_HeroClass = 0

	// 肉盾型
	E_HeroClass_Tank E_HeroClass = 1
	// 输出型
	E_HeroClass_Attacker E_HeroClass = 2
	// 治疗型
	E_HeroClass_Healer E_HeroClass = 3
	// 辅助型
	E_HeroClass_Assistant E_HeroClass = 4
)

var E_HeroClass_name = map[int]string{
	1: "E_HeroClass_Tank",
	2: "E_HeroClass_Attacker",
	3: "E_HeroClass_Healer",
	4: "E_HeroClass_Assistant",
}

var E_HeroClass_value = map[string]int{
	"E_HeroClass_Tank":      1,
	"E_HeroClass_Attacker":  2,
	"E_HeroClass_Healer":    3,
	"E_HeroClass_Assistant": 4,
}

var E_HeroClass_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_HeroClass) String() string {
	if name, ok := E_HeroClass_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_HeroClass_Size() int {
	return len(E_HeroClass_Slice)
}

func Check_E_HeroClass_I(value int) bool {
	if _, ok := E_HeroClass_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_HeroClass(value E_HeroClass) bool {
	return Check_E_HeroClass_I(int(value))
}

func Each_E_HeroClass(f func(E_HeroClass) (continued bool)) {
	for _, value := range E_HeroClass_Slice {
		if !f(E_HeroClass(value)) {
			break
		}
	}
}

func Each_E_HeroClass_I(f func(int) (continued bool)) {
	for _, value := range E_HeroClass_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_HeroClass] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_EquipmentType] begin

// 装备类型
type E_EquipmentType int

const (
	E_EquipmentType_Null E_EquipmentType = 0

	// 军师扇
	E_EquipmentType_Staff E_EquipmentType = 1
	// 布甲
	E_EquipmentType_ClothArmor E_EquipmentType = 2
	// 重武器
	E_EquipmentType_HeavyWeapon E_EquipmentType = 3
	// 板甲
	E_EquipmentType_PlateArmor E_EquipmentType = 4
	// 轻武器
	E_EquipmentType_LightWeapons E_EquipmentType = 5
	// 皮甲
	E_EquipmentType_LeatherArmor E_EquipmentType = 6
)

var E_EquipmentType_name = map[int]string{
	1: "E_EquipmentType_Staff",
	2: "E_EquipmentType_ClothArmor",
	3: "E_EquipmentType_HeavyWeapon",
	4: "E_EquipmentType_PlateArmor",
	5: "E_EquipmentType_LightWeapons",
	6: "E_EquipmentType_LeatherArmor",
}

var E_EquipmentType_value = map[string]int{
	"E_EquipmentType_Staff":        1,
	"E_EquipmentType_ClothArmor":   2,
	"E_EquipmentType_HeavyWeapon":  3,
	"E_EquipmentType_PlateArmor":   4,
	"E_EquipmentType_LightWeapons": 5,
	"E_EquipmentType_LeatherArmor": 6,
}

var E_EquipmentType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
}

func (x E_EquipmentType) String() string {
	if name, ok := E_EquipmentType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_EquipmentType_Size() int {
	return len(E_EquipmentType_Slice)
}

func Check_E_EquipmentType_I(value int) bool {
	if _, ok := E_EquipmentType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_EquipmentType(value E_EquipmentType) bool {
	return Check_E_EquipmentType_I(int(value))
}

func Each_E_EquipmentType(f func(E_EquipmentType) (continued bool)) {
	for _, value := range E_EquipmentType_Slice {
		if !f(E_EquipmentType(value)) {
			break
		}
	}
}

func Each_E_EquipmentType_I(f func(int) (continued bool)) {
	for _, value := range E_EquipmentType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_EquipmentType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_EquipmentPos] begin

// 装备部位类型
type E_EquipmentPos int

const (
	E_EquipmentPos_Null E_EquipmentPos = 0

	// 武器
	E_EquipmentPos_Weapon E_EquipmentPos = 1
	// 帽子
	E_EquipmentPos_Helmet E_EquipmentPos = 2
	// 衣服
	E_EquipmentPos_Armor E_EquipmentPos = 3
	// 鞋子
	E_EquipmentPos_Boot E_EquipmentPos = 4
)

var E_EquipmentPos_name = map[int]string{
	1: "E_EquipmentPos_Weapon",
	2: "E_EquipmentPos_Helmet",
	3: "E_EquipmentPos_Armor",
	4: "E_EquipmentPos_Boot",
}

var E_EquipmentPos_value = map[string]int{
	"E_EquipmentPos_Weapon": 1,
	"E_EquipmentPos_Helmet": 2,
	"E_EquipmentPos_Armor":  3,
	"E_EquipmentPos_Boot":   4,
}

var E_EquipmentPos_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_EquipmentPos) String() string {
	if name, ok := E_EquipmentPos_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_EquipmentPos_Size() int {
	return len(E_EquipmentPos_Slice)
}

func Check_E_EquipmentPos_I(value int) bool {
	if _, ok := E_EquipmentPos_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_EquipmentPos(value E_EquipmentPos) bool {
	return Check_E_EquipmentPos_I(int(value))
}

func Each_E_EquipmentPos(f func(E_EquipmentPos) (continued bool)) {
	for _, value := range E_EquipmentPos_Slice {
		if !f(E_EquipmentPos(value)) {
			break
		}
	}
}

func Each_E_EquipmentPos_I(f func(int) (continued bool)) {
	for _, value := range E_EquipmentPos_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_EquipmentPos] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_LimitRefreshType] begin

// 限制类型
type E_LimitRefreshType int

const (
	E_LimitRefreshType_Null E_LimitRefreshType = 0

	// 永远不刷新，用完拉倒
	E_LimitRefreshType_Never E_LimitRefreshType = 1
	// 每隔一段时间刷新，次数用完才开始走CD
	E_LimitRefreshType_Dur E_LimitRefreshType = 2
	// 每日固定时间点刷新
	E_LimitRefreshType_Day E_LimitRefreshType = 3
	// 每周固定时间点刷新
	E_LimitRefreshType_Week E_LimitRefreshType = 4
	// 每月固定时间点刷新
	E_LimitRefreshType_Month E_LimitRefreshType = 5
	// 手动重置后，在某段时间里生效
	E_LimitRefreshType_Period E_LimitRefreshType = 6
)

var E_LimitRefreshType_name = map[int]string{
	1: "E_LimitRefreshType_Never",
	2: "E_LimitRefreshType_Dur",
	3: "E_LimitRefreshType_Day",
	4: "E_LimitRefreshType_Week",
	5: "E_LimitRefreshType_Month",
	6: "E_LimitRefreshType_Period",
}

var E_LimitRefreshType_value = map[string]int{
	"E_LimitRefreshType_Never":  1,
	"E_LimitRefreshType_Dur":    2,
	"E_LimitRefreshType_Day":    3,
	"E_LimitRefreshType_Week":   4,
	"E_LimitRefreshType_Month":  5,
	"E_LimitRefreshType_Period": 6,
}

var E_LimitRefreshType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
}

func (x E_LimitRefreshType) String() string {
	if name, ok := E_LimitRefreshType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_LimitRefreshType_Size() int {
	return len(E_LimitRefreshType_Slice)
}

func Check_E_LimitRefreshType_I(value int) bool {
	if _, ok := E_LimitRefreshType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_LimitRefreshType(value E_LimitRefreshType) bool {
	return Check_E_LimitRefreshType_I(int(value))
}

func Each_E_LimitRefreshType(f func(E_LimitRefreshType) (continued bool)) {
	for _, value := range E_LimitRefreshType_Slice {
		if !f(E_LimitRefreshType(value)) {
			break
		}
	}
}

func Each_E_LimitRefreshType_I(f func(int) (continued bool)) {
	for _, value := range E_LimitRefreshType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_LimitRefreshType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_FunctionType] begin

// 功能类型
type E_FunctionType int

const (
	E_FunctionType_Null E_FunctionType = 0

	// 主城
	E_FunctionType_MainCtiy E_FunctionType = 10
	// 野外
	E_FunctionType_Wild E_FunctionType = 20
	// 战役
	E_FunctionType_Campaign E_FunctionType = 30
	// 关卡
	E_FunctionType_Stage E_FunctionType = 100
	// 挂机
	E_FunctionType_Idle E_FunctionType = 200
	// 快速挂机
	E_FunctionType_FastIdle E_FunctionType = 201
	// 战斗2倍速
	E_FunctionType_FastBattle E_FunctionType = 202
	// 充值任意金额领取指定时间内的挂机收益
	E_FunctionType_MoonlightBox E_FunctionType = 203
	// 英雄（英雄背包、英雄图鉴、英雄升级、英雄技能展示）
	E_FunctionType_Hero E_FunctionType = 300
	// 英雄背包
	E_FunctionType_HeroBag E_FunctionType = 400
	// 英雄图鉴
	E_FunctionType_HeroIllustration E_FunctionType = 500
	// 英雄升级
	E_FunctionType_HeroLevelUp E_FunctionType = 600
	// 主界面逻辑框架
	E_FunctionType_MainUILogic E_FunctionType = 700
	// 英雄装备
	E_FunctionType_Equip E_FunctionType = 800
	// 装备强化
	E_FunctionType_EquipStrengthen E_FunctionType = 900
	// 装备升阶
	E_FunctionType_EquipAdvance E_FunctionType = 901
	// 背包
	E_FunctionType_Bag E_FunctionType = 1000
	// 月桂酒馆
	E_FunctionType_EmployHero E_FunctionType = 1100
	// 指定阵营卡池消耗元宝切换阵营
	E_FunctionType_TavernFactionSwitch E_FunctionType = 1102
	// 演武台
	E_FunctionType_HeroEvolution E_FunctionType = 1200
	// 马车遣散
	E_FunctionType_HeroReborn E_FunctionType = 1301
	// 马车重置
	E_FunctionType_HeroDismiss E_FunctionType = 1302
	// 装备分解
	E_FunctionType_EquipBreak E_FunctionType = 1303
	// 武将转换
	E_FunctionType_HeroDisplace E_FunctionType = 1304
	// 装备重铸
	E_FunctionType_EquipmentDisplace E_FunctionType = 1305
	// 武将回退
	E_FunctionType_HeroBack E_FunctionType = 1306
	// 御将台
	E_FunctionType_HeroShare E_FunctionType = 1400
	// 御将台使用元宝购买槽位
	E_FunctionType_HeroSharePlaceGoLd E_FunctionType = 1401
	// 御将台等级突破
	E_FunctionType_HeroShareLevel E_FunctionType = 1402
	// 战斗（布阵页、战斗模块、结算面板）
	E_FunctionType_Battle E_FunctionType = 1500
	// 跳过战斗
	E_FunctionType_BattleSkip E_FunctionType = 1501
	// 关卡跳过战斗
	E_FunctionType_BattleSkipStage E_FunctionType = 1502
	// 试炼塔跳过战斗
	E_FunctionType_BattleSkipTower E_FunctionType = 1503
	// 竞技场跳过战斗
	E_FunctionType_BattleSkipArena E_FunctionType = 1504
	// 关卡战斗中BUFF
	E_FunctionType_BattleBUFFStage E_FunctionType = 1505
	// 试炼塔战斗中BUFF
	E_FunctionType_BattleBUFFTrialTower E_FunctionType = 1506
	// 英雄技能
	E_FunctionType_Skill E_FunctionType = 1600
	// 竞技场
	E_FunctionType_Arena E_FunctionType = 1701
	// 高阶竞技场
	E_FunctionType_LegendArena E_FunctionType = 1702
	// 巅峰竞技场
	E_FunctionType_Championship E_FunctionType = 1703
	// 竞技场跳过
	E_FunctionType_ArenaPass E_FunctionType = 1704
	// 悬赏栏个人
	E_FunctionType_BountyOneself E_FunctionType = 1801
	// 悬赏栏团队
	E_FunctionType_BountyTeam E_FunctionType = 1802
	// 悬赏栏一键上阵
	E_FunctionType_BountyAuto E_FunctionType = 1803
	// 商店
	E_FunctionType_Shop E_FunctionType = 1900
	// 普通商店
	E_FunctionType_CommonShop E_FunctionType = 1901
	// 公会商店
	E_FunctionType_GuildShop E_FunctionType = 1902
	// 遣散商店
	E_FunctionType_DismissShop E_FunctionType = 1903
	// 奇门商店
	E_FunctionType_DaoistMagic E_FunctionType = 1904
	// 高阶竞技场商店
	E_FunctionType_LegendArenaShop E_FunctionType = 1905
	// 竞技场商店
	E_FunctionType_ArenaShop E_FunctionType = 1906
	// 战功商店
	E_FunctionType_BattlePointsShop E_FunctionType = 1907
	// 兵书积分商城
	E_FunctionType_WarcraftPointShop E_FunctionType = 1908
	// 过关斩将商店
	E_FunctionType_HeroChallengeShop E_FunctionType = 1909
	// 异界迷宫
	E_FunctionType_Maze E_FunctionType = 2000
	// 异界迷宫第三层困难
	E_FunctionType_MazeHard E_FunctionType = 2001
	// 奇门遁甲手动刷新
	E_FunctionType_MazeRefresh E_FunctionType = 2002
	// 奇门遁甲一键扫荡
	E_FunctionType_MazeAuto E_FunctionType = 2003
	// 风云阁
	E_FunctionType_Library E_FunctionType = 2100
	// 邮件
	E_FunctionType_Mall E_FunctionType = 2200
	// 普通试炼塔
	E_FunctionType_TrialTower E_FunctionType = 2301
	// 阵营塔-魏国试练塔
	E_FunctionType_TrialTowerWei E_FunctionType = 2302
	// 阵营塔-蜀国试练塔
	E_FunctionType_TrialTowerShu E_FunctionType = 2303
	// 阵营塔-吴国试练塔
	E_FunctionType_TrialTowerWu E_FunctionType = 2304
	// 阵营塔-群雄试练塔
	E_FunctionType_TrialTowerQun E_FunctionType = 2305
	// 任务
	E_FunctionType_Task E_FunctionType = 2400
	// 日常任务
	E_FunctionType_DailyQuest E_FunctionType = 2401
	// 周常任务
	E_FunctionType_WeeklyQuest E_FunctionType = 2402
	// 主线任务
	E_FunctionType_CampaignQuest E_FunctionType = 2403
	// 聊天
	E_FunctionType_Chat E_FunctionType = 2500
	// 跨服聊天
	E_FunctionType_ChatAll E_FunctionType = 2501
	// 排行榜
	E_FunctionType_Rank E_FunctionType = 2600
	// 公会
	E_FunctionType_Guild E_FunctionType = 2700
	// 大厅
	E_FunctionType_Hall E_FunctionType = 2800
	// 狩猎
	E_FunctionType_Hunt E_FunctionType = 2900
	// 狩猎扫荡
	E_FunctionType_HuntPass E_FunctionType = 2901
	// 世界BOSS
	E_FunctionType_WorldBoss E_FunctionType = 2910
	// 世界BOSS挑战次数购买
	E_FunctionType_WorldBossTimesBuy E_FunctionType = 2911
	// 好友
	E_FunctionType_Friend E_FunctionType = 3000
	// 外援
	E_FunctionType_Reinforcement E_FunctionType = 3001
	// 设置面板（音量调节、角色设置、服务器选择）
	E_FunctionType_Setting E_FunctionType = 3200
	// 酒馆寻宝
	E_FunctionType_TreasureHunt E_FunctionType = 3300
	// 寻宝自选武将
	E_FunctionType_TreasureHuntSelect E_FunctionType = 3301
	// 每日免费领取寻宝令
	E_FunctionType_TreasureDailyReward E_FunctionType = 3302
	// 寻宝十连抽折扣
	E_FunctionType_TreasureHuntDiscount E_FunctionType = 3303
	// 充值
	E_FunctionType_Charge E_FunctionType = 3400
	// VIP
	E_FunctionType_Vip E_FunctionType = 3500
	// 过关斩将
	E_FunctionType_HeroChallenge E_FunctionType = 3600
	// 过关斩将单人模式
	E_FunctionType_HeroChallengeSingleHero E_FunctionType = 3601
	// 过关斩将全体模式
	E_FunctionType_HeroChallengeFiveHero E_FunctionType = 3602
	// 通关任务
	E_FunctionType_StageQuest E_FunctionType = 3701
	// 通关奖励
	E_FunctionType_StageReward E_FunctionType = 3702
	// 登录送大礼
	E_FunctionType_NewRoleLoginReward E_FunctionType = 3800
	// 我要变强
	E_FunctionType_BecomeStronger E_FunctionType = 3900
	// 通关攻略
	E_FunctionType_StageRecommend E_FunctionType = 3901
	// 礼包（月卡、成长礼包、礼包、首充礼包、限时礼包）
	E_FunctionType_GiftPack E_FunctionType = 4000
	// 获取途径
	E_FunctionType_Acquire E_FunctionType = 4100
	// 英雄救美
	E_FunctionType_Rescue E_FunctionType = 4200
	// 英雄救美购买次数
	E_FunctionType_RescueTimes E_FunctionType = 4210
	// 英雄救美一键救援
	E_FunctionType_RescueAuto E_FunctionType = 4220
	// 材料副本
	E_FunctionType_Dungeons E_FunctionType = 4300
	// 装备副本购买次数
	E_FunctionType_EquipmentDungeons E_FunctionType = 4310
	// 武将副本购买次数
	E_FunctionType_HeroDungeons E_FunctionType = 4320
	// 银币副本购买次数
	E_FunctionType_SilverCoinDungeons E_FunctionType = 4330
	// 武将经验副本购买次数
	E_FunctionType_HeroExpDungeons E_FunctionType = 4340
	// 突破丹副本购买次数
	E_FunctionType_LimitPassPillDungeons E_FunctionType = 4350
	// 功能预览
	E_FunctionType_GuideHelp E_FunctionType = 4400
	// 神器系统
	E_FunctionType_Artifact E_FunctionType = 4500
	// 兵书系统
	E_FunctionType_Warcraft E_FunctionType = 4600
	// 角色在卡关后每日削弱属性
	E_FunctionType_SleepEnemyWeaken E_FunctionType = 4700
	// 专属装备
	E_FunctionType_Exclusive E_FunctionType = 4800
	// 首次分享
	E_FunctionType_FirstShare E_FunctionType = 4801
	// 好友邀请
	E_FunctionType_FriendShare E_FunctionType = 4802
	// 微信订阅
	E_FunctionType_WeChatSubscribe E_FunctionType = 4803
	// 活动模块
	E_FunctionType_Activity E_FunctionType = 5000
	// 升级到101级
	E_FunctionType_Level101 E_FunctionType = 6000
	// 在非商店界面中直接购买某样物品
	E_FunctionType_FastBuy E_FunctionType = 7000
	// 角色改名
	E_FunctionType_RoleRename E_FunctionType = 7001
	// 发起分享直接获得的奖励
	E_FunctionType_ShareReward E_FunctionType = 7100
	// 分享次数达标后领取奖励
	E_FunctionType_ShareTimesReward E_FunctionType = 7101
)

var E_FunctionType_name = map[int]string{
	10:   "E_FunctionType_MainCtiy",
	20:   "E_FunctionType_Wild",
	30:   "E_FunctionType_Campaign",
	100:  "E_FunctionType_Stage",
	200:  "E_FunctionType_Idle",
	201:  "E_FunctionType_FastIdle",
	202:  "E_FunctionType_FastBattle",
	203:  "E_FunctionType_MoonlightBox",
	300:  "E_FunctionType_Hero",
	400:  "E_FunctionType_HeroBag",
	500:  "E_FunctionType_HeroIllustration",
	600:  "E_FunctionType_HeroLevelUp",
	700:  "E_FunctionType_MainUILogic",
	800:  "E_FunctionType_Equip",
	900:  "E_FunctionType_EquipStrengthen",
	901:  "E_FunctionType_EquipAdvance",
	1000: "E_FunctionType_Bag",
	1100: "E_FunctionType_EmployHero",
	1102: "E_FunctionType_TavernFactionSwitch",
	1200: "E_FunctionType_HeroEvolution",
	1301: "E_FunctionType_HeroReborn",
	1302: "E_FunctionType_HeroDismiss",
	1303: "E_FunctionType_EquipBreak",
	1304: "E_FunctionType_HeroDisplace",
	1305: "E_FunctionType_EquipmentDisplace",
	1306: "E_FunctionType_HeroBack",
	1400: "E_FunctionType_HeroShare",
	1401: "E_FunctionType_HeroSharePlaceGoLd",
	1402: "E_FunctionType_HeroShareLevel",
	1500: "E_FunctionType_Battle",
	1501: "E_FunctionType_BattleSkip",
	1502: "E_FunctionType_BattleSkipStage",
	1503: "E_FunctionType_BattleSkipTower",
	1504: "E_FunctionType_BattleSkipArena",
	1505: "E_FunctionType_BattleBUFFStage",
	1506: "E_FunctionType_BattleBUFFTrialTower",
	1600: "E_FunctionType_Skill",
	1701: "E_FunctionType_Arena",
	1702: "E_FunctionType_LegendArena",
	1703: "E_FunctionType_Championship",
	1704: "E_FunctionType_ArenaPass",
	1801: "E_FunctionType_BountyOneself",
	1802: "E_FunctionType_BountyTeam",
	1803: "E_FunctionType_BountyAuto",
	1900: "E_FunctionType_Shop",
	1901: "E_FunctionType_CommonShop",
	1902: "E_FunctionType_GuildShop",
	1903: "E_FunctionType_DismissShop",
	1904: "E_FunctionType_DaoistMagic",
	1905: "E_FunctionType_LegendArenaShop",
	1906: "E_FunctionType_ArenaShop",
	1907: "E_FunctionType_BattlePointsShop",
	1908: "E_FunctionType_WarcraftPointShop",
	1909: "E_FunctionType_HeroChallengeShop",
	2000: "E_FunctionType_Maze",
	2001: "E_FunctionType_MazeHard",
	2002: "E_FunctionType_MazeRefresh",
	2003: "E_FunctionType_MazeAuto",
	2100: "E_FunctionType_Library",
	2200: "E_FunctionType_Mall",
	2301: "E_FunctionType_TrialTower",
	2302: "E_FunctionType_TrialTowerWei",
	2303: "E_FunctionType_TrialTowerShu",
	2304: "E_FunctionType_TrialTowerWu",
	2305: "E_FunctionType_TrialTowerQun",
	2400: "E_FunctionType_Task",
	2401: "E_FunctionType_DailyQuest",
	2402: "E_FunctionType_WeeklyQuest",
	2403: "E_FunctionType_CampaignQuest",
	2500: "E_FunctionType_Chat",
	2501: "E_FunctionType_ChatAll",
	2600: "E_FunctionType_Rank",
	2700: "E_FunctionType_Guild",
	2800: "E_FunctionType_Hall",
	2900: "E_FunctionType_Hunt",
	2901: "E_FunctionType_HuntPass",
	2910: "E_FunctionType_WorldBoss",
	2911: "E_FunctionType_WorldBossTimesBuy",
	3000: "E_FunctionType_Friend",
	3001: "E_FunctionType_Reinforcement",
	3200: "E_FunctionType_Setting",
	3300: "E_FunctionType_TreasureHunt",
	3301: "E_FunctionType_TreasureHuntSelect",
	3302: "E_FunctionType_TreasureDailyReward",
	3303: "E_FunctionType_TreasureHuntDiscount",
	3400: "E_FunctionType_Charge",
	3500: "E_FunctionType_Vip",
	3600: "E_FunctionType_HeroChallenge",
	3601: "E_FunctionType_HeroChallengeSingleHero",
	3602: "E_FunctionType_HeroChallengeFiveHero",
	3701: "E_FunctionType_StageQuest",
	3702: "E_FunctionType_StageReward",
	3800: "E_FunctionType_NewRoleLoginReward",
	3900: "E_FunctionType_BecomeStronger",
	3901: "E_FunctionType_StageRecommend",
	4000: "E_FunctionType_GiftPack",
	4100: "E_FunctionType_Acquire",
	4200: "E_FunctionType_Rescue",
	4210: "E_FunctionType_RescueTimes",
	4220: "E_FunctionType_RescueAuto",
	4300: "E_FunctionType_Dungeons",
	4310: "E_FunctionType_EquipmentDungeons",
	4320: "E_FunctionType_HeroDungeons",
	4330: "E_FunctionType_SilverCoinDungeons",
	4340: "E_FunctionType_HeroExpDungeons",
	4350: "E_FunctionType_LimitPassPillDungeons",
	4400: "E_FunctionType_GuideHelp",
	4500: "E_FunctionType_Artifact",
	4600: "E_FunctionType_Warcraft",
	4700: "E_FunctionType_SleepEnemyWeaken",
	4800: "E_FunctionType_Exclusive",
	4801: "E_FunctionType_FirstShare",
	4802: "E_FunctionType_FriendShare",
	4803: "E_FunctionType_WeChatSubscribe",
	5000: "E_FunctionType_Activity",
	6000: "E_FunctionType_Level101",
	7000: "E_FunctionType_FastBuy",
	7001: "E_FunctionType_RoleRename",
	7100: "E_FunctionType_ShareReward",
	7101: "E_FunctionType_ShareTimesReward",
}

var E_FunctionType_value = map[string]int{
	"E_FunctionType_MainCtiy":                10,
	"E_FunctionType_Wild":                    20,
	"E_FunctionType_Campaign":                30,
	"E_FunctionType_Stage":                   100,
	"E_FunctionType_Idle":                    200,
	"E_FunctionType_FastIdle":                201,
	"E_FunctionType_FastBattle":              202,
	"E_FunctionType_MoonlightBox":            203,
	"E_FunctionType_Hero":                    300,
	"E_FunctionType_HeroBag":                 400,
	"E_FunctionType_HeroIllustration":        500,
	"E_FunctionType_HeroLevelUp":             600,
	"E_FunctionType_MainUILogic":             700,
	"E_FunctionType_Equip":                   800,
	"E_FunctionType_EquipStrengthen":         900,
	"E_FunctionType_EquipAdvance":            901,
	"E_FunctionType_Bag":                     1000,
	"E_FunctionType_EmployHero":              1100,
	"E_FunctionType_TavernFactionSwitch":     1102,
	"E_FunctionType_HeroEvolution":           1200,
	"E_FunctionType_HeroReborn":              1301,
	"E_FunctionType_HeroDismiss":             1302,
	"E_FunctionType_EquipBreak":              1303,
	"E_FunctionType_HeroDisplace":            1304,
	"E_FunctionType_EquipmentDisplace":       1305,
	"E_FunctionType_HeroBack":                1306,
	"E_FunctionType_HeroShare":               1400,
	"E_FunctionType_HeroSharePlaceGoLd":      1401,
	"E_FunctionType_HeroShareLevel":          1402,
	"E_FunctionType_Battle":                  1500,
	"E_FunctionType_BattleSkip":              1501,
	"E_FunctionType_BattleSkipStage":         1502,
	"E_FunctionType_BattleSkipTower":         1503,
	"E_FunctionType_BattleSkipArena":         1504,
	"E_FunctionType_BattleBUFFStage":         1505,
	"E_FunctionType_BattleBUFFTrialTower":    1506,
	"E_FunctionType_Skill":                   1600,
	"E_FunctionType_Arena":                   1701,
	"E_FunctionType_LegendArena":             1702,
	"E_FunctionType_Championship":            1703,
	"E_FunctionType_ArenaPass":               1704,
	"E_FunctionType_BountyOneself":           1801,
	"E_FunctionType_BountyTeam":              1802,
	"E_FunctionType_BountyAuto":              1803,
	"E_FunctionType_Shop":                    1900,
	"E_FunctionType_CommonShop":              1901,
	"E_FunctionType_GuildShop":               1902,
	"E_FunctionType_DismissShop":             1903,
	"E_FunctionType_DaoistMagic":             1904,
	"E_FunctionType_LegendArenaShop":         1905,
	"E_FunctionType_ArenaShop":               1906,
	"E_FunctionType_BattlePointsShop":        1907,
	"E_FunctionType_WarcraftPointShop":       1908,
	"E_FunctionType_HeroChallengeShop":       1909,
	"E_FunctionType_Maze":                    2000,
	"E_FunctionType_MazeHard":                2001,
	"E_FunctionType_MazeRefresh":             2002,
	"E_FunctionType_MazeAuto":                2003,
	"E_FunctionType_Library":                 2100,
	"E_FunctionType_Mall":                    2200,
	"E_FunctionType_TrialTower":              2301,
	"E_FunctionType_TrialTowerWei":           2302,
	"E_FunctionType_TrialTowerShu":           2303,
	"E_FunctionType_TrialTowerWu":            2304,
	"E_FunctionType_TrialTowerQun":           2305,
	"E_FunctionType_Task":                    2400,
	"E_FunctionType_DailyQuest":              2401,
	"E_FunctionType_WeeklyQuest":             2402,
	"E_FunctionType_CampaignQuest":           2403,
	"E_FunctionType_Chat":                    2500,
	"E_FunctionType_ChatAll":                 2501,
	"E_FunctionType_Rank":                    2600,
	"E_FunctionType_Guild":                   2700,
	"E_FunctionType_Hall":                    2800,
	"E_FunctionType_Hunt":                    2900,
	"E_FunctionType_HuntPass":                2901,
	"E_FunctionType_WorldBoss":               2910,
	"E_FunctionType_WorldBossTimesBuy":       2911,
	"E_FunctionType_Friend":                  3000,
	"E_FunctionType_Reinforcement":           3001,
	"E_FunctionType_Setting":                 3200,
	"E_FunctionType_TreasureHunt":            3300,
	"E_FunctionType_TreasureHuntSelect":      3301,
	"E_FunctionType_TreasureDailyReward":     3302,
	"E_FunctionType_TreasureHuntDiscount":    3303,
	"E_FunctionType_Charge":                  3400,
	"E_FunctionType_Vip":                     3500,
	"E_FunctionType_HeroChallenge":           3600,
	"E_FunctionType_HeroChallengeSingleHero": 3601,
	"E_FunctionType_HeroChallengeFiveHero":   3602,
	"E_FunctionType_StageQuest":              3701,
	"E_FunctionType_StageReward":             3702,
	"E_FunctionType_NewRoleLoginReward":      3800,
	"E_FunctionType_BecomeStronger":          3900,
	"E_FunctionType_StageRecommend":          3901,
	"E_FunctionType_GiftPack":                4000,
	"E_FunctionType_Acquire":                 4100,
	"E_FunctionType_Rescue":                  4200,
	"E_FunctionType_RescueTimes":             4210,
	"E_FunctionType_RescueAuto":              4220,
	"E_FunctionType_Dungeons":                4300,
	"E_FunctionType_EquipmentDungeons":       4310,
	"E_FunctionType_HeroDungeons":            4320,
	"E_FunctionType_SilverCoinDungeons":      4330,
	"E_FunctionType_HeroExpDungeons":         4340,
	"E_FunctionType_LimitPassPillDungeons":   4350,
	"E_FunctionType_GuideHelp":               4400,
	"E_FunctionType_Artifact":                4500,
	"E_FunctionType_Warcraft":                4600,
	"E_FunctionType_SleepEnemyWeaken":        4700,
	"E_FunctionType_Exclusive":               4800,
	"E_FunctionType_FirstShare":              4801,
	"E_FunctionType_FriendShare":             4802,
	"E_FunctionType_WeChatSubscribe":         4803,
	"E_FunctionType_Activity":                5000,
	"E_FunctionType_Level101":                6000,
	"E_FunctionType_FastBuy":                 7000,
	"E_FunctionType_RoleRename":              7001,
	"E_FunctionType_ShareReward":             7100,
	"E_FunctionType_ShareTimesReward":        7101,
}

var E_FunctionType_Slice = []int{
	10,
	20,
	30,
	100,
	200,
	201,
	202,
	203,
	300,
	400,
	500,
	600,
	700,
	800,
	900,
	901,
	1000,
	1100,
	1102,
	1200,
	1301,
	1302,
	1303,
	1304,
	1305,
	1306,
	1400,
	1401,
	1402,
	1500,
	1501,
	1502,
	1503,
	1504,
	1505,
	1506,
	1600,
	1701,
	1702,
	1703,
	1704,
	1801,
	1802,
	1803,
	1900,
	1901,
	1902,
	1903,
	1904,
	1905,
	1906,
	1907,
	1908,
	1909,
	2000,
	2001,
	2002,
	2003,
	2100,
	2200,
	2301,
	2302,
	2303,
	2304,
	2305,
	2400,
	2401,
	2402,
	2403,
	2500,
	2501,
	2600,
	2700,
	2800,
	2900,
	2901,
	2910,
	2911,
	3000,
	3001,
	3200,
	3300,
	3301,
	3302,
	3303,
	3400,
	3500,
	3600,
	3601,
	3602,
	3701,
	3702,
	3800,
	3900,
	3901,
	4000,
	4100,
	4200,
	4210,
	4220,
	4300,
	4310,
	4320,
	4330,
	4340,
	4350,
	4400,
	4500,
	4600,
	4700,
	4800,
	4801,
	4802,
	4803,
	5000,
	6000,
	7000,
	7001,
	7100,
	7101,
}

func (x E_FunctionType) String() string {
	if name, ok := E_FunctionType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_FunctionType_Size() int {
	return len(E_FunctionType_Slice)
}

func Check_E_FunctionType_I(value int) bool {
	if _, ok := E_FunctionType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_FunctionType(value E_FunctionType) bool {
	return Check_E_FunctionType_I(int(value))
}

func Each_E_FunctionType(f func(E_FunctionType) (continued bool)) {
	for _, value := range E_FunctionType_Slice {
		if !f(E_FunctionType(value)) {
			break
		}
	}
}

func Each_E_FunctionType_I(f func(int) (continued bool)) {
	for _, value := range E_FunctionType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_FunctionType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_TavernType] begin

// 道具大类型
type E_TavernType int

const (
	E_TavernType_Null E_TavernType = 0

	// 全阵营卡池
	E_TavernType_AllFaction E_TavernType = 1
	// 友情点卡池
	E_TavernType_CompanionPoints E_TavernType = 2
	// 指定阵营卡池
	E_TavernType_AssignFaction E_TavernType = 3
	// 美酒卡池
	E_TavernType_GoodWine E_TavernType = 4
	// 新将招募
	E_TavernType_NewTavern E_TavernType = 5
	// 限时招募
	E_TavernType_LimitTavern E_TavernType = 6
	// 酒馆寻宝;寻宝
	E_TavernType_TreasureHunt E_TavernType = 7
)

var E_TavernType_name = map[int]string{
	1: "E_TavernType_AllFaction",
	2: "E_TavernType_CompanionPoints",
	3: "E_TavernType_AssignFaction",
	4: "E_TavernType_GoodWine",
	5: "E_TavernType_NewTavern",
	6: "E_TavernType_LimitTavern",
	7: "E_TavernType_TreasureHunt",
}

var E_TavernType_value = map[string]int{
	"E_TavernType_AllFaction":      1,
	"E_TavernType_CompanionPoints": 2,
	"E_TavernType_AssignFaction":   3,
	"E_TavernType_GoodWine":        4,
	"E_TavernType_NewTavern":       5,
	"E_TavernType_LimitTavern":     6,
	"E_TavernType_TreasureHunt":    7,
}

var E_TavernType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
}

func (x E_TavernType) String() string {
	if name, ok := E_TavernType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_TavernType_Size() int {
	return len(E_TavernType_Slice)
}

func Check_E_TavernType_I(value int) bool {
	if _, ok := E_TavernType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_TavernType(value E_TavernType) bool {
	return Check_E_TavernType_I(int(value))
}

func Each_E_TavernType(f func(E_TavernType) (continued bool)) {
	for _, value := range E_TavernType_Slice {
		if !f(E_TavernType(value)) {
			break
		}
	}
}

func Each_E_TavernType_I(f func(int) (continued bool)) {
	for _, value := range E_TavernType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_TavernType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_HeroAdvance] begin

// 英雄进阶类型
type E_HeroAdvance int

const (
	E_HeroAdvance_Null E_HeroAdvance = 0

	// 无法升阶
	E_HeroAdvance_Cannot E_HeroAdvance = 1
	// 消耗相同组武将
	E_HeroAdvance_SameGroup E_HeroAdvance = 2
	// 消耗相同种族武将
	E_HeroAdvance_SameFaction E_HeroAdvance = 3
	// 消耗不同种族武将
	E_HeroAdvance_DifferentFaction E_HeroAdvance = 4
)

var E_HeroAdvance_name = map[int]string{
	1: "E_HeroAdvance_Cannot",
	2: "E_HeroAdvance_SameGroup",
	3: "E_HeroAdvance_SameFaction",
	4: "E_HeroAdvance_DifferentFaction",
}

var E_HeroAdvance_value = map[string]int{
	"E_HeroAdvance_Cannot":           1,
	"E_HeroAdvance_SameGroup":        2,
	"E_HeroAdvance_SameFaction":      3,
	"E_HeroAdvance_DifferentFaction": 4,
}

var E_HeroAdvance_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_HeroAdvance) String() string {
	if name, ok := E_HeroAdvance_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_HeroAdvance_Size() int {
	return len(E_HeroAdvance_Slice)
}

func Check_E_HeroAdvance_I(value int) bool {
	if _, ok := E_HeroAdvance_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_HeroAdvance(value E_HeroAdvance) bool {
	return Check_E_HeroAdvance_I(int(value))
}

func Each_E_HeroAdvance(f func(E_HeroAdvance) (continued bool)) {
	for _, value := range E_HeroAdvance_Slice {
		if !f(E_HeroAdvance(value)) {
			break
		}
	}
}

func Each_E_HeroAdvance_I(f func(int) (continued bool)) {
	for _, value := range E_HeroAdvance_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_HeroAdvance] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_NoticeBoardQuality] begin

// 品质类型
type E_NoticeBoardQuality int

const (
	E_NoticeBoardQuality_Null E_NoticeBoardQuality = 0

	// 绿色
	E_NoticeBoardQuality_Green E_NoticeBoardQuality = 1
	// 蓝色
	E_NoticeBoardQuality_Blue E_NoticeBoardQuality = 2
	// 紫色
	E_NoticeBoardQuality_Purple E_NoticeBoardQuality = 3
	// 橙色
	E_NoticeBoardQuality_Orange E_NoticeBoardQuality = 5
	// 红色
	E_NoticeBoardQuality_Red E_NoticeBoardQuality = 6
	// 白金
	E_NoticeBoardQuality_Platinum E_NoticeBoardQuality = 7
)

var E_NoticeBoardQuality_name = map[int]string{
	1: "E_NoticeBoardQuality_Green",
	2: "E_NoticeBoardQuality_Blue",
	3: "E_NoticeBoardQuality_Purple",
	5: "E_NoticeBoardQuality_Orange",
	6: "E_NoticeBoardQuality_Red",
	7: "E_NoticeBoardQuality_Platinum",
}

var E_NoticeBoardQuality_value = map[string]int{
	"E_NoticeBoardQuality_Green":    1,
	"E_NoticeBoardQuality_Blue":     2,
	"E_NoticeBoardQuality_Purple":   3,
	"E_NoticeBoardQuality_Orange":   5,
	"E_NoticeBoardQuality_Red":      6,
	"E_NoticeBoardQuality_Platinum": 7,
}

var E_NoticeBoardQuality_Slice = []int{
	1,
	2,
	3,
	5,
	6,
	7,
}

func (x E_NoticeBoardQuality) String() string {
	if name, ok := E_NoticeBoardQuality_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_NoticeBoardQuality_Size() int {
	return len(E_NoticeBoardQuality_Slice)
}

func Check_E_NoticeBoardQuality_I(value int) bool {
	if _, ok := E_NoticeBoardQuality_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_NoticeBoardQuality(value E_NoticeBoardQuality) bool {
	return Check_E_NoticeBoardQuality_I(int(value))
}

func Each_E_NoticeBoardQuality(f func(E_NoticeBoardQuality) (continued bool)) {
	for _, value := range E_NoticeBoardQuality_Slice {
		if !f(E_NoticeBoardQuality(value)) {
			break
		}
	}
}

func Each_E_NoticeBoardQuality_I(f func(int) (continued bool)) {
	for _, value := range E_NoticeBoardQuality_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_NoticeBoardQuality] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_NoticeBoardType] begin

// 悬赏任务类型
type E_NoticeBoardType int

const (
	E_NoticeBoardType_Null E_NoticeBoardType = 0

	// 单人悬赏任务
	E_NoticeBoardType_Single E_NoticeBoardType = 1
	// 团队悬赏任务
	E_NoticeBoardType_Team E_NoticeBoardType = 2
)

var E_NoticeBoardType_name = map[int]string{
	1: "E_NoticeBoardType_Single",
	2: "E_NoticeBoardType_Team",
}

var E_NoticeBoardType_value = map[string]int{
	"E_NoticeBoardType_Single": 1,
	"E_NoticeBoardType_Team":   2,
}

var E_NoticeBoardType_Slice = []int{
	1,
	2,
}

func (x E_NoticeBoardType) String() string {
	if name, ok := E_NoticeBoardType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_NoticeBoardType_Size() int {
	return len(E_NoticeBoardType_Slice)
}

func Check_E_NoticeBoardType_I(value int) bool {
	if _, ok := E_NoticeBoardType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_NoticeBoardType(value E_NoticeBoardType) bool {
	return Check_E_NoticeBoardType_I(int(value))
}

func Each_E_NoticeBoardType(f func(E_NoticeBoardType) (continued bool)) {
	for _, value := range E_NoticeBoardType_Slice {
		if !f(E_NoticeBoardType(value)) {
			break
		}
	}
}

func Each_E_NoticeBoardType_I(f func(int) (continued bool)) {
	for _, value := range E_NoticeBoardType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_NoticeBoardType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_BattleAttribute] begin

// 战斗属性，100以内是涉及战斗力计算的属性，1000以内是需要下发给客户端的属性，1000以上是战斗中用到的属性
type E_BattleAttribute int

const (
	E_BattleAttribute_Null E_BattleAttribute = 0

	// 生命
	E_BattleAttribute_Hp E_BattleAttribute = 1
	// 攻击
	E_BattleAttribute_Atk E_BattleAttribute = 2
	// 防御
	E_BattleAttribute_Def E_BattleAttribute = 3
	// 暴击
	E_BattleAttribute_Crit E_BattleAttribute = 4
	// 命中
	E_BattleAttribute_Hit E_BattleAttribute = 5
	// 闪避
	E_BattleAttribute_Miss E_BattleAttribute = 6
	// 急速
	E_BattleAttribute_Speed E_BattleAttribute = 7
	// 每秒恢复
	E_BattleAttribute_Recover E_BattleAttribute = 8
	// 物理减伤率
	E_BattleAttribute_PhysicReduce E_BattleAttribute = 9
	// 策略减伤率
	E_BattleAttribute_MagicReduce E_BattleAttribute = 10
	// 吸血等级
	E_BattleAttribute_LifeLeech E_BattleAttribute = 11
	// 受伤回能
	E_BattleAttribute_HurtEnergy E_BattleAttribute = 12
	// 暴击伤害加成
	E_BattleAttribute_CritDamage E_BattleAttribute = 13
	// 生命上限
	E_BattleAttribute_CurHpRatio E_BattleAttribute = 114
	// 能量
	E_BattleAttribute_Energy E_BattleAttribute = 115
	// 护甲
	E_BattleAttribute_Armor E_BattleAttribute = 1016
	// 移速
	E_BattleAttribute_MoveSpeed E_BattleAttribute = 1017
	// 攻击距离
	E_BattleAttribute_AttackRange E_BattleAttribute = 1018
	// 受伤加成
	E_BattleAttribute_DamageBonus E_BattleAttribute = 1019
	// 受到治疗时的额外加成
	E_BattleAttribute_AddRecovery E_BattleAttribute = 1020
	// 当前生命值
	E_BattleAttribute_CurHp E_BattleAttribute = 1021
	// 免死时的最低血量
	E_BattleAttribute_LowestHP E_BattleAttribute = 1022
	// 主动治疗的加成效果
	E_BattleAttribute_HealEffect E_BattleAttribute = 1023
	// 每秒恢复能量
	E_BattleAttribute_EnergyRecoverSecond E_BattleAttribute = 1024
	// 阵营克制
	E_BattleAttribute_RacialRestraint E_BattleAttribute = 1025
	// 增益buff的持续时间减少百分比
	E_BattleAttribute_BuffTimeReduce E_BattleAttribute = 1026
	// 减益buff的持续时间减少百分比
	E_BattleAttribute_DebuffTimeReduce E_BattleAttribute = 1027
	// 所有回能增加百分比
	E_BattleAttribute_AllEnergyReduce E_BattleAttribute = 1028
	// 物理增伤
	E_BattleAttribute_PhysicAdd E_BattleAttribute = 1029
	// 魔法增伤
	E_BattleAttribute_MagicAdd E_BattleAttribute = 1030
)

var E_BattleAttribute_name = map[int]string{
	1:    "E_BattleAttribute_Hp",
	2:    "E_BattleAttribute_Atk",
	3:    "E_BattleAttribute_Def",
	4:    "E_BattleAttribute_Crit",
	5:    "E_BattleAttribute_Hit",
	6:    "E_BattleAttribute_Miss",
	7:    "E_BattleAttribute_Speed",
	8:    "E_BattleAttribute_Recover",
	9:    "E_BattleAttribute_PhysicReduce",
	10:   "E_BattleAttribute_MagicReduce",
	11:   "E_BattleAttribute_LifeLeech",
	12:   "E_BattleAttribute_HurtEnergy",
	13:   "E_BattleAttribute_CritDamage",
	114:  "E_BattleAttribute_CurHpRatio",
	115:  "E_BattleAttribute_Energy",
	1016: "E_BattleAttribute_Armor",
	1017: "E_BattleAttribute_MoveSpeed",
	1018: "E_BattleAttribute_AttackRange",
	1019: "E_BattleAttribute_DamageBonus",
	1020: "E_BattleAttribute_AddRecovery",
	1021: "E_BattleAttribute_CurHp",
	1022: "E_BattleAttribute_LowestHP",
	1023: "E_BattleAttribute_HealEffect",
	1024: "E_BattleAttribute_EnergyRecoverSecond",
	1025: "E_BattleAttribute_RacialRestraint",
	1026: "E_BattleAttribute_BuffTimeReduce",
	1027: "E_BattleAttribute_DebuffTimeReduce",
	1028: "E_BattleAttribute_AllEnergyReduce",
	1029: "E_BattleAttribute_PhysicAdd",
	1030: "E_BattleAttribute_MagicAdd",
}

var E_BattleAttribute_value = map[string]int{
	"E_BattleAttribute_Hp":                  1,
	"E_BattleAttribute_Atk":                 2,
	"E_BattleAttribute_Def":                 3,
	"E_BattleAttribute_Crit":                4,
	"E_BattleAttribute_Hit":                 5,
	"E_BattleAttribute_Miss":                6,
	"E_BattleAttribute_Speed":               7,
	"E_BattleAttribute_Recover":             8,
	"E_BattleAttribute_PhysicReduce":        9,
	"E_BattleAttribute_MagicReduce":         10,
	"E_BattleAttribute_LifeLeech":           11,
	"E_BattleAttribute_HurtEnergy":          12,
	"E_BattleAttribute_CritDamage":          13,
	"E_BattleAttribute_CurHpRatio":          114,
	"E_BattleAttribute_Energy":              115,
	"E_BattleAttribute_Armor":               1016,
	"E_BattleAttribute_MoveSpeed":           1017,
	"E_BattleAttribute_AttackRange":         1018,
	"E_BattleAttribute_DamageBonus":         1019,
	"E_BattleAttribute_AddRecovery":         1020,
	"E_BattleAttribute_CurHp":               1021,
	"E_BattleAttribute_LowestHP":            1022,
	"E_BattleAttribute_HealEffect":          1023,
	"E_BattleAttribute_EnergyRecoverSecond": 1024,
	"E_BattleAttribute_RacialRestraint":     1025,
	"E_BattleAttribute_BuffTimeReduce":      1026,
	"E_BattleAttribute_DebuffTimeReduce":    1027,
	"E_BattleAttribute_AllEnergyReduce":     1028,
	"E_BattleAttribute_PhysicAdd":           1029,
	"E_BattleAttribute_MagicAdd":            1030,
}

var E_BattleAttribute_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	114,
	115,
	1016,
	1017,
	1018,
	1019,
	1020,
	1021,
	1022,
	1023,
	1024,
	1025,
	1026,
	1027,
	1028,
	1029,
	1030,
}

func (x E_BattleAttribute) String() string {
	if name, ok := E_BattleAttribute_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_BattleAttribute_Size() int {
	return len(E_BattleAttribute_Slice)
}

func Check_E_BattleAttribute_I(value int) bool {
	if _, ok := E_BattleAttribute_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_BattleAttribute(value E_BattleAttribute) bool {
	return Check_E_BattleAttribute_I(int(value))
}

func Each_E_BattleAttribute(f func(E_BattleAttribute) (continued bool)) {
	for _, value := range E_BattleAttribute_Slice {
		if !f(E_BattleAttribute(value)) {
			break
		}
	}
}

func Each_E_BattleAttribute_I(f func(int) (continued bool)) {
	for _, value := range E_BattleAttribute_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_BattleAttribute] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ShopType] begin

// 商店类型枚举
type E_ShopType int

const (
	E_ShopType_Null E_ShopType = 0

	// 普通商店
	E_ShopType_CommonShop E_ShopType = 1
	// 公会商店
	E_ShopType_GuildShop E_ShopType = 2
	// 遣散商店
	E_ShopType_DismissShop E_ShopType = 3
	// 奇门遁甲商店
	E_ShopType_DaoistMagicShop E_ShopType = 4
	// 高阶竞技场商店
	E_ShopType_LegendArenaShop E_ShopType = 5
	// 迷宫商人
	E_ShopType_DaoistMagicTrader E_ShopType = 6
	// 竞技场商店
	E_ShopType_ArenaShop E_ShopType = 7
	// 战功商店
	E_ShopType_BattlePointsShop E_ShopType = 8
	// 兵书积分商城
	E_ShopType_WarcraftPointShop E_ShopType = 9
	// 过关斩将商店
	E_ShopType_HeroChallengeShop E_ShopType = 10
)

var E_ShopType_name = map[int]string{
	1:  "E_ShopType_CommonShop",
	2:  "E_ShopType_GuildShop",
	3:  "E_ShopType_DismissShop",
	4:  "E_ShopType_DaoistMagicShop",
	5:  "E_ShopType_LegendArenaShop",
	6:  "E_ShopType_DaoistMagicTrader",
	7:  "E_ShopType_ArenaShop",
	8:  "E_ShopType_BattlePointsShop",
	9:  "E_ShopType_WarcraftPointShop",
	10: "E_ShopType_HeroChallengeShop",
}

var E_ShopType_value = map[string]int{
	"E_ShopType_CommonShop":        1,
	"E_ShopType_GuildShop":         2,
	"E_ShopType_DismissShop":       3,
	"E_ShopType_DaoistMagicShop":   4,
	"E_ShopType_LegendArenaShop":   5,
	"E_ShopType_DaoistMagicTrader": 6,
	"E_ShopType_ArenaShop":         7,
	"E_ShopType_BattlePointsShop":  8,
	"E_ShopType_WarcraftPointShop": 9,
	"E_ShopType_HeroChallengeShop": 10,
}

var E_ShopType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
}

func (x E_ShopType) String() string {
	if name, ok := E_ShopType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ShopType_Size() int {
	return len(E_ShopType_Slice)
}

func Check_E_ShopType_I(value int) bool {
	if _, ok := E_ShopType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ShopType(value E_ShopType) bool {
	return Check_E_ShopType_I(int(value))
}

func Each_E_ShopType(f func(E_ShopType) (continued bool)) {
	for _, value := range E_ShopType_Slice {
		if !f(E_ShopType(value)) {
			break
		}
	}
}

func Each_E_ShopType_I(f func(int) (continued bool)) {
	for _, value := range E_ShopType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ShopType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_MailType] begin

// 邮件类型
type E_MailType int

const (
	E_MailType_Null E_MailType = 0

	// 武将背包超限邮件
	E_MailType_HeroBag E_MailType = 1
	// 竞技场每日排名奖励邮件
	E_MailType_ArenaDayReward E_MailType = 2
	// 竞技场赛季排名奖励邮件
	E_MailType_ArenaMonthReward E_MailType = 3
	// 公会BOSS（团队狩猎）奖励邮件
	E_MailType_GuildBossReward E_MailType = 4
	// 公会BOSS（团队狩猎）最高伤害奖励邮件
	E_MailType_GuildBossNumberOneReward E_MailType = 5
	// 公会BOSS开启通知
	E_MailType_GuildBossOpen E_MailType = 6
	// 巅峰竞技场竞猜邮件
	E_MailType_ChampionshipGuessReward E_MailType = 7
	// 手动邮件
	E_MailType_ManualMail E_MailType = 8
	// 充值成功邮件
	E_MailType_ChargeSuccessMail E_MailType = 9
	// 开服抽将排行榜
	E_MailType_EmployHeroRank E_MailType = 10
	// 开服寻宝排行榜
	E_MailType_LuckBoxRank E_MailType = 11
	// 开服推图排行榜
	E_MailType_StageRank E_MailType = 12
	// 开服武将升级排行榜
	E_MailType_HeroLevelUpRank E_MailType = 13
	// 开服试练塔排行榜
	E_MailType_TrialTowerRank E_MailType = 14
	// 开服武将进阶排行榜
	E_MailType_HeroEvolutionRank E_MailType = 15
	// 开服装备强化排行榜
	E_MailType_EquipStrengthenRank E_MailType = 16
	// 开服公会boss排行榜
	E_MailType_HuntRank E_MailType = 17
	// 开服战力排行榜
	E_MailType_PowerRank E_MailType = 18
	// 开服消耗元宝排行榜
	E_MailType_CostGoldCoinRank E_MailType = 19
	// 月基金补发奖励邮件
	E_MailType_MonthFundReward E_MailType = 20
	// 我的小程序登录奖励
	E_MailType_MiniProgramReward E_MailType = 21
	// 公会会长退位让贤
	E_MailType_Impeach E_MailType = 22
	// 世界BOSS排行榜和参与奖
	E_MailType_WorldBossReward E_MailType = 23
	// 世界BOSS公会奖励
	E_MailType_WorldBossGuild E_MailType = 24
	// 公会自动解散
	E_MailType_GuildBreak E_MailType = 25
	// 每日首充奖励补发
	E_MailType_DayFirstChargeRewardBack E_MailType = 26
	// BUFF（锦囊）有效场次剩余场次补偿
	E_MailType_BattleBUFFMakeUp E_MailType = 27
)

var E_MailType_name = map[int]string{
	1:  "E_MailType_HeroBag",
	2:  "E_MailType_ArenaDayReward",
	3:  "E_MailType_ArenaMonthReward",
	4:  "E_MailType_GuildBossReward",
	5:  "E_MailType_GuildBossNumberOneReward",
	6:  "E_MailType_GuildBossOpen",
	7:  "E_MailType_ChampionshipGuessReward",
	8:  "E_MailType_ManualMail",
	9:  "E_MailType_ChargeSuccessMail",
	10: "E_MailType_EmployHeroRank",
	11: "E_MailType_LuckBoxRank",
	12: "E_MailType_StageRank",
	13: "E_MailType_HeroLevelUpRank",
	14: "E_MailType_TrialTowerRank",
	15: "E_MailType_HeroEvolutionRank",
	16: "E_MailType_EquipStrengthenRank",
	17: "E_MailType_HuntRank",
	18: "E_MailType_PowerRank",
	19: "E_MailType_CostGoldCoinRank",
	20: "E_MailType_MonthFundReward",
	21: "E_MailType_MiniProgramReward",
	22: "E_MailType_Impeach",
	23: "E_MailType_WorldBossReward",
	24: "E_MailType_WorldBossGuild",
	25: "E_MailType_GuildBreak",
	26: "E_MailType_DayFirstChargeRewardBack",
	27: "E_MailType_BattleBUFFMakeUp",
}

var E_MailType_value = map[string]int{
	"E_MailType_HeroBag":                  1,
	"E_MailType_ArenaDayReward":           2,
	"E_MailType_ArenaMonthReward":         3,
	"E_MailType_GuildBossReward":          4,
	"E_MailType_GuildBossNumberOneReward": 5,
	"E_MailType_GuildBossOpen":            6,
	"E_MailType_ChampionshipGuessReward":  7,
	"E_MailType_ManualMail":               8,
	"E_MailType_ChargeSuccessMail":        9,
	"E_MailType_EmployHeroRank":           10,
	"E_MailType_LuckBoxRank":              11,
	"E_MailType_StageRank":                12,
	"E_MailType_HeroLevelUpRank":          13,
	"E_MailType_TrialTowerRank":           14,
	"E_MailType_HeroEvolutionRank":        15,
	"E_MailType_EquipStrengthenRank":      16,
	"E_MailType_HuntRank":                 17,
	"E_MailType_PowerRank":                18,
	"E_MailType_CostGoldCoinRank":         19,
	"E_MailType_MonthFundReward":          20,
	"E_MailType_MiniProgramReward":        21,
	"E_MailType_Impeach":                  22,
	"E_MailType_WorldBossReward":          23,
	"E_MailType_WorldBossGuild":           24,
	"E_MailType_GuildBreak":               25,
	"E_MailType_DayFirstChargeRewardBack": 26,
	"E_MailType_BattleBUFFMakeUp":         27,
}

var E_MailType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	14,
	15,
	16,
	17,
	18,
	19,
	20,
	21,
	22,
	23,
	24,
	25,
	26,
	27,
}

func (x E_MailType) String() string {
	if name, ok := E_MailType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_MailType_Size() int {
	return len(E_MailType_Slice)
}

func Check_E_MailType_I(value int) bool {
	if _, ok := E_MailType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_MailType(value E_MailType) bool {
	return Check_E_MailType_I(int(value))
}

func Each_E_MailType(f func(E_MailType) (continued bool)) {
	for _, value := range E_MailType_Slice {
		if !f(E_MailType(value)) {
			break
		}
	}
}

func Each_E_MailType_I(f func(int) (continued bool)) {
	for _, value := range E_MailType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_MailType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_SkillType] begin

// 技能触发类型
type E_SkillType int

const (
	E_SkillType_Null E_SkillType = 0

	// 被动触发
	E_SkillType_Passive E_SkillType = 1
	// 开场触发
	E_SkillType_Start E_SkillType = 2
	// 死亡触发
	E_SkillType_Death E_SkillType = 3
	// 次数触发
	E_SkillType_Count E_SkillType = 4
	// 普通攻击
	E_SkillType_Normal E_SkillType = 5
	// 开战触发
	E_SkillType_FightBegin E_SkillType = 6
	// 战斗结束
	E_SkillType_FightEnd E_SkillType = 7
)

var E_SkillType_name = map[int]string{
	1: "E_SkillType_Passive",
	2: "E_SkillType_Start",
	3: "E_SkillType_Death",
	4: "E_SkillType_Count",
	5: "E_SkillType_Normal",
	6: "E_SkillType_FightBegin",
	7: "E_SkillType_FightEnd",
}

var E_SkillType_value = map[string]int{
	"E_SkillType_Passive":    1,
	"E_SkillType_Start":      2,
	"E_SkillType_Death":      3,
	"E_SkillType_Count":      4,
	"E_SkillType_Normal":     5,
	"E_SkillType_FightBegin": 6,
	"E_SkillType_FightEnd":   7,
}

var E_SkillType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
}

func (x E_SkillType) String() string {
	if name, ok := E_SkillType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_SkillType_Size() int {
	return len(E_SkillType_Slice)
}

func Check_E_SkillType_I(value int) bool {
	if _, ok := E_SkillType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_SkillType(value E_SkillType) bool {
	return Check_E_SkillType_I(int(value))
}

func Each_E_SkillType(f func(E_SkillType) (continued bool)) {
	for _, value := range E_SkillType_Slice {
		if !f(E_SkillType(value)) {
			break
		}
	}
}

func Each_E_SkillType_I(f func(int) (continued bool)) {
	for _, value := range E_SkillType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_SkillType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_QuestSystemType] begin

// 任务功能类型
type E_QuestSystemType int

const (
	E_QuestSystemType_Null E_QuestSystemType = 0

	// 日常任务
	E_QuestSystemType_DailyQuest E_QuestSystemType = 1
	// 周常任务
	E_QuestSystemType_WeeklyQuest E_QuestSystemType = 2
	// 主线任务
	E_QuestSystemType_CampaignQuest E_QuestSystemType = 3
	// 活动任务
	E_QuestSystemType_ActivityQuest E_QuestSystemType = 4
	// 引导任务
	E_QuestSystemType_GuideQuest E_QuestSystemType = 5
	// 神器任务
	E_QuestSystemType_ArtifactQuest E_QuestSystemType = 6
	// 通关任务
	E_QuestSystemType_StageQuest E_QuestSystemType = 7
)

var E_QuestSystemType_name = map[int]string{
	1: "E_QuestSystemType_DailyQuest",
	2: "E_QuestSystemType_WeeklyQuest",
	3: "E_QuestSystemType_CampaignQuest",
	4: "E_QuestSystemType_ActivityQuest",
	5: "E_QuestSystemType_GuideQuest",
	6: "E_QuestSystemType_ArtifactQuest",
	7: "E_QuestSystemType_StageQuest",
}

var E_QuestSystemType_value = map[string]int{
	"E_QuestSystemType_DailyQuest":    1,
	"E_QuestSystemType_WeeklyQuest":   2,
	"E_QuestSystemType_CampaignQuest": 3,
	"E_QuestSystemType_ActivityQuest": 4,
	"E_QuestSystemType_GuideQuest":    5,
	"E_QuestSystemType_ArtifactQuest": 6,
	"E_QuestSystemType_StageQuest":    7,
}

var E_QuestSystemType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
}

func (x E_QuestSystemType) String() string {
	if name, ok := E_QuestSystemType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_QuestSystemType_Size() int {
	return len(E_QuestSystemType_Slice)
}

func Check_E_QuestSystemType_I(value int) bool {
	if _, ok := E_QuestSystemType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_QuestSystemType(value E_QuestSystemType) bool {
	return Check_E_QuestSystemType_I(int(value))
}

func Each_E_QuestSystemType(f func(E_QuestSystemType) (continued bool)) {
	for _, value := range E_QuestSystemType_Slice {
		if !f(E_QuestSystemType(value)) {
			break
		}
	}
}

func Each_E_QuestSystemType_I(f func(int) (continued bool)) {
	for _, value := range E_QuestSystemType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_QuestSystemType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_QuestType] begin

// 任务类型
type E_QuestType int

const (
	E_QuestType_Null E_QuestType = 0

	// 参加战役
	E_QuestType_JoinCampaign E_QuestType = 1
	// 升级武将
	E_QuestType_HeroLevelUp E_QuestType = 2
	// 领取挂机收益
	E_QuestType_GetIdleReward E_QuestType = 3
	// 赠送好友点
	E_QuestType_GetCompanionPoints E_QuestType = 4
	// 装备强化
	E_QuestType_EquipmentStrengthen E_QuestType = 5
	// 快速挂机
	E_QuestType_GetFastIdle E_QuestType = 6
	// 酒馆召唤武将
	E_QuestType_TavernCallHero E_QuestType = 7
	// 挑战试练塔
	E_QuestType_JoinTrialTower E_QuestType = 8
	// 参加公会BOSS
	E_QuestType_JoinGuildBoss E_QuestType = 9
	// 参加竞技场
	E_QuestType_JoinArena E_QuestType = 10
	// 接受个人或团队布告栏任务
	E_QuestType_GetNoticeBoard E_QuestType = 11
	// 奇门遁甲商店购买商品
	E_QuestType_BuyDaoistMagicShop E_QuestType = 12
	// 公会商店购买商品
	E_QuestType_BuyGuildShop E_QuestType = 13
	// 普通商店购买商品
	E_QuestType_BuyCommonShop E_QuestType = 14
	// 取得普通竞技场胜利
	E_QuestType_WinCommonArena E_QuestType = 15
	// 接受团队布告栏任务
	E_QuestType_GetTeamNoticeBoard E_QuestType = 16
	// 在演武台进阶武将
	E_QuestType_AdvancedMartialArtYardl E_QuestType = 17
	// 完成关卡
	E_QuestType_HeroCompleteStage E_QuestType = 18
	// 升级武将到指定等级
	E_QuestType_HeroMasterYardUpLevel E_QuestType = 19
	// 御将台主将提升到指定等级
	E_QuestType_MainHeroUpLevel E_QuestType = 20
	// 主公等级升到指定等级
	E_QuestType_RoleLevelUpLevel E_QuestType = 21
	// 通关试练塔到指定层数
	E_QuestType_ClearanceTrialTowersFloor E_QuestType = 22
	// 竞技场积分到指定积分
	E_QuestType_ArenaPoints E_QuestType = 23
	// 御将台开启多少个槽位
	E_QuestType_HeroMasterYardPlace E_QuestType = 24
	// 从挂机中累计获得多少金币
	E_QuestType_IdleSilverCoin E_QuestType = 25
	// 获得多少个武将
	E_QuestType_HeroNumber E_QuestType = 26
	// 获得几个紫色的武将
	E_QuestType_PurpleHeroNumber E_QuestType = 27
	// 加入公会
	E_QuestType_JoinGuild E_QuestType = 28
	// 奇门遁甲第1层
	E_QuestType_WinDaoistMagic1Floor E_QuestType = 29
	// 奇门遁甲第2层
	E_QuestType_WinDaoistMagic2Floor E_QuestType = 30
	// 奇门遁甲第3层
	E_QuestType_WinDaoistMagic3Floor E_QuestType = 31
	// 四阵营试练塔都通关到指定层数
	E_QuestType_WinFactionTowerFloor E_QuestType = 32
	// 获取日常任务点数
	E_QuestType_GetDailyActivityPoints E_QuestType = 33
	// 完成指定章节数
	E_QuestType_ChapterClear E_QuestType = 34
	// 提升装备强化等级
	E_QuestType_EquipmentStrengthenLevel E_QuestType = 35
	// 奇门遁甲击败指定数量敌人
	E_QuestType_WinDaoistMagicWiner E_QuestType = 36
	// 分解多少普通武将
	E_QuestType_DecomposeHero E_QuestType = 37
	// 任意商店购买商品
	E_QuestType_BuyAnyShop E_QuestType = 38
	// 登入几天
	E_QuestType_LoginDay E_QuestType = 39
	// 穿戴装备
	E_QuestType_WearEquip E_QuestType = 40
	// 参与资源副本
	E_QuestType_JoinDungeons E_QuestType = 41
	// 参与英雄救美
	E_QuestType_JoinRescue E_QuestType = 42
	// 参与奇门遁甲战斗
	E_QuestType_JoinDaoistMagic E_QuestType = 43
	// 酒馆十连抽
	E_QuestType_TenCallHero E_QuestType = 44
	// 合成武将碎片
	E_QuestType_ComposeHero E_QuestType = 45
	// 重置奇门遁甲
	E_QuestType_ResetDaoistMagic E_QuestType = 46
	// 遣散商店购买商品
	E_QuestType_BuyDecomposeShop E_QuestType = 47
	// 角色取名
	E_QuestType_RoleNameSuccess E_QuestType = 48
	// 当前拥有多少紫色武将
	E_QuestType_HavePurpleHero E_QuestType = 49
	// 当前拥有多少紫色+武将
	E_QuestType_HavePurplePlusHero E_QuestType = 50
	// 当前拥有多少橙色武将
	E_QuestType_HaveOrangeHero E_QuestType = 51
	// 当前拥有多少橙色+武将
	E_QuestType_HaveOrangePlusHero E_QuestType = 52
	// 当前拥有多少红色武将
	E_QuestType_HaveRedHero E_QuestType = 53
	// 当前拥有多少红色+武将
	E_QuestType_HaveRedPlusHero E_QuestType = 54
	// 当前拥有多少白金武将
	E_QuestType_HavePlatinumHero E_QuestType = 55
	// 当前穿戴了多少绿色装备
	E_QuestType_WearGreenEquip E_QuestType = 56
	// 当前穿戴了多少蓝色装备
	E_QuestType_WearBlueEquip E_QuestType = 57
	// 当前穿戴了多少蓝色+装备
	E_QuestType_WearBluePlusEquip E_QuestType = 58
	// 当前穿戴了多少紫色装备
	E_QuestType_WearPurpleEquip E_QuestType = 59
	// 当前穿戴了多少紫色+装备
	E_QuestType_WearPurplePlusEquip E_QuestType = 60
	// 当前穿戴了多少橙色装备
	E_QuestType_WearOrangeEquip E_QuestType = 61
	// 当前穿戴了多少橙色+装备
	E_QuestType_WearOrangePlusEquip E_QuestType = 62
	// 当前穿戴了多少红色装备
	E_QuestType_WearRedEquip E_QuestType = 63
	// 当前穿戴了多少灰色装备
	E_QuestType_WearGrayEquip E_QuestType = 64
	// 激活神器
	E_QuestType_ArtifactActivation E_QuestType = 65
	// 当前拥有多少蓝色武将
	E_QuestType_HaveBlueHero E_QuestType = 66
	// 累计登录天数
	E_QuestType_TotalLoginDays E_QuestType = 67
	// 激活终身卡
	E_QuestType_LifeCardActivation E_QuestType = 68
	// 参加竞技场(历史值)
	E_QuestType_JoinArenaHistory E_QuestType = 69
	// 快速挂机(历史值)
	E_QuestType_GetFastIdleHistory E_QuestType = 70
	// 合成武将碎片(历史值)
	E_QuestType_ComposeHeroHistory E_QuestType = 71
	// 酒馆召唤武将(历史值)
	E_QuestType_TavernCallHeroHistory E_QuestType = 72
	// 领取挂机收益(历史值)
	E_QuestType_GetIdleRewardHistory E_QuestType = 73
	// 分解多少普通武将(历史值)
	E_QuestType_DecomposeHeroHistory E_QuestType = 74
	// 遣散商店购买商品(历史值)
	E_QuestType_BuyDecomposeShopHistory E_QuestType = 75
	// 在演武台进阶武将(历史值)
	E_QuestType_AdvancedMartialArtYardHistory E_QuestType = 76
	// 重置奇门遁甲(历史值)
	E_QuestType_ResetDaoistMagicHistory E_QuestType = 77
	// 激活指定武将的图鉴
	E_QuestType_HeroPortraits E_QuestType = 78
	// 激活月卡
	E_QuestType_MonthCardActivation E_QuestType = 79
	// 寻宝
	E_QuestType_TreasureHunt E_QuestType = 80
	// 全阵营卡池次数
	E_QuestType_AllFactionTimes E_QuestType = 81
	// 世界聊天发言
	E_QuestType_FirstWorldChat E_QuestType = 82
	// 穿戴N件强化5星装备
	E_QuestType_WearFiveStarEquipment E_QuestType = 83
	// 装备分解次数
	E_QuestType_EquipBreakTimes E_QuestType = 84
	// 世界BOSS段位排名达到
	E_QuestType_WorldBOSSRank E_QuestType = 85
	// 单人过关斩将层数
	E_QuestType_HeroChallengeSingleHeroLayer E_QuestType = 86
	// 多人过关斩将层数
	E_QuestType_HeroChallengeFiveHeroLayer E_QuestType = 87
	// 公会BOSS伤害达到
	E_QuestType_GuildBossDamage E_QuestType = 88
	// 穿戴N件紫色兵书
	E_QuestType_WearPurpleWarcraft E_QuestType = 89
	// 穿戴N件橙色兵书
	E_QuestType_WearOrangeWarcraft E_QuestType = 90
	// 穿戴N件红色兵书
	E_QuestType_WearRedWarcraft E_QuestType = 91
	// 穿戴N件白金兵书
	E_QuestType_WearPlatinumWarcraft E_QuestType = 92
	// 高阶竞技场排名
	E_QuestType_LegendArenaRank E_QuestType = 93
	// 兵书搜索次数
	E_QuestType_WarcraftSearchTimes E_QuestType = 94
	// 指定阵营卡池次数
	E_QuestType_AssignFactionTimes E_QuestType = 95
	// 无双招募次数
	E_QuestType_NewTavernTimes E_QuestType = 96
	// 世界BOSS挑战次数
	E_QuestType_WorldBossTimes E_QuestType = 97
	// 材料副本挑战次数
	E_QuestType_DungeonTimes E_QuestType = 98
	// 高阶竞技场挑战次数
	E_QuestType_LegendArenaTimes E_QuestType = 99
	// 过关斩将挑战次数
	E_QuestType_HeroChallengeTimes E_QuestType = 100
)

var E_QuestType_name = map[int]string{
	1:   "E_QuestType_JoinCampaign",
	2:   "E_QuestType_HeroLevelUp",
	3:   "E_QuestType_GetIdleReward",
	4:   "E_QuestType_GetCompanionPoints",
	5:   "E_QuestType_EquipmentStrengthen",
	6:   "E_QuestType_GetFastIdle",
	7:   "E_QuestType_TavernCallHero",
	8:   "E_QuestType_JoinTrialTower",
	9:   "E_QuestType_JoinGuildBoss",
	10:  "E_QuestType_JoinArena",
	11:  "E_QuestType_GetNoticeBoard",
	12:  "E_QuestType_BuyDaoistMagicShop",
	13:  "E_QuestType_BuyGuildShop",
	14:  "E_QuestType_BuyCommonShop",
	15:  "E_QuestType_WinCommonArena",
	16:  "E_QuestType_GetTeamNoticeBoard",
	17:  "E_QuestType_AdvancedMartialArtYardl",
	18:  "E_QuestType_HeroCompleteStage",
	19:  "E_QuestType_HeroMasterYardUpLevel",
	20:  "E_QuestType_MainHeroUpLevel",
	21:  "E_QuestType_RoleLevelUpLevel",
	22:  "E_QuestType_ClearanceTrialTowersFloor",
	23:  "E_QuestType_ArenaPoints",
	24:  "E_QuestType_HeroMasterYardPlace",
	25:  "E_QuestType_IdleSilverCoin",
	26:  "E_QuestType_HeroNumber",
	27:  "E_QuestType_PurpleHeroNumber",
	28:  "E_QuestType_JoinGuild",
	29:  "E_QuestType_WinDaoistMagic1Floor",
	30:  "E_QuestType_WinDaoistMagic2Floor",
	31:  "E_QuestType_WinDaoistMagic3Floor",
	32:  "E_QuestType_WinFactionTowerFloor",
	33:  "E_QuestType_GetDailyActivityPoints",
	34:  "E_QuestType_ChapterClear",
	35:  "E_QuestType_EquipmentStrengthenLevel",
	36:  "E_QuestType_WinDaoistMagicWiner",
	37:  "E_QuestType_DecomposeHero",
	38:  "E_QuestType_BuyAnyShop",
	39:  "E_QuestType_LoginDay",
	40:  "E_QuestType_WearEquip",
	41:  "E_QuestType_JoinDungeons",
	42:  "E_QuestType_JoinRescue",
	43:  "E_QuestType_JoinDaoistMagic",
	44:  "E_QuestType_TenCallHero",
	45:  "E_QuestType_ComposeHero",
	46:  "E_QuestType_ResetDaoistMagic",
	47:  "E_QuestType_BuyDecomposeShop",
	48:  "E_QuestType_RoleNameSuccess",
	49:  "E_QuestType_HavePurpleHero",
	50:  "E_QuestType_HavePurplePlusHero",
	51:  "E_QuestType_HaveOrangeHero",
	52:  "E_QuestType_HaveOrangePlusHero",
	53:  "E_QuestType_HaveRedHero",
	54:  "E_QuestType_HaveRedPlusHero",
	55:  "E_QuestType_HavePlatinumHero",
	56:  "E_QuestType_WearGreenEquip",
	57:  "E_QuestType_WearBlueEquip",
	58:  "E_QuestType_WearBluePlusEquip",
	59:  "E_QuestType_WearPurpleEquip",
	60:  "E_QuestType_WearPurplePlusEquip",
	61:  "E_QuestType_WearOrangeEquip",
	62:  "E_QuestType_WearOrangePlusEquip",
	63:  "E_QuestType_WearRedEquip",
	64:  "E_QuestType_WearGrayEquip",
	65:  "E_QuestType_ArtifactActivation",
	66:  "E_QuestType_HaveBlueHero",
	67:  "E_QuestType_TotalLoginDays",
	68:  "E_QuestType_LifeCardActivation",
	69:  "E_QuestType_JoinArenaHistory",
	70:  "E_QuestType_GetFastIdleHistory",
	71:  "E_QuestType_ComposeHeroHistory",
	72:  "E_QuestType_TavernCallHeroHistory",
	73:  "E_QuestType_GetIdleRewardHistory",
	74:  "E_QuestType_DecomposeHeroHistory",
	75:  "E_QuestType_BuyDecomposeShopHistory",
	76:  "E_QuestType_AdvancedMartialArtYardHistory",
	77:  "E_QuestType_ResetDaoistMagicHistory",
	78:  "E_QuestType_HeroPortraits",
	79:  "E_QuestType_MonthCardActivation",
	80:  "E_QuestType_TreasureHunt",
	81:  "E_QuestType_AllFactionTimes",
	82:  "E_QuestType_FirstWorldChat",
	83:  "E_QuestType_WearFiveStarEquipment",
	84:  "E_QuestType_EquipBreakTimes",
	85:  "E_QuestType_WorldBOSSRank",
	86:  "E_QuestType_HeroChallengeSingleHeroLayer",
	87:  "E_QuestType_HeroChallengeFiveHeroLayer",
	88:  "E_QuestType_GuildBossDamage",
	89:  "E_QuestType_WearPurpleWarcraft",
	90:  "E_QuestType_WearOrangeWarcraft",
	91:  "E_QuestType_WearRedWarcraft",
	92:  "E_QuestType_WearPlatinumWarcraft",
	93:  "E_QuestType_LegendArenaRank",
	94:  "E_QuestType_WarcraftSearchTimes",
	95:  "E_QuestType_AssignFactionTimes",
	96:  "E_QuestType_NewTavernTimes",
	97:  "E_QuestType_WorldBossTimes",
	98:  "E_QuestType_DungeonTimes",
	99:  "E_QuestType_LegendArenaTimes",
	100: "E_QuestType_HeroChallengeTimes",
}

var E_QuestType_value = map[string]int{
	"E_QuestType_JoinCampaign":                  1,
	"E_QuestType_HeroLevelUp":                   2,
	"E_QuestType_GetIdleReward":                 3,
	"E_QuestType_GetCompanionPoints":            4,
	"E_QuestType_EquipmentStrengthen":           5,
	"E_QuestType_GetFastIdle":                   6,
	"E_QuestType_TavernCallHero":                7,
	"E_QuestType_JoinTrialTower":                8,
	"E_QuestType_JoinGuildBoss":                 9,
	"E_QuestType_JoinArena":                     10,
	"E_QuestType_GetNoticeBoard":                11,
	"E_QuestType_BuyDaoistMagicShop":            12,
	"E_QuestType_BuyGuildShop":                  13,
	"E_QuestType_BuyCommonShop":                 14,
	"E_QuestType_WinCommonArena":                15,
	"E_QuestType_GetTeamNoticeBoard":            16,
	"E_QuestType_AdvancedMartialArtYardl":       17,
	"E_QuestType_HeroCompleteStage":             18,
	"E_QuestType_HeroMasterYardUpLevel":         19,
	"E_QuestType_MainHeroUpLevel":               20,
	"E_QuestType_RoleLevelUpLevel":              21,
	"E_QuestType_ClearanceTrialTowersFloor":     22,
	"E_QuestType_ArenaPoints":                   23,
	"E_QuestType_HeroMasterYardPlace":           24,
	"E_QuestType_IdleSilverCoin":                25,
	"E_QuestType_HeroNumber":                    26,
	"E_QuestType_PurpleHeroNumber":              27,
	"E_QuestType_JoinGuild":                     28,
	"E_QuestType_WinDaoistMagic1Floor":          29,
	"E_QuestType_WinDaoistMagic2Floor":          30,
	"E_QuestType_WinDaoistMagic3Floor":          31,
	"E_QuestType_WinFactionTowerFloor":          32,
	"E_QuestType_GetDailyActivityPoints":        33,
	"E_QuestType_ChapterClear":                  34,
	"E_QuestType_EquipmentStrengthenLevel":      35,
	"E_QuestType_WinDaoistMagicWiner":           36,
	"E_QuestType_DecomposeHero":                 37,
	"E_QuestType_BuyAnyShop":                    38,
	"E_QuestType_LoginDay":                      39,
	"E_QuestType_WearEquip":                     40,
	"E_QuestType_JoinDungeons":                  41,
	"E_QuestType_JoinRescue":                    42,
	"E_QuestType_JoinDaoistMagic":               43,
	"E_QuestType_TenCallHero":                   44,
	"E_QuestType_ComposeHero":                   45,
	"E_QuestType_ResetDaoistMagic":              46,
	"E_QuestType_BuyDecomposeShop":              47,
	"E_QuestType_RoleNameSuccess":               48,
	"E_QuestType_HavePurpleHero":                49,
	"E_QuestType_HavePurplePlusHero":            50,
	"E_QuestType_HaveOrangeHero":                51,
	"E_QuestType_HaveOrangePlusHero":            52,
	"E_QuestType_HaveRedHero":                   53,
	"E_QuestType_HaveRedPlusHero":               54,
	"E_QuestType_HavePlatinumHero":              55,
	"E_QuestType_WearGreenEquip":                56,
	"E_QuestType_WearBlueEquip":                 57,
	"E_QuestType_WearBluePlusEquip":             58,
	"E_QuestType_WearPurpleEquip":               59,
	"E_QuestType_WearPurplePlusEquip":           60,
	"E_QuestType_WearOrangeEquip":               61,
	"E_QuestType_WearOrangePlusEquip":           62,
	"E_QuestType_WearRedEquip":                  63,
	"E_QuestType_WearGrayEquip":                 64,
	"E_QuestType_ArtifactActivation":            65,
	"E_QuestType_HaveBlueHero":                  66,
	"E_QuestType_TotalLoginDays":                67,
	"E_QuestType_LifeCardActivation":            68,
	"E_QuestType_JoinArenaHistory":              69,
	"E_QuestType_GetFastIdleHistory":            70,
	"E_QuestType_ComposeHeroHistory":            71,
	"E_QuestType_TavernCallHeroHistory":         72,
	"E_QuestType_GetIdleRewardHistory":          73,
	"E_QuestType_DecomposeHeroHistory":          74,
	"E_QuestType_BuyDecomposeShopHistory":       75,
	"E_QuestType_AdvancedMartialArtYardHistory": 76,
	"E_QuestType_ResetDaoistMagicHistory":       77,
	"E_QuestType_HeroPortraits":                 78,
	"E_QuestType_MonthCardActivation":           79,
	"E_QuestType_TreasureHunt":                  80,
	"E_QuestType_AllFactionTimes":               81,
	"E_QuestType_FirstWorldChat":                82,
	"E_QuestType_WearFiveStarEquipment":         83,
	"E_QuestType_EquipBreakTimes":               84,
	"E_QuestType_WorldBOSSRank":                 85,
	"E_QuestType_HeroChallengeSingleHeroLayer":  86,
	"E_QuestType_HeroChallengeFiveHeroLayer":    87,
	"E_QuestType_GuildBossDamage":               88,
	"E_QuestType_WearPurpleWarcraft":            89,
	"E_QuestType_WearOrangeWarcraft":            90,
	"E_QuestType_WearRedWarcraft":               91,
	"E_QuestType_WearPlatinumWarcraft":          92,
	"E_QuestType_LegendArenaRank":               93,
	"E_QuestType_WarcraftSearchTimes":           94,
	"E_QuestType_AssignFactionTimes":            95,
	"E_QuestType_NewTavernTimes":                96,
	"E_QuestType_WorldBossTimes":                97,
	"E_QuestType_DungeonTimes":                  98,
	"E_QuestType_LegendArenaTimes":              99,
	"E_QuestType_HeroChallengeTimes":            100,
}

var E_QuestType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	14,
	15,
	16,
	17,
	18,
	19,
	20,
	21,
	22,
	23,
	24,
	25,
	26,
	27,
	28,
	29,
	30,
	31,
	32,
	33,
	34,
	35,
	36,
	37,
	38,
	39,
	40,
	41,
	42,
	43,
	44,
	45,
	46,
	47,
	48,
	49,
	50,
	51,
	52,
	53,
	54,
	55,
	56,
	57,
	58,
	59,
	60,
	61,
	62,
	63,
	64,
	65,
	66,
	67,
	68,
	69,
	70,
	71,
	72,
	73,
	74,
	75,
	76,
	77,
	78,
	79,
	80,
	81,
	82,
	83,
	84,
	85,
	86,
	87,
	88,
	89,
	90,
	91,
	92,
	93,
	94,
	95,
	96,
	97,
	98,
	99,
	100,
}

func (x E_QuestType) String() string {
	if name, ok := E_QuestType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_QuestType_Size() int {
	return len(E_QuestType_Slice)
}

func Check_E_QuestType_I(value int) bool {
	if _, ok := E_QuestType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_QuestType(value E_QuestType) bool {
	return Check_E_QuestType_I(int(value))
}

func Each_E_QuestType(f func(E_QuestType) (continued bool)) {
	for _, value := range E_QuestType_Slice {
		if !f(E_QuestType(value)) {
			break
		}
	}
}

func Each_E_QuestType_I(f func(int) (continued bool)) {
	for _, value := range E_QuestType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_QuestType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_LadderType] begin

// 排行榜类型枚举
type E_LadderType int

const (
	E_LadderType_Null E_LadderType = 0

	// 魏国积分榜
	E_LadderType_LadderWei E_LadderType = 1
	// 蜀国积分榜
	E_LadderType_LadderShu E_LadderType = 2
	// 吴国积分榜
	E_LadderType_LadderWu E_LadderType = 3
	// 群雄积分榜
	E_LadderType_LadderQun E_LadderType = 4
	// 关卡进度榜
	E_LadderType_LadderStage E_LadderType = 5
	// 试练塔进度榜
	E_LadderType_LadderTrialTower E_LadderType = 6
)

var E_LadderType_name = map[int]string{
	1: "E_LadderType_LadderWei",
	2: "E_LadderType_LadderShu",
	3: "E_LadderType_LadderWu",
	4: "E_LadderType_LadderQun",
	5: "E_LadderType_LadderStage",
	6: "E_LadderType_LadderTrialTower",
}

var E_LadderType_value = map[string]int{
	"E_LadderType_LadderWei":        1,
	"E_LadderType_LadderShu":        2,
	"E_LadderType_LadderWu":         3,
	"E_LadderType_LadderQun":        4,
	"E_LadderType_LadderStage":      5,
	"E_LadderType_LadderTrialTower": 6,
}

var E_LadderType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
}

func (x E_LadderType) String() string {
	if name, ok := E_LadderType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_LadderType_Size() int {
	return len(E_LadderType_Slice)
}

func Check_E_LadderType_I(value int) bool {
	if _, ok := E_LadderType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_LadderType(value E_LadderType) bool {
	return Check_E_LadderType_I(int(value))
}

func Each_E_LadderType(f func(E_LadderType) (continued bool)) {
	for _, value := range E_LadderType_Slice {
		if !f(E_LadderType(value)) {
			break
		}
	}
}

func Each_E_LadderType_I(f func(int) (continued bool)) {
	for _, value := range E_LadderType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_LadderType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_MissileType] begin

// 投掷物类型枚举
type E_MissileType int

const (
	E_MissileType_Null E_MissileType = 0

	// 直线型
	E_MissileType_Line E_MissileType = 1
	// 低抛型
	E_MissileType_LowParabola E_MissileType = 2
	// 高抛型
	E_MissileType_HighParabola E_MissileType = 3
	// 垂直型
	E_MissileType_Vertical E_MissileType = 4
	// 持续连线
	E_MissileType_LineSegment E_MissileType = 5
	// 贯穿
	E_MissileType_BreakThrough E_MissileType = 6
)

var E_MissileType_name = map[int]string{
	1: "E_MissileType_Line",
	2: "E_MissileType_LowParabola",
	3: "E_MissileType_HighParabola",
	4: "E_MissileType_Vertical",
	5: "E_MissileType_LineSegment",
	6: "E_MissileType_BreakThrough",
}

var E_MissileType_value = map[string]int{
	"E_MissileType_Line":         1,
	"E_MissileType_LowParabola":  2,
	"E_MissileType_HighParabola": 3,
	"E_MissileType_Vertical":     4,
	"E_MissileType_LineSegment":  5,
	"E_MissileType_BreakThrough": 6,
}

var E_MissileType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
}

func (x E_MissileType) String() string {
	if name, ok := E_MissileType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_MissileType_Size() int {
	return len(E_MissileType_Slice)
}

func Check_E_MissileType_I(value int) bool {
	if _, ok := E_MissileType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_MissileType(value E_MissileType) bool {
	return Check_E_MissileType_I(int(value))
}

func Each_E_MissileType(f func(E_MissileType) (continued bool)) {
	for _, value := range E_MissileType_Slice {
		if !f(E_MissileType(value)) {
			break
		}
	}
}

func Each_E_MissileType_I(f func(int) (continued bool)) {
	for _, value := range E_MissileType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_MissileType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_TrialTowerType] begin

// 试练塔类型枚举
type E_TrialTowerType int

const (
	E_TrialTowerType_Null E_TrialTowerType = 0

	// 普通试练塔
	E_TrialTowerType_TrialTowerNormal E_TrialTowerType = 1
	// 魏国试练塔
	E_TrialTowerType_TrialTowerWei E_TrialTowerType = 2
	// 蜀国试练塔
	E_TrialTowerType_TrialTowerShu E_TrialTowerType = 3
	// 吴国试练塔
	E_TrialTowerType_TrialTowerWu E_TrialTowerType = 4
	// 群雄试练塔
	E_TrialTowerType_TrialTowerQun E_TrialTowerType = 5
)

var E_TrialTowerType_name = map[int]string{
	1: "E_TrialTowerType_TrialTowerNormal",
	2: "E_TrialTowerType_TrialTowerWei",
	3: "E_TrialTowerType_TrialTowerShu",
	4: "E_TrialTowerType_TrialTowerWu",
	5: "E_TrialTowerType_TrialTowerQun",
}

var E_TrialTowerType_value = map[string]int{
	"E_TrialTowerType_TrialTowerNormal": 1,
	"E_TrialTowerType_TrialTowerWei":    2,
	"E_TrialTowerType_TrialTowerShu":    3,
	"E_TrialTowerType_TrialTowerWu":     4,
	"E_TrialTowerType_TrialTowerQun":    5,
}

var E_TrialTowerType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
}

func (x E_TrialTowerType) String() string {
	if name, ok := E_TrialTowerType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_TrialTowerType_Size() int {
	return len(E_TrialTowerType_Slice)
}

func Check_E_TrialTowerType_I(value int) bool {
	if _, ok := E_TrialTowerType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_TrialTowerType(value E_TrialTowerType) bool {
	return Check_E_TrialTowerType_I(int(value))
}

func Each_E_TrialTowerType(f func(E_TrialTowerType) (continued bool)) {
	for _, value := range E_TrialTowerType_Slice {
		if !f(E_TrialTowerType(value)) {
			break
		}
	}
}

func Each_E_TrialTowerType_I(f func(int) (continued bool)) {
	for _, value := range E_TrialTowerType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_TrialTowerType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_PlaySex] begin

// 角色性别
type E_PlaySex int

const (
	E_PlaySex_Null E_PlaySex = 0

	// 男
	E_PlaySex_Male E_PlaySex = 1
	// 女
	E_PlaySex_Female E_PlaySex = 2
)

var E_PlaySex_name = map[int]string{
	1: "E_PlaySex_Male",
	2: "E_PlaySex_Female",
}

var E_PlaySex_value = map[string]int{
	"E_PlaySex_Male":   1,
	"E_PlaySex_Female": 2,
}

var E_PlaySex_Slice = []int{
	1,
	2,
}

func (x E_PlaySex) String() string {
	if name, ok := E_PlaySex_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_PlaySex_Size() int {
	return len(E_PlaySex_Slice)
}

func Check_E_PlaySex_I(value int) bool {
	if _, ok := E_PlaySex_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_PlaySex(value E_PlaySex) bool {
	return Check_E_PlaySex_I(int(value))
}

func Each_E_PlaySex(f func(E_PlaySex) (continued bool)) {
	for _, value := range E_PlaySex_Slice {
		if !f(E_PlaySex(value)) {
			break
		}
	}
}

func Each_E_PlaySex_I(f func(int) (continued bool)) {
	for _, value := range E_PlaySex_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_PlaySex] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_Week] begin

// 星期
type E_Week int

const (
	E_Week_Null E_Week = 0

	// 星期一
	E_Week_Monday E_Week = 1
	// 星期二
	E_Week_Tuesday E_Week = 2
	// 星期三
	E_Week_Wednesday E_Week = 3
	// 星期四
	E_Week_Thursday E_Week = 4
	// 星期五
	E_Week_Friday E_Week = 5
	// 星期六
	E_Week_Saturday E_Week = 6
	// 星期日
	E_Week_Sunday E_Week = 7
)

var E_Week_name = map[int]string{
	1: "E_Week_Monday",
	2: "E_Week_Tuesday",
	3: "E_Week_Wednesday",
	4: "E_Week_Thursday",
	5: "E_Week_Friday",
	6: "E_Week_Saturday",
	7: "E_Week_Sunday",
}

var E_Week_value = map[string]int{
	"E_Week_Monday":    1,
	"E_Week_Tuesday":   2,
	"E_Week_Wednesday": 3,
	"E_Week_Thursday":  4,
	"E_Week_Friday":    5,
	"E_Week_Saturday":  6,
	"E_Week_Sunday":    7,
}

var E_Week_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
}

func (x E_Week) String() string {
	if name, ok := E_Week_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_Week_Size() int {
	return len(E_Week_Slice)
}

func Check_E_Week_I(value int) bool {
	if _, ok := E_Week_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_Week(value E_Week) bool {
	return Check_E_Week_I(int(value))
}

func Each_E_Week(f func(E_Week) (continued bool)) {
	for _, value := range E_Week_Slice {
		if !f(E_Week(value)) {
			break
		}
	}
}

func Each_E_Week_I(f func(int) (continued bool)) {
	for _, value := range E_Week_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_Week] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ControlType] begin

// 战斗状态
type E_ControlType int

const (
	E_ControlType_Null E_ControlType = 0

	// 无敌(免疫所有)
	E_ControlType_NonAttack1 E_ControlType = 1
	// 无敌(可被治疗)
	E_ControlType_NonAttack2 E_ControlType = 2
	// 霸体(免疫控制)
	E_ControlType_SuperArmor E_ControlType = 4
	// 冰冻(僵直无法攻击)
	E_ControlType_Freeze E_ControlType = 8
	// 晕眩/睡眠/缠绕(僵直无法攻击)
	E_ControlType_Dizzy E_ControlType = 16
	// 放逐(无法攻击和被攻击,可控制改变状态)
	E_ControlType_Exile E_ControlType = 32
	// 沉默(可普攻)
	E_ControlType_OnlyAttack E_ControlType = 64
	// 魅惑
	E_ControlType_Bewitch E_ControlType = 128
	// 嘲讽
	E_ControlType_Taunt E_ControlType = 256
)

var E_ControlType_name = map[int]string{
	1:   "E_ControlType_NonAttack1",
	2:   "E_ControlType_NonAttack2",
	4:   "E_ControlType_SuperArmor",
	8:   "E_ControlType_Freeze",
	16:  "E_ControlType_Dizzy",
	32:  "E_ControlType_Exile",
	64:  "E_ControlType_OnlyAttack",
	128: "E_ControlType_Bewitch",
	256: "E_ControlType_Taunt",
}

var E_ControlType_value = map[string]int{
	"E_ControlType_NonAttack1": 1,
	"E_ControlType_NonAttack2": 2,
	"E_ControlType_SuperArmor": 4,
	"E_ControlType_Freeze":     8,
	"E_ControlType_Dizzy":      16,
	"E_ControlType_Exile":      32,
	"E_ControlType_OnlyAttack": 64,
	"E_ControlType_Bewitch":    128,
	"E_ControlType_Taunt":      256,
}

var E_ControlType_Slice = []int{
	1,
	2,
	4,
	8,
	16,
	32,
	64,
	128,
	256,
}

func (x E_ControlType) String() string {
	if name, ok := E_ControlType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ControlType_Size() int {
	return len(E_ControlType_Slice)
}

func Check_E_ControlType_I(value int) bool {
	if _, ok := E_ControlType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ControlType(value E_ControlType) bool {
	return Check_E_ControlType_I(int(value))
}

func Each_E_ControlType(f func(E_ControlType) (continued bool)) {
	for _, value := range E_ControlType_Slice {
		if !f(E_ControlType(value)) {
			break
		}
	}
}

func Each_E_ControlType_I(f func(int) (continued bool)) {
	for _, value := range E_ControlType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ControlType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_TeamPosition] begin

// 阵容站位编号
type E_TeamPosition int

const (
	E_TeamPosition_Null E_TeamPosition = 0

	// 站位1
	E_TeamPosition_TeamPosition1 E_TeamPosition = 1
	// 站位2
	E_TeamPosition_TeamPosition2 E_TeamPosition = 2
	// 站位3
	E_TeamPosition_TeamPosition3 E_TeamPosition = 3
	// 站位4
	E_TeamPosition_TeamPosition4 E_TeamPosition = 4
	// 站位5
	E_TeamPosition_TeamPosition5 E_TeamPosition = 5
)

var E_TeamPosition_name = map[int]string{
	1: "E_TeamPosition_TeamPosition1",
	2: "E_TeamPosition_TeamPosition2",
	3: "E_TeamPosition_TeamPosition3",
	4: "E_TeamPosition_TeamPosition4",
	5: "E_TeamPosition_TeamPosition5",
}

var E_TeamPosition_value = map[string]int{
	"E_TeamPosition_TeamPosition1": 1,
	"E_TeamPosition_TeamPosition2": 2,
	"E_TeamPosition_TeamPosition3": 3,
	"E_TeamPosition_TeamPosition4": 4,
	"E_TeamPosition_TeamPosition5": 5,
}

var E_TeamPosition_Slice = []int{
	1,
	2,
	3,
	4,
	5,
}

func (x E_TeamPosition) String() string {
	if name, ok := E_TeamPosition_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_TeamPosition_Size() int {
	return len(E_TeamPosition_Slice)
}

func Check_E_TeamPosition_I(value int) bool {
	if _, ok := E_TeamPosition_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_TeamPosition(value E_TeamPosition) bool {
	return Check_E_TeamPosition_I(int(value))
}

func Each_E_TeamPosition(f func(E_TeamPosition) (continued bool)) {
	for _, value := range E_TeamPosition_Slice {
		if !f(E_TeamPosition(value)) {
			break
		}
	}
}

func Each_E_TeamPosition_I(f func(int) (continued bool)) {
	for _, value := range E_TeamPosition_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_TeamPosition] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ActivityType] begin

// 活动类型编号
type E_ActivityType int

const (
	E_ActivityType_Null E_ActivityType = 0

	// 新兵特训
	E_ActivityType_NewRole E_ActivityType = 1
	// 登入几天
	E_ActivityType_LoginDay E_ActivityType = 2
	// 投资计划
	E_ActivityType_Investment E_ActivityType = 3
	// 幸运宝箱（充值）
	E_ActivityType_LuckyBoxCharge E_ActivityType = 4
	// 幸运宝箱（元宝）
	E_ActivityType_LuckyBoxGoldCoin E_ActivityType = 5
	// 首充
	E_ActivityType_FirstCharge E_ActivityType = 6
	// 首充多倍
	E_ActivityType_Firstchargemultiple E_ActivityType = 7
	// 首次累充
	E_ActivityType_Addupfirstcharge E_ActivityType = 8
	// 单笔充值
	E_ActivityType_Singlecharge E_ActivityType = 9
	// 累计充值
	E_ActivityType_Addupcharge E_ActivityType = 10
	// 连续充值
	E_ActivityType_Continuouscharge E_ActivityType = 11
	// 一折礼包
	E_ActivityType_Discountgiftpack E_ActivityType = 12
	// 折扣商店
	E_ActivityType_Discountshop E_ActivityType = 13
	// 限时礼包
	E_ActivityType_Timelimitedgiftpack E_ActivityType = 14
	// 月卡
	E_ActivityType_MonthCard E_ActivityType = 15
	// 兑换码
	E_ActivityType_RedeemCode E_ActivityType = 16
	// 更新公告
	E_ActivityType_UpdateNotice E_ActivityType = 17
	// 开服排行榜
	E_ActivityType_OpenRank E_ActivityType = 21
	// 月基金小
	E_ActivityType_MonthFundSmall E_ActivityType = 22
	// 月基金大
	E_ActivityType_MonthFundBig E_ActivityType = 23
	// 任务活动
	E_ActivityType_QuestActivity E_ActivityType = 24
	// 貂蝉降临
	E_ActivityType_HeroAdvent E_ActivityType = 25
	// 周末福利
	E_ActivityType_WeekendBenefits E_ActivityType = 26
	// 我的小程序
	E_ActivityType_MiniProgram E_ActivityType = 27
	// 封赏令
	E_ActivityType_AssaultToken E_ActivityType = 28
	// 新将招募
	E_ActivityType_NewTavern E_ActivityType = 29
	// 限时招募
	E_ActivityType_LimitTavern E_ActivityType = 30
	// 兵书搜索
	E_ActivityType_WarcraftSearch E_ActivityType = 31
	// 每日签到
	E_ActivityType_DailyLoginReward E_ActivityType = 32
	// 开服限时礼包
	E_ActivityType_OpenServiceGiftPack E_ActivityType = 33
	// 每日首充
	E_ActivityType_DayFirstCharge E_ActivityType = 34
	// 兑换活动
	E_ActivityType_Exchange E_ActivityType = 35
	// 累计消费
	E_ActivityType_AddupConsume E_ActivityType = 36
	// 限时登录
	E_ActivityType_LimitLogin E_ActivityType = 37
	// 首次分享
	E_ActivityType_FirstShare E_ActivityType = 38
	// 终身卡
	E_ActivityType_LifeCard E_ActivityType = 39
	// 七日目标
	E_ActivityType_SevenTarget E_ActivityType = 40
	// 十四日目标
	E_ActivityType_FourteenTarget E_ActivityType = 41
	// 创角登录
	E_ActivityType_NewRoleLogin E_ActivityType = 42
	// 日常折扣
	E_ActivityType_DailyDiscount E_ActivityType = 43
)

var E_ActivityType_name = map[int]string{
	1:  "E_ActivityType_NewRole",
	2:  "E_ActivityType_LoginDay",
	3:  "E_ActivityType_Investment",
	4:  "E_ActivityType_LuckyBoxCharge",
	5:  "E_ActivityType_LuckyBoxGoldCoin",
	6:  "E_ActivityType_FirstCharge",
	7:  "E_ActivityType_Firstchargemultiple",
	8:  "E_ActivityType_Addupfirstcharge",
	9:  "E_ActivityType_Singlecharge",
	10: "E_ActivityType_Addupcharge",
	11: "E_ActivityType_Continuouscharge",
	12: "E_ActivityType_Discountgiftpack",
	13: "E_ActivityType_Discountshop",
	14: "E_ActivityType_Timelimitedgiftpack",
	15: "E_ActivityType_MonthCard",
	16: "E_ActivityType_RedeemCode",
	17: "E_ActivityType_UpdateNotice",
	21: "E_ActivityType_OpenRank",
	22: "E_ActivityType_MonthFundSmall",
	23: "E_ActivityType_MonthFundBig",
	24: "E_ActivityType_QuestActivity",
	25: "E_ActivityType_HeroAdvent",
	26: "E_ActivityType_WeekendBenefits",
	27: "E_ActivityType_MiniProgram",
	28: "E_ActivityType_AssaultToken",
	29: "E_ActivityType_NewTavern",
	30: "E_ActivityType_LimitTavern",
	31: "E_ActivityType_WarcraftSearch",
	32: "E_ActivityType_DailyLoginReward",
	33: "E_ActivityType_OpenServiceGiftPack",
	34: "E_ActivityType_DayFirstCharge",
	35: "E_ActivityType_Exchange",
	36: "E_ActivityType_AddupConsume",
	37: "E_ActivityType_LimitLogin",
	38: "E_ActivityType_FirstShare",
	39: "E_ActivityType_LifeCard",
	40: "E_ActivityType_SevenTarget",
	41: "E_ActivityType_FourteenTarget",
	42: "E_ActivityType_NewRoleLogin",
	43: "E_ActivityType_DailyDiscount",
}

var E_ActivityType_value = map[string]int{
	"E_ActivityType_NewRole":             1,
	"E_ActivityType_LoginDay":            2,
	"E_ActivityType_Investment":          3,
	"E_ActivityType_LuckyBoxCharge":      4,
	"E_ActivityType_LuckyBoxGoldCoin":    5,
	"E_ActivityType_FirstCharge":         6,
	"E_ActivityType_Firstchargemultiple": 7,
	"E_ActivityType_Addupfirstcharge":    8,
	"E_ActivityType_Singlecharge":        9,
	"E_ActivityType_Addupcharge":         10,
	"E_ActivityType_Continuouscharge":    11,
	"E_ActivityType_Discountgiftpack":    12,
	"E_ActivityType_Discountshop":        13,
	"E_ActivityType_Timelimitedgiftpack": 14,
	"E_ActivityType_MonthCard":           15,
	"E_ActivityType_RedeemCode":          16,
	"E_ActivityType_UpdateNotice":        17,
	"E_ActivityType_OpenRank":            21,
	"E_ActivityType_MonthFundSmall":      22,
	"E_ActivityType_MonthFundBig":        23,
	"E_ActivityType_QuestActivity":       24,
	"E_ActivityType_HeroAdvent":          25,
	"E_ActivityType_WeekendBenefits":     26,
	"E_ActivityType_MiniProgram":         27,
	"E_ActivityType_AssaultToken":        28,
	"E_ActivityType_NewTavern":           29,
	"E_ActivityType_LimitTavern":         30,
	"E_ActivityType_WarcraftSearch":      31,
	"E_ActivityType_DailyLoginReward":    32,
	"E_ActivityType_OpenServiceGiftPack": 33,
	"E_ActivityType_DayFirstCharge":      34,
	"E_ActivityType_Exchange":            35,
	"E_ActivityType_AddupConsume":        36,
	"E_ActivityType_LimitLogin":          37,
	"E_ActivityType_FirstShare":          38,
	"E_ActivityType_LifeCard":            39,
	"E_ActivityType_SevenTarget":         40,
	"E_ActivityType_FourteenTarget":      41,
	"E_ActivityType_NewRoleLogin":        42,
	"E_ActivityType_DailyDiscount":       43,
}

var E_ActivityType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	14,
	15,
	16,
	17,
	21,
	22,
	23,
	24,
	25,
	26,
	27,
	28,
	29,
	30,
	31,
	32,
	33,
	34,
	35,
	36,
	37,
	38,
	39,
	40,
	41,
	42,
	43,
}

func (x E_ActivityType) String() string {
	if name, ok := E_ActivityType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ActivityType_Size() int {
	return len(E_ActivityType_Slice)
}

func Check_E_ActivityType_I(value int) bool {
	if _, ok := E_ActivityType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ActivityType(value E_ActivityType) bool {
	return Check_E_ActivityType_I(int(value))
}

func Each_E_ActivityType(f func(E_ActivityType) (continued bool)) {
	for _, value := range E_ActivityType_Slice {
		if !f(E_ActivityType(value)) {
			break
		}
	}
}

func Each_E_ActivityType_I(f func(int) (continued bool)) {
	for _, value := range E_ActivityType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ActivityType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_EntranceType] begin

// 活动入口编号
type E_EntranceType int

const (
	E_EntranceType_Null E_EntranceType = 0

	// 充值入口
	E_EntranceType_ChargeEntrance E_EntranceType = 1
	// 福利入口
	E_EntranceType_BenefitsEntrance E_EntranceType = 2
	// 活动入口
	E_EntranceType_ActivityEntrance E_EntranceType = 3
	// 首充入口
	E_EntranceType_FirstChargeEntrance E_EntranceType = 4
	// 新兵特训入口
	E_EntranceType_NewRoleEntrance E_EntranceType = 5
	// 貂蝉降临入口
	E_EntranceType_HeroAdventEntrance E_EntranceType = 6
	// 节日活动入口
	E_EntranceType_FestivalEntrance E_EntranceType = 7
	// 封赏令活动入口
	E_EntranceType_AssaultTokenEntrance E_EntranceType = 8
	// 送元宝入口
	E_EntranceType_GoldWeChatMission E_EntranceType = 9
	// 开服限时礼包入口
	E_EntranceType_OpenServiceGiftPack E_EntranceType = 10
	// 开服累计充值入口
	E_EntranceType_OpenServiceAddupCharge E_EntranceType = 11
	// 寻宝礼包
	E_EntranceType_TreasureHuntGift E_EntranceType = 12
)

var E_EntranceType_name = map[int]string{
	1:  "E_EntranceType_ChargeEntrance",
	2:  "E_EntranceType_BenefitsEntrance",
	3:  "E_EntranceType_ActivityEntrance",
	4:  "E_EntranceType_FirstChargeEntrance",
	5:  "E_EntranceType_NewRoleEntrance",
	6:  "E_EntranceType_HeroAdventEntrance",
	7:  "E_EntranceType_FestivalEntrance",
	8:  "E_EntranceType_AssaultTokenEntrance",
	9:  "E_EntranceType_GoldWeChatMission",
	10: "E_EntranceType_OpenServiceGiftPack",
	11: "E_EntranceType_OpenServiceAddupCharge",
	12: "E_EntranceType_TreasureHuntGift",
}

var E_EntranceType_value = map[string]int{
	"E_EntranceType_ChargeEntrance":         1,
	"E_EntranceType_BenefitsEntrance":       2,
	"E_EntranceType_ActivityEntrance":       3,
	"E_EntranceType_FirstChargeEntrance":    4,
	"E_EntranceType_NewRoleEntrance":        5,
	"E_EntranceType_HeroAdventEntrance":     6,
	"E_EntranceType_FestivalEntrance":       7,
	"E_EntranceType_AssaultTokenEntrance":   8,
	"E_EntranceType_GoldWeChatMission":      9,
	"E_EntranceType_OpenServiceGiftPack":    10,
	"E_EntranceType_OpenServiceAddupCharge": 11,
	"E_EntranceType_TreasureHuntGift":       12,
}

var E_EntranceType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
}

func (x E_EntranceType) String() string {
	if name, ok := E_EntranceType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_EntranceType_Size() int {
	return len(E_EntranceType_Slice)
}

func Check_E_EntranceType_I(value int) bool {
	if _, ok := E_EntranceType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_EntranceType(value E_EntranceType) bool {
	return Check_E_EntranceType_I(int(value))
}

func Each_E_EntranceType(f func(E_EntranceType) (continued bool)) {
	for _, value := range E_EntranceType_Slice {
		if !f(E_EntranceType(value)) {
			break
		}
	}
}

func Each_E_EntranceType_I(f func(int) (continued bool)) {
	for _, value := range E_EntranceType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_EntranceType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_TimeType] begin

// 活动时间类型
type E_TimeType int

const (
	E_TimeType_Null E_TimeType = 0

	// 创角多少天
	E_TimeType_Create E_TimeType = 1
	// 开服多少天
	E_TimeType_OpenServer E_TimeType = 2
	// 定时多少天
	E_TimeType_Date E_TimeType = 3
	// 合服多少天
	E_TimeType_MergeServer E_TimeType = 4
	// 每周第几天
	E_TimeType_Weekly E_TimeType = 5
)

var E_TimeType_name = map[int]string{
	1: "E_TimeType_Create",
	2: "E_TimeType_OpenServer",
	3: "E_TimeType_Date",
	4: "E_TimeType_MergeServer",
	5: "E_TimeType_Weekly",
}

var E_TimeType_value = map[string]int{
	"E_TimeType_Create":      1,
	"E_TimeType_OpenServer":  2,
	"E_TimeType_Date":        3,
	"E_TimeType_MergeServer": 4,
	"E_TimeType_Weekly":      5,
}

var E_TimeType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
}

func (x E_TimeType) String() string {
	if name, ok := E_TimeType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_TimeType_Size() int {
	return len(E_TimeType_Slice)
}

func Check_E_TimeType_I(value int) bool {
	if _, ok := E_TimeType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_TimeType(value E_TimeType) bool {
	return Check_E_TimeType_I(int(value))
}

func Each_E_TimeType(f func(E_TimeType) (continued bool)) {
	for _, value := range E_TimeType_Slice {
		if !f(E_TimeType(value)) {
			break
		}
	}
}

func Each_E_TimeType_I(f func(int) (continued bool)) {
	for _, value := range E_TimeType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_TimeType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ActivityRefreshType] begin

// 活动刷新类型
type E_ActivityRefreshType int

const (
	E_ActivityRefreshType_Null E_ActivityRefreshType = 0

	// 限购
	E_ActivityRefreshType_ActivityRefreshNever E_ActivityRefreshType = 1
	// 每日限购
	E_ActivityRefreshType_ActivityRefreshDay E_ActivityRefreshType = 2
	// 每周限购
	E_ActivityRefreshType_ActivityRefreshWeek E_ActivityRefreshType = 3
	// 每月限购
	E_ActivityRefreshType_ActivityRefreshMonth E_ActivityRefreshType = 4
	// 奖励全部被领取后重置
	E_ActivityRefreshType_ActivityRefreshFinish E_ActivityRefreshType = 5
)

var E_ActivityRefreshType_name = map[int]string{
	1: "E_ActivityRefreshType_ActivityRefreshNever",
	2: "E_ActivityRefreshType_ActivityRefreshDay",
	3: "E_ActivityRefreshType_ActivityRefreshWeek",
	4: "E_ActivityRefreshType_ActivityRefreshMonth",
	5: "E_ActivityRefreshType_ActivityRefreshFinish",
}

var E_ActivityRefreshType_value = map[string]int{
	"E_ActivityRefreshType_ActivityRefreshNever":  1,
	"E_ActivityRefreshType_ActivityRefreshDay":    2,
	"E_ActivityRefreshType_ActivityRefreshWeek":   3,
	"E_ActivityRefreshType_ActivityRefreshMonth":  4,
	"E_ActivityRefreshType_ActivityRefreshFinish": 5,
}

var E_ActivityRefreshType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
}

func (x E_ActivityRefreshType) String() string {
	if name, ok := E_ActivityRefreshType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ActivityRefreshType_Size() int {
	return len(E_ActivityRefreshType_Slice)
}

func Check_E_ActivityRefreshType_I(value int) bool {
	if _, ok := E_ActivityRefreshType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ActivityRefreshType(value E_ActivityRefreshType) bool {
	return Check_E_ActivityRefreshType_I(int(value))
}

func Each_E_ActivityRefreshType(f func(E_ActivityRefreshType) (continued bool)) {
	for _, value := range E_ActivityRefreshType_Slice {
		if !f(E_ActivityRefreshType(value)) {
			break
		}
	}
}

func Each_E_ActivityRefreshType_I(f func(int) (continued bool)) {
	for _, value := range E_ActivityRefreshType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ActivityRefreshType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ActivityRankType] begin

// 活动排行榜类型
type E_ActivityRankType int

const (
	E_ActivityRankType_Null E_ActivityRankType = 0

	// 抽将
	E_ActivityRankType_EmployHero E_ActivityRankType = 1
	// 寻宝
	E_ActivityRankType_LuckBox E_ActivityRankType = 2
	// 推图
	E_ActivityRankType_Stage E_ActivityRankType = 3
	// 武将升级
	E_ActivityRankType_HeroLevelUp E_ActivityRankType = 4
	// 试练塔
	E_ActivityRankType_TrialTower E_ActivityRankType = 5
	// 武将进阶
	E_ActivityRankType_HeroEvolution E_ActivityRankType = 6
	// 装备强化
	E_ActivityRankType_EquipStrengthen E_ActivityRankType = 7
	// 公会boss伤害
	E_ActivityRankType_Hunt E_ActivityRankType = 8
	// 战力
	E_ActivityRankType_Power E_ActivityRankType = 9
	// 消耗元宝数
	E_ActivityRankType_CostGoldCoin E_ActivityRankType = 10
)

var E_ActivityRankType_name = map[int]string{
	1:  "E_ActivityRankType_EmployHero",
	2:  "E_ActivityRankType_LuckBox",
	3:  "E_ActivityRankType_Stage",
	4:  "E_ActivityRankType_HeroLevelUp",
	5:  "E_ActivityRankType_TrialTower",
	6:  "E_ActivityRankType_HeroEvolution",
	7:  "E_ActivityRankType_EquipStrengthen",
	8:  "E_ActivityRankType_Hunt",
	9:  "E_ActivityRankType_Power",
	10: "E_ActivityRankType_CostGoldCoin",
}

var E_ActivityRankType_value = map[string]int{
	"E_ActivityRankType_EmployHero":      1,
	"E_ActivityRankType_LuckBox":         2,
	"E_ActivityRankType_Stage":           3,
	"E_ActivityRankType_HeroLevelUp":     4,
	"E_ActivityRankType_TrialTower":      5,
	"E_ActivityRankType_HeroEvolution":   6,
	"E_ActivityRankType_EquipStrengthen": 7,
	"E_ActivityRankType_Hunt":            8,
	"E_ActivityRankType_Power":           9,
	"E_ActivityRankType_CostGoldCoin":    10,
}

var E_ActivityRankType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
}

func (x E_ActivityRankType) String() string {
	if name, ok := E_ActivityRankType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ActivityRankType_Size() int {
	return len(E_ActivityRankType_Slice)
}

func Check_E_ActivityRankType_I(value int) bool {
	if _, ok := E_ActivityRankType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ActivityRankType(value E_ActivityRankType) bool {
	return Check_E_ActivityRankType_I(int(value))
}

func Each_E_ActivityRankType(f func(E_ActivityRankType) (continued bool)) {
	for _, value := range E_ActivityRankType_Slice {
		if !f(E_ActivityRankType(value)) {
			break
		}
	}
}

func Each_E_ActivityRankType_I(f func(int) (continued bool)) {
	for _, value := range E_ActivityRankType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ActivityRankType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_TimeLimitedGiftPackType] begin

// 限时礼包类型
type E_TimeLimitedGiftPackType int

const (
	E_TimeLimitedGiftPackType_Null E_TimeLimitedGiftPackType = 0

	// 关卡限时礼包
	E_TimeLimitedGiftPackType_StageGiftPack E_TimeLimitedGiftPackType = 1
	// 首充限时礼包
	E_TimeLimitedGiftPackType_FirstChargeGiftpack E_TimeLimitedGiftPackType = 2
	// 武将稀有度限时礼包
	E_TimeLimitedGiftPackType_RarityGiftPack E_TimeLimitedGiftPackType = 3
	// 试练塔限时礼包
	E_TimeLimitedGiftPackType_TrialTowerGiftPack E_TimeLimitedGiftPackType = 4
	// 主公等级限时礼包
	E_TimeLimitedGiftPackType_RoleLevelGiftPack E_TimeLimitedGiftPackType = 5
	// 闯关限时礼包
	E_TimeLimitedGiftPackType_StageJammedGiftPack E_TimeLimitedGiftPackType = 6
	// 武将升级限时礼包1
	E_TimeLimitedGiftPackType_HeroLevelUpGiftPack1 E_TimeLimitedGiftPackType = 7
	// 武将升级限时礼包2
	E_TimeLimitedGiftPackType_HeroLevelUpGiftPack2 E_TimeLimitedGiftPackType = 8
	// 武将进阶限时礼包
	E_TimeLimitedGiftPackType_HeroAdvanceGiftPack E_TimeLimitedGiftPackType = 9
)

var E_TimeLimitedGiftPackType_name = map[int]string{
	1: "E_TimeLimitedGiftPackType_StageGiftPack",
	2: "E_TimeLimitedGiftPackType_FirstChargeGiftpack",
	3: "E_TimeLimitedGiftPackType_RarityGiftPack",
	4: "E_TimeLimitedGiftPackType_TrialTowerGiftPack",
	5: "E_TimeLimitedGiftPackType_RoleLevelGiftPack",
	6: "E_TimeLimitedGiftPackType_StageJammedGiftPack",
	7: "E_TimeLimitedGiftPackType_HeroLevelUpGiftPack1",
	8: "E_TimeLimitedGiftPackType_HeroLevelUpGiftPack2",
	9: "E_TimeLimitedGiftPackType_HeroAdvanceGiftPack",
}

var E_TimeLimitedGiftPackType_value = map[string]int{
	"E_TimeLimitedGiftPackType_StageGiftPack":        1,
	"E_TimeLimitedGiftPackType_FirstChargeGiftpack":  2,
	"E_TimeLimitedGiftPackType_RarityGiftPack":       3,
	"E_TimeLimitedGiftPackType_TrialTowerGiftPack":   4,
	"E_TimeLimitedGiftPackType_RoleLevelGiftPack":    5,
	"E_TimeLimitedGiftPackType_StageJammedGiftPack":  6,
	"E_TimeLimitedGiftPackType_HeroLevelUpGiftPack1": 7,
	"E_TimeLimitedGiftPackType_HeroLevelUpGiftPack2": 8,
	"E_TimeLimitedGiftPackType_HeroAdvanceGiftPack":  9,
}

var E_TimeLimitedGiftPackType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
}

func (x E_TimeLimitedGiftPackType) String() string {
	if name, ok := E_TimeLimitedGiftPackType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_TimeLimitedGiftPackType_Size() int {
	return len(E_TimeLimitedGiftPackType_Slice)
}

func Check_E_TimeLimitedGiftPackType_I(value int) bool {
	if _, ok := E_TimeLimitedGiftPackType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_TimeLimitedGiftPackType(value E_TimeLimitedGiftPackType) bool {
	return Check_E_TimeLimitedGiftPackType_I(int(value))
}

func Each_E_TimeLimitedGiftPackType(f func(E_TimeLimitedGiftPackType) (continued bool)) {
	for _, value := range E_TimeLimitedGiftPackType_Slice {
		if !f(E_TimeLimitedGiftPackType(value)) {
			break
		}
	}
}

func Each_E_TimeLimitedGiftPackType_I(f func(int) (continued bool)) {
	for _, value := range E_TimeLimitedGiftPackType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_TimeLimitedGiftPackType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_CountTimeType] begin

// 统计任务数值的时间类型
type E_CountTimeType int

const (
	E_CountTimeType_Null E_CountTimeType = 0

	// 历史值
	E_CountTimeType_HistoryTimeValue E_CountTimeType = 1
	// 实时值
	E_CountTimeType_RealTimeValue E_CountTimeType = 2
)

var E_CountTimeType_name = map[int]string{
	1: "E_CountTimeType_HistoryTimeValue",
	2: "E_CountTimeType_RealTimeValue",
}

var E_CountTimeType_value = map[string]int{
	"E_CountTimeType_HistoryTimeValue": 1,
	"E_CountTimeType_RealTimeValue":    2,
}

var E_CountTimeType_Slice = []int{
	1,
	2,
}

func (x E_CountTimeType) String() string {
	if name, ok := E_CountTimeType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_CountTimeType_Size() int {
	return len(E_CountTimeType_Slice)
}

func Check_E_CountTimeType_I(value int) bool {
	if _, ok := E_CountTimeType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_CountTimeType(value E_CountTimeType) bool {
	return Check_E_CountTimeType_I(int(value))
}

func Each_E_CountTimeType(f func(E_CountTimeType) (continued bool)) {
	for _, value := range E_CountTimeType_Slice {
		if !f(E_CountTimeType(value)) {
			break
		}
	}
}

func Each_E_CountTimeType_I(f func(int) (continued bool)) {
	for _, value := range E_CountTimeType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_CountTimeType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_CustomRewardType] begin

// 定制限时礼包奖励类型
type E_CustomRewardType int

const (
	E_CustomRewardType_Null E_CustomRewardType = 0

	// 装备奖励
	E_CustomRewardType_Equip E_CustomRewardType = 1
	// 资源奖励
	E_CustomRewardType_Resource E_CustomRewardType = 2
	// 升阶石奖励
	E_CustomRewardType_EquipAdvance E_CustomRewardType = 3
	// 武将进阶奖励
	E_CustomRewardType_HeroAdvance E_CustomRewardType = 4
	// 银币和经验奖励
	E_CustomRewardType_SilverCoinAndHeroExp E_CustomRewardType = 5
	// 突破丹奖励
	E_CustomRewardType_LimitPassPillReward E_CustomRewardType = 6
)

var E_CustomRewardType_name = map[int]string{
	1: "E_CustomRewardType_Equip",
	2: "E_CustomRewardType_Resource",
	3: "E_CustomRewardType_EquipAdvance",
	4: "E_CustomRewardType_HeroAdvance",
	5: "E_CustomRewardType_SilverCoinAndHeroExp",
	6: "E_CustomRewardType_LimitPassPillReward",
}

var E_CustomRewardType_value = map[string]int{
	"E_CustomRewardType_Equip":                1,
	"E_CustomRewardType_Resource":             2,
	"E_CustomRewardType_EquipAdvance":         3,
	"E_CustomRewardType_HeroAdvance":          4,
	"E_CustomRewardType_SilverCoinAndHeroExp": 5,
	"E_CustomRewardType_LimitPassPillReward":  6,
}

var E_CustomRewardType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
}

func (x E_CustomRewardType) String() string {
	if name, ok := E_CustomRewardType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_CustomRewardType_Size() int {
	return len(E_CustomRewardType_Slice)
}

func Check_E_CustomRewardType_I(value int) bool {
	if _, ok := E_CustomRewardType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_CustomRewardType(value E_CustomRewardType) bool {
	return Check_E_CustomRewardType_I(int(value))
}

func Each_E_CustomRewardType(f func(E_CustomRewardType) (continued bool)) {
	for _, value := range E_CustomRewardType_Slice {
		if !f(E_CustomRewardType(value)) {
			break
		}
	}
}

func Each_E_CustomRewardType_I(f func(int) (continued bool)) {
	for _, value := range E_CustomRewardType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_CustomRewardType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_CustomRewardResourceType] begin

// 定制限时礼包检测资源类型
type E_CustomRewardResourceType int

const (
	E_CustomRewardResourceType_Null E_CustomRewardResourceType = 0

	// 银币
	E_CustomRewardResourceType_SilverCoin E_CustomRewardResourceType = 1
	// 武将经验
	E_CustomRewardResourceType_HeroExp E_CustomRewardResourceType = 2
	// 突破丹
	E_CustomRewardResourceType_LimitPassPill E_CustomRewardResourceType = 3
)

var E_CustomRewardResourceType_name = map[int]string{
	1: "E_CustomRewardResourceType_SilverCoin",
	2: "E_CustomRewardResourceType_HeroExp",
	3: "E_CustomRewardResourceType_LimitPassPill",
}

var E_CustomRewardResourceType_value = map[string]int{
	"E_CustomRewardResourceType_SilverCoin":    1,
	"E_CustomRewardResourceType_HeroExp":       2,
	"E_CustomRewardResourceType_LimitPassPill": 3,
}

var E_CustomRewardResourceType_Slice = []int{
	1,
	2,
	3,
}

func (x E_CustomRewardResourceType) String() string {
	if name, ok := E_CustomRewardResourceType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_CustomRewardResourceType_Size() int {
	return len(E_CustomRewardResourceType_Slice)
}

func Check_E_CustomRewardResourceType_I(value int) bool {
	if _, ok := E_CustomRewardResourceType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_CustomRewardResourceType(value E_CustomRewardResourceType) bool {
	return Check_E_CustomRewardResourceType_I(int(value))
}

func Each_E_CustomRewardResourceType(f func(E_CustomRewardResourceType) (continued bool)) {
	for _, value := range E_CustomRewardResourceType_Slice {
		if !f(E_CustomRewardResourceType(value)) {
			break
		}
	}
}

func Each_E_CustomRewardResourceType_I(f func(int) (continued bool)) {
	for _, value := range E_CustomRewardResourceType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_CustomRewardResourceType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_BuyGrade] begin

// 购买档次类型
type E_BuyGrade int

const (
	E_BuyGrade_Null E_BuyGrade = 0

	// 购买力档次1
	E_BuyGrade_BuyGrade1 E_BuyGrade = 1
	// 购买力档次2
	E_BuyGrade_BuyGrade2 E_BuyGrade = 2
	// 购买力档次3
	E_BuyGrade_BuyGrade3 E_BuyGrade = 3
	// 购买力档次4
	E_BuyGrade_BuyGrade4 E_BuyGrade = 4
	// 购买力档次5
	E_BuyGrade_BuyGrade5 E_BuyGrade = 5
	// 购买力档次6
	E_BuyGrade_BuyGrade6 E_BuyGrade = 6
	// 购买力档次7
	E_BuyGrade_BuyGrade7 E_BuyGrade = 7
)

var E_BuyGrade_name = map[int]string{
	1: "E_BuyGrade_BuyGrade1",
	2: "E_BuyGrade_BuyGrade2",
	3: "E_BuyGrade_BuyGrade3",
	4: "E_BuyGrade_BuyGrade4",
	5: "E_BuyGrade_BuyGrade5",
	6: "E_BuyGrade_BuyGrade6",
	7: "E_BuyGrade_BuyGrade7",
}

var E_BuyGrade_value = map[string]int{
	"E_BuyGrade_BuyGrade1": 1,
	"E_BuyGrade_BuyGrade2": 2,
	"E_BuyGrade_BuyGrade3": 3,
	"E_BuyGrade_BuyGrade4": 4,
	"E_BuyGrade_BuyGrade5": 5,
	"E_BuyGrade_BuyGrade6": 6,
	"E_BuyGrade_BuyGrade7": 7,
}

var E_BuyGrade_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
}

func (x E_BuyGrade) String() string {
	if name, ok := E_BuyGrade_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_BuyGrade_Size() int {
	return len(E_BuyGrade_Slice)
}

func Check_E_BuyGrade_I(value int) bool {
	if _, ok := E_BuyGrade_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_BuyGrade(value E_BuyGrade) bool {
	return Check_E_BuyGrade_I(int(value))
}

func Each_E_BuyGrade(f func(E_BuyGrade) (continued bool)) {
	for _, value := range E_BuyGrade_Slice {
		if !f(E_BuyGrade(value)) {
			break
		}
	}
}

func Each_E_BuyGrade_I(f func(int) (continued bool)) {
	for _, value := range E_BuyGrade_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_BuyGrade] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ActContainerType] begin

// 活动容器类型
type E_ActContainerType int

const (
	E_ActContainerType_Null E_ActContainerType = 0

	// 福利
	E_ActContainerType_BenefitsEntrance E_ActContainerType = 1
	// 活动
	E_ActContainerType_ActivityEntrance E_ActContainerType = 2
	// 节日活动
	E_ActContainerType_FestivalEntrance E_ActContainerType = 3
	// 限时活动
	E_ActContainerType_LimitedTimeActivity E_ActContainerType = 4
	// 月卡基金
	E_ActContainerType_MonthCardFund E_ActContainerType = 5
)

var E_ActContainerType_name = map[int]string{
	1: "E_ActContainerType_BenefitsEntrance",
	2: "E_ActContainerType_ActivityEntrance",
	3: "E_ActContainerType_FestivalEntrance",
	4: "E_ActContainerType_LimitedTimeActivity",
	5: "E_ActContainerType_MonthCardFund",
}

var E_ActContainerType_value = map[string]int{
	"E_ActContainerType_BenefitsEntrance":    1,
	"E_ActContainerType_ActivityEntrance":    2,
	"E_ActContainerType_FestivalEntrance":    3,
	"E_ActContainerType_LimitedTimeActivity": 4,
	"E_ActContainerType_MonthCardFund":       5,
}

var E_ActContainerType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
}

func (x E_ActContainerType) String() string {
	if name, ok := E_ActContainerType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ActContainerType_Size() int {
	return len(E_ActContainerType_Slice)
}

func Check_E_ActContainerType_I(value int) bool {
	if _, ok := E_ActContainerType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ActContainerType(value E_ActContainerType) bool {
	return Check_E_ActContainerType_I(int(value))
}

func Each_E_ActContainerType(f func(E_ActContainerType) (continued bool)) {
	for _, value := range E_ActContainerType_Slice {
		if !f(E_ActContainerType(value)) {
			break
		}
	}
}

func Each_E_ActContainerType_I(f func(int) (continued bool)) {
	for _, value := range E_ActContainerType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ActContainerType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ActUIType] begin

// 活动UI类型
type E_ActUIType int

const (
	E_ActUIType_Null E_ActUIType = 0

	// 通用UI
	E_ActUIType_Common E_ActUIType = 1
	// 登入几天
	E_ActUIType_LoginDay E_ActUIType = 2
	// 投资计划
	E_ActUIType_Investment E_ActUIType = 3
	// 幸运宝箱
	E_ActUIType_LuckyBox E_ActUIType = 4
	// 一折礼包
	E_ActUIType_Discountgiftpack E_ActUIType = 5
	// 限时礼包
	E_ActUIType_Timelimitedgiftpack E_ActUIType = 6
	// 月卡
	E_ActUIType_MonthCard E_ActUIType = 7
	// 兑换码
	E_ActUIType_RedeemCode E_ActUIType = 8
	// 开服排行榜
	E_ActUIType_OpenRank E_ActUIType = 9
	// 月基金小
	E_ActUIType_MonthFundSmall E_ActUIType = 10
	// 月基金大
	E_ActUIType_MonthFundBig E_ActUIType = 11
	// 周末福利
	E_ActUIType_WeekendBenefits E_ActUIType = 12
	// 新将招募
	E_ActUIType_NewTavern E_ActUIType = 13
	// 限时招募
	E_ActUIType_LimitTavern E_ActUIType = 14
	// 兵书搜索
	E_ActUIType_WarcraftSearch E_ActUIType = 15
	// 每日签到
	E_ActUIType_DailyLoginReward E_ActUIType = 16
	// 任务活动
	E_ActUIType_ActivityTask E_ActUIType = 17
	// 兑换活动
	E_ActUIType_Exchange E_ActUIType = 18
	// 终身卡
	E_ActUIType_LifeCard E_ActUIType = 19
	// 更新公告
	E_ActUIType_UpdateNotice E_ActUIType = 20
	// 日常折扣
	E_ActUIType_DailyDiscount E_ActUIType = 21
)

var E_ActUIType_name = map[int]string{
	1:  "E_ActUIType_Common",
	2:  "E_ActUIType_LoginDay",
	3:  "E_ActUIType_Investment",
	4:  "E_ActUIType_LuckyBox",
	5:  "E_ActUIType_Discountgiftpack",
	6:  "E_ActUIType_Timelimitedgiftpack",
	7:  "E_ActUIType_MonthCard",
	8:  "E_ActUIType_RedeemCode",
	9:  "E_ActUIType_OpenRank",
	10: "E_ActUIType_MonthFundSmall",
	11: "E_ActUIType_MonthFundBig",
	12: "E_ActUIType_WeekendBenefits",
	13: "E_ActUIType_NewTavern",
	14: "E_ActUIType_LimitTavern",
	15: "E_ActUIType_WarcraftSearch",
	16: "E_ActUIType_DailyLoginReward",
	17: "E_ActUIType_ActivityTask",
	18: "E_ActUIType_Exchange",
	19: "E_ActUIType_LifeCard",
	20: "E_ActUIType_UpdateNotice",
	21: "E_ActUIType_DailyDiscount",
}

var E_ActUIType_value = map[string]int{
	"E_ActUIType_Common":              1,
	"E_ActUIType_LoginDay":            2,
	"E_ActUIType_Investment":          3,
	"E_ActUIType_LuckyBox":            4,
	"E_ActUIType_Discountgiftpack":    5,
	"E_ActUIType_Timelimitedgiftpack": 6,
	"E_ActUIType_MonthCard":           7,
	"E_ActUIType_RedeemCode":          8,
	"E_ActUIType_OpenRank":            9,
	"E_ActUIType_MonthFundSmall":      10,
	"E_ActUIType_MonthFundBig":        11,
	"E_ActUIType_WeekendBenefits":     12,
	"E_ActUIType_NewTavern":           13,
	"E_ActUIType_LimitTavern":         14,
	"E_ActUIType_WarcraftSearch":      15,
	"E_ActUIType_DailyLoginReward":    16,
	"E_ActUIType_ActivityTask":        17,
	"E_ActUIType_Exchange":            18,
	"E_ActUIType_LifeCard":            19,
	"E_ActUIType_UpdateNotice":        20,
	"E_ActUIType_DailyDiscount":       21,
}

var E_ActUIType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	14,
	15,
	16,
	17,
	18,
	19,
	20,
	21,
}

func (x E_ActUIType) String() string {
	if name, ok := E_ActUIType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ActUIType_Size() int {
	return len(E_ActUIType_Slice)
}

func Check_E_ActUIType_I(value int) bool {
	if _, ok := E_ActUIType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ActUIType(value E_ActUIType) bool {
	return Check_E_ActUIType_I(int(value))
}

func Each_E_ActUIType(f func(E_ActUIType) (continued bool)) {
	for _, value := range E_ActUIType_Slice {
		if !f(E_ActUIType(value)) {
			break
		}
	}
}

func Each_E_ActUIType_I(f func(int) (continued bool)) {
	for _, value := range E_ActUIType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ActUIType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_SevenTargetType] begin

// 七日目标类型
type E_SevenTargetType int

const (
	E_SevenTargetType_Null E_SevenTargetType = 0

	// 七日目标
	E_SevenTargetType_SevenTarget E_SevenTargetType = 1
	// 十四日目标
	E_SevenTargetType_FourteenTarget E_SevenTargetType = 2
)

var E_SevenTargetType_name = map[int]string{
	1: "E_SevenTargetType_SevenTarget",
	2: "E_SevenTargetType_FourteenTarget",
}

var E_SevenTargetType_value = map[string]int{
	"E_SevenTargetType_SevenTarget":    1,
	"E_SevenTargetType_FourteenTarget": 2,
}

var E_SevenTargetType_Slice = []int{
	1,
	2,
}

func (x E_SevenTargetType) String() string {
	if name, ok := E_SevenTargetType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_SevenTargetType_Size() int {
	return len(E_SevenTargetType_Slice)
}

func Check_E_SevenTargetType_I(value int) bool {
	if _, ok := E_SevenTargetType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_SevenTargetType(value E_SevenTargetType) bool {
	return Check_E_SevenTargetType_I(int(value))
}

func Each_E_SevenTargetType(f func(E_SevenTargetType) (continued bool)) {
	for _, value := range E_SevenTargetType_Slice {
		if !f(E_SevenTargetType(value)) {
			break
		}
	}
}

func Each_E_SevenTargetType_I(f func(int) (continued bool)) {
	for _, value := range E_SevenTargetType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_SevenTargetType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_BaoistMagicBuilding] begin

// 迷宫建筑类型
type E_BaoistMagicBuilding int

const (
	E_BaoistMagicBuilding_Null E_BaoistMagicBuilding = 0

	// 普通怪
	E_BaoistMagicBuilding_DaoistMagicCommonMonster E_BaoistMagicBuilding = 1
	// 精英怪
	E_BaoistMagicBuilding_DaoistMagicEliteMonster E_BaoistMagicBuilding = 2
	// 枭雄
	E_BaoistMagicBuilding_DaoistMagicBoss E_BaoistMagicBuilding = 3
	// 援军令
	E_BaoistMagicBuilding_DaoistMagicReliefTroops E_BaoistMagicBuilding = 4
	// 补给点
	E_BaoistMagicBuilding_DaoistMagicSupply E_BaoistMagicBuilding = 5
	// 神秘人
	E_BaoistMagicBuilding_DaoistMagicMysteryMen E_BaoistMagicBuilding = 6
	// 商人
	E_BaoistMagicBuilding_DaoistMagicTrader E_BaoistMagicBuilding = 7
	// 天牢
	E_BaoistMagicBuilding_DaoistMagicHardReliefTroops E_BaoistMagicBuilding = 8
	// 奇门宝藏
	E_BaoistMagicBuilding_DaoistMagicTreasure E_BaoistMagicBuilding = 9
)

var E_BaoistMagicBuilding_name = map[int]string{
	1: "E_BaoistMagicBuilding_DaoistMagicCommonMonster",
	2: "E_BaoistMagicBuilding_DaoistMagicEliteMonster",
	3: "E_BaoistMagicBuilding_DaoistMagicBoss",
	4: "E_BaoistMagicBuilding_DaoistMagicReliefTroops",
	5: "E_BaoistMagicBuilding_DaoistMagicSupply",
	6: "E_BaoistMagicBuilding_DaoistMagicMysteryMen",
	7: "E_BaoistMagicBuilding_DaoistMagicTrader",
	8: "E_BaoistMagicBuilding_DaoistMagicHardReliefTroops",
	9: "E_BaoistMagicBuilding_DaoistMagicTreasure",
}

var E_BaoistMagicBuilding_value = map[string]int{
	"E_BaoistMagicBuilding_DaoistMagicCommonMonster":    1,
	"E_BaoistMagicBuilding_DaoistMagicEliteMonster":     2,
	"E_BaoistMagicBuilding_DaoistMagicBoss":             3,
	"E_BaoistMagicBuilding_DaoistMagicReliefTroops":     4,
	"E_BaoistMagicBuilding_DaoistMagicSupply":           5,
	"E_BaoistMagicBuilding_DaoistMagicMysteryMen":       6,
	"E_BaoistMagicBuilding_DaoistMagicTrader":           7,
	"E_BaoistMagicBuilding_DaoistMagicHardReliefTroops": 8,
	"E_BaoistMagicBuilding_DaoistMagicTreasure":         9,
}

var E_BaoistMagicBuilding_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
}

func (x E_BaoistMagicBuilding) String() string {
	if name, ok := E_BaoistMagicBuilding_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_BaoistMagicBuilding_Size() int {
	return len(E_BaoistMagicBuilding_Slice)
}

func Check_E_BaoistMagicBuilding_I(value int) bool {
	if _, ok := E_BaoistMagicBuilding_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_BaoistMagicBuilding(value E_BaoistMagicBuilding) bool {
	return Check_E_BaoistMagicBuilding_I(int(value))
}

func Each_E_BaoistMagicBuilding(f func(E_BaoistMagicBuilding) (continued bool)) {
	for _, value := range E_BaoistMagicBuilding_Slice {
		if !f(E_BaoistMagicBuilding(value)) {
			break
		}
	}
}

func Each_E_BaoistMagicBuilding_I(f func(int) (continued bool)) {
	for _, value := range E_BaoistMagicBuilding_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_BaoistMagicBuilding] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_Heirloom] begin

// 迷宫技能品质
type E_Heirloom int

const (
	E_Heirloom_Null E_Heirloom = 0

	// 蓝色
	E_Heirloom_HeirloomBlue E_Heirloom = 1
	// 紫色
	E_Heirloom_HeirloomPurple E_Heirloom = 2
	// 橙色
	E_Heirloom_HeirloomOrange E_Heirloom = 3
)

var E_Heirloom_name = map[int]string{
	1: "E_Heirloom_HeirloomBlue",
	2: "E_Heirloom_HeirloomPurple",
	3: "E_Heirloom_HeirloomOrange",
}

var E_Heirloom_value = map[string]int{
	"E_Heirloom_HeirloomBlue":   1,
	"E_Heirloom_HeirloomPurple": 2,
	"E_Heirloom_HeirloomOrange": 3,
}

var E_Heirloom_Slice = []int{
	1,
	2,
	3,
}

func (x E_Heirloom) String() string {
	if name, ok := E_Heirloom_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_Heirloom_Size() int {
	return len(E_Heirloom_Slice)
}

func Check_E_Heirloom_I(value int) bool {
	if _, ok := E_Heirloom_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_Heirloom(value E_Heirloom) bool {
	return Check_E_Heirloom_I(int(value))
}

func Each_E_Heirloom(f func(E_Heirloom) (continued bool)) {
	for _, value := range E_Heirloom_Slice {
		if !f(E_Heirloom(value)) {
			break
		}
	}
}

func Each_E_Heirloom_I(f func(int) (continued bool)) {
	for _, value := range E_Heirloom_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_Heirloom] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_GuildRecordType] begin

// 公会日志内容类型
type E_GuildRecordType int

const (
	E_GuildRecordType_Null E_GuildRecordType = 0

	// 加入公会
	E_GuildRecordType_GuildJoin E_GuildRecordType = 1
	// 主动离开公会
	E_GuildRecordType_GuildLeave E_GuildRecordType = 2
	// 被移除公会
	E_GuildRecordType_GuildDetach E_GuildRecordType = 3
	// 任命长老
	E_GuildRecordType_BecomeManager E_GuildRecordType = 4
	// 降为成员
	E_GuildRecordType_BecomeMember E_GuildRecordType = 5
	// 会长转让
	E_GuildRecordType_BecomeMaster E_GuildRecordType = 6
	// 退位让贤
	E_GuildRecordType_Abdicate E_GuildRecordType = 7
	// 限时boss开启
	E_GuildRecordType_SpecialBossOpen E_GuildRecordType = 8
	// Boss到期（重置）
	E_GuildRecordType_BossLeave E_GuildRecordType = 9
	// 获得幸运奖励的成员
	E_GuildRecordType_LuckyMember E_GuildRecordType = 10
)

var E_GuildRecordType_name = map[int]string{
	1:  "E_GuildRecordType_GuildJoin",
	2:  "E_GuildRecordType_GuildLeave",
	3:  "E_GuildRecordType_GuildDetach",
	4:  "E_GuildRecordType_BecomeManager",
	5:  "E_GuildRecordType_BecomeMember",
	6:  "E_GuildRecordType_BecomeMaster",
	7:  "E_GuildRecordType_Abdicate",
	8:  "E_GuildRecordType_SpecialBossOpen",
	9:  "E_GuildRecordType_BossLeave",
	10: "E_GuildRecordType_LuckyMember",
}

var E_GuildRecordType_value = map[string]int{
	"E_GuildRecordType_GuildJoin":       1,
	"E_GuildRecordType_GuildLeave":      2,
	"E_GuildRecordType_GuildDetach":     3,
	"E_GuildRecordType_BecomeManager":   4,
	"E_GuildRecordType_BecomeMember":    5,
	"E_GuildRecordType_BecomeMaster":    6,
	"E_GuildRecordType_Abdicate":        7,
	"E_GuildRecordType_SpecialBossOpen": 8,
	"E_GuildRecordType_BossLeave":       9,
	"E_GuildRecordType_LuckyMember":     10,
}

var E_GuildRecordType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
}

func (x E_GuildRecordType) String() string {
	if name, ok := E_GuildRecordType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_GuildRecordType_Size() int {
	return len(E_GuildRecordType_Slice)
}

func Check_E_GuildRecordType_I(value int) bool {
	if _, ok := E_GuildRecordType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_GuildRecordType(value E_GuildRecordType) bool {
	return Check_E_GuildRecordType_I(int(value))
}

func Each_E_GuildRecordType(f func(E_GuildRecordType) (continued bool)) {
	for _, value := range E_GuildRecordType_Slice {
		if !f(E_GuildRecordType(value)) {
			break
		}
	}
}

func Each_E_GuildRecordType_I(f func(int) (continued bool)) {
	for _, value := range E_GuildRecordType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_GuildRecordType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_GuildCondition] begin

// 公会入会条件类型
type E_GuildCondition int

const (
	E_GuildCondition_Null E_GuildCondition = 0

	// 任何人都可加入
	E_GuildCondition_Everyone E_GuildCondition = 1
	// 需要审核
	E_GuildCondition_NeedAgree E_GuildCondition = 2
	// 不允许加入
	E_GuildCondition_Nobody E_GuildCondition = 3
)

var E_GuildCondition_name = map[int]string{
	1: "E_GuildCondition_Everyone",
	2: "E_GuildCondition_NeedAgree",
	3: "E_GuildCondition_Nobody",
}

var E_GuildCondition_value = map[string]int{
	"E_GuildCondition_Everyone":  1,
	"E_GuildCondition_NeedAgree": 2,
	"E_GuildCondition_Nobody":    3,
}

var E_GuildCondition_Slice = []int{
	1,
	2,
	3,
}

func (x E_GuildCondition) String() string {
	if name, ok := E_GuildCondition_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_GuildCondition_Size() int {
	return len(E_GuildCondition_Slice)
}

func Check_E_GuildCondition_I(value int) bool {
	if _, ok := E_GuildCondition_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_GuildCondition(value E_GuildCondition) bool {
	return Check_E_GuildCondition_I(int(value))
}

func Each_E_GuildCondition(f func(E_GuildCondition) (continued bool)) {
	for _, value := range E_GuildCondition_Slice {
		if !f(E_GuildCondition(value)) {
			break
		}
	}
}

func Each_E_GuildCondition_I(f func(int) (continued bool)) {
	for _, value := range E_GuildCondition_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_GuildCondition] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_GuildJob] begin

// 公会职位类型
type E_GuildJob int

const (
	E_GuildJob_Null E_GuildJob = 0

	// 会长
	E_GuildJob_Leader E_GuildJob = 1
	// 长老
	E_GuildJob_Elder E_GuildJob = 2
	// 普通会员
	E_GuildJob_Common E_GuildJob = 3
)

var E_GuildJob_name = map[int]string{
	1: "E_GuildJob_Leader",
	2: "E_GuildJob_Elder",
	3: "E_GuildJob_Common",
}

var E_GuildJob_value = map[string]int{
	"E_GuildJob_Leader": 1,
	"E_GuildJob_Elder":  2,
	"E_GuildJob_Common": 3,
}

var E_GuildJob_Slice = []int{
	1,
	2,
	3,
}

func (x E_GuildJob) String() string {
	if name, ok := E_GuildJob_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_GuildJob_Size() int {
	return len(E_GuildJob_Slice)
}

func Check_E_GuildJob_I(value int) bool {
	if _, ok := E_GuildJob_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_GuildJob(value E_GuildJob) bool {
	return Check_E_GuildJob_I(int(value))
}

func Each_E_GuildJob(f func(E_GuildJob) (continued bool)) {
	for _, value := range E_GuildJob_Slice {
		if !f(E_GuildJob(value)) {
			break
		}
	}
}

func Each_E_GuildJob_I(f func(int) (continued bool)) {
	for _, value := range E_GuildJob_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_GuildJob] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_GuildHunting] begin

// 公会狩猎类型
type E_GuildHunting int

const (
	E_GuildHunting_Null E_GuildHunting = 0

	// 常驻BOSS
	E_GuildHunting_Normal E_GuildHunting = 1
	// 限时BOSS
	E_GuildHunting_Elite E_GuildHunting = 2
)

var E_GuildHunting_name = map[int]string{
	1: "E_GuildHunting_Normal",
	2: "E_GuildHunting_Elite",
}

var E_GuildHunting_value = map[string]int{
	"E_GuildHunting_Normal": 1,
	"E_GuildHunting_Elite":  2,
}

var E_GuildHunting_Slice = []int{
	1,
	2,
}

func (x E_GuildHunting) String() string {
	if name, ok := E_GuildHunting_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_GuildHunting_Size() int {
	return len(E_GuildHunting_Slice)
}

func Check_E_GuildHunting_I(value int) bool {
	if _, ok := E_GuildHunting_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_GuildHunting(value E_GuildHunting) bool {
	return Check_E_GuildHunting_I(int(value))
}

func Each_E_GuildHunting(f func(E_GuildHunting) (continued bool)) {
	for _, value := range E_GuildHunting_Slice {
		if !f(E_GuildHunting(value)) {
			break
		}
	}
}

func Each_E_GuildHunting_I(f func(int) (continued bool)) {
	for _, value := range E_GuildHunting_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_GuildHunting] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_GuildRecommendType] begin

// 公会推荐分类
type E_GuildRecommendType int

const (
	E_GuildRecommendType_Null E_GuildRecommendType = 0

	// 公会推荐A类
	E_GuildRecommendType_GuildA E_GuildRecommendType = 1
	// 公会推荐B类
	E_GuildRecommendType_GuildB E_GuildRecommendType = 2
	// 公会推荐C类
	E_GuildRecommendType_GuildC E_GuildRecommendType = 3
	// 公会推荐D类
	E_GuildRecommendType_GuildD E_GuildRecommendType = 4
)

var E_GuildRecommendType_name = map[int]string{
	1: "E_GuildRecommendType_GuildA",
	2: "E_GuildRecommendType_GuildB",
	3: "E_GuildRecommendType_GuildC",
	4: "E_GuildRecommendType_GuildD",
}

var E_GuildRecommendType_value = map[string]int{
	"E_GuildRecommendType_GuildA": 1,
	"E_GuildRecommendType_GuildB": 2,
	"E_GuildRecommendType_GuildC": 3,
	"E_GuildRecommendType_GuildD": 4,
}

var E_GuildRecommendType_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_GuildRecommendType) String() string {
	if name, ok := E_GuildRecommendType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_GuildRecommendType_Size() int {
	return len(E_GuildRecommendType_Slice)
}

func Check_E_GuildRecommendType_I(value int) bool {
	if _, ok := E_GuildRecommendType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_GuildRecommendType(value E_GuildRecommendType) bool {
	return Check_E_GuildRecommendType_I(int(value))
}

func Each_E_GuildRecommendType(f func(E_GuildRecommendType) (continued bool)) {
	for _, value := range E_GuildRecommendType_Slice {
		if !f(E_GuildRecommendType(value)) {
			break
		}
	}
}

func Each_E_GuildRecommendType_I(f func(int) (continued bool)) {
	for _, value := range E_GuildRecommendType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_GuildRecommendType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_RunSystem] begin

// 角色系统类型
type E_RunSystem int

const (
	E_RunSystem_Null E_RunSystem = 0

	// 安卓
	E_RunSystem_AndroID E_RunSystem = 1
	// 苹果
	E_RunSystem_IOS E_RunSystem = 2
)

var E_RunSystem_name = map[int]string{
	1: "E_RunSystem_AndroID",
	2: "E_RunSystem_IOS",
}

var E_RunSystem_value = map[string]int{
	"E_RunSystem_AndroID": 1,
	"E_RunSystem_IOS":     2,
}

var E_RunSystem_Slice = []int{
	1,
	2,
}

func (x E_RunSystem) String() string {
	if name, ok := E_RunSystem_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_RunSystem_Size() int {
	return len(E_RunSystem_Slice)
}

func Check_E_RunSystem_I(value int) bool {
	if _, ok := E_RunSystem_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_RunSystem(value E_RunSystem) bool {
	return Check_E_RunSystem_I(int(value))
}

func Each_E_RunSystem(f func(E_RunSystem) (continued bool)) {
	for _, value := range E_RunSystem_Slice {
		if !f(E_RunSystem(value)) {
			break
		}
	}
}

func Each_E_RunSystem_I(f func(int) (continued bool)) {
	for _, value := range E_RunSystem_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_RunSystem] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_OnlineRewardType] begin

// 在线奖励类型
type E_OnlineRewardType int

const (
	E_OnlineRewardType_Null E_OnlineRewardType = 0

	// 福利大放送
	E_OnlineRewardType_Once E_OnlineRewardType = 1
	// 每日奖励
	E_OnlineRewardType_Daily E_OnlineRewardType = 2
)

var E_OnlineRewardType_name = map[int]string{
	1: "E_OnlineRewardType_Once",
	2: "E_OnlineRewardType_Daily",
}

var E_OnlineRewardType_value = map[string]int{
	"E_OnlineRewardType_Once":  1,
	"E_OnlineRewardType_Daily": 2,
}

var E_OnlineRewardType_Slice = []int{
	1,
	2,
}

func (x E_OnlineRewardType) String() string {
	if name, ok := E_OnlineRewardType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_OnlineRewardType_Size() int {
	return len(E_OnlineRewardType_Slice)
}

func Check_E_OnlineRewardType_I(value int) bool {
	if _, ok := E_OnlineRewardType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_OnlineRewardType(value E_OnlineRewardType) bool {
	return Check_E_OnlineRewardType_I(int(value))
}

func Each_E_OnlineRewardType(f func(E_OnlineRewardType) (continued bool)) {
	for _, value := range E_OnlineRewardType_Slice {
		if !f(E_OnlineRewardType(value)) {
			break
		}
	}
}

func Each_E_OnlineRewardType_I(f func(int) (continued bool)) {
	for _, value := range E_OnlineRewardType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_OnlineRewardType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ChatChannelType] begin

// 聊天频道类型
type E_ChatChannelType int

const (
	E_ChatChannelType_Null E_ChatChannelType = 0

	// 世界聊天
	E_ChatChannelType_World E_ChatChannelType = 1
	// 公会聊天
	E_ChatChannelType_Guild E_ChatChannelType = 2
	// 私聊
	E_ChatChannelType_Private E_ChatChannelType = 3
	// 同服聊天
	E_ChatChannelType_Server E_ChatChannelType = 4
	// 系统消息
	E_ChatChannelType_System E_ChatChannelType = 5
)

var E_ChatChannelType_name = map[int]string{
	1: "E_ChatChannelType_World",
	2: "E_ChatChannelType_Guild",
	3: "E_ChatChannelType_Private",
	4: "E_ChatChannelType_Server",
	5: "E_ChatChannelType_System",
}

var E_ChatChannelType_value = map[string]int{
	"E_ChatChannelType_World":   1,
	"E_ChatChannelType_Guild":   2,
	"E_ChatChannelType_Private": 3,
	"E_ChatChannelType_Server":  4,
	"E_ChatChannelType_System":  5,
}

var E_ChatChannelType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
}

func (x E_ChatChannelType) String() string {
	if name, ok := E_ChatChannelType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ChatChannelType_Size() int {
	return len(E_ChatChannelType_Slice)
}

func Check_E_ChatChannelType_I(value int) bool {
	if _, ok := E_ChatChannelType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ChatChannelType(value E_ChatChannelType) bool {
	return Check_E_ChatChannelType_I(int(value))
}

func Each_E_ChatChannelType(f func(E_ChatChannelType) (continued bool)) {
	for _, value := range E_ChatChannelType_Slice {
		if !f(E_ChatChannelType(value)) {
			break
		}
	}
}

func Each_E_ChatChannelType_I(f func(int) (continued bool)) {
	for _, value := range E_ChatChannelType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ChatChannelType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_SystemMessageType] begin

// 系统消息类型
type E_SystemMessageType int

const (
	E_SystemMessageType_Null E_SystemMessageType = 0

	// 领取1日首充
	E_SystemMessageType_FirstChargeFirstDay E_SystemMessageType = 1
	// 领取2日首充
	E_SystemMessageType_FirstChargeSecondDay E_SystemMessageType = 2
	// 领取3日首充
	E_SystemMessageType_FirstChargeThirdDay E_SystemMessageType = 3
	// 领取1日百充
	E_SystemMessageType_AddupfirstchargeFirstDay E_SystemMessageType = 4
	// 领取2日百充
	E_SystemMessageType_AddupfirstchargeSecondDay E_SystemMessageType = 5
	// 领取3日百充
	E_SystemMessageType_AddupfirstchargeThirdDay E_SystemMessageType = 6
	// 激活月卡
	E_SystemMessageType_MonthCardActivate E_SystemMessageType = 7
	// 激活终身卡
	E_SystemMessageType_LifeCardActivate E_SystemMessageType = 8
	// 领取投资奖励
	E_SystemMessageType_InvestmentReward E_SystemMessageType = 9
	// 领取高级投资奖励
	E_SystemMessageType_InvestmentVipReward E_SystemMessageType = 10
	// 幸运宝箱
	E_SystemMessageType_LuckyBoxReward E_SystemMessageType = 11
	// 酒馆抽到SSR
	E_SystemMessageType_TavernSsr E_SystemMessageType = 12
	// 酒馆抽到SSSR
	E_SystemMessageType_TavernSssr E_SystemMessageType = 13
	// 碎片合成SSR
	E_SystemMessageType_ComposeSsr E_SystemMessageType = 14
	// 碎片合成SSSR
	E_SystemMessageType_ComposeSssr E_SystemMessageType = 15
	// 激活神器
	E_SystemMessageType_ArtifactActivate E_SystemMessageType = 16
	// 贵族奖励
	E_SystemMessageType_VipReward E_SystemMessageType = 17
	// 竞技场连胜十场以上时
	E_SystemMessageType_ArenaVictor E_SystemMessageType = 18
	// 武将进阶紫+以上白金以下
	E_SystemMessageType_HeroAdvanceLow E_SystemMessageType = 19
	// 武将进阶白金及以上
	E_SystemMessageType_HeroAdvanceHigh E_SystemMessageType = 20
	// 全服前五完成章节计入排行榜
	E_SystemMessageType_ChapterClearTopFive E_SystemMessageType = 21
	// 全服前五完成试炼塔计入排行榜
	E_SystemMessageType_TowerClearTopFive E_SystemMessageType = 22
	// 红装升阶
	E_SystemMessageType_EquipmentAdvance E_SystemMessageType = 23
	// 限时招募SSR
	E_SystemMessageType_LimitTavernSsr E_SystemMessageType = 24
	// 新将招募SSR
	E_SystemMessageType_NewTavernSsr E_SystemMessageType = 25
	// 兵书搜索红色
	E_SystemMessageType_WarcraftSearchRed E_SystemMessageType = 26
	// 新将招募SSSR
	E_SystemMessageType_NewTavernSssr E_SystemMessageType = 27
)

var E_SystemMessageType_name = map[int]string{
	1:  "E_SystemMessageType_FirstChargeFirstDay",
	2:  "E_SystemMessageType_FirstChargeSecondDay",
	3:  "E_SystemMessageType_FirstChargeThirdDay",
	4:  "E_SystemMessageType_AddupfirstchargeFirstDay",
	5:  "E_SystemMessageType_AddupfirstchargeSecondDay",
	6:  "E_SystemMessageType_AddupfirstchargeThirdDay",
	7:  "E_SystemMessageType_MonthCardActivate",
	8:  "E_SystemMessageType_LifeCardActivate",
	9:  "E_SystemMessageType_InvestmentReward",
	10: "E_SystemMessageType_InvestmentVipReward",
	11: "E_SystemMessageType_LuckyBoxReward",
	12: "E_SystemMessageType_TavernSsr",
	13: "E_SystemMessageType_TavernSssr",
	14: "E_SystemMessageType_ComposeSsr",
	15: "E_SystemMessageType_ComposeSssr",
	16: "E_SystemMessageType_ArtifactActivate",
	17: "E_SystemMessageType_VipReward",
	18: "E_SystemMessageType_ArenaVictor",
	19: "E_SystemMessageType_HeroAdvanceLow",
	20: "E_SystemMessageType_HeroAdvanceHigh",
	21: "E_SystemMessageType_ChapterClearTopFive",
	22: "E_SystemMessageType_TowerClearTopFive",
	23: "E_SystemMessageType_EquipmentAdvance",
	24: "E_SystemMessageType_LimitTavernSsr",
	25: "E_SystemMessageType_NewTavernSsr",
	26: "E_SystemMessageType_WarcraftSearchRed",
	27: "E_SystemMessageType_NewTavernSssr",
}

var E_SystemMessageType_value = map[string]int{
	"E_SystemMessageType_FirstChargeFirstDay":       1,
	"E_SystemMessageType_FirstChargeSecondDay":      2,
	"E_SystemMessageType_FirstChargeThirdDay":       3,
	"E_SystemMessageType_AddupfirstchargeFirstDay":  4,
	"E_SystemMessageType_AddupfirstchargeSecondDay": 5,
	"E_SystemMessageType_AddupfirstchargeThirdDay":  6,
	"E_SystemMessageType_MonthCardActivate":         7,
	"E_SystemMessageType_LifeCardActivate":          8,
	"E_SystemMessageType_InvestmentReward":          9,
	"E_SystemMessageType_InvestmentVipReward":       10,
	"E_SystemMessageType_LuckyBoxReward":            11,
	"E_SystemMessageType_TavernSsr":                 12,
	"E_SystemMessageType_TavernSssr":                13,
	"E_SystemMessageType_ComposeSsr":                14,
	"E_SystemMessageType_ComposeSssr":               15,
	"E_SystemMessageType_ArtifactActivate":          16,
	"E_SystemMessageType_VipReward":                 17,
	"E_SystemMessageType_ArenaVictor":               18,
	"E_SystemMessageType_HeroAdvanceLow":            19,
	"E_SystemMessageType_HeroAdvanceHigh":           20,
	"E_SystemMessageType_ChapterClearTopFive":       21,
	"E_SystemMessageType_TowerClearTopFive":         22,
	"E_SystemMessageType_EquipmentAdvance":          23,
	"E_SystemMessageType_LimitTavernSsr":            24,
	"E_SystemMessageType_NewTavernSsr":              25,
	"E_SystemMessageType_WarcraftSearchRed":         26,
	"E_SystemMessageType_NewTavernSssr":             27,
}

var E_SystemMessageType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	14,
	15,
	16,
	17,
	18,
	19,
	20,
	21,
	22,
	23,
	24,
	25,
	26,
	27,
}

func (x E_SystemMessageType) String() string {
	if name, ok := E_SystemMessageType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_SystemMessageType_Size() int {
	return len(E_SystemMessageType_Slice)
}

func Check_E_SystemMessageType_I(value int) bool {
	if _, ok := E_SystemMessageType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_SystemMessageType(value E_SystemMessageType) bool {
	return Check_E_SystemMessageType_I(int(value))
}

func Each_E_SystemMessageType(f func(E_SystemMessageType) (continued bool)) {
	for _, value := range E_SystemMessageType_Slice {
		if !f(E_SystemMessageType(value)) {
			break
		}
	}
}

func Each_E_SystemMessageType_I(f func(int) (continued bool)) {
	for _, value := range E_SystemMessageType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_SystemMessageType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_GuideHelpType] begin

// 引导帮助类型
type E_GuideHelpType int

const (
	E_GuideHelpType_Null E_GuideHelpType = 0

	// 我要变强
	E_GuideHelpType_BecomeStronger E_GuideHelpType = 1
	// 获取资源
	E_GuideHelpType_GetResource E_GuideHelpType = 2
	// 推荐阵容
	E_GuideHelpType_RecommendFormation E_GuideHelpType = 3
	// 常见问题
	E_GuideHelpType_CommonProblem E_GuideHelpType = 4
)

var E_GuideHelpType_name = map[int]string{
	1: "E_GuideHelpType_BecomeStronger",
	2: "E_GuideHelpType_GetResource",
	3: "E_GuideHelpType_RecommendFormation",
	4: "E_GuideHelpType_CommonProblem",
}

var E_GuideHelpType_value = map[string]int{
	"E_GuideHelpType_BecomeStronger":     1,
	"E_GuideHelpType_GetResource":        2,
	"E_GuideHelpType_RecommendFormation": 3,
	"E_GuideHelpType_CommonProblem":      4,
}

var E_GuideHelpType_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_GuideHelpType) String() string {
	if name, ok := E_GuideHelpType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_GuideHelpType_Size() int {
	return len(E_GuideHelpType_Slice)
}

func Check_E_GuideHelpType_I(value int) bool {
	if _, ok := E_GuideHelpType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_GuideHelpType(value E_GuideHelpType) bool {
	return Check_E_GuideHelpType_I(int(value))
}

func Each_E_GuideHelpType(f func(E_GuideHelpType) (continued bool)) {
	for _, value := range E_GuideHelpType_Slice {
		if !f(E_GuideHelpType(value)) {
			break
		}
	}
}

func Each_E_GuideHelpType_I(f func(int) (continued bool)) {
	for _, value := range E_GuideHelpType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_GuideHelpType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_DungeonsType] begin

// 材料副本类型
type E_DungeonsType int

const (
	E_DungeonsType_Null E_DungeonsType = 0

	// 装备
	E_DungeonsType_Equipment E_DungeonsType = 1
	// 武将
	E_DungeonsType_Hero E_DungeonsType = 2
	// 银币
	E_DungeonsType_SilverCoin E_DungeonsType = 3
	// 武将经验
	E_DungeonsType_HeroExp E_DungeonsType = 4
	// 突破丹
	E_DungeonsType_LimitPassPill E_DungeonsType = 5
)

var E_DungeonsType_name = map[int]string{
	1: "E_DungeonsType_Equipment",
	2: "E_DungeonsType_Hero",
	3: "E_DungeonsType_SilverCoin",
	4: "E_DungeonsType_HeroExp",
	5: "E_DungeonsType_LimitPassPill",
}

var E_DungeonsType_value = map[string]int{
	"E_DungeonsType_Equipment":     1,
	"E_DungeonsType_Hero":          2,
	"E_DungeonsType_SilverCoin":    3,
	"E_DungeonsType_HeroExp":       4,
	"E_DungeonsType_LimitPassPill": 5,
}

var E_DungeonsType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
}

func (x E_DungeonsType) String() string {
	if name, ok := E_DungeonsType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_DungeonsType_Size() int {
	return len(E_DungeonsType_Slice)
}

func Check_E_DungeonsType_I(value int) bool {
	if _, ok := E_DungeonsType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_DungeonsType(value E_DungeonsType) bool {
	return Check_E_DungeonsType_I(int(value))
}

func Each_E_DungeonsType(f func(E_DungeonsType) (continued bool)) {
	for _, value := range E_DungeonsType_Slice {
		if !f(E_DungeonsType(value)) {
			break
		}
	}
}

func Each_E_DungeonsType_I(f func(int) (continued bool)) {
	for _, value := range E_DungeonsType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_DungeonsType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_DungeonsDifficultyType] begin

// 材料副本难度
type E_DungeonsDifficultyType int

const (
	E_DungeonsDifficultyType_Null E_DungeonsDifficultyType = 0

	// 普通
	E_DungeonsDifficultyType_Normal E_DungeonsDifficultyType = 1
	// 冒险
	E_DungeonsDifficultyType_Adventure E_DungeonsDifficultyType = 2
	// 困难
	E_DungeonsDifficultyType_Hard E_DungeonsDifficultyType = 3
	// 地狱
	E_DungeonsDifficultyType_Hell E_DungeonsDifficultyType = 4
	// 炼狱
	E_DungeonsDifficultyType_Purgatory E_DungeonsDifficultyType = 5
	// 英雄
	E_DungeonsDifficultyType_Hero E_DungeonsDifficultyType = 6
	// 王者
	E_DungeonsDifficultyType_King E_DungeonsDifficultyType = 7
)

var E_DungeonsDifficultyType_name = map[int]string{
	1: "E_DungeonsDifficultyType_Normal",
	2: "E_DungeonsDifficultyType_Adventure",
	3: "E_DungeonsDifficultyType_Hard",
	4: "E_DungeonsDifficultyType_Hell",
	5: "E_DungeonsDifficultyType_Purgatory",
	6: "E_DungeonsDifficultyType_Hero",
	7: "E_DungeonsDifficultyType_King",
}

var E_DungeonsDifficultyType_value = map[string]int{
	"E_DungeonsDifficultyType_Normal":    1,
	"E_DungeonsDifficultyType_Adventure": 2,
	"E_DungeonsDifficultyType_Hard":      3,
	"E_DungeonsDifficultyType_Hell":      4,
	"E_DungeonsDifficultyType_Purgatory": 5,
	"E_DungeonsDifficultyType_Hero":      6,
	"E_DungeonsDifficultyType_King":      7,
}

var E_DungeonsDifficultyType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
}

func (x E_DungeonsDifficultyType) String() string {
	if name, ok := E_DungeonsDifficultyType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_DungeonsDifficultyType_Size() int {
	return len(E_DungeonsDifficultyType_Slice)
}

func Check_E_DungeonsDifficultyType_I(value int) bool {
	if _, ok := E_DungeonsDifficultyType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_DungeonsDifficultyType(value E_DungeonsDifficultyType) bool {
	return Check_E_DungeonsDifficultyType_I(int(value))
}

func Each_E_DungeonsDifficultyType(f func(E_DungeonsDifficultyType) (continued bool)) {
	for _, value := range E_DungeonsDifficultyType_Slice {
		if !f(E_DungeonsDifficultyType(value)) {
			break
		}
	}
}

func Each_E_DungeonsDifficultyType_I(f func(int) (continued bool)) {
	for _, value := range E_DungeonsDifficultyType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_DungeonsDifficultyType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ChargeType] begin

// 充值商品类型
type E_ChargeType int

const (
	E_ChargeType_Null E_ChargeType = 0

	// 普通充值
	E_ChargeType_ChargeCommon E_ChargeType = 1
	// 礼包
	E_ChargeType_ChargeGiftPack E_ChargeType = 2
	// 月卡
	E_ChargeType_ChargeMonthCard E_ChargeType = 3
	// 首充多倍
	E_ChargeType_ChargeFirstMultTimes E_ChargeType = 4
	// 月基金
	E_ChargeType_ChargeMonthFund E_ChargeType = 5
	// 高阶封赏令
	E_ChargeType_ChargeHighAssaultToken E_ChargeType = 6
	// 英雄降临购买小乔
	E_ChargeType_HeroAdvent E_ChargeType = 7
	// 开服限时礼包
	E_ChargeType_OpenServiceGiftPack E_ChargeType = 8
	// 成长基金
	E_ChargeType_Investment E_ChargeType = 9
	// 成功充值3元可以激活剩余的２个BUFF
	E_ChargeType_BattleBUFF E_ChargeType = 10
)

var E_ChargeType_name = map[int]string{
	1:  "E_ChargeType_ChargeCommon",
	2:  "E_ChargeType_ChargeGiftPack",
	3:  "E_ChargeType_ChargeMonthCard",
	4:  "E_ChargeType_ChargeFirstMultTimes",
	5:  "E_ChargeType_ChargeMonthFund",
	6:  "E_ChargeType_ChargeHighAssaultToken",
	7:  "E_ChargeType_HeroAdvent",
	8:  "E_ChargeType_OpenServiceGiftPack",
	9:  "E_ChargeType_Investment",
	10: "E_ChargeType_BattleBUFF",
}

var E_ChargeType_value = map[string]int{
	"E_ChargeType_ChargeCommon":           1,
	"E_ChargeType_ChargeGiftPack":         2,
	"E_ChargeType_ChargeMonthCard":        3,
	"E_ChargeType_ChargeFirstMultTimes":   4,
	"E_ChargeType_ChargeMonthFund":        5,
	"E_ChargeType_ChargeHighAssaultToken": 6,
	"E_ChargeType_HeroAdvent":             7,
	"E_ChargeType_OpenServiceGiftPack":    8,
	"E_ChargeType_Investment":             9,
	"E_ChargeType_BattleBUFF":             10,
}

var E_ChargeType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
}

func (x E_ChargeType) String() string {
	if name, ok := E_ChargeType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ChargeType_Size() int {
	return len(E_ChargeType_Slice)
}

func Check_E_ChargeType_I(value int) bool {
	if _, ok := E_ChargeType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ChargeType(value E_ChargeType) bool {
	return Check_E_ChargeType_I(int(value))
}

func Each_E_ChargeType(f func(E_ChargeType) (continued bool)) {
	for _, value := range E_ChargeType_Slice {
		if !f(E_ChargeType(value)) {
			break
		}
	}
}

func Each_E_ChargeType_I(f func(int) (continued bool)) {
	for _, value := range E_ChargeType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ChargeType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_PayStatus] begin

// 订单支付状态类型
type E_PayStatus int

const (
	E_PayStatus_Null E_PayStatus = 0

	// 支付成功
	E_PayStatus_Success E_PayStatus = 1
	// 支付失败
	E_PayStatus_Failure E_PayStatus = 2
	// 取消支付
	E_PayStatus_Cancel E_PayStatus = 3
	// 待支付
	E_PayStatus_Pending E_PayStatus = 4
	// 超时
	E_PayStatus_Timeout E_PayStatus = 5
)

var E_PayStatus_name = map[int]string{
	1: "E_PayStatus_Success",
	2: "E_PayStatus_Failure",
	3: "E_PayStatus_Cancel",
	4: "E_PayStatus_Pending",
	5: "E_PayStatus_Timeout",
}

var E_PayStatus_value = map[string]int{
	"E_PayStatus_Success": 1,
	"E_PayStatus_Failure": 2,
	"E_PayStatus_Cancel":  3,
	"E_PayStatus_Pending": 4,
	"E_PayStatus_Timeout": 5,
}

var E_PayStatus_Slice = []int{
	1,
	2,
	3,
	4,
	5,
}

func (x E_PayStatus) String() string {
	if name, ok := E_PayStatus_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_PayStatus_Size() int {
	return len(E_PayStatus_Slice)
}

func Check_E_PayStatus_I(value int) bool {
	if _, ok := E_PayStatus_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_PayStatus(value E_PayStatus) bool {
	return Check_E_PayStatus_I(int(value))
}

func Each_E_PayStatus(f func(E_PayStatus) (continued bool)) {
	for _, value := range E_PayStatus_Slice {
		if !f(E_PayStatus(value)) {
			break
		}
	}
}

func Each_E_PayStatus_I(f func(int) (continued bool)) {
	for _, value := range E_PayStatus_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_PayStatus] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_HeroRarity] begin

// 英雄稀有度类型
type E_HeroRarity int

const (
	E_HeroRarity_Null E_HeroRarity = 0

	// R
	E_HeroRarity_RarityR E_HeroRarity = 1
	// SR
	E_HeroRarity_RaritySR E_HeroRarity = 2
	// SSR
	E_HeroRarity_RaritySSR E_HeroRarity = 3
	// SSSR
	E_HeroRarity_RaritySSSR E_HeroRarity = 4
)

var E_HeroRarity_name = map[int]string{
	1: "E_HeroRarity_RarityR",
	2: "E_HeroRarity_RaritySR",
	3: "E_HeroRarity_RaritySSR",
	4: "E_HeroRarity_RaritySSSR",
}

var E_HeroRarity_value = map[string]int{
	"E_HeroRarity_RarityR":    1,
	"E_HeroRarity_RaritySR":   2,
	"E_HeroRarity_RaritySSR":  3,
	"E_HeroRarity_RaritySSSR": 4,
}

var E_HeroRarity_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_HeroRarity) String() string {
	if name, ok := E_HeroRarity_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_HeroRarity_Size() int {
	return len(E_HeroRarity_Slice)
}

func Check_E_HeroRarity_I(value int) bool {
	if _, ok := E_HeroRarity_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_HeroRarity(value E_HeroRarity) bool {
	return Check_E_HeroRarity_I(int(value))
}

func Each_E_HeroRarity(f func(E_HeroRarity) (continued bool)) {
	for _, value := range E_HeroRarity_Slice {
		if !f(E_HeroRarity(value)) {
			break
		}
	}
}

func Each_E_HeroRarity_I(f func(int) (continued bool)) {
	for _, value := range E_HeroRarity_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_HeroRarity] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_DialogueType] begin

// 对话类型
type E_DialogueType int

const (
	E_DialogueType_Null E_DialogueType = 0

	// 剧情
	E_DialogueType_DialoguePlot E_DialogueType = 1
	// 气泡
	E_DialogueType_DialogueBubble E_DialogueType = 2
	// 引导
	E_DialogueType_DialogueGuide E_DialogueType = 3
	// 普通对话
	E_DialogueType_DialogueCommom E_DialogueType = 4
	// 战斗中出现援军时对话
	E_DialogueType_DialogueReinforce E_DialogueType = 5
)

var E_DialogueType_name = map[int]string{
	1: "E_DialogueType_DialoguePlot",
	2: "E_DialogueType_DialogueBubble",
	3: "E_DialogueType_DialogueGuide",
	4: "E_DialogueType_DialogueCommom",
	5: "E_DialogueType_DialogueReinforce",
}

var E_DialogueType_value = map[string]int{
	"E_DialogueType_DialoguePlot":      1,
	"E_DialogueType_DialogueBubble":    2,
	"E_DialogueType_DialogueGuide":     3,
	"E_DialogueType_DialogueCommom":    4,
	"E_DialogueType_DialogueReinforce": 5,
}

var E_DialogueType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
}

func (x E_DialogueType) String() string {
	if name, ok := E_DialogueType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_DialogueType_Size() int {
	return len(E_DialogueType_Slice)
}

func Check_E_DialogueType_I(value int) bool {
	if _, ok := E_DialogueType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_DialogueType(value E_DialogueType) bool {
	return Check_E_DialogueType_I(int(value))
}

func Each_E_DialogueType(f func(E_DialogueType) (continued bool)) {
	for _, value := range E_DialogueType_Slice {
		if !f(E_DialogueType(value)) {
			break
		}
	}
}

func Each_E_DialogueType_I(f func(int) (continued bool)) {
	for _, value := range E_DialogueType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_DialogueType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_DialoguePosition] begin

// 对话头像位置
type E_DialoguePosition int

const (
	E_DialoguePosition_Null E_DialoguePosition = 0

	// 左
	E_DialoguePosition_DialogueLeft E_DialoguePosition = 1
	// 右
	E_DialoguePosition_DialogueRight E_DialoguePosition = 2
)

var E_DialoguePosition_name = map[int]string{
	1: "E_DialoguePosition_DialogueLeft",
	2: "E_DialoguePosition_DialogueRight",
}

var E_DialoguePosition_value = map[string]int{
	"E_DialoguePosition_DialogueLeft":  1,
	"E_DialoguePosition_DialogueRight": 2,
}

var E_DialoguePosition_Slice = []int{
	1,
	2,
}

func (x E_DialoguePosition) String() string {
	if name, ok := E_DialoguePosition_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_DialoguePosition_Size() int {
	return len(E_DialoguePosition_Slice)
}

func Check_E_DialoguePosition_I(value int) bool {
	if _, ok := E_DialoguePosition_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_DialoguePosition(value E_DialoguePosition) bool {
	return Check_E_DialoguePosition_I(int(value))
}

func Each_E_DialoguePosition(f func(E_DialoguePosition) (continued bool)) {
	for _, value := range E_DialoguePosition_Slice {
		if !f(E_DialoguePosition(value)) {
			break
		}
	}
}

func Each_E_DialoguePosition_I(f func(int) (continued bool)) {
	for _, value := range E_DialoguePosition_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_DialoguePosition] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_DialogueACt] begin

// 对话头像动作类型
type E_DialogueACt int

const (
	E_DialogueACt_Null E_DialogueACt = 0

	// 抖动
	E_DialogueACt_DialogueACtShake E_DialogueACt = 1
	// 移动
	E_DialogueACt_DialogueACtMobile E_DialogueACt = 2
	// 伸缩
	E_DialogueACt_DialogueACtTelescopic E_DialogueACt = 3
)

var E_DialogueACt_name = map[int]string{
	1: "E_DialogueACt_DialogueACtShake",
	2: "E_DialogueACt_DialogueACtMobile",
	3: "E_DialogueACt_DialogueACtTelescopic",
}

var E_DialogueACt_value = map[string]int{
	"E_DialogueACt_DialogueACtShake":      1,
	"E_DialogueACt_DialogueACtMobile":     2,
	"E_DialogueACt_DialogueACtTelescopic": 3,
}

var E_DialogueACt_Slice = []int{
	1,
	2,
	3,
}

func (x E_DialogueACt) String() string {
	if name, ok := E_DialogueACt_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_DialogueACt_Size() int {
	return len(E_DialogueACt_Slice)
}

func Check_E_DialogueACt_I(value int) bool {
	if _, ok := E_DialogueACt_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_DialogueACt(value E_DialogueACt) bool {
	return Check_E_DialogueACt_I(int(value))
}

func Each_E_DialogueACt(f func(E_DialogueACt) (continued bool)) {
	for _, value := range E_DialogueACt_Slice {
		if !f(E_DialogueACt(value)) {
			break
		}
	}
}

func Each_E_DialogueACt_I(f func(int) (continued bool)) {
	for _, value := range E_DialogueACt_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_DialogueACt] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_GoldCost] begin

// 消耗元宝来源
type E_GoldCost int

const (
	E_GoldCost_Null E_GoldCost = 0

	// 酒馆招募
	E_GoldCost_TavernRecruit E_GoldCost = 1
	// 商店刷新
	E_GoldCost_ShopRefresh E_GoldCost = 2
	// 商店消费
	E_GoldCost_ShopBuy E_GoldCost = 3
	// 扩充背包
	E_GoldCost_HeroBagUp E_GoldCost = 4
	// 折扣商城
	E_GoldCost_DiscountShop E_GoldCost = 5
	// 投资计划
	E_GoldCost_Investment E_GoldCost = 6
	// 幸运宝箱
	E_GoldCost_LuckyBox E_GoldCost = 7
	// 快速挂机
	E_GoldCost_FastRewards E_GoldCost = 8
	// 奇门遁甲刷新
	E_GoldCost_DaoistMagicRefresh E_GoldCost = 9
	// 材料副本次数购买
	E_GoldCost_DungeonTimes E_GoldCost = 10
	// 英雄救美次数购买
	E_GoldCost_RescueTimes E_GoldCost = 11
	// 创建公会
	E_GoldCost_GuildCreate E_GoldCost = 12
	// 角色改名
	E_GoldCost_NameChange E_GoldCost = 13
	// 英雄重置
	E_GoldCost_HeroRefresh E_GoldCost = 14
	// 布告栏刷新
	E_GoldCost_NoticeBoardRefresh E_GoldCost = 15
	// 快捷购买
	E_GoldCost_QuickBuy E_GoldCost = 16
	// 御将台格子冷却
	E_GoldCost_HeroMasterGridCooling E_GoldCost = 17
	// 英雄救美奖励冷却
	E_GoldCost_RescueRewardCooling E_GoldCost = 18
	// 激活酒馆阵营卡池
	E_GoldCost_ActiveTavernFaction E_GoldCost = 19
	// 在搜索兵书活动中兑换兵书
	E_GoldCost_ExchangeWarcraftInActivity E_GoldCost = 20
	// 购买封赏令等级
	E_GoldCost_BuyAssaultTokeLevel E_GoldCost = 21
	// 购买高阶封赏令
	E_GoldCost_UnlockedHighAssaultToken E_GoldCost = 22
	// 购买世界BOSS挑战次数
	E_GoldCost_WorldBossTimes E_GoldCost = 23
)

var E_GoldCost_name = map[int]string{
	1:  "E_GoldCost_TavernRecruit",
	2:  "E_GoldCost_ShopRefresh",
	3:  "E_GoldCost_ShopBuy",
	4:  "E_GoldCost_HeroBagUp",
	5:  "E_GoldCost_DiscountShop",
	6:  "E_GoldCost_Investment",
	7:  "E_GoldCost_LuckyBox",
	8:  "E_GoldCost_FastRewards",
	9:  "E_GoldCost_DaoistMagicRefresh",
	10: "E_GoldCost_DungeonTimes",
	11: "E_GoldCost_RescueTimes",
	12: "E_GoldCost_GuildCreate",
	13: "E_GoldCost_NameChange",
	14: "E_GoldCost_HeroRefresh",
	15: "E_GoldCost_NoticeBoardRefresh",
	16: "E_GoldCost_QuickBuy",
	17: "E_GoldCost_HeroMasterGridCooling",
	18: "E_GoldCost_RescueRewardCooling",
	19: "E_GoldCost_ActiveTavernFaction",
	20: "E_GoldCost_ExchangeWarcraftInActivity",
	21: "E_GoldCost_BuyAssaultTokeLevel",
	22: "E_GoldCost_UnlockedHighAssaultToken",
	23: "E_GoldCost_WorldBossTimes",
}

var E_GoldCost_value = map[string]int{
	"E_GoldCost_TavernRecruit":              1,
	"E_GoldCost_ShopRefresh":                2,
	"E_GoldCost_ShopBuy":                    3,
	"E_GoldCost_HeroBagUp":                  4,
	"E_GoldCost_DiscountShop":               5,
	"E_GoldCost_Investment":                 6,
	"E_GoldCost_LuckyBox":                   7,
	"E_GoldCost_FastRewards":                8,
	"E_GoldCost_DaoistMagicRefresh":         9,
	"E_GoldCost_DungeonTimes":               10,
	"E_GoldCost_RescueTimes":                11,
	"E_GoldCost_GuildCreate":                12,
	"E_GoldCost_NameChange":                 13,
	"E_GoldCost_HeroRefresh":                14,
	"E_GoldCost_NoticeBoardRefresh":         15,
	"E_GoldCost_QuickBuy":                   16,
	"E_GoldCost_HeroMasterGridCooling":      17,
	"E_GoldCost_RescueRewardCooling":        18,
	"E_GoldCost_ActiveTavernFaction":        19,
	"E_GoldCost_ExchangeWarcraftInActivity": 20,
	"E_GoldCost_BuyAssaultTokeLevel":        21,
	"E_GoldCost_UnlockedHighAssaultToken":   22,
	"E_GoldCost_WorldBossTimes":             23,
}

var E_GoldCost_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	14,
	15,
	16,
	17,
	18,
	19,
	20,
	21,
	22,
	23,
}

func (x E_GoldCost) String() string {
	if name, ok := E_GoldCost_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_GoldCost_Size() int {
	return len(E_GoldCost_Slice)
}

func Check_E_GoldCost_I(value int) bool {
	if _, ok := E_GoldCost_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_GoldCost(value E_GoldCost) bool {
	return Check_E_GoldCost_I(int(value))
}

func Each_E_GoldCost(f func(E_GoldCost) (continued bool)) {
	for _, value := range E_GoldCost_Slice {
		if !f(E_GoldCost(value)) {
			break
		}
	}
}

func Each_E_GoldCost_I(f func(int) (continued bool)) {
	for _, value := range E_GoldCost_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_GoldCost] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_GoldGet] begin

// 获得元宝来源
type E_GoldGet int

const (
	E_GoldGet_Null E_GoldGet = 0

	// 邮件
	E_GoldGet_Mail E_GoldGet = 1
	// 兑换码
	E_GoldGet_ExchangeCode E_GoldGet = 2
	// 月卡/终身卡
	E_GoldGet_MonthCard E_GoldGet = 3
	// 奇门遁甲奖励
	E_GoldGet_DaoistMagicReward E_GoldGet = 4
	// 充值
	E_GoldGet_Charge E_GoldGet = 5
	// 充值活动奖励
	E_GoldGet_ChargeGift E_GoldGet = 6
	// 公会boss
	E_GoldGet_GuildBoss E_GoldGet = 7
	// 其他
	E_GoldGet_Other E_GoldGet = 8
	// 高阶封赏令
	E_GoldGet_HighAssaultToken E_GoldGet = 9
)

var E_GoldGet_name = map[int]string{
	1: "E_GoldGet_Mail",
	2: "E_GoldGet_ExchangeCode",
	3: "E_GoldGet_MonthCard",
	4: "E_GoldGet_DaoistMagicReward",
	5: "E_GoldGet_Charge",
	6: "E_GoldGet_ChargeGift",
	7: "E_GoldGet_GuildBoss",
	8: "E_GoldGet_Other",
	9: "E_GoldGet_HighAssaultToken",
}

var E_GoldGet_value = map[string]int{
	"E_GoldGet_Mail":              1,
	"E_GoldGet_ExchangeCode":      2,
	"E_GoldGet_MonthCard":         3,
	"E_GoldGet_DaoistMagicReward": 4,
	"E_GoldGet_Charge":            5,
	"E_GoldGet_ChargeGift":        6,
	"E_GoldGet_GuildBoss":         7,
	"E_GoldGet_Other":             8,
	"E_GoldGet_HighAssaultToken":  9,
}

var E_GoldGet_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
}

func (x E_GoldGet) String() string {
	if name, ok := E_GoldGet_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_GoldGet_Size() int {
	return len(E_GoldGet_Slice)
}

func Check_E_GoldGet_I(value int) bool {
	if _, ok := E_GoldGet_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_GoldGet(value E_GoldGet) bool {
	return Check_E_GoldGet_I(int(value))
}

func Each_E_GoldGet(f func(E_GoldGet) (continued bool)) {
	for _, value := range E_GoldGet_Slice {
		if !f(E_GoldGet(value)) {
			break
		}
	}
}

func Each_E_GoldGet_I(f func(int) (continued bool)) {
	for _, value := range E_GoldGet_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_GoldGet] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_EquipGet] begin

// 获得装备来源
type E_EquipGet int

const (
	E_EquipGet_Null E_EquipGet = 0

	// 挂机掉落
	E_EquipGet_Idle E_EquipGet = 1
	// 普通商店
	E_EquipGet_CommonShop E_EquipGet = 2
	// 公会商店
	E_EquipGet_GuildShop E_EquipGet = 3
	// 礼包开出
	E_EquipGet_ItemGet E_EquipGet = 4
	// 任务奖励
	E_EquipGet_QuestReward E_EquipGet = 5
	// 奇门遁甲商人
	E_EquipGet_DaoistMagicTrader E_EquipGet = 6
	// 功能开启奖励
	E_EquipGet_FunctionReward E_EquipGet = 7
	// 活动奖励
	E_EquipGet_ActivityReward E_EquipGet = 8
	// 装备升阶
	E_EquipGet_EquipAdvance E_EquipGet = 9
)

var E_EquipGet_name = map[int]string{
	1: "E_EquipGet_Idle",
	2: "E_EquipGet_CommonShop",
	3: "E_EquipGet_GuildShop",
	4: "E_EquipGet_ItemGet",
	5: "E_EquipGet_QuestReward",
	6: "E_EquipGet_DaoistMagicTrader",
	7: "E_EquipGet_FunctionReward",
	8: "E_EquipGet_ActivityReward",
	9: "E_EquipGet_EquipAdvance",
}

var E_EquipGet_value = map[string]int{
	"E_EquipGet_Idle":              1,
	"E_EquipGet_CommonShop":        2,
	"E_EquipGet_GuildShop":         3,
	"E_EquipGet_ItemGet":           4,
	"E_EquipGet_QuestReward":       5,
	"E_EquipGet_DaoistMagicTrader": 6,
	"E_EquipGet_FunctionReward":    7,
	"E_EquipGet_ActivityReward":    8,
	"E_EquipGet_EquipAdvance":      9,
}

var E_EquipGet_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
}

func (x E_EquipGet) String() string {
	if name, ok := E_EquipGet_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_EquipGet_Size() int {
	return len(E_EquipGet_Slice)
}

func Check_E_EquipGet_I(value int) bool {
	if _, ok := E_EquipGet_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_EquipGet(value E_EquipGet) bool {
	return Check_E_EquipGet_I(int(value))
}

func Each_E_EquipGet(f func(E_EquipGet) (continued bool)) {
	for _, value := range E_EquipGet_Slice {
		if !f(E_EquipGet(value)) {
			break
		}
	}
}

func Each_E_EquipGet_I(f func(int) (continued bool)) {
	for _, value := range E_EquipGet_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_EquipGet] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_EquipCost] begin

// 消耗装备渠道
type E_EquipCost int

const (
	E_EquipCost_Null E_EquipCost = 0

	// 装备分解
	E_EquipCost_EquipBreak E_EquipCost = 1
	// 装备升阶
	E_EquipCost_EquipAdvance E_EquipCost = 2
)

var E_EquipCost_name = map[int]string{
	1: "E_EquipCost_EquipBreak",
	2: "E_EquipCost_EquipAdvance",
}

var E_EquipCost_value = map[string]int{
	"E_EquipCost_EquipBreak":   1,
	"E_EquipCost_EquipAdvance": 2,
}

var E_EquipCost_Slice = []int{
	1,
	2,
}

func (x E_EquipCost) String() string {
	if name, ok := E_EquipCost_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_EquipCost_Size() int {
	return len(E_EquipCost_Slice)
}

func Check_E_EquipCost_I(value int) bool {
	if _, ok := E_EquipCost_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_EquipCost(value E_EquipCost) bool {
	return Check_E_EquipCost_I(int(value))
}

func Each_E_EquipCost(f func(E_EquipCost) (continued bool)) {
	for _, value := range E_EquipCost_Slice {
		if !f(E_EquipCost(value)) {
			break
		}
	}
}

func Each_E_EquipCost_I(f func(int) (continued bool)) {
	for _, value := range E_EquipCost_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_EquipCost] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_HeroGetChannel] begin

// 获得英雄渠道
type E_HeroGetChannel int

const (
	E_HeroGetChannel_Null E_HeroGetChannel = 0

	// 酒馆招募
	E_HeroGetChannel_Tavern E_HeroGetChannel = 1
	// 碎片合成
	E_HeroGetChannel_Compose E_HeroGetChannel = 2
	// 种族券
	E_HeroGetChannel_GiftPack E_HeroGetChannel = 3
	// 自选礼包
	E_HeroGetChannel_MultChoiceGiftPack E_HeroGetChannel = 4
	// 充值活动奖励
	E_HeroGetChannel_ChargeGift E_HeroGetChannel = 5
	// 任务奖励
	E_HeroGetChannel_QuestReward E_HeroGetChannel = 6
)

var E_HeroGetChannel_name = map[int]string{
	1: "E_HeroGetChannel_Tavern",
	2: "E_HeroGetChannel_Compose",
	3: "E_HeroGetChannel_GiftPack",
	4: "E_HeroGetChannel_MultChoiceGiftPack",
	5: "E_HeroGetChannel_ChargeGift",
	6: "E_HeroGetChannel_QuestReward",
}

var E_HeroGetChannel_value = map[string]int{
	"E_HeroGetChannel_Tavern":             1,
	"E_HeroGetChannel_Compose":            2,
	"E_HeroGetChannel_GiftPack":           3,
	"E_HeroGetChannel_MultChoiceGiftPack": 4,
	"E_HeroGetChannel_ChargeGift":         5,
	"E_HeroGetChannel_QuestReward":        6,
}

var E_HeroGetChannel_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
}

func (x E_HeroGetChannel) String() string {
	if name, ok := E_HeroGetChannel_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_HeroGetChannel_Size() int {
	return len(E_HeroGetChannel_Slice)
}

func Check_E_HeroGetChannel_I(value int) bool {
	if _, ok := E_HeroGetChannel_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_HeroGetChannel(value E_HeroGetChannel) bool {
	return Check_E_HeroGetChannel_I(int(value))
}

func Each_E_HeroGetChannel(f func(E_HeroGetChannel) (continued bool)) {
	for _, value := range E_HeroGetChannel_Slice {
		if !f(E_HeroGetChannel(value)) {
			break
		}
	}
}

func Each_E_HeroGetChannel_I(f func(int) (continued bool)) {
	for _, value := range E_HeroGetChannel_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_HeroGetChannel] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_LoginReward] begin

// 签到枚举
type E_LoginReward int

const (
	E_LoginReward_Null E_LoginReward = 0

	// 日常签到
	E_LoginReward_DailyLogin E_LoginReward = 1
	// 月基金签到小
	E_LoginReward_MonthFundSmall E_LoginReward = 2
	// 月基金签到大
	E_LoginReward_MonthFundBig E_LoginReward = 3
)

var E_LoginReward_name = map[int]string{
	1: "E_LoginReward_DailyLogin",
	2: "E_LoginReward_MonthFundSmall",
	3: "E_LoginReward_MonthFundBig",
}

var E_LoginReward_value = map[string]int{
	"E_LoginReward_DailyLogin":     1,
	"E_LoginReward_MonthFundSmall": 2,
	"E_LoginReward_MonthFundBig":   3,
}

var E_LoginReward_Slice = []int{
	1,
	2,
	3,
}

func (x E_LoginReward) String() string {
	if name, ok := E_LoginReward_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_LoginReward_Size() int {
	return len(E_LoginReward_Slice)
}

func Check_E_LoginReward_I(value int) bool {
	if _, ok := E_LoginReward_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_LoginReward(value E_LoginReward) bool {
	return Check_E_LoginReward_I(int(value))
}

func Each_E_LoginReward(f func(E_LoginReward) (continued bool)) {
	for _, value := range E_LoginReward_Slice {
		if !f(E_LoginReward(value)) {
			break
		}
	}
}

func Each_E_LoginReward_I(f func(int) (continued bool)) {
	for _, value := range E_LoginReward_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_LoginReward] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_RandomNameType] begin

// 随机名类型
type E_RandomNameType int

const (
	E_RandomNameType_Null E_RandomNameType = 0

	// 前缀
	E_RandomNameType_Prefix E_RandomNameType = 1
	// 姓
	E_RandomNameType_Surname E_RandomNameType = 2
	// 男名
	E_RandomNameType_MaleName E_RandomNameType = 3
	// 女名
	E_RandomNameType_FemaleName E_RandomNameType = 4
)

var E_RandomNameType_name = map[int]string{
	1: "E_RandomNameType_Prefix",
	2: "E_RandomNameType_Surname",
	3: "E_RandomNameType_MaleName",
	4: "E_RandomNameType_FemaleName",
}

var E_RandomNameType_value = map[string]int{
	"E_RandomNameType_Prefix":     1,
	"E_RandomNameType_Surname":    2,
	"E_RandomNameType_MaleName":   3,
	"E_RandomNameType_FemaleName": 4,
}

var E_RandomNameType_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_RandomNameType) String() string {
	if name, ok := E_RandomNameType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_RandomNameType_Size() int {
	return len(E_RandomNameType_Slice)
}

func Check_E_RandomNameType_I(value int) bool {
	if _, ok := E_RandomNameType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_RandomNameType(value E_RandomNameType) bool {
	return Check_E_RandomNameType_I(int(value))
}

func Each_E_RandomNameType(f func(E_RandomNameType) (continued bool)) {
	for _, value := range E_RandomNameType_Slice {
		if !f(E_RandomNameType(value)) {
			break
		}
	}
}

func Each_E_RandomNameType_I(f func(int) (continued bool)) {
	for _, value := range E_RandomNameType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_RandomNameType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_Platform] begin

// 平台
type E_Platform int

const (
	E_Platform_Null E_Platform = 0

	// 微信小游戏
	E_Platform_WeChatMiniGame E_Platform = 1
)

var E_Platform_name = map[int]string{
	1: "E_Platform_WeChatMiniGame",
}

var E_Platform_value = map[string]int{
	"E_Platform_WeChatMiniGame": 1,
}

var E_Platform_Slice = []int{
	1,
}

func (x E_Platform) String() string {
	if name, ok := E_Platform_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_Platform_Size() int {
	return len(E_Platform_Slice)
}

func Check_E_Platform_I(value int) bool {
	if _, ok := E_Platform_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_Platform(value E_Platform) bool {
	return Check_E_Platform_I(int(value))
}

func Each_E_Platform(f func(E_Platform) (continued bool)) {
	for _, value := range E_Platform_Slice {
		if !f(E_Platform(value)) {
			break
		}
	}
}

func Each_E_Platform_I(f func(int) (continued bool)) {
	for _, value := range E_Platform_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_Platform] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_WeChatMiniGameSenseValue] begin

// 微信小游戏场景值
type E_WeChatMiniGameSenseValue int

const (
	E_WeChatMiniGameSenseValue_Null E_WeChatMiniGameSenseValue = 0

	// 发现栏小程序主入口，「最近使用」列表（基础库2.2.4版本起包含「我的小程序」列表）
	E_WeChatMiniGameSenseValue_DiscoveryBarMiniAppMainRecent E_WeChatMiniGameSenseValue = 1001
	// 微信首页顶部搜索框的搜索结果页
	E_WeChatMiniGameSenseValue_WeChat1005 E_WeChatMiniGameSenseValue = 1005
	// 发现栏小程序主入口搜索框的搜索结果页
	E_WeChatMiniGameSenseValue_WeChat1006 E_WeChatMiniGameSenseValue = 1006
	// 单人聊天会话中的小程序消息卡片
	E_WeChatMiniGameSenseValue_WeChat1007 E_WeChatMiniGameSenseValue = 1007
	// 群聊会话中的小程序消息卡片
	E_WeChatMiniGameSenseValue_WeChat1008 E_WeChatMiniGameSenseValue = 1008
	// 扫描二维码
	E_WeChatMiniGameSenseValue_WeChat1011 E_WeChatMiniGameSenseValue = 1011
	// 长按图片识别二维码
	E_WeChatMiniGameSenseValue_WeChat1012 E_WeChatMiniGameSenseValue = 1012
	// 扫描手机相册中选取的二维码
	E_WeChatMiniGameSenseValue_WeChat1013 E_WeChatMiniGameSenseValue = 1013
	// 小程序模板消息
	E_WeChatMiniGameSenseValue_WeChat1014 E_WeChatMiniGameSenseValue = 1014
	// 前往小程序体验版的入口页
	E_WeChatMiniGameSenseValue_WeChat1017 E_WeChatMiniGameSenseValue = 1017
	// 微信钱包（微信客户端7.0.0版本改为支付入口）
	E_WeChatMiniGameSenseValue_WeChat1019 E_WeChatMiniGameSenseValue = 1019
	// 公众号 profile 页相关小程序列表
	E_WeChatMiniGameSenseValue_WeChat1020 E_WeChatMiniGameSenseValue = 1020
	// 聊天顶部置顶小程序入口（微信客户端6.6.1版本起废弃）
	E_WeChatMiniGameSenseValue_WeChat1022 E_WeChatMiniGameSenseValue = 1022
	// 安卓系统桌面图标
	E_WeChatMiniGameSenseValue_AndroidDesktopIcon E_WeChatMiniGameSenseValue = 1023
	// 小程序 profile 页
	E_WeChatMiniGameSenseValue_WeChat1024 E_WeChatMiniGameSenseValue = 1024
	// 扫描一维码
	E_WeChatMiniGameSenseValue_WeChat1025 E_WeChatMiniGameSenseValue = 1025
	// 发现栏小程序主入口，「附近的小程序」列表
	E_WeChatMiniGameSenseValue_WeChat1026 E_WeChatMiniGameSenseValue = 1026
	// 微信首页顶部搜索框搜索结果页「使用过的小程序」列表
	E_WeChatMiniGameSenseValue_WeChat1027 E_WeChatMiniGameSenseValue = 1027
	// 我的卡包
	E_WeChatMiniGameSenseValue_WeChat1028 E_WeChatMiniGameSenseValue = 1028
	// 小程序中的卡券详情页
	E_WeChatMiniGameSenseValue_WeChat1029 E_WeChatMiniGameSenseValue = 1029
	// 自动化测试下打开小程序
	E_WeChatMiniGameSenseValue_WeChat1030 E_WeChatMiniGameSenseValue = 1030
	// 长按图片识别一维码
	E_WeChatMiniGameSenseValue_WeChat1031 E_WeChatMiniGameSenseValue = 1031
	// 扫描手机相册中选取的一维码
	E_WeChatMiniGameSenseValue_WeChat1032 E_WeChatMiniGameSenseValue = 1032
	// 微信支付完成页
	E_WeChatMiniGameSenseValue_WeChat1034 E_WeChatMiniGameSenseValue = 1034
	// 公众号自定义菜单
	E_WeChatMiniGameSenseValue_OfficialAccountCustomMenu E_WeChatMiniGameSenseValue = 1035
	// App 分享消息卡片
	E_WeChatMiniGameSenseValue_WeChat1036 E_WeChatMiniGameSenseValue = 1036
	// 小程序打开小程序
	E_WeChatMiniGameSenseValue_WeChat1037 E_WeChatMiniGameSenseValue = 1037
	// 从另一个小程序返回
	E_WeChatMiniGameSenseValue_WeChat1038 E_WeChatMiniGameSenseValue = 1038
	// 摇电视
	E_WeChatMiniGameSenseValue_WeChat1039 E_WeChatMiniGameSenseValue = 1039
	// 添加好友搜索框的搜索结果页
	E_WeChatMiniGameSenseValue_WeChat1042 E_WeChatMiniGameSenseValue = 1042
	// 公众号模板消息
	E_WeChatMiniGameSenseValue_WeChat1043 E_WeChatMiniGameSenseValue = 1043
	// 带 shareTicket 的小程序消息卡片
	E_WeChatMiniGameSenseValue_WeChat1044 E_WeChatMiniGameSenseValue = 1044
	// 朋友圈广告
	E_WeChatMiniGameSenseValue_WeChat1045 E_WeChatMiniGameSenseValue = 1045
	// 朋友圈广告详情页
	E_WeChatMiniGameSenseValue_WeChat1046 E_WeChatMiniGameSenseValue = 1046
	// 扫描小程序码
	E_WeChatMiniGameSenseValue_WeChat1047 E_WeChatMiniGameSenseValue = 1047
	// 长按图片识别小程序码
	E_WeChatMiniGameSenseValue_WeChat1048 E_WeChatMiniGameSenseValue = 1048
	// 扫描手机相册中选取的小程序码
	E_WeChatMiniGameSenseValue_WeChat1049 E_WeChatMiniGameSenseValue = 1049
	// 卡券的适用门店列表
	E_WeChatMiniGameSenseValue_WeChat1052 E_WeChatMiniGameSenseValue = 1052
	// 搜一搜的结果页
	E_WeChatMiniGameSenseValue_WeChat1053 E_WeChatMiniGameSenseValue = 1053
	// 顶部搜索框小程序快捷入口（微信客户端版本6.7.4起废弃）
	E_WeChatMiniGameSenseValue_WeChat1054 E_WeChatMiniGameSenseValue = 1054
	// 聊天顶部音乐播放器右上角菜单
	E_WeChatMiniGameSenseValue_WeChat1056 E_WeChatMiniGameSenseValue = 1056
	// 钱包中的银行卡详情页
	E_WeChatMiniGameSenseValue_WeChat1057 E_WeChatMiniGameSenseValue = 1057
	// 公众号文章
	E_WeChatMiniGameSenseValue_OfficialAccountArticle E_WeChatMiniGameSenseValue = 1058
	// 体验版小程序绑定邀请页
	E_WeChatMiniGameSenseValue_WeChat1059 E_WeChatMiniGameSenseValue = 1059
	// 微信首页连Wi-Fi状态栏
	E_WeChatMiniGameSenseValue_WeChat1064 E_WeChatMiniGameSenseValue = 1064
	// 公众号文章广告
	E_WeChatMiniGameSenseValue_OfficialAccountArticleAD E_WeChatMiniGameSenseValue = 1067
	// 附近小程序列表广告（已废弃）
	E_WeChatMiniGameSenseValue_WeChat1068 E_WeChatMiniGameSenseValue = 1068
	// 移动应用
	E_WeChatMiniGameSenseValue_WeChat1069 E_WeChatMiniGameSenseValue = 1069
	// 钱包中的银行卡列表页
	E_WeChatMiniGameSenseValue_WeChat1071 E_WeChatMiniGameSenseValue = 1071
	// 二维码收款页面
	E_WeChatMiniGameSenseValue_WeChat1072 E_WeChatMiniGameSenseValue = 1072
	// 客服消息列表下发的小程序消息卡片
	E_WeChatMiniGameSenseValue_WeChat1073 E_WeChatMiniGameSenseValue = 1073
	// 公众号会话下发的小程序消息卡片
	E_WeChatMiniGameSenseValue_OfficialAccountSessionMiniApp E_WeChatMiniGameSenseValue = 1074
	// 摇周边
	E_WeChatMiniGameSenseValue_WeChat1077 E_WeChatMiniGameSenseValue = 1077
	// 微信连Wi-Fi成功提示页
	E_WeChatMiniGameSenseValue_WeChat1078 E_WeChatMiniGameSenseValue = 1078
	// 微信游戏中心
	E_WeChatMiniGameSenseValue_WeChat1079 E_WeChatMiniGameSenseValue = 1079
	// 客服消息下发的文字链
	E_WeChatMiniGameSenseValue_WeChat1081 E_WeChatMiniGameSenseValue = 1081
	// 公众号会话下发的文字链
	E_WeChatMiniGameSenseValue_WeChat1082 E_WeChatMiniGameSenseValue = 1082
	// 朋友圈广告原生页
	E_WeChatMiniGameSenseValue_WeChat1084 E_WeChatMiniGameSenseValue = 1084
	// 微信聊天主界面下拉，「最近使用」栏（基础库2.2.4版本起包含「我的小程序」栏）
	E_WeChatMiniGameSenseValue_ChatMainDropDownRecent E_WeChatMiniGameSenseValue = 1089
	// 长按小程序右上角菜单唤出最近使用历史
	E_WeChatMiniGameSenseValue_WeChat1090 E_WeChatMiniGameSenseValue = 1090
	// 公众号文章商品卡片
	E_WeChatMiniGameSenseValue_WeChat1091 E_WeChatMiniGameSenseValue = 1091
	// 城市服务入口
	E_WeChatMiniGameSenseValue_WeChat1092 E_WeChatMiniGameSenseValue = 1092
	// 小程序广告组件
	E_WeChatMiniGameSenseValue_WeChat1095 E_WeChatMiniGameSenseValue = 1095
	// 聊天记录
	E_WeChatMiniGameSenseValue_WeChat1096 E_WeChatMiniGameSenseValue = 1096
	// 微信支付签约页
	E_WeChatMiniGameSenseValue_WeChat1097 E_WeChatMiniGameSenseValue = 1097
	// 页面内嵌插件
	E_WeChatMiniGameSenseValue_WeChat1099 E_WeChatMiniGameSenseValue = 1099
	// 公众号 profile 页服务预览
	E_WeChatMiniGameSenseValue_WeChat1102 E_WeChatMiniGameSenseValue = 1102
	// 发现栏小程序主入口，「我的小程序」列表（基础库2.2.4版本起废弃）
	E_WeChatMiniGameSenseValue_DiscoveryBarMiniAppMain E_WeChatMiniGameSenseValue = 1103
	// 微信聊天主界面下拉，「我的小程序」栏（基础库2.2.4版本起废弃）
	E_WeChatMiniGameSenseValue_ChatMainDropDown E_WeChatMiniGameSenseValue = 1104
	// 微信爬虫访问
	E_WeChatMiniGameSenseValue_WeChat1129 E_WeChatMiniGameSenseValue = 1129
)

var E_WeChatMiniGameSenseValue_name = map[int]string{
	1001: "E_WeChatMiniGameSenseValue_DiscoveryBarMiniAppMainRecent",
	1005: "E_WeChatMiniGameSenseValue_WeChat1005",
	1006: "E_WeChatMiniGameSenseValue_WeChat1006",
	1007: "E_WeChatMiniGameSenseValue_WeChat1007",
	1008: "E_WeChatMiniGameSenseValue_WeChat1008",
	1011: "E_WeChatMiniGameSenseValue_WeChat1011",
	1012: "E_WeChatMiniGameSenseValue_WeChat1012",
	1013: "E_WeChatMiniGameSenseValue_WeChat1013",
	1014: "E_WeChatMiniGameSenseValue_WeChat1014",
	1017: "E_WeChatMiniGameSenseValue_WeChat1017",
	1019: "E_WeChatMiniGameSenseValue_WeChat1019",
	1020: "E_WeChatMiniGameSenseValue_WeChat1020",
	1022: "E_WeChatMiniGameSenseValue_WeChat1022",
	1023: "E_WeChatMiniGameSenseValue_AndroidDesktopIcon",
	1024: "E_WeChatMiniGameSenseValue_WeChat1024",
	1025: "E_WeChatMiniGameSenseValue_WeChat1025",
	1026: "E_WeChatMiniGameSenseValue_WeChat1026",
	1027: "E_WeChatMiniGameSenseValue_WeChat1027",
	1028: "E_WeChatMiniGameSenseValue_WeChat1028",
	1029: "E_WeChatMiniGameSenseValue_WeChat1029",
	1030: "E_WeChatMiniGameSenseValue_WeChat1030",
	1031: "E_WeChatMiniGameSenseValue_WeChat1031",
	1032: "E_WeChatMiniGameSenseValue_WeChat1032",
	1034: "E_WeChatMiniGameSenseValue_WeChat1034",
	1035: "E_WeChatMiniGameSenseValue_OfficialAccountCustomMenu",
	1036: "E_WeChatMiniGameSenseValue_WeChat1036",
	1037: "E_WeChatMiniGameSenseValue_WeChat1037",
	1038: "E_WeChatMiniGameSenseValue_WeChat1038",
	1039: "E_WeChatMiniGameSenseValue_WeChat1039",
	1042: "E_WeChatMiniGameSenseValue_WeChat1042",
	1043: "E_WeChatMiniGameSenseValue_WeChat1043",
	1044: "E_WeChatMiniGameSenseValue_WeChat1044",
	1045: "E_WeChatMiniGameSenseValue_WeChat1045",
	1046: "E_WeChatMiniGameSenseValue_WeChat1046",
	1047: "E_WeChatMiniGameSenseValue_WeChat1047",
	1048: "E_WeChatMiniGameSenseValue_WeChat1048",
	1049: "E_WeChatMiniGameSenseValue_WeChat1049",
	1052: "E_WeChatMiniGameSenseValue_WeChat1052",
	1053: "E_WeChatMiniGameSenseValue_WeChat1053",
	1054: "E_WeChatMiniGameSenseValue_WeChat1054",
	1056: "E_WeChatMiniGameSenseValue_WeChat1056",
	1057: "E_WeChatMiniGameSenseValue_WeChat1057",
	1058: "E_WeChatMiniGameSenseValue_OfficialAccountArticle",
	1059: "E_WeChatMiniGameSenseValue_WeChat1059",
	1064: "E_WeChatMiniGameSenseValue_WeChat1064",
	1067: "E_WeChatMiniGameSenseValue_OfficialAccountArticleAD",
	1068: "E_WeChatMiniGameSenseValue_WeChat1068",
	1069: "E_WeChatMiniGameSenseValue_WeChat1069",
	1071: "E_WeChatMiniGameSenseValue_WeChat1071",
	1072: "E_WeChatMiniGameSenseValue_WeChat1072",
	1073: "E_WeChatMiniGameSenseValue_WeChat1073",
	1074: "E_WeChatMiniGameSenseValue_OfficialAccountSessionMiniApp",
	1077: "E_WeChatMiniGameSenseValue_WeChat1077",
	1078: "E_WeChatMiniGameSenseValue_WeChat1078",
	1079: "E_WeChatMiniGameSenseValue_WeChat1079",
	1081: "E_WeChatMiniGameSenseValue_WeChat1081",
	1082: "E_WeChatMiniGameSenseValue_WeChat1082",
	1084: "E_WeChatMiniGameSenseValue_WeChat1084",
	1089: "E_WeChatMiniGameSenseValue_ChatMainDropDownRecent",
	1090: "E_WeChatMiniGameSenseValue_WeChat1090",
	1091: "E_WeChatMiniGameSenseValue_WeChat1091",
	1092: "E_WeChatMiniGameSenseValue_WeChat1092",
	1095: "E_WeChatMiniGameSenseValue_WeChat1095",
	1096: "E_WeChatMiniGameSenseValue_WeChat1096",
	1097: "E_WeChatMiniGameSenseValue_WeChat1097",
	1099: "E_WeChatMiniGameSenseValue_WeChat1099",
	1102: "E_WeChatMiniGameSenseValue_WeChat1102",
	1103: "E_WeChatMiniGameSenseValue_DiscoveryBarMiniAppMain",
	1104: "E_WeChatMiniGameSenseValue_ChatMainDropDown",
	1129: "E_WeChatMiniGameSenseValue_WeChat1129",
}

var E_WeChatMiniGameSenseValue_value = map[string]int{
	"E_WeChatMiniGameSenseValue_DiscoveryBarMiniAppMainRecent": 1001,
	"E_WeChatMiniGameSenseValue_WeChat1005":                    1005,
	"E_WeChatMiniGameSenseValue_WeChat1006":                    1006,
	"E_WeChatMiniGameSenseValue_WeChat1007":                    1007,
	"E_WeChatMiniGameSenseValue_WeChat1008":                    1008,
	"E_WeChatMiniGameSenseValue_WeChat1011":                    1011,
	"E_WeChatMiniGameSenseValue_WeChat1012":                    1012,
	"E_WeChatMiniGameSenseValue_WeChat1013":                    1013,
	"E_WeChatMiniGameSenseValue_WeChat1014":                    1014,
	"E_WeChatMiniGameSenseValue_WeChat1017":                    1017,
	"E_WeChatMiniGameSenseValue_WeChat1019":                    1019,
	"E_WeChatMiniGameSenseValue_WeChat1020":                    1020,
	"E_WeChatMiniGameSenseValue_WeChat1022":                    1022,
	"E_WeChatMiniGameSenseValue_AndroidDesktopIcon":            1023,
	"E_WeChatMiniGameSenseValue_WeChat1024":                    1024,
	"E_WeChatMiniGameSenseValue_WeChat1025":                    1025,
	"E_WeChatMiniGameSenseValue_WeChat1026":                    1026,
	"E_WeChatMiniGameSenseValue_WeChat1027":                    1027,
	"E_WeChatMiniGameSenseValue_WeChat1028":                    1028,
	"E_WeChatMiniGameSenseValue_WeChat1029":                    1029,
	"E_WeChatMiniGameSenseValue_WeChat1030":                    1030,
	"E_WeChatMiniGameSenseValue_WeChat1031":                    1031,
	"E_WeChatMiniGameSenseValue_WeChat1032":                    1032,
	"E_WeChatMiniGameSenseValue_WeChat1034":                    1034,
	"E_WeChatMiniGameSenseValue_OfficialAccountCustomMenu":     1035,
	"E_WeChatMiniGameSenseValue_WeChat1036":                    1036,
	"E_WeChatMiniGameSenseValue_WeChat1037":                    1037,
	"E_WeChatMiniGameSenseValue_WeChat1038":                    1038,
	"E_WeChatMiniGameSenseValue_WeChat1039":                    1039,
	"E_WeChatMiniGameSenseValue_WeChat1042":                    1042,
	"E_WeChatMiniGameSenseValue_WeChat1043":                    1043,
	"E_WeChatMiniGameSenseValue_WeChat1044":                    1044,
	"E_WeChatMiniGameSenseValue_WeChat1045":                    1045,
	"E_WeChatMiniGameSenseValue_WeChat1046":                    1046,
	"E_WeChatMiniGameSenseValue_WeChat1047":                    1047,
	"E_WeChatMiniGameSenseValue_WeChat1048":                    1048,
	"E_WeChatMiniGameSenseValue_WeChat1049":                    1049,
	"E_WeChatMiniGameSenseValue_WeChat1052":                    1052,
	"E_WeChatMiniGameSenseValue_WeChat1053":                    1053,
	"E_WeChatMiniGameSenseValue_WeChat1054":                    1054,
	"E_WeChatMiniGameSenseValue_WeChat1056":                    1056,
	"E_WeChatMiniGameSenseValue_WeChat1057":                    1057,
	"E_WeChatMiniGameSenseValue_OfficialAccountArticle":        1058,
	"E_WeChatMiniGameSenseValue_WeChat1059":                    1059,
	"E_WeChatMiniGameSenseValue_WeChat1064":                    1064,
	"E_WeChatMiniGameSenseValue_OfficialAccountArticleAD":      1067,
	"E_WeChatMiniGameSenseValue_WeChat1068":                    1068,
	"E_WeChatMiniGameSenseValue_WeChat1069":                    1069,
	"E_WeChatMiniGameSenseValue_WeChat1071":                    1071,
	"E_WeChatMiniGameSenseValue_WeChat1072":                    1072,
	"E_WeChatMiniGameSenseValue_WeChat1073":                    1073,
	"E_WeChatMiniGameSenseValue_OfficialAccountSessionMiniApp": 1074,
	"E_WeChatMiniGameSenseValue_WeChat1077":                    1077,
	"E_WeChatMiniGameSenseValue_WeChat1078":                    1078,
	"E_WeChatMiniGameSenseValue_WeChat1079":                    1079,
	"E_WeChatMiniGameSenseValue_WeChat1081":                    1081,
	"E_WeChatMiniGameSenseValue_WeChat1082":                    1082,
	"E_WeChatMiniGameSenseValue_WeChat1084":                    1084,
	"E_WeChatMiniGameSenseValue_ChatMainDropDownRecent":        1089,
	"E_WeChatMiniGameSenseValue_WeChat1090":                    1090,
	"E_WeChatMiniGameSenseValue_WeChat1091":                    1091,
	"E_WeChatMiniGameSenseValue_WeChat1092":                    1092,
	"E_WeChatMiniGameSenseValue_WeChat1095":                    1095,
	"E_WeChatMiniGameSenseValue_WeChat1096":                    1096,
	"E_WeChatMiniGameSenseValue_WeChat1097":                    1097,
	"E_WeChatMiniGameSenseValue_WeChat1099":                    1099,
	"E_WeChatMiniGameSenseValue_WeChat1102":                    1102,
	"E_WeChatMiniGameSenseValue_DiscoveryBarMiniAppMain":       1103,
	"E_WeChatMiniGameSenseValue_ChatMainDropDown":              1104,
	"E_WeChatMiniGameSenseValue_WeChat1129":                    1129,
}

var E_WeChatMiniGameSenseValue_Slice = []int{
	1001,
	1005,
	1006,
	1007,
	1008,
	1011,
	1012,
	1013,
	1014,
	1017,
	1019,
	1020,
	1022,
	1023,
	1024,
	1025,
	1026,
	1027,
	1028,
	1029,
	1030,
	1031,
	1032,
	1034,
	1035,
	1036,
	1037,
	1038,
	1039,
	1042,
	1043,
	1044,
	1045,
	1046,
	1047,
	1048,
	1049,
	1052,
	1053,
	1054,
	1056,
	1057,
	1058,
	1059,
	1064,
	1067,
	1068,
	1069,
	1071,
	1072,
	1073,
	1074,
	1077,
	1078,
	1079,
	1081,
	1082,
	1084,
	1089,
	1090,
	1091,
	1092,
	1095,
	1096,
	1097,
	1099,
	1102,
	1103,
	1104,
	1129,
}

func (x E_WeChatMiniGameSenseValue) String() string {
	if name, ok := E_WeChatMiniGameSenseValue_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_WeChatMiniGameSenseValue_Size() int {
	return len(E_WeChatMiniGameSenseValue_Slice)
}

func Check_E_WeChatMiniGameSenseValue_I(value int) bool {
	if _, ok := E_WeChatMiniGameSenseValue_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_WeChatMiniGameSenseValue(value E_WeChatMiniGameSenseValue) bool {
	return Check_E_WeChatMiniGameSenseValue_I(int(value))
}

func Each_E_WeChatMiniGameSenseValue(f func(E_WeChatMiniGameSenseValue) (continued bool)) {
	for _, value := range E_WeChatMiniGameSenseValue_Slice {
		if !f(E_WeChatMiniGameSenseValue(value)) {
			break
		}
	}
}

func Each_E_WeChatMiniGameSenseValue_I(f func(int) (continued bool)) {
	for _, value := range E_WeChatMiniGameSenseValue_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_WeChatMiniGameSenseValue] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_WeChatSubscribeFunction] begin

// 微信订阅的功能
type E_WeChatSubscribeFunction int

const (
	E_WeChatSubscribeFunction_Null E_WeChatSubscribeFunction = 0

	// 挂机收益
	E_WeChatSubscribeFunction_Idle E_WeChatSubscribeFunction = 1001
	// 首充
	E_WeChatSubscribeFunction_FirstCharge E_WeChatSubscribeFunction = 1002
	// 公告
	E_WeChatSubscribeFunction_Notice E_WeChatSubscribeFunction = 1003
)

var E_WeChatSubscribeFunction_name = map[int]string{
	1001: "E_WeChatSubscribeFunction_Idle",
	1002: "E_WeChatSubscribeFunction_FirstCharge",
	1003: "E_WeChatSubscribeFunction_Notice",
}

var E_WeChatSubscribeFunction_value = map[string]int{
	"E_WeChatSubscribeFunction_Idle":        1001,
	"E_WeChatSubscribeFunction_FirstCharge": 1002,
	"E_WeChatSubscribeFunction_Notice":      1003,
}

var E_WeChatSubscribeFunction_Slice = []int{
	1001,
	1002,
	1003,
}

func (x E_WeChatSubscribeFunction) String() string {
	if name, ok := E_WeChatSubscribeFunction_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_WeChatSubscribeFunction_Size() int {
	return len(E_WeChatSubscribeFunction_Slice)
}

func Check_E_WeChatSubscribeFunction_I(value int) bool {
	if _, ok := E_WeChatSubscribeFunction_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_WeChatSubscribeFunction(value E_WeChatSubscribeFunction) bool {
	return Check_E_WeChatSubscribeFunction_I(int(value))
}

func Each_E_WeChatSubscribeFunction(f func(E_WeChatSubscribeFunction) (continued bool)) {
	for _, value := range E_WeChatSubscribeFunction_Slice {
		if !f(E_WeChatSubscribeFunction(value)) {
			break
		}
	}
}

func Each_E_WeChatSubscribeFunction_I(f func(int) (continued bool)) {
	for _, value := range E_WeChatSubscribeFunction_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_WeChatSubscribeFunction] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_WeChatSubscribeResult] begin

// 微信订阅的结果
type E_WeChatSubscribeResult int

const (
	E_WeChatSubscribeResult_Null E_WeChatSubscribeResult = 0

	// 同意
	E_WeChatSubscribeResult_Accept E_WeChatSubscribeResult = 1
	// 拒绝
	E_WeChatSubscribeResult_Reject E_WeChatSubscribeResult = 2
	// 被后台封禁
	E_WeChatSubscribeResult_Ban E_WeChatSubscribeResult = 3
)

var E_WeChatSubscribeResult_name = map[int]string{
	1: "E_WeChatSubscribeResult_Accept",
	2: "E_WeChatSubscribeResult_Reject",
	3: "E_WeChatSubscribeResult_Ban",
}

var E_WeChatSubscribeResult_value = map[string]int{
	"E_WeChatSubscribeResult_Accept": 1,
	"E_WeChatSubscribeResult_Reject": 2,
	"E_WeChatSubscribeResult_Ban":    3,
}

var E_WeChatSubscribeResult_Slice = []int{
	1,
	2,
	3,
}

func (x E_WeChatSubscribeResult) String() string {
	if name, ok := E_WeChatSubscribeResult_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_WeChatSubscribeResult_Size() int {
	return len(E_WeChatSubscribeResult_Slice)
}

func Check_E_WeChatSubscribeResult_I(value int) bool {
	if _, ok := E_WeChatSubscribeResult_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_WeChatSubscribeResult(value E_WeChatSubscribeResult) bool {
	return Check_E_WeChatSubscribeResult_I(int(value))
}

func Each_E_WeChatSubscribeResult(f func(E_WeChatSubscribeResult) (continued bool)) {
	for _, value := range E_WeChatSubscribeResult_Slice {
		if !f(E_WeChatSubscribeResult(value)) {
			break
		}
	}
}

func Each_E_WeChatSubscribeResult_I(f func(int) (continued bool)) {
	for _, value := range E_WeChatSubscribeResult_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_WeChatSubscribeResult] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_BAG_TYPE] begin

// 充值商品类型
type E_BAG_TYPE int

const (
	E_BAG_TYPE_Null E_BAG_TYPE = 0

	// 普通道具
	E_BAG_TYPE_ComItem E_BAG_TYPE = 1
	// 装备
	E_BAG_TYPE_Equip E_BAG_TYPE = 2
	// 兵书
	E_BAG_TYPE_Warcraft E_BAG_TYPE = 3
	// 武将碎片
	E_BAG_TYPE_HeroDebris E_BAG_TYPE = 4
	// 全部
	E_BAG_TYPE_All E_BAG_TYPE = 5
)

var E_BAG_TYPE_name = map[int]string{
	1: "E_BAG_TYPE_ComItem",
	2: "E_BAG_TYPE_Equip",
	3: "E_BAG_TYPE_Warcraft",
	4: "E_BAG_TYPE_HeroDebris",
	5: "E_BAG_TYPE_All",
}

var E_BAG_TYPE_value = map[string]int{
	"E_BAG_TYPE_ComItem":    1,
	"E_BAG_TYPE_Equip":      2,
	"E_BAG_TYPE_Warcraft":   3,
	"E_BAG_TYPE_HeroDebris": 4,
	"E_BAG_TYPE_All":        5,
}

var E_BAG_TYPE_Slice = []int{
	1,
	2,
	3,
	4,
	5,
}

func (x E_BAG_TYPE) String() string {
	if name, ok := E_BAG_TYPE_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_BAG_TYPE_Size() int {
	return len(E_BAG_TYPE_Slice)
}

func Check_E_BAG_TYPE_I(value int) bool {
	if _, ok := E_BAG_TYPE_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_BAG_TYPE(value E_BAG_TYPE) bool {
	return Check_E_BAG_TYPE_I(int(value))
}

func Each_E_BAG_TYPE(f func(E_BAG_TYPE) (continued bool)) {
	for _, value := range E_BAG_TYPE_Slice {
		if !f(E_BAG_TYPE(value)) {
			break
		}
	}
}

func Each_E_BAG_TYPE_I(f func(int) (continued bool)) {
	for _, value := range E_BAG_TYPE_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_BAG_TYPE] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_FunctionSwitch] begin

// 游戏功能开关类型
type E_FunctionSwitch int

const (
	E_FunctionSwitch_Null E_FunctionSwitch = 0

	// 屏蔽充值
	E_FunctionSwitch_RechargeSwitch E_FunctionSwitch = 1
	// 分享诱导
	E_FunctionSwitch_ShareSwitch E_FunctionSwitch = 2
	// 客服充值
	E_FunctionSwitch_CustomerRechargeSwitch E_FunctionSwitch = 3
)

var E_FunctionSwitch_name = map[int]string{
	1: "E_FunctionSwitch_RechargeSwitch",
	2: "E_FunctionSwitch_ShareSwitch",
	3: "E_FunctionSwitch_CustomerRechargeSwitch",
}

var E_FunctionSwitch_value = map[string]int{
	"E_FunctionSwitch_RechargeSwitch":         1,
	"E_FunctionSwitch_ShareSwitch":            2,
	"E_FunctionSwitch_CustomerRechargeSwitch": 3,
}

var E_FunctionSwitch_Slice = []int{
	1,
	2,
	3,
}

func (x E_FunctionSwitch) String() string {
	if name, ok := E_FunctionSwitch_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_FunctionSwitch_Size() int {
	return len(E_FunctionSwitch_Slice)
}

func Check_E_FunctionSwitch_I(value int) bool {
	if _, ok := E_FunctionSwitch_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_FunctionSwitch(value E_FunctionSwitch) bool {
	return Check_E_FunctionSwitch_I(int(value))
}

func Each_E_FunctionSwitch(f func(E_FunctionSwitch) (continued bool)) {
	for _, value := range E_FunctionSwitch_Slice {
		if !f(E_FunctionSwitch(value)) {
			break
		}
	}
}

func Each_E_FunctionSwitch_I(f func(int) (continued bool)) {
	for _, value := range E_FunctionSwitch_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_FunctionSwitch] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_FunctionSwitchValue] begin

// 功能开关值
type E_FunctionSwitchValue int

const (
	E_FunctionSwitchValue_Null E_FunctionSwitchValue = 0

	// 微信安卓屏蔽
	E_FunctionSwitchValue_WeChatAndroID E_FunctionSwitchValue = 1
	// 微信ios屏蔽
	E_FunctionSwitchValue_WeChatIOS E_FunctionSwitchValue = 2
	// 微信安卓和ios都屏蔽
	E_FunctionSwitchValue_WeChatAndroidIOS E_FunctionSwitchValue = 3
)

var E_FunctionSwitchValue_name = map[int]string{
	1: "E_FunctionSwitchValue_WeChatAndroID",
	2: "E_FunctionSwitchValue_WeChatIOS",
	3: "E_FunctionSwitchValue_WeChatAndroidIOS",
}

var E_FunctionSwitchValue_value = map[string]int{
	"E_FunctionSwitchValue_WeChatAndroID":    1,
	"E_FunctionSwitchValue_WeChatIOS":        2,
	"E_FunctionSwitchValue_WeChatAndroidIOS": 3,
}

var E_FunctionSwitchValue_Slice = []int{
	1,
	2,
	3,
}

func (x E_FunctionSwitchValue) String() string {
	if name, ok := E_FunctionSwitchValue_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_FunctionSwitchValue_Size() int {
	return len(E_FunctionSwitchValue_Slice)
}

func Check_E_FunctionSwitchValue_I(value int) bool {
	if _, ok := E_FunctionSwitchValue_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_FunctionSwitchValue(value E_FunctionSwitchValue) bool {
	return Check_E_FunctionSwitchValue_I(int(value))
}

func Each_E_FunctionSwitchValue(f func(E_FunctionSwitchValue) (continued bool)) {
	for _, value := range E_FunctionSwitchValue_Slice {
		if !f(E_FunctionSwitchValue(value)) {
			break
		}
	}
}

func Each_E_FunctionSwitchValue_I(f func(int) (continued bool)) {
	for _, value := range E_FunctionSwitchValue_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_FunctionSwitchValue] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_RankType] begin

// 段位类型
type E_RankType int

const (
	E_RankType_Null E_RankType = 0

	// 最强王者
	E_RankType_Master E_RankType = 1
	// 传说
	E_RankType_Legend E_RankType = 2
	// 白金
	E_RankType_Platinum E_RankType = 3
	// 黄金
	E_RankType_Gold E_RankType = 4
	// 白银
	E_RankType_Silver E_RankType = 5
	// 青铜
	E_RankType_Bronze E_RankType = 6
	// 新兵
	E_RankType_NewBie E_RankType = 7
)

var E_RankType_name = map[int]string{
	1: "E_RankType_Master",
	2: "E_RankType_Legend",
	3: "E_RankType_Platinum",
	4: "E_RankType_Gold",
	5: "E_RankType_Silver",
	6: "E_RankType_Bronze",
	7: "E_RankType_NewBie",
}

var E_RankType_value = map[string]int{
	"E_RankType_Master":   1,
	"E_RankType_Legend":   2,
	"E_RankType_Platinum": 3,
	"E_RankType_Gold":     4,
	"E_RankType_Silver":   5,
	"E_RankType_Bronze":   6,
	"E_RankType_NewBie":   7,
}

var E_RankType_Slice = []int{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
}

func (x E_RankType) String() string {
	if name, ok := E_RankType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_RankType_Size() int {
	return len(E_RankType_Slice)
}

func Check_E_RankType_I(value int) bool {
	if _, ok := E_RankType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_RankType(value E_RankType) bool {
	return Check_E_RankType_I(int(value))
}

func Each_E_RankType(f func(E_RankType) (continued bool)) {
	for _, value := range E_RankType_Slice {
		if !f(E_RankType(value)) {
			break
		}
	}
}

func Each_E_RankType_I(f func(int) (continued bool)) {
	for _, value := range E_RankType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_RankType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_PushWindowCondition] begin

// 推送弹框触发条件类型
type E_PushWindowCondition int

const (
	E_PushWindowCondition_Null E_PushWindowCondition = 0

	// 通关关卡
	E_PushWindowCondition_WinStage E_PushWindowCondition = 1
	// 当日首次进入游戏
	E_PushWindowCondition_DayFirst E_PushWindowCondition = 2
)

var E_PushWindowCondition_name = map[int]string{
	1: "E_PushWindowCondition_WinStage",
	2: "E_PushWindowCondition_DayFirst",
}

var E_PushWindowCondition_value = map[string]int{
	"E_PushWindowCondition_WinStage": 1,
	"E_PushWindowCondition_DayFirst": 2,
}

var E_PushWindowCondition_Slice = []int{
	1,
	2,
}

func (x E_PushWindowCondition) String() string {
	if name, ok := E_PushWindowCondition_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_PushWindowCondition_Size() int {
	return len(E_PushWindowCondition_Slice)
}

func Check_E_PushWindowCondition_I(value int) bool {
	if _, ok := E_PushWindowCondition_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_PushWindowCondition(value E_PushWindowCondition) bool {
	return Check_E_PushWindowCondition_I(int(value))
}

func Each_E_PushWindowCondition(f func(E_PushWindowCondition) (continued bool)) {
	for _, value := range E_PushWindowCondition_Slice {
		if !f(E_PushWindowCondition(value)) {
			break
		}
	}
}

func Each_E_PushWindowCondition_I(f func(int) (continued bool)) {
	for _, value := range E_PushWindowCondition_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_PushWindowCondition] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_WindowOffType] begin

// 弹出对象的消失方式
type E_WindowOffType int

const (
	E_WindowOffType_Null E_WindowOffType = 0

	// 角色关闭
	E_WindowOffType_RoleClose E_WindowOffType = 1
)

var E_WindowOffType_name = map[int]string{
	1: "E_WindowOffType_RoleClose",
}

var E_WindowOffType_value = map[string]int{
	"E_WindowOffType_RoleClose": 1,
}

var E_WindowOffType_Slice = []int{
	1,
}

func (x E_WindowOffType) String() string {
	if name, ok := E_WindowOffType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_WindowOffType_Size() int {
	return len(E_WindowOffType_Slice)
}

func Check_E_WindowOffType_I(value int) bool {
	if _, ok := E_WindowOffType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_WindowOffType(value E_WindowOffType) bool {
	return Check_E_WindowOffType_I(int(value))
}

func Each_E_WindowOffType(f func(E_WindowOffType) (continued bool)) {
	for _, value := range E_WindowOffType_Slice {
		if !f(E_WindowOffType(value)) {
			break
		}
	}
}

func Each_E_WindowOffType_I(f func(int) (continued bool)) {
	for _, value := range E_WindowOffType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_WindowOffType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_PushWindowName] begin

// 推送的弹框名
type E_PushWindowName int

const (
	E_PushWindowName_Null E_PushWindowName = 0

	// 首充
	E_PushWindowName_FirstCharge E_PushWindowName = 1
	// 开服限时礼包
	E_PushWindowName_OpenServiceGiftPack E_PushWindowName = 2
	// 英雄降临
	E_PushWindowName_HeroAdvent E_PushWindowName = 3
)

var E_PushWindowName_name = map[int]string{
	1: "E_PushWindowName_FirstCharge",
	2: "E_PushWindowName_OpenServiceGiftPack",
	3: "E_PushWindowName_HeroAdvent",
}

var E_PushWindowName_value = map[string]int{
	"E_PushWindowName_FirstCharge":         1,
	"E_PushWindowName_OpenServiceGiftPack": 2,
	"E_PushWindowName_HeroAdvent":          3,
}

var E_PushWindowName_Slice = []int{
	1,
	2,
	3,
}

func (x E_PushWindowName) String() string {
	if name, ok := E_PushWindowName_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_PushWindowName_Size() int {
	return len(E_PushWindowName_Slice)
}

func Check_E_PushWindowName_I(value int) bool {
	if _, ok := E_PushWindowName_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_PushWindowName(value E_PushWindowName) bool {
	return Check_E_PushWindowName_I(int(value))
}

func Each_E_PushWindowName(f func(E_PushWindowName) (continued bool)) {
	for _, value := range E_PushWindowName_Slice {
		if !f(E_PushWindowName(value)) {
			break
		}
	}
}

func Each_E_PushWindowName_I(f func(int) (continued bool)) {
	for _, value := range E_PushWindowName_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_PushWindowName] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_HeroChallengeType] begin

// 过关斩将模式
type E_HeroChallengeType int

const (
	E_HeroChallengeType_Null E_HeroChallengeType = 0

	// 过关斩将单人模式
	E_HeroChallengeType_SingleHero E_HeroChallengeType = 1
	// 过关斩将全体模式
	E_HeroChallengeType_FiveHero E_HeroChallengeType = 2
)

var E_HeroChallengeType_name = map[int]string{
	1: "E_HeroChallengeType_SingleHero",
	2: "E_HeroChallengeType_FiveHero",
}

var E_HeroChallengeType_value = map[string]int{
	"E_HeroChallengeType_SingleHero": 1,
	"E_HeroChallengeType_FiveHero":   2,
}

var E_HeroChallengeType_Slice = []int{
	1,
	2,
}

func (x E_HeroChallengeType) String() string {
	if name, ok := E_HeroChallengeType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_HeroChallengeType_Size() int {
	return len(E_HeroChallengeType_Slice)
}

func Check_E_HeroChallengeType_I(value int) bool {
	if _, ok := E_HeroChallengeType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_HeroChallengeType(value E_HeroChallengeType) bool {
	return Check_E_HeroChallengeType_I(int(value))
}

func Each_E_HeroChallengeType(f func(E_HeroChallengeType) (continued bool)) {
	for _, value := range E_HeroChallengeType_Slice {
		if !f(E_HeroChallengeType(value)) {
			break
		}
	}
}

func Each_E_HeroChallengeType_I(f func(int) (continued bool)) {
	for _, value := range E_HeroChallengeType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_HeroChallengeType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ShareType] begin

// 分享类型枚举
type E_ShareType int

const (
	E_ShareType_Null E_ShareType = 0

	// 分享次数达标即可获得奖励
	E_ShareType_ShareTimes E_ShareType = 1
	// 拉新人数达标获得奖励
	E_ShareType_Invite E_ShareType = 2
	// 拉新人等级数达标获得奖励
	E_ShareType_InviteRank E_ShareType = 3
	// 拉新人数累充达标获得奖励
	E_ShareType_InviteCharge E_ShareType = 4
)

var E_ShareType_name = map[int]string{
	1: "E_ShareType_ShareTimes",
	2: "E_ShareType_Invite",
	3: "E_ShareType_InviteRank",
	4: "E_ShareType_InviteCharge",
}

var E_ShareType_value = map[string]int{
	"E_ShareType_ShareTimes":   1,
	"E_ShareType_Invite":       2,
	"E_ShareType_InviteRank":   3,
	"E_ShareType_InviteCharge": 4,
}

var E_ShareType_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_ShareType) String() string {
	if name, ok := E_ShareType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ShareType_Size() int {
	return len(E_ShareType_Slice)
}

func Check_E_ShareType_I(value int) bool {
	if _, ok := E_ShareType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ShareType(value E_ShareType) bool {
	return Check_E_ShareType_I(int(value))
}

func Each_E_ShareType(f func(E_ShareType) (continued bool)) {
	for _, value := range E_ShareType_Slice {
		if !f(E_ShareType(value)) {
			break
		}
	}
}

func Each_E_ShareType_I(f func(int) (continued bool)) {
	for _, value := range E_ShareType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ShareType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ShareGroup] begin

// 分享组
type E_ShareGroup int

const (
	E_ShareGroup_Null E_ShareGroup = 0

	// 首次分享
	E_ShareGroup_FirstShare E_ShareGroup = 1
	// 主动从功能界面发起分享
	E_ShareGroup_FriendShare E_ShareGroup = 2
	// 微信送元宝界面进行邀请
	E_ShareGroup_WechatGold E_ShareGroup = 3
	// 分享到朋友圈
	E_ShareGroup_ShareTimeLine E_ShareGroup = 4
)

var E_ShareGroup_name = map[int]string{
	1: "E_ShareGroup_FirstShare",
	2: "E_ShareGroup_FriendShare",
	3: "E_ShareGroup_WechatGold",
	4: "E_ShareGroup_ShareTimeLine",
}

var E_ShareGroup_value = map[string]int{
	"E_ShareGroup_FirstShare":    1,
	"E_ShareGroup_FriendShare":   2,
	"E_ShareGroup_WechatGold":    3,
	"E_ShareGroup_ShareTimeLine": 4,
}

var E_ShareGroup_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_ShareGroup) String() string {
	if name, ok := E_ShareGroup_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ShareGroup_Size() int {
	return len(E_ShareGroup_Slice)
}

func Check_E_ShareGroup_I(value int) bool {
	if _, ok := E_ShareGroup_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ShareGroup(value E_ShareGroup) bool {
	return Check_E_ShareGroup_I(int(value))
}

func Each_E_ShareGroup(f func(E_ShareGroup) (continued bool)) {
	for _, value := range E_ShareGroup_Slice {
		if !f(E_ShareGroup(value)) {
			break
		}
	}
}

func Each_E_ShareGroup_I(f func(int) (continued bool)) {
	for _, value := range E_ShareGroup_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ShareGroup] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [E_ShareRefreshType] begin

// 分享刷新类型
type E_ShareRefreshType int

const (
	E_ShareRefreshType_Null E_ShareRefreshType = 0

	// 从不刷新
	E_ShareRefreshType_ShareRefreshNever E_ShareRefreshType = 1
	// 日刷新
	E_ShareRefreshType_ShareRefreshDay E_ShareRefreshType = 2
	// 周刷新
	E_ShareRefreshType_ShareRefreshWeek E_ShareRefreshType = 3
	// 月刷新
	E_ShareRefreshType_ShareRefreshMonth E_ShareRefreshType = 4
)

var E_ShareRefreshType_name = map[int]string{
	1: "E_ShareRefreshType_ShareRefreshNever",
	2: "E_ShareRefreshType_ShareRefreshDay",
	3: "E_ShareRefreshType_ShareRefreshWeek",
	4: "E_ShareRefreshType_ShareRefreshMonth",
}

var E_ShareRefreshType_value = map[string]int{
	"E_ShareRefreshType_ShareRefreshNever": 1,
	"E_ShareRefreshType_ShareRefreshDay":   2,
	"E_ShareRefreshType_ShareRefreshWeek":  3,
	"E_ShareRefreshType_ShareRefreshMonth": 4,
}

var E_ShareRefreshType_Slice = []int{
	1,
	2,
	3,
	4,
}

func (x E_ShareRefreshType) String() string {
	if name, ok := E_ShareRefreshType_name[int(x)]; ok {
		return name
	}
	return ""
}

func E_ShareRefreshType_Size() int {
	return len(E_ShareRefreshType_Slice)
}

func Check_E_ShareRefreshType_I(value int) bool {
	if _, ok := E_ShareRefreshType_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_E_ShareRefreshType(value E_ShareRefreshType) bool {
	return Check_E_ShareRefreshType_I(int(value))
}

func Each_E_ShareRefreshType(f func(E_ShareRefreshType) (continued bool)) {
	for _, value := range E_ShareRefreshType_Slice {
		if !f(E_ShareRefreshType(value)) {
			break
		}
	}
}

func Each_E_ShareRefreshType_I(f func(int) (continued bool)) {
	for _, value := range E_ShareRefreshType_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [E_ShareRefreshType] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加枚举扩展代码
//<Extend>//</Extend>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
