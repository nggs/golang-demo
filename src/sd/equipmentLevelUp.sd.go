// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 装备升级表
type EquipmentLevelUp struct {
	id       int       // ID
	quality  E_Quality // 装备的品质，关联品质表 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+1
	levelMax int       // 此品质可升级到最高等级
	level    int       // 装备等级，0表示此品质初始等级
	exp      int       // 强化到此等级需要的经验，若等级=0时表示此品质0星时提供的经验

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	selfExp  int // 装备本身初始经验值
	totalExp int // 强化到此等级需要的总经验
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewEquipmentLevelUp() *EquipmentLevelUp {
	sd := &EquipmentLevelUp{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// ID
func (sd EquipmentLevelUp) ID() int {
	return sd.id
}

// 装备的品质，关联品质表 1灰色2绿色3蓝色4蓝色+5紫色6紫色+7橙色8橙色+9红色10红色+11白金12白金+1
func (sd EquipmentLevelUp) Quality() E_Quality {
	return sd.quality
}

// 此品质可升级到最高等级
func (sd EquipmentLevelUp) LevelMax() int {
	return sd.levelMax
}

// 装备等级，0表示此品质初始等级
func (sd EquipmentLevelUp) Level() int {
	return sd.level
}

// 强化到此等级需要的经验，若等级=0时表示此品质0星时提供的经验
func (sd EquipmentLevelUp) Exp() int {
	return sd.exp
}

func (sd EquipmentLevelUp) Clone() *EquipmentLevelUp {
	n := NewEquipmentLevelUp()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 装备升级表全局属性
type EquipmentLevelUpGlobal struct {
	equipmentLevelUpAdd    float64 // 装备升级加成比
	equipmentLevelUpSilver int     // 装备强化提供1经验时消耗银两数量

}

// 装备升级加成比
func (g EquipmentLevelUpGlobal) EquipmentLevelUpAdd() float64 {
	return g.equipmentLevelUpAdd
}

// 装备强化提供1经验时消耗银两数量
func (g EquipmentLevelUpGlobal) EquipmentLevelUpSilver() int {
	return g.equipmentLevelUpSilver
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type EquipmentLevelUpManager struct {
	Datas  []*EquipmentLevelUp
	Global EquipmentLevelUpGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*EquipmentLevelUp // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	byQuality map[E_Quality][]*EquipmentLevelUp // NormalIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	qualityLevelTotalExp map[E_Quality]map[int]int // 品质-》等级-》总经验
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newEquipmentLevelUpManager() *EquipmentLevelUpManager {
	mgr := &EquipmentLevelUpManager{
		Datas:     []*EquipmentLevelUp{},
		byID:      map[int]*EquipmentLevelUp{},
		byQuality: map[E_Quality][]*EquipmentLevelUp{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>
	mgr.qualityLevelTotalExp = map[E_Quality]map[int]int{}
	//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *EquipmentLevelUpManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

		mgr.byQuality[d.quality] = append(mgr.byQuality[d.quality], d)

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>
	for _, ds := range mgr.byQuality {
		totalExp := 0
		selfExp := 0
		for _, d := range ds {
			if d.level == 0 {
				selfExp = d.exp
			}
			totalExp += d.exp
			d.totalExp = totalExp
			d.selfExp = selfExp
		}
	}
	//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func EquipmentLevelUpSize() int {
	return equipmentLevelUpMgr.size
}

func (mgr EquipmentLevelUpManager) check(path string, row int, sd *EquipmentLevelUp) error {
	if _, ok := equipmentLevelUpMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *EquipmentLevelUpManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		if _, ok := GetQualityByID(d.quality); !ok {
			success = false
			log.Printf("装备升级表记录[%d]的品质[%d]找不到对应配置\n", d.id, d.quality)
		}
	}
	for _, ds := range mgr.byQuality {
		for i, d := range ds {
			if d.level != i {
				success = false
				log.Printf("装备升级表记录[%d]的等级不连续\n", d.id)
			}
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *EquipmentLevelUpManager) each(f func(sd *EquipmentLevelUp) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *EquipmentLevelUpManager) findIf(f func(sd *EquipmentLevelUp) (find bool)) *EquipmentLevelUp {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachEquipmentLevelUp(f func(sd EquipmentLevelUp) (continued bool)) {
	for _, sd := range equipmentLevelUpMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindEquipmentLevelUpIf(f func(sd EquipmentLevelUp) bool) (EquipmentLevelUp, bool) {
	for _, sd := range equipmentLevelUpMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilEquipmentLevelUp, false
}

func GetEquipmentLevelUpByID(id int) (EquipmentLevelUp, bool) {
	temp, ok := equipmentLevelUpMgr.byID[id]
	if !ok {
		return nilEquipmentLevelUp, false
	}
	return *temp, true
}

func GetEquipmentLevelUpByQuality(quality E_Quality) (vs []EquipmentLevelUp, ok bool) {
	var ds []*EquipmentLevelUp
	ds, ok = equipmentLevelUpMgr.byQuality[quality]
	if !ok {
		return
	}
	for _, d := range ds {
		vs = append(vs, *d)
	}
	return
}

func EachEquipmentLevelUpByQuality(quality E_Quality, fn func(d EquipmentLevelUp) (continued bool)) bool {
	ds, ok := equipmentLevelUpMgr.byQuality[quality]
	if !ok {
		return false
	}
	for _, d := range ds {
		if !fn(*d) {
			break
		}
	}
	return true
}

func GetEquipmentLevelUpGlobal() EquipmentLevelUpGlobal {
	return equipmentLevelUpMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func (sd EquipmentLevelUp) TotalExp() int {
	return sd.totalExp
}

// 装备本身初始经验值
func (sd EquipmentLevelUp) SelfExp() int {
	return sd.selfExp
}

func GetEquipmentLevelUpByQualityAndLevel(quality E_Quality, level int) (dAtA EquipmentLevelUp, ok bool) {
	ds, ok := equipmentLevelUpMgr.byQuality[quality]
	if !ok {
		return nilEquipmentLevelUp, false
	}
	for _, d := range ds {
		if d.level == level {
			return *d, true
		}
	}
	return
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
