// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// vip表
type Vip struct {
	level                int              // VIP等级
	exp                  int              // VIP经验，升级到下一级需要的经验
	reward               int              // 奖励
	heroBagAdd           int              // 武将背包增加数量 必须是5的倍数
	idleSliverAdd        float64          // 挂机额外金币产出增加
	idleHeroExpAdd       float64          // 挂机额外武将经验产出增加 千分数，300表示3%
	daoistMagicSliverAdd float64          // 迷宫产出银币增加
	daoistMagicTokenAdd  float64          // 迷宫产出奇门令增加
	noticeBoardOneself   int              // 悬赏栏个人任务数量
	noticeBoardTeam      int              // 悬赏栏团队任务数量
	visibleVipLevel      int              // v几可见
	treasureHuntDiscount float64          // 寻宝折扣系数
	buyGrade             int              // 定制礼包的购买档次
	noBuyTimes           int              // 定制礼包的购买档次连续几次没有购买后降档
	newGradeMoney        int              // 定制礼包的购买档次在累计充值多少金额后恢复正常档次
	specialRewardUp      float64          // 挂机保底累计时间加成
	privilege            []E_FunctionType // 特权(关联枚举表-功能分页)
	desc                 string           // 在VIP展示中的vip特权描述
	desc2                string           // 在VIP升级界面中的vip特权描述

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewVip() *Vip {
	sd := &Vip{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// VIP等级
func (sd Vip) Level() int {
	return sd.level
}

// VIP经验，升级到下一级需要的经验
func (sd Vip) Exp() int {
	return sd.exp
}

// 奖励
func (sd Vip) Reward() int {
	return sd.reward
}

// 武将背包增加数量 必须是5的倍数
func (sd Vip) HeroBagAdd() int {
	return sd.heroBagAdd
}

// 挂机额外金币产出增加
func (sd Vip) IdleSliverAdd() float64 {
	return sd.idleSliverAdd
}

// 挂机额外武将经验产出增加 千分数，300表示3%
func (sd Vip) IdleHeroExpAdd() float64 {
	return sd.idleHeroExpAdd
}

// 迷宫产出银币增加
func (sd Vip) DaoistMagicSliverAdd() float64 {
	return sd.daoistMagicSliverAdd
}

// 迷宫产出奇门令增加
func (sd Vip) DaoistMagicTokenAdd() float64 {
	return sd.daoistMagicTokenAdd
}

// 悬赏栏个人任务数量
func (sd Vip) NoticeBoardOneself() int {
	return sd.noticeBoardOneself
}

// 悬赏栏团队任务数量
func (sd Vip) NoticeBoardTeam() int {
	return sd.noticeBoardTeam
}

// v几可见
func (sd Vip) VisibleVipLevel() int {
	return sd.visibleVipLevel
}

// 寻宝折扣系数
func (sd Vip) TreasureHuntDiscount() float64 {
	return sd.treasureHuntDiscount
}

// 定制礼包的购买档次
func (sd Vip) BuyGrade() int {
	return sd.buyGrade
}

// 定制礼包的购买档次连续几次没有购买后降档
func (sd Vip) NoBuyTimes() int {
	return sd.noBuyTimes
}

// 定制礼包的购买档次在累计充值多少金额后恢复正常档次
func (sd Vip) NewGradeMoney() int {
	return sd.newGradeMoney
}

// 挂机保底累计时间加成
func (sd Vip) SpecialRewardUp() float64 {
	return sd.specialRewardUp
}

// 特权(关联枚举表-功能分页)
func (sd Vip) Privilege() []E_FunctionType {
	return sd.privilege
}

// 在VIP展示中的vip特权描述
func (sd Vip) Desc() string {
	return sd.desc
}

// 在VIP升级界面中的vip特权描述
func (sd Vip) Desc2() string {
	return sd.desc2
}

func (sd Vip) Clone() *Vip {
	n := NewVip()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type VipManager struct {
	Datas []*Vip

	size int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byLevel map[int]*Vip // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>
	minLevel int
	maxLevel int
	totalExp int
	//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newVipManager() *VipManager {
	mgr := &VipManager{
		Datas:   []*Vip{},
		byLevel: map[int]*Vip{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *VipManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byLevel[d.level] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func VipSize() int {
	return vipMgr.size
}

func (mgr VipManager) check(path string, row int, sd *Vip) error {
	if _, ok := vipMgr.byLevel[sd.level]; ok {
		return fmt.Errorf("[%s]第[%d]行的level[%v]重复", path, row, sd.level)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *VipManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	minSet := false
	for _, d := range mgr.Datas {
		if d.treasureHuntDiscount <= 0 {
			success = false
			log.Printf("vip表vip%d的寻宝折扣系数配置错误\n", d.level)
		}
		if minSet == false {
			mgr.minLevel = d.level
			minSet = true
		}
		if d.level < mgr.minLevel {
			mgr.minLevel = d.level
		}
		if d.level > mgr.maxLevel {
			mgr.maxLevel = d.level
		}
		mgr.totalExp += d.exp
		if d.reward > 0 {
			if _, ok := GetLootByGroup(d.reward); !ok {
				success = false
				log.Printf("vip表vip%d的奖励[%d]不存在\n", d.level, d.reward)
			}
		}
		if d.heroBagAdd > 0 {
			if d.heroBagAdd%GetMoneyBuyGlobal().HeroBagBuyNum() != 0 {
				success = false
				log.Printf("vip表vip%d的英雄背包奖励[%d]不能被每次开格数[%d]整除\n", d.level, d.heroBagAdd, GetMoneyBuyGlobal().HeroBagBuyNum())
			}
		}
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *VipManager) each(f func(sd *Vip) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *VipManager) findIf(f func(sd *Vip) (find bool)) *Vip {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachVip(f func(sd Vip) (continued bool)) {
	for _, sd := range vipMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindVipIf(f func(sd Vip) bool) (Vip, bool) {
	for _, sd := range vipMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilVip, false
}

func GetVipByLevel(level int) (Vip, bool) {
	temp, ok := vipMgr.byLevel[level]
	if !ok {
		return nilVip, false
	}
	return *temp, true
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
func GetMinVipLevel() int {
	return vipMgr.minLevel
}

func GetMaxVipLevel() int {
	return vipMgr.maxLevel
}

func CheckVipLevel(level int) bool {
	return level >= vipMgr.minLevel && level <= vipMgr.maxLevel
}

func GetTotalVipExp() int {
	return vipMgr.totalExp
}

func GetVipCurTotalExp(level int32) (totalExp int64) {
	for _, sd := range vipMgr.Datas {
		nowExp := int64(sd.exp)
		nowLevel := int32(sd.level)
		if nowLevel < level {
			totalExp += nowExp
		}
	}
	return totalExp
}

func GetVipLevelByExp(exp int64) int {
	level := 0
	totalExp := 0
	for _, sd := range vipMgr.Datas {
		nowExp := sd.exp
		totalExp += nowExp
		if exp < int64(totalExp) {
			level = sd.level
			break
		}
	}
	if exp >= int64(GetTotalVipExp()) {
		return GetMaxVipLevel()
	}
	return level
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
