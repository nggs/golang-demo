// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package sd

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	nrandom "nggs/random"
	nutil "nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = nutil.IsSameDayUnixTimeStamp
var _ = nrandom.String

// 兵书升级表
type WarcraftLevel struct {
	id    int // ID
	level int // 等级
	exp   int // 升级所需经验

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>
	totalExp int // 强化到此等级需要的总经验
	initExp  int // 初始经验值
	//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewWarcraftLevel() *WarcraftLevel {
	sd := &WarcraftLevel{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// ID
func (sd WarcraftLevel) ID() int {
	return sd.id
}

// 等级
func (sd WarcraftLevel) Level() int {
	return sd.level
}

// 升级所需经验
func (sd WarcraftLevel) Exp() int {
	return sd.exp
}

func (sd WarcraftLevel) Clone() *WarcraftLevel {
	n := NewWarcraftLevel()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 兵书升级表全局属性
type WarcraftLevelGlobal struct {
	lockLevel   int // 每次解锁属性需求等级
	lockPlus    int // 每次解锁属性提供的属性百分比
	silverCost  int // 每点经验消耗的银币
	purpleExp   int // 紫色兵书经验系数
	orangeExp   int // 橙色兵书经验系数
	redExp      int // 红色兵书经验系数
	platinumExp int // 白金色兵书经验系数
	levelMax    int // 兵书等级上限

}

// 每次解锁属性需求等级
func (g WarcraftLevelGlobal) LockLevel() int {
	return g.lockLevel
}

// 每次解锁属性提供的属性百分比
func (g WarcraftLevelGlobal) LockPlus() int {
	return g.lockPlus
}

// 每点经验消耗的银币
func (g WarcraftLevelGlobal) SilverCost() int {
	return g.silverCost
}

// 紫色兵书经验系数
func (g WarcraftLevelGlobal) PurpleExp() int {
	return g.purpleExp
}

// 橙色兵书经验系数
func (g WarcraftLevelGlobal) OrangeExp() int {
	return g.orangeExp
}

// 红色兵书经验系数
func (g WarcraftLevelGlobal) RedExp() int {
	return g.redExp
}

// 白金色兵书经验系数
func (g WarcraftLevelGlobal) PlatinumExp() int {
	return g.platinumExp
}

// 兵书等级上限
func (g WarcraftLevelGlobal) LevelMax() int {
	return g.levelMax
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type WarcraftLevelManager struct {
	Datas  []*WarcraftLevel
	Global WarcraftLevelGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID    map[int]*WarcraftLevel // UniqueIndex
	byLevel map[int]*WarcraftLevel // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newWarcraftLevelManager() *WarcraftLevelManager {
	mgr := &WarcraftLevelManager{
		Datas: []*WarcraftLevel{},
		byID:  map[int]*WarcraftLevel{}, byLevel: map[int]*WarcraftLevel{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *WarcraftLevelManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d
		mgr.byLevel[d.level] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func WarcraftLevelSize() int {
	return warcraftLevelMgr.size
}

func (mgr WarcraftLevelManager) check(path string, row int, sd *WarcraftLevel) error {
	if _, ok := warcraftLevelMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id[%v]重复", path, row, sd.id)
	}
	if _, ok := warcraftLevelMgr.byLevel[sd.level]; ok {
		return fmt.Errorf("[%s]第[%d]行的level[%v]重复", path, row, sd.level)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *WarcraftLevelManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>
	for _, d := range mgr.Datas {
		for _, c := range mgr.Datas {
			if c.level < d.level {
				d.totalExp += c.exp
			}
			if c.level == 0 {
				d.initExp = c.exp
			}
		}
		//log.Printf("level[%d] exp[%d] totalExp[%d] initExp[%d]\n", d.level, d.exp, d.totalExp, d.initExp)
	}
	//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *WarcraftLevelManager) each(f func(sd *WarcraftLevel) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *WarcraftLevelManager) findIf(f func(sd *WarcraftLevel) (find bool)) *WarcraftLevel {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachWarcraftLevel(f func(sd WarcraftLevel) (continued bool)) {
	for _, sd := range warcraftLevelMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindWarcraftLevelIf(f func(sd WarcraftLevel) bool) (WarcraftLevel, bool) {
	for _, sd := range warcraftLevelMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilWarcraftLevel, false
}

func GetWarcraftLevelByID(id int) (WarcraftLevel, bool) {
	temp, ok := warcraftLevelMgr.byID[id]
	if !ok {
		return nilWarcraftLevel, false
	}
	return *temp, true
}

func GetWarcraftLevelByLevel(level int) (WarcraftLevel, bool) {
	temp, ok := warcraftLevelMgr.byLevel[level]
	if !ok {
		return nilWarcraftLevel, false
	}
	return *temp, true
}

func GetWarcraftLevelGlobal() WarcraftLevelGlobal {
	return warcraftLevelMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>
// 此品质兵书材料转换的经验值
func GetWarcraftTotalExpByQuality(quality E_Quality, level int, curExp int) (exp int, ok bool) {
	warcraftLevelSD, ok := GetWarcraftLevelByLevel(level)
	if !ok {
		return
	}

	ok = true

	switch quality {
	case E_Quality_Purple:
		exp = (warcraftLevelSD.initExp+warcraftLevelSD.totalExp)*GetWarcraftLevelGlobal().purpleExp + curExp
	case E_Quality_Orange:
		exp = (warcraftLevelSD.initExp+warcraftLevelSD.totalExp)*GetWarcraftLevelGlobal().orangeExp + curExp
	case E_Quality_Red:
		exp = (warcraftLevelSD.initExp+warcraftLevelSD.totalExp)*GetWarcraftLevelGlobal().redExp + curExp
	case E_Quality_Platinum:
		exp = (warcraftLevelSD.initExp+warcraftLevelSD.totalExp)*GetWarcraftLevelGlobal().platinumExp + curExp
	default:
		ok = false
	}
	return
}

// 此品质兵书升级此等级所需经验值
func GetWarcraftExpByQualityAndLevel(quality E_Quality, level int) (exp int, ok bool) {
	warcraftLevelSD, ok := GetWarcraftLevelByLevel(level)
	if !ok {
		return
	}

	ok = true

	switch quality {
	case E_Quality_Purple:
		exp = warcraftLevelSD.exp * GetWarcraftLevelGlobal().purpleExp
	case E_Quality_Orange:
		exp = warcraftLevelSD.exp * GetWarcraftLevelGlobal().orangeExp
	case E_Quality_Red:
		exp = warcraftLevelSD.exp * GetWarcraftLevelGlobal().redExp
	case E_Quality_Platinum:
		exp = warcraftLevelSD.exp * GetWarcraftLevelGlobal().platinumExp
	default:
		ok = false
	}
	return
}

//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
