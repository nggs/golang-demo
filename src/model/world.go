package model

import "time"

type WorldMailContainer = Int64ToWorldMailMap

func NewWorldMailContainer() *WorldMailContainer {
	return NewInt64ToWorldMailMap()
}

func (m *World) WorldMailContainer() *WorldMailContainer {
	if m.Mails == nil {
		m.Mails = *NewWorldMailContainer()
	}
	return (*WorldMailContainer)(&m.Mails)
}

func (sc *SimpleClient) CreateWorldMail(tid int32, title string, content string, params map[string]string, reward *Reward, zoneIDs []int32) (d *WorldMail, err error) {
	nextSeq, err := sc.NextSeq(worldMailSeq)
	if err != nil {
		return nil, err
	}
	d = Get_WorldMail()
	d.UID = int64(nextSeq)
	d.TID = tid
	d.Title = title
	d.Content = content
	if len(params) > 0 {
		d.Params = make(map[string]string, len(params))
		for k, v := range params {
			d.Params[k] = v
		}
	}
	//if reward != nil && !reward.IsEmpty() {
	//	d.Reward = reward.Clone()
	//}
	d.SendTime = time.Now().Unix()
	if len(zoneIDs) > 0 {
		for _, value := range zoneIDs {
			d.ZoneIDs = append(d.ZoneIDs, value)
		}
	}
	return
}

//func (sc *SimpleClient) CreateSystemMarquee(serverID int32, content string, duration int32, interval int32, times int32, startTime int32, endTime int32) *SystemMarquee {
//	m := Get_SystemMarquee()
//	m.ServerID = serverID
//	m.Content = content
//	m.Duration = duration
//	m.Interval = interval
//	m.Times = times
//	m.StartTime = startTime
//	m.EndTime = endTime
//	return m
//}
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//type ArenaDispatchContainer = ArenaDispatchMap
//
//func NewArenaDispatchContainer() *ArenaDispatchContainer {
//	c := &ArenaDispatchContainer{}
//	return c
//}
//
//func ToArenaDispatchContainer(m map[int32]*ArenaDispatch) *ArenaDispatchContainer {
//	return (*ArenaDispatchContainer)(&m)
//}
//
//func (m *World) ArenaDispatchContainer() *ArenaDispatchContainer {
//	if m.ArenaDispatches == nil {
//		m.ArenaDispatches = *NewArenaDispatchContainer()
//	}
//	return ToArenaDispatchContainer(m.ArenaDispatches)
//}
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//type SeniorArenaDispatchContainer = SeniorArenaDispatchMap
//
//func NewSeniorArenaDispatchContainer() *SeniorArenaDispatchContainer {
//	c := &SeniorArenaDispatchContainer{}
//	return c
//}
//
//func ToSeniorArenaDispatchContainer(m map[int32]*SeniorArenaDispatch) *SeniorArenaDispatchContainer {
//	return (*SeniorArenaDispatchContainer)(&m)
//}
//
//func (m *World) SeniorArenaDispatchContainer() *SeniorArenaDispatchContainer {
//	if m.SeniorArenaDispatches == nil {
//		m.SeniorArenaDispatches = *NewSeniorArenaDispatchContainer()
//	}
//	return ToSeniorArenaDispatchContainer(m.SeniorArenaDispatches)
//}
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//func (m *World) SystemMarqueeContainer() *SystemMarqueeMap {
//	if m.SystemMarquees == nil {
//		m.SystemMarquees = *NewSystemMarqueeMap()
//	}
//	return (*SystemMarqueeMap)(&m.SystemMarquees)
//}
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//func (m *World) MergeServersContainer() *MergeServerInWorldMap {
//	if m.MergeServers == nil {
//		m.MergeServers = *NewMergeServerInWorldMap()
//	}
//	return (*MergeServerInWorldMap)(&m.MergeServers)
//}
