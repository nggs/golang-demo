// Code generated by goderive DO NOT EDIT.

package model

// deriveDeepCopyAccount recursively copies the contents of src into dst.
func deriveDeepCopyAccount(dst, src *Account) {
	dst.ID = src.ID
	dst.Name = src.Name
	dst.Password = src.Password
	dst.Ban = src.Ban
	dst.RegisterTime = src.RegisterTime
	dst.RegisterChannel = src.RegisterChannel
	dst.RegisterOS = src.RegisterOS
	dst.RegisterClientVersion = src.RegisterClientVersion
	dst.RegisterIP = src.RegisterIP
	dst.LastLoginServerID = src.LastLoginServerID
	dst.LastLoginTime = src.LastLoginTime
	dst.LastLoginChannel = src.LastLoginChannel
	dst.LastLoginOS = src.LastLoginOS
	dst.LastLoginClientVersion = src.LastLoginClientVersion
	dst.LastLoginIP = src.LastLoginIP
}

// deriveDeepCopyRoleInAccount recursively copies the contents of src into dst.
func deriveDeepCopyRoleInAccount(dst, src *RoleInAccount) {
	dst.Avatar = src.Avatar
	dst.Level = src.Level
	dst.LoginTime = src.LoginTime
}

// deriveDeepCopyAccountMinimal recursively copies the contents of src into dst.
func deriveDeepCopyAccountMinimal(dst, src *AccountMinimal) {
	dst.ID = src.ID
	dst.Name = src.Name
	dst.Password = src.Password
	dst.Ban = src.Ban
}

// deriveDeepCopyAccountInRole recursively copies the contents of src into dst.
func deriveDeepCopyAccountInRole(dst, src *AccountInRole) {
	dst.Ban = src.Ban
	dst.LastLoginChannel = src.LastLoginChannel
	dst.LastLoginOS = src.LastLoginOS
}

// deriveDeepCopyRewardThing recursively copies the contents of src into dst.
func deriveDeepCopyRewardThing(dst, src *RewardThing) {
	dst.Type = src.Type
	dst.TID = src.TID
	dst.Num = src.Num
	dst.Faction = src.Faction
	dst.Level = src.Level
}

// deriveDeepCopyReward recursively copies the contents of src into dst.
func deriveDeepCopyReward(dst, src *Reward) {
	if src.Things == nil {
		dst.Things = nil
	} else {
		if dst.Things != nil {
			if len(src.Things) > len(dst.Things) {
				if cap(dst.Things) >= len(src.Things) {
					dst.Things = (dst.Things)[:len(src.Things)]
				} else {
					dst.Things = make([]*RewardThing, len(src.Things))
				}
			} else if len(src.Things) < len(dst.Things) {
				dst.Things = (dst.Things)[:len(src.Things)]
			}
		} else {
			dst.Things = make([]*RewardThing, len(src.Things))
		}
		deriveDeepCopy(dst.Things, src.Things)
	}
}

// deriveDeepCopyRole recursively copies the contents of src into dst.
func deriveDeepCopyRole(dst, src *Role) {
	dst.ID = src.ID
	dst.AccountID = src.AccountID
	dst.ServerID = src.ServerID
	dst.Name = src.Name
	dst.CreateTime = src.CreateTime
	dst.LastLoginTime = src.LastLoginTime
	dst.LastLogoutTime = src.LastLogoutTime
}

// deriveDeepCopyRoleMinimal recursively copies the contents of src into dst.
func deriveDeepCopyRoleMinimal(dst, src *RoleMinimal) {
	dst.ID = src.ID
	dst.AccountID = src.AccountID
	dst.ServerID = src.ServerID
}

// deriveDeepCopyWorld recursively copies the contents of src into dst.
func deriveDeepCopyWorld(dst, src *World) {
	dst.ID = src.ID
	if src.Mails != nil {
		dst.Mails = make(map[int64]*WorldMail, len(src.Mails))
		deriveDeepCopy_(dst.Mails, src.Mails)
	} else {
		dst.Mails = nil
	}
}

// deriveDeepCopyWorldMail recursively copies the contents of src into dst.
func deriveDeepCopyWorldMail(dst, src *WorldMail) {
	dst.UID = src.UID
	dst.TID = src.TID
	dst.Title = src.Title
	dst.Content = src.Content
	if src.Params != nil {
		dst.Params = make(map[string]string, len(src.Params))
		deriveDeepCopy_1(dst.Params, src.Params)
	} else {
		dst.Params = nil
	}
	dst.SendTime = src.SendTime
	if src.ZoneIDs == nil {
		dst.ZoneIDs = nil
	} else {
		if dst.ZoneIDs != nil {
			if len(src.ZoneIDs) > len(dst.ZoneIDs) {
				if cap(dst.ZoneIDs) >= len(src.ZoneIDs) {
					dst.ZoneIDs = (dst.ZoneIDs)[:len(src.ZoneIDs)]
				} else {
					dst.ZoneIDs = make([]int32, len(src.ZoneIDs))
				}
			} else if len(src.ZoneIDs) < len(dst.ZoneIDs) {
				dst.ZoneIDs = (dst.ZoneIDs)[:len(src.ZoneIDs)]
			}
		} else {
			dst.ZoneIDs = make([]int32, len(src.ZoneIDs))
		}
		copy(dst.ZoneIDs, src.ZoneIDs)
	}
}

// deriveDeepCopyZone recursively copies the contents of src into dst.
func deriveDeepCopyZone(dst, src *Zone) {
	dst.ID = src.ID
	dst.OpenTime = src.OpenTime
	dst.Name = src.Name
}

// deriveDeepCopyZoneMinimal recursively copies the contents of src into dst.
func deriveDeepCopyZoneMinimal(dst, src *ZoneMinimal) {
	dst.ID = src.ID
	dst.OpenTime = src.OpenTime
	dst.Name = src.Name
}

// deriveDeepCopyZoneInRole recursively copies the contents of src into dst.
func deriveDeepCopyZoneInRole(dst, src *ZoneInRole) {
	dst.OpenTime = src.OpenTime
	dst.Name = src.Name
}

// deriveDeepCopy recursively copies the contents of src into dst.
func deriveDeepCopy(dst, src []*RewardThing) {
	for src_i, src_value := range src {
		if src_value == nil {
			dst[src_i] = nil
		} else {
			dst[src_i] = new(RewardThing)
			*dst[src_i] = *src_value
		}
	}
}

// deriveDeepCopy_ recursively copies the contents of src into dst.
func deriveDeepCopy_(dst, src map[int64]*WorldMail) {
	for src_key, src_value := range src {
		if src_value == nil {
			dst[src_key] = nil
		}
		if src_value == nil {
			dst[src_key] = nil
		} else {
			dst[src_key] = new(WorldMail)
			deriveDeepCopyWorldMail(dst[src_key], src_value)
		}
	}
}

// deriveDeepCopy_1 recursively copies the contents of src into dst.
func deriveDeepCopy_1(dst, src map[string]string) {
	for src_key, src_value := range src {
		dst[src_key] = src_value
	}
}
