package model

import (
	"strconv"
	"time"

	mongodb "nggs/db/mongodb"
)

const (
	serverIDOffset = 10000
	roleIDMagicNum = 97456
)

/*
规则：
1.  显示id = 账号id 异或  (服务器id 乘以 1000)
2.  显示id = 显示id 乘以 10000 加上 服务器id
3.  显示id = 显示id 异或 97456

代码：
roleID = accountID ^ int64(serverID * 1000)
roleID = roleID * 100000 + int64(serverID)
roleID = roleID ^ 97456

示例：
accountID=11, serverID=1, roleID=99566417
accountID=11, serverID=2, roleID=201193042
accountID=11, serverID=3, roleID=299597139
accountID=11, serverID=4, roleID=401158228
accountID=11, serverID=5, roleID=499433301
accountID=11, serverID=6, roleID=601190998
accountID=11, serverID=7, roleID=699464023
accountID=11, serverID=8, roleID=801025112
accountID=11, serverID=9, roleID=899431257
accountID=11, serverID=10, roleID=1001057882

accountID=12, serverID=1, roleID=99531313
accountID=12, serverID=2, roleID=201289010
accountID=12, serverID=3, roleID=299562035
accountID=12, serverID=4, roleID=401125172
accountID=12, serverID=5, roleID=499529269
accountID=12, serverID=6, roleID=601155894
accountID=12, serverID=7, roleID=699559991
accountID=12, serverID=8, roleID=801123128
accountID=12, serverID=9, roleID=899658297
accountID=12, serverID=10, roleID=1001153850

accountID=220515, serverID=17, roleID=20455576641
accountID=220515, serverID=18, roleID=20357172546
accountID=2220515, serverID=17, roleID=220455404609
accountID=2220515, serverID=18, roleID=220561866050

accountID=99999999, serverID=9999, roleID=9105879199199
*/
func AccountIDServerIDToRoleID(accountID int64, serverID int32) (roleID int64) {
	roleID = accountID ^ int64(serverID*1000)
	roleID = roleID*serverIDOffset + int64(serverID)
	roleID = roleID ^ roleIDMagicNum
	return
}

func RoleIDToAccountIDServerID(roleID int64) (accountID int64, serverID int32) {
	roleID = roleID ^ roleIDMagicNum
	serverID = int32(roleID % serverIDOffset)
	accountID = roleID / serverIDOffset
	accountID = accountID ^ int64(serverID*1000)
	return
}

func (sc *SimpleClient) CreateRole(accountID int64, serverID int32) (m *Role, err error) {
	now := time.Now().Unix()

	m = Get_Role()
	m.ID = AccountIDServerIDToRoleID(accountID, serverID)
	m.AccountID = accountID
	m.ServerID = serverID
	seed := (m.AccountID*100 + int64(m.ServerID)) ^ 98126
	m.Name = "角色" + strconv.FormatInt(seed, 10)
	m.CreateTime = now
	//m.LastLoginTime = now

	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//type StringSlice []string
//
//func NewStringSlice() *StringSlice {
//	return &StringSlice{}
//}
//
//func ToStringSlice(strings []string) *StringSlice {
//	return (*StringSlice)(&strings)
//}
//
//func (s *StringSlice) AddOne(newString string) {
//	*s = append(*s, newString)
//	return
//}
//
//func (s *StringSlice) RemoveOne(fn func(str string) (removed bool)) {
//	for i, str := range *s {
//		if fn(str) {
//			*s = append((*s)[:i], (*s)[i+1:]...)
//			break
//		}
//	}
//}
//
//func (s *StringSlice) RemoveSome(fn func(str string) (removed bool)) {
//	var left []string
//	for _, str := range *s {
//		if !fn(str) {
//			left = append(left, str)
//		}
//	}
//	*s = left
//}
//
//func (s StringSlice) Each(fn func(str string) (continued bool)) {
//	for _, str := range s {
//		if !fn(str) {
//			break
//		}
//	}
//}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//func (sc SimpleClient) FindSomeRoleByLimitAndSort(session *mongodb.Session, query interface{}, limit int, sortFields ...string) (some []*Role, err error) {
//	some = []*Role{}
//	err = session.DB(sc.dbName).C(TblRole).Find(query).Sort(sortFields...).Limit(limit).All(&some)
//	if err != nil {
//		return nil, err
//	}
//	return
//}
//
//func (sc SimpleClient) SelectOneRole(session *mongodb.Session, query interface{}, selector interface{}) (one *Role, err error) {
//	one = Get_Role()
//	err = session.DB(sc.dbName).C(TblRole).Find(query).Select(selector).One(one)
//	if err != nil {
//		//Put_Role(one)
//		return nil, err
//	}
//	return
//}
//
//func (sc SimpleClient) SelectSomeRoleFields(session *mongodb.Session, query interface{}, selector interface{}, limit int, result interface{}) (err error) {
//	return sc.SelectSomeDocumentFields(session, TblRole, query, selector, limit, result)
//}
//
//func (sc SimpleClient) SelectOneRoleFields(session *mongodb.Session, roleID int64, selector interface{}, result interface{}) (err error) {
//	return sc.SelectOneDocumentFields(session, TblRole, bson.M{"_id": roleID}, selector, result)
//}

func (sc SimpleClient) GetRoleCount(session *mongodb.Session, query interface{}) (n int, err error) {
	return sc.GetDocumentCount(session, TblRole, query)
}
