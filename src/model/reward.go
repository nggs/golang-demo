//+build ignore

package model

import (
	"sd"
)

func (m RewardThing) IsEmpty() bool {
	return m.Type == 0 || m.TID == 0 || m.Num == 0
}

func (m RewardThing) ToSD(n *sd.SimpleRewardThing) *sd.SimpleRewardThing {
	if n == nil {
		n = sd.Get_SimpleRewardThing()
	}
	n.Type = sd.E_ItemBigType(m.Type)
	n.TID = int(m.TID)
	n.Num = int(m.Num)
	n.Faction = sd.E_Faction(m.Faction)
	n.Level = int(m.Level)
	return n
}

func (m *RewardThing) FromSD(n sd.SimpleRewardThing) {
	m.Type = int32(n.Type)
	m.TID = int32(n.TID)
	m.Num = int32(n.Num)
	m.Faction = int32(n.Faction)
	m.Level = int32(n.Level)
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (m Reward) Each(f func(thing *RewardThing) (continued bool)) {
	for _, thing := range m.Things {
		if !f(thing) {
			break
		}
	}
}

func (m Reward) IsEmpty() bool {
	if len(m.Things) == 0 {
		return true
	}
	return false
}

func (m Reward) ToSD(n *sd.SimpleReward) *sd.SimpleReward {
	if n == nil {
		n = sd.Get_SimpleReward()
	}
	if len(m.Things) == 0 {
		return n
	}
	n.Things = make([]*sd.SimpleRewardThing, len(m.Things))
	for i, thing := range m.Things {
		n.Things[i] = thing.ToSD(n.Things[i])
	}
	return n
}

func (m *Reward) FromSD(n sd.SimpleReward) {
	if len(n.Things) == 0 {
		return
	}
	m.Things = make([]*RewardThing, len(n.Things))
	for i, thing := range n.Things {
		m.Things[i] = NewRewardThingFromSD(*thing)
	}
}

func (m *Reward) getByTID(tid int32) *RewardThing {
	for _, thing := range m.Things {
		if thing.TID == tid {
			return thing
		}
	}
	return nil
}

func (m *Reward) AppendOne(tYpE sd.E_ItemBigType, tid int, num int) *Reward {
	if tYpE <= 0 || tid <= 0 || num <= 0 {
		return m
	}

	thing := m.getByTID(int32(tid))
	if thing != nil {
		thing.Num += int32(num)
		return m
	}

	thing = Get_RewardThing()
	thing.Type = int32(tYpE)
	thing.TID = int32(tid)
	thing.Num = int32(num)
	switch sd.E_ItemBigType(thing.Type) {
	case sd.E_ItemBigType_Hero:
		// 英雄等级1级起
		thing.Level = 1
	}
	m.Things = append(m.Things, thing)

	return m
}

func (m *Reward) AppendOneThing(thing *RewardThing) *Reward {
	if thing == nil {
		return m
	}

	if thing.Num <= 0 {
		return m
	}

	t := m.getByTID(thing.TID)
	if t != nil {
		t.Num += thing.Num
		return m
	}

	m.Things = append(m.Things, thing.Clone())

	return m
}

func (m *Reward) Merge(other *Reward) *Reward {
	var thing *RewardThing
	for _, t := range other.Things {
		if t.Num <= 0 {
			continue
		}
		thing = m.getByTID(t.TID)
		if thing != nil {
			thing.Num += t.Num
		} else {
			m.Things = append(m.Things, t.Clone())
		}
	}
	return m
}

func NewRewardThingFromSD(n sd.SimpleRewardThing) (m *RewardThing) {
	m = Get_RewardThing()
	m.FromSD(n)
	return
}

func NewRewardFromSD(n sd.SimpleReward) (m *Reward) {
	m = Get_Reward()
	m.FromSD(n)
	return
}
