package model

import (
	"fmt"

	"github.com/globalsign/mgo/bson"

	"nggs/db/mongodb"
)

const PH = 0

const (
	TblCounters = "counters" // 用来生成递增序列的表
	TblAccount  = "account"  // 账号表
	TblRole     = "role"     // 角色表
	TblZone     = "zone"     // 区服表
	TblWorld    = "world"    // 世界表

	visitorNameSeq = "visitor"    // 用来生成游客名的递增序列
	worldMailSeq   = "world_mail" // 用来生成全服/区服邮件的递增序列
)

// 定义库表的递增序列
var seqs = []string{
	TblAccount,
	TblRole,
	visitorNameSeq,
}

// 定义库表的唯一索引
var uniqueIndexes = map[string][][]string{
	TblAccount: {
		[]string{"Name"},
	},
	TblRole: {
		[]string{"AccountID", "ServerID"},
		[]string{"Name", "ServerID"}, // 同区角色昵称不能重复，正式服时需要开启
	},
}

// 定义库表的索引
var indexes = map[string][][]string{
	//TblAccount: {
	//	[]string{"Name"},
	//},
	TblRole: {
		[]string{"AccountID"},
		[]string{"ServerID"},
		[]string{"Name"},
		//[]string{"ArenaID"},
		//[]string{"SeniorArena.ID"},
		[]string{"Level"},
		[]string{"LastLoginTime"},
		[]string{"LastLogoutTime"},
	},
}

var SC = NewSimpleClient()

type SimpleClient struct {
	url        string
	sessionNum int
	dbName     string

	dialContext *mongodb.DialContext
}

func NewSimpleClient() (sc *SimpleClient) {
	sc = &SimpleClient{}
	return
}

func (sc *SimpleClient) Init(url string, sessionNum int, dbName string) (err error) {
	sc.url = url
	sc.sessionNum = sessionNum
	sc.dbName = dbName

	sc.dialContext, err = mongodb.Dial(sc.url, sc.sessionNum)
	if err != nil {
		err = fmt.Errorf("connect to %s fail, %s", sc.url, err)
		return
	}

	for _, seq := range seqs {
		err = sc.dialContext.EnsureCounter(sc.dbName, TblCounters, seq)
		if err != nil {
			err = fmt.Errorf("ensure counters [%s] error, %s", seq, err)
			return
		}
	}

	for tbl, indexes := range uniqueIndexes {
		for _, index := range indexes {
			err = sc.dialContext.EnsureUniqueIndex(sc.dbName, tbl, index)
			if err != nil {
				err = fmt.Errorf("ensure table[%s] unique index[%+v] error, %s", tbl, index, err)
				return
			}
		}
	}

	for tbl, is := range indexes {
		for _, index := range is {
			err = sc.dialContext.EnsureIndex(sc.dbName, tbl, index)
			if err != nil {
				err = fmt.Errorf("ensure table[%s] index[%+v] error, %s", tbl, index, err)
				return
			}
		}
	}

	return
}

func (sc *SimpleClient) Release() {
	if sc.dialContext != nil {
		sc.dialContext.Close()
		sc.dialContext = nil
	}
}

func (sc *SimpleClient) DialContext() *mongodb.DialContext {
	return sc.dialContext
}

func (sc SimpleClient) DBName() string {
	return sc.dbName
}

func (sc *SimpleClient) GetSession() *mongodb.Session {
	return sc.dialContext.Ref()
}

func (sc *SimpleClient) PutSession(session *mongodb.Session) {
	sc.dialContext.UnRef(session)
}

func (sc *SimpleClient) NextSeq(id string) (int, error) {
	return sc.dialContext.NextSeq(sc.dbName, TblCounters, id)
}

func (sc SimpleClient) GetDocumentCount(session *mongodb.Session, collectionName string, query interface{}) (n int, err error) {
	n, err = session.DB(sc.dbName).C(collectionName).Find(query).Count()
	return
}

func (sc SimpleClient) SelectSomeDocumentFields(session *mongodb.Session, collectionName string, query interface{}, selector interface{}, limit int, result interface{}) (err error) {
	if limit > 0 {
		err = session.DB(sc.dbName).C(collectionName).Find(query).Select(selector).Limit(limit).All(result)
	} else {
		err = session.DB(sc.dbName).C(collectionName).Find(query).Select(selector).All(result)
	}
	return
}

func (sc SimpleClient) SelectOneDocumentFields(session *mongodb.Session, collectionName string, query interface{}, selector interface{}, result interface{}) (err error) {
	err = session.DB(sc.dbName).C(collectionName).Find(query).Select(selector).One(result)
	return
}

func NewObjectID() string {
	return bson.NewObjectId().Hex()
}
