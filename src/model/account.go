package model

import (
	"fmt"
	"regexp"
	"time"

	mongodb "nggs/db/mongodb"
	nrandom "nggs/random"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const VisitorNamePrefix = "游客"

var VisitorNameRegexp = regexp.MustCompile(fmt.Sprintf(`\s*%s(?:\s*\s*(\d+))?\s*`, VisitorNamePrefix))

func IsVisitorName(name string) bool {
	if matches := VisitorNameRegexp.FindStringSubmatch(name); len(matches) > 0 {
		return true
	}
	return false
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (sc *SimpleClient) CreateAccount(name string, password string, channel string, os string, clientVersion string, ip string) (d *Account, err error) {
	nextSeq, err := sc.NextSeq(TblAccount)
	if err != nil {
		return nil, err
	}
	d = Get_Account()
	d.ID = int64(nextSeq)
	d.Name = name
	d.Password = password // todo md5???
	d.RegisterTime = time.Now().Unix()
	d.RegisterChannel = channel
	d.RegisterOS = os
	d.RegisterClientVersion = clientVersion
	d.RegisterIP = ip
	return
}

func (sc *SimpleClient) CreateVisitorAccount(channel string, os string, clientVersion string, ip string) (d *Account, err error) {
	nextVisitorNameSeq, err := sc.NextSeq(visitorNameSeq)
	if err != nil {
		return nil, err
	}
	name := fmt.Sprintf("%s%d", VisitorNamePrefix, nextVisitorNameSeq)
	password := nrandom.String(8)
	d, err = sc.CreateAccount(name, password, channel, os, clientVersion, ip)
	return
}

func (sc SimpleClient) FindOneAccountMinimal(session *mongodb.Session, query interface{}) (one *AccountMinimal, err error) {
	one = Get_AccountMinimal()
	err = session.DB(sc.dbName).C(TblAccount).Find(query).One(one)
	if err != nil {
		//Put_AccountMinimal(one)
		return nil, err
	}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (m Account) ToMinimal(n *AccountMinimal) *AccountMinimal {
	if n == nil {
		n = Get_AccountMinimal()
	}
	n.ID = m.ID
	n.Name = m.Name
	n.Password = m.Password
	n.Ban = m.Ban
	return n
}

func (m Account) ToInRole(n *AccountInRole) *AccountInRole {
	if n == nil {
		n = Get_AccountInRole()
	}
	n.Ban = m.Ban
	n.LastLoginChannel = m.LastLoginChannel
	n.LastLoginOS = m.LastLoginOS
	return n
}
