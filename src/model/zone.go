package model

func (m Zone) ToMinimal(n *ZoneMinimal) *ZoneMinimal {
	if n == nil {
		n = Get_ZoneMinimal()
	}
	n.ID = m.ID
	n.OpenTime = m.OpenTime
	n.Name = m.Name
	return n
}

func (m Zone) ToInRole(n *ZoneInRole) *ZoneInRole {
	if n == nil {
		n = Get_ZoneInRole()
	}
	n.OpenTime = m.OpenTime
	n.Name = m.Name
	return n
}
