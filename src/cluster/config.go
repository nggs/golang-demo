package cluster

import (
	"fmt"
	"time"

	napp "nggs/app"
	ncluster "nggs/cluster"
	nexport "nggs/export"
	nservice "nggs/service"
	nutil "nggs/util"
)

const (
	GatewayServiceName  = "gateway"
	LoginServiceName    = "login"
	AccountsServiceName = "accounts"
	ZonesServiceName    = "zones"
	WorldServiceName    = "world"
	GameServiceName     = "game"
	//ChatsServiceName    = "chats"
	//ArenasServiceName   = "arenas"
	//GuildsServiceName   = "guilds"
	//PaymentServiceName = "payment"
	//WeChatServiceName   = "wechat"
	//CrossesServiceName = "crosses"
)

var (
	serviceNameMap = map[string]struct{}{
		GatewayServiceName:  {},
		LoginServiceName:    {},
		AccountsServiceName: {},
		ZonesServiceName:    {},
		WorldServiceName:    {},
		GameServiceName:     {},
		//ChatsServiceName:    {},
		//ArenasServiceName:   {},
		//GuildsServiceName:   {},
		//PaymentServiceName: {},
		//WeChatServiceName:   {},
		//CrossesServiceName: {},
	}
)

func IsServiceName(name string) bool {
	_, ok := serviceNameMap[name]
	return ok
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func MustGetAppConfig(id int) *napp.Config {
	serviceConfig, err := ncluster.C.GetServiceConfig(napp.ConfigName, id)
	if err != nil {
		panic(err)
	}
	return serviceConfig.(*napp.Config)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type MongoDBConfig struct {
	Url        string
	Name       string
	SessionNum int
}

func (cfg *MongoDBConfig) TidyAndCheck() error {
	if cfg.Url == "" {
		return fmt.Errorf("MongoDBConfig.Url must not empty")
	}
	if cfg.Name == "" {
		return fmt.Errorf("MongoDBConfig.Name must not empty")
	}
	if cfg.SessionNum <= 0 {
		cfg.SessionNum = 1
	}
	return nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type RedisConfig struct {
	Addr     string
	Password string
	DB       int
}

func (cfg RedisConfig) TidyAndCheck() error {
	if cfg.Addr == "" {
		return fmt.Errorf("invalid RedisConfig.Addr")
	}
	return nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type GlobalConfig struct {
	MainDB MongoDBConfig
}

func (cfg *GlobalConfig) TidyAndCheck() (err error) {
	if err := cfg.MainDB.TidyAndCheck(); err != nil {
		return fmt.Errorf("invalid MainDB, %s", err)
	}
	return
}

func (cfg GlobalConfig) Clone() nexport.IClusterGlobalConfig {
	n := &GlobalConfig{}
	*n = cfg
	return n
}

func MustGetGlobalConfig() *GlobalConfig {
	globalConfig, err := ncluster.C.GetGlobalConfig()
	if err != nil {
		panic(err)
	}
	return globalConfig.(*GlobalConfig)
}

func MustGetMainDBConfig() MongoDBConfig {
	globalConfig, err := ncluster.C.GetGlobalConfig()
	if err != nil {
		panic(err)
	}
	return globalConfig.(*GlobalConfig).MainDB
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type HttpServerConfig struct {
	ListenAddr string // 监听地址
	WanAddr    string // 外网地址
}

func (cfg *HttpServerConfig) TidyAndCheck() error {
	if cfg.ListenAddr == "" {
		return fmt.Errorf("invalid HttpServerConfig.ListenAddr")
	}
	if cfg.WanAddr == "" {
		return fmt.Errorf("invalid HttpServerConfig.WanAddr")
	}
	return nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// LoginGlobalConfig 登录全局配置
type LoginGlobalConfig struct {
	UseTLS      bool   // 是否启用TLS
	TLSCertFile string // CertFile路径
	TLSKeyFile  string // KeyFile路径
}

func (cfg *LoginGlobalConfig) TidyAndCheck() error {
	if cfg.UseTLS {
		if nutil.IsDirOrFileExist(cfg.TLSCertFile) != nil {
			return fmt.Errorf("invalid LoginGlobalConfig.TLSCertFile")
		}
		if nutil.IsDirOrFileExist(cfg.TLSKeyFile) != nil {
			return fmt.Errorf("invalid LoginGlobalConfig.TLSKeyFile")
		}
	}
	return nil
}

func (cfg LoginGlobalConfig) Clone() nexport.IServiceGlobalConfig {
	n := &LoginGlobalConfig{}
	*n = cfg
	return n
}

func MustGetLoginGlobalConfig() *LoginGlobalConfig {
	serviceGlobalConfig, err := ncluster.C.GetServiceGlobalConfig(LoginServiceName)
	if err != nil {
		panic(err)
	}
	return serviceGlobalConfig.(*LoginGlobalConfig)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// LoginConfig 登录服配置
type LoginConfig struct {
	nservice.Config
	HttpServer HttpServerConfig
}

func (cfg *LoginConfig) TidyAndCheck() error {
	if err := cfg.Config.TidyAndCheck(); err != nil {
		return fmt.Errorf("invalid login[%d] config, %s", cfg.GetID(), err)
	}
	if err := cfg.HttpServer.TidyAndCheck(); err != nil {
		return fmt.Errorf("invalid login[%d] config, %s", cfg.GetID(), err)
	}
	return nil
}

func (cfg LoginConfig) Clone() nexport.IServiceConfig {
	n := &LoginConfig{}
	*n = cfg
	return n
}

func MustGetLoginConfig(serviceID nexport.ServiceID) *LoginConfig {
	serviceConfig, err := ncluster.C.GetServiceConfig(LoginServiceName, serviceID)
	if err != nil {
		panic(err)
	}
	return serviceConfig.(*LoginConfig)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AccountsGlobalConfig 账号服全局配置
type AccountsGlobalConfig struct {
	TokenKey            string        // 登录token key
	TokenExpireSec      int64         // 登录token超时秒数
	LoginMinIntervalSec time.Duration // 登录最小间隔秒数
	IdleToStopSec       time.Duration // 空闲至停止的等待秒数
}

func (cfg *AccountsGlobalConfig) TidyAndCheck() error {
	if cfg.TokenKey == "" {
		return fmt.Errorf("invalid AccountsGlobalConfig.TokenKey")
	}

	if cfg.TokenExpireSec <= 0 {
		cfg.TokenExpireSec = 3600
	}

	if cfg.LoginMinIntervalSec > 0 {
		cfg.LoginMinIntervalSec *= time.Second
	}

	if cfg.IdleToStopSec <= 0 {
		cfg.IdleToStopSec = 60
	} else if cfg.IdleToStopSec < 60 {
		return fmt.Errorf("invalid AccountsGlobalConfig.IdleToStopSec, must >= 60")
	}
	cfg.IdleToStopSec *= time.Second

	return nil
}

func (cfg AccountsGlobalConfig) Clone() nexport.IServiceGlobalConfig {
	n := &AccountsGlobalConfig{}
	*n = cfg
	return n
}

func MustGetAccountsGlobalConfig() *AccountsGlobalConfig {
	serviceGlobalConfig, err := ncluster.C.GetServiceGlobalConfig(AccountsServiceName)
	if err != nil {
		panic(err)
	}
	return serviceGlobalConfig.(*AccountsGlobalConfig)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AccountsConfig accounts配置
type AccountsConfig struct {
	nservice.Config
}

func (cfg *AccountsConfig) TidyAndCheck() error {
	if err := cfg.Config.TidyAndCheck(); err != nil {
		return fmt.Errorf("invalid accounts[%d] config, %s", cfg.GetID(), err)
	}
	return nil
}

func (cfg AccountsConfig) Clone() nexport.IServiceConfig {
	n := &AccountsConfig{}
	*n = cfg
	return n
}

func MustGetAccountsConfig(serviceID nexport.ServiceID) *AccountsConfig {
	serviceConfig, err := ncluster.C.GetServiceConfig(AccountsServiceName, serviceID)
	if err != nil {
		panic(err)
	}
	return serviceConfig.(*AccountsConfig)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GatewayGlobalConfig 网关全局配置
type GatewayGlobalConfig struct {
	MaxConnNum     int           // 最多连接数
	RecvTimeoutSec time.Duration // 多少秒没收到包后断开连接
	SendTimeoutSec time.Duration // 多少秒没发包后断开连接
	UseTLS         bool          // 是否启用TLS
	TLSCertFile    string        // CertFile路径
	TLSKeyFile     string        // KeyFile路径
}

func (cfg *GatewayGlobalConfig) TidyAndCheck() error {
	if cfg.MaxConnNum <= 0 {
		cfg.MaxConnNum = 10000
	}

	if cfg.RecvTimeoutSec <= 0 {
		cfg.RecvTimeoutSec = 60
	}
	cfg.RecvTimeoutSec *= time.Second

	if cfg.SendTimeoutSec <= 0 {
		cfg.SendTimeoutSec = 60
	}
	cfg.SendTimeoutSec *= time.Second

	if cfg.UseTLS {
		if nutil.IsDirOrFileExist(cfg.TLSCertFile) != nil {
			return fmt.Errorf("invalid GatewayGlobalConfig.TLSCertFile")
		}
		if nutil.IsDirOrFileExist(cfg.TLSKeyFile) != nil {
			return fmt.Errorf("invalid GatewayGlobalConfig.TLSKeyFile")
		}
	}

	return nil
}

func (cfg GatewayGlobalConfig) Clone() nexport.IServiceGlobalConfig {
	n := &GatewayGlobalConfig{}
	*n = cfg
	return n
}

func MustGetGatewayGlobalConfig() *GatewayGlobalConfig {
	serviceGlobalConfig, err := ncluster.C.GetServiceGlobalConfig(GatewayServiceName)
	if err != nil {
		panic(err)
	}
	return serviceGlobalConfig.(*GatewayGlobalConfig)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type WebSocketServerConfig struct {
	ListenAddr string // 监听地址
	WanAddr    string // 外网地址
}

func (cfg WebSocketServerConfig) TidyAndCheck() error {
	if cfg.ListenAddr == "" {
		return fmt.Errorf("invalid WebSocketServerConfig.ListenAddr")
	}
	if cfg.WanAddr == "" {
		return fmt.Errorf("invalid WebSocketServerConfig.WanAddr")
	}
	return nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GatewayConfig 网关配置
type GatewayConfig struct {
	nservice.Config
	Server WebSocketServerConfig
}

func (cfg *GatewayConfig) TidyAndCheck() error {
	if err := cfg.Config.TidyAndCheck(); err != nil {
		return fmt.Errorf("invalid gateway[%d] config, %s", cfg.GetID(), err)
	}
	if err := cfg.Server.TidyAndCheck(); err != nil {
		return fmt.Errorf("invalid gateway[%d] config, %s", cfg.GetID(), err)
	}
	return nil
}

func (cfg GatewayConfig) Clone() nexport.IServiceConfig {
	n := &GatewayConfig{}
	*n = cfg
	return n
}

func MustGetGatewayConfig(serviceID nexport.ServiceID) *GatewayConfig {
	serviceConfig, err := ncluster.C.GetServiceConfig(GatewayServiceName, serviceID)
	if err != nil {
		panic(err)
	}
	return serviceConfig.(*GatewayConfig)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ZonesGlobalConfig Zones全局配置
type ZonesGlobalConfig struct {
	StartIntervalMilliSec time.Duration // 启动zone间隔
	//SaveIntervalSec       time.Duration // 保存间隔秒数
}

func (cfg *ZonesGlobalConfig) TidyAndCheck() error {
	if cfg.StartIntervalMilliSec <= 0 {
		cfg.StartIntervalMilliSec = 100
	} else if cfg.StartIntervalMilliSec < 100 {
		return fmt.Errorf("invalid ZonesGlobalConfig.StartIntervalMilliSec, must >= 60")
	}
	cfg.StartIntervalMilliSec *= time.Millisecond

	//if cfg.SaveIntervalSec <= 0 {
	//	cfg.SaveIntervalSec = 60
	//} else if cfg.SaveIntervalSec < 60 {
	//	return fmt.Errorf("invalid ZonesGlobalConfig.SaveIntervalSec, must >= 60")
	//}

	return nil
}

func (cfg ZonesGlobalConfig) Clone() nexport.IServiceGlobalConfig {
	n := &ZonesGlobalConfig{}
	*n = cfg
	return n
}

func MustGetZonesGlobalConfig() *ZonesGlobalConfig {
	serviceGlobalConfig, err := ncluster.C.GetServiceGlobalConfig(ZonesServiceName)
	if err != nil {
		panic(err)
	}
	return serviceGlobalConfig.(*ZonesGlobalConfig)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ZonesConfig 区服配置
type ZonesConfig struct {
	nservice.Config
}

func (cfg *ZonesConfig) TidyAndCheck() error {
	if err := cfg.Config.TidyAndCheck(); err != nil {
		return fmt.Errorf("invalid zones[%d] config, %s", cfg.GetID(), err)
	}
	return nil
}

func (cfg ZonesConfig) Clone() nexport.IServiceConfig {
	n := &ZonesConfig{}
	*n = cfg
	return n
}

func MustGetZonesConfig(serviceID nexport.ServiceID) *ZonesConfig {
	serviceConfig, err := ncluster.C.GetServiceConfig(ZonesServiceName, serviceID)
	if err != nil {
		panic(err)
	}
	return serviceConfig.(*ZonesConfig)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GameGlobalConfig Game全局配置
type GameGlobalConfig struct {
	IdleToStopSec time.Duration // 空闲至停止的等待秒数
}

func (cfg *GameGlobalConfig) TidyAndCheck() error {
	if cfg.IdleToStopSec <= 0 {
		cfg.IdleToStopSec = 60
	} else if cfg.IdleToStopSec < 60 {
		return fmt.Errorf("invalid GameGlobalConfig.IdleToStopSec, must >= 60")
	}
	cfg.IdleToStopSec *= time.Second
	return nil
}

func (cfg GameGlobalConfig) Clone() nexport.IServiceGlobalConfig {
	n := &GameGlobalConfig{}
	*n = cfg
	return n
}

func MustGetGameGlobalConfig() *GameGlobalConfig {
	serviceGlobalConfig, err := ncluster.C.GetServiceGlobalConfig(GameServiceName)
	if err != nil {
		panic(err)
	}
	return serviceGlobalConfig.(*GameGlobalConfig)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GameConfig 游服配置
type GameConfig struct {
	nservice.Config
}

func (cfg *GameConfig) TidyAndCheck() error {
	if err := cfg.Config.TidyAndCheck(); err != nil {
		return fmt.Errorf("invalid game[%d] config, %s", cfg.GetID(), err)
	}
	return nil
}

func (cfg GameConfig) Clone() nexport.IServiceConfig {
	n := &GameConfig{}
	*n = cfg
	return n
}

func MustGetGameConfig(serviceID nexport.ServiceID) *GameConfig {
	serviceConfig, err := ncluster.C.GetServiceConfig(GameServiceName, serviceID)
	if err != nil {
		panic(err)
	}
	return serviceConfig.(*GameConfig)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// WorldConfig 世界服配置
type WorldConfig struct {
	nservice.Config
	HttpServer  HttpServerConfig
	HttpSign    string
	UseTLS      bool   // 是否启用TLS
	TLSCertFile string // CertFile路径
	TLSKeyFile  string // KeyFile路径
	//Redis       RedisConfig
}

func (cfg *WorldConfig) TidyAndCheck() error {
	if err := cfg.Config.TidyAndCheck(); err != nil {
		return fmt.Errorf("invalid world[%d] config, %s", cfg.GetID(), err)
	}
	if err := cfg.HttpServer.TidyAndCheck(); err != nil {
		return fmt.Errorf("invalid world[%d] HttpServerConfig, %s", cfg.GetID(), err)
	}
	if cfg.HttpSign == "" {
		return fmt.Errorf("invalid world[%d] HttpSign", cfg.GetID())
	}
	if cfg.UseTLS {
		if nutil.IsDirOrFileExist(cfg.TLSCertFile) != nil {
			return fmt.Errorf("invalid world[%d] TLSCertFile", cfg.GetID())
		}
		if nutil.IsDirOrFileExist(cfg.TLSKeyFile) != nil {
			return fmt.Errorf("invalid world[%d] TLSKeyFile", cfg.GetID())
		}
	}
	return nil
}

func (cfg WorldConfig) Clone() nexport.IServiceConfig {
	n := &WorldConfig{}
	*n = cfg
	return n
}

func MustGetWorldConfig() *WorldConfig {
	serviceConfig, err := ncluster.C.GetServiceConfig(WorldServiceName, 1)
	if err != nil {
		panic(err)
	}
	return serviceConfig.(*WorldConfig)
}
