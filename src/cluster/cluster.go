package cluster

import (
	"encoding/json"
	"fmt"
	"log"
	nactor "nggs/actor"
	"path/filepath"
	"sync"

	"github.com/asynkron/protoactor-go/actor"
	"github.com/coreos/etcd/mvcc/mvccpb"
	"go.etcd.io/etcd/client/v3"

	napp "nggs/app"
	ncluster "nggs/cluster"
	nservice "nggs/service"
)

var (
	gLocker   sync.RWMutex
	gWorldPID *actor.PID
)

func Init(configDir string) (err error) {
	etcConfigFilePath := filepath.Join(configDir, "etcd.json")
	err = ncluster.C.Init(etcConfigFilePath,
		(*GlobalConfig)(nil),
		ncluster.ServiceGroupConfigMap{
			napp.ConfigName: {
				Config: (*napp.Config)(nil),
			},
			WorldServiceName: {
				Config: (*WorldConfig)(nil),
			},
			GameServiceName: {
				Config:       (*GameConfig)(nil),
				GlobalConfig: (*GameGlobalConfig)(nil),
			},
			ZonesServiceName: {
				Config:       (*ZonesConfig)(nil),
				GlobalConfig: (*ZonesGlobalConfig)(nil),
			},
			AccountsServiceName: {
				Config:       (*AccountsConfig)(nil),
				GlobalConfig: (*AccountsGlobalConfig)(nil),
			},
			LoginServiceName: {
				Config:       (*LoginConfig)(nil),
				GlobalConfig: (*LoginGlobalConfig)(nil),
			},

			GatewayServiceName: {
				Config:       (*GatewayConfig)(nil),
				GlobalConfig: (*GatewayGlobalConfig)(nil),
			},
		})
	if err != nil {
		return err
	}

	err = ncluster.C.AddWatchCallback(func(err error, section string, serviceName string, serviceID int, ev *clientv3.Event) {
		//log.Printf("[%v][%v][%v][%v][%v]", section, serviceName, serviceID, ev, err)
		if err != nil {
			log.Printf("%s", err)
			return
		}

		gLocker.Lock()
		defer gLocker.Unlock()

		switch section {
		case ncluster.SectionInstance:
			switch serviceName {
			case WorldServiceName:
				switch ev.Type {
				case mvccpb.PUT:
					instance := nservice.NewInstance()
					if err = json.Unmarshal(ev.Kv.Value, instance); err != nil {
						err = fmt.Errorf("put %s[%d] instance fail, unmarshal [%s] fail, %v",
							serviceName, serviceID, string(ev.Kv.Value), err)
						return
					}
					// 避免添加一个空的instance
					if instance.IsEmpty() {
						err = fmt.Errorf("can not put a %s[%d] nil instance", serviceName, serviceID)
						return
					}
					gWorldPID = instance.PID

				case mvccpb.DELETE:
					gWorldPID.Address = ""
					gWorldPID.Id = ""
				}

				//case LoginServiceName:
				//	switch ev.Type {
				//	case mvccpb.PUT:
				//		instance := nservice.NewInstance()
				//		if err = json.Unmarshal(ev.Kv.Value, instance); err != nil {
				//			err = fmt.Errorf("put %s[%d] instance fail, unmarshal [%s] fail, %v",
				//				serviceName, serviceID, string(ev.Kv.Value), err)
				//			return
				//		}
				//		// 服务启动时会put一个空的instance来占位，所以需要避免添加一个空的instance
				//		if instance.IsEmpty() {
				//			err = fmt.Errorf("can not put a %s[%d] nil instance", serviceName, serviceID)
				//			return
				//		}
				//		gLoginPID = instance.PID
				//
				//	case mvccpb.DELETE:
				//		gLoginPID.Address = ""
				//		gLoginPID.Id = ""
				//	}
				//
				//case PaymentServiceName:
				//	switch ev.Type {
				//	case mvccpb.PUT:
				//		instance := nservice.NewInstance()
				//		if err = json.Unmarshal(ev.Kv.Value, instance); err != nil {
				//			err = fmt.Errorf("put %s[%d] instance fail, unmarshal [%s] fail, %v",
				//				serviceName, serviceID, string(ev.Kv.Value), err)
				//			return
				//		}
				//		// 服务启动时会put一个空的instance来占位，所以需要避免添加一个空的instance
				//		if instance.IsEmpty() {
				//			err = fmt.Errorf("can not put a %s[%d] nil instance", serviceName, serviceID)
				//			return
				//		}
				//		gPaymentPID = instance.PID
				//
				//	case mvccpb.DELETE:
				//		gPaymentPID.Address = ""
				//		gPaymentPID.Id = ""
				//	}
				//
				//case WeChatServiceName:
				//	switch ev.Type {
				//	case mvccpb.PUT:
				//		instance := nservice.NewInstance()
				//		if err = json.Unmarshal(ev.Kv.Value, instance); err != nil {
				//			err = fmt.Errorf("put %s[%d] instance fail, unmarshal [%s] fail, %v",
				//				serviceName, serviceID, string(ev.Kv.Value), err)
				//			return
				//		}
				//		// 服务启动时会put一个空的instance来占位，所以需要避免添加一个空的instance
				//		if instance.IsEmpty() {
				//			err = fmt.Errorf("can not put a %s[%d] nil instance", serviceName, serviceID)
				//			return
				//		}
				//		gWeChatPID = instance.PID
				//
				//	case mvccpb.DELETE:
				//		gWeChatPID.Address = ""
				//		gWeChatPID.Id = ""
				//	}
			}
		}
	})
	if err != nil {
		return
	}

	return
}

func Close() {
	ncluster.C.Close()
}

func LeaseID() clientv3.LeaseID {
	return ncluster.C.LeaseID()
}

func PutInstance(serviceName string, serviceID int, instance *nservice.Instance) (err error) {
	err = ncluster.C.PutServiceInstance(serviceName, serviceID, instance)
	return
}

func GetInstance(serviceName string, serviceID int) (instance *nservice.Instance, err error) {
	return ncluster.C.GetServiceInstance(serviceName, serviceID)
}

// 尝试锁定在etcd上的坑位
func TryLockPosition(serviceName string, serviceID int, instance *nservice.Instance) error {
	return ncluster.C.TryLockServicePosition(serviceName, serviceID, instance)
}

func IsReady() bool {
	return true
}

func AddWatchCallback(cb ncluster.WatchCallback) (err error) {
	if cb == nil {
		err = fmt.Errorf("call back is nil")
		return
	}
	return ncluster.C.AddWatchCallback(func(err error, section string, serviceName string, serviceID int, ev *clientv3.Event) {
		gLocker.Lock()
		defer gLocker.Unlock()
		cb(err, section, serviceName, serviceID, ev)
	})
}

func ServiceInfoNum(serviceName string) (num int, err error) {
	return ncluster.C.GetServiceInfoNum(serviceName)
}

func ServiceInstanceNum(serviceName string) (num int, err error) {
	return ncluster.C.GetServiceInstanceNum(serviceName)
}

func EachServiceInfo(serviceName string, fn func(info *nservice.Info) (continued bool)) (err error) {
	infos, err := ncluster.C.GetServiceGroupAllInfo(serviceName)
	if err != nil {
		return
	}
	for _, i := range infos {
		if !fn(i) {
			break
		}
	}
	return
}

func EachServiceInstance(serviceName string, fn func(info *nservice.Info) (continued bool)) (err error) {
	infos, err := ncluster.C.GetServiceGroupAllInfo(serviceName)
	if err != nil {
		return
	}
	for _, i := range infos {
		if i.Instance == nil || i.Instance.IsEmpty() {
			continue
		}
		if !fn(i) {
			break
		}
	}
	return
}

func RoundRobinPickGateway() (serviceInfo *nservice.Info, err error) {
	serviceInfo, err = ncluster.C.RoundRobinPickService(GatewayServiceName)
	if err != nil {
		return
	}
	return
}

func WorldPID() *actor.PID {
	return gWorldPID.Clone()
}

func SendMsgToWorld(msg interface{}) {
	nactor.RootContext().Send(WorldPID(), msg)
}

//func RequestMsgFromWorldWithTimeout(send interface{}, timeout time.Duration) (recv interface{}, err error) {
//	f := nactor.RootContext().RequestFuture(WorldPID(), send, timeout)
//	recv, err = f.Result()
//	if err != nil {
//		return nil, err
//	}
//	return
//}
//
//func RequestMsgFromWorld(send interface{}) (recv interface{}, err error) {
//	return RequestMsgFromWorldWithTimeout(send, 5*time.Second)
//}
