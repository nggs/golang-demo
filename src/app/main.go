package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"model"
	"runtime/debug"
	"time"

	napp "nggs/app"

	"cluster"
	"sd"

	"server/src/entities"
	"server/src/services"
)

var (
	flagConfigDir     = flag.String("cfg", "../config", "config dir, default=../config")
	flagAppID         = flag.Int("id", 1, "app id, default=1")
	flagInitDevEnv    = flag.Bool("init-dev-env", false, "init dev environment, default=false")
	flagStaticDataDir = flag.String("sd", "../data/json", "static data dir, default=../data/json")
)

func initDevEnv() {
	var worldID = 1

	dbCfg := cluster.MustGetMainDBConfig()

	sc := model.NewSimpleClient()
	defer sc.Release()

	err := sc.Init(dbCfg.Url, 1, dbCfg.Name)
	if err != nil {
		log.Panicf("init dev env fail: %s", err)
		return
	}

	dbSession := sc.GetSession()
	defer sc.PutSession(dbSession)

	now := time.Now()
	nowUnix := now.Unix()

	world := model.Get_World()
	world.ID = int32(worldID)
	err = world.Insert(dbSession, sc.DBName())
	if err != nil {
		log.Printf("create world %d fail: %s", worldID, err)
	} else {
		log.Printf("create world %d success", worldID)
	}

	var zoneID = 1
	zone := model.Get_Zone()
	zone.ID = int32(zoneID)
	zone.OpenTime = nowUnix
	err = zone.Insert(dbSession, sc.DBName())
	if err != nil {
		log.Printf("create zone %d fail: %s", zoneID, err)
	} else {
		log.Printf("create zone %d success", zoneID)
	}

	//var arenaID = 1
	//arena := model.Get_Arena()
	//arena.ID = int32(arenaID)
	//// 竞技场开始时刻固定是每周一0点0分0秒
	//arena.OpenTime = util.GetThisWeekMondayBeginTime(now).Unix()
	//err = arena.Insert(dbSession, sc.DBName())
	//if err != nil {
	//	log.Printf("create arena %d fail: %s", arenaID, err)
	//} else {
	//	log.Printf("create arena %d success", arenaID)
	//}
	//
	//var seniorArenaID = 1
	//seniorArena := model.Get_SeniorArena()
	//seniorArena.ID = int32(seniorArenaID)
	//// 高阶竞技场开始时刻固定是每周一0点0分0秒
	//seniorArena.OpenTime = util.GetThisWeekMondayBeginTime(now).Unix()
	//err = seniorArena.Insert(dbSession, sc.DBName())
	//if err != nil {
	//	log.Printf("create senior arena %d fail: %s", seniorArenaID, err)
	//} else {
	//	log.Printf("create senior arena %d success", seniorArenaID)
	//}
	//
	////初始化世界聊天
	//worldChat := model.Get_Chat()
	//worldChat.ID = "1"
	//worldChat.Type = int32(sd.E_ChatChannelType_World)
	//worldChat.Name = "世界聊天"
	//worldChat.CreateTime = nowUnix
	//err = worldChat.Insert(dbSession, sc.DBName())
	//if err != nil {
	//	log.Printf("create world chat %s fail: %s", worldChat.ID, err)
	//} else {
	//	log.Printf("create world chat %s success", worldChat.ID)
	//}
}

func main() {
	defer func() {
		if r := recover(); r != nil {
			s := fmt.Sprintf("%v\n%s", r, debug.Stack())
			coreDumpFilePath := fmt.Sprintf("coredump.%d.%d.txt", *flagAppID, time.Now().Unix())
			err := ioutil.WriteFile(coreDumpFilePath, []byte(s), 777)
			if err != nil {
				log.Printf("write core dump file fail, %s\n%s", err, s)
			} else {
				time.Sleep(3 * time.Second)
				panic(r)
			}
		}
	}()

	flag.Parse()

	if *flagStaticDataDir != "" {
		if !sd.LoadAll(*flagStaticDataDir) {
			panic("加载静态表失败")
		}
		if !sd.AfterLoadAll(*flagStaticDataDir) {
			panic("加载静态表后处理失败")
		}
		log.Printf("加载静态表成功")
	}

	if err := cluster.Init(*flagConfigDir); err != nil {
		log.Panicf("init cluster fail, %s", err)
	}
	defer cluster.Close()

	if *flagInitDevEnv {
		initDevEnv()
		return
	}

	services.Init()
	entities.Init()

	appConfig := cluster.MustGetAppConfig(*flagAppID)

	app := napp.New(appConfig, nil)
	if err := app.Run(); err != nil {
		log.Panicf("run app fail, %s", err)
	}
}
