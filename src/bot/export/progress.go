package export

import (
	nactor "nggs/actor"
	nevent "nggs/event"
	npb "nggs/network/protocol/protobuf/v3"
)

type IProgress interface {
	IProgress()
	Init() error
	Run()
	HandleMessageStep(iMsg npb.IMessage, args ...interface{}) (noHandler bool)
	HandleEventStep(iEvent nevent.IEvent, args ...interface{}) (noHandler bool)
	HandleTimerStep(id nactor.TimerID, tag nactor.TimerID) (noHandler bool)
}
