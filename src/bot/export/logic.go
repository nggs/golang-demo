package export

import "time"

type ILCommon interface {
	LCommon()
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	SendGM(command string, args ...string)
}

type ILLogin interface {
	LLogin()
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	FetchLoginInfo() (err error)
	GatewayAddress() string
	LoginToken() string
	ConnectToGateway() (err error)
	Login() (err error)
	DelayLogin(delay time.Duration)
	BeginLoginTime() time.Time
	IsLoginFinished() bool
}
