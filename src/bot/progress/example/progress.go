package example

import (
	"time"

	"nggs/event"

	Event "server/src/bot/event"
	"server/src/bot/progress"
)

type Progress struct {
	progress.Super
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (p *Progress) Init() (err error) {
	err = p.Super.Init()
	if err != nil {
		return
	}
	err = p.regAllEventHandler()
	if err != nil {
		return
	}
	err = p.regAllMsgHandler()
	if err != nil {
		return
	}

	// todo 按顺序添加测试流程

	// 1. 启动后先登录
	p.AddEventStep(Event.OnStartedID, func(iEv event.IEvent, args ...interface{}) (moveOffset int) {
		delay := time.Duration(p.Cfg().ID()) * 100 * time.Millisecond
		//p.Debug("after %v will login", delay)
		p.ILLogin.DelayLogin(delay)
		return 1
	})

	// 2. todo 登录后
	p.AddEventStep(Event.OnLoginID, func(iEv event.IEvent, args ...interface{}) (moveOffset int) {
		//p.ILCommon.SendGM("money", "2", "1000")
		//p.NewTimer(500*time.Millisecond, func(id myactor.TimerID, tag myactor.TimerID) {
		//
		//	p.ILCommon.SendGM("money", "2", "-1")
		//})
		//p.NewTimer(1000*time.Millisecond, func(id myactor.TimerID, tag myactor.TimerID) {
		//	p.ILCommon.SendGM("item", "140", "100")
		//})
		//p.NewTimer(1500*time.Millisecond, func(id myactor.TimerID, tag myactor.TimerID) {
		//	p.ILCommon.SendGM("item", "140", "-1")
		//})

		//p.ILCommon.SendGM("item", "120", "1")
		//p.NewTimer(500*time.Millisecond, func(id myactor.TimerID, tag myactor.TimerID) {
		//	send := Msg.Get_C2S_UseItem()
		//	send.TID = 120
		//	send.Num = 1
		//	p.SendMessage(send)
		//})
		//
		//p.NewTimer(1000*time.Millisecond, func(id myactor.TimerID, tag myactor.TimerID) {
		//	p.ILCommon.SendGM("item", "126", "1")
		//})
		//p.NewTimer(1500*time.Millisecond, func(id myactor.TimerID, tag myactor.TimerID) {
		//	send := Msg.Get_C2S_UseItem()
		//	send.TID = 126
		//	send.Num = 1
		//	p.SendMessage(send)
		//})
		//
		//p.NewTimer(2000*time.Millisecond, func(id myactor.TimerID, tag myactor.TimerID) {
		//	p.ILCommon.SendGM("item", "130", "1")
		//})
		//p.NewTimer(2500*time.Millisecond, func(id myactor.TimerID, tag myactor.TimerID) {
		//	send := Msg.Get_C2S_UseItem()
		//	send.TID = 130
		//	send.Num = 1
		//	p.SendMessage(send)
		//})
		//
		//p.NewTimer(3000*time.Millisecond, func(id myactor.TimerID, tag myactor.TimerID) {
		//	p.ILCommon.SendGM("item", "136", "1")
		//})
		//p.NewTimer(3500*time.Millisecond, func(id myactor.TimerID, tag myactor.TimerID) {
		//	send := Msg.Get_C2S_UseItem()
		//	send.TID = 136
		//	send.Num = 1
		//	p.SendMessage(send)
		//})

		return 1
	})

	return nil
}
