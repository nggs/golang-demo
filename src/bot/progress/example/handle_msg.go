package example

import (
	protocol "nggs/network/protocol/protobuf/v1"
)

func (p *Progress) regAllMsgHandler() (err error) {
	//p.RegisterMessageHandler(MsgPB.MsgItemPacketCapabilityGS2C_MessageID(), p.handleMsgItemPacketCapabilityGS2C)
	return
}

func (p *Progress) handleMsgItemPacketCapabilityGS2C(iMsg protocol.IMessage, args ...interface{}) {
	//p.Debug("recv MsgItemPacketCapabilityGS2C[%v]", iMsg)
}
