package example

import (
	"server/src/bot/export"
	"server/src/bot/progress"
)

const (
	ID = progress.Example
)

var Dep []string

func NewProgress(iBot export.IBot) export.IProgress {
	p := &Progress{
		Super: progress.NewSuper(iBot, ID),
	}
	return p
}

func Init() {
	progress.RegisterFactory(ID, Dep, NewProgress)
}
