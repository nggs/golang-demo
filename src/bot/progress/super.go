package progress

import (
	"fmt"

	nactor "nggs/actor"
	nevent "nggs/event"
	npb "nggs/network/protocol/protobuf/v3"

	"server/src/bot/export"
	"server/src/bot/logic"
)

type StepType = int

const (
	StepTypeMessage StepType = iota
	StepTypeEvent
	StepTypeTimer
)

type StepMessageHandler = func(iMsg npb.IMessage, args ...interface{}) (moveOffset int)
type StepEventHandler = func(iEvent nevent.IEvent, args ...interface{}) (moveOffset int)
type StepTimerHandler = func(id nactor.TimerID, tag nactor.TimerID) (moveOffset int)

type step struct {
	t         StepType
	condition interface{}
	handler   interface{}
}

type StepManager struct {
	steps    []step
	cursor   int
	finished bool
}

func NewStepManager() StepManager {
	return StepManager{
		steps:    []step{},
		cursor:   0,
		finished: false,
	}
}

func (sm *StepManager) Move(offset int) (err error) {
	if sm.finished {
		// 流程已结束
		return
	}
	cursor := sm.cursor + offset
	stepNum := len(sm.steps)
	if cursor < 0 || cursor > stepNum {
		err = fmt.Errorf("invalid offset[%d], current cursor=[%d]", offset, sm.cursor)
		return
	}
	sm.cursor = cursor
	if sm.cursor == stepNum {
		sm.finished = true
	}
	return
}

//func (sm *StepManager) Reset() {
//	sm.cursor = 0
//	sm.finished = false
//}

type Super struct {
	export.IBot
	progressName string

	StepMgr StepManager

	ILLogin  export.ILLogin
	ILCommon export.ILCommon
}

func NewSuper(iBot export.IBot, progressName string) Super {
	return Super{
		IBot:         iBot,
		progressName: progressName,
		StepMgr:      NewStepManager(),
	}
}

func (Super) IProgress() {

}

func (s *Super) Init() error {
	s.ILLogin = s.GetLogic(logic.Login).(export.ILLogin)
	if s.ILLogin == nil {
		return fmt.Errorf("get Login logic fail")
	}
	s.ILCommon = s.GetLogic(logic.Common).(export.ILCommon)
	if s.ILCommon == nil {
		return fmt.Errorf("get Common logic fail")
	}
	return nil
}

func (Super) Run() {

}

func (s *Super) AddMessageStep(messageID npb.MessageID, handler StepMessageHandler) *Super {
	s.StepMgr.steps = append(s.StepMgr.steps, step{t: StepTypeMessage, condition: messageID, handler: handler})
	return s
}

func (s *Super) AddEventStep(eventID nevent.ID, handler StepEventHandler) *Super {
	s.StepMgr.steps = append(s.StepMgr.steps, step{t: StepTypeEvent, condition: eventID, handler: handler})
	return s
}

func (s *Super) AddTimerStep(timerTag nactor.TimerID, handler StepTimerHandler) *Super {
	s.StepMgr.steps = append(s.StepMgr.steps, step{t: StepTypeTimer, condition: timerTag, handler: handler})
	return s
}

func (s *Super) HandleMessageStep(iMsg npb.IMessage, args ...interface{}) (noHandler bool) {
	if s.StepMgr.finished {
		noHandler = true
		return
	}

	if s.StepMgr.cursor < 0 || s.StepMgr.cursor >= len(s.StepMgr.steps) {
		s.Error("HandleMessageStep fail, cursor[%d] out of range", s.StepMgr.cursor)
		noHandler = true
		return
	}

	step := s.StepMgr.steps[s.StepMgr.cursor]
	if step.t != StepTypeMessage {
		noHandler = true
		return
	}

	target := step.condition.(npb.MessageID)
	if target != iMsg.Head().MessageID {
		noHandler = true
		return
	}

	handler := step.handler.(StepMessageHandler)
	moveOffset := handler(iMsg, args...)
	if moveOffset != 0 {
		err := s.StepMgr.Move(moveOffset)
		if err != nil {
			s.Error("move step fail, %v", err)
		}
		if s.StepMgr.finished {
			s.Debug("progress[%s] finish", s.progressName)
		}
	}

	return
}

func (s *Super) HandleEventStep(iEvent nevent.IEvent, args ...interface{}) (noHandler bool) {
	if s.StepMgr.finished {
		return
	}

	if s.StepMgr.cursor < 0 || s.StepMgr.cursor >= len(s.StepMgr.steps) {
		s.Error("HandleEventStep fail, cursor[%d] out of range", s.StepMgr.cursor)
		return
	}

	step := s.StepMgr.steps[s.StepMgr.cursor]
	if step.t != StepTypeEvent {
		return
	}

	target := step.condition.(nevent.ID)
	if target != iEvent.EventID() {
		noHandler = true
		return
	}

	handler := step.handler.(StepEventHandler)
	moveOffset := handler(iEvent, args...)
	if moveOffset != 0 {
		err := s.StepMgr.Move(moveOffset)
		if err != nil {
			s.Error("move step fail, %v", err)
		}
		if s.StepMgr.finished {
			s.Debug("progress[%s] finish", s.progressName)
		}
	}

	return
}

func (s *Super) HandleTimerStep(id nactor.TimerID, tag nactor.TimerID) (noHandler bool) {
	if s.StepMgr.finished {
		return
	}

	if s.StepMgr.cursor < 0 || s.StepMgr.cursor >= len(s.StepMgr.steps) {
		s.Error("HandleTimerStep fail, cursor[%d] out of range", s.StepMgr.cursor)
		return
	}

	step := s.StepMgr.steps[s.StepMgr.cursor]
	if step.t != StepTypeTimer {
		return
	}

	target := step.condition.(nactor.TimerID)
	if target != tag {
		noHandler = true
		return
	}

	handler := step.handler.(StepTimerHandler)
	moveOffset := handler(id, tag)
	if moveOffset != 0 {
		err := s.StepMgr.Move(moveOffset)
		if err != nil {
			s.Error("move step fail, %v", err)
		}
		if s.StepMgr.finished {
			s.Debug("progress[%s] finish", s.progressName)
		}
	}

	return
}
