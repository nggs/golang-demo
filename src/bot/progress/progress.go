package progress

import (
	"log"
	"server/src/bot/export"
	"strings"
)

type ID = string

const (
	Example ID = "example"
)

type Factory struct {
	ID  ID
	Dep []string
	New func(bot export.IBot) export.IProgress
}

var gFactoryMap = map[ID]*Factory{}

func GetFactory(id ID) (*Factory, bool) {
	if factory, ok := gFactoryMap[id]; ok {
		return factory, true
	}
	return nil, false
}

func RegisterFactory(id ID, dep []string, New func(bot export.IBot) export.IProgress) {
	if _, ok := GetFactory(id); ok {
		log.Panicf("progress factory already exist, id=[%s]", id)
	}

	factory := &Factory{
		ID:  id,
		Dep: dep,
		New: New,
	}

	gFactoryMap[factory.ID] = factory
}

func GenerateProgressMap(bot export.IBot) (m map[ID]export.IProgress) {
	m = map[ID]export.IProgress{}

	if bot.Cfg().Progress() == "" {
		return
	}

	progressIDs := strings.Split(bot.Cfg().Progress(), ",")

	for _, id := range progressIDs {
		f, ok := gFactoryMap[id]
		if !ok || f == nil {
			log.Panicf("progress factory not exist, id=[%s]", id)
		}

		for _, depID := range f.Dep {
			if depID == "" {
				continue
			}
			depF, ok := gFactoryMap[depID]
			if !ok || depF == nil {
				log.Panicf("[%s] dependent progress [%s] not exist", id, depID)
			}
			if _, ok := m[depID]; !ok {
				m[depID] = depF.New(bot)
			}
		}

		m[id] = f.New(bot)
	}

	return m
}
