package event

import (
	"encoding/json"
)

const (
	OnStartedID uint16 = iota + 1
	OnConnectID
	OnDisconnectID
	OnRegisterID
	OnVisitorID
	OnBindID
	OnLoginID
)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type OnStarted struct {
}

func (OnStarted) EventID() uint16 {
	return OnStartedID
}

func (OnStarted) Event() {
}

func (e OnStarted) String() string {
	ba, _ := json.Marshal(e)
	return string(ba)
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type OnConnect struct {
}

func (OnConnect) EventID() uint16 {
	return OnConnectID
}

func (OnConnect) Event() {
}

func (e OnConnect) String() string {
	ba, _ := json.Marshal(e)
	return string(ba)
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type OnDisconnect struct {
}

func (OnDisconnect) EventID() uint16 {
	return OnDisconnectID
}

func (OnDisconnect) Event() {
}

func (e OnDisconnect) String() string {
	ba, _ := json.Marshal(e)
	return string(ba)
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type OnRegister struct {
}

func (OnRegister) EventID() uint16 {
	return OnRegisterID
}

func (OnRegister) Event() {
}

func (e OnRegister) String() string {
	ba, _ := json.Marshal(e)
	return string(ba)
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type OnVisitor struct {
}

func (OnVisitor) EventID() uint16 {
	return OnVisitorID
}

func (OnVisitor) Event() {
}

func (e OnVisitor) String() string {
	ba, _ := json.Marshal(e)
	return string(ba)
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type OnBind struct {
}

func (OnBind) EventID() uint16 {
	return OnBindID
}

func (OnBind) Event() {
}

func (e OnBind) String() string {
	ba, _ := json.Marshal(e)
	return string(ba)
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type OnLogin struct {
}

func (OnLogin) EventID() uint16 {
	return OnLoginID
}

func (OnLogin) Event() {
}

func (e OnLogin) String() string {
	ba, _ := json.Marshal(e)
	return string(ba)
}
