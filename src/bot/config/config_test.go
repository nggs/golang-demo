package config

import "testing"

const testJsonDir = "../../../data/bot"

func Test(t *testing.T) {
	if !LoadAll(testJsonDir) {
		t.Error("加载机器人配置失败")
		return
	}
	if !AfterLoadAll(testJsonDir) {
		t.Error("加载机器人配置后处理失败")
		return
	}
}
