package config

import (
	"github.com/json-iterator/go/extra"
)

func init() {
	extra.SupportPrivateFields()
}
