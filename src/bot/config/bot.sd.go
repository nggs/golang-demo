// 本文件由gen_static_data_code生成
// 请遵照提示添加修改！！！

package config

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"time"

	jsoniter "github.com/json-iterator/go"

	"nggs/random"
	"nggs/util"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加头部扩展代码
//<Head>//</Head>
//////////////////////////////////////////////////////////////////////////////////////////////////

var _ = fmt.Printf
var _ = time.Now
var _ = math.MaxInt32
var _ = strconv.Itoa
var _ = util.IsSameDayUnixTimeStamp
var _ = random.String

// 机器人配置表
type Bot struct {
	id                 int           // ID
	enable             bool          // 是否启用
	account_name       string        // 账号名
	account_password   string        // 账号密码
	login_server_addr  string        // 登录服地址
	server_id          int           // 登录区服id
	progress           string        // 测试流程
	trace_msg          bool          // 是否跟踪消息
	min_send_msg_delay time.Duration // 发送消息最小间隔（单位：毫秒）
	max_send_msg_delay time.Duration // 发送消息最大间隔（单位：毫秒）

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体扩展字段
	//<StructExtend>//</StructExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func NewBot() *Bot {
	sd := &Bot{}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体New代码
	//<StructNew>//</StructNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return sd
}

// ID
func (sd Bot) ID() int {
	return sd.id
}

// 是否启用
func (sd Bot) Enable() bool {
	return sd.enable
}

// 账号名
func (sd Bot) AccountName() string {
	return sd.account_name
}

// 账号密码
func (sd Bot) AccountPassword() string {
	return sd.account_password
}

// 登录服地址
func (sd Bot) LoginServerAddr() string {
	return sd.login_server_addr
}

// 登录区服id
func (sd Bot) ServerID() int {
	return sd.server_id
}

// 测试流程
func (sd Bot) Progress() string {
	return sd.progress
}

// 是否跟踪消息
func (sd Bot) TraceMsg() bool {
	return sd.trace_msg
}

// 发送消息最小间隔（单位：毫秒）
func (sd Bot) MinSendMsgDelay() time.Duration {
	return sd.min_send_msg_delay
}

// 发送消息最大间隔（单位：毫秒）
func (sd Bot) MaxSendMsgDelay() time.Duration {
	return sd.max_send_msg_delay
}

func (sd Bot) Clone() *Bot {
	n := NewBot()
	*n = sd
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加结构体Clone代码
	//<StructClone>//</StructClone>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return n
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 机器人配置表全局属性
type BotGlobal struct {
	startIntervalMSec time.Duration // 机器人启动间隔，单位：毫秒

}

// 机器人启动间隔，单位：毫秒
func (g BotGlobal) StartIntervalMSec() time.Duration {
	return g.startIntervalMSec
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
type BotManager struct {
	Datas  []*Bot
	Global BotGlobal
	size   int
	//////////////////////////////////////////////////////////////////////////////////////////////////
	byID map[int]*Bot // UniqueIndex

	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager扩展字段
	//<ManagerExtend>//</ManagerExtend>
	//////////////////////////////////////////////////////////////////////////////////////////////////
}

func newBotManager() *BotManager {
	mgr := &BotManager{
		Datas: []*Bot{},
		byID:  map[int]*Bot{},
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager的New代码
	//<ManagerNew>//</ManagerNew>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return mgr
}

func (mgr *BotManager) load(path string) (success bool) {
	success = true

	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Printf("获取[%s]的绝对路径失败, %s\n", path, err)
		success = false
		return false
	}

	bs, err := ioutil.ReadFile(absPath)
	if err != nil {
		log.Printf("读取[%s]的内容失败, %s\n", path, err)
		success = false
		return
	}

	err = jsoniter.Unmarshal(bs, mgr)
	if err != nil {
		log.Printf("解析[%s]失败, %s\n", path, err)
		success = false
		return
	}

	mgr.size = len(mgr.Datas)

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载前代码
	//<ManagerBeforeLoad>//</ManagerBeforeLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	for i, d := range mgr.Datas {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO 添加结构体加载代码
		//<StructLoad>//</StructLoad>
		//////////////////////////////////////////////////////////////////////////////////////////////////
		err = mgr.check(path, i+1, d)
		if err != nil {
			log.Println(err)
			success = false
			continue
		}
		mgr.byID[d.id] = d

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加manager加载后代码
	//<ManagerAfterLoad>//</ManagerAfterLoad>
	//////////////////////////////////////////////////////////////////////////////////////////////////

	return
}

func BotSize() int {
	return botMgr.size
}

func (mgr BotManager) check(path string, row int, sd *Bot) error {
	if _, ok := botMgr.byID[sd.id]; ok {
		return fmt.Errorf("[%s]第[%d]行的id重复", path, row)
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加检查代码
	//<Check>//</Check>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return nil
}

func (mgr *BotManager) afterLoadAll(path string) (success bool) {
	success = true
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO 添加加载后处理代码
	//<AfterLoadAll>//</AfterLoadAll>
	//////////////////////////////////////////////////////////////////////////////////////////////////
	return
}

func (mgr *BotManager) each(f func(sd *Bot) (continued bool)) {
	for _, sd := range mgr.Datas {
		if !f(sd) {
			break
		}
	}
}

func (mgr *BotManager) findIf(f func(sd *Bot) (find bool)) *Bot {
	for _, sd := range mgr.Datas {
		if f(sd) {
			return sd
		}
	}
	return nil
}

func EachBot(f func(sd Bot) (continued bool)) {
	for _, sd := range botMgr.Datas {
		if !f(*sd) {
			break
		}
	}
}

func FindBotIf(f func(sd Bot) bool) (Bot, bool) {
	for _, sd := range botMgr.Datas {
		if f(*sd) {
			return *sd, true
		}
	}
	return nilBot, false
}

func GetBotByID(id int) (Bot, bool) {
	temp, ok := botMgr.byID[id]
	if !ok {
		return nilBot, false
	}
	return *temp, true
}

func GetBotGlobal() BotGlobal {
	return botMgr.Global
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TODO 添加尾部扩展代码
//<Tail>//</Tail>
//////////////////////////////////////////////////////////////////////////////////////////////////
