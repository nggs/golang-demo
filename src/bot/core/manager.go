package core

import (
	"fmt"
	"sync"
	"time"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	nlog "nggs/log"
	nutil "nggs/util"

	"server/src/bot/config"
)

var logger = nlog.NewConsoleLogger()

func SetLogger(l nlog.ILogger) {
	logger = l
}

type manager struct {
	*nactor.Actor
	startedWg    sync.WaitGroup
	stoppedWg    sync.WaitGroup
	botStoppedWg sync.WaitGroup
}

func Run() {
	mgr := &manager{}

	mgr.Actor = nactor.New(
		nactor.WithLogger(logger),
		nactor.WithStartedWaitGroup(&mgr.startedWg),
		nactor.WithStoppedWaitGroup(&mgr.stoppedWg),
		nactor.WithOnStarted(mgr.onStarted),
		nactor.WithOnStopped(mgr.onStopped),
		nactor.WithOnActorTerminate(mgr.onActorTerminated),
	)

	err := mgr.Start(nil, "botMgr")
	if err != nil {
		panic(err)
	}

	go func() {
		nutil.WaitExitSignal()
		mgr.Info("receive exit signal")
		_ = mgr.Stop()
	}()

	mgr.WaitForStarted()

	mgr.WaitForStopped()
}

func (mgr *manager) onStarted(ctx actor.Context) {
	mgr.Info("started")

	startIntervalMSec := config.GetBotGlobal().StartIntervalMSec() * time.Millisecond

	mgr.NewTimer(100*time.Millisecond, 0, func(nactor.TimerID, nactor.TimerID) {
		config.EachBot(func(cfg config.Bot) bool {
			if !cfg.Enable() {
				return true
			}
			b := newBot(&mgr.botStoppedWg, cfg)
			name := fmt.Sprintf("bot-%d", cfg.ID())
			if err := b.Start(ctx, name); err != nil {
				mgr.Error("spawn %s fail, %s", name, err)
				return false
			}
			if startIntervalMSec > 0 {
				time.Sleep(startIntervalMSec)
			}
			return true
		})
	})
}

func (mgr *manager) onStopping(ctx actor.Context) {
}

func (mgr *manager) onStopped(ctx actor.Context) {
	mgr.Info("stopped")
	//time.Sleep(1 * time.Second)

	mgr.botStoppedWg.Wait()
}

func (mgr *manager) onActorTerminated(who *actor.PID, ctx actor.Context) {
	mgr.Info("[%s] terminated", who.Id)
}
