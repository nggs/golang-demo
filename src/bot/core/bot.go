package core

import (
	"fmt"
	"runtime/debug"
	"server/src/bot/logic"
	"sync"
	"time"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	"nggs/event"
	nproto "nggs/network/protocol"
	npb "nggs/network/protocol/protobuf/v3"
	nws "nggs/network/websocket/v2"
	nrandom "nggs/random"

	gameMsg "msg"

	"server/src/bot/cache"
	"server/src/bot/config"
	botEvent "server/src/bot/event"
	"server/src/bot/export"
	"server/src/bot/progress"
)

type bot struct {
	*nactor.Actor

	nws.SessionHandler
	session *nws.Session

	progressMap map[progress.ID]export.IProgress

	cfg config.Bot

	cache *cache.Bot

	timerTag nactor.TimerID
}

func newBot(stoppedWg *sync.WaitGroup, cfg config.Bot) *bot {
	b := &bot{
		cfg:   cfg,
		cache: cache.NewBot(),
	}

	b.Actor = nactor.New(
		nactor.WithLogger(logger),
		nactor.WithStoppedWaitGroup(stoppedWg),
		nactor.WithOnReceiveMessage(b.onReceiveMessage),
		nactor.WithOnStarted(b.onStarted),
		nactor.WithOnStopping(b.onStopping),
		nactor.WithOnStopped(b.onStopped),
		nactor.WithLogics(logic.GenerateLogicMap(b)),
		nactor.WithTimer(nil, func(err error, id nactor.TimerID, tag nactor.TimerTag, ctx actor.Context) {
			if err != nil {
				return
			}
			for _, p := range b.progressMap {
				p.HandleTimerStep(id, tag)
			}
		}),
		nactor.WithMessage(gameMsg.Protocol, func(iProtocol npb.IProtocol, iRecv npb.IMessage, ctx actor.Context) (iSend npb.IMessage, args []interface{}, ok bool, err error) {
			if b.cfg.TraceMsg() {
				b.Debug("recv %s, seq=%d", iRecv.String(), iRecv.Head().MessageSeq)
			}
			ok = true
			return
		}, func(err error, iRecv npb.IMessage, iSend npb.IMessage, ctx actor.Context, args ...interface{}) {
			noHandler := true
			if err != nil {
				switch err.(type) {
				case *nproto.ErrNoDispatcher:
					noHandler = true
				}
			} else {
				noHandler = false
			}
			progressNoHandler := true
			for _, p := range b.progressMap {
				progressNoHandler = p.HandleMessageStep(iRecv) && progressNoHandler
			}
			if noHandler && progressNoHandler {
				b.Error("process message %s fail, %s", iRecv.String(), err)
			}
		}),
		nactor.WithEvent(nil, func(err error, iEvent event.IEvent, ctx actor.Context, args ...interface{}) {
			var noHandler bool
			if err != nil {
				if err.Error() != event.ErrNoHandler.Error() {
					b.Error("process event [%#v] fail, %s", iEvent, err)
				}
				noHandler = true
			}
			for _, p := range b.progressMap {
				noHandler = p.HandleEventStep(iEvent) && noHandler
			}
			if noHandler {
				b.Error("no handler to process nevent [%#v]", iEvent)
			}
		}),
	)

	b.progressMap = progress.GenerateProgressMap(b)

	return b
}

func (b *bot) onStarted(ctx actor.Context) {
	if err := b.initProgress(); err != nil {
		b.Error("init progress fail, %s", err)
		_ = b.Stop()
		return
	}
	b.runProgress()

	b.timerTag = nactor.NextTimerTag()

	b.PostEvent(&botEvent.OnStarted{})
}

func (b *bot) onStopping(ctx actor.Context) {
	b.Disconnect()
}

func (b *bot) onStopped(ctx actor.Context) {
}

func (b *bot) onReceiveMessage(ctx actor.Context) {
	defer func() {
		if ir := recover(); ir != nil {
			b.Error("%v\n%s", ir, debug.Stack())
			panic(ir)
		}
	}()

	sender := ctx.Sender()
	switch msg := ctx.Message().(type) {
	default:
		b.Error("receive unsupported message [%#v] from [%s]", msg, sender)
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (b bot) Cache() *cache.Bot {
	return b.cache
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (b *bot) initProgress() error {
	for _, p := range b.progressMap {
		if err := p.Init(); err != nil {
			return err
		}
	}
	return nil
}

func (b *bot) runProgress() {
	for id, p := range b.progressMap {
		p.Run()
		b.Debug("progress[%s] begin", id)
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (b *bot) HandleConnect(session *nws.Session) error {
	//b.Info("session[%d] connected", session.ID())
	//if !b.IsStopping() {
	//	b.PostEvent(&botEvent.OnConnect{})
	//}
	return nil
}

func (b *bot) HandleDisconnect(session *nws.Session) {
	//b.Info("session[%d] disconnected", session.ID())
	if !b.IsStopping() {
		b.PostEvent(&botEvent.OnDisconnect{})
	}
	return
}

func (b *bot) HandleBinaryMessage(session *nws.Session, data []byte) {
	iMsg, err := gameMsg.Protocol.Decode(data)
	if err != nil {
		b.Error("decode [%v] fail, %s", data, err)
		return
	}
	nactor.RootContext().Send(b.PID(), iMsg)
}

func (b *bot) HandleTextMessage(session *nws.Session, msg []byte) {
	iMsg, err := gameMsg.Protocol.Decode(msg)
	if err != nil {
		b.Error("decode [%v] fail, %s", msg, err)
		return
	}
	nactor.RootContext().Send(b.PID(), iMsg)
}

func (b *bot) HandleError(session *nws.Session, err error) {
	b.Error("session[%d] error happened, %v", session.ID(), err)
}

func (b *bot) HandleClose(session *nws.Session, code int, msg string) error {
	b.Error("closed, code=[%d], msg=[%s]", code, msg)
	return nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (b *bot) Connect(addr string) (err error) {
	b.session, err = nws.C.Dial(addr, 3*time.Second, b)
	if err != nil {
		b.Error("connect to [%s] fail, %v", addr, err)
		return
	}

	b.PostEvent(&botEvent.OnConnect{})

	b.Info("connect to [%s] success", addr)
	return
}

func (b *bot) Disconnect() {
	if b.session != nil {
		_ = b.session.Close()
		b.session = nil
	}
}

func (b *bot) SendMessage(iMsg npb.IMessage) (err error) {
	if b.session == nil {
		err = fmt.Errorf("session is nil, maybe lost connection")
		b.Error("send %s fail, seq=%d, %s", iMsg.Head().MessageSeq, err)
		return
	}

	data, err := gameMsg.Protocol.Encode(iMsg)
	if err != nil {
		err = fmt.Errorf("encode fail, %w", err)
		b.Error("send %s fail, seq=%d, %s", iMsg.Head().MessageSeq, err)
		return
	}

	err = b.session.WriteBinary(data)
	if err != nil {
		b.Error("send %s fail, seq=%d, %s", iMsg.String(), iMsg.Head().MessageSeq, err)
		return
	}

	if b.cfg.TraceMsg() {
		b.Debug("send %s success, seq=%d", iMsg.String(), iMsg.Head().MessageSeq)
	}

	return
}

func (b *bot) DelaySendMessage(delay time.Duration, iMsg npb.IMessage) {
	if b.cfg.TraceMsg() {
		b.Debug("after %v while send %s, seq=%d", delay, iMsg.String(), iMsg.Head().MessageSeq)
	}
	b.NewTimer(delay, 0, func(id nactor.TimerID, tag nactor.TimerID) {
		_ = b.SendMessage(iMsg)
	})
}

func (b *bot) RandomDelaySendMessage(iMsg npb.IMessage) {
	delay := nrandom.TimeDuration(b.cfg.MinSendMsgDelay(), b.cfg.MaxSendMsgDelay()) * time.Millisecond
	b.DelaySendMessage(delay, iMsg)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (b bot) Cfg() config.Bot {
	return b.cfg
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (b bot) TimerTag() nactor.TimerID {
	return b.timerTag
}
