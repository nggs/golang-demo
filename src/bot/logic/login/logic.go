package login

import (
	"server/src/bot/logic"
	"time"

	nexport "nggs/export"
)

type Logic struct {
	logic.Super

	addr          string
	token         string
	needCreate    bool
	beginTime     time.Time
	loginFinished bool
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (Logic) ID() nexport.LogicID {
	return ID
}

func (Logic) LLogin() {

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) Init() (err error) {
	err = l.Super.Init()
	if err != nil {
		return
	}
	err = l.regAllEventHandler()
	if err != nil {
		return
	}
	err = l.regAllMsgHandler()
	if err != nil {
		return
	}
	return nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) Run() {
	//l.NewTimer(time.Duration(l.Cfg().ID())*100*time.Millisecond, func(myactor.TimerID, myactor.TimerID) {
	//	// 从login取登陆信息（包括token和gateway地址）
	//	err := l.FetchLoginInfo()
	//	if err != nil {
	//		l.Error("fetch token fail, %s", err)
	//		l.Stop()
	//		return
	//	}
	//	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	// 连接gateway
	//	err = l.Connect(l.addr)
	//	if err != nil {
	//		l.Stop()
	//		return
	//	}
	//})
}
