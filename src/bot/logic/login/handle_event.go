package login

import (
	"math"
	"time"

	nrandom "nggs/random"

	"github.com/asynkron/protoactor-go/actor"

	"nggs/event"

	Event "server/src/bot/event"

	"msg"
)

func (l *Logic) regAllEventHandler() (err error) {
	l.RegisterEventHandler(Event.OnConnectID, l.handleOnConnect)
	l.RegisterEventHandler(Event.OnDisconnectID, l.handleOnDisconnect)
	return
}

func (l *Logic) handleOnConnect(iEvent event.IEvent, ctx actor.Context, args ...interface{}) {
	l.beginTime = time.Now()
	l.loginFinished = false

	send := msg.Get_C2S_Login()
	send.MessageSeq = uint32(nrandom.Int64(0, math.MaxUint32))
	send.Token = l.token
	_ = l.SendMessage(send)
}

func (l *Logic) handleOnDisconnect(iEvent event.IEvent, ctx actor.Context, args ...interface{}) {
	l.Debug("disconnect from [%s]", l.addr)
	_ = l.Stop()
}
