package login

import (
	"fmt"
	"time"

	nactor "nggs/actor"
	nhttp "nggs/network/http"

	loginMsg "server/src/services/login/http_msg"
)

func (l *Logic) FetchLoginInfo() (err error) {
	var regResp loginMsg.ResponseRegister
	regUrl := fmt.Sprintf("http://%s/register?sign=1&acc=%s&pwd=%s",
		l.Cfg().LoginServerAddr(), l.Cfg().AccountName(), l.Cfg().AccountPassword())
	err = nhttp.GetJson(regUrl, 3*time.Second, "", nil, &regResp)
	if err != nil {
		return
	}

	var loginResp loginMsg.ResponseLogin
	loginUrl := fmt.Sprintf("http://%s/login?sign=1&acc=%s&pwd=%s&serverID=%d",
		l.Cfg().LoginServerAddr(), l.Cfg().AccountName(), l.Cfg().AccountPassword(), l.Cfg().ServerID())
	err = nhttp.GetJson(loginUrl, 3*time.Second, "", nil, &loginResp)
	if err != nil {
		return
	}

	if loginResp.ErrorCode != loginMsg.EC_Success {
		return fmt.Errorf("login fail, %s", loginResp.ErrorMessage)
	}

	// 保存登录信息
	l.token = loginResp.Token
	l.addr = fmt.Sprintf("ws://%s", loginResp.Addr)

	return
}

func (l Logic) GatewayAddress() string {
	return l.addr
}

func (l Logic) LoginToken() string {
	return l.token
}

func (l Logic) ConnectToGateway() (err error) {
	return l.Connect(l.addr)
}

func (l *Logic) Login() (err error) {
	// 从login取登陆信息（包括token和gateway地址）
	err = l.FetchLoginInfo()
	if err != nil {
		l.Error("fetch token fail, %s", err)
		_ = l.Stop()
		return
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 连接gateway
	err = l.ConnectToGateway()
	if err != nil {
		_ = l.Stop()
		return
	}
	return
}

func (l *Logic) DelayLogin(delay time.Duration) {
	l.NewTimer(delay, 0, func(nactor.TimerID, nactor.TimerID) {
		// 从login取登陆信息（包括token和gateway地址）
		err := l.FetchLoginInfo()
		if err != nil {
			l.Error("fetch token fail, %s", err)
			_ = l.Stop()
			return
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 连接gateway
		err = l.ConnectToGateway()
		if err != nil {
			_ = l.Stop()
			return
		}
	})
}

func (l Logic) BeginLoginTime() time.Time {
	return l.beginTime
}

func (l Logic) IsLoginFinished() bool {
	return l.loginFinished
}
