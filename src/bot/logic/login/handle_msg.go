package login

import (
	"math"
	"time"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	npb "nggs/network/protocol/protobuf/v3"
	nrandom "nggs/random"

	gameMsg "msg"

	botEvent "server/src/bot/event"
)

func (l *Logic) regAllMsgHandler() (err error) {
	err = l.RegisterMessageHandler(gameMsg.S2C_Login_MessageID(), l.handleLogin)
	if err != nil {
		return
	}
	err = l.RegisterMessageHandler(gameMsg.S2C_Pong_MessageID(), l.handlePong)
	if err != nil {
		return
	}
	return
}

func (l *Logic) handleLogin(iRecv npb.IMessage, iSend npb.IMessage, ctx actor.Context, args ...interface{}) {
	// 开始循环发送心跳包
	l.NewLoopTimer(30*time.Second, 0, func(id nactor.TimerID, tag nactor.TimerID) {
		send := gameMsg.Get_C2S_Ping()
		send.MessageSeq = uint32(nrandom.Int64(0, math.MaxUint32))
		send.Content = nrandom.String(8)
		_ = l.SendMessage(send)
	})
	// 触发登录事件
	l.PostEvent(&botEvent.OnLogin{})
}

func (l *Logic) handlePong(iRecv npb.IMessage, iSend npb.IMessage, ctx actor.Context, args ...interface{}) {
}
