package logic

import (
	"log"

	nexport "nggs/export"

	"server/src/bot/export"
)

type ID = nexport.LogicID

const (
	Example ID = iota + 1
	Login
	Common
)

type Factory struct {
	ID  ID
	New func(bot export.IBot) nexport.ILogic
}

var gFactoryMap = map[ID]*Factory{}

func GetFactory(id ID) (*Factory, bool) {
	if factory, ok := gFactoryMap[id]; ok {
		return factory, true
	}
	return nil, false
}

func RegisterFactory(id ID, New func(bot export.IBot) nexport.ILogic) {
	if _, ok := GetFactory(id); ok {
		log.Panicf("logic factory already exist, id=[%d]", id)
	}

	factory := &Factory{
		ID:  id,
		New: New,
	}

	gFactoryMap[factory.ID] = factory
}

func GenerateLogicMap(bot export.IBot) map[ID]nexport.ILogic {
	logicMap := map[ID]nexport.ILogic{}
	for id, factory := range gFactoryMap {
		logicMap[id] = factory.New(bot)
	}
	return logicMap
}
