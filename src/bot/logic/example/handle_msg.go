package example

import (
	protocol "nggs/network/protocol/protobuf/v1"
	// "msg"
)

func (l *Logic) regAllMsgHandler() (err error) {
	//l.RegisterMessageHandler(MsgPB.MsgGSResp_MessageID(), l.handleMsgGSResp)
	//l.RegisterMessageHandler(MsgPB.MsgServerVersion_MessageID(), l.handleMsgServerVersion)
	//l.RegisterMessageHandler(MsgPB.MsgRoleInfoGS2C_MessageID(), l.handleMsgRoleInfoGS2C)
	//l.RegisterMessageHandler(MsgPB.MsgLoginDayGS2C_MessageID(), l.handleMsgLoginDayGS2C)
	//l.RegisterMessageHandler(MsgPB.MsgCooldownGroupS2C_MessageID(), l.handleMsgCooldownGroupS2C)
	//l.RegisterMessageHandler(MsgPB.MsgItemShortcutS2C_MessageID(), l.handleMsgItemShortcutS2C)
	//l.RegisterMessageHandler(MsgPB.MsgRoleDataSyncDoneG2C_MessageID(), l.handleMsgRoleDataSyncDoneG2C)
	//l.RegisterMessageHandler(MsgPB.MsgChatInfo_MessageID(), l.handleMsgChatInfo)
	return
}

//func (logic *Logic) handleMsgGSResp(iMsg protocol.IMessage, args ...interface{}) {
//	logic.Debug("recv MsgGSResp[%v]", iMsg)
//}

func (l *Logic) handleMsgServerVersion(iMsg protocol.IMessage, args ...interface{}) {
	//l.Debug("recv MsgServerVersion[%v]", iMsg)
}

func (l *Logic) handleMsgRoleInfoGS2C(iMsg protocol.IMessage, args ...interface{}) {
	//l.Debug("recv MsgRoleInfoGS2C[%v]", iMsg)
}

func (l *Logic) handleMsgLoginDayGS2C(iMsg protocol.IMessage, args ...interface{}) {
	//l.Debug("recv MsgLoginDayGS2C[%v]", iMsg)
}

func (l *Logic) handleMsgCooldownGroupS2C(iMsg protocol.IMessage, args ...interface{}) {
	//l.Debug("recv MsgCooldownGroupS2C[%v]", iMsg)
}

func (l *Logic) handleMsgItemShortcutS2C(iMsg protocol.IMessage, args ...interface{}) {
	//l.Debug("recv MsgItemShortcutS2C[%v]", iMsg)
}

func (l *Logic) handleMsgRoleDataSyncDoneG2C(iMsg protocol.IMessage, args ...interface{}) {
	//l.Debug("recv MsgRoleDataSyncDoneG2C[%v]", iMsg)
}

func (l *Logic) handleMsgChatInfo(iMsg protocol.IMessage, args ...interface{}) {
	//l.Debug("recv MsgChatInfo[%v]", iMsg)
}
