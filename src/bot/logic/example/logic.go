package example

import (
	"server/src/bot/logic"

	nexport "nggs/export"
)

type Logic struct {
	logic.Super
}

func (Logic) ID() nexport.LogicID {
	return ID
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) Init() (err error) {
	err = l.Super.Init()
	if err != nil {
		return
	}
	err = l.regAllEventHandler()
	if err != nil {
		return
	}
	err = l.regAllMsgHandler()
	if err != nil {
		return
	}
	return nil
}
