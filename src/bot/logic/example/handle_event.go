package example

import (
	"nggs/event"
	//Msg "msg"
	//Event "server/src/bot/event"
)

func (l *Logic) regAllEventHandler() (err error) {
	//l.RegisterEventHandler(Event.OnConnectID, l.handleConnect)
	//l.RegisterEventHandler(Event.OnDisconnectID, l.handleDisconnect)
	return
}

func (l *Logic) handleConnect(iEv event.IEvent, args ...interface{}) {
	//l.Debug("connect to [%s] success", l.addr)
}

func (l *Logic) handleDisconnect(iEv event.IEvent, args ...interface{}) {
	//l.Debug("disconnect from [%s]", l.addr)
}
