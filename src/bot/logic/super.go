package logic

import (
	"fmt"

	"github.com/asynkron/protoactor-go/actor"

	"server/src/bot/export"
)

type Super struct {
	export.IBot

	ILogin  export.ILLogin
	ICommon export.ILCommon
}

func NewSuper(iBot export.IBot) Super {
	return Super{
		IBot: iBot,
	}
}

func (Super) ILogic() {

}

func (l *Super) Init() error {
	l.ILogin = l.GetLogic(Login).(export.ILLogin)
	if l.ILogin == nil {
		return fmt.Errorf("get Login logic fail")
	}
	l.ICommon = l.GetLogic(Common).(export.ILCommon)
	if l.ICommon == nil {
		return fmt.Errorf("get Common logic fail")
	}
	return nil
}

func (Super) Run() {

}

func (Super) Finish() {

}

func (Super) OnActorTerminated(who *actor.PID, ctx actor.Context) {

}

func (Super) OnPulse(ctx actor.Context) {

}
