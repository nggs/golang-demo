package common

import (
	"github.com/asynkron/protoactor-go/actor"

	nevent "nggs/event"
)

func (l *Logic) regAllEventHandler() (err error) {
	//l.RegisterEventHandler(botEvent.OnConnectID, l.handleOnConnect)
	//l.RegisterEventHandler(botEvent.OnDisconnectID, l.handleOnDisconnect)
	//l.RegisterEventHandler(botEvent.OnLoginID, l.handleOnLogin)
	return
}

func (l *Logic) handleOnConnect(iEvent nevent.IEvent, ctx actor.Context, args ...interface{}) {
	//l.Debug("connect to [%s] success", l.addr)

	//send := MsgPB.New_MsgClientLoginToGS()
	//send.SetIggID(l.Cfg().IggID)
	//send.SetCheckText(l.token)
	//l.SendMessage(send)
}

func (l *Logic) handleOnDisconnect(iEvent nevent.IEvent, ctx actor.Context, args ...interface{}) {
	//l.Debug("disconnect from [%s]", l.addr)
}

func (l *Logic) handleOnLogin(iEvent nevent.IEvent, ctx actor.Context, args ...interface{}) {
	//l.NewLoopTimer(10*time.Second, 0, func(nactor.TimerID, nactor.TimerID) {
	//	// 每隔一段时间发送一次心跳包
	//	send := gameMsg.Get_C2S_Ping()
	//	send.MessageSeq = uint32(nrandom.Int64(0, math.MaxUint32))
	//	send.Content = nrandom.String(16)
	//	_ = l.SendMessage(send, true)
	//})
	//
	//l.Debug("start pulse")
}
