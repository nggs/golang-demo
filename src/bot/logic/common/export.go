package common

import (
	gameMsg "msg"
)

func (l Logic) generateUploadDataSerial(bigSerial int64, littleSerial int32) int64 {
	return bigSerial*1000 + int64(littleSerial)
}

func (l *Logic) GetUploadDataSerial() int64 {
	return l.generateUploadDataSerial(l.uploadDataBigSerial, l.uploadDataLittleSerial)
}

func (l *Logic) InitUploadDataBigSerial(bigSerial int64) {
	l.uploadDataBeginBigSerial = bigSerial
	l.uploadDataBigSerial = bigSerial
}

func (l *Logic) IncreaseUploadDataBigSerial() {
	l.uploadDataBigSerial++
}

func (l *Logic) IncreaseUploadDataLittleSerial() {
	l.uploadDataLittleSerial++
}

func (l *Logic) ResetUploadDataLittleSerial() {
	l.uploadDataLittleSerial = 0
}

func (l *Logic) SendGM(command string, args ...string) {
	send := gameMsg.Get_C2S_GM()
	send.Cmd = command
	send.Args = args
	if err := l.SendMessage(send); err == nil {
		var strArgs string
		for _, arg := range args {
			strArgs += " " + arg
		}
		l.Debug("send gm [%s %s] success", command, strArgs)
	}
}
