package common

import (
	nexport "nggs/export"

	"server/src/bot/export"
	"server/src/bot/logic"
)

const (
	ID = logic.Common
)

func NewLogic(iBot export.IBot) nexport.ILogic {
	l := &Logic{
		Super: logic.NewSuper(iBot),
	}
	return l
}

func Init() {
	logic.RegisterFactory(ID, NewLogic)
}
