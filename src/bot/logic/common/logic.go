package common

import (
	nactor "nggs/actor"
	nexport "nggs/export"

	"server/src/bot/logic"
)

type Logic struct {
	logic.Super

	uploadDataBeginBigSerial int64
	uploadDataBigSerial      int64
	uploadDataLittleSerial   int32
	uploadDataTimerID        nactor.TimerID
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (Logic) ID() nexport.LogicID {
	return ID
}

func (Logic) LCommon() {

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) Init() (err error) {
	err = l.Super.Init()
	if err != nil {
		return
	}
	err = l.regAllEventHandler()
	if err != nil {
		return
	}
	err = l.regAllMsgHandler()
	if err != nil {
		return
	}
	return nil
}
