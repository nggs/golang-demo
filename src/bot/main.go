package main

import (
	"flag"
	"fmt"

	ndebug "nggs/debug"
	nlog "nggs/log"
	nutil "nggs/util"

	"sd"

	"server/src/bot/config"
	"server/src/bot/core"
)

var (
	flagPprofAddr     = flag.String("pprof", "", "pprof listen address")
	flagLogBaseDir    = flag.String("log", "../log", "log base dir, default=../log")
	flagConfigFileDir = flag.String("cfg", "../data/bot", "config file dir, default=../data/json")
	flagStaticDataDir = flag.String("sd", "../data/json", "static data file dir, default=../data/bot")

	logger nlog.ILogger
)

func main() {
	flag.Parse()

	logDir := fmt.Sprintf("%s/%s", *flagLogBaseDir, nutil.GetProgramFileBaseName())
	logger = nlog.New(logDir, nutil.GetProgramFileBaseName())
	defer func() {
		logger.Close()
	}()

	core.SetLogger(logger)

	if !config.LoadAll(*flagConfigFileDir) {
		panic("加载配置失败")
	}

	if !sd.LoadAll(*flagStaticDataDir) {
		panic("加载静态表失败")
	}
	if !sd.AfterLoadAll(*flagStaticDataDir) {
		panic("加载静态表后处理失败")
	}

	if *flagPprofAddr != "" {
		// 开启pprof server
		addr, err := ndebug.StartPprofServer2(*flagPprofAddr)
		if err != nil {
			panic(err)
		}
		defer ndebug.StopPprofServer()
		logger.Info("start pprof server in %s", addr)
	}

	Init()

	core.Run()
}
