package main

import (
	"server/src/bot/logic/common"
	"server/src/bot/logic/example"
	"server/src/bot/logic/login"

	progressExample "server/src/bot/progress/example"
)

func Init() {
	// todo 注册逻辑模块
	example.Init()
	common.Init()
	login.Init()

	// todo 注册流程模块
	progressExample.Init()

}
