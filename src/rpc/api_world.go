package rpc

import (
	nactor "nggs/actor"
	nservice "nggs/service"

	"cluster"
)

func SendDayRefreshToService(t int64, serviceName string) (err error) {
	send := Get_C2S_DayRefresh()
	send.Time = t

	err = cluster.EachServiceInstance(serviceName, func(info *nservice.Info) (continued bool) {
		nactor.RootContext().Send(info.Instance.PID, send)
		return true
	})

	return
}

func SendZeroClockToService(t int64, serviceName string) (err error) {
	send := Get_C2S_DayZeroClock()
	send.Time = t

	err = cluster.EachServiceInstance(serviceName, func(info *nservice.Info) (continued bool) {
		nactor.RootContext().Send(info.Instance.PID, send)
		return true
	})

	return
}
