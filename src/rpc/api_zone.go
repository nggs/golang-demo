package rpc

import (
	"fmt"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	nservice "nggs/service"

	"cluster"
	"model"
)

func GetZonesPIDByZoneID(zoneID int32) (zonesPID *actor.PID, err error) {
	serviceInfoNum, err := cluster.ServiceInfoNum(cluster.ZonesServiceName)
	if err != nil {
		err = fmt.Errorf("get zones service info num fail, %w", err)
		return
	}
	if serviceInfoNum == 0 {
		err = fmt.Errorf("zones service info num is 0")
		return
	}

	err = cluster.EachServiceInfo(cluster.ZonesServiceName, func(info *nservice.Info) (continued bool) {
		if info.Instance == nil || info.Instance.IsEmpty() {
			return true
		}
		id := int(zoneID)%int(serviceInfoNum) + 1
		if id == info.Config.GetID() {
			zonesPID = info.Instance.PID.Clone()
			return false
		}
		return true
	})
	if nactor.IsPIDPtrEmpty(zonesPID) {
		err = fmt.Errorf("zones pid is empty")
		return
	}
	return
}

func GetZonePID(zoneID int32) (zonePID *actor.PID, err error) {
	zonesPID, e := GetZonesPIDByZoneID(zoneID)
	if e != nil {
		err = fmt.Errorf("rpc.GetZonesPIDByZoneID fail, %w", e)
		return
	}

	send := Get_C2S_GetZonePID()
	send.ID = zoneID
	recv, e := Request_S2C_GetZonePID(FromRealPID(zonesPID), send)
	if e != nil {
		err = fmt.Errorf("Request_S2C_GetZonePID fail, %w", e)
		return
	}
	if recv.EC != EC_OK {
		err = fmt.Errorf("%v", recv.EC)
		return
	}

	zonePID = recv.PID.ToReal()

	if nactor.IsPIDPtrEmpty(zonePID) {
		err = fmt.Errorf("zone pid is empty")
		zonePID = nil
		return
	}

	return
}

func GetZoneData(zoneID int32) (zoneData *model.Zone, err error) {
	zonePID, err := GetZonePID(zoneID)
	if err != nil {
		return
	}

	send := Get_C2S_GetZoneData()
	recv, err := Request_S2C_GetZoneData(FromRealPID(zonePID), send)
	if err != nil {
		return
	}
	if recv.EC != EC_OK {
		err = fmt.Errorf("rpc.GetZoneData: %v", recv.EC)
		return
	}
	if recv.Data == nil {
		err = fmt.Errorf("rpc.GetZoneData: zone data is nil")
		return
	}

	zoneData = recv.Data.Clone()

	return
}

func GetZoneMinimal(zoneID int32) (minimal *model.ZoneMinimal, err error) {
	zonePID, e := GetZonePID(zoneID)
	if e != nil {
		err = fmt.Errorf("GetZonePID fail, %w", e)
		return
	}

	send := Get_C2S_GetZoneMinimal()
	recv, e := Request_S2C_GetZoneMinimal(FromRealPID(zonePID), send)
	if e != nil {
		err = fmt.Errorf("Request_S2C_GetZoneMinimal, %w", e)
		return
	}
	if recv.EC != EC_OK {
		err = fmt.Errorf("%v", recv.EC)
		return
	}
	if recv.Minimal == nil {
		err = fmt.Errorf("zone minimal is nil")
		return
	}

	minimal = recv.Minimal.Clone()

	return
}

func CreateZone(id int32, openTime int64, name string) (data *model.Zone, pid *actor.PID, err error) {
	zonesPID, err := GetZonesPIDByZoneID(id)
	if err != nil {
		err = fmt.Errorf("rpc.CreateZone fail, %s", err)
		return
	}

	send := Get_C2S_CreateZone()
	send.ID = id
	send.OpenTime = openTime
	send.Name = name
	recv, err := Request_S2C_CreateZone(FromRealPID(zonesPID), send)
	if err != nil {
		return
	}
	if recv.EC != EC_OK {
		err = fmt.Errorf("rpc.Get_C2S_SpawnZone: %v", recv.EC)
		return
	}
	if recv.Data == nil {
		err = fmt.Errorf("rpc.Get_C2S_SpawnZone: zone data nil")
		return
	}

	pid = recv.PID.ToReal()

	if nactor.IsPIDPtrEmpty(pid) {
		err = fmt.Errorf("rpc.Get_C2S_SpawnZone: zone pid is nil")
		pid = nil
		return
	}

	data = recv.Data.Clone()

	return
}

//func TryUpdateLeaderBoard(zoneID int32, tYpE sd.E_LadderType, roleID int64, point int64, reachTime int64, roleName string) (leaderBoard *model.LeaderBoard, ok bool, err error) {
//	zonePID, err := GetZonePID(zoneID)
//	if err != nil {
//		return
//	}
//
//	send := Get_C2S_TryUpdateLeaderBoard()
//	send.Type = int32(tYpE)
//	send.RoleID = roleID
//	send.Point = point
//	send.ReachTime = reachTime
//	send.RoleName = roleName
//
//	recv, err := Request_S2C_TryUpdateLeaderBoard(zonePID, send)
//	if err != nil {
//		return
//	}
//	if recv.EC != EC_OK {
//		return recv.LeaderBoard, false, nil
//	}
//
//	leaderBoard = recv.LeaderBoard.Clone()
//	ok = true
//
//	return
//}
//
//func GetLeaderBoard(zoneID int32) (leaderBoards map[int32]*model.LeaderBoard, err error) {
//	zonePID, e := GetZonePID(zoneID)
//	if e != nil {
//		err = fmt.Errorf("GetZonePID fail, %w", e)
//		return
//	}
//
//	send := Get_C2S_GetLeaderBoard()
//	recv, e := Request_S2C_GetLeaderBoard(zonePID, send)
//	if e != nil {
//		err = fmt.Errorf("Request_S2C_GetLeaderBoard, %w", e)
//		return
//	}
//	if recv.EC != EC_OK {
//		err = fmt.Errorf("%v", recv.EC)
//		return
//	}
//	//if recv.LeaderBoards == nil {
//	//	err = fmt.Errorf("leaderBoard data is nil")
//	//	return
//	//}
//
//	leaderBoards = make(map[int32]*model.LeaderBoard, len(recv.LeaderBoards))
//	for t, leaderBoard := range recv.LeaderBoards {
//		leaderBoards[t] = leaderBoard.Clone()
//	}
//
//	return
//}
//
//func TryUpdateActivityRank(sender *actor.PID, zoneID int32, tYpE sd.E_ActivityRankType, roleID int64, point int64, reachTime int64) (err error) {
//	zonePID, err := GetZonePID(zoneID)
//	if err != nil {
//		err = fmt.Errorf("TryUpdateActivityRank GetZonePIDFromWorld fail, zone id=%d, %s", zoneID, err)
//		return
//	}
//	send := Get_C2S_TryUpdateActivityRank()
//	send.Type = int32(tYpE)
//	send.RoleID = roleID
//	send.Point = point
//	send.ReachTime = reachTime
//	myactor.RootContext().RequestWithCustomSender(zonePID, send, sender)
//	return
//}
//
//func SendTodayRankActivityToAllGame(zoneID int32, rankActivity *model.RankActivity) (err error) {
//	send := Get_S2C_TodayRankActivity()
//	send.ZoneID = zoneID
//	send.RankActivity = rankActivity.Clone()
//	err = cluster.EachGameInstance(func(info *msservice.Info) (continued bool) {
//		myactor.RootContext().Send(&info.Instance.PID, send)
//		return true
//	})
//	return
//}
//
//func TryUpdateHeroChallengeLeaderBoard(tYpE sd.E_HeroChallengeType, roleID int64, order int32, reachTime int64) (err error) {
//	_, zoneID := model.RoleIDToAccountIDServerID(roleID)
//	zonePID, e := GetZonePID(zoneID)
//	if e != nil {
//		err = fmt.Errorf("GetZonePID fail, %w", e)
//		return
//	}
//	send := Get_C2S_TryUpdateHeroChallengeLeaderBoard()
//	send.Type = int32(tYpE)
//	send.RoleID = roleID
//	send.Order = order
//	send.ReachTime = reachTime
//	myactor.RootContext().Send(zonePID, send)
//	return
//}
//
//func SendHeroChallengeLeaderBoardToAllGame(zoneID int32, tYpE sd.E_HeroChallengeType, leaderBoard map[int32]*model.RoleInHeroChallengeRank) (err error) {
//	send := Get_S2C_HeroChallengeLeaderBoard()
//	send.ZoneID = zoneID
//	send.Type = int32(tYpE)
//	send.LeaderBoard = make(map[int32]*model.RoleInHeroChallengeRank, len(leaderBoard))
//	for pos, role := range leaderBoard {
//		send.LeaderBoard[pos] = role.Clone()
//	}
//	err = cluster.EachGameInstance(func(info *msservice.Info) (continued bool) {
//		myactor.RootContext().Send(&info.Instance.PID, send)
//		return true
//	})
//	return
//}
//
//func GetHeroChallengeLeaderBoard(zoneID int32, tYpE sd.E_HeroChallengeType) (leaderBoard map[int32]*model.RoleInHeroChallengeRank, err error) {
//	zonePID, e := GetZonePID(zoneID)
//	if e != nil {
//		err = fmt.Errorf("GetZonePID fail, %w", e)
//		return
//	}
//
//	send := Get_C2S_GetHeroChallengeLeaderBoard()
//	send.Type = int32(tYpE)
//	recv, e := Request_S2C_GetHeroChallengeLeaderBoard(zonePID, send)
//	if e != nil {
//		err = fmt.Errorf("Request_S2C_HeroChallengeLeaderBoard fail, %w", e)
//		return
//	}
//	if recv.EC != EC_OK {
//		err = fmt.Errorf("%v", recv.EC)
//		return
//	}
//	leaderBoard = make(map[int32]*model.RoleInHeroChallengeRank, len(recv.LeaderBoard))
//	for pos, role := range recv.LeaderBoard {
//		leaderBoard[pos] = role.Clone()
//	}
//	return
//}
//
//func GetOtherRoleHeroChallengeHeroes(roleID int64) (heroes *model.RoleInHeroChallengeHeroes, err error) {
//	_, zoneID := model.RoleIDToAccountIDServerID(roleID)
//	zonePID, e := GetZonePID(zoneID)
//	if e != nil {
//		err = fmt.Errorf("GetZonePID fail, %w", e)
//		return
//	}
//	send := Get_C2S_GetRoleHeroChallengeHeroes()
//	send.RoleID = roleID
//	recv, e := Request_S2C_GetRoleHeroChallengeHeroes(zonePID, send)
//	if e != nil {
//		err = fmt.Errorf("Request_S2C_GetOtherRoleHeroChallengeHeroes fail, %w", e)
//		return
//	}
//	if recv.EC != EC_OK {
//		err = fmt.Errorf("%v", recv.EC)
//		return
//	}
//	heroes = recv.Heroes.Clone()
//	return
//}
//
//func SetHeroChallengeHeroes(roleID int64, singleModeHeroes map[int32]*model.HeroNameCard, teamModeHeroes map[int32]*model.HeroNameCard) (err error) {
//	_, zoneID := model.RoleIDToAccountIDServerID(roleID)
//	zonePID, e := GetZonePID(zoneID)
//	if e != nil {
//		err = fmt.Errorf("GetZonePID fail, %w", e)
//		return
//	}
//	send := Get_C2S_SetRoleHeroChallengeHeroes()
//	send.RoleID = roleID
//	if len(singleModeHeroes) > 0 {
//		send.SingleModeHeroes = *(model.ToHeroNameCardMap(singleModeHeroes).Clone())
//	}
//	if len(teamModeHeroes) > 0 {
//		send.TeamModeHeroes = *(model.ToHeroNameCardMap(teamModeHeroes).Clone())
//	}
//	myactor.RootContext().Send(zonePID, send)
//	return
//}
