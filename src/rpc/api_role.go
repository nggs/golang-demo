package rpc

import (
	"fmt"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	nservice "nggs/service"

	"cluster"
	"model"
)

func GetGamePIDByAccountID(accountID int64) (gameID int32, gamePID *actor.PID, err error) {
	serviceInfoNum, err := cluster.ServiceInfoNum(cluster.GameServiceName)
	if err != nil {
		err = fmt.Errorf("get game service info num fail, %w", err)
		return
	}
	if serviceInfoNum == 0 {
		err = fmt.Errorf("game service info num is 0")
		return
	}

	err = cluster.EachServiceInfo(cluster.GameServiceName, func(info *nservice.Info) (continued bool) {
		if info.Instance == nil || info.Instance.IsEmpty() {
			return true
		}
		id := int(accountID)%int(serviceInfoNum) + 1
		if id == info.Config.GetID() {
			gameID = int32(id)
			gamePID = info.Instance.PID.Clone()
			return false
		}
		return true
	})
	if nactor.IsPIDPtrEmpty(gamePID) {
		err = fmt.Errorf("game pid is nil")
		return
	}
	return
}

func GetGamePIDByRoleID(roleID int64) (gameID int32, gamePID *actor.PID, err error) {
	accountID, _ := model.RoleIDToAccountIDServerID(roleID)
	return GetGamePIDByAccountID(accountID)
}

func AcceptConnection(sessionPID *actor.PID, sessionID uint64, accountID int64, serverID int32) (roleID int64, rolePID *actor.PID, err error) {
	accountPID, e := GetAccountPID(accountID)
	if e != nil {
		err = fmt.Errorf("GetAccountPID fail, %w", e)
		return
	}

	// 通知account接受连接
	send := Get_C2S_AcceptConnection()
	send.SessionPID = FromRealPID(sessionPID)
	send.SessionID = sessionID
	send.AccountID = accountID
	send.ServerID = serverID
	recv, e := Request_S2C_AcceptConnection(FromRealPID(accountPID), send)
	if e != nil {
		err = fmt.Errorf("Request_S2C_AcceptConnection fail, %w", e)
		return
	}
	if recv.EC != EC_OK {
		err = fmt.Errorf("%v", recv.EC)
		return
	}

	roleID = recv.RoleID
	if roleID <= 0 {
		err = fmt.Errorf("role id <= 0")
		return
	}

	rolePID = recv.RolePID.ToReal()
	if nactor.IsPIDPtrEmpty(rolePID) {
		err = fmt.Errorf("role pid is empty")
		rolePID = nil
		return
	}

	return
}

func SpawnRole(roleID int64, accountID int64, serverID int32, sessionPID *actor.PID, sessionID uint64,
	account *model.AccountInRole) (rolePID *actor.PID, err error) {

	zonePID, err := GetZonePID(serverID)
	if err != nil {
		err = fmt.Errorf("GetZonePID fail, %w", err)
		return
	}

	send := Get_C2S_SpawnRole()
	send.RoleID = roleID
	send.AccountID = accountID
	send.ServerID = serverID
	send.SessionPID = FromRealPID(sessionPID)
	send.SessionID = sessionID
	send.Account = account
	recv, e := Request_S2C_SpawnRole(FromRealPID(zonePID), send)
	if e != nil {
		err = fmt.Errorf("Request_S2C_SpawnRole fail, %w", e)
		return
	}
	if recv.EC != EC_OK {
		err = fmt.Errorf("%v", recv.EC)
		return
	}

	rolePID = recv.RolePID.ToReal()
	if nactor.IsPIDPtrEmpty(rolePID) {
		err = fmt.Errorf("role pid is empty")
		rolePID = nil
		return
	}

	return
}

func ForwardSpawnRoleToGame(send *C2S_SpawnRole) (rolePID *actor.PID, err error) {
	_, gamePID, e := GetGamePIDByAccountID(send.AccountID)
	if e != nil {
		err = fmt.Errorf("GetGamePIDByAccountID fail, %w", e)
		return
	}

	recv, e := Request_S2C_SpawnRole(FromRealPID(gamePID), send)
	if e != nil {
		err = fmt.Errorf("Request_S2C_SpawnRole fail, %w", e)
		return
	}
	if recv.EC != EC_OK {
		err = fmt.Errorf("%v", recv.EC)
		return
	}

	rolePID = recv.RolePID.ToReal()
	if nactor.IsPIDPtrEmpty(rolePID) {
		err = fmt.Errorf("role pid is empty")
		rolePID = nil
		return
	}

	return
}

//func CreateRole(roleID int64) (rolePID *actor.PID, err error) {
//	accountID, _ := model.RoleIDToAccountIDServerID(roleID)
//	_, gamePID, e := GetGamePIDByAccountID(accountID)
//	if e != nil {
//		err = fmt.Errorf("GetGamePIDByAccountID fail, %w", e)
//		return
//	}
//
//	send := Get_C2S_CreateRole()
//	send.RoleID = roleID
//	recv, e := Request_S2C_CreateRole(gamePID, send)
//	if e != nil {
//		err = fmt.Errorf("Request_S2C_CreateRole fail, %w", e)
//		return
//	}
//	if recv.EC != EC_OK {
//		err = fmt.Errorf("%v", recv.EC)
//		return
//	}
//
//	rolePID = recv.RolePID.Clone()
//
//	return
//}

func GetRolePID(roleID int64) (rolePID *actor.PID, err error) {
	accountID, _ := model.RoleIDToAccountIDServerID(roleID)
	pid, e := GetAccountPID(accountID)
	if e != nil {
		err = fmt.Errorf("GetAccountPID fail, %w", e)
		return
	}

	send := Get_C2S_GetRolePID()
	send.RoleID = roleID
	recv, e := Request_S2C_GetRolePID(FromRealPID(pid), send)
	if e != nil {
		err = fmt.Errorf("Request_S2C_GetRolePID fail, %w", e)
		return
	}
	if recv.EC != EC_OK {
		err = fmt.Errorf("%v", recv.EC)
		return
	}

	rolePID = recv.RolePID.ToReal()

	if nactor.IsPIDPtrEmpty(rolePID) {
		err = fmt.Errorf("role pid is empty")
		rolePID = nil
		return
	}

	return
}

func GetSpawnedRolePID(roleID int64) (rolePID *actor.PID, err error) {
	accountID, _ := model.RoleIDToAccountIDServerID(roleID)
	pid, e := GetAccountPID(accountID)
	if e != nil {
		err = fmt.Errorf("GetAccountPID fail, %w", e)
		return
	}

	send := Get_C2S_GetSpawnedRolePID()
	send.RoleID = roleID
	recv, e := Request_S2C_GetSpawnedRolePID(FromRealPID(pid), send)
	if e != nil {
		err = fmt.Errorf("Request_S2C_GetSpawnedRolePID fail, %w", e)
		return
	}
	if recv.EC != EC_OK {
		err = fmt.Errorf("%v", recv.EC)
		return
	}

	rolePID = recv.RolePID.ToReal()

	return
}

//func GetSomeSpawnedRolePID(roleIDs map[int64]int32) (rolePIDs map[int64]*actor.PID, ecs map[int64]EC, err error) {
//	if len(roleIDs) == 0 {
//		return
//	}
//
//	var serviceInfoNum = cluster.ServiceInfoNum(cluster.GameServiceName)
//	if serviceInfoNum == 0 {
//		err = fmt.Errorf("game info num is 0")
//		return
//	}
//
//	gamePIDs := map[int32]*actor.PID{}
//	allGameRoleIDs := map[int32]map[int64]int32{}
//	for roleID, _ := range roleIDs {
//		var accountID, _ = model.RoleIDToAccountIDServerID(roleID)
//		var gameID int32
//		var gamePID *actor.PID
//		err = cluster.EachServiceInfo(cluster.GameServiceName, func(info *msservice.Info) (continued bool) {
//			if info.Instance == nil || info.Instance.IsEmpty() {
//				return true
//			}
//			id := int(accountID)%int(serviceInfoNum) + 1
//			if id == info.Config.ID() {
//				gameID = int32(id)
//				gamePID = info.Instance.PID.Clone()
//				return false
//			}
//			return true
//		})
//		if gameID == 0 {
//			err = fmt.Errorf("game id is 0")
//			return
//		}
//		if nactor.IsPIDPtrEmpty(gamePID) {
//			err = fmt.Errorf("game pid is empty, game id=%d", gameID)
//			return
//		}
//		gamePIDs[gameID] = gamePID
//		if gameRoleIDs, ok := allGameRoleIDs[gameID]; ok {
//			gameRoleIDs[roleID] = 1
//		} else {
//			allGameRoleIDs[gameID] = map[int64]int32{roleID: 1}
//		}
//	}
//
//	rolePIDs = map[int64]*actor.PID{}
//	for gameID, gamePID := range gamePIDs {
//		gameRoleIDs, ok := allGameRoleIDs[gameID]
//		if !ok || gameRoleIDs == nil {
//			continue
//		}
//		send := Get_C2S_GetSomeSpawnedRolePID()
//		send.RoleIDs = gameRoleIDs
//		recv, e := Request_S2C_GetSomeSpawnedRolePID(gamePID, send)
//		if e != nil {
//			err = fmt.Errorf("Request_S2C_GetSomeSpawnedRolePID fail, zone id=%d, role ids=%v, %w", gameID, gameRoleIDs, e)
//			return
//		}
//		for roleID, ec := range recv.ECs {
//			if ec != EC_OK {
//				//err = fmt.Errorf("get spawned role pid fail, role id=%d, %v", roleID, ec)
//				continue
//			}
//			rolePID, ok := recv.RolePIDs[roleID]
//			if !ok {
//				//err = fmt.Errorf("get spawned role pid fail, role id=%d", roleID)
//				continue
//			}
//			rolePIDs[roleID] = rolePID.Clone()
//		}
//	}
//
//	return
//}
//
//func SendMessageToSomeSpawnedRole(roleIDs map[int64]int32, iMsg msrpc.IMessage) (rolePIDs map[int64]*actor.PID, ecs map[int64]EC, err error) {
//	rolePIDs, ecs, err = GetSomeSpawnedRolePID(roleIDs)
//	if err == nil {
//		for _, pid := range rolePIDs {
//			nactor.RootContext().Send(pid, iMsg)
//		}
//	}
//	return
//}

func NotifyRoleRelogin(rolePID *actor.PID, sessionPID *actor.PID, sessionID uint64) (err error) {
	send := Get_C2S_Relogin()
	send.SessionPID = FromRealPID(sessionPID)
	send.SessionID = sessionID
	recv, e := Request_S2C_Relogin(FromRealPID(rolePID), send)
	if e != nil {
		err = fmt.Errorf("Request_S2C_Relogin fail, %w", e)
		return
	}
	if recv.EC != EC_OK {
		err = fmt.Errorf("%v", recv.EC)
		return
	}
	return
}

func NotifyAccountRoleLogout(accountID int64, roleID int64) (err error) {
	pid, e := GetAccountPID(accountID)
	if e != nil {
		err = fmt.Errorf("GetAccountPID fail, account id=%d, %w", accountID, e)
		return
	}
	send := Get_C2S_RoleLogout()
	send.RoleID = roleID
	nactor.RootContext().Send(pid, send)
	return
}

func ExecuteGM(rolePID *actor.PID, cmd string) (err error) {
	send := Get_C2S_ExecuteGM()
	send.Cmd = cmd
	recv, err := Request_S2C_ExecuteGM(FromRealPID(rolePID), send)
	if err != nil {
		return
	}
	if recv.EC != EC_OK {
		err = fmt.Errorf("rpc.ExecuteGM: %v", recv.EC)
		return
	}
	return
}

func NotifyWorldRoleLogin(roleID int64, os string, channel string, rolePID *actor.PID) {
	send := Get_C2S_RoleLogin()
	send.RoleID = roleID
	send.OS = os
	send.Channel = channel
	send.RolePID = FromRealPID(rolePID)
	nactor.RootContext().Send(cluster.WorldPID(), send)
}

func NotifyWorldRoleLogout(roleID int64) {
	send := Get_C2S_RoleLogout()
	send.RoleID = roleID
	nactor.RootContext().Send(cluster.WorldPID(), send)
}

//func SetRoleNameByPID(pid *actor.PID, roleID int64, roleName string) (err error) {
//	send := Get_C2S_SetRoleName()
//	send.RoleID = roleID
//	send.RoleName = roleName
//	recv, e := Request_S2C_SetRoleName(pid, send)
//	if e != nil {
//		err = fmt.Errorf("Request_S2C_SetRoleName fail, %w", e)
//		return
//	}
//	if recv.EC != EC_OK {
//		err = fmt.Errorf("err code[%d], err msg[%v]", recv.EC, recv.EC)
//		return
//	}
//
//	return
//}

//func NewGetRolePIDPromise(promise *msrpc.Promise, sender *actor.PID, roleID int64,
//	cb func(roleID int64, rolePID *actor.PID, promise *msrpc.Promise)) *msrpc.Promise {
//
//	if promise == nil {
//		promise = msrpc.NewPromise()
//	}
//	promise = promise.Then(func(p *msrpc.Promise) {
//		_, gamePID, e := GetGamePIDByRoleID(roleID)
//		if e != nil {
//			p.Err = fmt.Errorf("GetGamePIDByRoleID fail, %w", e)
//			return
//		}
//		send := Get_C2S_GetRolePID()
//		send.RoleID = roleID
//		p.Sender = sender
//		p.IRequestMessage = send
//		p.Receiver = gamePID
//		p.Requested = false
//	}).Then(func(p *msrpc.Promise) {
//		recv, ok := p.IResponseMessage.(*S2C_GetRolePID)
//		if !ok || recv == nil {
//			p.Err = fmt.Errorf("need S2C_GetRolePID, but resp msg is other or nil")
//			return
//		}
//		if recv.EC != EC_OK {
//			p.Err = fmt.Errorf("get role pid fail, id=%d, ec=%v", recv.RoleID, recv.EC)
//			return
//		}
//		if nactor.IsPIDPtrEmpty(recv.RolePID) {
//			p.Err = fmt.Errorf("get role pid fail, id=%d, pid is empty", recv.RoleID)
//			return
//		}
//		if cb != nil {
//			cb(recv.RoleID, recv.RolePID, p)
//		}
//	})
//	return promise
//}

//func MustAsyncGetRolePID(iMgr msrpc.IPromiseManager, sender *actor.PID, roleID int64,
//	cb func(err error, roleID int64, rolePID *actor.PID), then func(promise *msrpc.Promise)) {
//
//	if roleID <= 0 {
//		log.Panicf("role id <= 0")
//	}
//	if cb == nil {
//		log.Panicf("call back is nil")
//	}
//
//	promise := NewGetRolePIDPromise(nil, sender, roleID, func(roleID int64, rolePID *actor.PID, promise *msrpc.Promise) {
//		cb(nil, roleID, rolePID)
//	}).OnComplete(func(err error) {
//		if err != nil {
//			cb(err, roleID, nil)
//		}
//	})
//	if then != nil {
//		then(promise)
//	}
//	iMgr.ExecutePromise(promise)
//}

//func NewGetSomeRolePIDPromise(promise *msrpc.Promise, sender *actor.PID, roleIDs map[int64]struct{},
//	one func(roleID int64, rolePID *actor.PID, promise *msrpc.Promise),
//	then func(subPromise *msrpc.Promise),
//	all func(rolePIDs map[int64]*actor.PID, promise *msrpc.Promise)) *msrpc.Promise {
//
//	var rolePIDs map[int64]*actor.PID
//	if all != nil {
//		rolePIDs = map[int64]*actor.PID{}
//	}
//
//	for roleID, _ := range roleIDs {
//		promise = NewGetRolePIDPromise(promise, sender, roleID, func(roleID int64, rolePID *actor.PID, p *msrpc.Promise) {
//			if one != nil {
//				one(roleID, rolePID, p)
//			}
//			if rolePIDs != nil {
//				rolePIDs[roleID] = rolePID
//			}
//		})
//		if then != nil {
//			then(promise)
//		}
//	}
//	if all != nil {
//		promise.Then(func(p *msrpc.Promise) {
//			all(rolePIDs, p)
//		})
//	}
//	return promise
//}

//func MustAsyncGetSomeRolePID(iMgr msrpc.IPromiseManager, sender *actor.PID, roleIDs map[int64]struct{},
//	one func(roleID int64, rolePID *actor.PID, ctx *msrpc.Promise),
//	then func(subPromise *msrpc.Promise),
//	all func(err error, rolePIDs map[int64]*actor.PID, p *msrpc.Promise)) {
//
//	var promise *msrpc.Promise
//	if len(roleIDs) > 0 {
//		promise = NewGetSomeRolePIDPromise(nil, sender, roleIDs, one, then, func(rolePIDs map[int64]*actor.PID, p *msrpc.Promise) {
//			all(nil, rolePIDs, p)
//		}).OnComplete(func(err error) {
//			if err != nil {
//				all(err, nil, nil)
//			}
//		})
//	} else {
//		if all == nil {
//			log.Panicf("neccesary call back is nil")
//		}
//		promise = msrpc.NewPromise().Then(func(p *msrpc.Promise) {
//			all(nil, nil, p)
//		})
//	}
//	iMgr.ExecutePromise(promise)
//}
