package rpc

import (
	nactor "nggs/actor"
	nservice "nggs/service"

	"github.com/asynkron/protoactor-go/actor"

	"cluster"
	"model"
)

func SendMailByRolePID(rolePID *actor.PID, tid int32, title string, content string, params map[string]string, reward *model.Reward) (err error) {
	send := Get_C2S_RecvMail()
	send.TID = tid
	send.Title = title
	send.Content = content
	send.Params = params
	send.Reward = reward
	nactor.RootContext().Send(rolePID, send)
	return
}

func SendMailByRoleID(roleID int64, tid int32, title string, content string, params map[string]string, reward *model.Reward) (err error) {
	rolePID, err := GetRolePID(roleID)
	if err != nil {
		return
	}
	return SendMailByRolePID(rolePID, tid, title, content, params, reward)
}

func SendMailToAllGame(uid int64, tid int32, title string, content string, params map[string]string, reward *model.Reward, zoneIDs map[int32]bool) (err error) {
	send := Get_C2S_RecvMail()
	send.UID = uid
	send.TID = tid
	send.Title = title
	send.Content = content

	if len(params) > 0 {
		send.Params = make(map[string]string, len(params))
		for k, v := range params {
			send.Params[k] = v
		}
	}

	//if reward != nil && !reward.IsEmpty() {
	//	send.Reward = reward.Clone()
	//}

	if len(zoneIDs) > 0 {
		send.ZoneIDs = map[int32]bool{}
		for k, v := range zoneIDs {
			send.ZoneIDs[k] = v
		}
	}

	err = cluster.EachServiceInstance(cluster.GameServiceName, func(info *nservice.Info) (continued bool) {
		nactor.RootContext().Send(info.Instance.PID, send)
		return true
	})

	return
}

func SendSelectWorldMail(sender *actor.PID, uid int64, zoneID int32, sendTime int64) (err error) {
	send := Get_C2S_SelectWorldMail()
	send.Sender = FromRealPID(sender)
	send.UID = uid
	send.ZoneID = zoneID
	send.SendTime = sendTime
	nactor.RootContext().Send(cluster.WorldPID(), send)
	return
}
