package rpc

import (
	"fmt"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	nservice "nggs/service"

	"cluster"
	"model"
)

func GetAccountsPIDByAccountID(accountID int64) (accountsPID *actor.PID, err error) {
	serviceInfoNum, err := cluster.ServiceInfoNum(cluster.AccountsServiceName)
	if err != nil {
		err = fmt.Errorf("get accounts info num fail, %w", err)
		return
	}
	if serviceInfoNum == 0 {
		err = fmt.Errorf("accounts info num is 0")
		return
	}

	err = cluster.EachServiceInfo(cluster.AccountsServiceName, func(info *nservice.Info) (continued bool) {
		if info.Instance == nil || info.Instance.IsEmpty() {
			return true
		}
		id := int(accountID)%int(serviceInfoNum) + 1
		if id == info.Config.GetID() {
			accountsPID = info.Instance.PID.Clone()
			return false
		}
		return true
	})
	if nactor.IsPIDPtrEmpty(accountsPID) {
		err = fmt.Errorf("accounts pid is nil")
		return
	}
	return
}

func GetAccountPID(accountID int64) (accountPID *actor.PID, err error) {
	accountsPID, e := GetAccountsPIDByAccountID(accountID)
	if e != nil {
		err = fmt.Errorf("GetAccountsPIDByAccountID fail, %w", e)
		return
	}

	send := Get_C2S_GetAccountPID()
	send.ID = accountID
	recv, e := Request_S2C_GetAccountPID(FromRealPID(accountsPID), send)
	if e != nil {
		err = fmt.Errorf("Request_S2C_GetAccountPID fail, %w", e)
		return
	}
	if recv.EC != EC_OK {
		err = fmt.Errorf("%v", recv.EC)
		return
	}
	if nactor.IsPIDPtrEmpty(recv.PID.ToReal()) {
		err = fmt.Errorf("account pid is empty")
		return
	}

	accountPID = recv.PID.ToReal()

	return
}

func AccountLogin(accountID int64, accountName string, serverID int32, channel string, os string, clientVersion string, ip string, isRelogin bool) (ec EC, token string, gatewayAddr string, err error) {
	pid, e := GetAccountPID(accountID)
	if e != nil {
		err = fmt.Errorf("GetAccountPID fail, id=%d, %w", accountID, e)
		return
	}

	send := Get_C2S_AccountLogin()
	send.ID = accountID
	send.AccountName = accountName
	send.ServerID = serverID
	send.Channel = channel
	send.OS = os
	send.ClientVersion = clientVersion
	send.IP = ip
	send.IsRelogin = isRelogin
	recv, e := Request_S2C_AccountLogin(FromRealPID(pid), send)
	if e != nil {
		err = fmt.Errorf("Request_S2C_AccountLogin fail, id=%d, %w", accountID, e)
		return
	}

	ec = recv.EC
	token = recv.Token
	gatewayAddr = recv.GatewayAddr

	return
}

func UpdateAccountLoginData(roleID int64, lastLoginTime int64, level int32, avatarID int32, rechargeTime int64) (err error) {
	accountID, _ := model.RoleIDToAccountIDServerID(roleID)
	pid, e := GetAccountPID(accountID)
	if e != nil {
		err = fmt.Errorf("GetAccountPID fail, id=%d, %w", accountID, e)
		return
	}
	send := Get_C2S_UpdateAccountLoginData()
	send.RoleID = roleID
	send.LastLoginTime = lastLoginTime
	send.Level = level
	send.AvatarID = avatarID
	send.RechargeTime = rechargeTime
	nactor.RootContext().Send(pid, send)
	return
}

func UpdateAccountBan(accountID int64, ban int32) (ec EC, err error) {
	pid, e := GetAccountPID(accountID)
	if e != nil {
		err = fmt.Errorf("GetAccountPID fail, id=%d, %w", accountID, e)
		return
	}
	send := Get_C2S_UpdateAccountBan()
	send.AccountID = accountID
	send.Ban = ban
	recv, e := Request_S2C_UpdateAccountBan(FromRealPID(pid), send)
	if e != nil {
		err = fmt.Errorf("Request_S2C_UpdateAccountBan fail, id=%d, %w", accountID, e)
		return
	}
	ec = recv.EC
	return
}
