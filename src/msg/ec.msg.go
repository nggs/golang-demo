// Code generated by protoc-gen-pbex-go. DO NOT EDIT IT!!!
// source: msg/ec.proto

package msg

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// enum [EC] begin

/*
/ 错误码枚举
*/

var EC_Slice = []int32{
	0,
	1,
	2,
	3,
	4,
}

func EC_Len() int {
	return len(EC_Slice)
}

func Check_EC_I(value int32) bool {
	if _, ok := EC_name[value]; ok && value != 0 {
		return true
	}
	return false
}

func Check_EC(value EC) bool {
	return Check_EC_I(int32(value))
}

func Each_EC(f func(EC) bool) {
	for _, value := range EC_Slice {
		if !f(EC(value)) {
			break
		}
	}
}

func Each_EC_I(f func(int32) bool) {
	for _, value := range EC_Slice {
		if !f(value) {
			break
		}
	}
}

// enum [EC] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
