@set WORK_DIR=%1

@set SRC_DIR=%WORK_DIR%src

@set TOOLS_DIR=%WORK_DIR%src\nggs\bin\tools

@set PROTOC=%TOOLS_DIR%\protoc\3.15.6-win64\bin\protoc.exe

@set PBPLUGIN_DIR=%TOOLS_DIR%\pbplugin

@setlocal enabledelayedexpansion enableextensions
@set PROTO_FILES=
@for %%x in (%SRC_DIR%\protos\rpc\*.proto) do @set PROTO_FILES=!PROTO_FILES! %%x
@set PROTO_FILES=%PROTO_FILES:~1%

%PROTOC% --plugin=protoc-gen-gogoslick=%PBPLUGIN_DIR%\protoc-gen-gogoslick.exe -I=%GOPATH%\src -I=%SRC_DIR%\protos --gogoslick_out=%SRC_DIR% %PROTO_FILES%
::%PROTOC% --plugin=protoc-gen-rpc-go=%PBPLUGIN_DIR%\protoc-gen-rpc-go.exe -I=%GOPATH%\src -I=%SRC_DIR%\protos --rpc-go_out=%SRC_DIR% --rpc-go_opt=tpl=%SRC_DIR%\protos\rpc.gohtml %PROTO_FILES%
%PROTOC% --plugin=protoc-gen-rpc-go=%PBPLUGIN_DIR%\protoc-gen-rpc-go.exe -I=%GOPATH%\src -I=%SRC_DIR%\protos --rpc-go_out=%SRC_DIR% %PROTO_FILES%
