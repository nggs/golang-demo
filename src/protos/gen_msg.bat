@set WORK_DIR=%1

echo [%WORK_DIR%]

@set SRC_DIR=%WORK_DIR%src

@set TOOLS_DIR=%WORK_DIR%src\nggs\bin\tools

@set PROTOC=%TOOLS_DIR%\protoc\3.15.6-win64\bin\protoc.exe

@set PBPLUGIN_DIR=%TOOLS_DIR%\pbplugin

@setlocal enabledelayedexpansion enableextensions
@set PROTO_FILES=
@for %%x in (%SRC_DIR%\protos\msg\*.proto) do @set PROTO_FILES=!PROTO_FILES! %%x
@set PROTO_FILES=%PROTO_FILES:~1%

%PROTOC% --plugin=protoc-gen-gogoslick=%PBPLUGIN_DIR%\protoc-gen-gogoslick.exe -I=%GOPATH%\src -I=%SRC_DIR%\protos --gogoslick_out=%SRC_DIR% %PROTO_FILES%
::%PROTOC% --plugin=protoc-gen-msg-go=%PBPLUGIN_DIR%\protoc-gen-msg-go.exe -I=%GOPATH%\src -I=%SRC_DIR%\protos --msg-go_out=%SRC_DIR% --msg-go_opt=tpl=%SRC_DIR%\protos\msg.gohtml;version=v3 %PROTO_FILES%
%PROTOC% --plugin=protoc-gen-msg-go=%PBPLUGIN_DIR%\protoc-gen-msg-go.exe -I=%GOPATH%\src -I=%SRC_DIR%\protos --msg-go_out=%SRC_DIR% --msg-go_opt=version=v3 %PROTO_FILES%
