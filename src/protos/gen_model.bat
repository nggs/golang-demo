@set WORK_DIR=%1

@set SRC_DIR=%WORK_DIR%src

@set TOOLS_DIR=%WORK_DIR%src\nggs\bin\tools

@set PROTOC=%TOOLS_DIR%\protoc\3.15.6-win64\bin\protoc.exe

@set PBPLUGIN_DIR=%TOOLS_DIR%\pbplugin

@setlocal enabledelayedexpansion enableextensions
@set PROTO_FILES=
@for %%x in (%SRC_DIR%\protos\model\*.proto) do @set PROTO_FILES=!PROTO_FILES! %%x
@set PROTO_FILES=%PROTO_FILES:~1%

%PROTOC% --plugin=protoc-gen-gogoslick=%PBPLUGIN_DIR%\protoc-gen-gogoslick.exe -I=%GOPATH%\src -I=%SRC_DIR%\protos --gogoslick_out=%SRC_DIR% %PROTO_FILES%
::%PROTOC% --plugin=protoc-gen-mgo-go=%PBPLUGIN_DIR%\protoc-gen-mgo-go.exe -I=%GOPATH%\src -I=%SRC_DIR%\protos --mgo-go_out=%SRC_DIR% --mgo-go_opt=tpl=%SRC_DIR%\protos\model.gohtml %PROTO_FILES%
%PROTOC% --plugin=protoc-gen-mgo-go=%PBPLUGIN_DIR%\protoc-gen-mgo-go.exe -I=%GOPATH%\src -I=%SRC_DIR%\protos --mgo-go_out=%SRC_DIR% %PROTO_FILES%

@set PB_FILES=
@for %%x in (%SRC_DIR%\model\*.pb.go) do %PBPLUGIN_DIR%\protoc-go-inject-tag.exe -XXX_skip=yaml,xml -input=%%x
