package accounts

import (
	"fmt"
	"runtime/debug"
	"sync"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	napp "nggs/app"
	nexport "nggs/export"
	nlog "nggs/log"
	nservice "nggs/service"

	"cluster"
	"model"
	"rpc"
)

const (
	ServiceName      = cluster.AccountsServiceName
	ServiceActorName = ServiceName
)

type Service struct {
	*nactor.Actor

	appCfg *napp.Config

	globalCfg *cluster.AccountsGlobalConfig
	cfg       *cluster.AccountsConfig

	dbCfg cluster.MongoDBConfig
	dbc   *model.SimpleClient

	instance *nservice.Instance

	accountPIDs      map[int64]*actor.PID // account id -> account pid
	accountStartedWg sync.WaitGroup
	accountStoppedWg sync.WaitGroup
}

func New() *Service {
	svc := &Service{
		dbc:         model.NewSimpleClient(),
		instance:    nservice.NewInstance(),
		accountPIDs: map[int64]*actor.PID{},
	}
	return svc
}

func (svc *Service) Init(iAppConfig nexport.IAppConfig, serviceID nexport.ServiceID, startedWg *sync.WaitGroup, stoppedWg *sync.WaitGroup, args ...interface{}) (err error) {
	var ok bool
	svc.appCfg, ok = iAppConfig.(*napp.Config)
	if !ok || svc.appCfg == nil {
		err = fmt.Errorf("init app config fail")
		return
	}

	svc.globalCfg = cluster.MustGetAccountsGlobalConfig()
	svc.cfg = cluster.MustGetAccountsConfig(serviceID)

	// 根据配置初始化日志
	if svc.appCfg.LogDir != "" {
		logDir := fmt.Sprintf("%s/%s-%d", svc.appCfg.LogDir, ServiceName, svc.cfg.GetID())
		logger = nlog.New(logDir, ServiceName)
		logger.SetLevel(initLogLevel)
	}

	svc.dbCfg = cluster.MustGetMainDBConfig()

	err = svc.dbc.Init(svc.dbCfg.Url, svc.dbCfg.SessionNum, svc.dbCfg.Name)
	if err != nil {
		err = fmt.Errorf("init db fail, %w", err)
		return
	}

	svc.Actor = nactor.New(
		nactor.WithLogger(logger),
		nactor.WithStartedWaitGroup(startedWg),
		nactor.WithStoppedWaitGroup(stoppedWg),
		nactor.WithOnReceiveMessage(svc.onReceiveMessage),
		nactor.WithOnStarted(svc.onStarted),
		nactor.WithOnStopping(svc.onStopping),
		nactor.WithOnStopped(svc.onStopped),
		nactor.WithOnActorTerminate(svc.onActorTerminated),
		nactor.WithRPC(rpc.Protocol, nil, nil),
	)

	return
}

func (svc *Service) Run(ctx actor.Context, pprofAddress string, args ...interface{}) (err error) {
	err = svc.Actor.Start(ctx, fmt.Sprintf("%s-%d", ServiceActorName, svc.cfg.GetID()))
	if err != nil {
		return
	}

	svc.instance.PID = svc.PID()
	svc.instance.PprofAddress = pprofAddress

	err = cluster.TryLockPosition(ServiceName, svc.cfg.GetID(), svc.instance)
	if err != nil {
		err = fmt.Errorf("try lock position fail, %w", err)
		return
	}

	err = svc.regAllRPCHandler()
	if err != nil {
		return
	}

	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (svc *Service) onStarted(ctx actor.Context) {

}

func (svc *Service) onStopping(ctx actor.Context) {

}

func (svc *Service) onStopped(ctx actor.Context) {

}

func (svc *Service) onReceiveMessage(ctx actor.Context) {
	defer func() {
		if r := recover(); r != nil {
			svc.Error("%v\n%s", r, debug.Stack())
			panic(r)
		}
	}()

	sender := ctx.Sender()
	switch msg := ctx.Message().(type) {
	default:
		svc.Error("recv unsupported msg [%#v] from [%v]", msg, sender)
	}
}

func (svc *Service) onActorTerminated(who *actor.PID, ctx actor.Context) {
	svc.Debug("[%s] terminated", who.Id)

	var appID int32
	var accountsID int32
	var accountID int64
	if _, err := fmt.Sscanf(who.Id, "app-%d/accounts-%d/account-%d", &appID, &accountsID, &accountID); err == nil {
		delete(svc.accountPIDs, accountID)
		svc.Info("delete account pid from map, accountID=%d", accountID)
	}
}
