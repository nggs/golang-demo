package accounts

import (
	"fmt"
	"server/src/entities/account"
	"strconv"
	"time"

	"github.com/asynkron/protoactor-go/actor"
	"github.com/globalsign/mgo/bson"

	nactor "nggs/actor"
	nlog "nggs/log"
	nrpc "nggs/rpc"

	"model"
	"rpc"
)

func (svc *Service) regAllRPCHandler() (err error) {
	err = svc.RegisterRPCHandler(rpc.C2S_GetAccountPID_MessageID(), svc.handleGetAccountPID)
	if err != nil {
		return
	}
	err = svc.RegisterRPCHandler(rpc.C2S_CMD_MessageID(), svc.handleCMD)
	if err != nil {
		return
	}
	return
}

func (svc *Service) spawnAccount(data *model.Account, ctx actor.Context) (pid *actor.PID, err error) {
	a := account.New(svc.Logger(), data, svc.dbc, *svc.globalCfg, *svc.cfg, &svc.accountStartedWg, &svc.accountStoppedWg)
	err = a.Start(ctx, fmt.Sprintf("account-%d", data.ID))
	if err != nil {
		return
	}
	a.WaitForStarted()
	pid = a.PID().Clone()
	return
}

func (svc *Service) handleGetAccountPID(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_GetAccountPID)
	send := iSend.(*rpc.S2C_GetAccountPID)
	send.ID = recv.ID
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
		if send.EC == rpc.EC_OK {
			svc.Debug("handleGetAccountPID success, send=%s", send.String())
		} else {
			svc.Error("handleGetAccountPID fail, send=%s", send.String())
		}
	}()

	if pid, ok := svc.accountPIDs[recv.ID]; ok {
		send.PID = rpc.FromRealPID(pid)
		return
	}

	dbSession := svc.dbc.GetSession()
	defer svc.dbc.PutSession(dbSession)

	accountData, err := svc.dbc.FindOneAccount(dbSession, bson.M{"_id": recv.ID})
	if err != nil {
		svc.Error("handleGetAccountPID load data fail, account id=%d, %s", recv.ID, err)
		send.EC = rpc.EC_Fail
		return
	}

	svc.Debug("handleGetAccountPID load data success, account id=%d", recv.ID)

	pid, err := svc.spawnAccount(accountData, ctx)
	if err != nil {
		svc.Error("handleGetAccountPID spawn fail, account id=%d, %s", recv.ID, err)
		send.EC = rpc.EC_Fail
		return
	}

	svc.accountPIDs[recv.ID] = pid

	send.PID = rpc.FromRealPID(pid)
}

func (svc *Service) handleCMD(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	var recv = iRecv.(*rpc.C2S_CMD)
	var send *rpc.S2C_CMD
	var needRespond bool
	if len(args) > 0 {
		send = args[0].(*rpc.S2C_CMD)
	}
	if send == nil {
		send = rpc.Get_S2C_CMD()
		needRespond = true
	}
	defer func() {
		if sender != nil && needRespond {
			ctx.Respond(send)
		}
		svc.Info("handleCMD, recv=%s, send=%s", recv.String(), send.String())
	}()

	switch recv.Type {
	case "debug_on":
		logger.SetLevel(nlog.LevelDebug)
		send.Result = "debug on"

	case "debug_off":
		logger.SetLevel(nlog.LevelInformational)
		send.Result = "debug off"

	case "account_num":
		send.Result = strconv.Itoa(len(svc.accountPIDs))

	case "repeated_account_num":
		var accountIDMap = map[int64]struct{}{}
		var repeatedAccountIDMap = map[int64]struct{}{}
		var repeatedNum int
		for _, accountPID := range svc.accountPIDs {
			iRecv, err := nactor.RootContext().RequestFuture(accountPID, "id", 3*time.Second).Result()
			if err != nil {
				svc.Error("send id to %v fail, %s", accountPID, err)
				return
			}
			id, ok := iRecv.(int64)
			if !ok {
				svc.Error("send id to %v, recv not int64", accountPID, err)
				return
			}
			if id == 0 {
				svc.Error("send id to %v, recv 0", accountPID)
				return
			}
			if _, ok := accountIDMap[id]; ok {
				repeatedNum += 1
				repeatedAccountIDMap[id] = struct{}{}
			} else {
				accountIDMap[id] = struct{}{}
			}
		}
		send.Result = fmt.Sprintf("%d, ids=%v", repeatedNum, repeatedAccountIDMap)

	case "flush_one":
		strID, ok := recv.Params["id"]
		if !ok || strID == "" {
			send.Result = "need id"
			return
		}
		id, err := strconv.Atoi(strID)
		if err != nil {
			send.Result = fmt.Sprintf("invalid id, %s", err)
			return
		}

		pid, ok := svc.accountPIDs[int64(id)]
		if !ok {
			send.Result = fmt.Sprintf("account not spawned")
			return
		}
		_, err = nactor.RootContext().RequestFuture(pid, "stop", 10*time.Second).Result()
		if err != nil {
			send.Result = fmt.Sprintf("send rpc msg fail, %s", err)
			return
		}

		send.Result = fmt.Sprintf("flush %d", id)

	default:
		send.Result = "unsupported cmd type"
		return
	}
}
