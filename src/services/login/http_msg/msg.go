package http_msg

import (
	"encoding/json"
	"strconv"

	nhttp "nggs/network/http"
)

type ErrorCode = int

const (
	EC_Success ErrorCode = iota
	EC_Fail
	EC_NotReady
	EC_NeedSign
	EC_NeedAccountName
	EC_NeedAccountPassword
	EC_InvalidAccountName
	EC_AccountExist
	EC_InvalidAccountPassword
	EC_CreateFail
	EC_RegisterFail
	EC_AccountNotExist
	EC_AccountPasswordNotCorrect
	EC_AccountBanned
	EC_AccountLoginTooOften
	EC_NeedAuthCode
	EC_NeedChannel
	EC_InvalidServerID
	EC_InvalidParam
	EC_LackParam
	EC_InvalidSign
)

var errorMessages = map[int]string{
	EC_Success:                   "success",
	EC_Fail:                      "fail",
	EC_NotReady:                  "not ready",
	EC_NeedSign:                  "need sign",
	EC_NeedAccountName:           "need account name",
	EC_NeedAccountPassword:       "need account password",
	EC_InvalidAccountName:        "invalid account name",
	EC_AccountExist:              "account exist",
	EC_InvalidAccountPassword:    "invalid account password",
	EC_CreateFail:                "create fail",
	EC_RegisterFail:              "register fail, maybe already exist",
	EC_AccountNotExist:           "account not exist",
	EC_AccountPasswordNotCorrect: "account password not correct",
	EC_AccountBanned:             "account banned",
	EC_AccountLoginTooOften:      "account login too often",
	EC_NeedAuthCode:              "need auth code",
	EC_NeedChannel:               "need channel",
	EC_InvalidServerID:           "invalid server id",
	EC_InvalidParam:              "invalid param",
	EC_LackParam:                 "lack param",
}

func ErrorMessage(ec int) string {
	return errorMessages[ec]
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ResponseRegister struct {
	nhttp.Response
}

func (r ResponseRegister) String() string {
	ba, _ := json.Marshal(r)
	return string(ba)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ResponseVisitor struct {
	nhttp.Response
	AccountName     string `json:"account_name,omitempty"`
	AccountPassword string `json:"account_password,omitempty"`
}

func (r ResponseVisitor) String() string {
	ba, _ := json.Marshal(r)
	return string(ba)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ResponseLogin struct {
	nhttp.Response
	Token string `json:"token,omitempty"`
	Addr  string `json:"addr,omitempty"`
}

func (r ResponseLogin) String() string {
	ba, _ := json.Marshal(r)
	return string(ba)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func parseStringRequestParam(requestParams map[string]string, requestParamName string, must bool) (requestParam string, err error) {
	var ok bool
	requestParam, ok = requestParams[requestParamName]
	if !ok || requestParam == "" {
		if must {
			err = ErrLackParam
			return
		}
	}
	return
}

func parseIntRequestParam(requestParams map[string]string, requestParamName string, must bool) (requestParam int, err error) {
	s, ok := requestParams[requestParamName]
	if !ok || s == "" {
		if must {
			err = ErrLackParam
			return
		}
	} else {
		requestParam, err = strconv.Atoi(s)
		if err != nil {
			return
		}
	}
	return
}

func parseBoolRequestParam(requestParams map[string]string, requestParamName string, must bool) (requestParam bool, err error) {
	s, ok := requestParams[requestParamName]
	if !ok || s == "" {
		if must {
			err = ErrLackParam
			return
		}
	} else {
		requestParam, err = strconv.ParseBool(s)
		if err != nil {
			return
		}
	}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//type RequestHuaWeiQuickGameRegister struct {
//	AccountName   string `json:"account_name"`
//	TimeStamp     string `json:"time_stamp"`
//	Sign          string `json:"sign"`
//	IdleSign      string `json:"idle_sign"`
//	Channel       string `json:"channel"`
//	OS            string `json:"os"`
//	ClientVersion string `json:"client_version"`
//}
//
//func NewRequestHuaWeiQuickGameRegister() RequestHuaWeiQuickGameRegister {
//	return RequestHuaWeiQuickGameRegister{}
//}
//
//func (req *RequestHuaWeiQuickGameRegister) UnmarshalFromHttpRequest(r *http.Request) (reqParams map[string]string, ec ErrorCode, err error) {
//	err = r.ParseForm()
//	if err != nil {
//		ec = EC_Fail
//		return
//	}
//
//	reqParams = nhttp.CollectRequestParams(r)
//
//	req.AccountName, err = parseStringRequestParam(reqParams, "account_name", true)
//	if err != nil {
//		ec = EC_LackParam
//		err = fmt.Errorf("parse account_name fail, %w", err)
//		return
//	}
//
//	req.TimeStamp, err = parseStringRequestParam(reqParams, "time_stamp", true)
//	if err != nil {
//		ec = EC_LackParam
//		err = fmt.Errorf("parse time_stamp fail, %w", err)
//		return
//	}
//
//	req.IdleSign, err = parseStringRequestParam(reqParams, "idle_sign", true)
//	if err != nil {
//		ec = EC_LackParam
//		err = fmt.Errorf("parse idle_sign fail, %w", err)
//		return
//	}
//
//	req.Sign, err = parseStringRequestParam(reqParams, "sign", true)
//	if err != nil {
//		ec = EC_LackParam
//		err = fmt.Errorf("parse sign fail, %w", err)
//		return
//	}
//
//	req.Channel, err = parseStringRequestParam(reqParams, "channel", true)
//	if err != nil {
//		ec = EC_LackParam
//		err = fmt.Errorf("parse channel fail, %w", err)
//		return
//	}
//
//	req.OS, err = parseStringRequestParam(reqParams, "os", true)
//	if err != nil {
//		ec = EC_LackParam
//		err = fmt.Errorf("parse os fail, %w", err)
//		return
//	}
//
//	req.ClientVersion, err = parseStringRequestParam(reqParams, "client_version", true)
//	if err != nil {
//		ec = EC_LackParam
//		err = fmt.Errorf("parse client_version fail, %w", err)
//		return
//	}
//
//	return
//}
//
//type ResponseHuaWeiQuickGameRegister struct {
//	Response
//	AccountName     string `json:"account_name,omitempty"`
//	AccountPassword string `json:"account_password,omitempty"`
//}
//
//func NewResponseHuaWeiQuickGameRegister() ResponseHuaWeiQuickGameRegister {
//	return ResponseHuaWeiQuickGameRegister{}
//}
//
//func (r ResponseHuaWeiQuickGameRegister) String() string {
//	ba, _ := json.Marshal(r)
//	return string(ba)
//}
