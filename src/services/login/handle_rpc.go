package login

func (svc *Service) regAllRPCHandler() (err error) {
	//svc.rpcMsgDispatcher.MustRegister(rpc.C2S_UpdateAccountLoginData_MessageID(), svc.handleUpdateAccountLoginData)
	//svc.rpcMsgDispatcher.MustRegister(rpc.C2S_UpdateAccountBan_MessageID(), svc.handleUpdateAccountBan)
	//svc.rpcMsgDispatcher.MustRegister(rpc.C2S_UpdateAccountWeChatSubscribe_MessageID(), svc.handleUpdateAccountWeChatSubscribe)
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//func (svc *Service) handleUpdateAccountLoginData(sender *actor.PID, iMsg msrpc.IMessage, ctx actor.Context, args ...interface{}) {
//	recv := iMsg.(*rpc.C2S_UpdateAccountLoginData)
//
//	accountID, serverID := model.RoleIDToAccountIDServerID(recv.RoleID)
//
//	dbSession := svc.dbc.GetSession()
//	defer svc.dbc.PutSession(dbSession)
//
//	// 加载账号数据
//	accountData, err := svc.dbc.FindOne_Account(dbSession, bson.M{"_id": accountID})
//	if err != nil {
//		svc.Error("handleUpdateAccountLoginData account not exist, id=%d, %s", accountID, err)
//		return
//	}
//
//	nowUnix := time.Now().Unix()
//
//	// 更新账号表中的最后登录服务器id
//	accountData.LastLoginServerID = serverID
//
//	// 角色首次登陆的服务器ID
//	if accountData.FirstLoginServerID <= 0 {
//		accountData.FirstLoginServerID = serverID
//	}
//
//	// 账号首次付费时间
//	if accountData.FirstPayTime <= 0 {
//		accountData.FirstPayTime = recv.RechargeTime
//	}
//
//	// 更新账号表中的最后登陆服务器列表
//	lastLoginServerListContainer := accountData.LastLoginServerListContainer()
//	loginInfo := model.Get_AccountRoleLoginInfo()
//	loginInfo.Level = recv.Level
//	loginInfo.Avatar = recv.AvatarID
//	loginInfo.LoginTime = recv.LastLoginTime
//	lastLoginServerListContainer.Set(serverID, loginInfo)
//	lastLoginServerListContainer.RemoveSome(func(serverID int32, info *model.AccountRoleLoginInfo) (continued bool) {
//		return info.LoginTime <= nowUnix-30*86400
//	})
//
//	accountData.UpdateByID(dbSession, svc.dbc.DBName())
//}
//
//func (svc *Service) handleUpdateAccountBan(sender *actor.PID, iMsg msrpc.IMessage, ctx actor.Context, args ...interface{}) {
//	recv := iMsg.(*rpc.C2S_UpdateAccountBan)
//	var send *rpc.S2C_UpdateAccountBan
//	var needRespond bool
//	if len(args) > 0 {
//		send = args[0].(*rpc.S2C_UpdateAccountBan)
//	}
//	if send == nil {
//		send = rpc.Get_S2C_UpdateAccountBan()
//		needRespond = true
//	}
//	defer func() {
//		if sender != nil && needRespond {
//			ctx.Respond(send)
//		}
//	}()
//
//	dbSession := svc.dbc.GetSession()
//	defer svc.dbc.PutSession(dbSession)
//
//	// 加载账号数据
//	accountData, err := svc.dbc.FindOne_Account(dbSession, bson.M{"_id": recv.AccountID})
//	if err != nil {
//		svc.Error("handleUpdateAccountLoginData account not exist, account id=%d, %s", recv.AccountID, err)
//		send.EC = rpc.EC_DataNotFound
//		return
//	}
//
//	if accountData.Ban == recv.Ban {
//		svc.Error("handleUpdateAccountLoginData account ban no change, account id=%d, ban=%d, %s", recv.AccountID, recv.Ban, err)
//		send.EC = rpc.EC_DataNoChange
//		return
//	}
//
//	//accountData.Ban = recv.Ban
//
//	if _, err := svc.dbc.UpsertID_Account(dbSession, recv.AccountID, bson.M{"$set": bson.M{"Ban": recv.Ban}}); err != nil {
//		svc.Error("handleUpdateAccountLoginData update account ban fail, account id=%d, ban=%d, %s", recv.AccountID, recv.Ban, err)
//		send.EC = rpc.EC_Fail
//		return
//	}
//
//	svc.Info("handleUpdateAccountLoginData update account ban success, account id=%d, ban=%d", recv.AccountID, recv.Ban)
//}
//
//func (svc *Service) handleUpdateAccountWeChatSubscribe(sender *actor.PID, iMsg msrpc.IMessage, ctx actor.Context, args ...interface{}) {
//	recv := iMsg.(*rpc.C2S_UpdateAccountWeChatSubscribe)
//
//	dbSession := svc.dbc.GetSession()
//	defer svc.dbc.PutSession(dbSession)
//
//	// 加载账号数据
//	accountData, err := svc.dbc.FindOne_Account(dbSession, bson.M{"_id": recv.AccountID})
//	if err != nil {
//		svc.Error("handleUpdateAccountWeChatSubscribe account not exist, id=%d, %s", recv.AccountID, err)
//		return
//	}
//	for k, v := range recv.TemplateData {
//		switch k {
//		case "ENEI8ta73OGYVqmECg9C1UnyCRZt01Ck2mkZMC_CZFU":
//			if v == "accept" {
//				accountData.WeChatSubscribeOfflineIdleFull = true
//			} else {
//				accountData.WeChatSubscribeOfflineIdleFull = false
//			}
//		case "hSstgs8NnOTIRs5Jzskp4FgUkbyAujhF-mLPPdxBciw":
//			if v == "accept" {
//				accountData.WeChatSubScribeRescueTimesFull = true
//			} else {
//				accountData.WeChatSubScribeRescueTimesFull = false
//			}
//		}
//	}
//	_ = accountData.UpdateByID(dbSession, svc.dbc.DBName())
//}
