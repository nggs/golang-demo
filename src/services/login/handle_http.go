package login

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"unicode"

	"github.com/gin-gonic/gin"
	"github.com/globalsign/mgo/bson"

	nmongodb "nggs/db/mongodb"
	nhttp "nggs/network/http"

	"cluster"
	"model"
	"rpc"

	loginMsg "server/src/services/login/http_msg"
)

const (
	signKey            = "sign"
	accountNameKey     = "acc"
	accountPasswordKey = "pwd"
	channelKey         = "channel"
	osKey              = "os"
	reloginKey         = "relogin"
	clientVerKey       = "clientVersion"
	serverIDKey        = "serverID"

	minAccountNameLen     = 2
	maxAccountNameLen     = 64 //长度为了兼容微信openid以及其他渠道账号
	minAccountPasswordLen = 6
	maxAccountPasswordLen = 16
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func checkAccountName(name string) error {
	nameWords := []rune(name)
	lenName := len(nameWords)
	if lenName < minAccountNameLen || lenName > maxAccountNameLen {
		return fmt.Errorf("length must >= %d and <= %d", minAccountNameLen, maxAccountNameLen)
	}
	for _, w := range nameWords {
		if !unicode.IsLetter(w) && !unicode.IsNumber(w) && !strings.ContainsRune("-_", w) {
			// 用户名不能包含字母、数字、减号、下划线以外的字符
			return fmt.Errorf("must only contain letter or number")
		}
	}
	return nil
}

func checkAccountPassword(pwd string) error {
	pwdWords := []rune(pwd)
	lenPwd := len(pwdWords)
	if lenPwd < minAccountPasswordLen || lenPwd > maxAccountPasswordLen {
		return fmt.Errorf("length must >= %d and <= %d", minAccountPasswordLen, maxAccountPasswordLen)
	}
	for _, r := range pwdWords {
		if !unicode.IsLetter(r) && !unicode.IsNumber(r) {
			// 密码不能包含字母和数字以外的字符
			return fmt.Errorf("must only contain letter or number")
		}
	}
	return nil
}

func (svc *Service) checkAccountExist(name string, dbSession *nmongodb.Session) error {
	_, err := svc.dbc.FindOneAccountMinimal(dbSession, bson.M{"Name": name})
	if err == nil {
		return fmt.Errorf("account[%s] already exist", name)
	}
	return nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (svc *Service) regAllHttpHandler() (err error) {
	err = svc.RegisterHttpRequestHandler("GET", "/register", svc.handleRegister, nil)
	if err != nil {
		return
	}
	err = svc.RegisterHttpRequestHandler("GET", "/visitor", svc.handleVisitor, nil)
	if err != nil {
		return
	}
	err = svc.RegisterHttpRequestHandler("GET", "/login", svc.handleLogin, nil)
	if err != nil {
		return
	}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (svc *Service) handleRegister(ctx *gin.Context) {
	ctx.Writer.Header().Set("Access-Control-Allow-Origin", "*")

	var resp loginMsg.ResponseRegister
	defer func() {
		if resp.ErrorMessage == "" {
			resp.ErrorMessage = loginMsg.ErrorMessage(resp.ErrorCode)
		}
		ctx.String(http.StatusOK, resp.String())
	}()

	// 服务器是否准备好
	if !cluster.IsReady() {
		svc.Error("handleRegister cluster not ready")
		resp.ErrorCode = loginMsg.EC_NotReady
		return
	}

	err := ctx.Request.ParseForm()
	if err != nil {
		svc.Error("handleRegister parse form fail, %v", err)
		resp.ErrorCode = loginMsg.EC_Fail
		resp.ErrorMessage = err.Error()
		return
	}

	reqParams := nhttp.CollectRequestParams(ctx.Request)
	svc.Debug("handleRegister request params=%+v", reqParams)

	sign, ok := reqParams[signKey]
	if !ok {
		svc.Error("handleRegister need sign")
		resp.ErrorCode = loginMsg.EC_NeedSign
		return
	}

	resp.Sign = sign

	accountName, ok := reqParams[accountNameKey]
	if !ok {
		svc.Error("handleRegister need account name")
		resp.ErrorCode = loginMsg.EC_NeedAccountName
		return
	}
	accountPassword, ok := reqParams[accountPasswordKey]
	if !ok {
		svc.Error("handleRegister need account password")
		resp.ErrorCode = loginMsg.EC_NeedAccountPassword
		return
	}

	// 1.检查帐号名是否有效
	err = checkAccountName(accountName)
	if err != nil {
		svc.Error("handleRegister fail invalid account name[%s], %v", accountName, err)
		resp.ErrorCode = loginMsg.EC_InvalidAccountName
		resp.ErrorMessage = fmt.Sprintf("invalid account name, %v", err)
		return
	}

	// 不能手动注册游客帐号
	if model.IsVisitorName(accountName) {
		svc.Error("handleRegister fail invalid account name[%s], cannot manually register visitor name", accountName)
		resp.ErrorCode = loginMsg.EC_InvalidAccountName
		return
	}

	// 2.检查密码是否有效
	err = checkAccountPassword(accountPassword)
	if err != nil {
		svc.Error("handleRegister fail invalid account password[%s], %v", accountPassword, err)
		resp.ErrorCode = loginMsg.EC_InvalidAccountPassword
		resp.ErrorMessage = fmt.Sprintf("invalid account password, %v", err)
		return
	}

	dbSession := svc.dbc.GetSession()
	defer svc.dbc.PutSession(dbSession)

	// 3.检查是否已注册
	if err := svc.checkAccountExist(accountName, dbSession); err != nil {
		svc.Error("handleRegister fail account[%s] exist", accountName)
		resp.ErrorCode = loginMsg.EC_AccountExist
		return
	}

	// 获取渠道名
	var registerChannelName string
	if val, ok := reqParams[channelKey]; ok {
		if len(val) > 0 {
			registerChannelName = strings.TrimSpace(val)
		}
	}

	// 获取os
	var registerOS string
	if val, ok := reqParams[osKey]; ok {
		if len(val) > 0 {
			registerOS = strings.TrimSpace(val)
		}
	}

	// 获取客户端版本号
	var registerClientVersion string
	if val, ok := reqParams[clientVerKey]; ok {
		if len(val) > 0 {
			registerClientVersion = strings.TrimSpace(val)
		}
	}

	registerIP := nhttp.GetRequestIP(ctx.Request)

	accountData, err := svc.dbc.CreateAccount(accountName, accountPassword, registerChannelName, registerOS, registerClientVersion, registerIP)
	if err != nil {
		svc.Error("handleRegister fail create account[%s] fail, %v", accountName, err)
		resp.ErrorCode = loginMsg.EC_CreateFail
		return
	}

	err = accountData.Insert(dbSession, svc.dbc.DBName())
	if err != nil {
		svc.Error("handleRegister fail insert account[%s] fail, %v", accountName, err)
		resp.ErrorCode = loginMsg.EC_RegisterFail
		return
	}

	svc.Debug("register account[%s] success", accountName)
}

func (svc *Service) handleVisitor(ctx *gin.Context) {
	ctx.Writer.Header().Set("Access-Control-Allow-Origin", "*")

	var resp loginMsg.ResponseVisitor
	defer func() {
		if resp.ErrorMessage == "" {
			resp.ErrorMessage = loginMsg.ErrorMessage(resp.ErrorCode)
		}
		ctx.String(http.StatusOK, resp.String())
	}()

	// 服务器是否准备好
	if !cluster.IsReady() {
		svc.Error("handleVisitor cluster not ready")
		resp.ErrorCode = loginMsg.EC_NotReady
		return
	}

	err := ctx.Request.ParseForm()
	if err != nil {
		svc.Error("handleVisitor parse form fail, %v", err)
		resp.ErrorCode = loginMsg.EC_Fail
		resp.ErrorMessage = err.Error()
		return
	}

	reqParams := nhttp.CollectRequestParams(ctx.Request)
	svc.Debug("handleVisitor request params=%+v", reqParams)

	sign, ok := reqParams[signKey]
	if !ok {
		svc.Error("handleVisitor need sign")
		resp.ErrorCode = loginMsg.EC_NeedSign
		return
	}

	// 获取渠道名
	var registerChannelName string
	if val, ok := reqParams[channelKey]; ok {
		if len(val) > 0 {
			registerChannelName = strings.TrimSpace(val)
		}
	}

	// 获取os
	var registerOS string
	if val, ok := reqParams[osKey]; ok {
		if len(val) > 0 {
			registerOS = strings.TrimSpace(val)
		}
	}

	var registerClientVersion string
	if val, ok := reqParams[clientVerKey]; ok {
		if len(val) > 0 {
			registerClientVersion = strings.TrimSpace(val)
		}
	}

	registerIP := nhttp.GetRequestIP(ctx.Request)

	resp.Sign = sign

	dbSession := svc.dbc.GetSession()
	defer svc.dbc.PutSession(dbSession)

	acc, err := svc.dbc.CreateVisitorAccount(registerChannelName, registerOS, registerClientVersion, registerIP)
	if err != nil {
		svc.Error("handleVisitor fail create visitor account fail, %v", err)
		resp.ErrorCode = loginMsg.EC_CreateFail
		return
	}

	err = acc.Insert(dbSession, svc.dbc.DBName())
	if err != nil {
		svc.Error("handleVisitor fail insert account[%s] fail, %v", acc.Name, err)
		resp.ErrorCode = loginMsg.EC_RegisterFail
		return
	}

	resp.AccountName = acc.Name
	resp.AccountPassword = acc.Password

	svc.Debug("register visitor account[%s] success", acc.Name)
}

func (svc *Service) handleLogin(ctx *gin.Context) {
	ctx.Writer.Header().Set("Access-Control-Allow-Origin", "*")

	var resp loginMsg.ResponseLogin
	defer func() {
		if resp.ErrorMessage == "" {
			resp.ErrorMessage = loginMsg.ErrorMessage(resp.ErrorCode)
		}
		//http.Error(w, resp.String(), http.StatusOK)
		ctx.String(http.StatusOK, resp.String())
	}()

	loginIP := nhttp.GetRequestIP(ctx.Request)

	// 服务器是否准备好
	if !cluster.IsReady() {
		svc.Error("handleLogin cluster not ready")
		resp.ErrorCode = loginMsg.EC_NotReady
		return
	}

	err := ctx.Request.ParseForm()
	if err != nil {
		svc.Error("handleLogin parse form fail, ip=%s, %v", loginIP, err)
		resp.ErrorCode = loginMsg.EC_Fail
		resp.ErrorMessage = err.Error()
		return
	}

	reqParams := nhttp.CollectRequestParams(ctx.Request)
	svc.Debug("handleLogin request params=%+v, ip=%s", reqParams, loginIP)

	sign, ok := reqParams[signKey]
	if !ok {
		svc.Error("handleLogin need sign")
		resp.ErrorCode = loginMsg.EC_NeedSign
		return
	}
	resp.Sign = sign

	accountName, ok := reqParams[accountNameKey]
	if !ok {
		svc.Error("handleLogin need account name, ip=%s", loginIP)
		resp.ErrorCode = loginMsg.EC_NeedAccountName
		return
	}
	accountPassword, ok := reqParams[accountPasswordKey]
	if !ok {
		svc.Error("handleLogin need account password, ip=%s", loginIP)
		resp.ErrorCode = loginMsg.EC_NeedAccountPassword
		return
	}
	// 1.检查帐号名是否有效
	err = checkAccountName(accountName)
	if err != nil {
		svc.Error("handleLogin invalid account name[%s], ip=%s, %v", accountName, loginIP, err)
		resp.ErrorCode = loginMsg.EC_InvalidAccountName
		resp.ErrorMessage = fmt.Sprintf("invalid account name, %v", err)
		return
	}
	// 2.检查密码是否有效
	err = checkAccountPassword(accountPassword)
	if err != nil {
		svc.Error("handleLogin invalid account password[%s], ip=%s, %v", accountPassword, loginIP, err)
		resp.ErrorCode = loginMsg.EC_InvalidAccountPassword
		resp.ErrorMessage = fmt.Sprintf("invalid account password, %v", err)
		return
	}

	var loginServerID int32
	if strServerID, ok := reqParams[serverIDKey]; ok && strServerID != "" {
		id, err := strconv.Atoi(strServerID)
		if err != nil {
			svc.Error("handleLogin invalid server id, %s, %s", strServerID, err)
			resp.ErrorCode = loginMsg.EC_InvalidServerID
			return
		}
		loginServerID = int32(id)
	}

	// 获取channel
	var loginChannel string
	if val, ok := reqParams[channelKey]; ok {
		if len(val) > 0 {
			loginChannel = strings.TrimSpace(val)
		}
	}

	// 获取os
	var loginOS string
	if val, ok := reqParams[osKey]; ok {
		if len(val) > 0 {
			loginOS = strings.TrimSpace(val)
		}
	}

	// 获取客户端版本
	var loginClientVersion string
	if val, ok := reqParams[clientVerKey]; ok {
		if len(val) > 0 {
			loginClientVersion = strings.TrimSpace(val)
		}
	}

	var isRelogin bool
	if val, ok := reqParams[reloginKey]; ok && val == "1" {
		// 断线重连标志
		isRelogin = true
	}

	//var sessionKey string
	//if val, ok := reqParams[SessionKey]; ok {
	//	if len(val) > 0 {
	//		sessionKey = strings.TrimSpace(val)
	//	}
	//}
	//
	//var shareTextID int32
	//if val, ok := reqParams[ShareTextID]; ok {
	//	if len(val) > 0 {
	//		intVal, err := strconv.Atoi(strings.TrimSpace(val))
	//		if err != nil {
	//			svc.Error("handleLogin shareTextID Atoi fail, err=%s", err)
	//			resp.ErrorCode = msg.EC_InvalidShareTextID
	//			return
	//		}
	//		shareTextID = int32(intVal)
	//	}
	//}

	dbSession := svc.dbc.GetSession()
	defer svc.dbc.PutSession(dbSession)

	// 检查帐号是否存在
	accountMinimal, err := svc.dbc.FindOneAccountMinimal(dbSession, bson.M{"Name": accountName})
	if err != nil {
		svc.Error("handleLogin account[%s] not exist, ip=%s, %s", accountName, loginIP, err)
		resp.ErrorCode = loginMsg.EC_AccountNotExist
		return
	}
	// 检查帐号是否已被封
	if accountMinimal.Ban != 0 {
		svc.Error("handleLogin account[%#v] has be banned, ip=%s", accountMinimal, loginIP)
		resp.ErrorCode = loginMsg.EC_AccountBanned
		return
	}
	// 检查密码
	if accountMinimal.Password != accountPassword {
		svc.Error("handleLogin account[%s] password[%s] not correct, ip=%s", accountName, accountPassword, loginIP)
		resp.ErrorCode = loginMsg.EC_AccountPasswordNotCorrect
		return
	}

	ec, token, gatewayAddr, err := rpc.AccountLogin(accountMinimal.ID, accountName, loginServerID, loginChannel, loginOS, loginClientVersion, loginIP, isRelogin)
	if err != nil {
		svc.Error("handleLogin rpc.AccountLogin fail, %s, ip=%s, %s", accountMinimal.String(), loginIP, err)
		resp.ErrorCode = loginMsg.EC_Fail
		return
	}
	if loginMsg.ErrorCode(ec) != loginMsg.EC_Success {
		svc.Error("handleLogin rpc.AccountLogin fail, %s, ip=%s, %v", accountMinimal.String(), loginIP, loginMsg.ErrorCode(ec))
		resp.ErrorCode = loginMsg.ErrorCode(ec)
		return
	}

	resp.Token = token
	resp.Addr = gatewayAddr

	//roleID := model.AccountIDServerIDToRoleID(accountMinimal.ID, loginServerID)
	//n, e := svc.dbc.GetRoleCount(dbSession, bson.M{"_id": roleID})
	//if e == nil {
	//	if n < 1 {
	//		// 创角
	//		if _, err := rpc.CreateRole(roleID); err != nil {
	//			svc.Error("handleLogin rpc.CreateRole fail, role id=%d", roleID)
	//		} else {
	//			svc.Info("handleLogin rpc.CreateRole success, role id=%d", roleID)
	//		}
	//	}
	//} else {
	//	// 数据库操作失败
	//	svc.Error("handleLogin svc.dbc.GetRoleCount fail, id=%d, %s", roleID, e)
	//}

	//loginOS := accountMinimal.OS
	//channelName := accountMinimal.ChannelName
	//// 新增
	//if oldLoginTime == 0 {
	//	stat.Record(stat.E_Type_New, loginOS, channelName, accountMinimal.ID)
	//} else {
	//	//当天首次登陆
	//	if !util.IsSameDayUnixTimeStamp(oldLoginTime, now) && isRelogin == false {
	//		// 参数：os, 渠道id, 账号id, 注册时间
	//		stat.Record(stat.E_Type_Active, loginOS, channelName, accountMinimal.ID, accountMinimal.RegisterTime)
	//		//svc.Debug("handleLogin accountMinimal[%s], oldLogin=[%s] nowLogin=[%s]", accountName, time.Unix(oldLoginTime, 0).Format("20060102"), time.Unix(now, 0).Format("20060102"))
	//	}
	//}

	svc.Debug("handleLogin login success, %s, ip=%s, token=%s", accountMinimal.String(), loginIP, resp.Token)
}
