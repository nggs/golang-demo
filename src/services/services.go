package services

import (
	nexport "nggs/export"
	nservice "nggs/service"

	"cluster"

	"server/src/services/accounts"
	"server/src/services/game"
	"server/src/services/gateway"
	"server/src/services/login"
	"server/src/services/world"
	"server/src/services/zones"
)

func Init() {
	nservice.FM.MustRegister(cluster.GatewayServiceName, func() nexport.IService { return gateway.New() })
	nservice.FM.MustRegister(cluster.LoginServiceName, func() nexport.IService { return login.New() })
	nservice.FM.MustRegister(cluster.AccountsServiceName, func() nexport.IService { return accounts.New() })
	nservice.FM.MustRegister(cluster.ZonesServiceName, func() nexport.IService { return zones.New() })
	nservice.FM.MustRegister(cluster.GameServiceName, func() nexport.IService { return game.New() })
	nservice.FM.MustRegister(cluster.WorldServiceName, func() nexport.IService { return world.New() })

	world.Init()
}
