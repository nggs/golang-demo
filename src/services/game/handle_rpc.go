package game

import (
	"fmt"
	"strconv"
	"time"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	nlog "nggs/log"
	nrpc "nggs/rpc"

	"rpc"

	"server/src/entities/role"
)

func (svc *Service) regAllRPCHandler() (err error) {
	err = svc.RegisterRPCHandler(rpc.C2S_CMD_MessageID(), svc.handleCMD)
	if err != nil {
		return
	}
	err = svc.RegisterRPCHandler(rpc.C2S_SpawnRole_MessageID(), svc.handleSpawnRole)
	return
}

func (svc *Service) handleCMD(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	var recv = iRecv.(*rpc.C2S_CMD)
	var send = iSend.(*rpc.S2C_CMD)
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
		svc.Info("handleCMD, recv=%s, send=%s", recv.String(), send.String())
	}()

	switch recv.Type {
	case "debug_on":
		logger.SetLevel(nlog.LevelDebug)
		send.Result = "debug on"

	case "debug_off":
		logger.SetLevel(nlog.LevelInformational)
		send.Result = "debug off"

	case "role_num":
		send.Result = strconv.Itoa(len(svc.rolePIDs))

	case "repeated_role_num":
		var idMap = map[int64]struct{}{}
		var repeatedIDMap = map[int64]struct{}{}
		var repeatedNum int
		for _, pid := range svc.rolePIDs {
			iRecv, err := nactor.RootContext().RequestFuture(pid, "id", 3*time.Second).Result()
			if err != nil {
				svc.Error("send id to %v fail, %s", pid, err)
				return
			}
			id, ok := iRecv.(int64)
			if !ok {
				svc.Error("send id to %v, recv not int64", pid, err)
				return
			}
			if id == 0 {
				svc.Error("send id to %v, recv 0", pid)
				return
			}
			if _, ok := idMap[id]; ok {
				repeatedNum += 1
				repeatedIDMap[id] = struct{}{}
			} else {
				idMap[id] = struct{}{}
			}
		}
		send.Result = fmt.Sprintf("%d, ids=%v", repeatedNum, repeatedIDMap)

	case "flush_one":
		strID, ok := recv.Params["id"]
		if !ok || strID == "" {
			send.Result = "need id"
			return
		}
		id, err := strconv.Atoi(strID)
		if err != nil {
			send.Result = fmt.Sprintf("invalid id, %s", err)
			return
		}

		pid, ok := svc.rolePIDs[int64(id)]
		if !ok {
			send.Result = fmt.Sprintf("role not spawned")
			return
		}
		_, err = nactor.RootContext().RequestFuture(pid, "stop", 10*time.Second).Result()
		if err != nil {
			send.Result = fmt.Sprintf("send rpc msg fail, %s", err)
			return
		}

		send.Result = fmt.Sprintf("flush %d", id)

	default:
		send.Result = "unsupported cmd type"
		return
	}
}

func (svc *Service) handleSpawnRole(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	var recv = iRecv.(*rpc.C2S_SpawnRole)
	var send = iSend.(*rpc.S2C_SpawnRole)
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
		svc.Info("handleSpawnRole, recv=%s, send=%s", recv.String(), send.String())
	}()

	if recv.AccountID <= 0 || recv.ServerID <= 0 {
		svc.Error("handleSpawnRole recv.AccountID <= 0 || recv.ServerID <= 0")
		send.EC = rpc.EC_Fail
		return
	}

	if recv.Account == nil {
		svc.Error("handleSpawnRole fail, recv.Account is nil")
		send.EC = rpc.EC_Fail
		return
	}

	if recv.Zone == nil {
		svc.Error("handleSpawnRole fail, recv.Zone is nil")
		send.EC = rpc.EC_Fail
		return
	}

	if rolePID, ok := svc.rolePIDs[recv.RoleID]; ok {
		svc.Debug("handleSpawnRole get role pid success, role id=[%d]", recv.RoleID)
		send.RolePID = rpc.FromRealPID(rolePID)
		return
	}

	iRole := role.New(logger, recv.SessionPID.ToReal(), recv.SessionID,
		svc.dbc, *svc.globalCfg, *svc.cfg, recv.Account, recv.Zone,
		recv.RoleID, recv.AccountID, recv.ServerID,
		&svc.roleStartedWg, &svc.roleStoppedWg)
	name := fmt.Sprintf("role-%d", recv.RoleID)
	if err := iRole.Start(ctx, name); err != nil {
		svc.Error("handleSpawnRole spawn role fail, role id=%d, %s", recv.RoleID, err)
		send.EC = rpc.EC_Fail
		return
	}

	iRole.WaitForStarted()

	rolePID := iRole.PID()

	send.RolePID = rpc.FromRealPID(rolePID)

	svc.rolePIDs[recv.RoleID] = rolePID

	svc.Debug("handleSpawnRole success, role id=[%d], role pid=%s", recv.RoleID, send.RolePID)
}
