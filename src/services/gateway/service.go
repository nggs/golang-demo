package gateway

import (
	"fmt"
	"net/http"
	"rpc"
	"runtime/debug"
	"strconv"
	"sync"
	"time"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	napp "nggs/app"
	ndebug "nggs/debug"
	nexport "nggs/export"
	nlog "nggs/log"
	nhttp "nggs/network/http"
	nws "nggs/network/websocket/v2"
	nservice "nggs/service"

	"cluster"
)

const (
	ServiceName      = cluster.GatewayServiceName
	ServiceActorName = ServiceName
	SessionActorName = "session"
)

type Service struct {
	*nactor.Actor

	appCfg            *napp.Config
	cfg               *cluster.GatewayConfig
	globalCfg         *cluster.GatewayGlobalConfig
	accountsGlobalCfg *cluster.AccountsGlobalConfig

	instance *nservice.Instance

	sessionStoppedWg sync.WaitGroup
}

func New() *Service {
	svc := &Service{
		instance: nservice.NewInstance(),
	}
	return svc
}

func (svc *Service) Init(iAppConfig nexport.IAppConfig, serviceID nexport.ServiceID, startedWg *sync.WaitGroup, stoppedWg *sync.WaitGroup, args ...interface{}) (err error) {
	var ok bool
	svc.appCfg, ok = iAppConfig.(*napp.Config)
	if !ok || svc.appCfg == nil {
		err = fmt.Errorf("init app config fail")
		return
	}

	svc.cfg = cluster.MustGetGatewayConfig(serviceID)
	svc.globalCfg = cluster.MustGetGatewayGlobalConfig()
	svc.accountsGlobalCfg = cluster.MustGetAccountsGlobalConfig()

	// 根据配置初始化日志
	if svc.appCfg.LogDir != "" {
		logDir := fmt.Sprintf("%s/%s-%d", svc.appCfg.LogDir, ServiceName, svc.cfg.GetID())
		logger = nlog.New(logDir, ServiceName)
		logger.SetLevel(initLogLevel)
	}

	svc.Actor = nactor.New(
		nactor.WithLogger(logger),
		nactor.WithStartedWaitGroup(startedWg),
		nactor.WithStoppedWaitGroup(stoppedWg),
		nactor.WithOnReceiveMessage(svc.onReceiveMessage),
		nactor.WithOnStarted(svc.onStarted),
		nactor.WithOnStopping(svc.onStopping),
		nactor.WithOnStopped(svc.onStopped),
		nactor.WithOnActorTerminate(svc.onActorTerminated),
		nactor.WithRPC(rpc.Protocol, nil, nil),
		nactor.WithWebsocketServer(svc.cfg.Server.ListenAddr, svc.globalCfg.TLSCertFile, svc.globalCfg.TLSKeyFile, nil, svc.produceSession, func(err error, server *nws.Server) {
			if err == nil {
				svc.Info("start websocket server success, addr=[%s]", server.ListenAddr())
			}
		}),
	)

	ndebug.RegisterCommand("/session_num", func(writer http.ResponseWriter, request *http.Request) {
		f := nactor.RootContext().RequestFuture(svc.PID(), "session_num", 3*time.Second)
		iRecv, err := f.Result()
		if err != nil {
			svc.Error("send session_num fail, %s", err)
			_, _ = writer.Write([]byte(fmt.Sprintf("send session_num fail, %s\n", err)))
			return
		}
		_, _ = writer.Write([]byte(fmt.Sprintf("session_num=%v\n", iRecv)))
	})

	return
}

func (svc *Service) Run(ctx actor.Context, pprofAddress string, args ...interface{}) (err error) {
	err = svc.Actor.Start(ctx, fmt.Sprintf("%s-%d", ServiceActorName, svc.cfg.GetID()))
	if err != nil {
		return
	}

	svc.instance.PID = svc.PID()
	svc.instance.PprofAddress = pprofAddress

	err = cluster.TryLockPosition(ServiceName, svc.cfg.GetID(), svc.instance)
	if err != nil {
		err = fmt.Errorf("try lock position fail, %w", err)
		return
	}

	err = svc.regAllRPCHandler()
	if err != nil {
		return
	}

	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (svc *Service) onStarted(ctx actor.Context) {

}

func (svc *Service) onStopping(ctx actor.Context) {
}

func (svc *Service) onStopped(ctx actor.Context) {
	svc.sessionStoppedWg.Wait()
}

func (svc *Service) onReceiveMessage(ctx actor.Context) {
	defer func() {
		if reason := recover(); reason != nil {
			svc.Error("%v\n%s", reason, debug.Stack())
			panic(reason)
		}
	}()

	sender := ctx.Sender()
	switch msg := ctx.Message().(type) {
	case *requestAcceptSession:
		if sender == nil {
			svc.Error("recv *requestAcceptSession but sender is nil")
			return
		}

		send := &responseAcceptSession{
			session: newSession(msg.ip, msg.version, msg.token,
				*svc.globalCfg, *svc.cfg, *svc.accountsGlobalCfg,
				logger, nil, &svc.sessionStoppedWg),
		}
		sessionName := fmt.Sprintf("%s-%d-%s", SessionActorName, send.session.ID(), send.session.IP())
		send.err = send.session.Start(ctx, sessionName)
		if send.err != nil {
			svc.Error("start %s fail, %s", sessionName, send.err)
		} else {
			svc.Debug("start %s success", sessionName)
		}
		ctx.Respond(send)

	case *rpc.C2S_BroadcastMsgToClient:
		for _, pid := range ctx.Children() {
			// 转发给所有session
			nactor.RootContext().Send(pid, msg)
		}

	case string:
		switch msg {
		case "session_num":
			if sender != nil {
				ctx.Respond(strconv.Itoa(len(ctx.Children())))
			}

		default:
			svc.Error("unknown command %s", msg)
		}

	default:
		svc.Error("recv unsupported msg [%#v] from [%v]", msg, sender)
	}
}

func (svc *Service) onActorTerminated(who *actor.PID, ctx actor.Context) {
	svc.Debug("[%s] terminated", who.Id)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type requestAcceptSession struct {
	ip      string
	version string
	token   string
}

type responseAcceptSession struct {
	err     error
	session *Session
}

func (svc *Service) produceSession(r *http.Request) nws.ISessionHandler {
	ip := nhttp.GetRequestIP(r)

	sessionNum := svc.WebsocketServerSessionNum()
	if sessionNum >= svc.globalCfg.MaxConnNum {
		// 连接数超过上限
		svc.Error("session reach max, sessionNum=[%d]", sessionNum)
		return nil
	}

	requestParams := nhttp.CollectRequestParams(r)

	version, ok := requestParams["version"]
	if ok && version == "" {
		// 带了token参数，但是传了个空字符串
		svc.Error("client send a empty version, ip=%s", ip)
		return nil
	}

	token, ok := requestParams["token"]
	if ok && token == "" {
		// 带了token参数，但是传了个空字符串
		svc.Error("client send a empty token, ip=%s", ip)
		return nil
	}

	f := nactor.RootContext().RequestFuture(svc.PID(), &requestAcceptSession{
		ip:      ip,
		version: version,
		token:   token,
	}, 15*time.Second)
	iResult, err := f.Result()
	if err != nil {
		svc.Error("accept session fail, path=[%s], %s", r.URL.Path, err)
		return nil
	}
	result := iResult.(*responseAcceptSession)
	if result.err != nil {
		svc.Error("accept session fail, path=[%s], %s", r.URL.Path, result.err)
		return nil
	}

	svc.Info("accept session from %s, id=%d", ip, result.session.id)

	return result.session
}
