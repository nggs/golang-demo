package gateway

import (
	"fmt"
	"runtime/debug"
	"server/src/entities/account/auth"
	"sync"
	"sync/atomic"
	"time"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	nlog "nggs/log"
	nproto "nggs/network/protocol"
	npb "nggs/network/protocol/protobuf/v3"
	nws "nggs/network/websocket/v2"

	"cluster"
	gameMsg "msg"
	"rpc"
)

type closeCode int

const (
	closeSessionStop closeCode = 4000
	closeRecvTimeout closeCode = 4001
	closeSendTimeout closeCode = 4002
)

var closeMessages = map[closeCode]string{
	closeSessionStop: "session stop",
	closeRecvTimeout: "recv msg time out",
	closeSendTimeout: "send msg time out",
}

func (c closeCode) String() string {
	if m, ok := closeMessages[c]; ok {
		return m
	}
	return "unknown code"
}

////////////////////////////////////////////////////////////////////////////////
type SessionID = uint64

////////////////////////////////////////////////////////////////////////////////
var gSessionID SessionID

// todo 将id生成规则替换成snowflake
func nextSessionID() SessionID {
	return atomic.AddUint64(&gSessionID, 1)
}

////////////////////////////////////////////////////////////////////////////////
type Session struct {
	*nactor.Actor

	id SessionID

	nws.SessionHandler

	session *nws.Session

	broker *npb.MessageBroker

	lastRecvMsgTime time.Time
	lastSendMsgTime time.Time

	gatewayGlobalCfg  cluster.GatewayGlobalConfig
	gatewayCfg        cluster.GatewayConfig
	accountsGlobalCfg cluster.AccountsGlobalConfig

	claims *auth.Claims

	roleID  int64
	rolePID *actor.PID

	ip      string
	version string
	token   string

	closed bool
}

func newSession(ip string, version string, token string, gatewayGlobalCfg cluster.GatewayGlobalConfig, gatewayCfg cluster.GatewayConfig, accountGlobalCfg cluster.AccountsGlobalConfig,
	logger nlog.ILogger, startedWg *sync.WaitGroup, stoppedWg *sync.WaitGroup) *Session {

	s := &Session{
		id:                nextSessionID(),
		broker:            npb.NewMessageBroker(),
		lastRecvMsgTime:   time.Now(),
		lastSendMsgTime:   time.Now(),
		gatewayGlobalCfg:  gatewayGlobalCfg,
		gatewayCfg:        gatewayCfg,
		accountsGlobalCfg: accountGlobalCfg,
		ip:                ip,
		version:           version,
		token:             token,
	}

	s.Actor = nactor.New(
		nactor.WithLogger(logger),
		nactor.WithStartedWaitGroup(startedWg),
		nactor.WithStoppedWaitGroup(stoppedWg),
		nactor.WithOnReceiveMessage(s.onReceiveMessage),
		nactor.WithOnStarted(s.onStarted),
		nactor.WithOnStopping(s.onStopping),
		nactor.WithOnStopped(s.onStopped),
		nactor.WithOnActorTerminate(s.onActorTerminated),
		nactor.WithPulse(30*time.Second, s.onPulse),
		nactor.WithMessage(gameMsg.Protocol, func(iProtocol npb.IProtocol, iRecv npb.IMessage, ctx actor.Context) (iSend npb.IMessage, args []interface{}, ok bool, err error) {
			s.lastSendMsgTime = time.Now()
			data, e := iProtocol.Encode(iRecv)
			if e != nil {
				err = fmt.Errorf("encode fail, msg=%s, %w", iRecv.String(), e)
				return
			}
			if e := s.session.WriteBinary(data); e != nil {
				err = fmt.Errorf("send binary msg fail, msg=%s, %w", iRecv.String(), e)
				return
			}
			switch iRecv.Head().MessageID {
			case gameMsg.S2C_ReloginNeedOffline_MessageID():
				s.rolePID = nil
				s.NewTimer(5*time.Second, 0, func(id nactor.TimerID, tag nactor.TimerID) {
					// 发送顶号提示包5秒后断开连接
					s.Info("stop because relogin")
					_ = s.Stop()
				})
			}
			return
		}, nil),
	)

	return s
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (s Session) ID() SessionID {
	return s.id
}

func (s Session) IP() string {
	return s.ip
}

func (s *Session) CloseWithCode(code closeCode) {
	if s.closed {
		return
	}
	_ = s.session.CloseWithMsg(nws.FormatCloseMessage(int(code), code.String()))
	s.closed = true
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (s *Session) HandleConnect(session *nws.Session) error {
	s.session = session
	return nil
}

func (s *Session) HandleDisconnect(session *nws.Session) {
	s.Debug("session disconnect")
	_ = s.Stop()
	return
}

type recvMsgFromClient struct {
	tYpE rpc.E_MessageType
	data []byte
}

func (s *Session) HandleTextMessage(session *nws.Session, msg []byte) {
	nactor.RootContext().Send(s.PID(), &recvMsgFromClient{tYpE: rpc.E_MessageType_Text, data: msg})
}

func (s *Session) HandleBinaryMessage(session *nws.Session, msg []byte) {
	nactor.RootContext().Send(s.PID(), &recvMsgFromClient{tYpE: rpc.E_MessageType_Binary, data: msg})
}

func (s *Session) HandleError(session *nws.Session, err error) {
	s.Error("error happened, %v", err)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (s *Session) onStarted(ctx actor.Context) {
	if s.token != "" {
		if err := s.auth(s.version, s.token); err != nil {
			s.Error("auth in onStarted fail, token=%s, %s", s.token, err)
			_ = s.Stop()
			return
		}
	}
}

func (s *Session) onStopping(ctx actor.Context) {
	s.CloseWithCode(closeSessionStop)
}

func (s *Session) onStopped(ctx actor.Context) {
	if !nactor.IsPIDPtrEmpty(s.rolePID) {
		nactor.RootContext().Send(s.rolePID, rpc.Get_C2S_Disconnect())
	}
}

func (s *Session) onPulse(ctx actor.Context) {
	now := time.Now()

	recvMsgInterval := now.Sub(s.lastRecvMsgTime)
	//sendMsgInterval := now.Sub(s.lastSendMsgTime)

	//if recvMsgInterval < s.gatewayGlobalCfg.RecvTimeoutSec ||
	//	sendMsgInterval < s.gatewayGlobalCfg.SendTimeoutSec {
	//	return
	//}

	if recvMsgInterval >= s.gatewayGlobalCfg.RecvTimeoutSec {
		// 太长时间没收包，断开连接
		s.Error("receive message timed out, now=[%v], lastRecvMsgTime=[%v], disconnect", now, s.lastRecvMsgTime)
		_ = s.Stop()
		return
	}

	//if sendMsgInterval >= s.gatewayGlobalCfg.SendTimeoutSec {
	//	// 太长时间没发包，断开连接
	//	s.Error("send message timed out, now=[%v], lastSendMsgTime=[%v], disconnect", now, s.lastSendMsgTime)
	//	_ = s.Stop()
	//	return
	//}
}

func (s *Session) auth(version string, token string) (err error) {
	// 校验token
	s.claims, err = auth.ParseToken(s.accountsGlobalCfg.TokenKey, token)
	if err != nil {
		err = fmt.Errorf("parse token fail, %w", err)
		return
	}
	if s.claims.AccountID <= 0 {
		err = fmt.Errorf("account id <= 0")
		return
	}
	if s.claims.ServerID <= 0 {
		err = fmt.Errorf("server id <= 0")
		return
	}
	if s.claims.GatewayID != s.gatewayCfg.GetID() {
		err = fmt.Errorf("gateway id not match, [%d]!=[%d]", s.claims.GatewayID, s.gatewayCfg.GetID())
		return
	}

	// 接受连接
	roleID, rolePID, e := rpc.AcceptConnection(s.PID(), s.id, s.claims.AccountID, s.claims.ServerID)
	if e != nil {
		err = fmt.Errorf("rpc.AcceptConnection fail, %w", e)
		return
	}

	s.roleID = roleID
	s.rolePID = rolePID
	//ctx.Watch(s.rolePID)

	s.SetSign(fmt.Sprintf("%s-%d-%s-%d", SessionActorName, s.id, s.ip, s.roleID))

	// 转发登录包给game上的role，触发登录流程
	send := gameMsg.Get_C2S_Login()
	send.Version = version
	send.Token = token
	send.IsRelogin = s.claims.IsRelogin
	nactor.RootContext().Send(s.rolePID, send)

	return
}

func (s *Session) onReceiveMessage(ctx actor.Context) {
	defer func() {
		if reason := recover(); reason != nil {
			s.Error("%v\n%s", reason, debug.Stack())
			panic(reason)
		}
	}()

	sender := ctx.Sender()
	now := time.Now()
	switch msg := ctx.Message().(type) {
	case *recvMsgFromClient:
		iRecv, err := gameMsg.Protocol.Decode(msg.data)
		if err != nil {
			s.Error("recv *recvMsgFromClient decode msg fail, %s", err)
			_ = s.Stop()
			return
		}

		err = s.broker.CanAccept(iRecv)
		if err != nil {
			switch err.(type) {
			case *nproto.ErrTooOften:
				send := gameMsg.Get_S2C_ACK()
				send.EC = gameMsg.EC_SendMsgTooOften
				d, _ := gameMsg.Protocol.Encode(send)
				switch msg.tYpE {
				case rpc.E_MessageType_Binary:
					_ = s.session.WriteBinary(d)
				case rpc.E_MessageType_Text:
					_ = s.session.WriteText(d)
				}
			case *nproto.ErrRecvTimesReachMax:
				send := gameMsg.Get_S2C_ACK()
				send.EC = gameMsg.EC_RecvMsgTimesReachMax
				d, _ := gameMsg.Protocol.Encode(send)
				switch msg.tYpE {
				case rpc.E_MessageType_Binary:
					_ = s.session.WriteBinary(d)
				case rpc.E_MessageType_Text:
					_ = s.session.WriteText(d)
				}
			default:
				s.Error("recv *recvMsgFromClient, but can not accept msg, %s", err)
				_ = s.Stop()
			}
			return
		}

		if s.claims != nil && s.rolePID != nil {
			s.lastRecvMsgTime = now

			switch recv := iRecv.(type) {
			case *gameMsg.C2S_Ping_EX:
				send := gameMsg.Get_S2C_Pong()
				send.MessageSeq = recv.MessageSeq
				send.Content = recv.Content
				d, _ := gameMsg.Protocol.Encode(send)
				switch msg.tYpE {
				case rpc.E_MessageType_Binary:
					_ = s.session.WriteBinary(d)
				case rpc.E_MessageType_Text:
					_ = s.session.WriteText(d)
				}

			default:
				nactor.RootContext().Send(s.rolePID, iRecv)
			}
		} else {
			// 还未验证

			switch recv := iRecv.(type) {
			case *gameMsg.C2S_Login_EX:
				s.lastRecvMsgTime = now

				if err := s.auth(recv.Version, recv.Token); err != nil {
					s.Error("recv C2S_Login, but auth fail, recv=%s, %s", recv.String(), err)
					_ = s.Stop()
					return
				}

				s.Info("recv C2S_Login, auth success, recv=%s", recv.String())

			//case *gameMsg.C2S_Ping_EX:
			//	s.lastRecvMsgTime = now
			//
			//	send := gameMsg.Get_S2C_Pong()
			//	send.Content = recv.GetContent()
			//	d, _ := gameMsg.Protocol.Encode(send)
			//	switch msg.tYpE {
			//	case rpc.E_MessageType_Binary:
			//		_ = s.session.WriteBinary(d)
			//	case rpc.E_MessageType_Text:
			//		_ = s.session.WriteText(d)
			//	}

			default:
				s.Error("recv unsupported msg: %s", iRecv.String())
				_ = s.Stop()
				return
			}
		}

	//case *rpc.C2S_SendMsgToClient:
	//	s.lastSendMsgTime = time.Now()
	//	switch msg.Type {
	//	case rpc.E_MessageType_Text:
	//		err := s.session.WriteText(msg.Data)
	//		if err != nil {
	//			s.Error("send text msg fail, %s", err)
	//		}
	//	case rpc.E_MessageType_Binary:
	//		err := s.session.WriteBinary(msg.Data)
	//		if err != nil {
	//			s.Error("send binary msg fail, %s", err)
	//		}
	//	default:
	//		s.Error("recv unsupported msg type [%s], %d", msg.Type)
	//	}

	//case *rpc.C2S_ReloginNeedOffline:
	//	send := gameMsg.Get_S2C_ReloginNeedOffline()
	//	d, _ := gameMsg.Protocol.Encode(send)
	//	_ = s.session.WriteBinary(d)
	//	s.NewTimer(5*time.Second, 0, func(id nactor.TimerID, tag nactor.TimerID) {
	//		// 发送顶号提示包5秒后断开连接
	//		s.Info("stop because relogin")
	//		_ = s.Stop()
	//	})

	case *rpc.C2S_BroadcastMsgToClient:
		if s.claims == nil || s.session == nil {
			return
		}
		if msg.ToServerID > 0 && s.claims.ServerID != msg.ToServerID {
			//消息有指定区服，当前session不在该区，则过滤
			return
		}
		var needSend bool
		if len(msg.IncludeRoleIDs) > 0 {
			if _, ok := msg.IncludeRoleIDs[s.roleID]; ok {
				needSend = true
			} else {
				needSend = false
			}
		} else if len(msg.ExcludeRoleIDs) > 0 {
			if _, ok := msg.ExcludeRoleIDs[s.roleID]; ok {
				needSend = false
			} else {
				needSend = true
			}
		}
		if !needSend {
			return
		}
		s.lastSendMsgTime = time.Now()
		switch msg.Type {
		case rpc.E_MessageType_Text:
			_ = s.session.WriteText(msg.Data)
		case rpc.E_MessageType_Binary:
			_ = s.session.WriteBinary(msg.Data)
		default:
			s.Error("unsupported msg type, %d", msg.Type)
		}

	case *rpc.C2S_Disconnect:
		s.Debug("recv *rpc.C2S_Disconnect, disconnect!!!")
		s.rolePID = nil
		_ = s.Stop()

	default:
		s.Error("recv unsupported message [%#v] from [%s]", msg, sender)
	}
}

func (s *Session) onActorTerminated(who *actor.PID, ctx actor.Context) {
	s.Info("[%s] terminated", who.Id)

	if s.rolePID != nil && s.rolePID.Address == who.Address && s.rolePID.Id == who.Id {
		s.Info("actor in game terminated")
		// todo 在game上对应的role终止后的处理
		// todo 直接退出
		_ = s.Stop()
		return
	}
}
