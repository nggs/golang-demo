package zones

import (
	"fmt"
	"runtime/debug"
	"sync"
	"time"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	napp "nggs/app"
	nexport "nggs/export"
	nlog "nggs/log"
	nservice "nggs/service"

	"cluster"
	"model"
	"rpc"

	"server/src/entities/zone"
)

const (
	ServiceName      = cluster.ZonesServiceName
	ServiceActorName = ServiceName
)

type Service struct {
	*nactor.Actor

	appCfg *napp.Config

	globalCfg *cluster.ZonesGlobalConfig
	cfg       *cluster.ZonesConfig

	dbCfg cluster.MongoDBConfig
	dbc   *model.SimpleClient

	instance *nservice.Instance

	zonePIDs      map[int32]*actor.PID // zone id -> zone pid
	zoneStartedWg sync.WaitGroup
	zoneStoppedWg sync.WaitGroup
}

func New() *Service {
	svc := &Service{
		dbc:      model.NewSimpleClient(),
		instance: nservice.NewInstance(),
		zonePIDs: map[int32]*actor.PID{},
	}
	return svc
}

func (svc *Service) Init(iAppConfig nexport.IAppConfig, serviceID int, startedWg *sync.WaitGroup, stoppedWg *sync.WaitGroup, args ...interface{}) (err error) {
	var ok bool
	svc.appCfg, ok = iAppConfig.(*napp.Config)
	if !ok || svc.appCfg == nil {
		err = fmt.Errorf("init app config fail")
		return
	}

	svc.globalCfg = cluster.MustGetZonesGlobalConfig()
	svc.cfg = cluster.MustGetZonesConfig(serviceID)

	// 根据配置初始化日志
	if svc.appCfg.LogDir != "" {
		logDir := fmt.Sprintf("%s/%s-%d", svc.appCfg.LogDir, ServiceName, svc.cfg.GetID())
		logger = nlog.New(logDir, ServiceName)
		logger.SetLevel(initLogLevel)
	}

	svc.dbCfg = cluster.MustGetMainDBConfig()

	err = svc.dbc.Init(svc.dbCfg.Url, svc.dbCfg.SessionNum, svc.dbCfg.Name)
	if err != nil {
		err = fmt.Errorf("init db fail, %w", err)
		return
	}

	svc.Actor = nactor.New(
		nactor.WithLogger(logger),
		nactor.WithStartedWaitGroup(startedWg),
		nactor.WithStoppedWaitGroup(stoppedWg),
		nactor.WithOnReceiveMessage(svc.onReceiveMessage),
		nactor.WithOnStarted(svc.onStarted),
		nactor.WithOnStopping(svc.onStopping),
		nactor.WithOnStopped(svc.onStopped),
		nactor.WithOnActorTerminate(svc.onActorTerminated),
		nactor.WithRPC(rpc.Protocol, nil, nil),
	)

	return
}

func (svc *Service) Run(ctx actor.Context, pprofAddress string, args ...interface{}) (err error) {
	err = svc.Actor.Start(ctx, fmt.Sprintf("%s-%d", ServiceActorName, svc.cfg.GetID()))
	if err != nil {
		return
	}

	svc.instance.PID = svc.PID()
	svc.instance.PprofAddress = pprofAddress

	err = cluster.TryLockPosition(ServiceName, svc.cfg.GetID(), svc.instance)
	if err != nil {
		err = fmt.Errorf("try lock position fail, %w", err)
		return
	}

	err = svc.regAllRPCHandler()
	if err != nil {
		return
	}

	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (svc *Service) onStarted(ctx actor.Context) {
	serviceInfoNum, err := cluster.ServiceInfoNum(cluster.ZonesServiceName)
	if err != nil {
		svc.Error("get zones service info num fail, %s", err)
		_ = svc.Stop()
		return
	}
	if serviceInfoNum == 0 {
		svc.Error("zones service info num is 0")
		_ = svc.Stop()
		return
	}

	dbSession := svc.dbc.GetSession()
	defer svc.dbc.PutSession(dbSession)

	datas, err := svc.dbc.FindSomeZone(dbSession, nil)
	if err != nil {
		svc.Error("load zone data fail, %s", err)
		return
	}

	for _, data := range datas {
		dispatchID := int(data.ID)%serviceInfoNum + 1
		if dispatchID != svc.cfg.GetID() {
			continue
		}

		iZone := zone.New(logger, data, svc.dbc, *svc.globalCfg, *svc.cfg, &svc.zoneStartedWg, &svc.zoneStoppedWg)
		name := fmt.Sprintf("zone-%d", data.ID)
		err := iZone.Start(ctx, name)
		if err != nil {
			svc.Error("start %s fail, %s", name, err)
			_ = svc.Stop()
			return
		}
		iZone.WaitForStarted()

		svc.zonePIDs[data.ID] = iZone.PID()
		svc.Info("start %s success", name)

		// 每启动一个zone，sleep一定时间，确保错开zone保存到数据库的操作，避免数据库拥堵
		time.Sleep(svc.globalCfg.StartIntervalMilliSec)
	}
}

func (svc *Service) onStopping(ctx actor.Context) {

}

func (svc *Service) onStopped(ctx actor.Context) {

}

func (svc *Service) onReceiveMessage(ctx actor.Context) {
	defer func() {
		if reason := recover(); reason != nil {
			svc.Error("%v\n%s", reason, debug.Stack())
			panic(reason)
		}
	}()

	sender := ctx.Sender()
	switch msg := ctx.Message().(type) {
	default:
		svc.Error("recv unsupported msg [%#v] from [%v]", msg, sender)
	}
}

func (svc *Service) onActorTerminated(who *actor.PID, ctx actor.Context) {
	svc.Debug("[%s] terminated", who.Id)

	var appID int32
	var zonesID int32
	var zoneID int32
	if _, err := fmt.Sscanf(who.Id, "app-%d/zones-%d/zone-%d", &appID, &zonesID, &zoneID); err == nil {
		delete(svc.zonePIDs, zoneID)
		svc.Info("delete zone pid from map, zoneID=%d", zoneID)
	}
}
