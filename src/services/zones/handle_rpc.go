package zones

import (
	"fmt"

	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	nrpc "nggs/rpc"

	"model"
	"rpc"
	"server/src/entities/zone"
)

func (svc *Service) regAllRPCHandler() (err error) {
	err = svc.RegisterRPCHandler(rpc.C2S_CreateZone_MessageID(), svc.handleCreateZone)
	if err != nil {
		return
	}
	err = svc.RegisterRPCHandler(rpc.C2S_GetZonePID_MessageID(), svc.handleGetZonePID)
	if err != nil {
		return
	}
	err = svc.RegisterRPCHandler(rpc.C2S_DayRefresh_MessageID(), nactor.ForwardToChildren)
	if err != nil {
		return
	}
	err = svc.RegisterRPCHandler(rpc.C2S_DayZeroClock_MessageID(), nactor.ForwardToChildren)
	if err != nil {
		return
	}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (svc *Service) handleCreateZone(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_CreateZone)
	send := iSend.(*rpc.S2C_CreateZone)
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
	}()

	dbSession := svc.dbc.GetSession()
	defer svc.dbc.PutSession(dbSession)

	data := model.Get_Zone()
	data.ID = recv.ID
	data.OpenTime = recv.OpenTime
	data.Name = recv.Name

	if err := data.Insert(dbSession, svc.dbc.DBName()); err != nil {
		svc.Error("handleCreateZone insert %s fail, %s", data.String(), err)
		send.EC = rpc.EC_Fail
		return
	}

	iZone := zone.New(logger, data, svc.dbc, *svc.globalCfg, *svc.cfg, &svc.zoneStartedWg, &svc.zoneStoppedWg)
	name := fmt.Sprintf("zone-%d", data.ID)
	err := iZone.Start(ctx, name)
	if err != nil {
		svc.Error("handleCreateZone start %s fail, %s", name, err)
		send.EC = rpc.EC_Fail
		return
	}
	iZone.WaitForStarted()

	zonePID := iZone.PID()

	svc.zonePIDs[data.ID] = zonePID

	send.Data = data
	send.PID = rpc.FromRealPID(zonePID)

	svc.Info("handleCreateZone start %s success", name)
}

func (svc *Service) handleGetZonePID(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_GetZonePID)
	send := iSend.(*rpc.S2C_GetZonePID)
	send.ID = recv.ID
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
	}()

	pid, ok := svc.zonePIDs[recv.ID]
	if !ok {
		svc.Error("handleGetZonePID fail, zone id=%d", recv.ID)
		send.EC = rpc.EC_Fail
		return
	}
	if nactor.IsPIDPtrEmpty(pid) {
		svc.Error("handleGetZonePID fail, pid is empty, zone id=%d", recv.ID)
		send.EC = rpc.EC_Fail
		return
	}

	send.PID = rpc.FromRealPID(pid)

	svc.Debug("handleGetZonePID success, zone id=%d, zone pid=%s", recv.ID, pid.String())
}
