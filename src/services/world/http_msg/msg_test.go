package http_msg

import "testing"

func TestParseIntRequestParam(t *testing.T) {
	reqParams := map[string]string{
		"i": "123",
	}
	_, ec := parseIntRequestParam(reqParams, "i", true)
	if ec != EC_OK {
		t.Errorf("i must not empty")
		return
	}
	_, ec = parseIntRequestParam(reqParams, "i2", false)
	if ec != EC_OK {
		t.Errorf("i2 can be empty")
		return
	}
}

func TestParseStringRequestParam(t *testing.T) {
	reqParams := map[string]string{
		"s": "test",
	}
	_, ec := parseStringRequestParam(reqParams, "s", true)
	if ec != EC_OK {
		t.Errorf("s must not empty")
		return
	}
	_, ec = parseStringRequestParam(reqParams, "s2", false)
	if ec != EC_OK {
		t.Errorf("s2 can be empty")
		return
	}
}

func TestParseString2StringMapRequestParam(t *testing.T) {
	reqParams := map[string]string{
		"kv": "1:1;2:;3:3;test:测试",
	}
	reqParam, ec := parseString2StringMapRequestParam(reqParams, "kv", true)
	if ec != EC_OK {
		t.Errorf("parse reqParams %#v fail, %v", reqParams, ec)
		return
	}
	t.Logf("%#v", reqParam)
}

func TestParseRewardRequestParam(t *testing.T) {
	reqParams := map[string]string{
		"reward": "1:1:1;2:2:2;3:3:3",
	}
	reqParam, ec := parseRewardRequestParam(reqParams, "reward", true)
	if ec != EC_OK {
		t.Errorf("parse reqParams %#v fail, %v", reqParams, ec)
		return
	}
	t.Logf("%s", reqParam.String())
}
