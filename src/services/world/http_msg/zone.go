package http_msg

import "net/http"

type RequestCreateZone struct {
	URLRequest
	ID       int32  `json:"id"`
	OpenTime int64  `json:"open_time"`
	Name     string `json:"name"`
}

func NewRequestCreateZone() RequestCreateZone {
	return RequestCreateZone{
		URLRequest: NewURLRequest(),
	}
}

func (req *RequestCreateZone) UnmarshalFromHttpRequest(r *http.Request, validSign string) (reqParams map[string]string, ec ErrorCode) {
	reqParams, ec = req.URLRequest.UnmarshalFromHttpRequest(r, validSign)
	if ec != EC_OK {
		return
	}

	zoneID, ec := parseIntRequestParam(reqParams, "id", true)
	if ec != EC_OK {
		ec = EC_InvalidZoneID
		return
	}
	req.ID = int32(zoneID)

	openTime, ec := parseIntRequestParam(reqParams, "open_time", true)
	if ec != EC_OK {
		ec = EC_InvalidParam
		return
	}
	req.OpenTime = int64(openTime)

	zoneName, ec := parseStringRequestParam(reqParams, "name", true)
	if ec != EC_OK {
		ec = EC_InvalidParam
		return
	}
	req.Name = zoneName

	return
}
