package http_msg

import (
	"encoding/json"
	"net/http"

	"model"
	//"sd"
)

type RequestMail struct {
	URLRequest
	//Type    sd.E_MailType     `json:"type"`
	Title   string            `json:"title"`
	Content string            `json:"content"`
	Params  map[string]string `json:"params"`
	Reward  *model.Reward     `json:"reward"`
}

func NewRequestMail() RequestMail {
	return RequestMail{
		URLRequest: NewURLRequest(),
		Params:     map[string]string{},
	}
}

func (req *RequestMail) UnmarshalFromHttpRequest(r *http.Request, validSign string) (reqParams map[string]string, ec ErrorCode) {
	reqParams, ec = req.URLRequest.UnmarshalFromHttpRequest(r, validSign)
	if ec != EC_OK {
		return
	}

	//tYpE, ec := parseIntRequestParam(reqParams, "type", true)
	//if ec != EC_OK {
	//	return
	//}
	//if !sd.Check_E_MailType_I(tYpE) {
	//	ec = EC_InvalidMailType
	//	return
	//}
	//req.Type = sd.E_MailType(tYpE)

	title, ec := parseStringRequestParam(reqParams, "title", false)
	if ec != EC_OK {
		return
	}
	req.Title = title

	content, ec := parseStringRequestParam(reqParams, "content", false)
	if ec != EC_OK {
		return
	}
	req.Content = content

	params, ec := parseString2StringMapRequestParam(reqParams, "params", false)
	if ec != EC_OK {
		return
	}
	req.Params = params

	reward, ec := parseRewardRequestParam(reqParams, "reward", false)
	if ec != EC_OK {
		return
	}
	req.Reward = reward

	return
}

func (req RequestMail) String() string {
	ba, _ := json.Marshal(req)
	return string(ba)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type RequestPersonalMail struct {
	RequestMail
	RoleIDs []int64 `json:"role_ids"`
}

func NewRequestPersonMail() RequestPersonalMail {
	return RequestPersonalMail{
		RequestMail: NewRequestMail(),
	}
}

func (req *RequestPersonalMail) UnmarshalFromHttpRequest(r *http.Request, validSign string) (reqParams map[string]string, ec ErrorCode) {
	reqParams, ec = req.RequestMail.UnmarshalFromHttpRequest(r, validSign)
	if ec != EC_OK {
		return
	}

	roleIDs, ec := parseIntSliceRequestParam(reqParams, "role_ids", true)
	if ec != EC_OK {
		ec = EC_InvalidRoleID
		return
	}
	if len(roleIDs) == 0 {
		ec = EC_InvalidRoleID
		return
	}
	for _, roleID := range roleIDs {
		req.RoleIDs = append(req.RoleIDs, int64(roleID))
	}

	return
}

func (req RequestPersonalMail) String() string {
	ba, _ := json.Marshal(req)
	return string(ba)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type RequestWorldMail struct {
	RequestMail
	ZoneIDS []int32 `json:"zone_ids"`
}

func NewRequestWorldMail() RequestWorldMail {
	return RequestWorldMail{
		RequestMail: NewRequestMail(),
		ZoneIDS:     []int32{},
	}
}

func (req *RequestWorldMail) UnmarshalFromHttpRequest(r *http.Request, validSign string) (reqParams map[string]string, ec ErrorCode) {
	reqParams, ec = req.RequestMail.UnmarshalFromHttpRequest(r, validSign)
	if ec != EC_OK {
		return
	}
	zoneIDs, ec := parseIntSliceRequestParam(reqParams, "zone_ids", false)
	if ec != EC_OK {
		ec = EC_InvalidZoneID
		return
	}
	if len(zoneIDs) > 0 {
		for _, value := range zoneIDs {
			req.ZoneIDS = append(req.ZoneIDS, int32(value))
		}
	}

	return
}

func (req RequestWorldMail) String() string {
	ba, _ := json.Marshal(req)
	return string(ba)
}
