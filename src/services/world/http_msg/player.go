package http_msg

import (
	"encoding/json"
	"net/http"
)

type RequestGM struct {
	URLRequest
	RoleID  int64  `json:"role_id"`
	Command string `json:"command"`
}

func NewRequestGM() RequestGM {
	return RequestGM{
		URLRequest: NewURLRequest(),
	}
}

func (req *RequestGM) UnmarshalFromHttpRequest(r *http.Request, validSign string) (reqParams map[string]string, ec ErrorCode) {
	reqParams, ec = req.URLRequest.UnmarshalFromHttpRequest(r, validSign)
	if ec != EC_OK {
		return
	}

	roleID, ec := parseIntRequestParam(reqParams, "role_id", true)
	if ec != EC_OK {
		return
	}

	command, ec := parseStringRequestParam(reqParams, "command", true)
	if ec != EC_OK {
		return
	}

	req.RoleID = int64(roleID)
	req.Command = command

	return
}

func (req RequestGM) String() string {
	ba, _ := json.Marshal(req)
	return string(ba)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type ResponseGM struct {
	Response
}

func (resp ResponseGM) String() string {
	ba, _ := json.Marshal(resp)
	return string(ba)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//type RoleInfo struct {
//	ID         int64             `json:"id"`
//	AccountID  int64             `json:"account_id"`
//	ServerID   int32             `json:"server_id"`
//	Name       string            `json:"name"`
//	Level      int32             `json:"level"`
//	VipLevel   int32             `json:"vip_level"`
//	CreateTime int64             `json:"create_time"`
//	Channel    string            `json:"channel"`
//	OS         string            `json:"os"`
//	Currency   []*model.Currency `json:"currency"`
//	Online     int32             `json:"online"`
//	Ban        int32             `json:"ban"`
//}
//
//func NewRoleInfo() RoleInfo {
//	return RoleInfo{
//		Currency: []*model.Currency{},
//	}
//}
//
//type RequestRoleIds struct {
//	URLRequest
//	RoleIDs []int64 `json:"role_ids"`
//}
//
//func NewRequestRoleIds() RequestRoleIds {
//	return RequestRoleIds{
//		URLRequest: NewURLRequest(),
//	}
//}
//
//func (req *RequestRoleIds) UnmarshalFromHttpRequest(r *http.Request, validSign string) (reqParams map[string]string, ec ErrorCode) {
//	reqParams, ec = req.URLRequest.UnmarshalFromHttpRequest(r, validSign)
//	if ec != EC_OK {
//		return
//	}
//
//	roleIDs, ec := parseIntSliceRequestParam(reqParams, "role_ids", true)
//	if ec != EC_OK {
//		ec = EC_InvalidRoleID
//		return
//	}
//	if len(roleIDs) == 0 {
//		ec = EC_InvalidRoleID
//		return
//	}
//	for _, roleID := range roleIDs {
//		req.RoleIDs = append(req.RoleIDs, int64(roleID))
//	}
//
//	return
//}
//
//type ResponseRoleList struct {
//	Response
//	RoleList map[int64]*RoleInfo `json:"role_list"`
//}
//
//func NewResponseRoleList() ResponseRoleList {
//	return ResponseRoleList{
//		Response:   NewResponse(),
//		RoleList: map[int64]*RoleInfo{},
//	}
//}
//
//func (r ResponseRoleList) String() string {
//	s, _ := json.MarshalToString(r)
//	return s
//}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type RequestAccountBan struct {
	URLRequest
	AccountID int64 `json:"account_id"`
	// 0-解禁 1-封禁
	Ban int32 `json:"ban"`
}

func NewRequestAccountBan() RequestAccountBan {
	return RequestAccountBan{
		URLRequest: NewURLRequest(),
	}
}

func (req *RequestAccountBan) UnmarshalFromHttpRequest(r *http.Request, validSign string) (reqParams map[string]string, ec ErrorCode) {
	reqParams, ec = req.URLRequest.UnmarshalFromHttpRequest(r, validSign)
	if ec != EC_OK {
		return
	}

	accountID, ec := parseIntRequestParam(reqParams, "account_id", true)
	if ec != EC_OK {
		ec = EC_InvalidAccountID
		return
	}
	req.AccountID = int64(accountID)

	ban, ec := parseIntRequestParam(reqParams, "ban", true)
	if ec != EC_OK {
		ec = EC_InvalidParam
		return
	}
	req.Ban = int32(ban)

	return
}

type ResponseAccountBan struct {
	Response
	Ban int32 `json:"ban"`
}

func NewResponseAccountBan() ResponseAccountBan {
	return ResponseAccountBan{
		Response: NewResponse(),
	}
}

func (r ResponseAccountBan) String() string {
	ba, _ := json.Marshal(r)
	return string(ba)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type RequestAccountPassword struct {
	URLRequest
	AccountID int64 `json:"account_id"`
}

func NewRequestAccountPassword() RequestAccountPassword {
	return RequestAccountPassword{
		URLRequest: NewURLRequest(),
	}
}

func (req *RequestAccountPassword) UnmarshalFromHttpRequest(r *http.Request, validSign string) (reqParams map[string]string, ec ErrorCode) {
	reqParams, ec = req.URLRequest.UnmarshalFromHttpRequest(r, validSign)
	if ec != EC_OK {
		return
	}

	accountID, ec := parseIntRequestParam(reqParams, "account_id", true)
	if ec != EC_OK {
		ec = EC_InvalidAccountID
		return
	}
	req.AccountID = int64(accountID)

	return
}

type ResponseAccountPassword struct {
	Response
	Password    string `json:"password"`
	AccountName string `json:"account_name"`
}

func NewResponseAccountPassword() ResponseAccountPassword {
	return ResponseAccountPassword{
		Response: NewResponse(),
	}
}

func (r ResponseAccountPassword) String() string {
	ba, _ := json.Marshal(r)
	return string(ba)
}
