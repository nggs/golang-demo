package http_msg

type ErrorCode int32

const (
	EC_OK ErrorCode = iota
	EC_Fail
	EC_ParseFormFail
	EC_InvalidSign
	EC_InvalidParam
	EC_RoleNotExist
	EC_InvalidMailType
	EC_InvalidRoleID
	EC_InvalidReward
	EC_InvalidZoneID
	EC_InvalidAccountID
	EC_AccountAlreadyBan
	EC_AccountAlreadyNotBan
	EC_InvalidArenaID
	EC_InvalidArenaOpenTime
	EC_AlreadyOpenZone
	EC_AlreadyOpenChat
	EC_AlreadyOpenArena
)

var errorMessages = map[ErrorCode]string{
	EC_OK:                   "success",
	EC_Fail:                 "fail",
	EC_ParseFormFail:        "parse form fail",
	EC_InvalidSign:          "invalid sign",
	EC_InvalidParam:         "invalid param",
	EC_RoleNotExist:         "role not exist",
	EC_InvalidMailType:      "invalid mail type",
	EC_InvalidRoleID:        "invalid role id",
	EC_InvalidReward:        "invalid reward",
	EC_InvalidZoneID:        "invalid zone id",
	EC_InvalidAccountID:     "invalid account id",
	EC_AccountAlreadyBan:    "account already ban",
	EC_AccountAlreadyNotBan: "account not ban",
	EC_InvalidArenaID:       "invalid arena id",
	EC_InvalidArenaOpenTime: "invalid arena open time",
	EC_AlreadyOpenZone:      "already open zone",
	EC_AlreadyOpenChat:      "already open chat",
	EC_AlreadyOpenArena:     "already open arena",
}

func (ec ErrorCode) String() string {
	return errorMessages[ec]
}
