package http_msg

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	nhttp "nggs/network/http"

	"model"
	//"sd"
)

const (
	reqParamSignName = "sign"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type URLRequest struct {
	Sign string `json:"sign"`
}

func NewURLRequest() URLRequest {
	return URLRequest{}
}

func (req *URLRequest) UnmarshalFromHttpRequest(r *http.Request, validSign string) (reqParams map[string]string, ec ErrorCode) {
	err := r.ParseForm()
	if err != nil {
		ec = EC_Fail
		return
	}

	reqParams = nhttp.CollectRequestParams(r)

	sign, ok := reqParams[reqParamSignName]
	if !ok || sign != validSign {
		ec = EC_InvalidSign
		return
	}

	req.Sign = sign

	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func parseIntRequestParam(requestParams map[string]string, requestParamName string, must bool) (requestParam int, ec ErrorCode) {
	s, ok := requestParams[requestParamName]
	if !ok || s == "" {
		if must {
			ec = EC_InvalidParam
			return
		}
	} else {
		var err error
		requestParam, err = strconv.Atoi(s)
		if err != nil {
			ec = EC_InvalidParam
			return
		}
	}
	return
}

func parseIntSliceRequestParam(requestParams map[string]string, requestParamName string, must bool) (requestParam []int, ec ErrorCode) {
	s, ok := requestParams[requestParamName]
	if !ok || s == "" {
		if must {
			ec = EC_InvalidParam
			return
		}
	} else {
		var words = strings.Split(s, "|")
		for _, word := range words {
			i, err := strconv.Atoi(word)
			if err != nil {
				ec = EC_InvalidRoleID
				return
			}
			requestParam = append(requestParam, i)
		}
	}
	return
}

func parseStringRequestParam(requestParams map[string]string, requestParamName string, must bool) (requestParam string, ec ErrorCode) {
	var ok bool
	requestParam, ok = requestParams[requestParamName]
	if !ok || requestParam == "" {
		if must {
			ec = EC_InvalidParam
			return
		}
	}
	return
}

func parseString2StringMapRequestParam(requestParams map[string]string, requestParamName string, must bool) (requestParam map[string]string, ec ErrorCode) {
	requestParam = map[string]string{}
	s, ok := requestParams[requestParamName]
	if !ok || s == "" {
		if must {
			ec = EC_InvalidParam
			return
		}
	} else {
		kvs := strings.Split(s, "|")
		for _, s := range kvs {
			kv := strings.Split(s, ":")
			k := kv[0]
			if len(kv) > 1 {
				requestParam[k] = kv[1]
			} else {
				requestParam[k] = ""
			}
		}
	}
	return
}

func parseRewardRequestParam(requestParams map[string]string, requestParamName string, must bool) (requestParam *model.Reward, ec ErrorCode) {
	requestParam = model.Get_Reward()
	s, ok := requestParams[requestParamName]
	if !ok || s == "" {
		if must {
			ec = EC_InvalidParam
			return
		}
	} else {
		sThings := strings.Split(s, "|")
		for _, sThing := range sThings {
			sections := strings.Split(sThing, ":")
			if len(sections) < 3 {
				ec = EC_InvalidParam
				return
			}
			//tYpE, err := strconv.Atoi(sections[0])
			//if err != nil {
			//	ec = EC_InvalidParam
			//	return
			//}
			//tid, err := strconv.Atoi(sections[1])
			//if err != nil {
			//	ec = EC_InvalidParam
			//	return
			//}
			//num, err := strconv.Atoi(sections[2])
			//if err != nil {
			//	ec = EC_InvalidParam
			//	return
			//}
			//requestParam.AppendOne(sd.E_ItemBigType(tYpE), tid, num)
		}
	}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type Response struct {
	Sign         string    `json:"sign"`
	ErrorCode    ErrorCode `json:"error_code"`
	ErrorMessage string    `json:"error_msg"`
	Data         string    `json:"data"`
}

func NewResponse() Response {
	return Response{}
}

func (resp *Response) Marshal() string {
	if resp.ErrorMessage == "" {
		resp.ErrorMessage = resp.ErrorCode.String()
	}
	ba, _ := json.Marshal(resp)
	return string(ba)
}
