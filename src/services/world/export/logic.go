package export

type ILogic interface {
	Init() error
	Run()
	Stop()
}

type ICommon interface {
	ICommon()
}

type IMail interface {
	IMail()
}

type IRole interface {
	IRole()
}

type IZone interface {
	IZone()
}
