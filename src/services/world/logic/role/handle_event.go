package role

import (
	"fmt"
	"strconv"

	"github.com/asynkron/protoactor-go/actor"

	"model"

	"cluster"

	"nggs/event"

	"server/src/services/world/cache"
	Event "server/src/services/world/event"
)

func (l *Logic) regAllEventHandler() (err error) {
	l.RegisterEventHandler(Event.FiveMinutePassedID, l.handleFiveMinutePassed)
	l.RegisterEventHandler(Event.GameInstanceDeleteID, l.handleGameInstanceDelete)
	return
}

func (l *Logic) handleFiveMinutePassed(iEvent event.IEvent, ctx actor.Context, args ...interface{}) {
	worldCache := l.Cache()
	if worldCache == nil {
		l.Error("handleFiveMinutePassed world cache is nil")
		return
	}

	onlineData := make(map[string]int)
	worldCache.OnlineRoleContainer().Each(func(onlineRoleID int64, onlineRole *cache.OnlineRole) (continued bool) {
		k := fmt.Sprintf("%s|%s|%s", strconv.Itoa(int(onlineRole.ServerID)), onlineRole.OS(), onlineRole.Channel())
		if _, ok := onlineData[k]; ok {
			onlineData[k] += 1
		} else {
			onlineData[k] = 1
		}
		return true
	})

	//stat.Record(stat.E_Type_Online, onlineData)

	l.Debug("handleFiveMinutePassed, onlineData=%+v", onlineData)
}

func (l *Logic) handleGameInstanceDelete(iEvent event.IEvent, ctx actor.Context, args ...interface{}) {
	worldCache := l.Cache()
	if worldCache == nil {
		l.Error("handleGameInstanceDelete world cache is nil")
		return
	}

	gameServiceNum, err := cluster.ServiceInfoNum(cluster.GameServiceName)
	if err != nil {
		l.Error("handleGameInstanceDelete get game service info num fail, %s", err)
		return
	}
	if gameServiceNum == 0 {
		l.Error("handleGameInstanceDelete game service info num is 0")
		return
	}

	ev := iEvent.(*Event.GameInstanceDelete)

	l.Info("handleGameInstanceDelete %+v", ev)

	worldCache.OnlineRoleContainer().RemoveSome(func(onlineRoleID int64, onlineRole *cache.OnlineRole) (continued bool) {
		accountID, _ := model.RoleIDToAccountIDServerID(onlineRoleID)
		dispatchID := int(accountID)%int(gameServiceNum) + 1
		if dispatchID == ev.ID {
			l.Info("handleGameInstanceDelete remove online role, id=%d", onlineRoleID)
			return true
		}
		return false
	})
}
