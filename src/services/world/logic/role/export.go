package role

//func (l *Logic) GetRolePID(roleID int64) (ec rpc.EC, rolePID *actor.PID) {
//	if roleID == 0 {
//		l.Error("GetRolePID role roleID is 0")
//		ec = rpc.EC_InvalidParam
//		return
//	}
//
//	worldCache := l.Cache()
//	if worldCache == nil {
//		l.Error("GetRolePID world cache is nil")
//		ec = rpc.EC_Fail
//		return
//	}
//
//	roleContainer := worldCache.RoleContainer()
//	role, ok := roleContainer.Get(roleID)
//	if ok {
//		rolePID = role.PID()
//		l.Debug("GetRolePID get role[%d] pid[%v] success", roleID, rolePID)
//		return
//	}
//
//	// 缓存里未找到，从数据库查询
//	dbc := l.DBC()
//	dbSession := dbc.GetSession()
//	defer dbc.PutSession(dbSession)
//
//	_, err := dbc.FindOne_RoleMinimal(dbSession, bson.M{"_id": roleID})
//	if err != nil {
//		// 未找到角色
//		l.Error("GetRolePID role not exist, id=[%d]", roleID)
//		ec = rpc.EC_Fail
//		return
//	}
//
//	accountID, serverID := model.RoleIDToAccountIDServerID(roleID)
//
//	zoneInfo, ok := worldCache.ZoneInfos[serverID]
//	if !ok || zoneInfo == nil {
//		l.Error("GetRolePID zone info not found, zone id=%d", serverID)
//		ec = rpc.EC_Fail
//		return
//	}
//
//	rolePID, err = rpc.SpawnRole(accountID, serverID, nil, 0, zoneInfo.ZoneMinimal)
//	if err != nil {
//		l.Error("GetRolePID spawn role fail, %s", err)
//		ec = rpc.EC_SpawnRoleFail
//		return
//	}
//
//	worldCache.SetRole(roleID, rolePID, false)
//
//	l.Debug("GetRolePID register role[%d] success, pid=%v", roleID, rolePID)
//
//	return
//}
//
//func (l *Logic) GetSpawnedRolePID(roleID int64) (ec rpc.EC, rolePID *actor.PID) {
//	if roleID <= 0 {
//		l.Error("GetSpawnedRolePID role roleID <= 0")
//		ec = rpc.EC_InvalidParam
//		return
//	}
//
//	worldCache := l.Cache()
//	if worldCache == nil {
//		l.Error("GetSpawnedRolePID world cache is nil")
//		ec = rpc.EC_Fail
//		return
//	}
//
//	role, ok := worldCache.RoleContainer().Get(roleID)
//	if !ok {
//		l.Debug("GetSpawnedRolePID role[%d] not spawned", roleID)
//		ec = rpc.EC_RoleNotSpawned
//		return
//	}
//
//	rolePID = role.PID()
//
//	l.Debug("GetSpawnedRolePID get spawned role[%d] pid[%v] success", roleID, rolePID)
//
//	return
//}
