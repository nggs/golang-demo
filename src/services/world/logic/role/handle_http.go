package role

import (
	"model"
	"net/http"
	"rpc"

	"github.com/gin-gonic/gin"
	"github.com/globalsign/mgo/bson"

	nactor "nggs/actor"

	"server/src/services/world/cache"
	"server/src/services/world/http_msg"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) regAllHttpHandler() (err error) {
	//err = l.RegisterHttpRequestHandler("/role_list", l.handleRoleList, nil)
	//if err != nil {
	//	return
	//}
	err = l.RegisterHttpRequestHandler("GET", "/account_ban", l.handleAccountBan, nil)
	if err != nil {
		return
	}
	err = l.RegisterHttpRequestHandler("GET", "/account_password", l.handleAccountPassword, nil)
	if err != nil {
		return
	}
	err = l.RegisterHttpRequestHandler("GET", "/gm", l.handleGM, nil)
	if err != nil {
		return
	}
	//err = l.RegisterHttpRequestHandler("/set_monitor_val", l.handleSetMonitorVal, nil)
	//if err != nil {
	//	return
	//}
	//err = l.RegisterHttpRequestHandler("/set_arena_point", l.handleSetArenaPoint, nil)
	//if err != nil {
	//	return
	//}
	//err = l.RegisterHttpRequestHandler("/admin_trigger_limited_gift_pack", l.handleAdminTriggerLimitedGiftPack, nil)
	//if err != nil {
	//	return
	//}
	//err = l.RegisterHttpRequestHandler("/get_customize_limit_gift_pack", l.handleGetCustomizeLimitGiftPack, nil) //获取角色身上的定制限时礼包列表
	//if err != nil {
	//	return
	//}
	//err = l.RegisterHttpRequestHandler("/set_role_name", l.handleSetRoleName, nil)
	//if err != nil {
	//	return
	//}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//func (l *Logic) handleRoleList(w http.ResponseWriter, r *http.Request) {
//	//w.Header().Set("Access-Control-Allow-Origin", "*")
//
//	resp := http_msg.NewResponseRoleList()
//	defer func() {
//		http.Error(w, resp.String(), http.StatusOK)
//	}()
//
//	req := http_msg.NewRequestRoleIds()
//
//	reqParams, reqEC := req.UnmarshalFromHttpRequest(r, l.Config().HttpSign)
//	if reqEC != http_msg.EC_OK {
//		resp.ErrorCode = reqEC
//		l.Error("handleRoleList marshal request fail, request params=%+v, %v", reqParams, resp.ErrorCode)
//		return
//	}
//
//	dbc := l.DBC()
//	dbSession := dbc.GetSession()
//	defer dbc.PutSession(dbSession)
//
//	var ids []int64
//	var accIds []int64
//	for _, roleID := range req.RoleIDs {
//		ids = append(ids, roleID)
//	}
//
//	query := bson.M{"_id": bson.M{"$in": ids}}
//	roleDatas, err := dbc.FindSomeRole(dbSession, query)
//	if err != nil {
//		l.Error("handleRoleList query role fail, ids=[%v], %s", ids, err)
//		resp.ErrorCode = http_msg.EC_Fail
//		return
//	}
//	for _, value := range roleDatas {
//		accIds = append(accIds, value.AccountID)
//	}
//
//	query = bson.M{"_id": bson.M{"$in": accIds}}
//	accountDatas, err := dbc.FindSome_Account(dbSession, query)
//	if err != nil {
//		l.Error("handleRoleList query role fail, ids=[%v], %s", ids, err)
//		resp.ErrorCode = http_msg.EC_Fail
//		return
//	}
//
//	onlineRoleContainer := l.Cache().OnlineRoleContainer()
//
//	roleList := make(map[int64]*http_msg.RoleInfo, len(roleDatas))
//	for _, value := range roleDatas {
//		var info *http_msg.RoleInfo
//		var c []*model.Currency
//		for _, cVal := range value.Currencies {
//			c = append(c, cVal)
//		}
//		var channel string
//		var os string
//		var ban int32
//		for _, accVal := range accountDatas {
//			if accVal.ID == value.AccountID {
//				channel = accVal.Channel
//				os = accVal.OS
//				ban = accVal.Ban
//				break
//			}
//		}
//
//		// 是否在线
//		var online int32
//		_, ok := onlineRoleContainer.Get(value.ID)
//		if ok {
//			online = 1
//		}
//
//		info = &http_msg.RoleInfo{
//			ID:         value.ID,
//			AccountID:  value.AccountID,
//			ServerID:   value.ServerID,
//			Name:       value.Name,
//			Level:      value.Level,
//			VipLevel:   value.VipLevel,
//			CreateTime: value.CreateTime,
//			Channel:    channel,
//			OS:         os,
//			Currency:   c,
//			Online:     online,
//			Ban:        ban,
//		}
//		roleList[value.ID] = info
//	}
//
//	resp.RoleList = roleList
//}

func (l *Logic) handleAccountBan(ctx *gin.Context) {
	//w.Header().Set("Access-Control-Allow-Origin", "*")

	resp := http_msg.NewResponseAccountBan()
	defer func() {
		ctx.String(http.StatusOK, resp.String())
	}()

	req := http_msg.NewRequestAccountBan()
	reqParams, reqEC := req.UnmarshalFromHttpRequest(ctx.Request, l.Config().HttpSign)
	if reqEC != http_msg.EC_OK {
		resp.ErrorCode = reqEC
		l.Error("handleAccountBan marshal request fail, request params=%+v, %v", reqParams, resp.ErrorCode)
		return
	}

	ec, err := rpc.UpdateAccountBan(req.AccountID, req.Ban)
	if err != nil {
		resp.ErrorCode = http_msg.EC_Fail
		l.Error("handleAccountBan rpc.UpdateAccountBan fail, request params=%+v, %s", reqParams, err)
		return
	}

	switch ec {
	case rpc.EC_OK:
		resp.ErrorCode = http_msg.EC_OK
	case rpc.EC_Fail:
	case rpc.EC_DataNotFound:
		resp.ErrorCode = http_msg.EC_InvalidAccountID
	case rpc.EC_DataNoChange:
		if req.Ban != 0 {
			resp.ErrorCode = http_msg.EC_AccountAlreadyBan
		} else {
			resp.ErrorCode = http_msg.EC_AccountAlreadyNotBan
		}
	default:
		resp.ErrorCode = http_msg.EC_Fail
	}
	if resp.ErrorCode != http_msg.EC_OK {
		l.Error("handleAccountBan rpc.UpdateAccountBan fail, request params=%+v, ec=%v", reqParams, resp.ErrorCode)
		return
	}

	resp.Ban = req.Ban

	l.Cache().OnlineRoleContainer().Each(func(roleID int64, onlineRole *cache.OnlineRole) (continued bool) {
		accountID, _ := model.RoleIDToAccountIDServerID(roleID)
		if accountID != req.AccountID {
			return true
		}
		// 账号在线, 通知账号因为封号下线
		notify := rpc.Get_C2S_UpdateAccountBan()
		notify.Ban = req.Ban
		nactor.RootContext().Send(onlineRole.PID(), notify)
		return false
	})
}

func (l *Logic) handleAccountPassword(ctx *gin.Context) {
	//w.Header().Set("Access-Control-Allow-Origin", "*")

	resp := http_msg.NewResponseAccountPassword()
	defer func() {
		ctx.String(http.StatusOK, resp.String())
	}()

	req := http_msg.NewRequestAccountPassword()

	reqParams, reqEC := req.UnmarshalFromHttpRequest(ctx.Request, l.Config().HttpSign)
	if reqEC != http_msg.EC_OK {
		resp.ErrorCode = reqEC
		l.Error("handleAccountPassword marshal request fail, request params=%+v, %v", reqParams, resp.ErrorCode)
		return
	}

	dbc := l.DBC()
	dbSession := dbc.GetSession()
	defer dbc.PutSession(dbSession)

	query := bson.M{"_id": req.AccountID}
	accountInfo, err := dbc.FindOneAccount(dbSession, query)
	if err != nil {
		l.Error("handleAccountPassword query account=[%d] data fail", req.AccountID, err)
		resp.ErrorCode = http_msg.EC_InvalidAccountID
		return
	}

	resp.Password = accountInfo.Password
	resp.AccountName = accountInfo.Name
}

func (l *Logic) handleGM(ctx *gin.Context) {
	//w.Header().Set("Access-Control-Allow-Origin", "*")

	var resp http_msg.ResponseGM
	defer func() {
		ctx.String(http.StatusOK, resp.Marshal())
	}()

	req := http_msg.NewRequestGM()
	reqParams, reqEC := req.UnmarshalFromHttpRequest(ctx.Request, l.Config().HttpSign)
	if resp.ErrorCode != http_msg.EC_OK {
		resp.ErrorCode = reqEC
		l.Error("handleGM marshal request fail, request params=%+v, %v", reqParams, resp.ErrorCode)
		return
	}

	l.Debug("handleGM request params=%+v", reqParams)

	resp.Sign = req.Sign

	rolePID, e := rpc.GetRolePID(req.RoleID)
	if e != nil {
		l.Error("handleGM get role[%d] pid fail, %s", req.RoleID, e)
		resp.ErrorCode = http_msg.EC_RoleNotExist
		return
	}

	err := rpc.ExecuteGM(rolePID, req.Command)
	if err != nil {
		l.Error("handleGM execute gm fail, req=%s, %s", req.String(), err)
		resp.ErrorCode = http_msg.EC_Fail
		return
	}
}

//type MonitorValConfig struct {
//	RoleID   int64  `json:"role_id"`   //角色ID
//	MonitorVal int32  `json:"monitor_val"` //监控值
//	Sign       string `json:"sign"`        //加密密钥
//}
//
//func (l *Logic) handleSetMonitorVal(w http.ResponseWriter, r *http.Request) {
//	var rsp http_msg.Response
//	rsp.ErrorCode = http_msg.EC_OK
//	defer func() {
//		http.Error(w, rsp.Marshal(), http.StatusOK)
//	}()
//	if r.Method != "POST" {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "bad request"
//		return
//	}
//	defer func() {
//		r.Body.Close()
//	}()
//	body, err := ioutil.ReadAll(r.Body)
//	if err != nil {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = err.Error()
//		return
//	}
//
//	var post MonitorValConfig
//	if err = json.Unmarshal(body, &post); err != nil {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = err.Error()
//		return
//	}
//
//	if post.Sign != l.Config().HttpSign {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "check sign fail"
//		return
//	}
//
//	if post.RoleID <= 0 {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "param error"
//		return
//	}
//
//	rolePID, e := rpc.GetRolePID(post.RoleID)
//	if e != nil {
//		l.Error("handleSetMonitorVal, get role pid fail, role id=%d, %s", post.RoleID, e)
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "notify fail role nil"
//		return
//	}
//
//	notify := rpc.Get_C2S_UpdateMonitorValNotify()
//	notify.RoleID = post.RoleID
//	notify.MonitorVal = post.MonitorVal
//	nactor.RootContext().Send(rolePID, notify)
//}
//
//type ArenaPointData struct {
//	RoleID int64  `json:"role_id"` //角色ID
//	ArenaID  int32  `json:"arena_id"`  //竞技场ID
//	Point    int32  `json:"point"`     //分数
//	Sign     string `json:"sign"`      //加密密钥
//}
//
//func (l *Logic) handleSetArenaPoint(w http.ResponseWriter, r *http.Request) {
//	var rsp http_msg.Response
//	rsp.ErrorCode = http_msg.EC_OK
//	defer func() {
//		http.Error(w, rsp.Marshal(), http.StatusOK)
//	}()
//	if r.Method != "POST" {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "bad request"
//		return
//	}
//	defer func() {
//		r.Body.Close()
//	}()
//	body, err := ioutil.ReadAll(r.Body)
//	if err != nil {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = err.Error()
//		return
//	}
//
//	var post ArenaPointData
//	if err = json.Unmarshal(body, &post); err != nil {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = err.Error()
//		return
//	}
//
//	if post.Sign != l.Config().HttpSign {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "check sign fail"
//		return
//	}
//
//	if post.RoleID <= 0 {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "param error"
//		return
//	}
//
//	_, e := rpc.GetRolePID(post.RoleID)
//	if e != nil {
//		l.Error("handleSetArenaPoint, get role pid fail, role id=%d, %s", post.RoleID, e)
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "notify fail role nil"
//		return
//	}
//
//	if post.Point <= 0 {
//		post.Point = int32(sd.GetArenaGlobal().ArenaPointsStartPoints())
//	}
//
//	if post.ArenaID <= 0 {
//		dbc := l.DBC()
//		dbSession := dbc.GetSession()
//		defer dbc.PutSession(dbSession)
//
//		query := bson.M{"_id": post.RoleID}
//		project := bson.M{"ArenaID": 1, "ArenaOpenTime": 1}
//		roleInfo, err := dbc.SelectOne_Role(dbSession, query, project)
//		if err != nil {
//			l.Error("handleSetArenaPoint get role[%d] data fail", post.RoleID, err)
//			rsp.ErrorCode = http_msg.EC_Fail
//			rsp.ErrorMessage = "get role data fail"
//			return
//		}
//		if roleInfo.ArenaID <= 0 || roleInfo.ArenaOpenTime <= 0 {
//			l.Error("handleSetArenaPoint get role arena ArenaID[%d] ArenaOpenTime[%d] error", roleInfo.ArenaID, roleInfo.ArenaOpenTime)
//			rsp.ErrorCode = http_msg.EC_Fail
//			rsp.ErrorMessage = fmt.Sprintf("get role arena ArenaID[%d] ArenaOpenTime[%d] error", roleInfo.ArenaID, roleInfo.ArenaOpenTime)
//			return
//		}
//		post.ArenaID = roleInfo.ArenaID
//	}
//	worldCache := l.Cache()
//	if worldCache == nil {
//		l.Error("handleSetArenaPoint cache is nil")
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "get worldCache data fail"
//		return
//	}
//	arenaInfo, ok := worldCache.ArenaInfos[post.ArenaID]
//	if !ok {
//		l.Error("handleSetArenaPoint get arenaInfo fail")
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "get arenaInfo fail"
//		return
//	}
//
//	// 设置竞技场分数，同时获得排名
//	rank, err := rpc.SetArenaPointByPID(arenaInfo.PID, post.RoleID, post.Point)
//	if err != nil {
//		l.Error("handleSetArenaPoint, rpc.SetArenaPointByPID fail [%s]", err.Error())
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "SetArenaPoint fail, " + err.Error()
//		return
//	}
//
//	rsp.ErrorMessage = strconv.Itoa(int(rank))
//}
//
//type AdminTriggerLimitedGiftPackParams struct {
//	RoleID      int64  `json:"role_id"`      //角色ID
//	ActivityID    int32  `json:"activity_id"`    //活动ID
//	PurchaseLevel int32  `json:"purchase_level"` //购买力等级
//	Sign          string `json:"sign"`           //加密密钥
//}
//
//func (l *Logic) handleAdminTriggerLimitedGiftPack(w http.ResponseWriter, r *http.Request) {
//	var rsp http_msg.Response
//	rsp.ErrorCode = http_msg.EC_OK
//	defer func() {
//		http.Error(w, rsp.Marshal(), http.StatusOK)
//	}()
//	if r.Method != "POST" {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "bad request"
//		return
//	}
//	defer func() {
//		r.Body.Close()
//	}()
//	body, err := ioutil.ReadAll(r.Body)
//	if err != nil {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = err.Error()
//		return
//	}
//
//	var post AdminTriggerLimitedGiftPackParams
//	if err = json.Unmarshal(body, &post); err != nil {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = err.Error()
//		return
//	}
//
//	if post.Sign != l.Config().HttpSign {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "check sign fail"
//		return
//	}
//
//	if post.RoleID <= 0 {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "param error"
//		return
//	}
//
//	rolePID, e := rpc.GetRolePID(post.RoleID)
//	if e != nil {
//		l.Error("handleAdminTriggerLimitedGiftPack, get role pid fail, role id=%d, %s", post.RoleID, e)
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "notify fail role nil"
//		return
//	}
//
//	notify := rpc.Get_C2S_AdminTriggerLimitedGiftPack()
//	notify.RoleID = post.RoleID
//	notify.ActivityID = post.ActivityID
//	notify.PurchaseLevel = post.PurchaseLevel
//	nactor.RootContext().Send(rolePID, notify)
//}
//
//func (l *Logic) handleGetCustomizeLimitGiftPack(w http.ResponseWriter, r *http.Request) {
//	var rsp http_msg.Response
//	rsp.ErrorCode = http_msg.EC_OK
//	defer func() {
//		http.Error(w, rsp.Marshal(), http.StatusOK)
//	}()
//	if r.Method != "POST" {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "bad request"
//		return
//	}
//	defer func() {
//		r.Body.Close()
//	}()
//	body, err := ioutil.ReadAll(r.Body)
//	if err != nil {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = err.Error()
//		return
//	}
//
//	var post struct {
//		RoleID int64  `json:"role_id"`
//		Sign     string `json:"sign"`
//	}
//	if err = json.Unmarshal(body, &post); err != nil {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = err.Error()
//		return
//	}
//
//	if post.Sign != l.Config().HttpSign {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "check sign fail"
//		return
//	}
//
//	if post.RoleID <= 0 {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "param error"
//		return
//	}
//
//	rolePID, e := rpc.GetRolePID(post.RoleID)
//	if e != nil {
//		l.Error("handleGetCustomizeLimitGiftPack, get role pid fail, role id=%d, %s", post.RoleID, e)
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "notify fail role nil"
//		return
//	}
//
//	list, err := rpc.GetCustomizeLimitGiftPack(rolePID)
//	if err != nil {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "rpc.GetCustomizeLimitGiftPack fail, " + err.Error()
//		return
//	}
//
//	s, _ := json.Marshal(list)
//
//	rsp.Data = string(s)
//}
//
//func (l *Logic) handleSetRoleName(w http.ResponseWriter, r *http.Request) {
//	var rsp http_msg.Response
//	rsp.ErrorCode = http_msg.EC_OK
//	defer func() {
//		http.Error(w, rsp.Marshal(), http.StatusOK)
//	}()
//	if r.Method != "POST" {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "bad request"
//		return
//	}
//	defer func() {
//		r.Body.Close()
//	}()
//	body, err := ioutil.ReadAll(r.Body)
//	if err != nil {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = err.Error()
//		return
//	}
//
//	type role struct {
//		RoleID   int64  `json:"role_id"`   //角色ID
//		RoleName string `json:"role_name"` //角色新昵称
//		Sign       string `json:"sign"`        //加密密钥
//	}
//	var post *role
//	if err = json.Unmarshal(body, &post); err != nil {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = err.Error()
//		return
//	}
//
//	if post.Sign != l.Config().HttpSign {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "check sign fail"
//		return
//	}
//	if post.RoleID <= 0 {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "param error"
//		return
//	}
//	if post.RoleName == "" {
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "param error"
//		return
//	}
//
//	rolePID, e := rpc.GetRolePID(post.RoleID)
//	if e != nil {
//		l.Error("handleSetRoleName, get role pid fail, role id=%d, %s", post.RoleID, e)
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = "notify fail role nil"
//		return
//	}
//
//	err = rpc.SetRoleNameByPID(rolePID, post.RoleID, post.RoleName)
//	if err != nil {
//		l.Error("handleSetRoleName, rpc.SetRoleNameByPID fail [%s]", err.Error())
//		rsp.ErrorCode = http_msg.EC_Fail
//		rsp.ErrorMessage = err.Error()
//		return
//	}
//}
