package role

import (
	"github.com/asynkron/protoactor-go/actor"

	nrpc "nggs/rpc"

	"rpc"

	"server/src/services/world/cache"
)

func (l *Logic) regAllRPCHandler() (err error) {
	err = l.RegisterRPCHandler(rpc.C2S_RoleLogin_MessageID(), l.handleRoleLogin)
	if err != nil {
		return
	}
	err = l.RegisterRPCHandler(rpc.C2S_RoleLogout_MessageID(), l.handleRoleLogout)
	if err != nil {
		return
	}
	return
}

func (l *Logic) handleRoleLogin(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	var recv = iRecv.(*rpc.C2S_RoleLogin)

	worldCache := l.Cache()
	if worldCache == nil {
		l.Error("handleRoleLogin world cache is nil")
		return
	}

	onlineRole := cache.NewOnlineRole(recv.RoleID, recv.OS, recv.Channel, recv.RolePID.ToReal())
	worldCache.OnlineRoleContainer().Set(recv.RoleID, onlineRole)
	l.Debug("handleRoleLogin success, role id=%d, os=%s, channel=%s, pid=%v", recv.RoleID, recv.OS, recv.Channel, recv.RolePID)
}

func (l *Logic) handleRoleLogout(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	var recv = iRecv.(*rpc.C2S_RoleLogout)

	worldCache := l.Cache()
	if worldCache == nil {
		l.Error("handleRoleLogout world cache is nil")
		return
	}

	worldCache.OnlineRoleContainer().Remove(recv.RoleID)
	l.Debug("handleRoleLogout success, role id=%d", recv.RoleID)
}
