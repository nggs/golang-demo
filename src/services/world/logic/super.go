package logic

import (
	"fmt"

	"github.com/asynkron/protoactor-go/actor"

	"server/src/services/world/export"
)

type Super struct {
	export.IWorld

	ICommon export.ICommon
	IMail   export.IMail
	IRole   export.IRole
	IZone   export.IZone
}

func NewSuper(iWorld export.IWorld) Super {
	return Super{
		IWorld: iWorld,
	}
}

func (Super) ILogic() {

}

func (l *Super) Init() error {
	l.ICommon = l.GetLogic(Common).(export.ICommon)
	if l.ICommon == nil {
		return fmt.Errorf("get Common logic fail")
	}
	l.IMail = l.GetLogic(Mail).(export.IMail)
	if l.IMail == nil {
		return fmt.Errorf("get Mail logic fail")
	}
	l.IRole = l.GetLogic(Role).(export.IRole)
	if l.IRole == nil {
		return fmt.Errorf("get Role logic fail")
	}
	l.IZone = l.GetLogic(Zone).(export.IZone)
	if l.IZone == nil {
		return fmt.Errorf("get Zone logic fail")
	}
	return nil
}

func (Super) Run() {

}

func (Super) Finish() {

}

func (Super) OnActorTerminated(who *actor.PID, ctx actor.Context) {

}

func (Super) OnPulse(ctx actor.Context) {

}
