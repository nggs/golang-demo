package zone

import (
	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	nrpc "nggs/rpc"

	"rpc"

	"server/src/services/world/cache"
)

func (l *Logic) regAllRPCHandler() (err error) {
	err = l.RegisterRPCHandler(rpc.C2S_RegisterZone_MessageID(), l.handleRegisterZone)
	if err != nil {
		return
	}
	err = l.RegisterRPCHandler(rpc.C2S_UnRegisterZone_MessageID(), l.handleUnRegisterZone)
	if err != nil {
		return
	}
	err = l.RegisterRPCHandler(rpc.C2S_GetZonePID_MessageID(), l.handleGetZonePID)
	if err != nil {
		return
	}
	return
}

func (l *Logic) handleRegisterZone(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_RegisterZone)
	if recv.ID == 0 || recv.OpenTime == 0 {
		l.Error("handleRegisterZone register zone[%d] fail, recv.ID == 0 || recv.OpenTime == 0", recv.ID)
		return
	}

	worldCache := l.Cache()
	if worldCache == nil {
		l.Error("handleRegisterZone worldCache is nil")
		return
	}

	zonePID := recv.PID.ToReal()

	if nactor.IsPIDPtrEmpty(zonePID) {
		l.Error("handleRegisterZone register zone[%d] fail, recv.PID is empty", recv.ID)
		return
	}

	if _, ok := worldCache.ZoneInfos[recv.ID]; ok {
		//l.Error("handleRegisterZone register zone[%d] fail, already register", recv.ID)
		return
	}

	worldCache.ZoneInfos[recv.ID] = cache.NewZoneInfo(recv.ID, recv.OpenTime, recv.Name, zonePID)

	if worldCache.LastOpenZone.OpenTime < recv.OpenTime ||
		(worldCache.LastOpenZone.OpenTime == recv.OpenTime && worldCache.LastOpenZone.ID < worldCache.LastOpenZone.ID) {

		worldCache.LastOpenZone.ID = recv.ID
		worldCache.LastOpenZone.OpenTime = recv.OpenTime
		worldCache.LastOpenZone.PID = zonePID.Clone()
		l.Info("handleRegisterZone last open zone id=%d, last open zone time=%d, last open zone pid=%s",
			worldCache.LastOpenZone.ID, worldCache.LastOpenZone.OpenTime, worldCache.LastOpenZone.PID.String())
	}

	l.Debug("handleRegisterZone register zone[%d] success", recv.ID)
}

func (l *Logic) handleUnRegisterZone(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_UnRegisterZone)

	worldCache := l.Cache()
	if worldCache == nil {
		l.Error("handleUnRegisterZone worldCache is nil")
		return
	}

	if _, ok := worldCache.ZoneInfos[recv.ID]; !ok {
		l.Error("handleUnRegisterZone unregister zone[%d] fail, not found")
		return
	}

	delete(worldCache.ZoneInfos, recv.ID)
	l.Debug("handleUnRegisterZone unregister zone[%d] success", recv.ID)

	if worldCache.LastOpenZone.ID == recv.ID {
		worldCache.LastOpenZone.ID = 0
		worldCache.LastOpenZone.OpenTime = 0
		worldCache.LastOpenZone.PID = nil
		// 找出最后开服的zone
		for _, info := range worldCache.ZoneInfos {
			if worldCache.LastOpenZone.OpenTime < info.OpenTime {
				worldCache.LastOpenZone.ID = info.ID
				worldCache.LastOpenZone.OpenTime = info.OpenTime
				worldCache.LastOpenZone.PID = info.PID.Clone()
			}
		}
		l.Info("handleUnRegisterZone last open info id=%d, last open info time=%d, last open info pid=%s",
			worldCache.LastOpenZone.ID, worldCache.LastOpenZone.OpenTime, worldCache.LastOpenZone.PID.String())
	}
}

func (l *Logic) handleGetZonePID(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_GetZonePID)
	send := rpc.Get_S2C_GetZonePID()
	send.ID = recv.ID
	defer func() {
		if sender != nil {
			ctx.Respond(send)
			l.Debug("handleGetZonePID, recv=%s, send=%s", recv.String(), send.String())
		}
	}()

	worldCache := l.Cache()
	if worldCache == nil {
		l.Error("handleGetZonePID cache is nil")
		send.EC = rpc.EC_InvalidParam
		return
	}

	if recv.ID == 0 {
		l.Error("handleGetZonePID recv.ID is 0")
		send.EC = rpc.EC_InvalidParam
		return
	}

	info, ok := worldCache.ZoneInfos[recv.ID]
	if !ok {
		send.EC = rpc.EC_Fail
		return
	}

	send.PID = rpc.FromRealPID(info.PID)
}
