package zone

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"server/src/services/world/cache"
	"server/src/services/world/http_msg"

	"rpc"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) regAllHttpHandler() (err error) {
	err = l.RegisterHttpRequestHandler("GET", "/create_zone", l.handleCreateZone, nil)
	if err != nil {
		return
	}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func (l *Logic) handleCreateZone(ctx *gin.Context) {
	var resp http_msg.Response
	defer func() {
		ctx.String(http.StatusOK, resp.Marshal())
	}()
	recv := http_msg.NewRequestCreateZone()

	reqParams, reqEC := recv.UnmarshalFromHttpRequest(ctx.Request, l.Config().HttpSign)
	if reqEC != http_msg.EC_OK {
		resp.ErrorCode = reqEC
		l.Error("handleCreateZone marshal request fail, request params=%+v, %v", reqParams, resp.ErrorCode)
		return
	}

	if recv.ID == 0 || recv.OpenTime == 0 || recv.Name == "" {
		l.Error("handleCreateZone create zone fail, recv.ID == 0 || recv.OpenTime == 0 || recv.Name ==")
		resp.ErrorCode = http_msg.EC_Fail
		return
	}

	worldCache := l.Cache()
	if worldCache == nil {
		l.Error("handleCreateZone worldCache is nil")
		resp.ErrorCode = http_msg.EC_Fail
		return
	}

	if _, ok := worldCache.ZoneInfos[recv.ID]; ok {
		l.Error("handleCreateZone fail, already exist, zone id=%d", recv.ID)
		resp.ErrorCode = http_msg.EC_AlreadyOpenZone
		return
	}

	_, newZonePID, err := rpc.CreateZone(recv.ID, recv.OpenTime, recv.Name)
	if err != nil {
		l.Error("")
		resp.ErrorCode = http_msg.EC_Fail
		return
	}

	worldCache.ZoneInfos[recv.ID] = cache.NewZoneInfo(recv.ID, recv.OpenTime, recv.Name, newZonePID)

	if recv.OpenTime > worldCache.LastOpenZone.OpenTime {
		worldCache.LastOpenZone.ID = recv.ID
		worldCache.LastOpenZone.OpenTime = recv.OpenTime
		worldCache.LastOpenZone.PID = newZonePID.Clone()
		l.Info("handleCreateZone last open zone id=%d, last open zone time=%d, last open zone pid=%s",
			worldCache.LastOpenZone.ID, worldCache.LastOpenZone.OpenTime, worldCache.LastOpenZone.PID.String())
	}

	l.Info("handleCreateZone create zone[%d] success, open time=[%d] name=[%s]", recv.ID, recv.OpenTime, recv.Name)
}
