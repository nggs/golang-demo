package mail

import (
	"server/src/services/world/export"
	"server/src/services/world/logic"

	nexport "nggs/export"
)

const (
	ID = logic.Mail
)

func NewLogic(iWorld export.IWorld) nexport.ILogic {
	l := &Logic{
		Super: logic.NewSuper(iWorld),
	}
	return l
}

func Init() {
	logic.RegisterFactory(ID, NewLogic)
}
