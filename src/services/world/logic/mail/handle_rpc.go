package mail

import (
	"github.com/asynkron/protoactor-go/actor"

	nactor "nggs/actor"
	nrpc "nggs/rpc"

	"model"
	"rpc"
)

func (l *Logic) regAllRPCHandler() (err error) {
	err = l.RegisterRPCHandler(rpc.C2S_SelectWorldMail_MessageID(), l.handleSelectWorldMail)
	if err != nil {
		return
	}
	return
}

func (l *Logic) handleSelectWorldMail(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	recv := iRecv.(*rpc.C2S_SelectWorldMail)
	send := iSend.(*rpc.S2C_SelectWorldMail)
	defer func() {
		if !nactor.IsPIDPtrEmpty(recv.Sender.ToReal()) {
			nactor.RootContext().Send(recv.Sender.ToReal(), send)
		} else if sender != nil {
			ctx.Respond(send)
		}
	}()

	if recv.UID == 0 && recv.SendTime == 0 {
		l.Error("handleSelectWorldMail recv.UID == 0 && recv.SendTime == 0")
		send.EC = rpc.EC_InvalidParam
		return
	}

	data := l.Data()

	container := data.WorldMailContainer()
	container.Each(func(uid int64, worldMail *model.WorldMail) (continued bool) {
		if len(worldMail.ZoneIDs) > 0 && recv.ZoneID > 0 {
			var inZone bool
			for _, value := range worldMail.ZoneIDs {
				if recv.ZoneID == value {
					inZone = true
					break
				}
			}
			if !inZone {
				return true
			}
		}
		if recv.UID > 0 {
			if worldMail.UID > recv.UID {
				send.Mails = append(send.Mails, worldMail.Clone())
			}
		} else if recv.SendTime > 0 {
			if worldMail.SendTime >= recv.SendTime {
				send.Mails = append(send.Mails, worldMail.Clone())
			}
		}
		return true
	})
}
