package mail

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/asynkron/protoactor-go/actor"

	"rpc"

	"server/src/services/world/http_msg"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) regAllHttpHandler() (err error) {
	err = l.RegisterHttpRequestHandler("GET", "/personal_mail", l.handlePersonalMail, nil)
	if err != nil {
		return
	}
	err = l.RegisterHttpRequestHandler("GET", "/world_mail", l.handleWorldMail, nil)
	if err != nil {
		return
	}
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) handlePersonalMail(ctx *gin.Context) {
	//w.Header().Set("Access-Control-Allow-Origin", "*")

	var resp http_msg.Response
	defer func() {
		ctx.String(http.StatusOK, resp.Marshal())
	}()

	req := http_msg.NewRequestPersonMail()
	reqParams, reqEC := req.UnmarshalFromHttpRequest(ctx.Request, l.Config().HttpSign)
	if reqEC != http_msg.EC_OK {
		resp.ErrorCode = reqEC
		l.Error("handlePersonalMail marshal request fail, request params=%+v, %v", reqParams, resp.ErrorCode)
		return
	}

	l.Debug("handlePersonalMail request params=%+v, req=%s", reqParams, req.String())

	// 检查奖励
	//if req.Reward != nil && !req.Reward.IsEmpty() {
	//	reward := req.Reward.ToSD(nil)
	//	if err := reward.Check(); err != nil {
	//		l.Error("handlePersonalMail check reward fail, %s", err)
	//		resp.ErrorCode = http_msg.EC_InvalidReward
	//		return
	//	}
	//}

	var pids []*actor.PID
	for _, roleID := range req.RoleIDs {
		pid, err := rpc.GetRolePID(roleID)
		if err != nil {
			l.Error("handlePersonalMail get role pid fail, role id=%d, %s", roleID, err)
			resp.ErrorCode = http_msg.EC_RoleNotExist
			return
		}
		pids = append(pids, pid)
	}
	//for _, pid := range pids {
	//	err := rpc.SendMailByRolePID(pid, int32(req.Type), req.Title, req.Content, req.Params, req.Reward)
	//	if err != nil {
	//		l.Error("handlePersonalMail rpc.SendMailByRolePID fail, %s", err)
	//		resp.ErrorCode = http_msg.EC_Fail
	//		return
	//	}
	//}
}

func (l *Logic) handleWorldMail(ctx *gin.Context) {
	//w.Header().Set("Access-Control-Allow-Origin", "*")

	var resp http_msg.Response
	defer func() {
		ctx.String(http.StatusOK, resp.Marshal())
	}()

	data := l.Data()
	if data == nil {
		l.Error("handleWorldMail fail, world data is nil")
		resp.ErrorCode = http_msg.EC_Fail
		return
	}

	req := http_msg.NewRequestWorldMail()
	reqParams, reqEC := req.UnmarshalFromHttpRequest(ctx.Request, l.Config().HttpSign)
	if reqEC != http_msg.EC_OK {
		resp.ErrorCode = reqEC
		l.Error("handleWorldMail marshal request fail, request params=%+v, %v", reqParams, resp.ErrorCode)
		return
	}

	l.Debug("handleWorldMail request params=%+v, req=%s", reqParams, req.String())

	//if req.Reward != nil && !req.Reward.IsEmpty() {
	//	// 检查奖励
	//	reward := req.Reward.ToSD(nil)
	//	if err := reward.Check(); err != nil {
	//		l.Error("handleWorldMail check reward fail, %s", err)
	//		resp.ErrorCode = http_msg.EC_InvalidReward
	//		return
	//	}
	//}

	//// 保存到数据库
	//dbc := l.DBC()
	//worldMail, err := dbc.CreateWorldMail(int32(req.Type), req.Title, req.Content, req.Params, req.Reward, req.ZoneIDS)
	//if err != nil {
	//	l.Error("handleWorldMail create world mail fail, %s", err)
	//	resp.ErrorCode = http_msg.EC_InvalidReward
	//	return
	//}
	//container := data.WorldMailContainer()
	//container.Set(worldMail.UID, worldMail)
	//
	//err = rpc.SendMailToAllGame(worldMail.UID, int32(req.Type), req.Title, req.Content, req.Params, req.Reward, req.ZoneIDS)
	//if err != nil {
	//	l.Error("handleWorldMail rpc.SendMailToAllGame fail, %s", err)
	//	resp.ErrorCode = http_msg.EC_Fail
	//	return
	//}
}
