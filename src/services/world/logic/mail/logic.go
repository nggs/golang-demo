package mail

import (
	"server/src/services/world/logic"

	nexport "nggs/export"
)

type Logic struct {
	logic.Super
}

func (Logic) ID() nexport.LogicID {
	return ID
}

func (Logic) IMail() {

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) Init() (err error) {
	err = l.Super.Init()
	if err != nil {
		return
	}
	err = l.regAllRPCHandler()
	if err != nil {
		return
	}
	err = l.regAllHttpHandler()
	if err != nil {
		return
	}
	return nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) Run() {
}
