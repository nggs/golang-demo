package common

import (
	"fmt"

	"github.com/asynkron/protoactor-go/actor"

	nrpc "nggs/rpc"

	"rpc"
)

func (l *Logic) regAllRPCHandler() (err error) {
	err = l.RegisterRPCHandler(rpc.C2S_CMD_MessageID(), l.handleCMD)
	if err != nil {
		return
	}
	return
}

func (l *Logic) handleCMD(sender *actor.PID, iRecv nrpc.IMessage, iSend nrpc.IMessage, ctx actor.Context, args ...interface{}) {
	var recv = iRecv.(*rpc.C2S_CMD)
	var send = iSend.(*rpc.S2C_CMD)
	defer func() {
		if sender != nil {
			ctx.Respond(send)
		}
		l.Info("handleCMD, recv=%s, send=%s", recv.String(), send.String())
	}()

	worldCache := l.Cache()
	if worldCache == nil {
		send.Result = "world cache is nil"
		return
	}

	switch recv.Type {
	case "online_num":
		send.Result = fmt.Sprintf("%d", worldCache.OnlineRoleContainer().Size())

	default:
		send.Result = "unsupported cmd type"
		return
	}
}
