package common

import (
	"cluster"
	"time"

	nactor "nggs/actor"
	nexport "nggs/export"

	"rpc"

	"server/src/services/world/logic"
)

type Logic struct {
	logic.Super
}

func (Logic) ID() nexport.LogicID {
	return ID
}

func (Logic) ICommon() {

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) Init() (err error) {
	err = l.Super.Init()
	if err != nil {
		return
	}
	err = l.regAllRPCHandler()
	if err != nil {
		return
	}
	return nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (l *Logic) Run() {
	l.startNextDayRefreshTimer()
	l.startNextZeroClockTimer()
}

func (l *Logic) startNextDayRefreshTimer() {
	//now := time.Now()
	//nextTime := sd.GetNextDayRefreshTime()
	//dur := nextTime.Sub(now)
	//l.NewTimer(dur, 0, l.dayRefresh)
	//l.Info("after %v is day refresh time", dur)
}

func (l *Logic) dayRefresh(nactor.TimerID, nactor.TimerID) {
	nowUnix := time.Now().Unix()

	if err := rpc.SendDayRefreshToService(nowUnix, cluster.ZonesServiceName); err != nil {
		l.Error("dayRefresh rpc.SendDayRefreshToService fail, %s", err)
	}
	if err := rpc.SendDayRefreshToService(nowUnix, cluster.ZonesServiceName); err != nil {
		l.Error("dayRefresh rpc.SendDayRefreshToService fail, %s", err)
	}
	if err := rpc.SendDayRefreshToService(nowUnix, cluster.ZonesServiceName); err != nil {
		l.Error("dayRefresh rpc.SendDayRefreshToService fail, %s", err)
	}

	l.startNextDayRefreshTimer()
}

func (l *Logic) startNextZeroClockTimer() {
	//now := time.Now()
	//nextTime := sd.GetNextZeroClock()
	//dur := nextTime.Sub(now)
	//l.NewTimer(dur, 0, l.zeroClock)
	//l.Info("after %v is zero clock", dur)
}

func (l *Logic) zeroClock(nactor.TimerID, nactor.TimerID) {
	nowUnix := time.Now().Unix()

	if err := rpc.SendZeroClockToService(nowUnix, cluster.ZonesServiceName); err != nil {
		l.Error("zeroClock rpc.SendZeroClockToService fail, %s", err)
	}
	if err := rpc.SendZeroClockToService(nowUnix, cluster.ZonesServiceName); err != nil {
		l.Error("zeroClock rpc.SendZeroClockToServices fail, %s", err)
	}
	if err := rpc.SendZeroClockToService(nowUnix, cluster.ZonesServiceName); err != nil {
		l.Error("zeroClock rpc.SendZeroClockToService fail, %s", err)
	}

	l.startNextZeroClockTimer()
}
