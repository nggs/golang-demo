package common

import (
	nexport "nggs/export"

	"server/src/services/world/export"
	"server/src/services/world/logic"
)

const (
	ID = logic.Common
)

func NewLogic(iWorld export.IWorld) nexport.ILogic {
	l := &Logic{
		Super: logic.NewSuper(iWorld),
	}
	return l
}

func Init() {
	logic.RegisterFactory(ID, NewLogic)
}
