package cache

type World struct {
	Dirty bool

	onlineRoles map[int64]*OnlineRole // roleID -> OnlineRole

	ZoneInfos    map[int32]*ZoneInfo
	LastOpenZone *ZoneInfo // 最后开启zone

	//ArenaInfos    map[int32]*ArenaInfo
	//LastOpenArena *ArenaInfo // 最后开启的arena
	//
	//SeniorArenaInfos    map[int32]*SeniorArenaInfo
	//LastOpenSeniorArena *SeniorArenaInfo // 最后开启的arena

	//recommendGuildManager *RecommendGuildManager
}

func New() *World {
	w := &World{
		onlineRoles:  map[int64]*OnlineRole{},
		ZoneInfos:    map[int32]*ZoneInfo{},
		LastOpenZone: NewZoneInfo(0, 0, "", nil),
		//ArenaInfos:            map[int32]*ArenaInfo{},
		//LastOpenArena:         NewArenaInfo(0, nil),
		//SeniorArenaInfos:      map[int32]*SeniorArenaInfo{},
		//LastOpenSeniorArena:   NewSeniorArenaInfo(0, nil),
		//recommendGuildManager: NewRecommendGuildManager(),
	}
	return w
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (m *World) OnlineRoleContainer() *RoleContainer {
	if m.onlineRoles == nil {
		m.onlineRoles = *NewRoleContainer()
	}
	return (*RoleContainer)(&m.onlineRoles)
}

//func (m *World) RecommendGuildManager() *RecommendGuildManager {
//	return m.recommendGuildManager
//}
