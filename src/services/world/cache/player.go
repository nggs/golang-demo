package cache

import (
	"github.com/asynkron/protoactor-go/actor"

	"model"
)

type OnlineRole struct {
	*model.RoleMinimal
	pid     *actor.PID
	os      string
	channel string
}

func NewOnlineRole(id int64, os string, channel string, pid *actor.PID) *OnlineRole {
	p := &OnlineRole{
		RoleMinimal: model.Get_RoleMinimal(),
		os:          os,
		channel:     channel,
		pid:         pid.Clone(),
	}
	p.ID = id
	p.AccountID, p.ServerID = model.RoleIDToAccountIDServerID(p.ID)
	return p
}

func (p OnlineRole) Clone() *OnlineRole {
	n := &OnlineRole{
		os:      p.os,
		channel: p.channel,
	}
	n.RoleMinimal = p.RoleMinimal.Clone()
	return n
}

func (p OnlineRole) PID() *actor.PID {
	return p.pid.Clone()
}

func (p OnlineRole) OS() string {
	return p.os
}

func (p OnlineRole) Channel() string {
	return p.channel
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type RoleContainer map[int64]*OnlineRole

func ToRoleContainer(m map[int64]*OnlineRole) *RoleContainer {
	if m == nil {
		return nil
	}
	return (*RoleContainer)(&m)
}

func NewRoleContainer() (m *RoleContainer) {
	m = &RoleContainer{}
	return
}

func (m *RoleContainer) Get(key int64) (value *OnlineRole, ok bool) {
	value, ok = (*m)[key]
	return
}

func (m *RoleContainer) Set(key int64, value *OnlineRole) {
	(*m)[key] = value
}

func (m *RoleContainer) Add(key int64) (value *OnlineRole) {
	value = &OnlineRole{}
	(*m)[key] = value
	return
}

func (m *RoleContainer) Remove(key int64) (removed bool) {
	if _, ok := (*m)[key]; ok {
		delete(*m, key)
		return true
	}
	return false
}

func (m *RoleContainer) RemoveOne(fn func(key int64, value *OnlineRole) (removed bool)) {
	for key, value := range *m {
		if fn(key, value) {
			delete(*m, key)
			break
		}
	}
}

func (m *RoleContainer) RemoveSome(fn func(key int64, value *OnlineRole) (removed bool)) {
	left := map[int64]*OnlineRole{}
	for key, value := range *m {
		if !fn(key, value) {
			left[key] = value
		}
	}
	*m = left
}

func (m *RoleContainer) Each(f func(key int64, value *OnlineRole) (continued bool)) {
	for key, value := range *m {
		if !f(key, value) {
			break
		}
	}
}

func (m RoleContainer) Size() int {
	return len(m)
}

func (m RoleContainer) Clone() (n *RoleContainer) {
	if m.Size() == 0 {
		return nil
	}
	n = ToRoleContainer(make(map[int64]*OnlineRole, m.Size()))
	for k, v := range m {
		if v != nil {
			(*n)[k] = v.Clone()
		} else {
			(*n)[k] = nil
		}
	}
	return n
}

func (m *RoleContainer) Clear() {
	*m = *NewRoleContainer()
}
