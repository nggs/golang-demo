package cache

import (
	"github.com/asynkron/protoactor-go/actor"

	"model"
)

type ZoneInfo struct {
	*model.ZoneMinimal
	PID *actor.PID
}

func NewZoneInfo(id int32, openTime int64, name string, pid *actor.PID) *ZoneInfo {
	i := &ZoneInfo{
		ZoneMinimal: model.Get_ZoneMinimal(),
		PID:         pid,
	}
	i.ID = id
	i.OpenTime = openTime
	i.Name = name
	return i
}
