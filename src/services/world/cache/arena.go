package cache

import (
	"github.com/asynkron/protoactor-go/actor"
)

type ArenaInfo struct {
	ID  int32
	PID *actor.PID
}

func NewArenaInfo(id int32, pid *actor.PID) *ArenaInfo {
	i := &ArenaInfo{
		ID:  id,
		PID: pid,
	}
	i.ID = id
	return i
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type SeniorArenaInfo struct {
	ID  int32
	PID *actor.PID
}

func NewSeniorArenaInfo(id int32, pid *actor.PID) *SeniorArenaInfo {
	i := &SeniorArenaInfo{
		ID:  id,
		PID: pid,
	}
	return i
}
