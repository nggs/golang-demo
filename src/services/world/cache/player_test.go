//go:build ignore
// +build ignore

package cache

import (
	"testing"

	"model"

	"github.com/asynkron/protoactor-go/actor"
)

func TestWorld_Role(t *testing.T) {
	cache := New()

	cache.SetRole(model.AccountIDServerIDToRoleID(1, 1), nil, false)
	cache.SetRole(model.AccountIDServerIDToRoleID(1, 2), nil, false)

	{
		_, _, _, ok := cache.GetAccountOnlineRole(1)
		if ok {
			t.Error("GetAccountOnlineRole error")
			return
		}
	}

	cache.SetAccountOnlineRole(1, 1, &actor.PID{Address: "localhost", Id: "1"})

	{
		serverID, roleID, rolePID, ok := cache.GetAccountOnlineRole(1)
		if !ok {
			t.Error("GetAccountOnlineRole error")
			return
		}
		if roleID != model.AccountIDServerIDToRoleID(1, 1) {
			t.Errorf("GetAccountOnlineRole error")
			return
		}

		t.Logf("online OnlineRole: server id=%d, OnlineRole id=%d, OnlineRole pid=%v", serverID, roleID, rolePID)
	}

	cache.SetAccountOnlineRole(1, 2, &actor.PID{Address: "localhost", Id: "2"})

	{
		serverID, roleID, rolePID, ok := cache.GetAccountOnlineRole(1)
		if !ok {
			t.Error("GetAccountOnlineRole error")
			return
		}

		if roleID != model.AccountIDServerIDToRoleID(1, 2) {
			t.Errorf("SetAccountOnlineRole error")
			return
		}

		t.Logf("online OnlineRole: server id=%d, OnlineRole id=%d, OnlineRole pid=%v", serverID, roleID, rolePID)
	}

	cache.SetAccountOnlineRole(1, 3, &actor.PID{Address: "localhost", Id: "3"})

	{
		serverID, roleID, rolePID, ok := cache.GetAccountOnlineRole(1)
		if !ok {
			t.Error("GetAccountOnlineRole error")
			return
		}

		if roleID != model.AccountIDServerIDToRoleID(1, 3) {
			t.Errorf("SetAccountOnlineRole error")
			return
		}

		t.Logf("online OnlineRole: server id=%d, OnlineRole id=%d, OnlineRole pid=%v", serverID, roleID, rolePID)
	}
}
