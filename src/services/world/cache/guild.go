package cache

type GuildInRecommendList struct {
	ID        int64
	MemberNum int32
	Level     int32
}

type RecommendGuildManager struct {
	m map[int64]*GuildInRecommendList
}

func NewRecommendGuildManager() *RecommendGuildManager {
	return &RecommendGuildManager{
		m: map[int64]*GuildInRecommendList{},
	}
}

func (mgr *RecommendGuildManager) Clear() {
	mgr.m = map[int64]*GuildInRecommendList{}
}

func (mgr *RecommendGuildManager) Update(guildID int64, memberNum int32, level int32) {
	if g, ok := mgr.m[guildID]; ok && g != nil {
		g.MemberNum = memberNum
		g.Level = level
	} else {
		mgr.m[guildID] = &GuildInRecommendList{
			ID:        guildID,
			MemberNum: memberNum,
			Level:     level,
		}
	}
}

func (mgr *RecommendGuildManager) Remove(guildID int64) {
	delete(mgr.m, guildID)
}

func (mgr *RecommendGuildManager) Each(fn func(guildInRecommendList *GuildInRecommendList) (continued bool)) {
	if fn == nil {
		return
	}
	for _, g := range mgr.m {
		if !fn(g) {
			break
		}
	}
}
