package event

const (
	OneMinutePassedID    uint16 = iota + 1 // 上线每分钟事件
	FiveMinutePassedID                     // 上线5分钟事件
	OneHourPassedID                        // 每小时事件
	GameInstanceDeleteID                   // game实例被删除
)
