package event

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 上线每分钟事件
type OneMinutePassed struct {
}

func (OneMinutePassed) EventID() uint16 {
	return OneMinutePassedID
}

func (e OneMinutePassed) Event() {
}

// 上线5分钟事件
type FiveMinutePassed struct {
}

func (FiveMinutePassed) EventID() uint16 {
	return FiveMinutePassedID
}

func (e FiveMinutePassed) Event() {
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 每小时事件
type OneHourPassed struct {
}

func (OneHourPassed) EventID() uint16 {
	return OneHourPassedID
}

func (e OneHourPassed) Event() {
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// game实例被删除事件
type GameInstanceDelete struct {
	ID int
}

func (GameInstanceDelete) EventID() uint16 {
	return GameInstanceDeleteID
}

func (e GameInstanceDelete) Event() {
}
