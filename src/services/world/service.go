package world

import (
	"fmt"
	"net/http"
	"runtime/debug"
	"strconv"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"go.etcd.io/etcd/client/v3"

	"github.com/asynkron/protoactor-go/actor"
	"github.com/coreos/etcd/mvcc/mvccpb"
	"github.com/globalsign/mgo/bson"

	nactor "nggs/actor"
	napp "nggs/app"
	ncluster "nggs/cluster"
	ndebug "nggs/debug"
	nexport "nggs/export"
	nlog "nggs/log"
	nhttp "nggs/network/http"
	nrpc "nggs/rpc"
	nservice "nggs/service"

	"cluster"
	"model"
	"rpc"

	"server/src/services/world/cache"
	worldEvent "server/src/services/world/event"
	"server/src/services/world/logic"
)

const (
	ServiceName      = cluster.WorldServiceName
	ServiceActorName = ServiceName
)

type Service struct {
	*nactor.Actor

	appCfg *napp.Config

	cfg *cluster.WorldConfig

	dbCfg cluster.MongoDBConfig
	dbc   *model.SimpleClient

	//redisCli *redis.Client

	instance *nservice.Instance

	data *model.World

	cache *cache.World

	fiveMinuteCounter int // 5分钟计数器
	oneHourCounter    int // 1小时计数器
}

func New() *Service {
	svc := &Service{
		instance: nservice.NewInstance(),
		dbc:      model.NewSimpleClient(),
		cache:    cache.New(),
	}
	return svc
}

func (svc *Service) Init(iAppConfig nexport.IAppConfig, serviceID int, startedWg *sync.WaitGroup, stoppedWg *sync.WaitGroup, args ...interface{}) (err error) {
	var ok bool
	svc.appCfg, ok = iAppConfig.(*napp.Config)
	if !ok || svc.appCfg == nil {
		err = fmt.Errorf("init app config fail")
		return
	}

	svc.cfg = cluster.MustGetWorldConfig()

	// 根据配置初始化日志
	if svc.appCfg.LogDir != "" {
		logDir := fmt.Sprintf("%s/%s-%d", svc.appCfg.LogDir, ServiceName, svc.cfg.GetID())
		logger = nlog.New(logDir, ServiceName)
		logger.SetLevel(initLogLevel)
	}

	svc.dbCfg = cluster.MustGetMainDBConfig()

	err = svc.dbc.Init(svc.dbCfg.Url, svc.dbCfg.SessionNum, svc.dbCfg.Name)
	if err != nil {
		return
	}

	dbSession := svc.dbc.GetSession()
	defer svc.dbc.PutSession(dbSession)

	// 加载world数据
	svc.data, err = svc.dbc.FindOneWorld(dbSession, bson.M{"_id": svc.cfg.GetID()})
	if err != nil {
		svc.Error("load world data fail, %s", err)
		return
	}

	////初始化redis连接
	//svc.redisCli = redis.NewClient(&redis.Options{
	//	Addr:     svc.cfg.Redis.Addr,
	//	Password: svc.cfg.Redis.Password,
	//	DB:       svc.cfg.Redis.DB,
	//})
	//
	//err = svc.redisCli.Ping().Err()
	//if err != nil {
	//	return
	//}

	svc.Actor = nactor.New(
		nactor.WithLogger(logger),
		nactor.WithStartedWaitGroup(startedWg),
		nactor.WithStoppedWaitGroup(stoppedWg),
		nactor.WithOnReceiveMessage(svc.onReceiveMessage),
		nactor.WithOnStarted(svc.onStarted),
		nactor.WithOnStopping(svc.onStopping),
		nactor.WithOnStopped(svc.onStopped),
		nactor.WithOnActorTerminate(svc.onActorTerminated),
		nactor.WithLogics(logic.GenerateLogicMap(svc)),
		nactor.WithPulse(60*time.Second, svc.onPulse),
		nactor.WithRPC(rpc.Protocol, func(sender *actor.PID, iRequestMessage nrpc.IMessage, ctx actor.Context) (args []interface{}, ok bool, err error) {
			if svc.data == nil {
				svc.Error("before dispatch rpc, data is nil")
				return
			}
			if svc.cache == nil {
				svc.Error("before dispatch rpc, cache is nil")
				return
			}
			ok = true
			return
		}, nil),
		nactor.WithHttpServer(svc.cfg.HttpServer.ListenAddr, "", "", nil, nil, func(ctx *gin.Context) (ok bool, err error) {
			if svc.data == nil {
				svc.Error("before dispatch http request, data is nil")
				return
			}
			if svc.cache == nil {
				svc.Error("before dispatch http request, cache is nil")
				return
			}
			ok = true
			return
		}, nil),
	)

	err = cluster.AddWatchCallback(func(err error, section string, serviceName string, serviceID int, ev *clientv3.Event) {
		switch section {
		case ncluster.SectionInstance:
			switch serviceName {
			case cluster.GameServiceName:
				switch ev.Type {
				case mvccpb.DELETE:
					// 触发game实例被删除事件
					svc.PostEvent(&worldEvent.GameInstanceDelete{ID: serviceID})
				}
			}
		}
		return
	})

	return
}

func (svc *Service) Run(ctx actor.Context, pprofAddress string, args ...interface{}) (err error) {
	err = svc.Actor.Start(ctx, fmt.Sprintf("%s-%d", ServiceActorName, svc.cfg.GetID()))
	if err != nil {
		return
	}

	svc.instance.PID = svc.PID()
	svc.instance.PprofAddress = pprofAddress

	err = cluster.TryLockPosition(ServiceName, svc.cfg.GetID(), svc.instance)
	if err != nil {
		err = fmt.Errorf("try lock position fail, %w", err)
		return
	}

	ndebug.RegisterCommand("/cmd", svc.handleCMD)

	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (svc *Service) handleCMD(writer http.ResponseWriter, request *http.Request) {
	err := request.ParseForm()
	if err != nil {
		svc.Error("handleCMD ParseForm fail, %s", err)
		return
	}

	requestParams := nhttp.CollectRequestParams(request)

	svc.Info("handleCMD requestParams=%+v", requestParams)

	var result string
	defer func() {
		_, err := writer.Write([]byte(result + "\n"))
		if err != nil {
			svc.Error("handleCMD write result fail, result=%s, %e", result, err)
		} else {
			svc.Info("handleCMD %s", result)
		}
	}()

	serviceName, ok := requestParams["serviceName"]
	if !ok || serviceName == "" {
		result = "service name is empty"
		return
	}

	if !cluster.IsServiceName(serviceName) {
		result = "invalid service name"
		return
	}

	strServiceID, ok := requestParams["serviceID"]
	if !ok || strServiceID == "" {
		result = "service id is empty"
		return
	}
	serviceID, err := strconv.Atoi(strServiceID)
	if err != nil {
		result = "invalid service id"
		return
	}

	var servicePID *actor.PID
	err = cluster.EachServiceInfo(serviceName, func(info *nservice.Info) (continued bool) {
		if info.Config.GetID() != serviceID {
			return true
		}
		if info.Instance == nil || info.Instance.IsEmpty() {
			return true
		}
		servicePID = info.Instance.PID.Clone()
		return true
	})
	if nactor.IsPIDPtrEmpty(servicePID) {
		result = "service instance not exist"
		return
	}
	if err != nil {
		result = fmt.Sprintf("get service instance fail, %s", err)
		return
	}

	send := rpc.Get_C2S_CMD()

	send.Type, ok = requestParams["type"]
	if !ok || send.Type == "" {
		result = "type is empty"
		return
	}

	send.Params = map[string]string{}
	for k, v := range requestParams {
		switch k {
		case "serviceName", "serviceID", "type":
			continue
		}
		send.Params[k] = v
	}

	f := nactor.RootContext().RequestFuture(servicePID, send, 10*time.Second)
	iRecv, err := f.Result()
	if err != nil {
		result = fmt.Sprintf("send rpc msg fail, %s", err)
		return
	}
	recv, ok := iRecv.(*rpc.S2C_CMD)
	if !ok || recv == nil {
		result = fmt.Sprintf("recv rpc msg fail")
		return
	}
	result = recv.Result
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (svc *Service) onStarted(ctx actor.Context) {
}

func (svc *Service) onStopping(ctx actor.Context) {
	svc.SaveData(true)

	//_ = svc.redisCli.Close()
}

func (svc *Service) onStopped(ctx actor.Context) {

}

func (svc *Service) onReceiveMessage(ctx actor.Context) {
	defer func() {
		if r := recover(); r != nil {
			svc.Error("%v\n%s", r, debug.Stack())
			// todo 为了不让world挂掉，暂时不往外panic
			//panic(r)
		}
	}()

	sender := ctx.Sender()
	//svc.Debug("recv [%#v] from [%v]", ctx.Message(), sender)
	switch msg := ctx.Message().(type) {
	default:
		svc.Error("recv unsupported msg [%#v] from [%v]", msg, sender)
	}
}

func (svc *Service) onActorTerminated(who *actor.PID, ctx actor.Context) {
	svc.Debug("[%s] terminated", who.Id)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (svc *Service) onPulse(ctx actor.Context) {
	// 每次心跳都检查是否脏数据，如果是脏数据则保存
	svc.SaveData(false)

	//now := time.Now()
	// 触发1分钟事件
	svc.PostEvent(&worldEvent.OneMinutePassed{})

	svc.fiveMinuteCounter += 1
	if svc.fiveMinuteCounter >= 5 {
		svc.fiveMinuteCounter = 0
		// 触发5分钟事件
		svc.PostEvent(&worldEvent.FiveMinutePassed{})
	}

	svc.oneHourCounter += 1
	if svc.oneHourCounter >= 60 {
		svc.oneHourCounter = 0
		// todo 触发一小时事件
		svc.PostEvent(&worldEvent.OneHourPassed{})
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (Service) IWorld() {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (svc *Service) DBC() *model.SimpleClient {
	return svc.dbc
}

func (svc *Service) Data() *model.World {
	return svc.data
}

func (svc *Service) SaveData(force bool) {
	if svc.data == nil || svc.cache == nil {
		return
	}

	if !force {
		if !svc.cache.Dirty {
			return
		}
	}

	dbSession := svc.dbc.GetSession()
	defer svc.dbc.PutSession(dbSession)

	_, err := svc.dbc.UpsertWorld(dbSession, bson.M{"_id": svc.data.ID}, svc.data)
	if err != nil {
		svc.Error("save data fail, %s", err)
	} else {
		svc.Debug("save data success")
	}

	svc.cache.Dirty = false
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (svc *Service) Cache() *cache.World {
	return svc.cache
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (svc Service) Config() cluster.WorldConfig {
	return *svc.cfg
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//func (svc *Service) RDS() *redis.Client {
//	return svc.redisCli
//}
