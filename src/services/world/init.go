package world

import (
	"server/src/services/world/logic/common"
	"server/src/services/world/logic/mail"
	"server/src/services/world/logic/role"
	"server/src/services/world/logic/zone"
)

func Init() {
	common.Init()
	mail.Init()
	role.Init()
	zone.Init()
}
