@setlocal enabledelayedexpansion
@setlocal enableextensions

@set WORK_DIR=%~dp0

@set TOOLS_DIR=%WORK_DIR%\src\nggs\bin\tools

@set LOG_DIR=%WORK_DIR%\log
@set PROTOS_DIR=%WORK_DIR%\src\protos
@set CLUSTER_DIR=%WORK_DIR%\src\cluster
@set MODEL_DIR=%WORK_DIR%\src\model
@set MSG_DIR=%WORK_DIR%\src\msg
@set SERVICES_DIR=%WORK_DIR%\src\services
@set RPC_DIR=%WORK_DIR%\src\rpc
@set DATA_DIR=%WORK_DIR%\data
@set SD_DIR=%WORK_DIR%\src\sd
@set BOT_DIR=%WORK_DIR%\src\bot

@IF "%1" == "" call :appd & cd %WORK_DIR% & goto :exit

@IF "%1" == "all" call :all & cd %WORK_DIR% & goto :exit

@IF "%1" == "clean-log" call :clean-log & cd %WORK_DIR% & goto :exit

@IF "%1" == "mod-tidy" call :mod-tidy & cd %WORK_DIR% & goto :exit

::@IF "%1" == "sync-sd" call :sync-sd & cd %WORK_DIR% & goto :exit

@IF "%1" == "sd" call :sd & cd %WORK_DIR% & goto :exit

@IF "%1" == "sd-test" call :sd-test & cd %WORK_DIR% & goto :exit

@IF "%1" == "msg" call :msg & cd %WORK_DIR% & goto exit

@IF "%1" == "model" call :model & cd %WORK_DIR% & goto exit

@IF "%1" == "rpc" call :rpc & cd %WORK_DIR% & goto exit

@IF "%1" == "appd" call :appd & cd %WORK_DIR% & goto :exit

@IF "%1" == "app" call :app & cd %WORK_DIR% & goto :exit

@IF "%1" == "bot-cfg" call :bot-cfg & cd %WORK_DIR% & goto :exit

@IF "%1" == "bot-model" call :bot-model & cd %WORK_DIR% & goto :exit

@IF "%1" == "bot" call :bot & cd %WORK_DIR% & goto :exit

@echo unsupported operate [%1]

@goto :exit


:all
@echo make all begin
@call :msg
@call :model
@call :rpc
::@call :sd
@call :appd
::@call :bot-cfg
::@call :bot
@echo make all end
cd %WORK_DIR%
@goto :exit


:clean-log
rmdir /q /s "%LOG_DIR%"
@goto exit


:mod-tidy
@echo [mod-tidy] begin
@echo off
@for /R %WORK_DIR% %%f in (*.mod) do (
    @set "GO_MOD_FILE_DIR=%%~dpf"
    @cd !GO_MOD_FILE_DIR!
    echo go mod tidy in [!GO_MOD_FILE_DIR!]
    go mod tidy
)
@echo on
@echo [mod-tidy] end
@goto :exit


:sync-sd
@echo [sync-sd] begin
del /q /s %WORK_DIR%\config\excel\*.xlsx
del /q /s %WORK_DIR%\config\json\*.json
svn up %AFK_TRUNK%\templates
copy %AFK_TRUNK%\templates\*.xlsx %WORK_DIR%\config\excel\
del /q /q %WORK_DIR%\config\excel\enums.xlsx
copy %AFK_TRUNK%\templates\enums.xlsx %WORK_DIR%\config\enums.xlsx
cd %WORK_DIR%\config
%TOOLS_DIR%\gen_static_data_json -cfg=static_data_json.json
%TOOLS_DIR%\gen_static_data_code -cfg=static_data_code.json
cd %SD_DIR%
ren %AFK_TRUNK%\tools\static_data_checker\sd\sd.go sd.go.bak
del /q /s %AFK_TRUNK%\tools\static_data_checker\sd\*.go
copy .\*.go %AFK_TRUNK%\tools\static_data_checker\sd
del /q /s %AFK_TRUNK%\tools\static_data_checker\sd\sd.go
ren %AFK_TRUNK%\tools\static_data_checker\sd\sd.go.bak sd.go
cd %MSG_DIR%
del /q /s %AFK_TRUNK%\tools\static_data_checker\msg\*.go
copy .\*.go %AFK_TRUNK%\tools\static_data_checker\msg
cd %SD_DIR%
go test
@echo [sync-sd] end
@goto :exit


:sd
@echo [sd] begin
cd %DATA_DIR%
%TOOLS_DIR%\gen_static_data_json -cfg=static_data_json.json
%TOOLS_DIR%\gen_static_data_code -cfg=static_data_code.json
cd %SD_DIR%
go test
@echo [sd] end
@goto :exit


:sd-test
@echo [sd-test] begin
cd %SD_DIR%
go test -v
@echo [sd-test] end
@goto :exit


:msg
@echo build [msg] begin
cd %MSG_DIR%
del /q /s *.pb.go
del /q /s *.msg.go
del /q /s derived.gen.go
cd %PROTOS_DIR%
@call gen_msg.bat %WORK_DIR%
cd %MSG_DIR%
%TOOlS_DIR%\goderive.exe .
go test -v
@cd %WORK_DIR%
@echo build [msg] end
@goto exit


:model
@echo build [model] begin
cd %MODEL_DIR%
del /q /s *.pb.go
del /q /s *.mgo.go
del /q /s derived.gen.go
cd %PROTOS_DIR%
@call gen_model.bat %WORK_DIR%
cd %MODEL_DIR%
%TOOlS_DIR%\goderive.exe .
go test -v
@cd %WORK_DIR%
@echo build [model] end
@goto exit


:rpc
@echo build [rpc] begin
cd %RPC_DIR%
del /q /s *.pb.go
del /q /s *.rpc.go
del /q /s derived.gen.go
cd %PROTOS_DIR%
@call gen_rpc.bat %WORK_DIR%
cd %RPC_DIR%
%TOOlS_DIR%\goderive.exe .
go test -v
@cd %WORK_DIR%
@echo build [rpc] end
@goto exit


:appd
@echo [appd] begin
go build -tags debug -o %WORK_DIR%\bin\appd.exe .\src\app
@echo [appd] end
@goto :exit


:app
@echo [program] begin
go build -tags release -o %WORK_DIR%\bin\app.exe .\src\app
@echo [program] end
@goto :exit


:bot-cfg
@echo [bot-cfg] begin
cd %DATA_DIR%
%TOOLS_DIR%\gen_static_data_json -cfg=bot_config_json.json
%TOOLS_DIR%\gen_static_data_code -cfg=bot_config_code.json
cd %BOT_DIR%\config
go test
@echo [bot-cfg] end
@goto :exit


:bot-model
@echo build [bot-model] begin
cd %PROTOS_DIR%
@call gen_bot_model.bat %WORK_DIR%
cd %BOT_DIR%\cache
go test -v
@cd %WORK_DIR%
@echo build [bot-model] end
@goto exit


:bot
@echo [bot] begin
go build -o %WORK_DIR%\bin\bot.exe .\src\bot
@echo [bot] end
@goto :exit


:exit